DROP TABLE IF EXISTS `detalles_orden_impre`;
CREATE TABLE IF NOT EXISTS `detalles_orden_impre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden` int(11) NOT NULL,
  `fecha_solicita` date NOT NULL,
  `fecha_servicio` date NOT NULL,
  `ing_asignado` text COLLATE utf8_spanish_ci NOT NULL,
  `nom_razon` text COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `direc` text COLLATE utf8_spanish_ci NOT NULL,
  `giro` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `contacto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tel` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `info_rl` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info_te` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info_ta` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info_tf` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info_il` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `info_rp` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `doc_imss` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_sua` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_certif` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_analisis` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_listado` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_permiso` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `doc_otro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `botas` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `casco` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `chaleco` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `lentes` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `tapones` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `guantes` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `otro_equipo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


DROP TABLE IF EXISTS `detalles_cotiza_impre`;
CREATE TABLE IF NOT EXISTS `detalles_cotiza_impre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(155) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `edo_cp` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tel` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `atencion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `preg1` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg2` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg3` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg4` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg5` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg6` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `preg7` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `declara1` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `declara2` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `declara3` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `declara4` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `declara5` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `metodo1` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `metodo2` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `metodo3` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `metodo4` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `id_cotizacion` int(11) NOT NULL,
  `fecha_reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/* agregar equipo_medidor_detalle */
DROP TABLE IF EXISTS `equipo_medidor_detalle`;
CREATE TABLE IF NOT EXISTS `equipo_medidor_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `line` tinyint(4) NOT NULL,
  `referencia` decimal(14,8) NOT NULL,
  `prueba` decimal(18,3) NOT NULL,
  `absoluto` decimal(6,3) NOT NULL,
  `incerti` decimal(4,2) NOT NULL,
  `valfc` decimal(5,3) NOT NULL,
  `interseccion` decimal(7,5) NOT NULL,
  `pendiente` decimal(7,5) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/* agregar equipo_higro_detalle */
DROP TABLE IF EXISTS `equipo_higro_detalle`;
CREATE TABLE IF NOT EXISTS `equipo_higro_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `patron` decimal(5,2) NOT NULL,
  `calibracion` decimal(5,2) NOT NULL,
  `instrumento` decimal(5,2) NOT NULL,
  `masmenosue` decimal(5,2) NOT NULL,
  `correccion` decimal(6,4) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `nom22` ADD `id_higro` INT NOT NULL AFTER `id_set_resis`;

/* ******************10-04-2024***************************/
/*ALTER TABLE `nom11d_lecturas` ADD `horai1` TIME NOT NULL AFTER `periodo1`, ADD `horaf1` TIME NOT NULL AFTER `horai1`;
ALTER TABLE `nom11d_lecturas` ADD `horai2` TIME NOT NULL AFTER `periodo2`, ADD `horaf2` TIME NOT NULL AFTER `horai2`;
ALTER TABLE `nom11d_lecturas` ADD `horai3` TIME NOT NULL AFTER `periodo3`, ADD `horaf3` TIME NOT NULL AFTER `horai3`;*/

ALTER TABLE `nom11d` ADD `horai1` TIME NOT NULL AFTER `observaciones`, ADD `horaf1` TIME NOT NULL AFTER `horai1`, ADD `horai2` TIME NOT NULL AFTER `horaf1`, ADD `horaf2` TIME NOT NULL AFTER `horai2`, ADD `horai3` TIME NOT NULL AFTER `horaf2`, ADD `horaf3` TIME NOT NULL AFTER `horai3`;
ALTER TABLE `nom11d_reg_espec_acustico` ADD `horai` TIME NOT NULL AFTER `periodo`, ADD `horaf` TIME NOT NULL AFTER `horai`;
ALTER TABLE `nom11d` ADD `val_nrr` FLOAT NOT NULL AFTER `horaf3`;

/* *****************13-09-24******************* */
ALTER TABLE `reconocimiento` CHANGE `giro` `giro` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;