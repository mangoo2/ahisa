
/*--------------------12/11/24---------------------*/
ALTER TABLE `nom22d` CHANGE `area` `area` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;


/*--------------------06/12/24---------------------*/
ALTER TABLE `nom11d_lecturas` ADD `horai1` TIME NOT NULL AFTER `periodo1`, ADD `horaf1` TIME NOT NULL AFTER `horai1`;
ALTER TABLE `nom11d_lecturas` ADD `horai2` TIME NOT NULL AFTER `periodo2`, ADD `horaf2` TIME NOT NULL AFTER `horai2`;
ALTER TABLE `nom11d_lecturas` ADD `horai3` TIME NOT NULL AFTER `periodo3`, ADD `horaf3` TIME NOT NULL AFTER `horai3`;

ALTER TABLE `reconocimiento_nom11` CHANGE `registros_prod` `registros_prod` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `reconocimiento_paso2_nom11` CHANGE `descrip` `descrip` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;