ALTER TABLE `servicios_ahisa` ADD `norma` VARCHAR(2) NOT NULL AFTER `aplica_desc`, ADD `tipo_viatico` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '1=tipo viatico, 0=no viatico' AFTER `norma`;
INSERT INTO `perfiles` (`nombre`) VALUES ('TÉCNICO');
INSERT INTO `permisos_perfil` (`permiso_administrador`, `permiso_catalogos`, `permiso_clientes`, `permiso_configuraciones`, `permiso_creacion`, `permiso_edicion`, `permiso_estatus`, `permiso_envio`, `permiso_datos`, `permiso_cancelacion`, `permiso_programacion`, `permiso_program_env`, `permiso_estatus_servicio`, `permiso_pdf`, `permiso_agenda`, `perfiles_id`) VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'on', 'on', 5);

/* ****************UPDATE 21/06/22 *****************/ 

ALTER TABLE `nom` ADD `txt_intro` LONGTEXT NOT NULL AFTER `norma`;

/* ****************UPDATE 21/06/22 *****************/ 

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 22-06-2022 a las 16:03:45
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes_nom`
--

DROP TABLE IF EXISTS `imagenes_nom`;
CREATE TABLE IF NOT EXISTS `imagenes_nom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `url_img` text COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


/* *******************24-26-22******************************/ 
ALTER TABLE `equipo` ADD `certificado` TEXT NOT NULL AFTER `equipo`;

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 24-06-2022 a las 18:13:36
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes_planos`
--

DROP TABLE IF EXISTS `imagenes_planos`;
CREATE TABLE IF NOT EXISTS `imagenes_planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `url_img` text COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `nom` ADD `num_informe` VARCHAR(25) NOT NULL AFTER `idnom`;

/***************************************************/

ALTER TABLE `equipo` ADD `marca` VARCHAR(50) NOT NULL AFTER `certificado`, ADD `modelo` VARCHAR(50) NOT NULL AFTER `marca`, ADD `no_informe_calibracion` VARCHAR(35) NOT NULL AFTER `modelo`, ADD `fecha_calibracion` DATE NOT NULL AFTER `no_informe_calibracion`;


-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-06-2022 a las 16:28:31
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reconocimiento`
--

DROP TABLE IF EXISTS `reconocimiento`;
CREATE TABLE IF NOT EXISTS `reconocimiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden` int(11) NOT NULL,
  `num_informe` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `cliente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `calle_num` varchar(125) COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `poblacion` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `cp` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `giro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `representa` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `nom_cargo` text COLLATE utf8_spanish_ci NOT NULL,
  `priero` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `segdo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tercero` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `hora_admin` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `hora_otro` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `pero_turno` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `sdo_turno` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tero_turno` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `otro_turno` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `justifica_horario` text COLLATE utf8_spanish_ci NOT NULL,
  `desc_proceso` text COLLATE utf8_spanish_ci NOT NULL,
  `percep_condi` text COLLATE utf8_spanish_ci NOT NULL,
  `plan_mante` text COLLATE utf8_spanish_ci NOT NULL,
  `si` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `no` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `cuales_areas` text COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `reconocimiento`
--

INSERT INTO `reconocimiento` (`id`, `id_orden`, `num_informe`, `fecha`, `cliente`, `calle_num`, `colonia`, `poblacion`, `estado`, `cp`, `rfc`, `giro`, `telefono`, `representa`, `nom_cargo`, `priero`, `segdo`, `tercero`, `hora_admin`, `hora_otro`, `pero_turno`, `sdo_turno`, `tero_turno`, `otro_turno`, `justifica_horario`, `desc_proceso`, `percep_condi`, `plan_mante`, `si`, `no`, `cuales_areas`, `reg`) VALUES
(1, 3, '', '2022-06-27', 'RAZON SOCIAL DE MANGOO SOFTWARE', 'AVE. JUAREZ No. Int PH No. Ext. 1295', 'LA PAZ', 'PUEBLA', 'Pueblita', '72160', '', 'TI', '01800MANGOO', 'EL REPRESENTANTE LEGAL', 'EL REPRESENTANTE LEGAL', '', '', '', '', '', '', '', '', '', 'JUSTIFICACIÓN DEL HORARIO EN EL QUE SE REALIZARÁN LAS MEDICIONES                                     ', 'DESCRIPCIÓN DEL PROCESO PRODUCTIVO                                        ', 'PERCEPCIÓN DE LAS CONDICIONES DE ILUMINACIÓN POR PARTE DEL TRABAJADOR                                          ', 'PLAN DE MANTENIMIENTO DE LUMINARIAS                          ', 'X', '', 'EN CASO AFIRMATIVO, MENCIONAR CUALES SON ESAS ÁREAS', '0000-00-00 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-06-2022 a las 16:33:14
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reconocimiento_paso2_nom25`
--

DROP TABLE IF EXISTS `reconocimiento_paso2_nom25`;
CREATE TABLE IF NOT EXISTS `reconocimiento_paso2_nom25` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_reconocimiento` int(11) NOT NULL,
  `num` tinyint(4) NOT NULL,
  `area` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `requiere_ilum` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `puesto` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `num_trabaja` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `tarea_visual` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_lumin` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `num_lumin` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `existe_incidencia` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `piso` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pared` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `techo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/* **************29-06-22*************** */
ALTER TABLE `imagenes_planos` ADD `tipo` VARCHAR(1) NOT NULL COMMENT '1=puntos de medición, 2=ubicación de las luminarias' AFTER `url_img`;
ALTER TABLE `nom` ADD `txt_desc_proc` TEXT NOT NULL AFTER `txt_intro`;
ALTER TABLE `reconocimiento_paso2_nom25` ADD `actividades` VARCHAR(75) NOT NULL AFTER `existe_incidencia`;
ALTER TABLE `nom` ADD `txt_criterios_metodo` TEXT NOT NULL AFTER `txt_desc_proc`;
ALTER TABLE `nom` ADD `veri_ini` VARCHAR(30) NOT NULL AFTER `fecha`, ADD `veri_fin` VARCHAR(30) NOT NULL AFTER `veri_ini`, ADD `criterio` VARCHAR(55) NOT NULL AFTER `veri_fin`, ADD `cumple_criterio` VARCHAR(35) NOT NULL AFTER `criterio`;
ALTER TABLE `nom` CHANGE `activo` `activo` VARCHAR(1) NULL DEFAULT '1';

ALTER TABLE `reconocimiento` CHANGE `num_informe` `num_informe_rec` VARCHAR(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;

ALTER TABLE `nom25_detalle` ADD `fecha` DATE NOT NULL AFTER `identificacion`;

/* *******************04-06-22********************** */
ALTER TABLE `nom25_detalle` ADD `area` VARCHAR(100) NOT NULL AFTER `h1`, ADD `identifica` VARCHAR(100) NOT NULL AFTER `area`;
ALTER TABLE `nom25_detalle` ADD `area2` VARCHAR(100) NOT NULL AFTER `h2`, ADD `identifica2` VARCHAR(100) NOT NULL AFTER `area2`;
ALTER TABLE `nom25_detalle` ADD `area3` VARCHAR(100) NOT NULL AFTER `h3`, ADD `identifica3` VARCHAR(100) NOT NULL AFTER `area3`;
ALTER TABLE `nom25_detalle` ADD `plano_t1` VARCHAR(1) NOT NULL COMMENT 'h= horizontal, v=vertical y O=oblicuo' AFTER `identifica`;
ALTER TABLE `nom25_detalle` ADD `plano_t2` VARCHAR(1) NOT NULL COMMENT 'h= horizontal, v=vertical y O=oblicuo' AFTER `identifica2`;
ALTER TABLE `nom25_detalle` ADD `plano_t3` VARCHAR(1) NOT NULL COMMENT 'h= horizontal, v=vertical y O=oblicuo' AFTER `identifica3`;
ALTER TABLE `nom` ADD `observaciones` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL AFTER `tablageneral2char`;


/* *******************14-07-22***************************** */
ALTER TABLE `reconocimiento` ADD `adm_turno` VARCHAR(1) NOT NULL AFTER `otro_turno`;
ALTER TABLE `reconocimiento_paso2_nom25` ADD `tipo` VARCHAR(1) NOT NULL COMMENT '1=desc areas, 2=desc activ y tareas' AFTER `techo`;
ALTER TABLE `reconocimiento_paso2_nom25` CHANGE `tarea_visual` `tarea_visual` VARCHAR(75) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;

/* ************************17-06-22*************************/
ALTER TABLE `cotizaciones_has_servicios` ADD `id_tecnico` INT NOT NULL AFTER `cotizacion_id`;
ALTER TABLE `cotizaciones_has_servicios` ADD `unico_tecnico` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1 = si' AFTER `id_empresa_serv`;
ALTER TABLE `nom25_detalle` ADD `id_chs` INT NOT NULL AFTER `idnom`;
/*ALTER TABLE `nom25_detalle` ADD  CONSTRAINT `chs_fk_nom` FOREIGN KEY (`id_chs`) REFERENCES `cotizaciones_has_servicios`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;*/
ALTER TABLE `nom` ADD `id_chs` INT NOT NULL AFTER `idordenes`;
ALTER TABLE `reconocimiento` ADD `id_chs` INT NOT NULL COMMENT 'id de cotizaciones has servicios' AFTER `id_orden`;
ALTER TABLE `nom25_resolucion_equipo` ADD `id_punto` BIGINT NOT NULL AFTER `idnom`;
/*ALTER TABLE `nom25_resolucion_equipo` ADD CONSTRAINT `punto_fk_resolucion` FOREIGN KEY (`id_punto`) REFERENCES `nom_detalle`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;*/
ALTER TABLE `nom25_resolucion_equipo` ADD `todos_ptos` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '1=si, 2=no' AFTER `sencibilidad3`;
ALTER TABLE `nom25_resolucion_equipo` CHANGE `id_punto` `id_punto` BIGINT(20) NOT NULL COMMENT 'es el id del punto de la tabla nom25_detalle';
ALTER TABLE `reconocimiento_inicial` ADD `id_chs` INT NOT NULL COMMENT 'id de cotizaciones has servicios' AFTER `orden`;

ALTER TABLE `empleados` ADD `firma` TEXT NOT NULL AFTER `perfil`;

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 20-07-2022 a las 21:38:33
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condiciones_nom25det`
--

DROP TABLE IF EXISTS `condiciones_nom25det`;
CREATE TABLE IF NOT EXISTS `condiciones_nom25det` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_nom25` bigint(20) NOT NULL,
  `condiciones` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


/* *****************************01-08-22************************************** */
ALTER TABLE `reconocimiento_paso2_nom25` CHANGE `requiere_ilum` `requiere_ilum` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;


/* *************************05-08-22***************************** */
-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-08-2022 a las 00:21:36
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conclusiones`
--

DROP TABLE IF EXISTS `conclusiones`;
CREATE TABLE IF NOT EXISTS `conclusiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_nom` int(11) NOT NULL,
  `total_ptos` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_reg` datetime NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conclusiones`
--

INSERT INTO `conclusiones` (`id`, `id_nom`, `total_ptos`, `fecha_reg`, `id_usuario`) VALUES
(1, 5, '21', '2022-08-05 19:21:13', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-08-2022 a las 00:21:46
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos_conclusion`
--

DROP TABLE IF EXISTS `puntos_conclusion`;
CREATE TABLE IF NOT EXISTS `puntos_conclusion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_conclusion` int(11) NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=iluminacion, 2=reflexion',
  `tipo_incidencia` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=con incidencia,2=sin incidencia',
  `con_incidencia` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `sin_incidencia` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `num_ptos_evalua` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `num_supera` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `num_no_supera` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `img_chart` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conclusion_fk_detalles` (`id_conclusion`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `puntos_conclusion`
--

INSERT INTO `puntos_conclusion` (`id`, `id_conclusion`, `tipo`, `tipo_incidencia`, `con_incidencia`, `sin_incidencia`, `num_ptos_evalua`, `num_supera`, `num_no_supera`, `img_chart`) VALUES
(1, 1, '1', '1', '15', '6', '15', '12', '3', ''),
(2, 1, '1', '1', '15', '6', '15', '10', '5', ''),
(3, 1, '1', '1', '15', '6', '15', '15', '0', ''),
(4, 1, '1', '2', '15', '6', '15', '12', '3', ''),
(25, 1, '2', '1', '14', '7', '14', '12', '2', ''),
(26, 1, '2', '1', '14', '7', '14', '9', '5', ''),
(27, 1, '2', '1', '14', '7', '14', '14', '0', ''),
(28, 1, '2', '2', '14', '7', '7', '5', '2', '');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `puntos_conclusion`
--
ALTER TABLE `puntos_conclusion`
  ADD CONSTRAINT `conclusion_fk_detalles` FOREIGN KEY (`id_conclusion`) REFERENCES `conclusiones` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `puntos_conclusion` ADD `medicion` VARCHAR(1) NOT NULL AFTER `id_conclusion`;