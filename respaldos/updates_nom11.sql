CREATE TABLE IF NOT EXISTS `reconocimiento_nom11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_orden` int(11) NOT NULL,
  `id_chs` int(11) NOT NULL COMMENT 'id de cotizaciones has servicios',
  `num_informe_rec` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `cliente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `calle_num` varchar(125) COLLATE utf8_spanish_ci NOT NULL,
  `colonia` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `poblacion` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `cp` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `rfc` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `giro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `representa` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `nom_cargo` text COLLATE utf8_spanish_ci NOT NULL,
  `priero` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `segdo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tercero` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `hora_admin` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `hora_otro` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
    
  `pero_comida` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `sdo_comida` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tero_comida` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `otro_comida` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `adm_comida` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `desc_proceso` text COLLATE utf8_spanish_ci NOT NULL,
  `plan_mante` text COLLATE utf8_spanish_ci NOT NULL,
  `registros_prod` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


CREATE TABLE IF NOT EXISTS `reconocimiento_paso2_nom11` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_reconocimiento` int(11) NOT NULL,
  `num` tinyint(4) NOT NULL,
  `area` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `puesto` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `num_trabaja` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `descrip` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `tiempo_expo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=desc areas, 2=desc activ',
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `reconocimiento_paso3_nom11`;
CREATE TABLE IF NOT EXISTS `reconocimiento_paso3_nom11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reconocimiento` int(11) NOT NULL,
  `num` tinyint(4) NOT NULL,
  `fuentes_genera` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `min_db` float NOT NULL,
  `max_db` float NOT NULL,
  `tipo_ruido` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `num_trabaja_fijo` tinyint(4) NOT NULL,
  `num_trabaja_mov` tinyint(4) NOT NULL,
  `gradiente` tinyint(4) NOT NULL,
  `puesto_fijo` tinyint(4) NOT NULL,
  `prioridad` tinyint(4) NOT NULL,
  `personal` tinyint(4) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;


ALTER TABLE `reconocimiento_paso2_nom11` ADD CONSTRAINT `recono_fk_paso2` FOREIGN KEY (`id_reconocimiento`) REFERENCES `reconocimiento_nom11`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;
ALTER TABLE `reconocimiento_paso3_nom11` ADD CONSTRAINT `recono_fk_paso3` FOREIGN KEY (`id_reconocimiento`) REFERENCES `reconocimiento_nom11`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;

CREATE TABLE IF NOT EXISTS `reconocimiento_paso4_nom11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_reconocimiento` int(11) NOT NULL,
  `justificacion` text NOT NULL,
  `equipo_protec` text NOT NULL,
  `plano_distri` text NOT NULL,
  `observaciones` text NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recono_fk_paso4` (`id_reconocimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `reconocimiento_paso4_nom11`
--
ALTER TABLE `reconocimiento_paso4_nom11`
  ADD CONSTRAINT `recono_fk_paso4` FOREIGN KEY (`id_reconocimiento`) REFERENCES `reconocimiento_nom11` (`id`) ON UPDATE NO ACTION;
COMMIT;

ALTER TABLE `reconocimiento_paso4_nom11` CHANGE `estatus` `estatus` VARCHAR(1) NOT NULL DEFAULT '1';
ALTER TABLE `equipo` ADD `tipo` VARCHAR(1) NOT NULL AFTER `fecha_calibracion`;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_sonometro_detalle`
--

DROP TABLE IF EXISTS `equipo_sonometro_detalle`;
CREATE TABLE IF NOT EXISTS `equipo_sonometro_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_equipo` bigint(20) NOT NULL,
  `num_calibra` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_calibra` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `equipo_fk_deta` (`id_equipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipo_sonometro_detalle`
--
ALTER TABLE `equipo_sonometro_detalle`
  ADD CONSTRAINT `equipo_fk_deta` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id`) ON UPDATE NO ACTION;
COMMIT;

ALTER TABLE `imagenes_planos` ADD `nom` VARCHAR(3) NOT NULL AFTER `tipo`;
/* solo de momento para el update del add nom-11 */
UPDATE `imagenes_planos` SET nom=25

ALTER TABLE `nom` ADD `txt_proceso_fabrica` TEXT NOT NULL AFTER `txt_criterios_metodo`;

CREATE TABLE IF NOT EXISTS `imagenes_fichas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_nom` int(11) NOT NULL,
  `url_img` text COLLATE utf8_spanish_ci NOT NULL,
  `nom` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

ALTER TABLE `imagenes_programa` ADD `nom` VARCHAR(3) NOT NULL AFTER `idnom`;
ALTER TABLE `empleados` ADD `cedula` TEXT NULL AFTER `firma`;