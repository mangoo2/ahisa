ALTER TABLE `nom25` CHANGE `idnom` `idordenes` INT NULL DEFAULT NULL;

ALTER TABLE `nom25_detalle` ADD `table_generado1` LONGTEXT NULL DEFAULT NULL AFTER `activo`, ADD `table_generado2` LONGTEXT NULL DEFAULT NULL AFTER `table_generado1`, ADD `grafica` LONGTEXT NULL DEFAULT NULL AFTER `table_generado2`;

ALTER TABLE `nom` ADD `tablageneral1` LONGTEXT NULL DEFAULT NULL AFTER `activo`, ADD `tablageneral2` LONGTEXT NULL DEFAULT NULL AFTER `tablageneral1`;

ALTER TABLE `nom` ADD `tablageneral1char` LONGTEXT NULL DEFAULT NULL AFTER `tablageneral2`, ADD `tablageneral2char` LONGTEXT NULL DEFAULT NULL AFTER `tablageneral1char`;
----------------------------------
ALTER TABLE `nom` ADD `fecha` DATE NULL DEFAULT NULL AFTER `txt_intro`;
