-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-06-2022 a las 15:38:43
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reconocimiento_inicial`
--

DROP TABLE IF EXISTS `reconocimiento_inicial`;
CREATE TABLE IF NOT EXISTS `reconocimiento_inicial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden` int(11) NOT NULL,
  `nom_documento` varchar(300) NOT NULL,
  `iden_documento` varchar(100) NOT NULL,
  `version` varchar(10) NOT NULL,
  `no_copia_controlada` varchar(100) NOT NULL,
  `estrategia` text DEFAULT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reconocimiento_inicial`
--

INSERT INTO `reconocimiento_inicial` (`id`, `orden`, `nom_documento`, `iden_documento`, `version`, `no_copia_controlada`, `estrategia`, `reg`, `activo`) VALUES
(1, 1, 'RECONOCIMIENTO INICIAL PARA ILUMINACION', 'REG-TEC/05-01', '1', 'Original', 'se evalua por puesto dijo de trabajo debido a que en cada area solo se encuentra solo 1 persona trabajando', '2022-06-28 15:13:06', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reconocimiento_inicial_detalle`
--

DROP TABLE IF EXISTS `reconocimiento_inicial_detalle`;
CREATE TABLE IF NOT EXISTS `reconocimiento_inicial_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_recini` int(11) NOT NULL,
  `largo` double NOT NULL,
  `ancho` double NOT NULL,
  `altura` double NOT NULL,
  `ic` decimal(10,2) NOT NULL,
  `minimo` double NOT NULL,
  `limitacion` double NOT NULL,
  `puestos` int(11) NOT NULL,
  `puestos_menor` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reconocimiento_inicial_detalle`
--

INSERT INTO `reconocimiento_inicial_detalle` (`id`, `id_recini`, `largo`, `ancho`, `altura`, `ic`, `minimo`, `limitacion`, `puestos`, `puestos_menor`, `activo`) VALUES
(1, 1, 4, 4, 2.5, '0.80', 4, 6, 1, 1, 1),
(2, 1, 30, 13, 10, '0.91', 4, 6, 1, 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
