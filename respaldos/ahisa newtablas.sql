-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-06-2022 a las 15:33:28
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom`
--

DROP TABLE IF EXISTS `nom`;
CREATE TABLE IF NOT EXISTS `nom` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `luxometro` varchar(300) NOT NULL,
  `equipo` varchar(300) NOT NULL,
  `cotizacion` int(11) NOT NULL DEFAULT 0,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom25`
--

DROP TABLE IF EXISTS `nom25`;
CREATE TABLE IF NOT EXISTS `nom25` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idnom` bigint(20) NOT NULL,
  `h1` time NOT NULL,
  `h1_medicion_a` decimal(10,2) NOT NULL,
  `h1_medicion_b` decimal(10,2) NOT NULL,
  `h1_mdicion_c` decimal(10,2) NOT NULL,
  `h2` time NOT NULL,
  `h2_medicion_a` decimal(10,2) NOT NULL,
  `h2_medicion_b` decimal(10,2) NOT NULL,
  `h2_medicion_c` decimal(10,2) NOT NULL,
  `h3` time NOT NULL,
  `h3_medicion_a` decimal(10,2) NOT NULL,
  `h3_medicion_b` decimal(10,2) NOT NULL,
  `h3_medicion_c` decimal(10,2) NOT NULL,
  `nmi` int(11) NOT NULL,
  `h1_e1a` decimal(10,2) NOT NULL,
  `h1_e2a` decimal(10,2) NOT NULL,
  `h2_e1a` decimal(10,2) NOT NULL,
  `h2_e2a` decimal(10,2) NOT NULL,
  `h3_e1a` decimal(10,2) NOT NULL,
  `h3_e2a` decimal(10,2) NOT NULL,
  `h1_e1b` decimal(10,2) NOT NULL,
  `h1_e2b` decimal(10,2) NOT NULL,
  `h2_e1b` decimal(10,2) NOT NULL,
  `h2_e2b` decimal(10,2) NOT NULL,
  `h3_e1b` decimal(10,2) NOT NULL,
  `h3_e2b` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom_detalle`
--

DROP TABLE IF EXISTS `nom_detalle`;
CREATE TABLE IF NOT EXISTS `nom_detalle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idnom` bigint(20) NOT NULL,
  `escala_ini` decimal(10,2) NOT NULL,
  `escala_fin` decimal(10,2) NOT NULL,
  `iluminancia_ref` decimal(10,2) NOT NULL,
  `iluminancia_pro` decimal(10,2) NOT NULL,
  `insertidumbre` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `nom_fk_detalle` (`idnom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `nom_detalle`
--
ALTER TABLE `nom_detalle`
  ADD CONSTRAINT `nom_fk_detalle` FOREIGN KEY (`idnom`) REFERENCES `nom` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
