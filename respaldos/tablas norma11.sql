-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-08-2022 a las 21:31:15
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11`
--

DROP TABLE IF EXISTS `nom11`;
CREATE TABLE IF NOT EXISTS `nom11` (
  `idnom` int(11) NOT NULL AUTO_INCREMENT,
  `num_informe` varchar(25) NOT NULL,
  `idordenes` int(11) NOT NULL,
  `id_chs` int(11) DEFAULT NULL,
  `razon_social` text DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnom`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11`
--

INSERT INTO `nom11` (`idnom`, `num_informe`, `idordenes`, `id_chs`, `razon_social`, `fecha`, `reg`, `activo`) VALUES
(1, 'AHISA/202205-03RL', 4, NULL, 'Comercializadora Gonac, S.A. de C.V.', '2022-07-26', '2022-07-26 23:19:25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11d`
--

DROP TABLE IF EXISTS `nom11d`;
CREATE TABLE IF NOT EXISTS `nom11d` (
  `idnomd` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `punto` int(11) NOT NULL,
  `tiempo_ruido` int(11) DEFAULT NULL,
  `ti` int(11) NOT NULL,
  `te` int(11) NOT NULL,
  `area` varchar(100) NOT NULL,
  `identificacion` varchar(100) NOT NULL,
  `tipo_ruido` tinyint(1) NOT NULL DEFAULT 0,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnomd`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11d`
--

INSERT INTO `nom11d` (`idnomd`, `idnom`, `punto`, `tiempo_ruido`, `ti`, `te`, `area`, `identificacion`, `tipo_ruido`, `reg`, `activo`) VALUES
(1, 1, 1, 7, 420, 480, 'Extrusores', 'Frente al tablerox', 0, '2022-07-26 23:19:25', 1),
(3, 1, 2, 0, 0, 0, '', '', 0, '2022-07-27 20:29:54', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11d_faepa`
--

DROP TABLE IF EXISTS `nom11d_faepa`;
CREATE TABLE IF NOT EXISTS `nom11d_faepa` (
  `idnomd` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `punto` int(11) NOT NULL,
  `faepa_125` decimal(10,2) DEFAULT NULL,
  `faepa_250` decimal(10,2) DEFAULT NULL,
  `faepa_500` decimal(10,2) DEFAULT NULL,
  `faepa_1000` decimal(10,2) DEFAULT NULL,
  `faepa_2000` decimal(10,2) DEFAULT NULL,
  `faepa_3000` decimal(10,2) DEFAULT NULL,
  `faepa_4000` decimal(10,2) DEFAULT NULL,
  `faepa_6000` decimal(10,2) DEFAULT NULL,
  `faepa_8000` decimal(10,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnomd`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11d_faepa`
--

INSERT INTO `nom11d_faepa` (`idnomd`, `idnom`, `punto`, `faepa_125`, `faepa_250`, `faepa_500`, `faepa_1000`, `faepa_2000`, `faepa_3000`, `faepa_4000`, `faepa_6000`, `faepa_8000`, `activo`) VALUES
(1, 1, 1, '29.40', '29.30', '30.70', '32.30', '37.40', '41.90', '38.50', '40.90', '43.80', 1),
(3, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11d_lecturas`
--

DROP TABLE IF EXISTS `nom11d_lecturas`;
CREATE TABLE IF NOT EXISTS `nom11d_lecturas` (
  `idnomd` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `punto` int(11) NOT NULL,
  `periodo1` decimal(10,2) DEFAULT NULL,
  `periodo2` decimal(10,2) DEFAULT NULL,
  `periodo3` decimal(10,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnomd`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11d_lecturas`
--

INSERT INTO `nom11d_lecturas` (`idnomd`, `idnom`, `punto`, `periodo1`, `periodo2`, `periodo3`, `activo`) VALUES
(1, 1, 1, '84.60', '84.60', '0.00', 1),
(3, 1, 1, '84.60', '84.70', '0.00', 1),
(4, 1, 1, '84.90', '84.60', '0.00', 1),
(5, 1, 1, '84.80', '84.80', '0.00', 1),
(6, 1, 1, '84.90', '85.10', '0.00', 1),
(7, 1, 1, '84.80', '85.00', '0.00', 1),
(8, 1, 1, '84.60', '84.20', '0.00', 1),
(9, 1, 1, '84.70', '84.80', '0.00', 1),
(10, 1, 1, '85.80', '84.60', '0.00', 1),
(11, 1, 1, '84.50', '84.80', '0.00', 1),
(12, 1, 2, NULL, NULL, NULL, 0),
(13, 1, 2, NULL, NULL, NULL, 0),
(14, 1, 1, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11d_puntos`
--

DROP TABLE IF EXISTS `nom11d_puntos`;
CREATE TABLE IF NOT EXISTS `nom11d_puntos` (
  `idnomd` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `punto` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnomd`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11d_puntos`
--

INSERT INTO `nom11d_puntos` (`idnomd`, `idnom`, `punto`, `activo`) VALUES
(1, 1, 1, 1),
(4, 1, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom11d_reg_espec_acustico`
--

DROP TABLE IF EXISTS `nom11d_reg_espec_acustico`;
CREATE TABLE IF NOT EXISTS `nom11d_reg_espec_acustico` (
  `idnomd` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `punto` int(11) NOT NULL,
  `periodo` int(11) DEFAULT NULL,
  `rea_db` decimal(10,2) DEFAULT NULL,
  `rea_lineal` decimal(10,2) DEFAULT NULL,
  `rea_31_5` decimal(10,2) DEFAULT NULL,
  `rea_63` decimal(10,2) DEFAULT NULL,
  `rea_125` decimal(10,2) DEFAULT NULL,
  `rea_250` decimal(10,2) DEFAULT NULL,
  `rea_500` decimal(10,2) DEFAULT NULL,
  `rea_1000` decimal(10,2) DEFAULT NULL,
  `rea_2000` decimal(10,2) DEFAULT NULL,
  `rea_4000` decimal(10,2) DEFAULT NULL,
  `rea_8000` decimal(10,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idnomd`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom11d_reg_espec_acustico`
--

INSERT INTO `nom11d_reg_espec_acustico` (`idnomd`, `idnom`, `punto`, `periodo`, `rea_db`, `rea_lineal`, `rea_31_5`, `rea_63`, `rea_125`, `rea_250`, `rea_500`, `rea_1000`, `rea_2000`, `rea_4000`, `rea_8000`, `activo`) VALUES
(1, 1, 1, 1, '85.10', '87.00', '71.10', '74.30', '76.70', '78.40', '80.90', '78.80', '76.40', '78.70', '75.30', 1),
(3, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 1, 1, NULL, '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', '87.00', 0),
(7, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
