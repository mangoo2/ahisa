-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-06-2022 a las 22:40:23
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

DROP TABLE IF EXISTS `equipo`;
CREATE TABLE IF NOT EXISTS `equipo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luxometro` varchar(300) NOT NULL,
  `equipo` varchar(300) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id`, `luxometro`, `equipo`, `reg`, `activo`) VALUES
(1, 'AH-LX-EV/01', 'M154879CM1371120', '2022-06-06 18:30:39', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_detalle`
--

DROP TABLE IF EXISTS `equipo_detalle`;
CREATE TABLE IF NOT EXISTS `equipo_detalle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idnom` bigint(20) NOT NULL,
  `escala_ini` decimal(10,2) NOT NULL,
  `escala_fin` decimal(10,2) NOT NULL,
  `iluminancia_ref` decimal(10,2) NOT NULL,
  `iluminancia_pro` decimal(10,2) NOT NULL,
  `insertidumbre` decimal(10,2) NOT NULL,
  `resolucion` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `nom_fk_detalle` (`idnom`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo_detalle`
--

INSERT INTO `equipo_detalle` (`id`, `idnom`, `escala_ini`, `escala_fin`, `iluminancia_ref`, `iluminancia_pro`, `insertidumbre`, `resolucion`, `activo`) VALUES
(1, 1, '1000.00', '9999.00', '4000.00', '3790.34', '41.45', '1.00', 1),
(3, 1, '1000.00', '9999.00', '2000.00', '1909.44', '20.50', '1.00', 1),
(4, 1, '100.00', '999.90', '1007.02', '953.74', '10.56', '0.10', 1),
(5, 1, '100.00', '999.90', '701.67', '683.54', '7.55', '0.10', 1),
(6, 1, '100.00', '999.90', '506.63', '493.14', '5.75', '0.10', 1),
(7, 1, '100.00', '999.90', '300.00', '291.34', '3.39', '0.10', 1),
(8, 1, '100.00', '999.90', '201.05', '189.99', '6.40', '0.10', 1),
(9, 1, '100.00', '999.90', '100.00', '94.97', '3.18', '0.10', 1),
(10, 1, '0.00', '99.99', '50.00', '49.41', '2.49', '0.01', 1),
(11, 1, '0.00', '99.99', '20.00', '20.72', '3.32', '0.01', 1),
(27, 1, '0.00', '99.99', '10.00', '10.23', '1.66', '0.01', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipo_detalle`
--
ALTER TABLE `equipo_detalle`
  ADD CONSTRAINT `nom_fk_detalle` FOREIGN KEY (`idnom`) REFERENCES `equipo` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
