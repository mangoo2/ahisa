-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 04-07-2022 a las 18:10:36
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom25_resolucion_equipo`
--

DROP TABLE IF EXISTS `nom25_resolucion_equipo`;
CREATE TABLE IF NOT EXISTS `nom25_resolucion_equipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnom` int(11) NOT NULL,
  `escala1` int(11) DEFAULT NULL,
  `escala2` int(11) DEFAULT NULL,
  `escala3` int(11) DEFAULT NULL,
  `distribucion1` varchar(100) DEFAULT NULL,
  `distribucion2` varchar(100) DEFAULT NULL,
  `distribucion3` varchar(100) DEFAULT NULL,
  `sencibilidad1` float DEFAULT NULL,
  `sencibilidad2` float DEFAULT NULL,
  `sencibilidad3` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
