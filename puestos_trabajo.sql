-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 22-02-2023 a las 23:04:54
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puestos_trabajo`
--

DROP TABLE IF EXISTS `puestos_trabajo`;
CREATE TABLE IF NOT EXISTS `puestos_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(10) DEFAULT NULL,
  `puesto_trabajo` text DEFAULT NULL,
  `area` text DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puestos_trabajo`
--

INSERT INTO `puestos_trabajo` (`id`, `identificacion`, `puesto_trabajo`, `area`, `nivel`, `activo`) VALUES
(1, 'N1', 'En exteriores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.', 'Exteriores generales: patios y estacionamientos.', 20, 1),
(2, 'N2', 'En interiores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.', 'Interiores generales: almacenes de poco movimiento, pasillos, escaleras, estacionamientos cubiertos, labores en minas subterráneas, iluminación de emergencia.', 50, 1),
(3, 'N3', 'En interiores.', 'Áreas de circulación y pasillos; salas de espera; salas de descanso; cuartos de almacén; plataformas; cuartos de calderas', 100, 1),
(4, 'N4', 'Requerimiento visual simple: inspección visual, recuento de piezas, trabajo en banco y máquina.', 'Servicios al personal: almacenaje rudo, recepción y despacho, casetas de vigilancia, cuartos de compresores y pailería.', 200, 1),
(5, 'N5', 'Distinción moderada de detalles: ensamble simple, trabajo medio en banco y máquina, inspección simple, empaque y trabajos de oficina.', 'Talleres: áreas de empaque y ensamble, aulas y oficinas.', 300, 1),
(6, 'N6', 'Distinción clara de detalles: maquinado y acabados delicados, ensamble de inspección moderadamente difícil, captura y procesamiento de información, manejo de instrumentos y equipo de laboratorio.', 'Talleres de precisión: salas de cómputo, áreas de dibujo, laboratorios.', 500, 1),
(7, 'N7', 'Distinción fina de detalles: maquinado de precisión, ensamble e inspección de trabajos delicados, manejo de instrumentos y equipo de precisión, manejo de piezas pequeñas.', 'Talleres de alta precisión: de pintura y acabado de superficies y laboratorios de control de calidad.', 750, 1),
(8, 'N8', 'Alta exactitud en la distinción de detalles: ensamble, proceso e inspección de piezas pequeñas y complejas, acabado con pulidos finos.', 'Proceso: ensamble e inspección de piezas complejas y acabados con pulidos finos.', 1000, 1),
(9, 'N9', 'Alto grado de especialización en la distinción de detalles,', 'Proceso de gran exactitud. Ejecución de tareas visuales:\r\n                        • De bajo contraste y tamaño muy pequeño por periodos prolongados;\r\n                        • Exactas y muy prolongadas, y\r\n                        • Muy especiales de extremadamente bajo contraste y pequeño tamaño.', 2000, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
