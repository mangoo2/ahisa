var base_url = $('#base_url').val();
var table;
$(document).ready(function() {	
	table = $('#table_equipo').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#table_equipo').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Equipos/getlistfacturas",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            //{"data": "id"},
            {"data": "luxometro"},
            {"data": "equipo"},
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    	html+='<a href="'+base_url+'Equipos/add/'+row.id+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a>';
                    	html+='<button type="button" class="btn btn-sm btn-icon btn-danger white" onclick="deleteequipo('+row.id+')"><i class="ft-trash-2"></i></button>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],    
    });
}
function deleteequipo(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el equipo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/deleteequipo",
                    data: {
                        idequipo:id
                    },
                    success:function(response){  
                        
                            loadtable();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}