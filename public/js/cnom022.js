var base_url = $('#base_url').val();
let rowpunto=1;
let html="";
let html2="";
let pto_ant=0;
$(document).ready(function($) {
	if($("#idnom").val()=="0"){
        addpunto();
    }
    if($("#idnom").val()!="0"){
        infopuntos($("#idnom").val());
    }

    $("#punto_prin").on("change",function(){
        //console.log("cambia punto: "+$("#punto_prin").val());
        let TABLAFT   = $("#table_fuentes tbody > tr");
        TABLAFT.each(function(){         
            $(this).find("input[id*='punto']").val($("#punto_prin").val());
        });
    });
});

function cambiaPunto(val){
    //console.log("cambia punto: "+val);
    //console.log("val: "+val);
    //console.log("pto_ant: "+pto_ant);
    let TABLAFT   = $("#table_fuentes tbody > tr");
    TABLAFT.each(function(){ 
        //console.log("punto: "+$(this).find("input[id*='punto']").val());
        if(pto_ant==$(this).find("input[class*='ptos_fts_"+pto_ant+"']").val())
            $(this).find("input[id*='punto']").val(val);
    });  
}

function addpunto(band=0){
	//addpuntos(0,"","","","","",0,"","","","","","","","","","","","","","","","","","",0,"","","",0,"","","",0,"","","",0,"","","","","");
    addpuntos(0,"","","","","",0,"","","","","","","","","","","","","","","","","","","");
    addpuntos2(0,"",0,"","","",0,"","","",0,"","","",0,"","","","","");
    //console.log("rowpunto: "+rowpunto);
    rowpunto_ant = rowpunto-1;
    if(band!=0){
        $('html, body').animate({
            scrollTop: $(".table_rowpuntofts_"+rowpunto_ant+"").offset().top
        }, 2000);
    }
}

function addpuntos(id,punto,area,fecha,ubicacion,desconectado,pararrayo,id_valor,d1,d2,d3,d4,d5,d6,d7,d8,d9,r1,r2,r3,r4,r5,r6,r7,r8,r9){
    //console.log("fecha: "+fecha);
    //console.log("fuente de addpuntos: "+fuente);
    //console.log("valor de addpuntos: "+valor);

    let fecha_pri="";
    if(fecha=="" || fecha=="0000-00-00"){
        fecha_pri=$("#fecha").val();
    }else if(fecha!="" && fecha!="0000-00-00"){
        fecha_pri=fecha;
    }
    desconectado1="";desconectado2="";
    if(desconectado=="1")
        desconectado1="checked";
    if(desconectado=="2")
        desconectado2="checked";

    pararrayo1="";pararrayo2="";
    if(pararrayo=="1")
        pararrayo1="checked";
    if(pararrayo=="2")
        pararrayo2="checked";

    if(punto=="")
        punto=rowpunto;

    //console.log("valor de punto en addpuntos: "+punto);
    //console.log("fecha_pri: "+fecha_pri);
    let buttondelete='<button type="button" \
                      class="btn btn-sm btn-raised gradient-ibiza-sunset white" \
                      onclick="deletepunto('+id+','+rowpunto+')"><i class="fa fa-trash-o"></i></button>';


    html='<table class="table table-bordered tablecaptura" id="table_puntos">\
                <tbody class="trbody">\
                    <tr class="rowpunto_'+rowpunto+'">\
                        <td>\
                            <div class="row">\
                                <div class="col-md-2 col-sm-5 col-5">\
                                    '+buttondelete+' No. Punto\
                                </div>\
                                <div class="col-md-1 col-sm-3 col-3">\
                                    <input type="number" onchange="cambiaPunto(this.value)" id="punto_prin" value="'+punto+'" class="form-control">\
                                </div>\
                                <div class="col-md-1">\
                                    Área\
                                </div>\
                                <div class="col-md-2 col-sm-8 col-8">\
                                    <input type="hidden" id="id" value="'+id+'" class="form-control">\
                                    <input type="text" id="area" value="'+area+'" class="form-control area_'+rowpunto+'">\
                                </div>\
                                <div class="col-md-1">\
                                    Fecha\
                                </div>\
                                <div class="col-md-3 col-sm-8 col-8">\
                                    <input type="date" id="fecha_pto" value="'+fecha_pri+'" class="form-control fecha_pto_'+rowpunto+'">\
                                </div>\
                            </div>\
                            <div class="row">\
                                <div class="col-md-2">\
                                    Ubicación del electrodo\
                                </div>\
                                <div class="col-md-5 col-sm-8 col-8">\
                                    <input type="text" id="ubicacion" value="'+ubicacion+'" class="form-control"><br>\
                                </div>\
                                <div class="col-md-2">\
                                    ¿Electrodo desconectado?\
                                </div>\
                                <div class="col-md-2 col-sm-4 col-4">\
                                    <label>SI<input onclick="deconectadoR(this, 1);" type="radio" '+desconectado1+' id="desconectadoS" class="form-control form-control-sm chk_form"></label>\
                                    <label>NO<input onclick="deconectadoR(this, 0);" type="radio" '+desconectado2+' id="desconectadoN" class="form-control form-control-sm chk_form"></label>\
                                </div>\
                                <div class="col-md-2">\
                                    ¿Pararrayos?\
                                </div>\
                                <div class="col-md-2 col-sm-4 col-4">\
                                    <label>SI<input onclick="pararrayosR(this, 1);" type="radio" '+pararrayo1+' id="pararrayosS" class="form-control form-control-sm chk_form"></label>\
                                    <label>NO<input onclick="pararrayosR(this, 0);" type="radio" '+pararrayo2+' id="pararrayosN" class="form-control form-control-sm chk_form"></label>\
                                </div>\
                            </div>\
                            <div class="row">\
                                <table class="table table-bordered tablecaptura" id="table_valores">\
                                    <thead><tr><td style="font-size:11px; vertical-align: middle; text-align:center" colspan="10"><b>VALORES DE RESISTENCIA</b></td></tr></thead>\
                                    <tbody class="trbodyvals">\
                                        <tr>\
                                            <th class="tdinput" style="font-size:11px; vertical-align: middle">Distancia(m)</th>\
                                            <th class="tdinput"><input type="hidden" id="id_valor" value="'+id_valor+'">\
                                                                <input type="hidden" id="id_nom22d" value="'+id+'">\
                                                                <input min="0" type="text" id="d1" value="'+d1+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d2" value="'+d2+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d3" value="'+d3+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d4" value="'+d4+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d5" value="'+d5+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d6" value="'+d6+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d7" value="'+d7+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d8" value="'+d8+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="d9" value="'+d9+'" class="form-control"></th>\
                                        </tr>\
                                        <tr>\
                                            <th class="tdinput" style="font-size:11px; vertical-align: middle">Resistencia(Ω)</th>\
                                            <th class="tdinput"><input min="0" type="text" id="r1" value="'+r1+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r2" value="'+r2+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r3" value="'+r3+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r4" value="'+r4+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r5" value="'+r5+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r6" value="'+r6+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r7" value="'+r7+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r8" value="'+r8+'" class="form-control"></th>\
                                            <th class="tdinput"><input min="0" type="text" id="r9" value="'+r9+'" class="form-control"></th>\
                                        </tr>\
                                    </tbody>\
                                </table>\
                            </div>\
                        </td>\
                    </tr>\
                </tbody>\
            </table>';
	//$('#cont_mast').append(html2);
	//rowpunto++;
}

function addpuntos2(id,punto,id_fuente,fuente,valor,caracteristica,id_fuente2,fuente2,valor2,caracteristica2,id_fuente3,fuente3,valor3,caracteristica3,id_fuente4,fuente4,valor4,caracteristica4,medicion_humedad,observaciones){
    if(punto=="")
        punto=rowpunto;

    html2=''+html+'\
            <table class="table table-bordered tablecaptura table_rowpuntofts_'+rowpunto+'" id="table_fuentes" align="center">\
                <thead>\
                    <tr><td style="font-size:11px; vertical-align: middle; text-align:center" colspan="5"><b>FUENTES GENERADORAS CONECTADAS AL ELECTRODO</b></td></tr>\
                    <tr style="font-size:12px; text-align:center">\
                        <td style="vertical-align: middle;" width="30%">FUENTE</td>\
                        <td style="vertical-align: middle;" width="15%">VALOR DE<br> CONTINUIDAD</td>\
                        <td style="vertical-align: middle;" width="50%" colspan="2">CARACTERISTICAS DEL SISTEMA DE PARARRAYOS</td>\
                        <td width="5%"></td>\
                    </tr>\
                </thead>\
                <tbody class="trbodyfts">\
                    <tr class="rowpuntofts_'+rowpunto+'">\
                        <td width="30%" class="tdinput"><input type="text" id="fuente" value="'+fuente+'" class="form-control">\
                                            <input type="hidden" id="id_fuente" value="'+id_fuente+'">\
                                            <input type="hidden" id="id_nom22d" value="'+id+'"><input type="hidden" id="tipo" value="1">\
                                            <input class="ptos_fts_'+punto+'" type="hidden" id="punto" value="'+punto+'"></td>\
                        <td width="15%" class="tdinput"><input type="text" id="valor" value="'+valor+'" class="form-control"></td>\
                        <td width="25%" class="tdinput" style="text-align:center; font-size:11px; vertical-align: middle:"><b>Tipo de sistema de pararrayos</b></td>\
                        <td width="25%" class="tdinput"><input type="text" id="caracteristica" value="'+caracteristica+'" class="form-control"></td>\
                        <td width="5%" class="tdinput"><!--<button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletel('+rowpunto+')"><i class="fa fa-trash-o"></i></button>--></td>\
                    </tr>\
                    <tr class="rowpuntofts_'+rowpunto+'">\
                        <td width="30%" class="tdinput"><input type="text" id="fuente2" value="'+fuente2+'" class="form-control">\
                                            <input type="hidden" id="id_fuente2" value="'+id_fuente2+'">\
                                            <input type="hidden" id="id_nom22d" value="'+id+'"><input type="hidden" id="tipo" value="2">\
                                            <input class="ptos_fts_'+punto+'" type="hidden" id="punto" value="'+punto+'"></td>\
                        <td width="15%" class="tdinput"><input type="text" id="valor2" value="'+valor2+'" class="form-control"></td>\
                        <td width="25%" class="tdinput" style="text-align:center; font-size:11px; vertical-align: middle"><b>Ubicación del pararrayos</b></td>\
                        <td width="25%" class="tdinput"><input type="text" id="caracteristica2" value="'+caracteristica2+'" class="form-control"></td>\
                        <td width="5%" class="tdinput"><!--<button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletel('+rowpunto+')"><i class="fa fa-trash-o"></i></button>--></td>\
                    </tr>\
                    <tr class="rowpuntofts_'+rowpunto+'">\
                        <td width="30%" class="tdinput"><input type="text" id="fuente3" value="'+fuente3+'" class="form-control">\
                                            <input type="hidden" id="id_fuente3" value="'+id_fuente3+'">\
                                            <input type="hidden" id="id_nom22d" value="'+id+'"><input type="hidden" id="tipo" value="3">\
                                            <input class="ptos_fts_'+punto+'" type="hidden" id="punto" value="'+punto+'"></td>\
                        <td width="15%" class="tdinput"><input type="text" id="valor3" value="'+valor3+'" class="form-control"></td>\
                        <td width="25%" class="tdinput" style="text-align:center; font-size:11px; vertical-align: middle"><b>Altura de las terminales aéreas (m)</b></td>\
                        <td width="25%" class="tdinput"><input type="text" id="caracteristica3" value="'+caracteristica3+'" class="form-control"></td>\
                        <td width="5%" class="tdinput"><!--<button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletel('+rowpunto+')"><i class="fa fa-trash-o"></i></button>--></td>\
                    </tr>\
                    <tr class="rowpuntofts_'+rowpunto+'">\
                        <td width="30%" class="tdinput"><input type="text" id="fuente4" value="'+fuente4+'" class="form-control">\
                                            <input type="hidden" id="id_fuente4" value="'+id_fuente4+'">\
                                            <input type="hidden" id="id_nom22d" value="'+id+'"><input type="hidden" id="tipo" value="4">\
                                            <input class="ptos_fts_'+punto+'" type="hidden" id="punto" value="'+punto+'"></td>\
                        <td width="15%" class="tdinput"><input type="text" id="valor4" value="'+valor4+'" class="form-control"></td>\
                        <td width="25%" class="tdinput" style="text-align:center; font-size:11px; vertical-align: middle"><b>Área de cobertura de protección (m<sup>2</sup>)</b></td>\
                        <td width="25%" class="tdinput"><input type="text" id="caracteristica4" value="'+caracteristica4+'" class="form-control"></td>\
                        <td width="5%" class="tdinput"><!--<button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletel('+rowpunto+')"><i class="fa fa-trash-o"></i></button>--></td>\
                    </tr>\
                </tbody>\
            </table>\
            <table id="table_meds" class="table table-bordered tablecaptura rowpuntomds_'+rowpunto+'" style="text-align:center" width="100%">\
                <thead>\
                    <tr>\
                        <td style="font-size:12px; vertical-align: middle">MEDICIÓN DE HUMEDAD RELATIVA (%)</td>\
                        <td style="font-size:12px; vertical-align: middle">OBSERVACIONES</td>\
                    </tr>\
                </thead\
                <tbody class="trbodymds">\
                    <tr class="rowpuntofts_'+rowpunto+'">\
                        <td class="tdinput" width="35%"><input type="text" id="medicion_humedad" value="'+medicion_humedad+'" class="form-control">\
                                                        <input type="hidden" id="id_nom22d" value="'+id+'"></td>\
                        <td class="tdinput" width="65%"><input type="text" id="observaciones" value="'+observaciones+'" class="form-control"></td>\
                    </tr>\
                </tbody>\
            </table>';
    $('#cont_mast').append(html2);
    pto_ant = rowpunto;
    rowpunto++;
    
}

function save(){
	let datos = $('#form_nom').serialize();
    //console.log("Datos: " + JSON.stringify(datos));
    //let TABLA   = $("#table_puntos .trbody > tr");
    //if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Nom/submitNom22",
            data: datos,
            async:false,
            beforeSend: function(){
                $(".buttonsave").attr("disabled",true);
            },
            success: function (response){
                var res = $.parseJSON(response);
                console.log("res: "+response);
                if(res.status=="ok"){
                    //saveDetalles(res.idnom,res.id_nomd);
                    if(res.idnom>0){
                        saveDetalles(res.idnom);
                    }
                    swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                    
                    setTimeout(function () { 
                        window.location.href=base_url+"Nom"; 
                    }, 2500);  

                }else{
                    swal("¡Error!", "Error al enviar datos, intente nuevamente", "warning");
                }
            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    /*}else{
        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
    }*/
}

function saveDetalles(idnom) {
	//console.log("saveDetalles");
	/* **********************************/
	let pto_tabla = 0;
	let pto_tabla2 = 0;
	let DATAr = [];
	let TABLA = $("#table_puntos .trbody > tr");

	TABLA.each(function (index) {
		pto_tabla2++;
		//punto = $(this).find("input[id*='punto_prin']").val();
		item = {};

		item["id"] = $(this).find("input[id*='id']").val();
		item["punto"] = $(this).find("input[id*='punto_prin']").val();
		item["area"] = $(this).find("input[id*='area']").val();
		item["fecha_pto"] = $(this).find("input[id*='fecha_pto']").val();
		item["ubicacion"] = $(this).find("input[id*='ubicacion']").val();
		item["desconectado"] = "1";
		if ($(this).find("input[id*='desconectadoS']").is(":checked") == true) {
			item["desconectado"] = "1";
		} else if (
			$(this).find("input[id*='desconectadoN']").is(":checked") == true
		) {
			item["desconectado"] = "2";
		}
		item["pararrayo"] = "2"; //no es pararrayo
		if ($(this).find("input[id*='pararrayosS']").is(":checked") == true) {
			item["pararrayo"] = "1"; //es pararrayo
		}
		item["d1"] = $(this).find("input[id*='d1']").val();
		item["d2"] = $(this).find("input[id*='d2']").val();
		item["d3"] = $(this).find("input[id*='d3']").val();
		item["d4"] = $(this).find("input[id*='d4']").val();
		item["d5"] = $(this).find("input[id*='d5']").val();
		item["d6"] = $(this).find("input[id*='d6']").val();
		item["d7"] = $(this).find("input[id*='d7']").val();
		item["d8"] = $(this).find("input[id*='d8']").val();
		item["d9"] = $(this).find("input[id*='d9']").val();
		item["r1"] = $(this).find("input[id*='r1']").val();
		item["r2"] = $(this).find("input[id*='r2']").val();
		item["r3"] = $(this).find("input[id*='r3']").val();
		item["r4"] = $(this).find("input[id*='r4']").val();
		item["r5"] = $(this).find("input[id*='r5']").val();
		item["r6"] = $(this).find("input[id*='r6']").val();
		item["r7"] = $(this).find("input[id*='r7']").val();
		item["r8"] = $(this).find("input[id*='r8']").val();
		item["r9"] = $(this).find("input[id*='r9']").val();
		DATAr.push(item);
		arraypuntos = JSON.stringify(DATAr);
		//console.log("arraypuntos: "+arraypuntos);
		//console.log("pto_tabla2: "+pto_tabla2);
		//console.log("TABLA.length: "+TABLA.length);

		//if(pto_tabla2>1 && pto_tabla2<=TABLA.length){
		//if(pto_tabla2>0 && pto_tabla2==TABLA.length){
	});

	$.ajax({
		type: "POST",
		url: base_url + "Nom/submitNom22Detalle",
		data: "idnom=" + idnom + "&arraypuntos=" + arraypuntos,
		async: false,
		success: function (data) {
			var res = $.parseJSON(data);
			var idsNomd = JSON.parse(res.ids_nomd);

			//console.log("guardado detalle de nom22: "+res.id_nomd);
			if (res.status == "ok") {
				let id_nomd = res.id_nomd;
				//pto_tabla++;

				for (let index = 0; index < pto_tabla2; index++) {
					id_nomd = idsNomd[index];
					console.log("pto_tabla " + pto_tabla);
					pto_tabla++;
					//console.log("puntos_tabla: "+pto_tabla);

					let DATAFT = [];
					let TABLAFT = $(".table_rowpuntofts_" + pto_tabla + " tbody > tr");
					//$(".table_rowpuntofts_" + pto_tabla + " tbody > tr").addClass("bg-danger");
					TABLAFT.each(function () {
						itemF = {};
						//itemF["id_nomd"] = idsNomd[index];
						itemF["punto"] = $(this).find("input[id*='punto']").val();
						itemF["id_fuente"] = $(this).find("input[id*='id_fuente']").val();
						itemF["id_nom22d"] = id_nomd;
						itemF["tipo"] = $(this).find("input[id*='tipo']").val();
						itemF["fuente"] = $(this).find("input[id*='fuente']").val();
						itemF["valor"] = $(this).find("input[id*='valor']").val();
						itemF["caracteristica"] = $(this)
							.find("input[id*='caracteristica']")
							.val();
						DATAFT.push(itemF);
					});
					arrayfts = JSON.stringify(DATAFT);
					//console.log("arrayfts" + arrayfts);
					//console.log("punto que hereda: " + pto_tabla);

					let DATAFM = [];
					let TABLAMD = $(".rowpuntomds_" + pto_tabla + " tbody > tr");
					//$(".rowpuntomds_" + pto_tabla + " tbody > tr").addClass("bg-success");
					TABLAMD.each(function () {
						//console.log("medicion_humedad: "+$(this).find("input[id*='medicion_humedad']").val());
						itemFM = {};
						itemFM["id_nom22d"] = id_nomd;
						itemFM["medicion_humedad"] = $(this)
							.find("input[id*='medicion_humedad']")
							.val();
						itemFM["observaciones"] = $(this)
							.find("input[id*='observaciones']")
							.val();
						DATAFM.push(itemFM);
					});
					arraymds = JSON.stringify(DATAFM);
					//console.log("arraymds"+arraymds);
					console.log("idnom: " + idnom);
					console.log("id_nomd: " + id_nomd);
					console.log("arrayfts: " + arrayfts);
					console.log("arraymds: " + arraymds);

					$.ajax({
						type: "POST",
						url: base_url + "Nom/submitNom22Fuentes",
						async: false,
						data:
							"idnom=" +
							idnom +
							"&id_nomd=" +
							id_nomd +
							"&arrayfts=" +
							arrayfts +
							"&arraymds=" +
							arraymds,
						success: function (resp_fuente) {
							var res2 = $.parseJSON(resp_fuente);
							if (res2.status == "ok") {
								//console.log("resp_fuente: "+res2.id_fuente);
							} else {
								swal(
									"¡Error!",
									"Error al enviar datos, intente nuevamente",
									"warning"
								);
							}
						},
						error: function (resp_fuente) {
							swal(
								"Error!",
								"Se produjo un error, intente de nuevamente o contacte al administrador del sistema",
								"error"
							);
						},
					});
				}
			} else {
				swal("¡Error!", "Error al enviar datos, intente nuevamente", "warning");
			}
		},
		error: function (response) {
			swal(
				"Error!",
				"Se produjo un error, intente de nuevamente o contacte al administrador del sistema",
				"error"
			);
		},
	});
	//});
}


function infopuntos(id){
    $.ajax({
        type:'POST',
        url: base_url+"Nom/infoDatosNom22",
        data: {
            idpunto:id, id_chs:$("#id_chs").val()
        },
        success: function (data){
            var array = $.parseJSON(data);
            array2 = array.data;
            vacio = array.vac;
            //console.log(array2.idnom);
            //console.log("vacio: "+vacio);
            if(array2.length>0 || vacio!="vacio"){
                $('#idnom').val(array2.idnom);
                $('#idordenes').val(array2.idordenes);
                //$('#num_informe').val(array2.num_informe);
                $('#razon_social').val(array2.razon_social);
                $('#fecha').val(array2.fecha);
                $('#id_medidor').val(array2.id_medidor);
                $('#id_multimetro').val(array2.id_multimetro);
                $('#id_set_resis').val(array2.id_set_resis);
                $('#valor_ref').val(array2.valor_ref);
                $('#veri_ini').val(array2.veri_ini);
                $('#cumple_criterio').val(array2.cumple_criterio);
                $('#veri_fin').val(array2.veri_fin);
                $('#cumple_criterio_fin').val(array2.cumple_criterio_fin);
                $('#criterio').val(array2.criterio);
                $('#valor_rk').val(array2.valor_rk);
                let con_areas=0;
                let tam_array = array2.detalle.length;
                $.each(array2.detalle, function(index,i) {
                    
                    addpuntos(i.id,i.punto,i.area,i.fecha_pto,i.ubicacion,i.desconectado,i.pararrayo,i.id_valor,i.d1,i.d2,i.d3,i.d4,i.d5,i.d6,i.d7,i.d8,i.d9,i.r1,i.r2,i.r3,i.r4,i.r5,i.r6,i.r7,i.r8,i.r9);
                    $.each(array2.fuentes, function(index,j) {
                        //console.log("punto i: "+i.punto);
                        //console.log("punto j: "+j.punto);
                        //console.log("fuente: "+j.fuente);
                        if(i.id==j.id && i.punto==j.punto && con_areas<=tam_array)
                            addpuntos2(j.id,j.punto,j.id_fuente,j.fuente,j.valor,j.caracteristica,j.id_fuente2,j.fuente2,j.valor2,j.caracteristica2,j.id_fuente3,j.fuente3,j.valor3,j.caracteristica3,j.id_fuente4,j.fuente4,j.valor4,j.caracteristica4,j.medicion_humedad,j.observaciones);
                    });
                    con_areas++;
                    id_fuente="";fuente="";valor="";caracteristica="";
                    id_fuente2="";fuente2="";valor2="";caracteristica2="";
                    id_fuente3="";fuente3="";valor3="";caracteristica3="";
                    id_fuente4="";fuente4="";valor4="";caracteristica4="";
                    medicion_humedad="";observaciones="";
                    //$.each(array2.fuentes, function(index,j) {
                        /*console.log("id de nomd: "+i.id);
                        console.log("id de nomd de fuentes: "+j.id);
                        console.log("i de punto: "+i.id);
                        console.log("j de punto de fuentes: "+j.punto);*/
                        /*console.log("fuente: "+j.fuente);
                        console.log("fuente: "+j.fuente2);
                        console.log("fuente: "+j.fuente3);
                        console.log("fuente: "+j.fuente4);
                        id_fuente=j.id_fuente;fuente=j.fuente;valor=j.valor;caracteristica=j.caracteristica;
                        id_fuente2=j.id_fuente2;fuente2=j.fuente2;valor2=j.valor2;caracteristica2=j.caracteristica2;
                        id_fuente3=j.id_fuente3;fuente3=j.fuente3;valor3=j.valor3;caracteristica3=j.caracteristica3;
                        id_fuente4=j.id_fuente4;fuente4=j.fuente4;valor4=j.valor4;caracteristica4=j.caracteristica4;
                        medicion_humedad=j.medicion_humedad;observaciones=j.observaciones;*/

                        //addpuntos(i.id,i.punto,i.area,i.fecha_pto,i.ubicacion,i.desconectado,i.id_valor,i.d1,i.d2,i.d3,i.d4,i.d5,i.d6,i.d7,i.d8,i.d9,i.r1,i.r2,i.r3,i.r4,i.r5,i.r6,i.r7,i.r8,i.r9,i.id_fuente,i.fuente,i.valor,i.caracteristica,i.id_fuente2,i.fuente2,i.valor2,i.caracteristica2,i.id_fuente3,i.fuente3,i.valor3,i.caracteristica3,i.id_fuente4,i.fuente4,i.valor4,i.caracteristica4,i.medicion_humedad,i.observaciones);
                        //if(i.id==j.id && i.punto==j.punto && con_areas<=tam_array)
                            //addpuntos(i.id,i.punto,i.area,i.fecha_pto,i.ubicacion,i.desconectado,i.id_valor,i.d1,i.d2,i.d3,i.d4,i.d5,i.d6,i.d7,i.d8,i.d9,i.r1,i.r2,i.r3,i.r4,i.r5,i.r6,i.r7,i.r8,i.r9,j.id_fuente,j.fuente,j.valor,j.caracteristica,j.id_fuente2,j.fuente2,j.valor2,j.caracteristica2,j.id_fuente3,j.fuente3,j.valor3,j.caracteristica3,j.id_fuente4,j.fuente4,j.valor4,j.caracteristica4,j.medicion_humedad,j.observaciones);
                    //});
                    //addpuntos(i.id,i.punto,i.area,i.fecha_pto,i.ubicacion,i.desconectado,i.id_valor,i.d1,i.d2,i.d3,i.d4,i.d5,i.d6,i.d7,i.d8,i.d9,i.r1,i.r2,i.r3,i.r4,i.r5,i.r6,i.r7,i.r8,i.r9,id_fuente,fuente,valor,caracteristica,id_fuente2,fuente2,valor2,caracteristica2,id_fuente3,fuente3,valor3,caracteristica3,id_fuente4,fuente4,valor4,caracteristica4,medicion_humedad,observaciones);
                }); 

                /*$.each(array2.fuentes, function(index,i) {
                    console.log("punto: "+i.punto);
                    console.log("fuente: "+i.fuente);
                    addpuntos2(i.id,i.punto,i.id_fuente,i.fuente,i.valor,i.caracteristica,i.id_fuente2,i.fuente2,i.valor2,i.caracteristica2,i.id_fuente3,i.fuente3,i.valor3,i.caracteristica3,i.id_fuente4,i.fuente4,i.valor4,i.caracteristica4,i.medicion_humedad,i.observaciones);
                });*/
            }

            
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}
function deletepunto(id,row){
    if(id>0){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar el punto?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Nom/deletePunto22",
                        data: {
                            id:id
                        },
                        success:function(response){                         
                            $('.rowpunto_'+row).remove();
                            $('.rowpunto_'+row).remove();
                            $('.rowpuntoval_'+row).remove();
                            $('.table_rowpuntofts_'+row).remove();
                            $('.rowpuntofts_'+row).remove();
                            $('.rowpuntomds_'+row).remove();
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
        $('.rowpunto_'+row).remove();
        $('.rowpuntoval_'+row).remove();
        $('.table_rowpuntofts_'+row).remove();
        $('.rowpuntofts_'+row).remove();
        $('.rowpuntomds_'+row).remove();
        rowpunto--;
    }
}

function deconectadoR(input, tipo){
    const parent = $(input).parent().parent();
    const targetN = parent.find('#desconectadoN');
    const targetS = parent.find('#desconectadoS');

    if(tipo==1){
        $(targetN).prop('checked', false);
        $(targetS).prop('checked', true);
    } else  if(tipo ==0){
        $(targetS).prop('checked', false);
        $(targetN).prop('checked', true);
    }
    
}

function pararrayosR(input, tipo){
    const parent = $(input).parent().parent();
    const targetN = parent.find('#pararrayosN');
    const targetS = parent.find('#pararrayosS');

    if(tipo==1){
        $(targetN).prop('checked', false);
        $(targetS).prop('checked', true);
    } else  if(tipo ==0){
        $(targetS).prop('checked', false);
        $(targetN).prop('checked', true);
    }
    
}