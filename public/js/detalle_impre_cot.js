var base_url = $("#base_url").val();
$(document).ready(function () {

});
function save_detalles(){
    var form_register = $('#form_cotizacion');
    //var preg1=1; var preg2=1; var preg3=1; var preg4=1; var preg5=1; var preg6=1; var preg7=1;
    $("#preg1").is(":checked")==true ? preg1=1 : preg1=2;
    $("#preg2").is(":checked")==true ? preg2=1 : preg2=2;
    $("#preg3").is(":checked")==true ? preg3=1 : preg3=2;
    $("#preg4").is(":checked")==true ? preg4=1 : preg4=2;
    $("#preg5").is(":checked")==true ? preg5=1 : preg5=2;
    $("#preg6").is(":checked")==true ? preg6=1 : preg6=2;
    $("#preg7").is(":checked")==true ? preg7=1 : preg7=2;

    $("#declara1").is(":checked")==true ? declara1=1 : declara1=0;
    $("#declara2").is(":checked")==true ? declara2=1 : declara2=0;
    $("#declara3").is(":checked")==true ? declara3=1 : declara3=0;
    $("#declara4").is(":checked")==true ? declara4=1 : declara4=0;
    $("#declara5").is(":checked")==true ? declara5=1 : declara5=0;

    $("#metodo1").is(":checked")==true ? metodo1=1 : metodo1=0;
    $("#metodo2").is(":checked")==true ? metodo2=1 : metodo2=0;
    $("#metodo3").is(":checked")==true ? metodo3=1 : metodo3=0;
    $("#metodo4").is(":checked")==true ? metodo4=1 : metodo4=0;
    var datos = "&preg1="+preg1+"&preg2="+preg2+"&preg3="+preg3+"&preg4="+preg4+"&preg5="+preg5+"&preg6="+preg6+"&preg7="+preg7+"&declara1="+declara1+"&declara2="+declara2+"&declara3="+declara3+"&declara4="+declara4+"&declara5="+declara5+"&metodo1="+metodo1+"&metodo2="+metodo2+"&metodo3="+metodo3+"&metodo4="+metodo4;
    $.ajax({
        data: form_register.serialize()+datos,
        type: 'POST',
        url : base_url+'Cotizaciones/submit_detalleImpresion',
        async: false,
        beforeSend: function(){
            $("#save").attr("disabled",true);
         },
        success: function(data){
            var id = $("#id_cotizacion").val();
            swal("¡Éxito!", "Guardado correctamente", "success");
            window.open(base_url+"Cotizaciones/pdfCotizacion/"+id, '_blank');
            setTimeout(function () {  window.location.href = base_url+"Cotizaciones" }, 1500);
        }
    }); 
}


function checkSlidersPago(input, tipo){
    if($(input).is(":checked") == true){
        switch (tipo) {
            case 1:
                //console.log("Elegiste la opción 1");
                $("#metodo2").prop('checked', false);
                $("#metodo3").prop('checked', false);
                $("#metodo4").prop('checked', false);
                break;

            case 2:
                //console.log("Elegiste la opción 2");
                $("#metodo1").prop('checked', false);
                $("#metodo3").prop('checked', false);
                $("#metodo4").prop('checked', false);
                break;

            case 3:
                //console.log("Elegiste la opción 3");
                $("#metodo1").prop('checked', false);
                $("#metodo2").prop('checked', false);
                $("#metodo4").prop('checked', false);
                break;

            case 4:
                //console.log("Elegiste la opción 4");
                $("#metodo1").prop('checked', false);
                $("#metodo2").prop('checked', false);
                $("#metodo3").prop('checked', false);
                break;

            default:
                console.log("Opción no válida");
                $("#metodo1").prop('checked', false);
                $("#metodo2").prop('checked', false);
                $("#metodo3").prop('checked', false);
                $("#metodo4").prop('checked', false);
                break;
        }
    }
}