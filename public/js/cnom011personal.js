var base_url=$('#base_url').val();
function deletep(id){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar la lectura?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Nom/deletenom11_personal",
                        data: {
                            idnomd:id
                        },
                        success:function(response){  
                            window.location.reload();
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });  
}

function deletept(id){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar la lectura?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Nom/deletenom11_personal_ti",
                        data: {
                            idnomd:id
                        },
                        success:function(response){  
                            window.location.reload();
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });  
}
function saveform(table,id){
    setTimeout(function(){ 
       
        if(table=='nom11_personal'){

            var id_dosimetro = $('#id_dosimetro_'+id).val();
            var trabajador = $('#trabajador_'+id).val();
            var area = $('#area_'+id).val();
            var puesto = $('#puesto_'+id).val();
            var dosis = $('#dosis_'+id).val();
            var fecha = $('#fecha_'+id).val();

            var datos='table='+table+'&idnomd='+id+'&id_dosimetro='+id_dosimetro+'&trabajador='+trabajador+'&area='+area+'&puesto='+puesto+'&dosis='+dosis+'&fecha='+fecha;
        }
        if (table=='nom11_personal_ti') {
            var ti = $('#ti_'+id).val();
            var tf = $('#tf_'+id).val();
            var nrr = $('#nrr_'+id).val();

            var datos='table='+table+'&idnomd='+id+'&ti='+ti+'&tf='+tf+'&nrr='+nrr;
        }
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Nom/inserupdate11/",
            data: datos,
            success: function (response){
                
                
            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }, 1000);
    
}