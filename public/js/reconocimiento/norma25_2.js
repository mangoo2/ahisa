var base_url = $('#base_url').val();
$(document).ready(function($) {
	recono_ini();	

});
function adddetalleri(tipo){
	adddetalleri_tr(0,'','','','','','','','');
}

function calculaZona(row_det,val){
    //console.log("row_det: "+row_det);
    //console.log("val: "+val);
    if(val<=1){
        $(".minimo_"+row_det+"").val(4);
        $(".limite_"+row_det+"").val(6);
    }else if(val>1 && val<=2){
        $(".minimo_"+row_det+"").val(9);
        $(".limite_"+row_det+"").val(12);
    }else if(val>2 && val<=3){
        $(".minimo_"+row_det+"").val(16);
        $(".limite_"+row_det+"").val(20);
    }else{
        $(".minimo_"+row_det+"").val(25);
        $(".limite_"+row_det+"").val(30);  
    }
}

var rowtrri=1;
function adddetalleri_tr(tipoid,largo,ancho,altura,ic,min,limit,puestos,npunto){
	if(tipoid==0){
		var buttons='<button type="button" class="btn btn-sm btn-icon btn-danger white" onclick="deleterow('+rowtrri+')"><i class="ft-trash-2"></i></button>';
	}else{
		var buttons='<button type="button" class="btn btn-sm btn-icon btn-danger white" onclick="deleterowi('+rowtrri+','+tipoid+')"><i class="ft-trash-2"></i></button>';
	}
	var html='<tr class="eidetalle_'+rowtrri+'">\
                <td class="tdinput">\
                	'+rowtrri+'\
                	<input type="hidden" id="id" value="'+tipoid+'">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="largo" id="largo" class="form-control largo_'+rowtrri+'" value="'+largo+'" onchange="calcular('+rowtrri+')">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="ancho" id="ancho" class="form-control ancho_'+rowtrri+'" value="'+ancho+'" onchange="calcular('+rowtrri+')">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="altura" id="altura" class="form-control altura_'+rowtrri+'" value="'+altura+'" onchange="calcular('+rowtrri+')">\
                </td>\
                <td class="tdinput">\
                    <input type="text" onchange="calculaZona('+rowtrri+',this.value)" name="ic" id="ic" class="form-control ic_'+rowtrri+'" value="'+ic+'">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="minimo" id="minimo" class="form-control minimo_'+rowtrri+'"" value="'+min+'">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="limitacion" id="limitacion" class="form-control limite_'+rowtrri+'"" value="'+limit+'">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="puestos" id="puestos" class="form-control" value="'+puestos+'">\
                </td>\
                <td class="tdinput">\
                    <input type="text" name="puestos_menor" id="puestos_menor" class="form-control" value="'+npunto+'">\
                </td>\
                <td class="tdinput">\
                	'+buttons+'\
                </td>\
            </tr>';
        $('.table_evaluar_tbody').append(html);
	rowtrri++;
}
function deleterow(row){
	$('.eidetalle_'+row).remove();
    rowtrri--;
}
function deleterowi(row,id){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Nom/deletedetallepuntori",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        
                        swal("Éxito!", "Se ha eliminado", "success");
                        $('.eidetalle_'+row).remove();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function recono_ini(){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Nom/obtenerinfo_reconocimiento_ini",
        data: {
        	orden:$('#id_orden').val()
        },
        success: function (data){
            var array = $.parseJSON(data);
            console.log(array);
            if(array.num==0){
            	$('.savereconocimientoini').html('Guardar');
            }else{
                $('#idri').val(array.table.id);
                $('#estrategia').val(array.table.estrategia);
                $('#iden_documento').val(array.table.iden_documento);
                $('#no_copia_controlada').val(array.table.no_copia_controlada);
                $('#nom_documento').val(array.table.nom_documento);
                $('#orden').val(array.table.orden);
                $.each(array.detalle, function(index, item) {
                    adddetalleri_tr(item.id,item.largo,item.ancho,item.altura,item.ic,item.minimo,item.limitacion,item.puestos,item.puestos_menor)
                });
            	$('.savereconocimientoini').html('Actualizar');
            }
            
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}
function finalizarProceso(){
    swal("¡Proceso Finalizado!", "Guardado correctamente", "success");
    setTimeout(function () {  window.location.href = base_url+"index.php/Ordenes" }, 1500);       
}

function savereconocimientoini(){
	var DATAr  = [];
        var TABLA   = $("#table_evaluar tbody > tr");
            TABLA.each(function(){         
            item = {};
            item["id"] = $(this).find("input[id*='id']").val();
			item["largo"] = $(this).find("input[id*='largo']").val();
			item["ancho"] = $(this).find("input[id*='ancho']").val();
			item["altura"] = $(this).find("input[id*='altura']").val();
			item["ic"] = $(this).find("input[id*='ic']").val();
			item["minimo"] = $(this).find("input[id*='minimo']").val();
			item["limitacion"] = $(this).find("input[id*='limitacion']").val();
			item["puestos"] = $(this).find("input[id*='puestos']").val();
			item["puestos_menor"] = $(this).find("input[id*='puestos_menor']").val();
            DATAr.push(item);
        });
        arraydetalles   = JSON.stringify(DATAr);
    var datos = $('#form_rec_ini').serialize()+'&estrategia='+$('#estrategia').val()+'&arraydetalles='+arraydetalles;
    if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Nom/savereconocimientoini",
            data: datos,
            success: function (response){
                swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                //setTimeout(function () {  window.location.href = base_url+"index.php/Ordenes" }, 1500);
            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }else{
        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
    }
}
function calcular(row){
    var largo =$('.largo_'+row).val();
        if(largo==''){
            largo=0;
        }
    var ancho =$('.ancho_'+row).val();
        if(ancho==''){
            ancho=0;
        }
    var altura =$('.altura_'+row).val();
        if(altura==''){
            altura=0;
        }
    var ic =(parseFloat(largo)*parseFloat(ancho))/(parseFloat(altura)*(parseFloat(largo)+parseFloat(ancho)));
        ic=ic.toFixed(2);
    $('.ic_'+row).val(ic).prop('title', '(('+largo+')('+ancho+'))/(('+altura+')('+largo+' + '+ancho+'))');

    calculaZona(row,$(".ic_"+row+"").val());

}