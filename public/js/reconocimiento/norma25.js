var base_url = $("#base_url").val();
$(document).ready(function () {
    $("#savep1").on("click",function(){
        guardarp1();
    });
    if($("#id").val==0){
        $(".siguientepaso").attr("disabled",true);
    }
    else{
        $(".siguientepaso").attr("disabled",false);
    }
});
function guardarp1(){
    var form_register = $('#form_reco1');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_reco1").valid();
    if(valid) {
        var p_turno = $('#pero_turno').is(":checked")==true?1:0;
        var s_turno = $('#sdo_turno').is(":checked")==true?1:0;
        var t_turno = $('#tero_turno').is(":checked")==true?1:0;
        var o_turno = $('#otro_turno').is(":checked")==true?1:0;
        var adm_turno = $('#adm_turno').is(":checked")==true?1:0;
        var si = $('#si').is(":checked")==true?1:0;
        var no = $('#no').is(":checked")==true?1:0;

        $.ajax({
            data: form_register.serialize()+"&id="+$("#id").val()+"&nom=25"+"&pero_turno="+p_turno+"&sdo_turno="+s_turno+"&tero_turno="+t_turno+"&otro_turno="+o_turno+"&adm_turno="+adm_turno+"&si="+si+"&no="+no,
            type: 'POST',
            url : base_url+'Nom/submit_Reco1',
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data){
                //console.log(data);
                swal("¡Éxito!", "Guardado correctamente", "success");
                $("#id_reconocimiento").val(data);
                //$("#id_reco_paso2").val(data);
                //$("#id_reco_paso2_2").val(data);
                $("input[name*='id_reconocimiento']").val(data);
                /*setTimeout(function () { 
                    window.location = base_url+"Nom/reconocimientoServicio2/"+data+"/2";
                }, 2000);*/

                $(".siguientepaso").attr("disabled",false);
            }
        }); 
    }
}