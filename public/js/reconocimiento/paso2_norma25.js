var base_url = $("#base_url").val();
$(document).ready(function () {
    //console.log("paso 2 norma25");
    $("#savep2").on("click",function(){
        if($("#table_det tbody > tr").length>0){
            guardar();  
        }else{
            swal("Álerta!", "Ingrese al menos un detalle en el paso del reconocimiento", "warning");
        }
        
    });
    $(".add").on("click",function(){
        agregarDetalle();
    });

    if($("#id_reconocimiento").val()!="0"){
        $("#cont_tabla_det").html("");
        getDetalles($("#id_reconocimiento").val());
    }
    /*else{
        agregarDetalle();
    }*/ 

    /* *****************PARA EL PASO 3 QUE FALTÓ, HEREDADO DEL 2************************ */
    $("#savep2_2").on("click",function(){
        guardar2();
    });
    $(".add2_2").on("click",function(){
        agregarDetalle2();
    });

    if($("#id_reconocimiento2").val()>0 || $("#id_reconocimiento").val()>0)
        getDetalles2($("#id_reconocimiento2").val());

    /*********************************************/
    $("#savep5").on("click",function(){
        guardarPaso5();
    });
});

var row=2; var band_paso=0;
function agregarDetalle(){
    var html="";
    
    if($("#id_reco_paso2").val()>0 && band_paso==0){
        //nvo_row=Number($("#id_reco_paso2").val());
        nvo_row=Number($("#table_det").find('tbody tr').length);
        //console.log("nvo_row: "+nvo_row);
        row=nvo_row+1;
        band_paso++;
    }
    html+='<tr id="det_'+row+'">\
            <td class="tdinput" width="10%"><input type="hidden" name="id" class="form-control form-control-sm" value="0">\
            <input type="text" name="num" class="form-control form-control-sm" value="'+row+'"></td>\
            <td class="tdinput" width="15%"><input type="text" name="area" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="15%"><input type="text" name="requiere_ilum" class="form-control form-control-sm"></td>\
            <!--<td class="tdinput" width="5%"><input type="text" name="puesto" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="5%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="5%"><input type="text" name="tarea_visual" class="form-control form-control-sm"> </td>-->\
            <td class="tdinput" width="10%"><input type="text" name="tipo_lumin" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="10%"><input type="text" name="num_lumin" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="10%"><input type="text" name="existe_incidencia" class="form-control form-control-sm"></td>\
            <!--<td class="tdinput" width="15%"><input type="text" name="actividades" class="form-control form-control-sm"></td>-->\
            <td class="tdinput" width="10%"><input type="text" name="piso" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="10%"><input type="text" name="pared" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="9%"><input type="text" name="techo" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="1%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteRow($(this),'+row+')"><i class="fa fa-minus"></i></button></td>\
        </tr>';
    //console.log("html: "+html);
    $("#cont_tabla_det").append(html);
    row++;
}

function deleteRow(id,row_c){
    id.closest("#det_"+row_c+"").remove();
    row--;
}

function guardar(){
    var DATA  = [];
    var TABLA = $("#table_det tbody > tr");                  
    TABLA.each(function(){        
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        item ["id_reconocimiento"] = $("#id_reconocimiento").val();
        item ["num"] = $(this).find("input[name*='num']").val();
        item ["area"] = $(this).find("input[name*='area']").val();
        item ["requiere_ilum"] = $(this).find("input[name*='requiere_ilum']").val();
        /*item ["puesto"] = $(this).find("input[name*='puesto']").val();
        item ["num_trabaja"] = $(this).find("input[name*='num_trabaja']").val();
        item ["tarea_visual"] = $(this).find("input[name*='tarea_visual']").val();*/
        item ["tipo_lumin"] = $(this).find("input[name*='tipo_lumin']").val();
        item ["num_lumin"] = $(this).find("input[name*='num_lumin']").val();
        item ["existe_incidencia"] = $(this).find("input[name*='existe_incidencia']").val();
        //item ["actividades"] = $(this).find("input[name*='actividades']").val();
        item ["piso"] = $(this).find("input[name*='piso']").val();
        item ["pared"] = $(this).find("input[name*='pared']").val();
        item ["techo"] = $(this).find("input[name*='techo']").val();
        item ["nom"]=25;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco2',
        processData: false, 
        contentType: false,
        async: false,
        beforeSend: function(){
            $("#savep2").attr("disabled",true);
        },
        success: function(data2){
            $("#id_reco_paso2").val(data2);
            swal("¡Éxito!", "Guardado correctamente", "success");
            //getDetallesPaso5($("#id_reconocimiento").val());
            getDetalles($("#id_reconocimiento").val());
            
            getDetalles2($("#id_reconocimiento2").val()); //TRAE DATOS DEL PASO 3. QUE ERA 2 Y SE DIVIDIO EN UN TERCER PASO
            
            $("#savep2").attr("disabled",false);
            $("#savep2_2").attr("disabled",false); //activa boton del paso 3
        }
    });
}

function guardarPaso5(){
    var DATA  = [];
    var TABLA = $("#table_det_puesto_area tbody > tr");                  
    TABLA.each(function(){        
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        if($(this).find("input[name*='indice_area']").is(":checked")==true)
            item ["indice_area"] = 1;
        else
            item ["indice_area"] = 0;

        if($(this).find("input[name*='puesto_trabajo']").is(":checked")==true)
            item ["puesto_trabajo"] = 1;
        else
            item ["puesto_trabajo"] = 0;

        if($(this).find("input[name*='pasillo_escalera']").is(":checked")==true)
            item ["pasillo_escalera"] = 1;
        else
            item ["pasillo_escalera"] = 0;

        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco5',
        processData: false, 
        contentType: false,
        async: false,
        success: function(data2){
            swal("¡Éxito!", "Guardado correctamente", "success");
        }
    });
}

function getDetalles(id){ //trae datos del paso 2
    $("#cont_tabla_det").html("");
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, name:"tipo", tipo:1, tabla:"reconocimiento_paso2_nom25"
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html=""; var html2="";
            $.each(array.detalle, function(index, i) {
                //console.log("id de paso 2: "+i.id);
                /*indice_area=""; puesto_trabajo=""; pasillo_escalera="";
                if(i.indice_area==1)
                    indice_area="checked";
                if(i.puesto_trabajo==1)
                    puesto_trabajo="checked";
                if(i.pasillo_escalera==1)
                    pasillo_escalera="checked";*/

                html+='<tr id="det_'+i.id+'">\
                        <td class="tdinput" width="10%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput" width="15%"><input type="text" name="area" class="form-control form-control-sm" value="'+i.area+'"> </td>\
                        <td class="tdinput" width="15%"><input type="text" name="requiere_ilum" class="form-control form-control-sm" value="'+i.requiere_ilum+'"></td>\
                        <!--<td class="tdinput" width="5%"><input type="text" name="puesto" class="form-control form-control-sm" value="'+i.puesto+'"> </td>\
                        <td class="tdinput" width="5%"><input type="text" name="num_trabaja" class="form-control form-control-sm" value="'+i.num_trabaja+'"></td>\
                        <td class="tdinput" width="5%"><input type="text" name="tarea_visual" class="form-control form-control-sm" value="'+i.tarea_visual+'"> </td>-->\
                        <td class="tdinput" width="10%"><input type="text" name="tipo_lumin" class="form-control form-control-sm" value="'+i.tipo_lumin+'"></td>\
                        <td class="tdinput" width="10%"><input type="text" name="num_lumin" class="form-control form-control-sm" value="'+i.num_lumin+'"> </td>\
                        <td class="tdinput" width="10%"><input type="text" name="existe_incidencia" class="form-control form-control-sm" value="'+i.existe_incidencia+'"></td>\
                        <!--<td class="tdinput" width="15%"><input type="text" name="actividades" class="form-control form-control-sm" value="'+i.actividades+'"></td>-->\
                        <td class="tdinput" width="10%"><input type="text" name="piso" class="form-control form-control-sm" value="'+i.piso+'"></td>\
                        <td class="tdinput" width="10%"><input type="text" name="pared" class="form-control form-control-sm" value="'+i.pared+'"> </td>\
                        <td class="tdinput" width="9%"><input type="text" name="techo" class="form-control form-control-sm" value="'+i.techo+'"></td>\
                        <td class="tdinput" width="1%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteDetalle($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                    </tr>';

                    /*html2+='<tr id="det_areas_puesto'+i.id+'">\
                        <td class="tdinput" width="15%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput" width="30%"><input type="text" name="area" class="form-control form-control-sm" value="'+i.area+'"> </td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+indice_area+' name="indice_area" class="form-control form-control-sm chk_form3"></td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+puesto_trabajo+'  name="puesto_trabajo" class="form-control form-control-sm chk_form3"></td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+pasillo_escalera+' name="pasillo_escalera" class="form-control form-control-sm chk_form3"></td>\
                    </tr>';*/
            });
            $("#cont_tabla_det").append(html); 
            //$("#cont_tabla_det_puesto_area").append(html2); 
            getDetallesPaso5(id);
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}

function deleteDetalle(row,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteDetalleNom25",
                    data: {
                        id:id,tabla:"reconocimiento_paso2_nom25"
                    },
                    success:function(response){  
                       row.closest("#det_"+id+"").remove();
                       swal("¡Éxito!", "Eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function getDetallesPaso5(id){
    $("#cont_tabla_det_puesto_area").html(""); 
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, name:"tipo", tipo:1, tabla:"reconocimiento_paso2_nom25"
        },
        async:false,
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html=""; var html2="";
            $.each(array.detalle, function(index, i) {
                //console.log(i.id);
                indice_area=""; puesto_trabajo=""; pasillo_escalera="";
                if(i.indice_area==1)
                    indice_area="checked";
                if(i.puesto_trabajo==1)
                    puesto_trabajo="checked";
                if(i.pasillo_escalera==1)
                    pasillo_escalera="checked";

                    html2+='<tr id="det_areas_puesto'+i.id+'">\
                        <td class="tdinput" width="15%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput" width="30%"><input type="text" name="area" class="form-control form-control-sm" value="'+i.area+'"> </td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+indice_area+' name="indice_area" class="form-control form-control-sm chk_form3"></td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+puesto_trabajo+'  name="puesto_trabajo" class="form-control form-control-sm chk_form3"></td>\
                        <td class="tdinput" width="15%"><input type="checkbox" '+pasillo_escalera+' name="pasillo_escalera" class="form-control form-control-sm chk_form3"></td>\
                    </tr>';
            });
            $("#cont_tabla_det_puesto_area").append(html2); 
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}


/* ********************************************* */
var row2=2; var band_paso2=0;
function agregarDetalle2(){
    var html2="";
    if($("#id_reco_paso2_2").val()>0 && band_paso2==0){
        //nvo_row2=Number($("#id_reco_paso2_2").val());
        nvo_row2=Number($("#table_det2").find('tbody tr').length);
        //console.log("nvo_row: "+nvo_row);
        row2=nvo_row2+1;
        band_paso2++;
    }
    var select_p=pintarpuestos(0);
    html2+='<tr id="det2_'+row2+'">\
            <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="0">\
            <input type="text" name="num" class="form-control form-control-sm" value=""></td>\
            <td class="tdinput" width="25%"><input type="text" name="puesto" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="15%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="25%"><input type="text" name="actividades" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="25%">'+select_p+' </td>\
            <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete2" onclick="deleteRow2($(this),'+row2+')"><i class="fa fa-minus"></i></button></td>\
        </tr>';
    //console.log("html: "+html);
    $("#cont_tabla_det_2").append(html2);
    row2++;
}

function deleteRow2(id,row){
    id.closest("#det2_"+row+"").remove();
    row2--;
}

function guardar2(){
    var DATA  = [];
    var TABLA = $("#table_det2 tbody > tr");                  
    TABLA.each(function(){        
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        item ["id_reconocimiento"] = $("#id_reconocimiento").val();
        item ["num"] = $(this).find("input[name*='num']").val();
        item ["puesto"] = $(this).find("input[name*='puesto']").val();
        item ["num_trabaja"] = $(this).find("input[name*='num_trabaja']").val();
        item ["actividades"] = $(this).find("input[name*='actividades']").val();
        item ["tarea_visual"] = $(this).find("select[name*='tarea_visual'] option:selected").val();
        item ["nom"]=25;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco2_2',
        processData: false, 
        contentType: false,
        async: false,
        beforeSend: function(){
            $("#savep2_2").attr("disabled",true);
        },
        success: function(data2){
            swal("¡Éxito!", "Guardado correctamente", "success");
            $("#savep2_2").attr("disabled",false);
            $(".savereconocimientoini").attr("disabled",false); //activa boton del paso 4
            getDetalles2($("#id_reconocimiento2").val());
        }
    });
}

function getDetalles2(id){
    $("#cont_tabla_det_2").html(""); 
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, name:"tipo", tipo:1, tabla:"reconocimiento_paso2_nom25"
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html2=""; var cont_t2=0;
            $.each(array.detalle, function(index, i) {
                //console.log("ide d tipo 2: "+i.id);
                cont_t2++;
                var select_p=pintarpuestos(i.tarea_visual);
                html2+='<tr id="det2_'+i.id+'">\
                        <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput" width="25%"><input type="text" name="puesto" class="form-control form-control-sm" value="'+i.puesto+'"></td>\
                        <td class="tdinput" width="15%"><input type="text" name="num_trabaja" class="form-control form-control-sm" value="'+i.num_trabaja+'"></td>\
                        <td class="tdinput" width="25%"><input type="text" name="actividades" class="form-control form-control-sm" value="'+i.actividades+'"></td>\
                        <td class="tdinput" width="25%">'+select_p+' </td>\
                        <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete2" onclick="deleteDetalle2($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                    </tr>';
            });
            $("#cont_tabla_det_2").append(html2);
            //console.log("cont_t2: "+cont_t2);
            if(cont_t2==0){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/getDetalles25",
                    data: {
                        id_recon:id, name:"tipo", tipo:1, tabla:"reconocimiento_paso2_nom25"
                    },
                    success: function (data){
                        var array = $.parseJSON(data);
                        //console.log(array);
                        var html2=""; var cont_t2=0;
                        $.each(array.detalle, function(index, i) {
                            //console.log(i.id);
                            cont_t2++;
                            var select_p=pintarpuestos(i.tarea_visual);
                            html2+='<tr id="det2_'+i.id+'">\
                                    <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                                    <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                                    <td class="tdinput" width="25%"><input type="text" name="puesto" class="form-control form-control-sm" value="'+i.puesto+'"></td>\
                                    <td class="tdinput" width="15%"><input type="text" name="num_trabaja" class="form-control form-control-sm" value="'+i.num_trabaja+'"></td>\
                                    <td class="tdinput" width="25%"><input type="text" name="actividades" class="form-control form-control-sm" value="'+i.actividades+'"></td>\
                                    <td class="tdinput" width="25%">'+select_p+' </td>\
                                    <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete2" onclick="deleteDetalle2($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                                </tr>';
                        });
                        $("#cont_tabla_det_2").append(html2); 
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }
             
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}
function pintarpuestos(valor){
    var selectpuestos='<select name="tarea_visual" class="form-control form-control-sm">';
    puestostrabajo.forEach(function (element) {
        //console.log(valor);
        if(parseInt(valor)==parseInt(element.id)){
            var selected='selected';
        }else{
            var selected='';
        }
        selectpuestos+='<option value="'+element.id+'" '+selected+' title="'+element.puesto_trabajo+'">'+element.identificacion+'</option>';
    });
    return selectpuestos;
}
function deleteDetalle2(row,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteDetalleNom25",
                    data: {
                        id:id,tabla:"reconocimiento_paso2_nom25"
                    },
                    success:function(response){  
                       row.closest("#det_"+id+"").remove();
                       swal("¡Éxito!", "Eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
