var base_url = $("#base_url").val();
$(document).ready(function () {
    $("#savep1").on("click",function(){
        guardarp1();
    });
});
function guardarp1(){
    var form_register = $('#form_reco1');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_reco1").valid();
    if(valid) {
        $.ajax({
            data: form_register.serialize()+"&id="+$("#id").val()+"&nom=11",
            type: 'POST',
            url : base_url+'Nom/submit_Reco1',
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data){
                swal("¡Éxito!", "Guardado correctamente", "success");
                $("#id").val(data);
                $("#id_reconocimiento").val(data);
                $("input[name*='id_reconocimiento']").val(data);
            }
        }); 
    }
}