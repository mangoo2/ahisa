var base_url = $("#base_url").val();
$(document).ready(function () {
    $("#savep1").on("click",function(){
        guardarp1();
    });
    $(".add").on("click",function(){
        agregarDetalle();
    });
    if($("#id").val()!="0"){
        getDetalles($("#id").val());
    }

});
function guardarp1(){
    var form_register = $('#form_reco1');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_reco1").valid();
    if(valid) {
        $.ajax({
            data: form_register.serialize()+"&id="+$("#id").val()+"&nom=15",
            type: 'POST',
            url : base_url+'Nom/submit_Reco1',
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data){
                swal("¡Éxito!", "Guardado correctamente", "success");
                $("#id").val(data);
                $("#id_reconocimiento2").val(data);
                $("input[name*='id_reconocimiento']").val(data);
                //$("input[id*='id_reconocimiento2']").val(data);
                guardarDetalles(data);
            }
        }); 
    }
}

var row=2; var band_paso=0;
function agregarDetalle(){
    var html="";
    
    if($("#id_reco_paso2").val()>0 && band_paso==0){
        //nvo_row=Number($("#id_reco_paso2").val());
        nvo_row=Number($("#table_det").find('tbody tr').length);
        //console.log("nvo_row: "+nvo_row);
        row=nvo_row+1;
        band_paso++;
    }
    html+='<tr id="det_'+row+'">\
            <td class="tdinput" width="20%">\
                <input type="hidden" id="id" class="form-control form-control-sm" value="0">\
                <input type="text" id="area" class="form-control form-control-sm">\
            <td class="tdinput" width="5%">\
                <input type="radio" id="condicion_termica" name="condicion_termica_'+row+'" class="form-control form-control-sm chk_form"></td>\
            <td class="tdinput" width="5%">\
                <input type="radio" id="condicion_termica2" name="condicion_termica'+row+'" class="form-control form-control-sm chk_form">\
            </td>\
            <td class="tdinput" width="5%">\
                <input type="radio" id="tipo_area" name="tipo_area_'+row+'" class="form-control form-control-sm chk_form"></td>\
            <td class="tdinput" width="5%">\
                <input type="radio" id="tipo_area2" name="tipo_area_'+row+'" class="form-control form-control-sm chk_form">\
            </td>\
            <td class="tdinput" width="5%">\
                <input type="radio" id="tipo_ventilacion" name="tipo_ventilacion_'+row+'" class="form-control form-control-sm chk_form"></td>\
            <td class="tdinput" width="5%">\
                <input type="radio" id="tipo_ventilacion2" name="tipo_ventilacion_'+row+'" class="form-control form-control-sm chk_form">\
            </td>\
            <td class="tdinput" width="20%"><input type="text" id="generadores_condicion" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="20%"><input type="text" id="puesto" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="10%"><input type="text" id="tiempo_expo" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="10%"><input type="text" id="frecuencia" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteRow($(this),'+row+')"><i class="fa fa-minus"></i></button></td>\
        </tr>';
    //console.log("html: "+html);
    $("#cont_tabla_det").append(html);
    row++;
}

function deleteRow(id,row){
    id.closest("#det_"+row+"").remove();
    row--;
}

function guardarDetalles(id_reco){
    var DATA  = [];
    var TABLA = $("#table_det tbody > tr");                  
    TABLA.each(function(){        
        id_tab=$(this).find("input[id*='id']").val();
        item = {};
        item ["id"] = $(this).find("input[id*='id']").val();
        item ["area"] = $(this).find("input[id*='area']").val();
        if($(this).find("input[class*='condicion_termica_"+id_tab+"']").is(":checked")==true){
            item ["condicion_termica"]=1;
        }else /*if($(this).find("input[id*='condicion_termica2']").is(":checked")==true)*/{
            item ["condicion_termica"]=2;
        }
        if($(this).find("input[class*='tipo_area_"+id_tab+"']").is(":checked")==true){
            item ["tipo_area"]=1;
        }else /*if($(this).find("input[id*='tipo_area2']").is(":checked")==true)*/{
            item ["tipo_area"]=2;
        }
        if($(this).find("input[class*='tipo_ventilacion_"+id_tab+"']").is(":checked")==true){
            item ["tipo_ventilacion"]=1;
        }else /*if($(this).find("input[id*='tipo_ventilacion2']").is(":checked")==true)*/{
            item ["tipo_ventilacion"]=2;
        }
        /*console.log("condicion_termica chk: "+$(this).find("input[id*='condicion_termica']").is(":checked"));
        console.log("condicion_termica: "+item ["condicion_termica"]);
        console.log("tipo_area: "+item ["tipo_area"]);*/
        item ["generadores_condicion"] = $(this).find("input[id*='generadores_condicion']").val();
        item ["puesto"] = $(this).find("input[id*='puesto']").val();
        item ["tiempo_expo"] = $(this).find("input[id*='tiempo_expo']").val();
        item ["frecuencia"] = $(this).find("input[id*='frecuencia']").val();
        item ["id_reconocimiento"] = id_reco;
        item ["nom"]=15;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco2',
        processData: false, 
        contentType: false,
        async: false,
        success: function(data2){
            swal("¡Éxito!", "Guardado correctamente", "success");
            //$("#id_reco_paso2").val(data);
        }
    });
}

function getDetalles(id){
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, tipo:1, tabla:"reconocimiento_areas_nom15"
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html="";
            $.each(array.detalle, function(index, i) {
                //console.log("condicion_termica " +i.condicion_termica);
                cond_ter_chk="";
                cond_ter_chk2="";
                tipo_area_chk="";
                tipo_area_chk2="";
                tipo_ventila_chk="";
                tipo_ventila_chk2="";
                if(i.condicion_termica=="1"){
                    //$(".condicion_termica_"+i.id+"").attr("checked",true);
                    cond_ter_chk="checked";
                }
                else{
                    //$(".condicion_termica2_"+i.id+"").attr("checked",true);
                    cond_ter_chk2="checked";
                }
                if(i.tipo_area=="1"){
                    //$("#tipo_area").attr("checked",true);
                    tipo_area_chk="checked";
                }
                else{
                    //$("#tipo_area2").attr("checked",true);
                    tipo_area_chk2="checked";
                }
                if(i.tipo_ventilacion=="1"){
                    //$("#tipo_ventilacion").attr("checked",true);
                    tipo_ventila_chk="checked";
                }
                else{
                    //$("#tipo_ventilacion2").attr("checked",true);
                    tipo_ventila_chk2="checked";
                }
                
                html+='<tr id="det_'+i.id+'">\
                        <td class="tdinput" width="20%">\
                        <input type="hidden" id="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" id="area" class="form-control form-control-sm" value="'+i.area+'">\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',1,1)" '+cond_ter_chk+' type="radio" id="condicion_termica" name="condicion_termica_'+i.id+'" class="condicion_termica_'+i.id+' form-control form-control-sm chk_form"></td>\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',1,2)" '+cond_ter_chk2+' type="radio" id="condicion_termica2" name="condicion_termica_'+i.id+'" class="condicion_termica2_'+i.id+' form-control form-control-sm chk_form">\
                    </td>\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',2,1)" '+tipo_area_chk+' type="radio" id="tipo_area" name="tipo_area_'+i.id+'" class="tipo_area_'+i.id+' form-control form-control-sm chk_form"></td>\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',2,2)" '+tipo_area_chk2+' type="radio" id="tipo_area2" name="tipo_area_'+i.id+'" class="tipo_area2_'+i.id+' form-control form-control-sm chk_form">\
                    </td>\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',3,1)" '+tipo_ventila_chk+' type="radio" id="tipo_ventilacion" name="tipo_ventilacion_'+i.id+'" class="tipo_ventilacion_'+i.id+' form-control form-control-sm chk_form"></td>\
                    <td class="tdinput" width="5%">\
                        <input onchange="cambiaRadio('+i.id+',3,2)" '+tipo_ventila_chk2+' type="radio" id="tipo_ventilacion2" name="tipo_ventilacion_'+i.id+'" class="tipo_ventilacion2_'+i.id+' form-control form-control-sm chk_form">\
                    </td>\
                    <td class="tdinput" width="20%"><input type="text" id="generadores_condicion" class="form-control form-control-sm" value="'+i.generadores_condicion+'"></td>\
                    <td class="tdinput" width="20%"><input type="text" id="puesto" class="form-control form-control-sm" value="'+i.puesto+'"> </td>\
                    <td class="tdinput" width="10%"><input type="text" id="tiempo_expo" class="form-control form-control-sm" value="'+i.tiempo_expo+'"></td>\
                    <td class="tdinput" width="10%"><input type="text" id="frecuencia" class="form-control form-control-sm" value="'+i.frecuencia+'"></td>\
                        <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteDetalle($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                    </tr>';
            });
           $("#cont_tabla_det").append(html); 
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}

function cambiaRadio(id,tipo,val){
    //console.log("cambia radio");
    var TABLA = $("#table_det tbody > tr");                  
    if(tipo==1 && val==1){ //condicion_termica
        $(".condicion_termica_"+id+"").attr("checked",true);
        $(".condicion_termica2_"+id+"").attr("checked",false);
    }else if(tipo==1 && val==2){
        $(".condicion_termica_"+id+"").attr("checked",false);
        $(".condicion_termica2_"+id+"").attr("checked",true);
    }

    if(tipo==2 && val==1){ //tipo_area
        $(".tipo_area_"+id+"").attr("checked",true);
        $(".tipo_area2_"+id+"").attr("checked",false);
    }else if(tipo==2 && val==2){
        $(".tipo_area_"+id+"").attr("checked",false);
        $(".tipo_area2_"+id+"").attr("checked",true);
    }

    if(tipo==3 && val==1){ //tipo_ventilacion
        $(".tipo_ventilacion_"+id+"").attr("checked",true);
        $(".tipo_ventilacion2_"+id+"").attr("checked",false);
    }else if(tipo==3 && val==2){
        $(".tipo_ventilacion_"+id+"").attr("checked",false);
        $(".tipo_ventilacion2_"+id+"").attr("checked",true);
    }
}

function deleteDetalle(row,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteDetalleNom25",
                    data: {
                        id:id, tabla:"reconocimiento_areas_nom15"
                    },
                    success:function(response){  
                       row.closest("#det_"+id+"").remove();
                       swal("¡Éxito!", "Eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

 /**************************************************************** */
function guardarp2(){
    var form_register = $('#form_reco_2');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_reco_2").valid();
    if(valid) {
        $.ajax({
            data: form_register.serialize()+"&nom=15",
            type: 'POST',
            url : base_url+'Nom/submit_Reco1',
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data){
                swal("¡Éxito!", "Guardado correctamente", "success");
                //setTimeout(function () {  window.location.href = base_url+"index.php/Ordenes" }, 1500);
            }
        }); 
    }
}

function finalizarProceso(){
    swal("¡Proceso Finalizado!", "Guardado correctamente", "success");
    setTimeout(function () {  window.location.href = base_url+"index.php/Ordenes" }, 1500);       
}