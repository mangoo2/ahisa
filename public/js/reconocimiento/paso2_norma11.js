var base_url = $("#base_url").val();
$(document).ready(function () {
    $("#savep2").on("click",function(){
        guardar();
    });
    $(".add").on("click",function(){
        agregarDetalle();
    });

    if($("#id_reconocimiento").val()>0)
        getDetalles($("#id_reconocimiento").val());
    /*else
        agregarDetalle();*/ 

    /* *****************PARA EL PASO 3 QUE FALTÓ, HEREDADO DEL 2************************ */
    $("#savep2_2").on("click",function(){
        guardar2();
    });
    $(".add2_2").on("click",function(){
        agregarDetalle2();
    });

    if($("#id_reconocimiento2").val()>0 || $("#id_reconocimiento").val()>0)
        getDetalles2($("#id_reconocimiento2").val());

    /*********************************************/
});

var row=2; var band_paso=0;
function agregarDetalle(){
    var html="";
    
    if($("#id_reco_paso2").val()>0 && band_paso==0){
        //nvo_row=Number($("#id_reco_paso2").val());
        nvo_row=Number($("#table_det").find('tbody tr').length);
        //console.log("nvo_row: "+nvo_row);
        row=nvo_row+1;
        band_paso++;
    }
    html+='<tr id="det_'+row+'">\
            <td class="tdinput" width="10%"><input type="hidden" name="id" class="form-control form-control-sm" value="0">\
            <input type="text" name="num" class="form-control form-control-sm" value="'+row+'"></td>\
            <td class="tdinput" width="20%"><input type="text" name="area" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="20%"><input type="text" name="puesto" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="10%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="25%"><input type="text" name="descrip" class="form-control form-control-sm"> </td>\
            <td class="tdinput" width="10%"><input type="text" name="tiempo_expo" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteRow($(this),'+row+')"><i class="fa fa-minus"></i></button></td>\
        </tr>';
    //console.log("html: "+html);
    $("#cont_tabla_det").append(html);
    row++;
}

function deleteRow(id,row){
    id.closest("#det_"+row+"").remove();
    row--;
}

function guardar(){
    var DATA  = [];
    var TABLA = $("#table_det tbody > tr");                  
    TABLA.each(function(){        
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        item ["id_reconocimiento"] = $("#id_reconocimiento").val();
        item ["num"] = $(this).find("input[name*='num']").val();
        item ["area"] = $(this).find("input[name*='area']").val();
        item ["puesto"] = $(this).find("input[name*='puesto']").val();
        item ["num_trabaja"] = $(this).find("input[name*='num_trabaja']").val();
        item ["descrip"] = $(this).find("input[name*='descrip']").val();
        item ["tiempo_expo"] = $(this).find("input[name*='tiempo_expo']").val();
        item ["nom"]=11;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco2',
        processData: false, 
        contentType: false,
        async: false,
        success: function(data2){
            swal("¡Éxito!", "Guardado correctamente", "success");
            //$("#id_reco_paso2").val(data);
        }
    });
}

function getDetalles(id){
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, tipo:1, tabla:"reconocimiento_paso2_nom11"
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html="";
            $.each(array.detalle, function(index, i) {
                //console.log(i.id);
                
                html+='<tr id="det_'+i.id+'">\
                        <td class="tdinput" width="10%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput" width="20%"><input type="text" name="area" class="form-control form-control-sm" value="'+i.area+'"> </td>\
                        <td class="tdinput" width="20%"><input type="text" name="puesto" class="form-control form-control-sm" value="'+i.puesto+'"> </td>\
                        <td class="tdinput" width="10%"><input type="text" name="num_trabaja" class="form-control form-control-sm" value="'+i.num_trabaja+'"></td>\
                        <td class="tdinput" width="25%"><input type="text" name="descrip" class="form-control form-control-sm" value="'+i.descrip+'"> </td>\
                        <td class="tdinput" width="10%"><input type="text" name="tiempo_expo" class="form-control form-control-sm" value="'+i.tiempo_expo+'"></td>\
                        <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete" onclick="deleteDetalle($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                    </tr>';
            });
           $("#cont_tabla_det").append(html); 
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}

function deleteDetalle(row,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteDetalleNom25",
                    data: {
                        id:id, tabla:"reconocimiento_paso2_nom11"
                    },
                    success:function(response){  
                       row.closest("#det_"+id+"").remove();
                       swal("¡Éxito!", "Eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


/* ********************PASO 3************************* */
var row2=2; var band_paso2=0;
function agregarDetalle2(){
    var html2="";
    if($("#id_reco_paso2_2").val()>0 && band_paso2==0){
        //nvo_row2=Number($("#id_reco_paso2_2").val());
        nvo_row2=Number($("#table_det2").find('tbody tr').length);
        //console.log("nvo_row: "+nvo_row);
        row2=nvo_row2+1;
        band_paso2++;
    }
    html2+='<tr id="det2_'+row2+'">\
            <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="0">\
            <input type="text" name="num" class="form-control form-control-sm" value=""></td>\
            <td class="tdinput"><input type="text" name="fuentes_genera" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="min_db" class="form-control form-control-sm"></td>\
            <td class="tdinput" ><input type="text" name="max_db" class="form-control form-control-sm"></td>\
            <td class="tdinput" ><input type="text" name="tipo_ruido" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="num_trabaja_fijo" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="num_trabaja_mov" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="gradiente" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="puesto_fijo" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="prioridad" class="form-control form-control-sm"></td>\
            <td class="tdinput"><input type="text" name="personal" class="form-control form-control-sm"></td>\
            <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete2" onclick="deleteRow2($(this),'+row2+')"><i class="fa fa-minus"></i></button></td>\
        </tr>';
    //console.log("html: "+html);
    $("#cont_tabla_det_2").append(html2);
    row2++;
}

function deleteRow2(id,row){
    id.closest("#det2_"+row+"").remove();
    row2--;
}

function guardar2(){
    var DATA  = [];
    var TABLA = $("#table_det2 tbody > tr");                  
    TABLA.each(function(){        
        item = {};
        item ["id"] = $(this).find("input[name*='id']").val();
        item ["id_reconocimiento"] = $("#id_reconocimiento").val();
        item ["num"] = $(this).find("input[name*='num']").val();
        item ["fuentes_genera"] = $(this).find("input[name*='fuentes_genera']").val();
        item ["min_db"] = $(this).find("input[name*='min_db']").val();
        item ["max_db"] = $(this).find("input[name*='max_db']").val();
        item ["tipo_ruido"] = $(this).find("input[name*='tipo_ruido']").val();
        item ["num_trabaja_fijo"] = $(this).find("input[name*='num_trabaja_fijo']").val();
        item ["num_trabaja_mov"] = $(this).find("input[name*='num_trabaja_mov']").val();
        item ["gradiente"] = $(this).find("input[name*='gradiente']").val();
        item ["puesto_fijo"] = $(this).find("input[name*='puesto_fijo']").val();
        item ["prioridad"] = $(this).find("input[name*='prioridad']").val();
        item ["personal"] = $(this).find("input[name*='personal']").val();
        item ["nom"]=11;
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url+'Nom/submit_Reco2_2',
        processData: false, 
        contentType: false,
        async: false,
        success: function(data2){
            swal("¡Éxito!", "Guardado correctamente", "success");
        }
    });
}

function getDetalles2(id){
    $.ajax({
        type:'POST',
        url: base_url+"Nom/getDetalles25",
        data: {
            id_recon:id, tipo:2, tabla:"reconocimiento_paso3_nom11"
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            var html2=""; var cont_t2=0;
            $.each(array.detalle, function(index, i) {
                //console.log("ide d tipo 2: "+i.id);
                cont_t2++;
                html2+='<tr id="det2_'+i.id+'">\
                        <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                        <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                        <td class="tdinput"><input type="text" value="'+i.fuentes_genera+'" name="fuentes_genera" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.min_db+'" name="min_db" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.max_db+'" name="max_db" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.tipo_ruido+'" name="tipo_ruido" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.num_trabaja_fijo+'" name="num_trabaja_fijo" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.num_trabaja_mov+'" name="num_trabaja_mov" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.gradiente+'" name="gradiente" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.puesto_fijo+'" name="puesto_fijo" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.prioridad+'" name="prioridad" class="form-control form-control-sm"></td>\
                        <td class="tdinput"><input type="text" value="'+i.personal+'" name="personal" class="form-control form-control-sm"></td>\
                        <td class="tdinput" width="5%"><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white delete2" onclick="deleteDetalle2($(this),'+i.id+')"><i class="fa fa-minus"></i></button></td>\
                    </tr>';
            });
            $("#cont_tabla_det_2").append(html2);
            /*if(cont_t2==0){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/getDetalles25",
                    data: {
                        id_recon:id, tipo:1, tabla:"reconocimiento_paso2_nom11"
                    },
                    success: function (data){
                        var array = $.parseJSON(data);
                        //console.log(array);
                        var html2=""; var cont_t2=0;
                        $.each(array.detalle, function(index, i) {
                            //console.log(i.id);
                            cont_t2++;
                            html2+='<tr id="det2_'+i.id+'">\
                                    <td class="tdinput" width="5%"><input type="hidden" name="id" class="form-control form-control-sm" value="'+i.id+'">\
                                    <input type="text" name="num" class="form-control form-control-sm" value="'+i.num+'"></td>\
                                    <td class="tdinput"><input type="text" value="" name="fuentes_genera" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="min_db" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="max_db" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="tipo_ruido" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="num_trabaja_fijo" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="num_trabaja_mov" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="gradiente" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="puesto_fijo" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="prioridad" class="form-control form-control-sm"></td>\
                                    <td class="tdinput"><input type="text" value="" name="personal" class="form-control form-control-sm"></td>\
                                </tr>';
                        });
                        $("#cont_tabla_det_2").append(html2); 
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }*/
             
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}

function deleteDetalle2(row,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el registro?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteDetalleNom25",
                    data: {
                        id:id, tabla:"reconocimiento_paso3_nom11"
                    },
                    success:function(response){  
                       row.closest("#det2_"+id+"").remove();
                       swal("¡Éxito!", "Eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

/* ***************** PASO 4 ******************* */

function guardarp4(){
    var form_register = $('#form_reco_4');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input    
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_reco_4").valid();
    if(valid) {
        $.ajax({
            data: form_register.serialize(),
            type: 'POST',
            url : base_url+'Nom/submit_RecoP4',
            async: false,
            beforeSend: function(){
                $("#save").attr("disabled",true);
             },
            success: function(data){
                swal("¡Éxito!", "Guardado correctamente", "success");
                $("#id_recp4").val(data);
                //console.log("data: "+data);
                //$("input[name*='id_reconocimiento']").val(data);
            }
        }); 
    }
}

function finalizarProceso(){
    swal("¡Proceso Finalizado!", "Guardado correctamente", "success");
    setTimeout(function () {  window.location.href = base_url+"index.php/Ordenes" }, 1500);       
}