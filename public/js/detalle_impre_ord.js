var base_url = $("#base_url").val();
$(document).ready(function () {

});
function save_detalles(){
    var form_register = $('#form_orden');
    $("#doc_imss").is(":checked")==true ? doc_imss=1 : doc_imss=0;
    $("#doc_sua").is(":checked")==true ? doc_sua=1 : doc_sua=2;
    $("#doc_certif").is(":checked")==true ? doc_certif=1 : doc_certif=2;
    $("#doc_analisis").is(":checked")==true ? doc_analisis=1 : doc_analisis=2;
    $("#doc_listado").is(":checked")==true ? doc_listado=1 : doc_listado=2;
    $("#doc_permiso").is(":checked")==true ? doc_permiso=1 : doc_permiso=2;

    $("#botas").is(":checked")==true ? botas=1 : botas=0;
    $("#casco").is(":checked")==true ? casco=1 : casco=0;
    $("#chaleco").is(":checked")==true ? chaleco=1 : chaleco=0;
    $("#lentes").is(":checked")==true ? lentes=1 : lentes=0;
    $("#tapones").is(":checked")==true ? tapones=1 : tapones=0;
    $("#guantes").is(":checked")==true ? guantes=1 : guantes=0;

    var id_orden=$("#id_orden").val();
    var id_cot=$("#id_cot").val();
    var datos = "&doc_imss="+doc_imss+"&doc_sua="+doc_sua+"&doc_certif="+doc_certif+"&doc_analisis="+doc_analisis+"&doc_listado="+doc_listado+"&doc_permiso="+doc_permiso+"&botas="+botas+"&casco="+casco+"&chaleco="+chaleco+"&lentes="+lentes+"&tapones="+tapones+"&guantes="+guantes;
    $.ajax({
        data: form_register.serialize()+datos,
        type: 'POST',
        url : base_url+'Ordenes/submit_detalleImpresion',
        async: false,
        beforeSend: function(){
            $("#save").attr("disabled",true);
         },
        success: function(data){
            var DATA  = [];
            var TABLA = $("#tabla_servs tbody > tr");                  
            TABLA.each(function(){        
                item = {};
                item ["id_chs"] = $(this).find("input[id*='id_chs']").val();
                item ["coments"] = $(this).find("textarea[id*='coments']").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            //console.log("aInfo: "+aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url: base_url+'Ordenes/editarServiciosOrden',
                processData: false, 
                contentType: false,
                async: false,
                success: function(data2){

                }
            });
            swal("¡Éxito!", "Guardado correctamente", "success");
            window.open(base_url+"Ordenes/pdfOrden/"+id_orden+"/"+id_cot, '_blank');
            setTimeout(function () {  window.location.href = base_url+"Ordenes" }, 1500);
        }
    }); 
}