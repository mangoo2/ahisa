var base_url = $('#base_url').val();
let rowpunto=1;
let html="";
var arrayfts;
var arrayfondo;
var arrayaisla;
var arraydistft;
var DATAFT=[];
var TABLAFT;
var DATAFN=[];
var TABLAFN;
var DATAAIS=[];
var TABLAAIS;
var DATADFT=[];
var TABLADFT;
$(document).ready(function($) {

    $("#ec_largo, #ec_ancho").on("change",function(){
        //console.log("cambia punto: "+$("#punto_prin").val());
        $("#ec_area").val($("#ec_largo").val()*$("#ec_ancho").val());
    });
    $("#add_zona").on("click",function(){
        addpuntos();
    });
});


function addpunto(){
    addpuntos(0,"","","","","",0,"","","","","","","","","","","","","","","","","","");
}

/*function addpuntos(id,punto,area,fecha,ubicacion,desconectado,pararrayo,id_valor,d1,d2,d3,d4,d5,d6,d7,d8,d9,r1,r2,r3,r4,r5,r6,r7,r8,r9){
    //console.log("fecha: "+fecha);
    //console.log("fuente de addpuntos: "+fuente);
    //console.log("valor de addpuntos: "+valor);

    let fecha_pri="";
    if(fecha=="" || fecha=="0000-00-00"){
        fecha_pri=$("#fecha").val();
    }else if(fecha!="" && fecha!="0000-00-00"){
        fecha_pri=fecha;
    }

    sel_hor="";
    sel_hor2="";
    if(horario=="1")
        sel_hor="selected";
    if(horario=="2")
        sel_hor2="selected";

    if(punto=="")
        punto=rowpunto;

    //console.log("valor de punto en addpuntos: "+punto);
    //console.log("fecha_pri: "+fecha_pri);
    let buttondelete='<button type="button" \
                      class="btn btn-sm btn-raised gradient-ibiza-sunset white" \
                      onclick="deletepunto('+id+','+rowpunto+')"><i class="fa fa-trash-o"></i></button>';


    html='<table class="table table-bordered tablecaptura" id="table_puntos">\
            <tbody class="trbody">\
                <tr class="rowpunto_'+rowpunto+'">\
                    <td>\
                        <div class="row">\
                            <div class="col-md-4 col-sm-6 col-6">\
                                <label>Fecha</label>\
                            </div>\
                            <div class="col-md-8 col-sm-6 col-6 form-group">\
                                <input type="date" class="form-control" id="fecha" name="fecha" value="'+fecha+'">\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-5">\
                                <label>Lugar</label>\
                                <input type="text" name="lugar" id="lugar" class="form-control" value="'+lugar+'">\
                            </div>\
                            <div class="col-md-4">\
                                <label>Horario de medición</label>\
                                <select name="horario" id="horario" class="form-control">\
                                    <option value="1" '+sel_hor+'>Diurno</option>\
                                    <option value="2" '+sel_hor2+'>Nocturno</option>\
                                </select>\
                            </div>\
                            <div class="col-md-3">\
                                <label>Zona crítica</label>\
                                <input type="number" name="zona_critica" id="zona_critica" class="form-control" value="'+zona_critica+'">\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-3">\
                                <label>Verificación inicial</label>\
                                <input type="text" name="veri_ini" id="veri_ini" class="form-control" value="'+veri_ini+'">\
                            </div>\
                            <div class="col-md-3"> \
                                <label>Verificación final</label>\
                                <input type="text" name="veri_fin" id="veri_fin" class="form-control" value="'+veri_fin+'">\
                            </div>\
                            <div class="col-md-3">\
                                <label>Criterio de aceptación</label>\
                                <input type="text" name="criterio" id="criterio" class="form-control" value="'+criterio+'">\
                            </div>\
                            <div class="col-md-3">\
                                <label>Cumple criterio</label>\
                                <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="'+cumple_criterio+'">\
                            </div>\
                            <div class="col-md-3">\
                                <label>Potencia batería inicio</label>\
                                <input type="text" name="pot_bat_ini" id="pot_bat_ini" class="form-control" value="'+pot_bat_ini+'">\
                            </div>\
                            <div class="col-md-3">\
                                <label>Potencia batería fin</label>\
                                <input type="text" name="pot_bat_fin" id="pot_bat_fin" class="form-control" value="'+pot_bat_fin+'">\
                            </div>\
                            <div class="col-md-3">\
                                <div class="form-group">\
                                    <label>Sonómetro</label>\
                                    <select name="id_sonometro" id="id_sonometro" class="form-control">\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="col-md-3">\
                                <div class="form-group">\
                                    <label>Calibrador Acústico</label>\
                                    <select name="id_calibrador" id="id_calibrador" class="form-control">\
                                    </select>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-12 titulosC">\
                                <label>Observaciones</label>\
                            </div>\
                            <div class="col-md-12">\
                                <textarea name="observaciones" class="form-control" rows="2" value="'+observaciones+'">'+observaciones+'></textarea>\
                            </div>\
                        </div>\
                        <table class="table table-responsive">\
                            <tr>\
                                <td class="tdinput titulos">Elemento constructivo</td>\
                                <td class="tdinput titulos">Largo (m)</td>\
                                <td class="tdinput"><input type="number" name="ec_largo" id="ec_largo" class="form-control" value="'+ec_largo+'"></td>\
                                <td class="tdinput titulos">Ancho (m)</td>\
                                <td class="tdinput"><input type="number" name="ec_ancho" id="ec_ancho" class="form-control" value="'+ec_ancho+'"></td>\
                                <td class="tdinput titulos">Área (m2)</td>\
                                <td class="tdinput"><input type="number" id="ec_area" class="form-control" value="'+ec_largo*$ec_ancho+'"></td>\
                            </tr>\
                        </table>\
                    </td>\
                </tr>\
            </tbody>\
        </table>';
        $('#cont_mast').append(html);
	//rowpunto++;
}*/

let cont_zonas=0; let cont_ter_ant=0; let $row=""; let $row2="";
function addpuntos(){
    //console.log("funcion clonar ptos");
    cont_zonas++;
    $row=$(".datos_nom_principal").clone().addClass("datos_nom_clone").addClass("datos_nom_clone_"+cont_zonas+"");
    $row2=$(".datos_nom_principal2").clone().attr("id","datos_nom_clone2").addClass("datos_nom_clone2_"+cont_zonas+"");
    $row2.find("#cont_ocu1").hide().find("#cont_ocu2").hide();

    $row.find(".cleans").val(""); $row2.find(".cleans").val("");
    $row2.find("button").attr("onclick","remove_zona($(this),"+cont_zonas+")").addClass("btn-danger").html("<i class='fa fa-trash-o'></i> Eliminar");

    //console.log("cont_zonas: "+cont_zonas);
    if(cont_zonas==1 && $("#id_aux").val()=="0"){
        //console.log("primr if de id_aux=0: "+$("#id_aux").val());
        $row2.insertAfter(".datos_nom_principal");
        $row.insertAfter(".datos_nom_principal");
    }
    else{
        if($("#id_aux").val()=="0"){
            //console.log("id_aux=0: "+$("#id_aux").val());
            cont_ter_ant = cont_zonas-1;
            $row2.insertAfter(".datos_nom_clone_"+cont_ter_ant+"");
            $row.insertAfter(".datos_nom_clone_"+cont_ter_ant+"");
            //$(".datos_nom_clone_"+cont_ter_ant+"").scrollTop(50);
            /*$('html, body').animate({
                scrollTop: $(".datos_nom_clone_"+cont_ter_ant+"").offset().top
            }, 2000);*/
        }else{
            //console.log("id_aux>0: "+$("#id_aux").val());
            //var id_tr = $('#tabla_nom81 tr:last').attr('id'); 
            /*var id_tr = $('table tr:last').attr('id');
            var id_tr = $("#tabla_nom81").find("tr").length +1;*/
            TABLA = $("#tabla_nom81 tbody > tr");
            last_id=0;
            TABLA.each(function(){ 
                if($(this).find("input[id*='idnom']").val()!=undefined)
                    last_id = $(this).find("input[id*='idnom']").val();
            });
            //console.log(last_id);
            $row2.insertAfter(".datos_nom_detalles_"+last_id+"");
            $row.insertAfter(".datos_nom_detalles_"+last_id+"");  
            //$(".datos_nom_detalles_for_").scrollTop(50);
            /*$('html, body').animate({
                scrollTop: $(".datos_nom_detalles_for").offset().top
            }, 2000);   */
        }
    }
    $('html, body').animate({
        scrollTop: $(".datos_nom_clone_"+cont_zonas+"").offset().top
    }, 2000);
}

function remove_zona(i,cont_ter){
    console.log("cont_ter: "+cont_ter);
    if(cont_ter>0){
        $(".datos_nom_clone_"+cont_ter+"").remove();
        $(".datos_nom_clone2_"+cont_ter+"").remove();
        cont_ter--;
    }else if(cont_ter==0){
        $(".datos_nom_principal").remove();
    }
}

function minimizar(tipo,id){
    if(tipo==0){ //minimiza
        $(".datos_nom_principal_"+id+"").hide("slow");
        $(".datos_nom_detalles_"+id+"").hide("slow");
        $(".cont_max_"+id+"").show();
    }else{ //maximiza
        $(".datos_nom_principal_"+id+"").show("slow");
        $(".datos_nom_detalles_"+id+"").show("slow");
        $(".cont_max_"+id+"").hide();
    }
}

function eliminarZona(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar la zona?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deleteZona081",
                    data: {
                        id:id
                    },
                    success:function(response){                         
                        $('.datos_nom_principal_'+id).remove();
                        $('.datos_nom_detalles_'+id).remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function saveDetalles(){ //guarda principales nvos
    let DATAn  = [];
    let TABLA   = $("#tabla_nom81 tbody > #datos_nom_principal");
    let cont_prin=0;
    TABLA.each(function(){ 
        item = {};
        item ["id"] = $(this).find("input[id*='idnom']").val();
        item ["id_chs"] = $("input[id*='id_chs']").val();
        item ["idordenes"] = $("input[id*='id_orden']").val();
        item ["num_informe"] = $(this).find("input[name*='num_informe']").val();
        item ["razon_social"] = $(this).find("input[name*='razon_social']").val();
        item ["fecha"] = $(this).find("input[name*='fecha']").val();
        item ["lugar"] = $(this).find("input[name*='lugar']").val();
        item ["horario"] = $(this).find("select[name*='horario'] option:selected").val();
        item ["zona_critica"] = $(this).find("input[name*='zona_critica']").val();
        item ["veri_ini"] = $(this).find("input[name*='veri_ini']").val();
        item ["veri_fin"] = $(this).find("input[name*='veri_fin']").val();
        item ["criterio"] = $(this).find("input[name*='criterio']").val();
        item ["cumple_criterio"] = $(this).find("input[name*='cumple_criterio']").val();
        item ["pot_bat_ini"] = $(this).find("input[name*='pot_bat_ini']").val();
        item ["pot_bat_fin"] = $(this).find("input[name*='pot_bat_fin']").val();
        item ["id_sonometro"] = $(this).find("select[name*='id_sonometro'] option:selected").val();
        item ["id_calibrador"] = $(this).find("select[name*='id_calibrador'] option:selected").val();
        item ["observaciones"] = $(this).find("textarea[name*='observaciones']").val();
        item ["ec_largo"] = $(this).find("input[name*='ec_largo']").val();
        item ["ec_ancho"] = $(this).find("input[name*='ec_ancho']").val();
        if($(this).find("input[id*='num_informe']").val()!=""){
            DATAn.push(item);
            cont_prin++;
        }
    });
    array_nom   = JSON.stringify(DATAn);  
    //console.log("array_nom: "+array_nom);
    
    DATAFT  = [];
    TABLAFT   = $(".ruido_fte_prin tbody > tr");
    let cont_fts=0; let hora_inicio=""; let hora_fin="";
    TABLAFT.each(function(){    
        cont_fts++;      
        itemF = {};
        itemF ["id"] = $(this).find("input[id*='id_fte']").val();
        //itemF ["id_nom"] = res.id_nom;
        //console.log("hora_ini: "+$(this).find("input[id*='hora_ini']").val());
        //console.log("cont_fts: "+cont_fts);
        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio = $(this).find("input[id*='hora_ini']").val();
            hora_fin = $(this).find("input[id*='hora_fin']").val(); 
        }
        //console.log("hora_inicio: "+hora_inicio);
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemF ["hora_inicio"] = hora_inicio;
            itemF ["hora_fin"] = hora_fin;
            itemF ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemF ["a"] = $(this).find("input[id*='rfte_a']").val();
            itemF ["b"] = $(this).find("input[id*='rfte_b']").val();
            itemF ["c"] = $(this).find("input[id*='rfte_c']").val();
            itemF ["d"] = $(this).find("input[id*='rfte_d']").val();
            itemF ["e"] = $(this).find("input[id*='rfte_e']").val();
            DATAFT.push(itemF);
        }
        //console.log("hora_inicio: "+hora_inicio);
        //console.log("hora_fin: "+hora_fin);
        //DATAFT.push(itemF);
    });
    arrayfts  = JSON.stringify(DATAFT);
    //console.log("arrayfts principal: "+arrayfts);

    DATAFN=[];
    TABLAFN = $(".ruido_fondo_prin tbody > tr");
    let cont_fondo=0; let hora_inicio2=""; let hora_fin2="";
    TABLAFN.each(function(){  
        cont_fondo++;
        itemFN = {};    

        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio2 = $(this).find("input[id*='hora_ini']").val();
            hora_fin2 = $(this).find("input[id*='hora_fin']").val(); 
        }
        itemFN ["id"] = $(this).find("input[id*='id_fondo']").val();
        //itemFN ["id_nom"] = res.id_nom;
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemFN ["hora_inicio"] = hora_inicio2;
            itemFN ["hora_fin"] = hora_fin2;
            itemFN ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemFN ["uno"] = $(this).find("input[id*='rfte_1']").val();
            itemFN ["dos"] = $(this).find("input[id*='rfte_2']").val();
            itemFN ["tres"] = $(this).find("input[id*='rfte_3']").val();
            itemFN ["cuatro"] = $(this).find("input[id*='rfte_4']").val();
            itemFN ["cinco"] = $(this).find("input[id*='rfte_5']").val();
            DATAFN.push(itemFN);
        }
        //DATAFN.push(itemFN);
    });
    arrayfondo  = JSON.stringify(DATAFN);
    //console.log("arrayfondo principal: "+arrayfondo);

    DATAAIS=[];
    TABLAAIS = $(".ruido_aisla_prin tbody > tr");
    let cont_ais=0; let hora_inicio3=""; let hora_fin3="";
    TABLAAIS.each(function(){  
        itemAIS = {};    
        itemAIS ["id"] = $(this).find("input[id*='id_aisla']").val();
        //itemAIS ["id_nom"] = res.id_nom;
        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio3 = $(this).find("input[id*='hora_ini']").val();
            hora_fin3 = $(this).find("input[id*='hora_fin']").val(); 
        }
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemAIS ["hora_inicio"] = hora_inicio3;
            itemAIS ["hora_fin"] = hora_fin3;
            itemAIS ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemAIS ["a"] = $(this).find("input[id*='rfte_a']").val();
            itemAIS ["b"] = $(this).find("input[id*='rfte_b']").val();
            itemAIS ["c"] = $(this).find("input[id*='rfte_c']").val();
            itemAIS ["d"] = $(this).find("input[id*='rfte_d']").val();
            itemAIS ["e"] = $(this).find("input[id*='rfte_e']").val();
            DATAAIS.push(itemAIS);
        }
        //DATAAIS.push(itemAIS);
    });
    arrayaisla  = JSON.stringify(DATAAIS);
    //console.log("arrayaisla principal: "+arrayaisla);

    DATADFT=[];
    TABLADFT = $(".distancia_fte_prin ");
    //TABLADFT = $(".distancia_fte_prin tbody > .tr_save"); //PROBAR CON ESTA CONFIGURACION
    TABLADFT.each(function(){  
        itemDFN = {};    
        itemDFN ["id"] = $(this).find("input[id*='id_dist']").val();
        //itemDFN ["id_nom"] = res.id_nom;
        itemDFN ["altura_fte"] = $(this).find("input[id*='altura_fte']").val();
        itemDFN ["distancia_fte"] = $(this).find("input[id*='distancia_fte']").val();
        itemDFN ["altura_fondo"] = $(this).find("input[id*='altura_fondo']").val();
        itemDFN ["distancia_fondo"] = $(this).find("input[id*='distancia_fondo']").val();
        itemDFN ["altura_reduc"] = $(this).find("input[id*='altura_reduc']").val();
        itemDFN ["distancia_reduc"] = $(this).find("input[id*='distancia_reduc']").val();
        DATADFT.push(itemDFN);
    });
    arraydistft  = JSON.stringify(DATADFT);
    //console.log("arraydistft principal: "+arraydistft);

    var cont_noms=0;
    $.ajax({
        type:'POST',
        url: base_url+"Nom/submitNom81",
        data: "array_nom="+array_nom+"&arrayfts="+arrayfts+"&arrayfondo="+arrayfondo+"&arrayaisla="+arrayaisla+"&arraydistft="+arraydistft,
        //data: "array_nom="+array_nom,
        async:false,
        beforeSend: function(){
            $(".buttonsave").attr("disabled",true);
        },
        success: function (data){
            //console.log(data);
            var res = $.parseJSON(data);
            res.array_ids.forEach(function(idnom){
                //console.log("idnom de array recorrido: "+idnom);
                guardaDetalles(idnom);
                cont_noms++;
            });
            //console.log("cont_noms: "+cont_noms);
            //console.log("cont_prin: "+cont_prin);
            if(cont_noms == cont_prin){
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function () {  window.location.href = base_url+"index.php/Nom" }, 2500);
            }

            /*swal("¡Éxito!", "Guardado correctamente", "success");
            setTimeout(function () {  window.location.href = base_url+"index.php/Nom" }, 2500);*/
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });    
}

function guardaDetalles(idnom) {
    //console.log("idnom de de guardaDetalles: "+idnom);
    DATAFT  = [];
    TABLAFT   = $(".ruido_fte_"+idnom+" tbody > tr");
    //TABLAFT   = $(".ruido_fte tbody > tr");
    let cont_fts=0; let hora_inicio=""; let hora_fin="";
    TABLAFT.each(function(){    
        cont_fts++;      
        itemF = {};
        itemF ["id"] = $(this).find("input[id*='id_fte']").val();
        //itemF ["id_nom"] = res.id_nom;
        itemF ["id_nom"] = idnom;
        //console.log("hora_ini: "+$(this).find("input[id*='hora_ini']").val());
        //console.log("cont_fts: "+cont_fts);
        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio = $(this).find("input[id*='hora_ini']").val();
            hora_fin = $(this).find("input[id*='hora_fin']").val(); 
        }
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemF ["hora_inicio"] = hora_inicio;
            itemF ["hora_fin"] = hora_fin;
            itemF ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemF ["a"] = $(this).find("input[id*='rfte_a']").val();
            itemF ["b"] = $(this).find("input[id*='rfte_b']").val();
            itemF ["c"] = $(this).find("input[id*='rfte_c']").val();
            itemF ["d"] = $(this).find("input[id*='rfte_d']").val();
            itemF ["e"] = $(this).find("input[id*='rfte_e']").val();
            DATAFT.push(itemF);
        }
        //console.log("hora_inicio: "+hora_inicio);
        //console.log("hora_fin: "+hora_fin);
        //DATAFT.push(itemF);
    });
    arrayfts  = JSON.stringify(DATAFT);
    //console.log("arrayfts con id nom: "+arrayfts);

    DATAFN=[];
    TABLAFN = $(".ruido_fondo_"+idnom+" tbody > tr");
    //TABLAFN = $(".ruido_fondo tbody > tr");
    let cont_fondo=0; let hora_inicio2=""; let hora_fin2="";
    TABLAFN.each(function(){  
        cont_fondo++;
        itemFN = {};    

        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio2 = $(this).find("input[id*='hora_ini']").val();
            hora_fin2 = $(this).find("input[id*='hora_fin']").val(); 
        }
        itemFN ["id"] = $(this).find("input[id*='id_fondo']").val();
        //itemFN ["id_nom"] = res.id_nom;
        itemFN ["id_nom"] = idnom;
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemFN ["hora_inicio"] = hora_inicio2;
            itemFN ["hora_fin"] = hora_fin2;
            itemFN ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemFN ["uno"] = $(this).find("input[id*='rfte_1']").val();
            itemFN ["dos"] = $(this).find("input[id*='rfte_2']").val();
            itemFN ["tres"] = $(this).find("input[id*='rfte_3']").val();
            itemFN ["cuatro"] = $(this).find("input[id*='rfte_4']").val();
            itemFN ["cinco"] = $(this).find("input[id*='rfte_5']").val();
            DATAFN.push(itemFN);
        }
        //DATAFN.push(itemFN);
    });
    arrayfondo  = JSON.stringify(DATAFN);
    //console.log("arrayfondo con id nom: "+arrayfondo);

    DATAAIS=[];
    TABLAAIS = $(".ruido_aisla_"+idnom+" tbody > tr");
    //TABLAAIS = $(".ruido_aisla tbody > tr");
    let cont_ais=0; let hora_inicio3=""; let hora_fin3="";
    TABLAAIS.each(function(){  
        itemAIS = {};    
        itemAIS ["id"] = $(this).find("input[id*='id_aisla']").val();
        //itemAIS ["id_nom"] = res.id_nom;
        itemAIS ["id_nom"] = idnom;
        if($(this).find("input[id*='hora_ini']").val()!=undefined){
            hora_inicio3 = $(this).find("input[id*='hora_ini']").val();
            hora_fin3 = $(this).find("input[id*='hora_fin']").val(); 
        }
        if($(this).find("input[id*='rfte_no']").val()!=undefined){
            itemAIS ["hora_inicio"] = hora_inicio3;
            itemAIS ["hora_fin"] = hora_fin3;
            itemAIS ["no_lectura"] = $(this).find("input[id*='rfte_no']").val();
            itemAIS ["a"] = $(this).find("input[id*='rfte_a']").val();
            itemAIS ["b"] = $(this).find("input[id*='rfte_b']").val();
            itemAIS ["c"] = $(this).find("input[id*='rfte_c']").val();
            itemAIS ["d"] = $(this).find("input[id*='rfte_d']").val();
            itemAIS ["e"] = $(this).find("input[id*='rfte_e']").val();
            DATAAIS.push(itemAIS);
        }
        //DATAAIS.push(itemAIS);
    });
    arrayaisla  = JSON.stringify(DATAAIS);
    //console.log("arrayaisla con id nom: "+arrayaisla);

    DATADFT=[];
    TABLADFT = $(".distancias_fte_"+idnom);
    //TABLADFT = $(".distancias_fte_"+idnom+" tbody > .tr_save"); //PROBAR CON ESTA CONFIGURACION
    //TABLADFT = $(".distancias_fte");
    TABLADFT.each(function(){  
        itemDFN = {};    
        itemDFN ["id"] = $(this).find("input[id*='id_dist']").val();
        //itemDFN ["id_nom"] = res.id_nom;
        itemDFN ["id_nom"] = idnom;
        itemDFN ["altura_fte"] = $(this).find("input[id*='altura_fte']").val();
        itemDFN ["distancia_fte"] = $(this).find("input[id*='distancia_fte']").val();
        itemDFN ["altura_fondo"] = $(this).find("input[id*='altura_fondo']").val();
        itemDFN ["distancia_fondo"] = $(this).find("input[id*='distancia_fondo']").val();
        itemDFN ["altura_reduc"] = $(this).find("input[id*='altura_reduc']").val();
        itemDFN ["distancia_reduc"] = $(this).find("input[id*='distancia_reduc']").val();
        DATADFT.push(itemDFN);
    });
    arraydistft  = JSON.stringify(DATADFT);
    //console.log("arraydistft con id nom: "+arraydistft);

    $.ajax({
        type:'POST',
        url: base_url+"Nom/submitDetallesNom81_indep",
        data: "idnom="+idnom+"&arrayfts="+arrayfts+"&arrayfondo="+arrayfondo+"&arrayaisla="+arrayaisla+"&arraydistft="+arraydistft,
        async:false,
        beforeSend: function(){
            $(".buttonsave").attr("disabled",true);
        },
        success: function (data){
            //swal("¡Éxito!", "Guardado correctamente", "success");
            //setTimeout(function () {  window.location.href = base_url+"index.php/Nom" }, 2500);
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}