var base_url = $("#base_url").val();
var table="";
var status=$("#tipostock option:selected").val();
var logeoEd;
$(document).ready(function () {
    $("#tipostock").on("change",function(){
        load();
    });
    load();
    logeoEd = $('#logeoEd').val();
});

function load(){
    table = $('#tabla').DataTable({
        responsive: true,
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            url: base_url+"index.php/cotizaciones/getData_cotizaciones",
            type: "post",
            data: {status:$("#tipostock option:selected").val()},
            error: function () {
                $("#tabla").css("display", "none");
            }
        },
        "columns": [
            //{"data": "folio"},
            {"data": "id"},
            {"data": "alias"},
            {"data": "fecha_creacion"},
            //{"data": "importe"},
            {"data": null,
                "render" : function ( url, type, full) {
                    var msj='';
                    msj+="$" +full["importe"];
                    return msj;
                }
            },
            //{"data": "descuento"},
            {"data": null,
                "render" : function ( url, type, full) {
                    var msj='';
                    if(full["descuento"]!=null)
                        msj+=full["descuento"] +" %";
                    else
                        msj+="0"+" %";

                    return msj;
                }
            }, 
            {"data": "forma_pago",
                "render": function (data, type, row, meta) {
                    return forma_pago(data);
                }
            },
            {"data": "nombre"},
            {"data": "status",
                "render": function (data, type, row, meta) {
                    return estatus(data);
                }
            },
            {"data": "status",
                "render": function (data, type, row, meta) {
                    return acciones(data);
                }
            }
        ],language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        
    });

    $('#tabla').on('click', 'button.aceptar', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        estatus_cotizacion(data.id, 2);
    });
    $('#tabla').on('click', 'button.rechazar', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        estatus_cotizacion(data.id, 3);
    });
    $('#tabla').on('click', 'button.imprimir', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        imprimir(data.id);
    });
    $('#tabla').on('click', 'a.mail', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        mail(data.id);
    });
    $('#tabla').on('click', 'a.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        eliminar(data.id);
    });
    $('#tabla').on('click', 'a.modificar', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        window.location.href = base_url+"index.php/cotizaciones/modificar_cotizacion/" + data.id;
    });
    $('#tabla').on('click', 'button.modificar_aceptada', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        //modal_autoriza(data.id);
        $("#modal_pass").modal();
        $("#id_cotiza_auto").val(data.id);
    });
    $("#aceptar_pass").on("click",function(){
        modal_autoriza();
    }); 
}

function acciones(data, type, row, meta) {

    var btn = "<button class='btn btn-sm mb-0 btn-success imprimir'><i class='fa fa-print'></i></button>";
    btn += '<div class="btn-group ml-1 mb-0">\
            <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">'
    if (data != "2" && data != "3") {
        btn += '<button class="dropdown-item aceptar" type="button">Aceptar</button>\
                <button class="dropdown-item rechazar" type="button">Rechazar</button>';
    }else{
       btn += '<button class="dropdown-item modificar_aceptada" type="button">Solicitar Modificación</button>'; 
    }
    btn += '<a class="dropdown-item mail" href="#">Envíar por email</a>';
    if($("#perf").val()==1){
        btn += '<a class="dropdown-item delete" href="#">Eliminar</a>';
    }
    if (data != "2" && data != "3") {
        if(logeoEd){
            btn += '<div class="dropdown-divider"></div><a class="dropdown-item modificar" href="#">Modificar</a>';
        }
    }

    btn += '</div></div>';
    return btn;
}

function imprimir(id) {
    /*window.open(base_url+'index.php/Formatos/cotizacion/' + id,
            'imprimir',
            'width=700,height=800');*/
    window.open(base_url+'Cotizaciones/preimpresion/'+id);
}

function mail(id) {
    $.ajax({
            type: "POST",
            url: base_url+"index.php/Formatos/email_cotizacion/"+id,
            success: function (data) {
                //console.log(data);
                swal("", "Se ha enviado el correo de cotización", "success");
            }
        });
}

function eliminar(id){
    swal({
        title: "Eliminar",
        text: "¿Desea eliminar este elemento?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar"
    }).then(function(t){t &&
        $.ajax({
                 type: "POST",
                 url: base_url+'index.php/Cotizaciones/cambiarEstatus/',
                 data: {id:id, status: 0},
                 success: function (result) {
                    //console.log(result);
                    load();  
                    swal("Exito!", "Se ha eliminado correctamente", "success");
                 }
             });
    });
}

function forma_pago(data, type, row, meta) {
    var forma = "50% de anticipo y 50% contraentrega";
    switch (data) {
        case "2":
            forma = "Crédito 30 días";
            break;
        case "3":
            forma = "Crédito 60 días";
            break;
        case "4":
            forma = "100% contraentrega";
            break;
        case "5":
            forma = "En común acuerdo con el cliente";
            break;
    }

    return forma;
}

function estatus(data, type, row, meta) {
    var stat = "<span class='badge badge-secondary width-100'>Pendiente</span>";
    switch (data) {
        case "2":
            stat = "<span class='badge badge-success width-100'>Aceptada</span>";
            break;
        case "3":
            stat = "<span class='badge badge-danger width-100'>Rechazada</span>";
            break;
        case "4":
            stat = "<span class='badge badge-warning width-100'>Modificada</span>";
            break;
    }

    return stat;
}

function estatus_cotizacion(id, estatus) {
    var conf=0;
    if(estatus==2){
        swal({
            title: '¿Desea aceptar la cotización?',
            html:
                '<div class="row">\
                    <div class="col-md-12">\
                        <label>Persona(s) a quien(es) va dirigido el informe:</label>\
                    </div>\
                    <div class="col-md-12">\
                        <input id="swal-input1" type="text" class="swal2-input" required>\
                    </div>\
                    <div class="col-md-12">\
                        <label>Nombre y cargo a quien se dirigirán los técnicos:</label>\
                    </div>\
                    <div class="col-md-12">\
                        <input id="swal-input2" type="text" class="swal2-input" required>\
                    </div>\
                </div>',
              preConfirm: function () {
                return new Promise(function (resolve) {
                  resolve([
                    $('#swal-input1').val()
                  ])
                })
              },
            text: "¿Desea cambiar el estatus de la Orden?",
            type: 'info',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Aceptar",
            closeOnConfirm: false,
        }).then(function (isConfirm) {
            if (isConfirm && $("#swal-input1").val()!="") {
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/cotizaciones/cambiarEstatus",
                    data: {id: id, status: estatus, recibe: $("#swal-input1").val(), nom_tec: $("#swal-input2").val()},
                    beforeSend: function() {
                        $(".aceptar").attr('disabled',true); 
                        $(".swal2-confirm").attr('disabled',true); 
                    },
                    success: function (data) {
                        swal("", "Se cambió el estatus de la cotización", "success");
                        load();
                    }
                });
            }else{
                swal("", "Ingresa a quien va dirigido", "warning");
            }
        }).catch(swal.noop);
    }else{ //rechazar
        swal({
            title: '',
            text: "¿Desea cambiar el estatus de la Orden?",
            type: 'info',
            showCancelButton: true,
            allowOutsideClick: false,
        }).then(function (isConfirm) {
            conf++;
            if (isConfirm && conf==1) {
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/cotizaciones/cambiarEstatus",
                    data: {id: id, status: estatus},
                    beforeSend: function() {
                        $(".rechazar").attr('disabled',true); 
                    },
                    success: function (data) {
                        swal("", "Se cambió el estatus de la cotización", "success");
                        load();
                    }
                });
            }
        }).catch(swal.noop);
    }
    
}

function modal_autoriza(id){
    var id_mod = id;
    if($("#pass_insert").val()!=""){
        $.ajax({
            type: "POST",
            url: base_url+"index.php/Cotizaciones/autorizaModifica",
            data: {pass: $("#pass_insert").val(), id_cot: $("#id_cotiza_auto").val()},
            success: function (data) {
                var array = $.parseJSON(data);
                if(array.usada==0 && array.exist>0){
                    swal("Éxito", "Contraseña correcta", "success");
                     $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Cotizaciones/changeStatusPass2",
                        data: {id:array.id },
                        success: function (data2) {
                            $("#pass_insert").val("");
                        }
                    });
                    setTimeout(function () { history.back() 
                        window.location.href = base_url+"index.php/cotizaciones/modificar_cotizacion/"+$("#id_cotiza_auto").val()+"/"+1;
                    }, 1500);
                }else{
                    swal("Error", "Contraseña invalida", "warning");
                }
            }
        });
    }else{
        swal("Error", "Ingrese una contraseña", "warning");
    }
}
