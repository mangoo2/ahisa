var base_url=$("#base_url").val();
$(document).ready(function () {
    $('.pickadate').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar',
        format: "yyyy-mm-dd"
    });
    $('.timepicker').pickatime({
        format: "HH:i:00",
        formatLabel: "HH:i a ",
        min: [7, 0],
        max: [23, 30]
    });
    load();
    $("#Fecha1").on("change",function(){
        if($("#Fecha1").val()!="" && $("#Fecha2").val()!="")
            load();
    });
    $("#Fecha2").on("change",function(){
        if($("#Fecha1").val()!="" && $("#Fecha2").val()!="")
            load();
    });
    $("#tipostock").on("change",function(){
        load();
    });
    $("#empresa").on("change",function(){
        load();
    });
    $("#export").on("click",function(){
        estatus = $('#tipostock option:selected').val();
        fecha1= $('#Fecha1').val();
        fecha2= $('#Fecha2').val();
        empresa= $('#empresa').val();

        if(estatus=="")
            estatus=0;
        if(fecha1=="")
            fecha1=0;
        if(fecha2=="")
            fecha2=0;

        window.open(base_url+"index.php/Ordenes/ExportOrdenes/"+fecha1+"/"+fecha2+"/"+estatus+"/"+empresa, '_blank');
    });
    $("#unico_tecnico").on("change",function(){
        mostrarTec();
    });
});

function mostrarTec(){
    if($("#unico_tecnico").is(":checked")==true){
        $("#cont_servs_padre").hide("slow");
        $("#cont_tec_unico").show("slow");
    }else{
        $("#cont_servs_padre").show("slow");
        $("#cont_tec_unico").hide("slow");
    }
}
function load(){
    url = base_url+"index.php/Ordenes/getData_ordenes";
    var table = $('#tabla').DataTable({
        responsive: true,
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            url: url,
            data: { fechai:$("#Fecha1").val(), fechaf:$("#Fecha2").val(),status:$("#tipostock option:selected").val() },
            type: "post",
            error: function () {
                //$("#tabla").css("display", "none");
            }
        },
        "columns": [
            {"data": "id_orden"},
            {"data": "nomVendedor"},
            {"data": "cotizacion_id"},
            //{"data": "nombre"},
            {"data": "fecha_creacion"},
            {"data": "alias"},
            //{"data": "proveedor"},
            /*{"data": null,
                "render": function (data, type, row, meta) {
                   return verProveedor(row.cotizacion_id,row.id_empresa);
                }
            },*/
            {"data": "status",
                "render": function (data, type, row, meta) {
                    return estatus(data, row.fecha_inicio);
                }
            },
            {"data": "id_orden",
                "render": function (data, type, row, meta) {
                    return acciones(data,row.fecha_inicio,row.idnom,row.cliente_id,row.id_rec,row.tot_chs,row.cotizacion_id,row.id_chs,row.id_rec11,row.id_rec22,row.id_rec81,row.id_rec15);
                }
            }
        ],language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
    });
    $('#tabla').on('click', 'a.prog', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        //console.log("cotizacion_id: "+data.cotizacion_id);
        $("#servicio").val(data.cotizacion_id);
        $("#cliente_orden").val(data.cliente_id);
        $("#hora_inicio").val(data.hora_inicio);
        $("#hora_fin").val(data.hora_fin);
        if(data.fecha_inicio!="" && data.fecha_inicio!="0"){
            $("#fecha_inicio").val(data.fecha_inicio);
        }
        $("#fecha_fin").val(data.fecha_fin);
        //console.log("id_tec_gral: "+data.id_tec_gral);
        if(data.id_tecnico>0){
            $("#tecnico").val(data.id_tecnico);
            $("#lable_tec").html("Técnico Asignado:");
        }
        $("#comentarios").val(data.comentarios);
        $("#modal_program").modal("show");
        if(data.unico_tecnico=="0"){ //no es el unico
            $("#unico_tecnico").attr("checked",false);
        }else{
            $("#unico_tecnico").attr("checked",true);
            $("#tecnico").val(data.id_tec_gral);
            $("#lable_tec").html("Técnico Asignado:");
        }
        mostrarTec();
        if(data.tot_chs<=1){
            $("#tecnico").select2({
                width: '100%',
                minimumInputLength: 3,
                minimumResultsForSearch: 10,
                placeholder: 'Buscar un técnico',
                language: {
                    noResults: function() {
                      return "No hay resultado";        
                    },
                    searching: function() {
                      return "Buscando..";
                    }
                }
            });
        }
        else if(data.tot_chs>1){
            $.ajax({
                type: "POST",
                url: base_url+"Ordenes/getServsOrden",
                data: {id:data.cotizacion_id},
                success: function (data) {
                    $("#cont_servs").html(data)
                }
            });
        }
    });
    /*$('#tabla').on('click', 'a.print', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        imprimir(data.id_orden);
    });*/

    $('#tabla').on('click', 'a.env_mail', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        enviarEmail(data.cliente_id,data.cotizacion_id,data.hora_inicio,data.hora_fin,data.fecha_inicio,data.fecha_fin,data.comentarios);
    });
}
function acciones(data, fechai,idnom,idcli,id_rec,tot_chs,cotizacion_id,id_chs,id_rec11,id_rec22,id_rec81,id_rec15) {
    if($("#idus").val()=="1"){ //super admin
         var opcion = "";
         var opcion2 = "";
         var opcion3 = "";
         var opcion4 = "";
         var opcion5 = ""; var opcion6= "";
             opcion = '<a class="dropdown-item prog" >Programación</a>'; 
             opcion2 = '<a class="dropdown-item print" onclick="imprimir('+data+','+cotizacion_id+')">Generar Orden</a>';
             opcion3 = '<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cambiar a "Realizada"</a>'; //administracion no puede 
        if(fechai!="" && fechai!="0" && fechai!=null){
            opcion4 = '<a title="Envíar email a cliente de confirmación" class="dropdown-item env_mail" >Enviar Programación</a>'; 
            opcion5 = '<a class="dropdown-item" onclick="reconocimiento('+data+','+idnom+','+idcli+','+tot_chs+','+cotizacion_id+','+id_chs+')">Ir a reconocimiento</a>';  
            if(id_rec>0 || id_rec11>0 || id_rec22>0 || id_rec81>0 || id_rec15>0) {
                opcion6 = '<a class="dropdown-item" onclick="levantamiento('+data+','+idnom+','+tot_chs+','+cotizacion_id+','+id_chs+')">Ir a mediciones</a>';   
            }
        }
        var btn='<div class="btn-group ml-1 mb-0">\
            <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                '+opcion+'\
                '+opcion4+'\
                '+opcion2+'\
                '+opcion3+'\
                '+opcion5+'\
                '+opcion6+'\
            </div>\
        </div>';

    } 
    else if($("#idus").val()!="1"){
        var opcion = "";
        var opcion2 = "";
        var opcion3 = "";
        var opcion4 = "";  var opcion5 = ""; var opcion6 = ""; 
            opcion = '<a class="dropdown-item prog" >Programación</a>'; 
            opcion2 = '<a class="dropdown-item print" onclick="imprimir('+data+','+cotizacion_id+')">Generar Orden</a>';
            opcion3 = '<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cambiar a "Realizada"</a>'; //administracion no puede realizar esta accion, comentar despues de pruebas 
        if($("#perm_pe").val()==1) {
            if(fechai!="" && fechai!="0" && fechai!=null){
                opcion4 = '<a title="Envíar email a cliente de confirmación" class="dropdown-item env_mail" >Enviar Programación</a>';   
            }
        } 
        //if($("#perfil").val()=="5"){
            if(fechai!="" && fechai!="0" && fechai!=null){
                opcion5 = '<a class="dropdown-item" onclick="reconocimiento('+data+','+idnom+','+idcli+','+tot_chs+','+cotizacion_id+','+id_chs+')">Ir a reconocimiento</a>'; 
                if(id_rec>0 || id_rec11>0 || id_rec22>0 || id_rec81>0 || id_rec15>0) {
                    opcion6 = '<a class="dropdown-item" onclick="levantamiento('+data+','+idnom+','+tot_chs+','+cotizacion_id+','+id_chs+')">Ir a mediciones</a>';   
                }   
            }
        //}
        var btn='<div class="btn-group ml-1 mb-0">\
            <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                '+opcion+'\
                '+opcion4+'\
                '+opcion2+'\
                '+opcion3+'\
                '+opcion5+'\
                '+opcion6+'\
            </div>\
        </div>';
    } 
    else{
        var btn='<div class="btn-group ml-1 mb-0">\
                <button type="button" class="btn btn-sm btn-raised mb-0 btn-dark dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i></button>\
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                    <a class="dropdown-item" href="'+base_url+'Ordenes/orden/'+data+'">Datos de la Orden</a>\
                </div>\
            </div>';     
    } 
    //<a class="dropdown-item" onclick="cambiar_estatus('+data+',2)">Cancelar</a>\
    return btn;
}

function estatus(data, fechai){
    var str='<span class="badge badge-warning width-100">Pendiente</span>';
    if(data==1 && fechai!="" && fechai!="0" && fechai!=null && fechai!="0000:00:00"){
        str='<span class="badge badge-primary width-100">En Proceso</span>';
    }
    if(data==2){
        str='<span class="badge badge-success width-100">Realizado</span>';
    }
    if(data==3){
        str='<span class="badge badge-danger width-100">Cancelado</span>';
    }
    return str;
}

function cambiar_estatus(id,estatus){
    //$("#modal_program").modal("show");
    swal({
        title: '',
        text: "¿Desea cambiar el estatus de la Orden?",
        type: 'info',
        showCancelButton: true,
        allowOutsideClick: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: base_url+"index.php/ordenes/cambiarEstatus",
                data: {id: id, status: estatus},
                success: function (data) {
                    swal("", "Se cambió el estatus de la cotización", "success");
                    //table.ajax.reload();
                    load();
                }
            });
        }
    }).catch(swal.noop);
    
}

function programacion(){
    //console.log("servicio: "+$("#servicio").val());
    var datos={
        hora_inicio: $("#hora_inicio").val(),
        hora_fin: $("#hora_fin").val(),
        fecha_inicio: $("#fecha_inicio").val(),
        fecha_fin: $("#fecha_fin").val(),
        comentarios: $("#comentarios").val(),
        tecnico: $("#tecnico option:selected").val()
    };
    var unico_tecnico=0; //no
    if($("#unico_tecnico").is(":checked")==true){
        unico_tecnico=1; //si
        if($("#hora_inicio").val()!="" && $("#hora_fin").val()!="" && $("#fecha_inicio").val()!="" && $("#fecha_fin").val()!="" && $("#tecnico option:selected").val()!="" && $("#tecnico option:selected").val()!=null && $("#tecnico option:selected").val()!=undefined){
            $.ajax({
                type: "POST",
                url: base_url+"index.php/ordenes/programacion/"+$("#servicio").val()+"/"+$("#cliente_orden").val()+"/"+unico_tecnico,
                data: datos,
                success: function (data) {
                    swal("", "Se ha actualizado el evento", "success");
                    $("#modal_program").modal("hide");
                    //table.ajax.reload();
                    load();
                }
            });
        }else{
            swal("Álerta", "Ingrese los datos necesarios", "warning");
        }
    }else{
        if($("#hora_inicio").val()!="" && $("#hora_fin").val()!="" && $("#fecha_inicio").val()!="" && $("#fecha_fin").val()){
            unico_tecnico=0;
            var DATA  = [];
            var TABLA = $("#table_servs tbody > tr");                  
            TABLA.each(function(){        
                item = {};
                item ["unico_tecnico"] = unico_tecnico;
                item ["servicio"] = $("#servicio").val();
                item ["cliente_orden"] = $("#cliente_orden").val();
                item ["hora_inicio"] = $("#hora_inicio").val();
                item ["hora_fin"] = $("#hora_fin").val();
                item ["fecha_inicio"] = $("#fecha_inicio").val();
                item ["fecha_fin"] = $("#fecha_fin").val();
                item ["comentarios"] = $("#comentarios").val();
                item ["id_chs"] = $(this).find("input[id*='id_chs']").val();
                item ["id_tecnico"] = $(this).find("select[id*='tecnico_multiple'] option:selected").val();
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            /*console.log("INFO: "+INFO);
            console.log("aInfo: "+aInfo);
            console.log("DATA: "+DATA);*/
            $.ajax({
                type: "POST",
                url: base_url+"index.php/Ordenes/asignaTecnicos",
                data: INFO,
                processData: false, 
                contentType: false,
                async: false,
                success: function (data) {
                    swal("", "Se ha actualizado el evento", "success");
                    $("#modal_program").modal("hide");
                    //table.ajax.reload();
                    load();
                }
            });
        }else{
            swal("Álerta", "Ingrese los datos necesarios", "warning");
        }
    }
} 

function imprimir(id,id_cot) {
    /*window.open(base_url+'index.php/formatos/orden/'+id,
            'imprimir',
            'width=700,height=800');*/
    //window.open(base_url+'Ordenes/preimpresion/'+id+"/"+id_cot);
    window.location.href=(base_url+'Ordenes/preimpresion/'+id+"/"+id_cot);
}

function enviarEmail(id_cli,servicio,hora_inicio,hora_fin,fecha_inicio,fecha_fin,comentarios){
    var datos={
        hora_inicio: hora_inicio,
        hora_fin: hora_fin,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        comentarios: comentarios,
        servicio: servicio,
        id_cli: id_cli,
    };
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ordenes/avisarMailCli",
        data: datos,
        success: function (data) {
            //console.log("data: "+data);
            swal("Éxito", "Email de confirmación enviado correctamente", "success");;
        }
    });
} 

function reconocimiento(id,idnom,idcli,tot_chs,cotizacion_id,id_chs) {
    if(tot_chs==1){
        //window.open(base_url+'index.php/Nom/reconocimientoServicio/'+id+'/'+idnom+"/"+idcli+"/"+id_chs);
        window.location.href=(base_url+'index.php/Nom/reconocimientoServicio/'+id+'/'+idnom+"/"+idcli+"/"+id_chs);
    }else{
        $("#modal_serv_tec").modal();
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/getServsRecoTecnico",
            data: { cotizacion_id:cotizacion_id, id_orden:id, idnom:idnom,idcli:idcli },
            success: function (data) {
                $("#cont_serv_ir").html(data);
            }
        });
    }
}
function levantamiento(id,idnom,tot_chs,cotizacion_id,id_chs) {
    //console.log("idnom: "+idnom);
    if(tot_chs==1){
        //window.open(base_url+'index.php/Nom/lentamientoServicio/'+id+'/'+idnom+'/'+id_chs);
        window.location.href=(base_url+'index.php/Nom/lentamientoServicio/'+id+'/'+idnom+'/'+id_chs);
    }else{
        $("#modal_serv_tec").modal();
        $.ajax({
            type: "POST",
            url: base_url+"Ordenes/getServsTecnico",
            data: { cotizacion_id:cotizacion_id, id_orden:id, idnom:idnom },
            success: function (data) {
                $("#cont_serv_ir").html(data);
                //$("#iframe").html("<iframe src='"+base_url+"Barcode?id="+id+"&page=1'></iframe>");
            }
        });
    }
}