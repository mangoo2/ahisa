var base_url = $('#base_url').val();
var rowpunto=1;
$(document).ready(function($) {
    
});

let cont_pto=0;
function addpunto(){
    cont_pto++;
    $row=$("#pto_princi").clone().attr("id","pto_clone").addClass("pto_clone_"+cont_pto+"").addClass("pto_princi");
    $row.find("input").val("");  
    $row.find("button").removeClass("btn-info").addClass("btn-danger").attr("onclick","remove_punto($(this),"+cont_pto+")").html("<i class='fa fa-trash-o'></i> Eliminar");
    //console.log("cont_pto: "+cont_pto);
    $row.find("input[name*='tipo_evalua']").attr("name","tipo_evaluac_"+cont_pto+"");
    if(cont_pto==1){
        $row.insertAfter("#pto_princi");
    }
    else{
        cont_ter_ant = cont_pto-1;
        //console.log("cont_ter_ant: "+cont_ter_ant);
        $row.insertAfter(".pto_clone_"+cont_ter_ant+"");
    }
    $('html, body').animate({
        scrollTop: $(".pto_clone_"+cont_pto+"").offset().top
    }, 2000);
}

function remove_punto(i,cont){
    i.closest(".pto_clone_"+cont+"").remove();
    cont_pto--;
}

function saveDetalles(){
    var DATAr  = [];
    var TABLA   = $("#cont_ptos_gral #cont_ptos > .pto_princi");
        TABLA.each(function(){         
        item = {};
        item ["casco"]="0";
        item ["tapones"]="0";
        item ["botas"]="0";
        item ["lentes"]="0";
        item ["guantes"]="0";
        item ["peto"]="0";
        item ["respirador"]="0";

        item ["id"] = $(this).find("input[id*='id_pto']").val();
        item ["fecha"] = $(this).find("input[id*='fecha']").val();
        item ["id_termo"] = $(this).find("select[id*='id_termo'] option:selected").val();
        item ["id_termo_digi"] = $(this).find("select[id*='id_termo_digi'] option:selected").val();
        item ["id_anemo"] = $(this).find("select[id*='id_anemo'] option:selected").val();
        item ["area"] = $(this).find("input[id*='area']").val();
        item ["num_punto"] = $(this).find("input[id*='num_punto']").val();
        item ["ubicacion"] = $(this).find("input[id*='ubicacion']").val();
        item ["fte_generadora"] = $(this).find("input[id*='fte_generadora']").val();
        item ["controles_tec"] = $(this).find("input[id*='controles_tec']").val();
        item ["controles_adm"] = $(this).find("input[id*='controles_adm']").val();
        if($(this).find("input[id*='tipo_evalua']").is(":checked")==true){
           item ["tipo_evalua"] = 1;
        }else{
           item ["tipo_evalua"] = 2; 
        }
        item ["nom_trabaja"] = $(this).find("input[id*='nom_trabaja']").val();
        if($(this).find("input[id*='casco']").is(":checked")==true){
            item ["casco"]="1";
        }if($(this).find("input[id*='tapones']").is(":checked")==true){
            item ["tapones"]="1";
        }if($(this).find("input[id*='botas']").is(":checked")==true){
            item ["botas"]="1";
        }if($(this).find("input[id*='lentes']").is(":checked")==true){
            item ["lentes"]="1";
        }if($(this).find("input[id*='guantes']").is(":checked")==true){
            item ["guantes"]="1";
        }if($(this).find("input[id*='peto']").is(":checked")==true){
            item ["peto"]="1";
        }if($(this).find("input[id*='respirador']").is(":checked")==true){
            item ["respirador"]="1";
        }
        item ["otro"] = $(this).find("input[id*='otro']").val();
        item ["puesto"] = $(this).find("input[id*='puesto']").val();
        item ["tiempo_expo"] = $(this).find("input[id*='tiempo_expo']").val();
        item ["num_ciclos"] = $(this).find("input[id*='num_ciclos']").val();
        item ["cliclo"] = $(this).find("input[id*='cliclo']").val();
        item ["desc_activ"] = $(this).find("input[id*='desc_activ']").val();
        item ["observaciones"] = $(this).find("input[id*='observaciones']").val();
        DATAr.push(item);
    });
    arraypuntos   = JSON.stringify(DATAr);
    //console.log("arraypuntos: "+arraypuntos);
    /* ****************************** */
    var DATAFP  = [];
    var TABLAFP   = $("#ciclos_mediciones tbody"); //mandar toda la tabla como 1 solo arreglo
        TABLAFP.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_medicion']").val();
        item ["ciclo_med"] = $(this).find("input[id*='ciclo_med']").val();
        item ["temp_ini"] = $(this).find("input[id*='temp_ini']").val();
        item ["temp_fin"] = $(this).find("input[id*='temp_fin']").val();
        item ["altura_ini"] = $(this).find("input[id*='altura_ini']").val();
        item ["hora_ini"] = $(this).find("input[id*='hora_ini']").val();
        item ["tbs_ini"] = $(this).find("input[id*='tbs_ini']").val();
        item ["velocidad_ini"] = $(this).find("input[id*='velocidad_ini']").val();

        item ["altura_media"] = $(this).find("input[id*='altura_media']").val();
        item ["hora_media"] = $(this).find("input[id*='hora_media']").val();
        item ["tbs_media"] = $(this).find("input[id*='tbs_media']").val();
        item ["velocidad_media"] = $(this).find("input[id*='velocidad_media']").val();
        item ["altura_fin"] = $(this).find("input[id*='altura_fin']").val();
        item ["hora_fin"] = $(this).find("input[id*='hora_fin']").val();
        item ["tbs_fin"] = $(this).find("input[id*='tbs_fin']").val();
        item ["velocidad_fin"] = $(this).find("input[id*='velocidad_fin']").val();
        DATAFP.push(item);
    });
    mediciones   = JSON.stringify(DATAFP);
    //console.log("mediciones: "+mediciones);
    /* ************************************** */
    var datos = $('#form_nom').serialize()+'&arraypuntos='+arraypuntos+'&mediciones='+mediciones;
    if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Nom/inserupdate015",
            data: datos,
            success: function (response){
                swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                setTimeout(function () { 
                    window.location.href=base_url+"Nom"; 
                }, 2000);  
            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }else{
        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
    }
}

function deletepunto(id,row){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el punto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deletePunto015",
                    data: {
                        id:id, tabla:"nom15_punto"
                    },
                    success:function(response){  
                        $('.pto_princifor_'+row).remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}