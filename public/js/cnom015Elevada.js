var base_url = $('#base_url').val();
$(document).ready(function($) {
    
});

let cont_pto=0;
function addpunto(){
    cont_pto++;
    $row=$("#pto_princi").clone().attr("id","pto_clone").addClass("pto_clone_"+cont_pto+"").addClass("pto_princi");

    $row.removeClass(function(index, className) {
        return className.split(' ').filter(function(c) {
            return c.startsWith('pto_princifor_');
        }).join(' ');
    });

    //$row.find("input").val("");
    //if($row.find("input").data("reset")!="0"){
        $row.find("input").val("");
    //}
    $row.find("button").removeClass("btn-info").addClass("btn-danger").attr("onclick","remove_punto($(this),"+cont_pto+")").html("<i class='fa fa-trash-o'></i> Eliminar");
    //console.log("cont_pto: "+cont_pto);

    //al clonar buscar los tipos check y radio para cambiarles el name a +row
    $row.find("input[id*='carga_solar']").attr("name","carga_solarc_"+cont_pto+"");
    $row.find("input[id*='carga_solar']").attr("onchange","cambiaRadio(0,1,1,1)");
    $row.find("input[id*='carga_solar2']").attr("name","carga_solarc_"+cont_pto+"");
    $row.find("input[id*='carga_solar2']").attr("onchange","cambiaRadio(0,1,1,1)");

    $row.find("input[id*='tipo_evalua']").attr("name","tipo_evaluac_"+cont_pto+"");
    $row.find("input[id*='tipo_evalua']").attr("onchange","cambiaRadio(0,1,1,1)");
    $row.find("input[id*='tipo_evalua2']").attr("name","tipo_evaluac_"+cont_pto+"");
    $row.find("input[id*='tipo_evalua2']").attr("onchange","cambiaRadio(0,1,1,1)");

    $row.find("input[id*='tipo_mov']").attr("name","tipo_movc_"+cont_pto+"");
    $row.find("input[id*='tipo_mov']").attr("onchange","cambiaRadio(0,1,1,1)");
    $row.find("input[id*='tipo_mov2']").attr("name","tipo_movc_"+cont_pto+"");
    $row.find("input[id*='tipo_mov2']").attr("onchange","cambiaRadio(0,1,1,1)");

    if(cont_pto==1){
        $row.insertAfter("#pto_princi");
    }
    else{
        cont_ter_ant = cont_pto-1;
        //console.log("cont_ter_ant: "+cont_ter_ant);
        $row.insertAfter(".pto_clone_"+cont_ter_ant+"");
    }
    $('html, body').animate({
        scrollTop: $(".pto_clone_"+cont_pto+"").offset().top
    }, 2000);
}

function remove_punto(i,cont){
    i.closest(".pto_clone_"+cont+"").remove();
    cont_pto--;
}

function cambiaRadio(id,tipo,val,clone=0){     
    if(clone==0){             
        if(tipo==1 && val==1){ //carga_solar
            $("input[name*='carga_solar_"+id+"']").attr("checked",true);
            $("input[name*='carga_solar2_"+id+"']").attr("checked",false);
            //$(".carga_solar2_"+id+"").attr("checked",false);
        }else if(tipo==1 && val==2){
            /*$(".carga_solar_"+id+"").attr("checked",false);
            $(".carga_solar2_"+id+"").attr("checked",true);*/
            $("input[name*='carga_solar_"+id+"']").attr("checked",false);
            $("input[name*='carga_solar2_"+id+"']").attr("checked",true);
        }

        if(tipo==2 && val==1){ //tipo_evalua
            /*$(".tipo_evalua_"+id+"").attr("checked",true);
            $(".tipo_evalua2_"+id+"").attr("checked",false);*/
            $("input[name*='tipo_evalua_"+id+"']").attr("checked",true);
            $("input[name*='tipo_evalua2_"+id+"']").attr("checked",false);
        }else if(tipo==2 && val==2){
            /*$(".tipo_evalua_"+id+"").attr("checked",false);
            $(".tipo_evalua2_"+id+"").attr("checked",true);*/
            $("input[name*='tipo_evalua_"+id+"']").attr("checked",false);
            $("input[name*='tipo_evalua2_"+id+"']").attr("checked",true);
        }

        if(tipo==3 && val==1){ //tipo_mov
            /*$(".tipo_mov_"+id+"").attr("checked",true);
            $(".tipo_mov2_"+id+"").attr("checked",false);*/
            $("input[name*='tipo_mov_"+id+"']").attr("checked",true);
            $("input[name*='tipo_mov2_"+id+"']").attr("checked",false);
        }else if(tipo==3 && val==2){
            /*$(".tipo_mov_"+id+"").attr("checked",false);
            $(".tipo_mov2_"+id+"").attr("checked",true);*/
            $("input[name*='tipo_mov_"+id+"']").attr("checked",false);
            $("input[name*='tipo_mov2_"+id+"']").attr("checked",true);
        }
    }else{
        if(tipo==1 && val==1){ //carga_solar
            $("input[name*='carga_solarc_"+id+"']").attr("checked",true);
            $("input[name*='carga_solarc2_"+id+"']").attr("checked",false);
            //$(".carga_solar2_"+id+"").attr("checked",false);
        }else if(tipo==1 && val==2){
            /*$(".carga_solar_"+id+"").attr("checked",false);
            $(".carga_solar2_"+id+"").attr("checked",true);*/
            $("input[name*='carga_solarc_"+id+"']").attr("checked",false);
            $("input[name*='carga_solarc2_"+id+"']").attr("checked",true);
        }

        if(tipo==2 && val==1){ //tipo_evalua
            /*$(".tipo_evalua_"+id+"").attr("checked",true);
            $(".tipo_evalua2_"+id+"").attr("checked",false);*/
            $("input[name*='tipo_evaluac_"+id+"']").attr("checked",true);
            $("input[name*='tipo_evaluac2_"+id+"']").attr("checked",false);
        }else if(tipo==2 && val==2){
            /*$(".tipo_evalua_"+id+"").attr("checked",false);
            $(".tipo_evalua2_"+id+"").attr("checked",true);*/
            $("input[name*='tipo_evaluac_"+id+"']").attr("checked",false);
            $("input[name*='tipo_evaluac2_"+id+"']").attr("checked",true);
        }

        if(tipo==3 && val==1){ //tipo_mov
            /*$(".tipo_mov_"+id+"").attr("checked",true);
            $(".tipo_mov2_"+id+"").attr("checked",false);*/
            $("input[name*='tipo_movc_"+id+"']").attr("checked",true);
            $("input[name*='tipo_movc2_"+id+"']").attr("checked",false);
        }else if(tipo==3 && val==2){
            /*$(".tipo_mov_"+id+"").attr("checked",false);
            $(".tipo_mov2_"+id+"").attr("checked",true);*/
            $("input[name*='tipo_movc_"+id+"']").attr("checked",false);
            $("input[name*='tipo_movc2_"+id+"']").attr("checked",true);
        }
    }
}

function saveDetalles() {
	let pto_tabla = 0;
	let pto_tabla2 = 0;

	var DATAr = [];
	var TABLA = $("#cont_ptos_gral #cont_ptos > .pto_princi");
    //$("#cont_ptos_gral #cont_ptos > .pto_princi").addClass("bg-success");

	TABLA.each(function () {
		pto_tabla2++;

		item = {};
		item["casco"] = "0";
		item["tapones"] = "0";
		item["botas"] = "0";
		item["lentes"] = "0";
		item["guantes"] = "0";
		item["peto"] = "0";
		item["respirador"] = "0";

		item["id"] = $(this).find("input[id*='id_pto']").val();
		item["fecha"] = $(this).find("input[id*='fecha']").val();
		item["id_bulbo"] = $(this)
			.find("select[id*='id_bulbo'] option:selected")
			.val();
		item["id_bulbo_humedo"] = $(this)
			.find("select[id*='id_bulbo_humedo'] option:selected")
			.val();
		item["id_globo"] = $(this)
			.find("select[id*='id_globo'] option:selected")
			.val();
		item["id_termo"] = $(this)
			.find("select[id*='id_termo'] option:selected")
			.val();

		item["th_bulbo"] = $(this).find("input[id*='th_bulbo']").val();
		item["ts_bulbo"] = $(this).find("input[id*='ts_bulbo']").val();
		item["tg_bulbo"] = $(this).find("input[id*='tg_bulbo']").val();
		item["acepta_bulbo"] = $(this).find("input[id*='acepta_bulbo']").val();
		item["th_bulbo2"] = $(this).find("input[id*='th_bulbo2']").val();
		item["ts_bulbo2"] = $(this).find("input[id*='ts_bulbo2']").val();
		item["tg_bulbo2"] = $(this).find("input[id*='tg_bulbo2']").val();
		item["acepta_bulbo2"] = $(this).find("input[id*='acepta_bulbo2']").val();

		item["th_bulbo_hum"] = $(this).find("input[id*='th_bulbo_hum']").val();
		item["ts_bulbo_hum"] = $(this).find("input[id*='ts_bulbo_hum']").val();
		item["tg_bulbo_hum"] = $(this).find("input[id*='tg_bulbo_hum']").val();
		item["acepta_bulbo_hum"] = $(this)
			.find("input[id*='acepta_bulbo_hum']")
			.val();
		item["th_bulbo_hum2"] = $(this).find("input[id*='th_bulbo_hum2']").val();
		item["ts_bulbo_hum2"] = $(this).find("input[id*='ts_bulbo_hum2']").val();
		item["tg_bulbo_hum2"] = $(this).find("input[id*='tg_bulbo_hum2']").val();
		item["acepta_bulbo_hum2"] = $(this)
			.find("input[id*='acepta_bulbo_hum2']")
			.val();

		item["th_globo"] = $(this).find("input[id*='th_globo']").val();
		item["ts_globo"] = $(this).find("input[id*='ts_globo']").val();
		item["tg_globo"] = $(this).find("input[id*='tg_globo']").val();
		item["acepta_globo"] = $(this).find("input[id*='acepta_globo']").val();
		item["th_globo2"] = $(this).find("input[id*='th_globo2']").val();
		item["ts_globo2"] = $(this).find("input[id*='ts_globo2']").val();
		item["tg_globo2"] = $(this).find("input[id*='tg_globo2']").val();
		item["acepta_globo2"] = $(this).find("input[id*='acepta_globo2']").val();

		item["area"] = $(this).find("input[id*='area']").val();
		item["num_punto"] = $(this).find("input[id*='num_punto']").val();
		//console.log("carga solar checked: "+$(this).find("input[id*='carga_solar']").is(":checked"));
		//console.log("carga solar checked2: "+$(this).find("input[id*='carga_solar2']").is(":checked"));
		if ($(this).find("input[id*='carga_solar']").is(":checked") == true) {
			//verificar
			item["carga_solar"] = 1;
		} else if (
			$(this).find("input[id*='carga_solar2']").is(":checked") == true
		) {
			item["carga_solar"] = 2;
		}

		item["fte_generadora"] = $(this).find("input[id*='fte_generadora']").val();
		item["controles_tec"] = $(this).find("input[id*='controles_tec']").val();
		item["controles_adm"] = $(this).find("input[id*='controles_adm']").val();
		if ($(this).find("input[id*='tipo_evalua']").is(":checked") == true) {
			item["tipo_evalua"] = 1;
		} else if (
			$(this).find("input[id*='tipo_evalua2']").is(":checked") == true
		) {
			item["tipo_evalua"] = 2;
		}
		if ($(this).find("input[id*='tipo_mov']").is(":checked") == true) {
			item["tipo_mov"] = 1;
		} else if ($(this).find("input[id*='tipo_mov2']").is(":checked") == true) {
			item["tipo_mov"] = 2;
		}
		item["nom_trabaja"] = $(this).find("input[id*='nom_trabaja']").val();
		item["temp_aux_ini"] = $(this).find("input[id*='temp_aux_ini']").val();
		item["temp_aux_fin"] = $(this).find("input[id*='temp_aux_fin']").val();
		if ($(this).find("input[id*='casco']").is(":checked") == true) {
			item["casco"] = "1";
		}
		if ($(this).find("input[id*='tapones']").is(":checked") == true) {
			item["tapones"] = "1";
		}
		if ($(this).find("input[id*='botas']").is(":checked") == true) {
			item["botas"] = "1";
		}
		if ($(this).find("input[id*='lentes']").is(":checked") == true) {
			item["lentes"] = "1";
		}
		if ($(this).find("input[id*='guantes']").is(":checked") == true) {
			item["guantes"] = "1";
		}
		if ($(this).find("input[id*='peto']").is(":checked") == true) {
			item["peto"] = "1";
		}
		if ($(this).find("input[id*='respirador']").is(":checked") == true) {
			item["respirador"] = "1";
		}
		item["otro"] = $(this).find("input[id*='otro']").val();
		item["puesto"] = $(this).find("input[id*='puesto']").val();
		item["tiempo_expo"] = $(this).find("input[id*='tiempo_expo']").val();
		item["num_ciclos"] = $(this).find("input[id*='num_ciclos']").val();
		item["porc_expo"] = $(this).find("input[id*='porc_expo']").val();
		item["porc_no_expo"] = $(this).find("input[id*='porc_no_expo']").val();
		item["regimen"] = $(this).find("input[id*='regimen']").val();
		item["desc_activ"] = $(this).find("input[id*='desc_activ']").val();
		item["observaciones"] = $(this).find("input[id*='observaciones']").val();

		DATAr.push(item);

		arraypuntos = JSON.stringify(DATAr);
		//console.log("arraypuntos: " + arraypuntos);
	});
	/* ****************************** */
    
	//var datos = $('#form_nom').serialize()+'&arraypuntos='+arraypuntos+'&mediciones='+mediciones;
    var datos = $('#form_nom').serialize()+'&arraypuntos='+arraypuntos;
    //console.log("DATOS: "+ JSON.stringify(datos));

	if (TABLA.length > 0) {
		$.ajax({
			type: "POST",
			url: base_url + "Nom/inserupdate015Elevada",
			data: datos,
			async: false,
			success: function (response) {
				//swal("¡Agradecemos su colaboración!","Enviado correctamente","success");

				var res = $.parseJSON(response);
				var idsPto = JSON.parse(res.ids_pto);
				//console.log("guardado detalle de nom15E: "+res.id_pto);

				if (res.status == "ok") {
                    let id_nom = res.id_nom;
                    let id_pto = res.id_pto;
                    let band_exis = res.band_exis;

					for (let index = 0; index < pto_tabla2; index++) {
						id_pto = idsPto[index];

						//console.log("pto_tabla " + pto_tabla);
						pto_tabla++;
						//console.log("puntos_tabla: "+pto_tabla);


						var DATAFP = [];
                        var TABLAFP2 = TABLA.eq(index).find("#ciclos_mediciones tbody .datos");
                        
/*
                        if(index == 1){
                            TABLA.eq(index).find("#ciclos_mediciones tbody .datos").addClass("bg-danger");
                        }else{
                            TABLA.eq(index).find("#ciclos_mediciones tbody .datos").addClass("bg-warning");
                        }
*/
						
						let cont_tab = 0;
						let ciclo_med = "";
						let temp_ini = "";
						let temp_fin = "";

						TABLAFP2.each(function () {
							cont_tab++;
							if (cont_tab == 1) {
								ciclo_med = $(this).find("input[id*='ciclo_med']").val();
								temp_ini = $(this).find("input[id*='temp_ini']").val();
								temp_fin = $(this).find("input[id*='temp_fin']").val();
							}
						});
						//console.log("ciclo_med: "+ciclo_med);

						var TABLAFP = TABLA.eq(index).find("#ciclos_mediciones tbody .datos"); //mandar toda la tabla como 1 solo arreglo
						TABLAFP.each(function () {
							item = {};
							item["id"] = $(this).find("input[id*='id_medicion']").val();
							/*item ["ciclo_med"] = $(this).find("input[id*='ciclo_med']").val();
                            item ["temp_ini"] = $(this).find("input[id*='temp_ini']").val();
                            item ["temp_fin"] = $(this).find("input[id*='temp_fin']").val();*/
							item["ciclo_med"] = ciclo_med;
							item["temp_ini"] = temp_ini;
							item["temp_fin"] = temp_fin;

							item["tipo"] = $(this).find("input[id*='tipo']").data("tipo");
							item["tipo_med"] = $(this)
								.find("input[id*='tipo_med']")
								.data("tipo_med");
							item["altura_ini"] = $(this)
								.find("input[id*='altura_ini']")
								.val();

							item["hora_ini"] = $(this).find("input[id*='hora_ini']").val();
							item["tbh_ini"] = $(this).find("input[id*='tbh_ini']").val();
							item["tbs_ini"] = $(this).find("input[id*='tbs_ini']").val();
							item["tg_ini"] = $(this).find("input[id*='tg_ini']").val();

							DATAFP.push(item);
						});
						mediciones = JSON.stringify(DATAFP);
						
                        //console.log("idnom: " + id_nom );
                        //console.log("id_pto: " + id_pto);

                        //console.log("mediciones: "+mediciones);
                        
						/* ************************************** */

                        var datosM = "id_nom=" + id_nom + "&id_pto=" + id_pto + "&band_exis=" + band_exis + "&mediciones=" + mediciones;
                        //console.log("DATOSM: "+ JSON.stringify(datosM));

						$.ajax({
							type: "POST",
							url: base_url + "Nom/saveMediciones",
							data: datosM,
							async: false,
							success: function (response) {
								swal("¡Agradecemos su colaboración!","Enviado correctamente","success");

                                setTimeout(function () { 
                                    window.location.href=base_url+"Nom"; 
                                }, 2000);  
							},
							error: function (response) {
								swal("Error!","Se produjo un error, intente de nuevamente o contacte al administrador del sistema","error");
							},
						});
					}
				}
			},
		});

	} else {
		swal(
			"Error!",
			"Se produjo un error, intente de nuevamente o contacte al administrador del sistema",
			"error"
		);
	}
}

function deletepunto(id,row){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el punto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/deletePunto015",
                    data: {
                        id:id, tabla:"nom15_punto_elevada"
                    },
                    success:function(response){  
                        $('.pto_princifor_'+id).remove();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}