var base_url =$('#base_url').val();
$(document).ready(function($) {
    if($("#id").val()>0){
        $("#cont_carga").show("slow");
    	/*$("#inputFile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["jpg","jpeg","png","webp"],
            browseLabel: 'Seleccionar Certificado',
            uploadUrl: base_url+'Equipos/uploadImg',
            showDrag: true,
            dragTitle: 'Arrastra o selecciona certificado',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'png': '<i class="fa fa-file-photo-o text-warning"></i>',
                'webp': '<i class="fa fa-file-photo-o text-warning"></i>',
            },
            uploadExtraData: function (previewId, index) {
                var info = {
                        id:$('#id').val()
                    };
                return info;
            }
        }).on('fileselect', function(event, numFiles, label) {
            var btn_upload='<button tabindex="500" \
                            title="Upload selected files" \
                            class="btn btn-default btn-secondary fileinput-upload fileinput-upload-button"\
                            onclick="subirarchivo()"\
                            ><i class="glyphicon glyphicon-upload"></i>\
                            <span class="hidden-xs">Subir</span>\
                            </a>';
            $('.file-input').append(btn_upload); 
        }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
            //location.reload();
            //$('#inputFile').fileinput('refresh');
        })*/;

        $("#inputFile").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["jpg","jpeg","png","webp"],
            browseLabel: 'Seleccionar Certificado',
            showDrag: true,
            dragTitle: 'Arrastra o selecciona certificado',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'png': '<i class="fa fa-file-photo-o text-warning"></i>',
                'webp': '<i class="fa fa-file-photo-o text-warning"></i>',
            },
        });
        $(".file-drop-zone-title").html("Arrastra o selecciona certificado");

        $("#inputFile").on("filepredelete", function(jqXHR) {
            verificaCertificado();
        });  
    }

    $("#kv-file-remove").on("click", function(){
        verificaCertificado();
    });

    var tama_perm = false;
    $('#inputFile').change( function() {
        filezise = this.files[0].size;                     
        if(filezise > 2548000) { // 512000 bytes = 500 Kb
            //console.log($('.img')[0].files[0].size);
            $(this).val('');
            swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
        }else { //ok
            tama_perm = true; 
            var archivo = this.value;
            var name = this.id;

            extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
            extensiones_permitidas = new Array(".jpeg",".png",".jpg",".webp");
            permitida = false;
            if($('#'+name)[0].files.length > 0) {
                for (var i = 0; i < extensiones_permitidas.length; i++) {
                    if (extensiones_permitidas[i] == extension) {
                        permitida = true;
                    break;
                    }
                }  
                if(permitida==true && tama_perm==true){
                    //console.log("tamaño permitido");
                    var inputFileImage = document.getElementById(name);
                    name=document.getElementById('inputFile').files[0].name;
                    $("#name_cert").val(name);
                    var file = inputFileImage.files[0];
                    var data = new FormData();
                    //id_docs = $("#id").val();
                    data.append('foto',file);
                    data.append('id_equipo',$('#id').val());
                    $.ajax({
                      url:base_url+'Equipos/uploadImg',
                      type:'POST',
                      contentType:false,
                      data:data,
                      processData:false,
                      cache:false,
                      success: function(data) {
                        var array = $.parseJSON(data);
                        //console.log(array.ok);
                        if (array.ok=true) {
                          id_docs = array.id;
                          //console.log("id_docs: "+id_docs);
                          //swal("Éxito", "Imagen cargada Correctamente", "success");
                        }else{
                          //swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        if(array.id==0) {
                            //swal("Éxito", "Imagen cargada Correctamente", "success");
                        }
                        $("#acept_cert").show();
                      },
                      error: function(jqXHR, textStatus, errorThrown) {
                          var data = JSON.parse(jqXHR.responseText);
                      }
                    });
                }else if(permitida==false){
                    swal("Error!", "Tipo de archivo no permitido", "error");
                } 
            }else{
              swal("Error!", "No se a selecionado un archivo", "error");
            } 
        }
    });
    if($("#id").val()>0){
        verificaCertificado();
    }
    $('#acept_cert').click( function() {
        swal("¡Éxito!", "Cargado correctamente", "success");
        $("#acept_cert").hide();
        $('#inputFile').fileinput('reset');
        $('#inputFile').val('');
        verificaCertificado();
    });

    $("#tipo").on("change",function(){
        if($("#id").val()>0){
            infodatosequipo($("#id").val(),$("#tipo option:selected").val());
        }else if($("#id").val()==0 && $("#tipo option:selected").val()==8){
            adddetalleTermo();
        }else if($("#id").val()==0 && $("#tipo option:selected").val()==10){
            adddetalleAnemometro();
        }else if($("#id").val()==0 && $("#tipo option:selected").val()==11){
            adddetalleBulboSeco(1);
        }else if($("#id").val()==0 && $("#tipo option:selected").val()==12){
            adddetalleBulboSeco(2);
        }else if($("#id").val()==0 && $("#tipo option:selected").val()==13){
            adddetalleBulboSeco(3);
        }
        showDet();
    });
    showDet();

    $(".val_ch").on("change",function(){
        var line=$(this).data("line");
        var om=$(this).data("om");
        var ref=$("#ref"+om+"_"+line).val();
        var prue=$("#prueba"+om+"_"+line).val();
        //console.log("ref: "+ref);
        //console.log("prue: "+prue);
        if(Number(prue)!=0 && Number(ref)!=0){
            var error = Number(prue)-Number(ref);
            $("#absoluto"+om+"_"+line).val(parseFloat(error).toFixed(3));

            var fc = Number(ref)/Number(prue);
            $("#fc"+om+"_"+line).val(parseFloat(fc).toFixed(3));
        }
        //calculaIterPendMedidor();
    });

    $(".change_higro").on("change",function(){ //Higrotermoanemometro
        var lineval=$(this).data("lineval");
        var patron=$(".patron"+lineval).val();
        var calibracion=$(".calibracion"+lineval).val();
        //console.log("patron: "+patron);
        //console.log("calibracion: "+calibracion);
        if(Number(patron)!=0 && Number(calibracion)!=0){
            var instrumento = Number(patron)-Number(calibracion);
            $(".instrumento"+lineval).val(parseFloat(instrumento).toFixed(4));

            var correccion = Number(patron) / Number(calibracion);
            $(".correccion"+lineval).val(parseFloat(correccion).toFixed(4));

            var datos_res = []; 
            var TABLADR   = $("#higro_detalle tbody > tr");
            TABLADR.each(function(){  
                correccion= $(this).find("input[id*='correccion']").val();
                datos_res.push(Number(correccion));
            });
            //console.log("datos_res: "+datos_res);
            var prom = ArrayAvg(datos_res);
            //console.log("prom: "+prom);
            $(".prom_higro").html(parseFloat(prom).toFixed(4));
        }
    });
});

function ArrayAvg(myArray) {
    var i = 0, summ = 0, ArrayLen = myArray.length;
    while (i < ArrayLen) {
        summ = summ + myArray[i++];
    }
    return summ / ArrayLen;
}

function showDet(){
    if($("#tipo option:selected").val()==1){ //luxometro
        $("#cont_luxo").show("slow");
        $("#cont_sono").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==2){ //sonómetro
        $("#cont_sono").show("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==5){ //medidor de resistencia de tierras
        $("#cont_med_tierra").show("slow");
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
    }else if($("#tipo option:selected").val()==8){ //termometro liquido
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").show("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==10){ //anenometro
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").show("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==11){ //bulbo seco
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").show("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==12){ //bulbo humedo
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").show("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==13){ //globo
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").show("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }else if($("#tipo option:selected").val()==14){ //Higrotermoanemometro
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").show("slow");
        $("#cont_med_tierra").hide("slow");
    }else{ 
        $("#cont_sono").hide("slow");
        $("#cont_luxo").hide("slow");
        $("#cont_termo").hide("slow");
        $("#cont_anemo").hide("slow");
        $("#cont_bulbo_seco").hide("slow");
        $("#cont_bulbo_humedo").hide("slow");
        $("#cont_globo").hide("slow");
        $("#cont_higro").hide("slow");
        $("#cont_med_tierra").hide("slow");
    }
}

function verificaCertificado(){
    /*$.ajax({ // 
       type: "POST",
       url: base_url+"Equipos/verificaCert",
       data: { "id": $("#id").val() },
       async: false,
       success: function (result) {
          var data= JSON.parse(result);
          $.each(data, function (key, dato){
            if(dato.certificado!="0" || dato.certificado!=""){
                img = dato.certificado;
                $("#inputFile").fileinput({
                    showCaption: false,
                    showUpload: false,// quita el boton de upload
                    //rtl: true,
                    allowedFileExtensions: ["jpg","jpeg","png","webp"],
                    browseLabel: 'Seleccionar Certificado',
                    showDrag: true,
                    dragTitle: 'Arrastra o selecciona certificado',
                    maxFilePreviewSize: 5000,
                    elErrorContainer: '#kv-avatar-errors-1',
                    msgErrorClass: 'alert alert-block alert-danger',
                    defaultPreviewContent: '<img src="'+base_url+'uploads/imgEquipos/'+img+'" width="50px" alt="Sin Dato">',
                    layoutTemplates: {main2: '{preview} {remove} {browse}'},
                    allowedFileExtensions: ["jpg", "jpeg", "png","webp"],
                    initialPreview: [
                    // IMAGE RAW MARKUP
                    '<img src="'+base_url+'uploads/imgEquipos/'+img+'" class="kv-preview-data file-preview-image" >',
                    
                    ],
                    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                    initialPreviewConfig: [
                        {type: "image", url: base_url+"Equipos/file_delete/"+$('#id').val(), caption: img, key: 1},
                    ]
                });
            }
          });
       }
    });*/
    $.ajax({ // 
        type: "POST",
        url: base_url+"Equipos/verificaCert",
        data: { "id": $("#id").val() },
        async: false,
        success: function (result) {
            $("#cont_cert").html(result);       
        }
    });
}

function subirarchivo(){
    if($('#id').val()>0){
        $('#inputFile').fileinput('upload');
    }
}

function adddetalle(tipo){
	adddetalleinfo(tipo,'','','','','','');
}
var rowtr=1;
function adddetalleinfo(tipo,es_ini,es_fin,red,pro,inser,reso){
	if(tipo==0){
		var buttons='<button type="button" class="btn btn-sm btn-icon btn-danger white" onclick="deleterow('+rowtr+')"><i class="ft-trash-2"></i></button>';
	}else{
		var buttons='<button type="button" class="btn btn-sm btn-icon btn-danger white" onclick="deleterowi('+rowtr+',tipo)"><i class="ft-trash-2"></i></button>';
	}
	var html='<tr class="equipodetalle_'+rowtr+'">\
                <td>'+rowtr+'\
                	<input type="hidden" id="id" value="'+tipo+'">\
                </td>\
                <td>\
                	<input type="text" id="escala_ini" class="escala_ini escala_ini_'+rowtr+'" onchange="calcular('+rowtr+')" value="'+es_ini+'">\
                </td>\
                <td>\
                	<input type="text" id="escala_fin" class="escala_fin escala_fin_'+rowtr+'" onchange="calcular('+rowtr+')" value="'+es_fin+'">\
                </td>\
                <td>\
                	<input type="text" id="iluminancia_ref" class="iluminancia_ref iluminancia_ref_'+rowtr+'" onchange="calcular('+rowtr+')" value="'+red+'">\
                </td>\
                <td>\
                	<input type="text" id="iluminancia_pro" class="iluminancia_pro iluminancia_pro_'+rowtr+'" onchange="calcular('+rowtr+')" value="'+pro+'">\
                </td>\
                <td class="errorlx_'+rowtr+'"></td>\
                <td class="errorrelativo_'+rowtr+'"></td>\
                <td>\
                	<input type="text" id="insertidumbre" class="insertidumbre insertidumbre_'+rowtr+'" onchange="calcular('+rowtr+')" value="'+inser+'">\
                </td>\
                <td class="factorcorreccion_'+rowtr+'">-</td>\
                <td>\
                	<input type="text" id="resolucion" class="resolucion resolucion_'+rowtr+'"  value="'+reso+'">\
                </td>\
                <td>'+buttons+'</td>\
            </tr>';
    $('.equipos_detalle_class').append(html);
    rowtr++;
}
function deleterow(row){
	$('.equipodetalle_'+row).remove();
}
function calcular(row){
	//console.log(row);
	var escala_ini = $('.escala_ini_'+row).val();
 	var escala_fin = $('.escala_fin_'+row).val();
 	var iluminancia_ref = $('.iluminancia_ref_'+row).val();
 	var iluminancia_pro = $('.iluminancia_pro_'+row).val();
 	var insertidumbre = $('.insertidumbre_'+row).val();
 	if(escala_ini==''){
 		escala_ini=0;
 	}
	if(escala_fin==''){
		escala_fin=0;
	}
	if(iluminancia_ref==''){
		iluminancia_ref=0;
	}
	if(iluminancia_pro==''){
		iluminancia_pro=0;
	}
	if(insertidumbre==''){
		insertidumbre=0;
	}
	var errorlx =(parseFloat(iluminancia_pro)-parseFloat(iluminancia_ref)).toFixed(2);
	$('.errorlx_'+row).html(errorlx);
	
	var errorrelativo = ((parseFloat(iluminancia_pro)-parseFloat(iluminancia_ref))/parseFloat(iluminancia_ref)*100).toFixed(2);
	$('.errorrelativo_'+row).html(errorrelativo);
	
	var factorcorreccion = (parseFloat(iluminancia_ref)/parseFloat(iluminancia_pro)).toFixed(4);
	$('.factorcorreccion_'+row).html(factorcorreccion);
	
}
function saveequipo(){
    var valid = 0;
    if($("#luxometro").val()=="" || $("#equipo").val()=="" || $("#marca").val()=="" || $("#modelo").val()==""){
        valid++;
    }
    if(valid==0){
        if($("#tipo option:selected").val()==1){//luxomentro
            var DATAr  = [];
            var TABLA   = $("#equipos_detalle tbody > tr");
                TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["escala_ini"] = $(this).find("input[id*='escala_ini']").val();
                item ["escala_fin"]   = $(this).find("input[id*='escala_fin']").val();
                item ["iluminancia_ref"]  = $(this).find("input[id*='iluminancia_ref']").val();
                item ["iluminancia_pro"]  = $(this).find("input[id*='iluminancia_pro']").val();
                item ["insertidumbre"]  = $(this).find("input[id*='insertidumbre']").val();
                item ["resolucion"]  = $(this).find("input[id*='resolucion']").val();
                DATAr.push(item);
            });
            arrayequipos   = JSON.stringify(DATAr);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserupdate",
                    data: datos,
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");

                        setTimeout(function () { 
                            
                                window.location.href=base_url+"Equipos"; 
                        }, 2000);
                        
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
            }
        }
        else if($("#tipo option:selected").val()==5){//medidor de resistencias
            var DATAr  = [];
            var TABLA   = $("#medidor_detalle tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["unidad"] = $(this).find("input[id*='unidad']").val();
                item ["line"] = $(this).find("input[id*='line']").val();
                item ["referencia"]  = $(this).find("input[class*='referencia']").val();
                item ["prueba"]  = $(this).find("input[class*='prueba']").val();
                item ["absoluto"]  = $(this).find("input[class*='absoluto']").val();
                item ["incerti"]  = $(this).find("input[class*='incerti']").val();
                item ["valfc"]  = $(this).find("input[class*='valfc']").val();
                //if($(this).find("input[class*='referencia']").val()!="" && $(this).find("input[class*='prueba']").val()!=""){
                    DATAr.push(item);
                //}
            });
            arrayequipos   = JSON.stringify(DATAr);
            //console.log("arrayequipos: "+arrayequipos);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            //console.log("datos: "+datos);
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserUpdateTermo",
                    data: datos+"&tipo_equipo=5",
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                        setTimeout(function () { 
                            window.location.href=base_url+"Equipos"; 
                        }, 2000);
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
            }
        }
        else if($("#tipo option:selected").val()==8){//temometro liquido
            var DATAr  = [];
            var TABLA   = $("#termo_detalle tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["temp_patron"] = $(this).find("input[id*='temp_patron']").val();
                item ["temp_ibc"]   = $(this).find("input[id*='temp_ibc']").val();
                item ["error"]  = $(this).find("input[id*='error']").val();
                item ["dato_u"]  = $(this).find("input[id*='dato_u']").val();
                item ["fc"]  = $(this).find("input[id*='fc']").val();
                //item ["id_equipo"]  = $(this).find("input[id*='id_equipo']").val();
                DATAr.push(item);
            });
            arrayequipos   = JSON.stringify(DATAr);
            //console.log("arrayequipos: "+arrayequipos);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserUpdateTermo",
                    data: datos+"&tipo_equipo=8",
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                        setTimeout(function () { 
                            window.location.href=base_url+"Equipos"; 
                        }, 2000);
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
            }
        }
        else if($("#tipo option:selected").val()==10){ //anemometro
            var DATAr  = [];
            var TABLA   = $("#anemo_detalle tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["vel_patron"] = $(this).find("input[id*='vel_patron']").val();
                item ["vel_ibc"]   = $(this).find("input[id*='vel_ibc']").val();
                item ["error"]  = $(this).find("input[id*='error_anemo']").val();
                item ["dato_u"]  = $(this).find("input[id*='dato_u']").val();
                item ["fc"]  = $(this).find("input[id*='fc']").val();
                DATAr.push(item);
            });
            arrayequipos   = JSON.stringify(DATAr);
            //console.log("arrayequipos: "+arrayequipos);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserUpdateTermo",
                    data: datos+"&tipo_equipo=10",
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                        setTimeout(function () { 
                            window.location.href=base_url+"Equipos"; 
                        }, 2000);
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
            }
        }
        else if($("#tipo option:selected").val()==11 || $("#tipo option:selected").val()==12 || $("#tipo option:selected").val()==13){ //bulbo seco, humedo, globo
            var DATAr  = [];
            if($("#tipo option:selected").val()==11)
                var TABLA   = $("#bulbo_seco_detalle tbody > tr");
            else if($("#tipo option:selected").val()==12)
                var TABLA   = $("#bulbo_humedo_detalle tbody > tr");
            else if($("#tipo option:selected").val()==13)
                var TABLA   = $("#globo_detalle tbody > tr");

            TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["patron"] = $(this).find("input[id*='patron']").val();
                item ["libc"]   = $(this).find("input[id*='libc']").val();
                item ["error"]  = $(this).find("input[id*='error']").val();
                item ["incertidumbre"]  = $(this).find("input[id*='incertidumbre']").val();
                item ["tipo"]  = $(this).find("input[id*='tipo']").val();
                DATAr.push(item);
            });
            arrayequipos   = JSON.stringify(DATAr);
            //console.log("arrayequipos: "+arrayequipos);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserUpdateTermo",
                    data: datos+"&tipo_equipo="+$("#tipo option:selected").val(),
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                        setTimeout(function () { 
                            window.location.href=base_url+"Equipos"; 
                        }, 2000);
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Ingrese los datos completos e intente nuevamente", "error")
            }
        }
        else if($("#tipo option:selected").val()==14){ //Higrotermoanemometro
            var DATAr  = [];
            var TABLA   = $("#higro_detalle tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id"] = $(this).find("input[id*='id']").val();
                item ["line"] = $(this).find("input[id*='line']").val();
                item ["patron"] = $(this).find("input[id*='patron']").val();
                item ["calibracion"]  = $(this).find("input[class*='calibracion']").val();
                item ["instrumento"]  = $(this).find("input[class*='instrumento']").val();
                item ["masmenosue"]  = $(this).find("input[class*='masmenosue']").val();
                item ["correccion"]  = $(this).find("input[class*='correccion']").val();
                if($(this).find("input[class*='patron']").val()!="" && $(this).find("input[class*='calibracion']").val()!=""){
                    DATAr.push(item);
                }
            });
            arrayequipos   = JSON.stringify(DATAr);
            //console.log("arrayequipos: "+arrayequipos);
            var datos = $('#form_equipo').serialize()+'&arrayequipos='+arrayequipos;
            //console.log("datos: "+datos);
            if(TABLA.length>0){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/inserUpdateTermo",
                    data: datos+"&tipo_equipo=14",
                    beforeSend: function(){
                        $("#buttonsave").attr("disabled",true);
                    },
                    success: function (response){
                        swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                        setTimeout(function () { 
                            window.location.href=base_url+"Equipos"; 
                        }, 2000);
                    },
                    error: function(response){
                        swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                    }
                });
            }else{
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
            }
        }
        else{ //Sonómetro
        //else if($("#tipo option:selected").val()==2){ //Sonómetro
            var num_calibra=$("#num_calibra").val();
            var fecha_calibra=$("#fecha_calibra").val();
            var datos = $('#form_equipo').serialize()+'&num_calibra='+num_calibra+'&fecha_calibra='+fecha_calibra;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Equipos/inserupdateSonometro",
                data: datos,
                beforeSend: function(){
                    $("#buttonsave").attr("disabled",true);
                },
                success: function (response){
                    swal("¡Agradecemos su colaboración!", "Enviado correctamente", "success");
                    setTimeout(function () { 
                        window.location.href=base_url+"Equipos"; 
                    }, 2000);
                    
                },
                error: function(response){
                    swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
                }
            });
        }
    }else{
        swal("Error!", "Ingrese los datos correspondientes", "error");
    }
}


/* *****************TERMOMETRO**************************** */
function adddetalleTermo(){
    for (var i=0; i<8; i++) {
        detalleTermometro(0,'','','','','',0);
    }
}
var row_term=1;
function detalleTermometro(id,temp_patron,temp_ibc,error,u,fc,id_equipo){
    var html='<tr class="detallesonometro'+row_term+'">\
                <td>'+row_term+'\
                    <input type="hidden" id="id" value="'+id+'"><input type="hidden" id="id_equipo" value="'+id_equipo+'">\
                </td>\
                <td>\
                    <input type="number" id="temp_patron" class="temp_patron temp_patron_'+row_term+'" onchange="calcular_termo('+row_term+')" value="'+temp_patron+'">\
                </td>\
                <td>\
                    <input type="number" id="temp_ibc" class="temp_ibc temp_ibc_'+row_term+'" onchange="calcular_termo('+row_term+')" value="'+temp_ibc+'">\
                </td>\
                <td>\
                    <input type="number" id="error" class="error error_'+row_term+'" value="'+error+'">\
                </td>\
                <td>\
                    <input type="number" id="dato_u" class="u u_'+row_term+'" value="'+u+'">\
                </td>\
                <td>\
                    <input type="number" id="fc" class="fc fc_'+row_term+'" value="'+fc+'">\
                </td>\
            </tr>';
    $('.termo_detalle_class').append(html);
    row_term++;
}

function calcular_termo(row){
    //console.log(row);
    var temp_patron = $('.temp_patron_'+row).val();
    var temp_ibc = $('.temp_ibc_'+row).val();

    var error =(parseFloat(temp_ibc)-parseFloat(temp_patron)).toFixed(2);
    $('.error_'+row).val(error);
    //console.log("error: "+error);

    var fc = (parseFloat(temp_ibc)/parseFloat(temp_patron)).toFixed(4);
    $('.fc_'+row).val(fc);
    //console.log("fc: "+fc);
}
/* ************************ANEMOMETRO**************************************/
function adddetalleAnemometro(){
    for (var i=0; i<5; i++) {
        detalleAnemometro(0,'','','','','',0);
    }
}
var row_anemo=1;
function detalleAnemometro(id,vel_patron,vel_ibc,error,u,fc,id_equipo){
    var html='<tr class="detallesonometro'+row_anemo+'">\
                <td>'+row_anemo+'\
                    <input type="hidden" id="id" value="'+id+'"><input type="hidden" id="id_equipo" value="'+id_equipo+'">\
                </td>\
                <td>\
                    <input type="text" id="vel_patron" class="vel_patron vel_patron_'+row_anemo+'" onchange="calcular_anemo('+row_anemo+')" value="'+vel_patron+'">\
                </td>\
                <td>\
                    <input type="text" id="vel_ibc" class="vel_ibc vel_ibc_'+row_anemo+'" onchange="calcular_anemo('+row_anemo+')" value="'+vel_ibc+'">\
                </td>\
                <td>\
                    <input type="text" id="error_anemo" class="error_anemo error_anemo_'+row_anemo+'" value="'+error+'">\
                </td>\
                <td>\
                    <input type="text" id="dato_u" class="u u_'+row_anemo+'" value="'+u+'">\
                </td>\
                <td>\
                    <input type="text" id="fc" class="fc fc_anemo_'+row_anemo+'" value="'+fc+'">\
                </td>\
            </tr>';
    $('.anemo_detalle_class').append(html);
    row_anemo++;
}
function calcular_anemo(row){
    //console.log(row);
    var vel_patron = $('.vel_patron_'+row).val();
    var vel_ibc = $('.vel_ibc_'+row).val();

    var error =(parseFloat(vel_ibc)-parseFloat(vel_patron)).toFixed(2);
    $('.error_anemo_'+row).val(error);
    //console.log("error: "+error);

    var fc = (parseFloat(vel_ibc)/parseFloat(vel_patron)).toFixed(4);
    $('.fc_anemo_'+row).val(fc);
    //console.log("fc: "+fc);
}

/* ************************BULBO SECO**************************************/
function adddetalleBulboSeco(tipo){
    
    for (var i=0; i<5; i++) {
        detalleBulboSeco(0,'','','','',0,tipo);
    }
}
var row_bulbos=1;
function detalleBulboSeco(id,patron,libc,error,incertidumbre,id_equipo,tipo){
    //console.log("tipo: "+tipo);
    var html='<tr class="detallesbulbo'+row_bulbos+'">\
                <td>'+row_bulbos+'\
                    <input type="hidden" id="id" value="'+id+'"><input type="hidden" id="id_equipo" value="'+id_equipo+'">\
                    <input type="hidden" id="id" value="'+id+'"><input type="hidden" id="tipo" value="'+tipo+'">\
                </td>\
                <td>\
                    <input type="text" id="patron" class="patron patron_'+row_bulbos+'_'+tipo+'" onchange="calcular_bulbo('+row_bulbos+','+tipo+')" value="'+patron+'">\
                </td>\
                <td>\
                    <input type="text" id="libc" class="libc libc_'+row_bulbos+'_'+tipo+'" onchange="calcular_bulbo('+row_bulbos+','+tipo+')" value="'+libc+'">\
                </td>\
                <td>\
                    <input type="text" id="error" class="error error_'+row_bulbos+'_'+tipo+'" value="'+error+'">\
                </td>\
                <td>\
                    <input type="text" id="incertidumbre" class="incertidumbre incertidumbre_'+row_bulbos+'" value="'+incertidumbre+'">\
                </td>\
            </tr>';
    if(tipo==1)
        $('.bulbos_detalle_class').append(html);
    else if(tipo==2)
        $('.bulboh_detalle_class').append(html);
    else if(tipo==3)
        $('.globo_detalle_class').append(html);

    row_bulbos++;
}
function calcular_bulbo(row,tipo){
    //console.log(row);
    var patron = $('.patron_'+row+'_'+tipo).val();
    var libc = $('.libc_'+row+'_'+tipo).val();

    var error =(parseFloat(libc)-parseFloat(patron)).toFixed(2);
    $('.error_'+row+'_'+tipo).val(error);
    //console.log("error: "+error);
}
/**************************************************************/

function infodatosequipo(id,tipo){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Equipos/infodatosequipo",
        data: {
        	idequipos:id,tipo:tipo
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log("array: "+array);
            /*$('#luxometro').val(array.luxometro);
            $('#equipo').val(array.equipo);
            $('#marca').val(array.marca);
            $('#modelo').val(array.modelo);
            $('#no_informe_calibracion').val(array.no_informe_calibracion);
            $('#fecha_calibracion').val(array.fecha_calibracion);*/
            //console.log("tipo: "+array.tipo);
            if(array.tipo==1){ //luxometro
                $("#cont_luxo").show("slow");
                $("#cont_sono").hide("slow");
                $("#cont_termo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                $('.cal_interseccion').html(array.intercept.toFixed(6));
                $('.cal_pendiente').html(array.slope.toFixed(6));
                
                $.each(array.equiposdetalle, function(index, item) {
                	adddetalleinfo(item.id,item.escala_ini,item.escala_fin,item.iluminancia_ref,item.iluminancia_pro,item.insertidumbre,item.resolucion);
                });
                setTimeout(function(){ 
    				calcularrow();
    			}, 1000);
            }else if(array.tipo==2){
                $("#cont_sono").show("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_termo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                $.each(array.equiposdetalle, function(index, item) {
                    $("#num_calibra").val(item.num_calibra);
                    $("#fecha_calibra").val(item.fecha_calibra);
                });
            }else if(array.tipo==5){
                $("#cont_med_tierra").show("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_termo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").show("slow");
                $.each(array.equiposdetalle, function(index, item) {
                    var unidad=item.unidad;
                    var line=item.line;
                    //console.log("unidad: "+unidad);
                    //console.log("line: "+line);
                    $(".id"+line+"_"+unidad).val(item.id);
                    $("#ref"+unidad+"_"+line).val(item.referencia);
                    $("#prueba"+unidad+"_"+line).val(item.prueba);
                    $("#absoluto"+unidad+"_"+line).val(item.absoluto);
                    $("#insert"+unidad+"_"+line).val(item.incerti);
                    $("#fc"+unidad+"_"+line).val(item.valfc);
                    $("#inter"+unidad).val(parseFloat(item.interseccion).toFixed(4));
                    $("#pend"+unidad).val(parseFloat(item.pendiente).toFixed(4));
                });
            }else if(array.tipo==8){ //termometro liquido
                $("#cont_termo").show("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                //console.log("intercept: "+array.intercept);
                //console.log("slope: "+array.slope);
                $('.termo_interseccion').html(array.intercept.toFixed(6)); //verificar, no muestra dato en form
                $('.termo_pendiente').html(array.slope.toFixed(6));
                $.each(array.equiposdetalle, function(index, i) {
                    //console.log("u: "+i.u);
                    detalleTermometro(i.id,i.temp_patron,i.temp_ibc,i.error,i.u,i.fc,i.id_equipo);
                });
            }else if(array.tipo==10){ //anenometro
                $("#cont_termo").hide("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_anemo").show("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                //console.log("intercept: "+array.intercept);
                //console.log("slope: "+array.slope);
                $('.anemo_interseccion').html(array.intercept.toFixed(6)); //verificar, no muestra dato en form
                $('.anemo_pendiente').html(array.slope.toFixed(6));
                $.each(array.equiposdetalle, function(index, i) {
                    //console.log("u: "+i.u);
                    detalleAnemometro(i.id,i.vel_patron,i.vel_ibc,i.error,i.u,i.fc,i.id_equipo);
                });
            }else if(array.tipo==11 || array.tipo==12 || array.tipo==13){ //bulbo seco, humedo, globo
                $('.bulbos_detalle_class').html('');
                $('.bulboh_detalle_class').html('');
                $('.globo_detalle_class').html('');
                row_bulbos=1;
                $("#cont_termo").hide("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                if(array.tipo==11){
                    $("#cont_bulbo_seco").show("slow");
                    $("#cont_bulbo_humedo").hide("slow");
                    $("#cont_globo").hide("slow");
                    $('.bulbos_interseccion').html(array.intercept.toFixed(6)); 
                    $('.bulbos_pendiente').html(array.slope.toFixed(6));
                }else if(array.tipo==12){
                    $("#cont_bulbo_seco").hide("slow");
                    $("#cont_bulbo_humedo").show("slow");
                    $("#cont_globo").hide("slow");
                    $('.bulboh_interseccion').html(array.intercept.toFixed(6)); 
                    $('.bulboh_pendiente').html(array.slope.toFixed(6));
                }else if(array.tipo==13){
                    $("#cont_bulbo_seco").hide("slow");
                    $("#cont_bulbo_humedo").hide("slow");
                    $("#cont_globo").show("slow");
                    $('.globo_interseccion').html(array.intercept.toFixed(6)); 
                    $('.globo_pendiente').html(array.slope.toFixed(6));
                }
                
                $.each(array.equiposdetalle, function(index, i) {
                    //console.log("u: "+i.u);
                    detalleBulboSeco(i.id,i.patron,i.libc,i.error,i.incertidumbre,i.id_equipo,i.tipo);
                });
            }
            else if(array.tipo==14){
                $("#cont_higro").show("slow");
                $("#cont_med_tierra").hide("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_termo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
                var datos_res = []; 
                $.each(array.equiposdetalle, function(index, item) {
                    var line=item.line;
                    //console.log("line: "+line);
                    $(".id"+line).val(item.id);
                    $(".patron"+line).val(item.patron);
                    $(".calibracion"+line).val(item.calibracion);
                    $(".instrumento"+line).val(item.instrumento);
                    $(".masmenosue"+line).val(item.masmenosue);
                    $(".correccion"+line).val(item.correccion);
                    
                    datos_res.push(Number(item.correccion));
                    //console.log("datos_res: "+datos_res);
                    if(item.line==4){
                        var prom = ArrayAvg(datos_res);
                        //console.log("prom: "+prom);
                        $(".prom_higro").html(parseFloat(prom).toFixed(4));
                    }
                });
            }
            else{
                $("#cont_higro").hide("slow");
                $("#cont_med_tierra").hide("slow");
                $("#cont_sono").hide("slow");
                $("#cont_luxo").hide("slow");
                $("#cont_termo").hide("slow");
                $("#cont_anemo").hide("slow");
                $("#cont_bulbo_seco").hide("slow");
                $("#cont_bulbo_humedo").hide("slow");
                $("#cont_globo").hide("slow");
            }
            
        },
        error: function(response){
            swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
        }
    });
}
var rowtri=1;
function calcularrow(){
	var TABLA   = $("#equipos_detalle tbody > tr");
    TABLA.each(function(){         
        calcular(rowtri)
        rowtri++;
    });
}

/* ********************MEDIDOR DE RESISTENCIAS DE TIERRA****************************** */
function calculaIterPendMedidor(){

}
