var base_url=$("#base_url").val();
$(document).ready(function () {
     $('.select2').select2();
    
    $("#slc_tamaño").on("change",function(){
        buscar_familias();
    });
    
    $("#slc_familia").on("change",function(){
        buscar_servicios($("#slc_familia option:selected").data("emp"));
    });
    
    $("#table_servicios").on("change",".cant",function(){
        console.log("Cambio Cantidad");
        cambia_cantidad($(this),$(this).val());
    });
    
    $("#cliente").on("change",function(){
        cambia_contacto();
    });

    $("#descuento").on("change",function(){
        valida_descuentos();
    });

    if($("#cliente").val()!=""){
        cambia_contacto();
    }
    
    buscar_familias();
  
    if($("#id").val()>0){
        //load_list();
        valida_descuentos();
    }
    else{
        //clear_list();
    }
   
    $("#ingresar_pass").on("click",function(){
        $("#modal_pass").modal();
    });
    $("#aceptar_pass").on("click",function(){
        validaPassDesc();
    }); 
});

    function validaPassDesc(){
        if($("#pass_insert").val()!=""){
            $.ajax({
                type: "POST",
                url: base_url+"index.php/Cotizaciones/validaPass",
                data: {pass: $("#pass_insert").val()},
                success: function (data) {
                    var array = $.parseJSON(data);
                    if(array.usada==0 && array.exist>0){
                        swal("Éxito", "Contraseña correcta, descuento activado", "success");
                        $("#modal_pass").modal("hide");
                        $("#descuento").attr("disabled",false);
                        $("#aux_desc").val(1);
                        $("#id_pass").val(array.id);
                    }else{
                        swal("Error", "Contraseña invalida", "warning");
                    }
                }
            });
        }else{
            swal("Error", "Ingrese una contraseña", "warning");
        }
    }
    
    function cambia_contacto(){
        //console.log("contacto: "+$("#contacto").val());
        $.ajax({
                type: "POST",
                url: base_url+"index.php/cotizaciones/searchContacto",
                data: {cliente: $("#cliente").val(), contacto: $("#contacto").val()},
                //async: false,
                //dataType: "JSON",
                success: function (data) {
                    $("#contacto").empty();
                    $("#contacto").append(data);
                }
            });
    }
       
    function buscar_familias(){
        $.ajax({
            type: "POST",
            url: base_url+"index.php/cotizaciones/searchFamilias",
            data: {tamaño: $("#slc_tamaño").val()},
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#slc_familia").empty();
                $("#slc_familia").append(data);
                var id_emp = $("#slc_familia option:selected").data("emp");
                buscar_servicios(id_emp);
            }
        });
    }
    
    function buscar_servicios(id_emp){
        //console.log("id_emp: "+id_emp);
        $.ajax({
            type: "POST",
            url: base_url+"index.php/cotizaciones/searchServicios",
            data: {familia: $("#slc_familia").val(),tamaño: $("#slc_tamaño").val(), id_emp:id_emp},
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                $("#slc_servicio").empty();
                $("#slc_servicio").append(data);
                $("#slc_servicio").select2();
            }
        });
    }
    
    function save_cotizacion(btn){
        btn.attr("disabled",true);
        var id = $("#id").val();
        //console.log("id cotizacion: "+id);
        //console.log("descuento:" +$("#descuento").val());

        $.ajax({
            type: "POST",
            url: base_url+"index.php/cotizaciones/saveCotizacion",
            data: {subtotal:$("#tot").val(), cliente: $("#cliente").val(),contacto: $("#contacto").val(),forma_pago: $('#forma_pago').val(), otro: "",descuento: $("#descuento").val(),desc_aplicado: $("#desc_aplicado").val(),observaciones: $('#observaciones').val(), id: id, aux_auto: $('#aux_auto').val(), id_empresa: $("#id_empresa").val() },
            //async: false,
            //dataType: "JSON",
            success: function (data) {
                var id_cotizacion = data;
                if($("#aux_desc").val()!="0"){
                    cambiaEstatusPass(data);
                }
                var DATA  = [];
                var TABLA = $("#tabla_servs tbody > tr");                  
                TABLA.each(function(){        
                    item = {};
                    item ["id_cot_serv"] = $(this).find("input[id*='id_cot_serv']").val();
                    item ["servicio_id"] = $(this).find("input[id*='serv_id']").val();
                    item ["cantidad"] = $(this).find("input[class*='cant']").val();
                    item ["precio"] = $(this).find("input[class*='precio']").val();
                    item ["descuento"] = $(this).find("input[id*='serv_id']").data("ad");
                    item ["cotizacion_id"] = id_cotizacion;
                    DATA.push(item);
                });
                INFO  = new FormData();
                aInfo   = JSON.stringify(DATA);
                INFO.append('data', aInfo);
                $.ajax({
                    data: INFO,
                    type: 'POST',
                    url: base_url+'index.php/Cotizaciones/guardaServiciosCotiza',
                    processData: false, 
                    contentType: false,
                    async: false,
                    success: function(data2){
                        total_monto = data2;
                    }
                });
                swal({
                    title: 'Éxito!',
                    text: "Se guardó la cotización con el No."+data,
                    type: 'success',
                    showCancelButton: false,
                    allowOutsideClick: false
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        window.location = base_url+"index.php/cotizaciones";
                    }
                }).catch(swal.noop);
            }
        });
    }
    
    function cambiaEstatusPass(id_cotiza){
        $.ajax({
            type: "POST",
            url: base_url+"index.php/Cotizaciones/changeStatusPass",
            data: {id: $("#id_pass").val(), id_cotiza:id_cotiza},
            success: function (data) {

            }
        });
    }

    var cont_row=1; var partida=1; var total_servs=0;
    function agregar_servicio(){
        var id_cli = $("#cliente option:selected").val();
        var ser_sel=$("#slc_servicio").val();
        var repetido=0; var serv_asig=0; var cant_ant=0; var cantidad=0; var contPart=0;
        var TABLA = $("#tabla_servs tbody > tr");
        TABLA.each(function(){         
            serv_asig = $(this).find("input[id*='serv_id']").val();
            contPart++;
            if (serv_asig==ser_sel) {
                repetido++;
                
                cant_ant = parseFloat($(this).find("input[class*='cant_"+serv_asig+"']").val());
                //console.log("serv_asig: "+serv_asig);
                /*console.log("ser_sel: "+ser_sel);
                console.log("cant_ant: "+cant_ant);*/
                cant_nva = cant_ant+1;
                //console.log("cant_nva: "+cant_nva);
                if(repetido>0){
                    //console.log("contPart: "+contPart);
                    cantidad = $(this).find("input[class*='cant_"+serv_asig+"']").val(cant_nva);
                    precio=parseFloat($(this).find("input[class*='precio_"+serv_asig+"']").val());
                    //console.log("precio: "+precio);
                    nvo_tot = cant_nva*precio;
                    //console.log("nvo_tot: "+nvo_tot);
                    $(this).find("[class*='sub_"+serv_asig+"']").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(nvo_tot));
                }
            }
            calculaSuper();
        });
        if(repetido==0){
            $.ajax({
                type: "POST",
                url: base_url+"index.php/cotizaciones/agregarServicio/"+$("#slc_servicio").val()+"/1/1/0/0/"+id_cli,
                //async: false,
                //dataType: "JSON",
                success: function (data) {
                    //$("#table_servicios").html(data);
                    //valida_descuentos(1,0);
                    var array = $.parseJSON(data);
                    console.log("precio: "+array.precio);
                    
                    var html=""; var btn ="";

                    btn = "<td><button type='button' onclick='elimina_servicio($(this),"+array.id_servicio+")' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-minus'></i></button></td>";

                    html+= "<tr class=ord_"+cont_row+" id='prodid_"+array.id_servicio+"'>\
                                <td style='display:none;'>"+partida+"</td>\
                                <td>"+partida+"</td>\
                                <input type='hidden' id='id_cot_serv' value='0'>\
                                <td><input class='id_serv_"+array.id_servicio+"' id='id_serv_add' type='hidden' value='"+array.id_servicio+"'>"+array.clave+"</td>\
                                <td>"+array.desc+"</td>\
                                <td><input type='hidden' data-ad='"+array.ad+"' id='serv_id' value='"+array.id_servicio+"'>\
                                    <input type='number' class='cant cant_"+array.id_servicio+" form-control form-control-sm width-150' value='1'></td>\
                                <td><input type='hidden' class='precio precio_"+array.id_servicio+" form-control form-control-sm' value='"+array.precio+"'> "+parseFloat(array.precio)+"</td>\
                                <td data-sub='' class='sub sub_"+array.id_servicio+"' id='tot_"+cont_row+"'>"+parseFloat(array.precio*1)+"</td>\
                                "+btn+"\
                            </tr>";
                    $("#table_servicios").append(html); 
                    partida++;
                    cont_row++;
                    calculaSuper();
                }
            });
        }    
    }

    function calculaSuper(){
        var final=0;
        //console.log("calculaSuper: ");
        $("td[class*='sub']").each(function() {
            total = $(this).text();
            //console.log("total: "+total);
            total = total.replace("$","");
            total = total.replace(",","");
            var stotal = total;
            final += Number(stotal);
        });
        $("#tot").val(final);
    }
    
    function cambia_cantidad(input,cant){
        //console.log("cambia_cantidad: "+input);
        //console.log("cant: "+cant);
        if(!$.isNumeric(input.val()) && input.val()<1){
            swal("","Por favor ingrese un valor positivo","warning");
            input.val(1);
        }
        var row=input.closest("tr").find("td").eq(0).html();
        console.log('row '+row);
        var serv_asig = $(".ord_"+row).find("input[id*='serv_id']").val();
        console.log('serv '+serv_asig);
        precio = $(".precio_"+serv_asig).val();
        subt = parseFloat(cant) * parseFloat(precio);
        //console.log("row: "+row);
        $("#tot_"+row).html(subt);   
        $(".sub_"+serv_asig+"").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(subt));
    
        valida_descuentos(); 
        calculaSuper();        
        //console.log("precio: "+precio);
        //console.log("subt: "+subt);
    }

    function elimina_servicio(btn,partida){
        btn.closest("#prodid_"+partida+"").remove();
        calculaSuper();
    }

    function elimina_servicioAsignado(id){
        swal({
            title: '¡Alerta!',
            text: "¿Desea eliminar el servicio de la cotización? <br> El cambio será irreversible",
            type: 'warning',
            showCancelButton: true,
            allowOutsideClick: false
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: base_url+"index.php/cotizaciones/eliminaServicio",
                    data: {id: id},
                    success: function (data) {
                        //valida_descuentos(1,0);
                        getServicios($("#id").val());
                        calculaSuper();
                    }
                });
            }
        }).catch(swal.noop);    
    }

    function getServicios(id) {
        $.ajax({
            type: "POST",
            url: base_url+"index.php/cotizaciones/serviciosCotiza",
            data: {id: id},
            success: function (data) {
                $("#table_servicios").html(data);
                valida_descuentos();
            }
        });
    }

    function valida_descuentos(){
        var tot=0;
        var TABLA   = $("#tabla_servs tbody > tr");
        var descuento = $("#descuento").val();
        TABLA.each(function(){         
            if ($(this).find("input[id*='serv_id']").data("ad")==1) {
                sub = parseFloat($(this).find("input[class*='cant']").val()) * parseFloat($(this).find("input[class*='precio']").val())
                tot += Number(sub);
            }
        });
        $("#tot").val(tot);
        //console.log("tot: "+tot);
        desc = ((descuento*tot)/100);
        var html=""; 
            html+="<tr>\
                    <td>Descuento sobre el total:</td>\
                    <td></td>\
                    <td>"+descuento+" %</td>\
                    <td>"+convertMoneda(desc)+"</td></tr>";
        $("#table_descuentos").html(html);
    }

    function convertMoneda(num){
        monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(num);
        return monto;
    }
    
