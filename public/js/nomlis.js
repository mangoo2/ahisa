var base_url = $('#base_url').val();
var table; var tipo_plano_g=0;
var indnom_glob=0;
$(document).ready(function() {	
    $('#id_cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Nom/getCliente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#intro').richText({
        imageUpload: true,
        fileUpload: false,
        videoEmbed: false,
        urls: false,
        code: false
    });

    $("#save").on("click",function(){
        guardarIntro();
    });

    $("#id_cliente").on("change",function(){
        if($("#id_cliente option:selected").val()!=undefined){
            loadtable();
        }
    });
    $("#Fecha1","#Fecha2").on("change",function(){
        if($("#Fecha1").val()!="" && $("#Fecha2").val()!=""){
            loadtable();
        }
    });
    $("#norma").on("change",function(){
        loadtable();
    });

    loadtable();
    $('#save_re').on("click",function(){
        var todos_ptos=2; var id_pto=0;
        if($("#todos_ptos").is(":checked")==true){
            todos_ptos=1;
        }else{
            id_pto=$("#id_punto option:selected").val();
        }
        $.ajax({
            type:'POST',
            url: base_url+'Nom/inserupdate_resolucion_e',
            data: { 
                idnom: $('#id_nom_re').val(), 
                escala1:$('input:radio[name=escala1]:checked').val(),
                escala2:$('input:radio[name=escala2]:checked').val(),
                escala3:$('input:radio[name=escala3]:checked').val(),
                distribucion1:$('#tipo_dis1').val(),
                distribucion2:$('#tipo_dis2').val(),
                distribucion3:$('#tipo_dis3').val(),
                sencibilidad1:$('#h1_coef_sensibilidad').val(),
                sencibilidad2:$('#h2_coef_sensibilidad').val(),
                sencibilidad3:$('#h3_coef_sensibilidad').val(),
                todos_ptos:todos_ptos,
                id_punto:id_pto
            },
            async: false,
            success:function(data){
                toastr.success('Hecho!', 'Guardado Correctamente');
                $('#modal_reso_equipo').modal('hide');
            }
        });
    });
    $('#todos_ptos').on("click",function(){
        if($("#todos_ptos").is(":checked")==false){ //por punto
            $("#cont_ptos").show("slow");
            $.ajax({
                type:'POST',
                url: base_url+'Nom/getPuntosNom',
                data: { 
                    idnom: $('#id_nom_re').val()
                },
                async: false,
                success:function(data){
                    $('#id_punto').html(data);
                }
            });
        }else{
            $("#cont_ptos").hide("slow");
            verficaPunto(1);
        }
    });
    $('#id_punto').on("change",function(){
        verficaPunto(2);
    });

    //para las nuevas conclusiones
    $("#ptos_incidencia").on("change",function(){
        $("input[id*='num_ptos_evalua1']").val($(this).val());
        $("input[id*='num_ptos_evalua2']").val($(this).val());
        $("input[id*='num_ptos_evalua3']").val($(this).val());

        //$(".chr").css("display","block");
    });

    $("#num_supera1,#num_no_supera1").on("change",function(){
        $(".chr1").css("display","block");
    });
    $("#num_supera2,#num_no_supera2").on("change",function(){
        $(".chr2").css("display","block");
    });
    $("#num_supera3,#num_no_supera3").on("change",function(){
        $(".chr3").css("display","block");
    });

    $("#num_superar1,#num_no_superar1").on("change",function(){
        $(".chr_ref1").css("display","block");
    });
    $("#num_superar2,#num_no_superar2").on("change",function(){
        $(".chr_re2").css("display","block");
    });
    $("#num_superar3,#num_no_superar3").on("change",function(){
        $(".chr_re3").css("display","block");
    });


    $("#ptos_sin_incidencia").on("change",function(){
        $("input[class*='num_ptos_evalua_sin']").val($(this).val());
        if($(this).val()==0){
            $("#num_sin_supera1").val("");
            $("#num_sin_no_supera1").val("");
            //$("#table_ilum_sin").hide();
            $(".chrs").hide();
            $("#cont_table_ilum_sin").hide("slow");

        }else if($(this).val()!=0){
            $(".chrs").show(); 
            $("#cont_table_ilum_sin").show("slow");  
        }
    });


    if($("#ptos_sin_incidencia").val()==0){
        $(".chrs").hide();
        $("#cont_table_ilum_sin").hide("slow");
    }else{
        $(".chrs").show();
        $("#cont_table_ilum_sin").show("slow");
    }

    $("#ilumina").on("click",function(){
        if($("#ilumina").is(":checked")==true){
            $("#cont_table_ilum").show("slow");
        }else{
            $("#cont_table_ilum").hide("slow");
            //$("#cont_table_ilum_sin_reflex").hide("slow");
        }
    });

    //para las nuevas conclusiones de reflexion
    $("#ptos_incidencia_ref").on("change",function(){
        $("input[id*='num_ptos_evaluar1']").val($(this).val());
        $("input[id*='num_ptos_evaluar2']").val($(this).val());
        $("input[id*='num_ptos_evaluar3']").val($(this).val());
    });
    $("#ptos_sin_incidencia_ref").on("change",function(){
        $("input[class*='num_ptos_evaluaref_sin']").val($(this).val());
        if($(this).val()==0){
            $("#num_sin_superar1").val("");
            $("#num_sin_no_superar1").val("");
            //$("#table_ilum_sin_reflex").hide();
            $(".chrs_ref").hide();
            $("#cont_table_ilum_sin_reflex").hide("slow");
        }else if($(this).val()!=0){
            $(".chrs_ref").show();
            $("#cont_table_ilum_sin_reflex").show("slow"); 
        }
    });

    if($("#ptos_sin_incidencia_ref").val()==0){
        $(".chrs_ref").hide();
        $("#cont_table_ilum_sin_reflex").hide("slow");
    }else{
        $(".chrs_ref").show();
        $("#cont_table_ilum_sin_reflex").show("slow");
    }

    $("#reflexion").on("click",function(){
        if($("#reflexion").is(":checked")==true){
            $("#cont_table_reflex").show("slow");
        }else{
            $("#cont_table_reflex").hide("slow");
        }
    });

    $("#saveConclusion11").on("click",function(){
        guardarConclusiones(11);
    });
    $("#saveConclusion").on("click",function(){
        guardarConclusiones(25);
    });
    $("#saveConclusion22").on("click",function(){
        guardarConclusiones(22);
    });

    $("#tipo_plano11").on("change",function(){
        tipo_plano_g = $("#tipo_plano11 option:selected").val();
        getPlanosNom(2,$("#tipo_plano11 option:selected").val(),11);
    });

    $("#tipo_plano81").on("change",function(){
        tipo_plano_g = $("#tipo_plano81 option:selected").val();
        getPlanosNom(2,$("#tipo_plano81 option:selected").val(),81);
    });
    $("#tipo_plano15").on("change",function(){
        tipo_plano_g = $("#tipo_plano15 option:selected").val();
        getPlanosNom(2,$("#tipo_plano15 option:selected").val(),15);
    });

    $(".kv-file-remove").on("click",function(){
        $('#inputFile').fileinput('refresh');
        $("#modal_img_desc").modal("hide");
    });

    $(".puntos_graph").on("change",function(){
        //conclusionesGraficas(indnom_glob,25);
        //console.log("cambia y grafico");
        if($(this).data("tipo")=="1"){
            var val=$("#num_supera"+$(this).data("med")+"").val();
            var val_no=$("#num_no_supera"+$(this).data("med")+"").val();
            var val_num=$("#num_ptos_evalua"+$(this).data("med")+"").val();
        }else{
            var val=$("#num_sin_supera"+$(this).data("med")+"").val();
            var val_no=$("#num_sin_no_supera"+$(this).data("med")+"").val();
            var val_num=$("#num_ptos_sin_evalua"+$(this).data("med")+"").val();
        }
        //console.log("val: "+val);
        //console.log("val_no: "+val_no);
        //console.log("val_num: "+val_num);
        graficaIluminacionIncidencia($(this).data("tipo"),$(this).data("med"),val_num,val,val_no,1)
    });
    $(".puntos_graph_sin").on("change",function(){
        //conclusionesGraficas(indnom_glob,25);
        if($(this).data("tipo")=="2"){
            var val=$("#num_superar"+$(this).data("med")+"").val();
            var val_no=$("#num_no_superar"+$(this).data("med")+"").val();
            var val_num=$("#num_ptos_evaluar"+$(this).data("med")+"").val();
        }
        else{
            var val=$("#num_sin_superar"+$(this).data("med")+"").val();
            var val_no=$("#num_sin_no_superar"+$(this).data("med")+"").val();
            var val_num=$("#num_ptos_sin_evaluar"+$(this).data("med")+"").val();
        }
        /*console.log("tipo: "+$(this).data("tipo"));
        console.log("val: "+val);
        console.log("val_no: "+val_no);
        console.log("val_num: "+val_num);*/
        graficaIluminacionIncidencia($(this).data("tipo"),$(this).data("med"),val_num,val,val_no,1)
    });
    
    /*$(".kv-file-remove").on("click",function(){
        $("#modal_img_desc").modal("hide");
    });*/

    $(document).on('click','.kv-file-remove',function(){
        swal("¡Éxito!", "Eliminado correctamente", "success");
        setTimeout(function () { 
            $("#modal_img_desc").modal("hide");
        }, 1500);
    })

});

function verficaPunto(tod){
    var idnom=$('#id_nom_re').val();
    var id_pto=0;
    id_pto=$("#id_punto option:selected").val();
    
    $.ajax({
            type:'POST',
            url: base_url+'Nom/get_resolucion_e',
            data: { 
                idnom: idnom, todos:tod, id_pto:id_pto,
            },
            async: false,
            success:function(data){
                var array = $.parseJSON(data);
                if(array.datosnum>0){
                    $.each(array.datos, function(index, item) {
                        $('input:radio[name=escala1][value='+item.escala1+']').prop('checked',true);
                        $('input:radio[name=escala2][value='+item.escala2+']').prop('checked',true);
                        $('input:radio[name=escala3][value='+item.escala3+']').prop('checked',true);

                        $('#tipo_dis1').val(item.distribucion1);
                        $('#tipo_dis2').val(item.distribucion1);
                        $('#tipo_dis3').val(item.distribucion1);
                        $('#h1_coef_sensibilidad').val(item.sencibilidad1);
                        $('#h2_coef_sensibilidad').val(item.sencibilidad2);
                        $('#h3_coef_sensibilidad').val(item.sencibilidad3);
                        calcularre();
                    });
                }else{
                    $('input:radio[name=escala1]').prop('checked',false);
                    $('input:radio[name=escala2]').prop('checked',false);
                    $('input:radio[name=escala3]').prop('checked',false);

                    $('#tipo_dis1').val("B, rectangular");
                    $('#tipo_dis2').val("B, rectangular");
                    $('#tipo_dis3').val("B, rectangular");
                    $('#h1_coef_sensibilidad').val("");
                    $('#h2_coef_sensibilidad').val("");
                    $('#h3_coef_sensibilidad').val("");
                    calcularre();
                }
            }
        });
}

function inputFileCharge(nom,tipo_plano=0){
    $('#inputFile').fileinput('destroy');
    var btn_upload="";
    //console.log("nom de inputFile: "+nom);
    //$("#cont_inputFile").html("");
    /*if(nom==11){
        var tipo_plano = $("#tipo_plano11 option:selected").val();
    }
    else if(nom==81){
        var tipo_plano = $("#tipo_plano81 option:selected").val();
    }else{
        var tipo_plano = 0;
    }*/
    //console.log("tipo_plano_g inputFileCharge: "+tipo_plano_g);
    $("#inputFile").fileinput({
        showCaption: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png","webp"],
        browseLabel: 'Seleccionar Archivo',
        uploadUrl: base_url+'Nom/uploadImg',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'webp': '<i class="fa fa-file-photo-o text-warning"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                        id_nom:$('#id_nom_img_desc').val(),
                        tipo: $("#tipo_img_carga").val(),
                        nom:nom,
                        tipo_plano:tipo_plano_g
                    };
            return info;
        }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
        //location.reload();
        $('#inputFile').fileinput('refresh');
        $("#modal_img_desc").modal("hide");
        if(nom==11){
            if($("#tipo_img_carga").val()==2){
                //insertPlanosNom11($("#id_nom_img_desc").val());
                getPlanosNom(2,$("#tipo_plano11 option:selected").val(),11);
            }else if($("#tipo_img_carga").val()==3){
                //insertFichaTecnica($("#id_nom_img_desc").val());
                getPlanosNom(3,0,11);
            }else if($("#tipo_img_carga").val()==4){
                //insertPrograma11($("#id_nom_img_desc").val());
                getPlanosNom(4,0,11);
            }
        }
        if(nom==25){
            if($("#tipo_img_carga").val()==1){
                insertImgDescPro($("#id_nom_img_desc").val());
            }
            else if($("#tipo_img_carga").val()==2){
                insertPrograma($("#id_nom_img_desc").val());
            }
            else if($("#tipo_img_carga").val()==3){
                insertPlanos($("#id_nom_img_desc").val());
            }
            else if($("#tipo_img_carga").val()==4){
                insertPlanosLumi($("#id_nom_img_desc").val());
            }
        }if(nom==22){
            if($("#tipo_img_carga").val()==2){
                getPlanosNom(2,0,22);
            }
        }
        if(nom==81){
            if($("#tipo_img_carga").val()==2){
                getPlanosNom(2,$("#tipo_plano81 option:selected").val(),81);
            }
        }if(nom==15){
            if($("#tipo_img_carga").val()==2){
                getPlanosNom(2,$("#tipo_plano15 option:selected").val(),15);
            }
        }
    }).on('fileuploaded', function(event, data, previewId, index) {
        console.log("Archivo subido exitosamente");
        swal("¡Éxito!", "Archivo subido exitosamente.", "success");
    }).on('fileuploaderror', function(event, data, msg) {
        console.log("Error al subir el archivo.");
        swal("¡Error!", "Error al subir el archivo.", "error");
        console.log(msg); // Mensaje de error específico
        console.log(data.jqXHR); // Objeto jqXHR para ver detalles de la respuesta
    });
}

function conclusionesGraficas(id,nom){
    //console.log("tipo: "+tipo);
    //console.log("id: "+id);
    $("#modal_conclusion").modal();
    //$(".chr1").html("").html('<canvas id="myChart" style="width:100%;"></canvas>');
    indnom_glob=id;

    if(nom==11){
        $("#cont_conc11").show();
        $("#foot_cont_conc11").show();
        $("#cont_conc25").hide();
        $("#foot_cont_conc25").hide();
        $("#cont_conc22").hide();
        $("#foot_cont_conc22").hide();
        //console.log("id de la nom11: "+id);
        $("#id_nom_conc11").val(id); //el id de la tabla conclusiones
        $.ajax({
            type:'POST',
            url: base_url+'Nom/getConclusiones',
            data: { id_nom: id, nom:11 },
            async: false,
            success:function(data){
                //console.log("data: "+data);
                var array = $.parseJSON(data);
                conc = array.conclusiones;
                $("#total_ptos_amb").val(conc.total_ptos_amb);
                $("#id_conclu11").val(conc.id);
                //$("#id_nom_conc11").val(conc.id_nom);
                $("#no_superan_amb").val(conc.no_superan_amb);
                $("#superan_amb").val(conc.superan_amb);
                $("#total_ptos_pers").val(conc.total_ptos_pers);
                $("#no_superan_pers").val(conc.no_superan_pers);
                $("#superan_pers").val(conc.superan_pers);
                //console.log("total_ptos_amb: "+conc.total_ptos_amb);
                //console.log("total_ptos_pers: "+conc.total_ptos_pers);
                if(conc.total_ptos_amb!="" && conc.total_ptos_amb>0){
                    $(".chr_amb").show("slow");
                    graficaNom11(1,1,conc.id,11);
                }
                if(conc.total_ptos_pers!="" && conc.total_ptos_pers>0){
                    $(".chr_pers").show("slow");
                    graficaNom11(2,1,conc.id,11);
                }
            }
        });
    }
    if(nom==25){
        $("#cont_conc25").show();
        $("#foot_cont_conc25").show();
        $("#cont_conc11").hide();
        $("#foot_cont_conc11").hide();
        $("#cont_conc22").hide();
        $("#foot_cont_conc22").hide();
        $("#id_nom_conc").val(id); //el id de la tabla conclusiones
        $/*("#cont_conc11").hide();
        $("#foot_cont_conc11").hide();*/
        $.ajax({
            type:'POST',
            url: base_url+'Nom/getConclusiones',
            data: { id_nom: id, nom:25 },
            async: false,
            success:function(data){
                //console.log("data: "+data);
                var array = $.parseJSON(data);
                conc = array.conclusiones;
                pts = array.puntos;
                if(pts!=""){
                    $("#total_ptos").val(conc.total_ptos);
                    $("#id_conclu").val(conc.id);
                    //console.log("con_incidencia: "+pts[0].con_incidencia);
                    var j=0; var k=0; var j2=0; var k2=0;
                    $.each(pts, function(index, i) {
                        if(i.tipo=="1" && i.con_incidencia!="0" && i.con_incidencia!="" && i.tipo_incidencia=="1"){
                            $("#ilumina").attr("checked",true);
                            $("#cont_table_ilum").show("slow");
                            j++;
                            //console.log("j: "+j);
                            $("#ptos_incidencia").val(i.con_incidencia);
                            $("#id"+j+"").val(i.id);
                            $("#num_ptos_evalua"+j+"").val(i.num_ptos_evalua);
                            $("#num_supera"+j+"").val(i.num_supera);
                            $("#num_no_supera"+j+"").val(i.num_no_supera);
                            $(".chr").show("slow");
                            img_chart = i.img_chart;
                            //console.log("img_chart.length: "+img_chart.length);
                            /*if(img_chart.length<=7)
                                bandd=1;
                            else{
                                bandd=0;
                            }*/
                            bandd=0;
                            //setTimeout(function () { 
                                graficaIluminacionIncidencia(1,j,i.num_ptos_evalua,i.num_supera,i.num_no_supera,bandd);
                            //}, 1500);
                            
                        }
                        /*console.log("tipo: "+i.tipo);
                        console.log("tipo_incidencia: "+i.tipo_incidencia);
                        console.log("sin_incidencia: "+i.sin_incidencia);*/
                        if(i.tipo=="1" && i.sin_incidencia!="0" && i.sin_incidencia!="" && i.tipo_incidencia=="2"){ //sin incidencia
                            k++;
                            $("#ptos_sin_incidencia").val(i.sin_incidencia);
                            $("#ids"+k+"").val(i.id);
                            $("#num_ptos_sin_evalua"+k+"").val(i.num_ptos_evalua);
                            $("#num_sin_supera"+k+"").val(i.num_supera);
                            $("#num_sin_no_supera"+k+"").val(i.num_no_supera);

                            $("#cont_table_ilum_sin").show("slow");  
                            $(".chrs1").show("slow");
                            img_chart = i.img_chart;
                            /*if(img_chart.length<=7)
                                bandd=1;
                            else{
                                bandd=0;
                            }*/
                            bandd=0;
                            //setTimeout(function () { 
                                graficaIluminacionIncidencia(11,k,i.num_ptos_evalua,i.num_supera,i.num_no_supera,bandd);
                            //}, 1500);  
                        }
                        //para los puntos de reflexion
                        if(i.tipo=="2" && i.con_incidencia!="0" && i.con_incidencia!="" && i.tipo_incidencia=="1"){
                            $("#reflexion").attr("checked",true);
                            $("#cont_table_reflex").show("slow");
                            j2++;
                            //console.log("j: "+j);
                            $("#ptos_incidencia_ref").val(i.con_incidencia);
                            $("#idr"+j2+"").val(i.id);
                            $("#num_ptos_evaluar"+j2+"").val(i.num_ptos_evalua);
                            $("#num_superar"+j2+"").val(i.num_supera);
                            $("#num_no_superar"+j2+"").val(i.num_no_supera);

                            $(".chr_ref").show("slow");
                            /*if(img_chart.length<=7)
                                bandd=1;
                            else{
                                bandd=0;
                            }*/
                            bandd=0;
                            //setTimeout(function () { 
                                graficaIluminacionIncidencia(2,j2,i.num_ptos_evalua,i.num_supera,i.num_no_supera,bandd);
                            //}, 1500); 
                        }
                        if(i.tipo=="2" && i.sin_incidencia!="0" && i.sin_incidencia!="" && i.tipo_incidencia=="2"){
                            k2++;
                            $("#ptos_sin_incidencia_ref").val(i.sin_incidencia);
                            $("#idsr"+k2+"").val(i.id);
                            $("#num_ptos_sin_evaluar"+k2+"").val(i.num_ptos_evalua);
                            $("#num_sin_superar"+k2+"").val(i.num_supera);
                            $("#num_sin_no_superar"+k2+"").val(i.num_no_supera);
                            $("#cont_table_ilum_sin_reflex").show("slow"); 
                            $(".chrs_ref").show("slow");
                            img_chart = i.img_chart;
                            /*if(img_chart.length<=7)
                                bandd=1;
                            else{
                                bandd=0;
                            }*/
                            bandd=0;
                            //setTimeout(function () { 
                                graficaIluminacionIncidencia(22,k2,i.num_ptos_evalua,i.num_supera,i.num_no_supera,bandd); //solo consultar graficas
                            //}, 1500); 
                        }
                        //console.log("J: "+j);
                        if(j==0){
                            $("#ilumina").attr("checked",false);
                            $("#cont_table_ilum").hide();
                        }else{
                            $("#cont_table_ilum").show("slow");
                        }if(j2==0){
                            //$("#reflexion").attr("checked",false);
                            //$("#cont_table_reflex").hide();
                        }else{
                            $("#cont_table_reflex").show("slow");
                        }
                    });
                }else{
                    $("#total_ptos").val("");
                    $("#id_conclu").val("0");
                    $("#ptos_incidencia").val("");
                    $("#ptos_sin_incidencia").val("0");
                    $("#ptos_incidencia_ref").val("");
                    $("#ptos_sin_incidencia_ref").val("0");
                    //$("#cont_table_reflex").hide("slow");
                    $("#cont_table_ilum_sin").hide("slow");
                    $(".chrs").hide("slow");
                    $(".chrs_ref").hide("slow");
                    //$("#ptos_sin_incidencia_ref").val("");
                    //$("#reflexion").attr("checked",false);

                    //$("input[type*='input']").val("");

                    /*$(".chr").hide("slow");
                    $(".chrs").hide("slow");
                    $(".chr_ref").hide("slow");
                    $(".chrs_ref").hide("slow");*/

                    var TABLA = $("#table_ilum tbody > tr"); //tabla de iluminacion con incidencia solar
                    ct1=0;
                    TABLA.each(function(){    
                        ct1++;    
                        //console.log("ct1: "+ct1);
                        $("input[id*='num_ptos_evalua"+ct1+"']").val("");
                        $("input[id*='num_supera"+ct1+"']").val("");
                        $("input[id*='num_no_supera"+ct1+"']").val("");

                        $("input[id*='num_ptos_sin_evalua"+ct1+"']").val("");
                        $("input[id*='num_sin_supera"+ct1+"']").val("");
                        $("input[id*='num_sin_no_supera"+ct1+"']").val("");

                        $("input[id*='num_ptos_evaluar"+ct1+"']").val("");
                        $("input[id*='num_superar"+ct1+"']").val("");
                        $("input[id*='num_no_superar"+ct1+"']").val("");

                        $("input[id*='num_ptos_sin_evaluar"+ct1+"']").val("");
                        $("input[id*='num_sin_superar"+ct1+"']").val("");
                        $("input[id*='num_sin_no_superar"+ct1+"']").val("");
                        //$("#cont_table_reflex").hide("slow");
                    });
                }
            }
        });
    }
    if(nom==22){
        $("#cont_conc22").show();
        $("#foot_cont_conc22").show();
        $("#cont_conc11").hide();
        $("#foot_cont_conc11").hide();
        $("#cont_conc25").hide();
        $("#foot_cont_conc25").hide();
        //console.log("id de la nom11: "+id);
        $("#id_nom_conc22").val(id); //el id de la tabla conclusiones
        $.ajax({
            type:'POST',
            url: base_url+'Nom/getConclusiones',
            data: { id_nom: id, nom:22 },
            async: false,
            success:function(data){
                //console.log("data: "+data);
                var array = $.parseJSON(data);
                conc = array.conclusiones;
                $("#total_electrodo_pt").val(conc.total_electrodo_pt);
                $("#id_conclu22").val(conc.id);
                $("#no_superan_pt").val(conc.no_superan_pt);
                $("#supera_pt").val(conc.supera_pt);
                $("#total_electrodo_pr").val(conc.total_electrodo_pr);
                $("#no_supera_pr").val(conc.no_supera_pr);
                $("#supera_pr").val(conc.supera_pr);
                $("#total_ptos_continuidad").val(conc.total_ptos_continuidad);
                $("#existe_conti").val(conc.existe_conti);
                $("#no_existe_conti").val(conc.no_existe_conti);
                if(conc.total_electrodo_pt!="" && conc.total_electrodo_pt>0){
                    $(".chr_spt").show("slow");
                    graficaNom11(1,1,conc.id,22);
                }
                if(conc.total_electrodo_pr!="" && conc.total_electrodo_pr>0){
                    $(".chr_spr").show("slow");
                    graficaNom11(2,1,conc.id,22);
                }
                if(conc.total_ptos_continuidad!="" && conc.total_ptos_continuidad>0){
                    $(".chr_ce").show("slow");
                    graficaNom11(3,1,conc.id,22);
                }
            }
        });
    }
}

function graficaIluminacionIncidencia(tipo,med,num,supera,nosupera,band_save){
    /*console.log("tipo: "+tipo);
    console.log("med: "+med);
    console.log("num: "+num);
    console.log("supera: "+supera);
    console.log("nosupera: "+nosupera);
    console.log("band_save: "+band_save);*/
    var tots = parseFloat(supera * 100/num).toFixed(2);
    var totns = parseFloat(nosupera * 100/num).toFixed(2);

    //console.log("tipo: "+tipo);
    //console.log("med: "+med);
    //console.log("tots: "+tots);
    //console.log("totns: "+totns);
    if(tipo==1 || tipo==11){
        var xValues = ["Supera el NMI", "No supera el NMI"];
    }else{
        var xValues = ["Supera el NMI", "No supera el NMP"];
    }
    var yValues = [tots, totns];
    var barColors = [
      "#002060",
      "#7F7F7F"
    ];
    if(tipo==1){ //grafica de iluminacion con incidecia
        if(med==1){
            text = "Primera medición";
            charta="myChart";
        }else if(med==2){
            text = "Segunda medición";
            charta="myChart2";
        }else if(med==3){
            text = "Tercera medición";
            charta="myChart3";
        }
        new Chart(charta, {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: text
                },
                plugins: {
                  datalabels: {
                    anchor: "center",
                    formatter: (dato) => dato + "%",
                    color: "white",
                    font: {
                      family: '"Times New Roman", Times, serif',
                      size: "14",
                      weight: "bold",
                    },
                  }
                },
            }
        });
        /*if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
            setTimeout(function () { 
                savecanvas(1,med);
            }, 2000);
        }*/
    }
    if(tipo==11){ //grafica de iluminacion sin incidecia
        new Chart("myChartsin", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Única medición"
                },
                plugins: {
                  datalabels: {
                    anchor: "center",
                    formatter: (dato) => dato + "%",
                    color: "white",
                    font: {
                      family: '"Times New Roman", Times, serif',
                      size: "14",
                      weight: "bold",
                    },
                  }
                },
            }
        });
        /*if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
            setTimeout(function () { 
                savecanvas(11,med);
            }, 2000);
        }*/
    }

    if(tipo==2){ //grafica de iluminacion con incidecia
        if(med==1){
            text = "Primera medición";
            charta="myChartr";
        }else if(med==2){
            text = "Segunda medición";
            charta="myChartr2";
        }else if(med==3){
            text = "Tercera medición";
            charta="myChartr3";
        }
        new Chart(charta, {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: text
                },
                plugins: {
                  datalabels: {
                    anchor: "center",
                    formatter: (dato) => dato + "%",
                    color: "white",
                    font: {
                      family: '"Times New Roman", Times, serif',
                      size: "14",
                      weight: "bold",
                    },
                  }
                },
            }
        });
        /*if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
            setTimeout(function () { 
                savecanvas(2,med);
            }, 3000);
        }*/
    }
    if(tipo==22){ //grafica de iluminacion sin incidecia
        new Chart("myChartsin_ref", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Única medición"
                },
                plugins: {
                  datalabels: {
                    anchor: "center",
                    formatter: (dato) => dato + "%",
                    color: "white",
                    font: {
                      family: '"Times New Roman", Times, serif',
                      size: "14",
                      weight: "bold",
                    },
                  }
                },
            }
        });
        /*if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
            setTimeout(function () { 
                savecanvas(22,med);
            }, 3000);
        }*/
    }
    if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
        if(tipo==1)
        setTimeout(function () { 
            savecanvas(1,med);
        }, 3000);
        if(tipo==11)    
        setTimeout(function () { 
            savecanvas(11,med);
        }, 3500);
    }
    if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
        if(tipo==2){
            setTimeout(function () { 
                savecanvas2(2,med);
            }, 3000);
        }if(tipo==22){
            setTimeout(function () { 
                savecanvas2(22,med);
            }, 3500);
        }
    }
}

function graficaNom11(tipo,band_save,id_conc11,nom){
    /*console.log("tipo: "+tipo);
    console.log("band_save: "+band_save);
    console.log("id_conc11: "+id_conc11);*/
    if(nom==11){
        var xValues = ["Supera el TMPE", "No supera el TMPE"];
        if(tipo==1){
            var text = "mediciones ambientales";
            var charname="chartAmb";
            var tots = parseFloat(Number($("#superan_amb").val()) * 100/Number($("#total_ptos_amb").val()).toFixed(2));
            var totns = parseFloat(Number($("#no_superan_amb").val()) * 100/Number($("#total_ptos_amb").val()).toFixed(2));
        }
        if(tipo==2){
            var text = "mediciones personales";
            var charname="chartPers";
            var tots = parseFloat(Number($("#superan_pers").val()) * 100/Number($("#total_ptos_pers").val()).toFixed(2));
            var totns = parseFloat(Number($("#no_superan_pers").val()) * 100/Number($("#no_superan_pers").val()).toFixed(2));
        }
        var yValues = [tots, totns];

    }else if(nom==22){
        var xValues = ["Supera el LMP", "No supera el LMP"];
        if(tipo==1){
            var text = "Sistema de puesta a tierra";
            var charname="chartSPT";

            var tots = parseFloat(((Number($("#supera_pt").val()) * 100) / Number($("#total_electrodo_pt").val())).toFixed(1)); 
            var totns = parseFloat(((Number($("#no_superan_pt").val()) * 100) / Number($("#total_electrodo_pt").val())).toFixed(1));


        }else if(tipo==2){
            var text = "Sistema de pararrayos";
            var charname="chartSPR";
            var tots = parseFloat(((Number($("#supera_pr").val()) * 100) / Number($("#total_electrodo_pr").val())).toFixed(1));
            var totns = parseFloat(((Number($("#no_supera_pr").val()) * 100) / Number($("#total_electrodo_pr").val())).toFixed(1));

        }else if(tipo==3){
            xValues = ["Existe continuidad", "No existe continuidad"];
            var text = "Continuidad eléctrica";
            var charname="chartCE";
            if(Number($("#total_ptos_continuidad").val()).toFixed(2)==Number($("#existe_conti").val()) && Number($("#total_ptos_continuidad").val()).toFixed(2)!=0){
                var tots=100;
                var totns=0;
            }else{
                var tots = parseFloat(((Number($("#existe_conti").val()) * 100) / Number($("#total_ptos_continuidad").val())).toFixed(1));
                var totns = parseFloat(((Number($("#no_existe_conti").val()) * 100) / Number($("#total_ptos_continuidad").val())).toFixed(1));
            }  
        }
        var yValues = [tots, totns];
    }

    var barColors = [
      "#002060",
      "#7F7F7F"
    ];

    new Chart(charname, {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: text
            },
            plugins: {
              datalabels: {
                anchor: "center",
                formatter: (dato) => dato + "%",
                color: "white",
                font: {
                  family: '"Times New Roman", Times, serif',
                  size: "14",
                  weight: "bold",
                },
              }
            },
        }
    });
    if(band_save==1){ //band 1 es pera guardar, 0 es para consultar
        setTimeout(function () { 
            var canvasx= document.getElementById(charname);
            var dataURL = canvasx.toDataURL('image/jpg', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/saveCanvasConclusion',
                async:false,
                data: {
                    id: id_conc11,
                    grafica:dataURL,
                    charname: charname,
                    nom:nom
                },
                success:function(data){
                    //console.log("hecho");
                }
            });        
        }, 1000);
    }
    
}

function guardarConclusiones(nom){//para guarda conclusiones
    var id_conc=0;
    if(nom==11){
        var form_register = $('#form_conclusiones11');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input    
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var valid = $("#form_conclusiones11").valid();
        if(valid) {
            $.ajax({
                data: form_register.serialize()+"&nom=11",
                type: 'POST',
                url : base_url+'Nom/submitConclusion',
                async: false,
                beforeSend: function(){
                    $("#saveConclusion11").attr("disabled",true);
                 },
                success: function(data){
                    var id_conc11=data;
                    if($("#total_ptos_amb").val()!="" && $("#total_ptos_amb").val()>0){
                        $(".chr_amb").show("slow");
                        graficaNom11(1,1,id_conc11);
                    }
                    if($("#total_ptos_pers").val()!="" && $("#total_ptos_pers").val()>0){
                        $(".chr_pers").show("slow");
                        graficaNom11(2,1,id_conc11);
                    }
                    setTimeout(function () { 
                        toastr.success('Hecho!', 'Conclusiones guardadas correctamente');
                        $("#saveConclusion11").attr("disabled",false);
                        $("#modal_conclusion").modal("hide");
                        loadtable();
                    }, 2000);
                }
            }); 
        }
    }
    if(nom==25){
        $.ajax({
            type:'POST',
            url: base_url+'Nom/submitConclusion',
            data: $("#form_conclusiones").serialize()+"&nom=25",
            async:false,
            beforeSend: function(){
                $("#saveConclusion").attr("disabled",true);
            },
            success:function(res){
                id_conc = res;
                var cont_time=1000;
                $("#id_conclu").val(id_conc);
                if($("#ilumina").is(":checked")==true){       
                    var DATA  = [];
                    var TABLA = $("#table_ilum tbody > tr"); //tabla de iluminacion con incidencia solar
                    var j=0;
                    TABLA.each(function(){    
                        j++;    
                        item = {};
                        item["id_conclusion"]=id_conc;
                        item["tipo"] = 1;
                        item["tipo_incidencia"] = 1;
                        item["con_incidencia"] = $("#ptos_incidencia").val();
                        item["sin_incidencia"] = $("#ptos_sin_incidencia").val();

                        item["id"] = $(this).find("input[name*='id']").val();
                        item["medicion"] = $(this).find("input[name*='medicion']").val();
                        item["num_ptos_evalua"] = $(this).find("input[name*='num_ptos_evalua']").val();
                        item["num_supera"] = $(this).find("input[name*='num_supera']").val();
                        item["num_no_supera"] = $(this).find("input[name*='num_no_supera']").val();
                        //console.log("j: "+j);
                        DATA.push(item);
                        //setTimeout(function () { 
                            //savecanvas(1,j);
                            //console.log("j: "+j);
                            graficaIluminacionIncidencia(1,j,item["num_ptos_evalua"],item["num_supera"],item["num_no_supera"],1);//solo se comenta esta linea
                        //}, 500);
                        cont_time=500;
                    });
                    /*INFO  = new FormData();
                    aInfo   = JSON.stringify(DATA);
                    INFO.append('data', aInfo);*/
                    array_incidencia   = JSON.stringify(DATA);
                    //console.log("array_incidencia: "+array_incidencia);
                    var jj=0;
                    var DATArs  = [];
                    var TABLA2 = $("#table_ilum_sin tbody > tr"); //tabla de iluminacion sin incidencia solar
                    if($("#ptos_sin_incidencia").val()!="" && $("#ptos_sin_incidencia").val()!="0"){
                        TABLA2.each(function(){   
                            jj++;     
                            item2 = {};
                            item2["id_conclusion"]=id_conc;
                            item2["tipo"] = 1;
                            item2["tipo_incidencia"] = 2;
                            item2["con_incidencia"] = $("#ptos_incidencia").val();
                            item2["sin_incidencia"] = $("#ptos_sin_incidencia").val();

                            item2["id"] = $(this).find("input[id*='ids1']").val();
                            item2["medicion"] = $(this).find("input[name*='medicion']").val();
                            item2["num_ptos_evalua"] = $(this).find("input[name*='num_ptos_evalua']").val();
                            item2["num_supera"] = $(this).find("input[name*='num_supera']").val();
                            item2["num_no_supera"] = $(this).find("input[name*='num_no_supera']").val();
                            DATArs.push(item2);
                            //setTimeout(function () { 
                                //savecanvas2(11,jj);
                                //console.log("jj osea 11: "+jj);
                                graficaIluminacionIncidencia(11,jj,item2["num_ptos_evalua"],item2["num_supera"],item2["num_no_supera"],1); //solo se comenta esta linea
                            //}, 1000);       
                            cont_time=cont_time+600;
                        });
                    }
                    array_sinincidencia = JSON.stringify(DATArs);
                    //console.log("array_sinincidencia: "+array_sinincidencia);
                    var datos='array_incidencia='+array_incidencia+'&array_sinincidencia='+array_sinincidencia;
                    $.ajax({
                        data: datos,
                        type: 'POST',
                        url: base_url+'Nom/submitDetallesConclusion',
                        async: false,
                        /*processData: false, 
                        contentType: false,
                        async: false,*/
                        success: function(data2){
                            //swal("¡Éxito!", "Puntos guardados correctamente", "success");
                            //comentado porque guarda en cambio de datos input
                            /*savecanvas(1,1);
                            savecanvas2(1,2);
                            savecanvas3(1,3);
                            savecanvas4(11,1);*/
                        }
                    });
                }//termina if de iluminacion
                else{
                    $.ajax({
                        data: { id_conc: id_conc, tipo:1 },
                        type: 'POST',
                        url: base_url+'Nom/submitSinReflexion',
                        success: function(data2){
                            //swal("¡Éxito!", "Puntos guardados correctamente", "success");
                        }
                    });
                }

                if($("#reflexion").is(":checked")==true){       
                    var DATA2R = [];
                    var TABLA2R = $("#table_ilum_reflex tbody > tr"); //tabla de iluminacion con incidencia solar
                    var k=0;
                    TABLA2R.each(function(){      
                        k++;  
                        item2r = {};
                        item2r["id_conclusion"]=id_conc;
                        item2r["tipo"] = 2;
                        item2r["tipo_incidencia"] = 1;
                        item2r["con_incidencia"] = $("#ptos_incidencia_ref").val();
                        item2r["sin_incidencia"] = $("#ptos_sin_incidencia_ref").val();

                        item2r["id"] = $(this).find("input[name*='id']").val();
                        item2r["medicion"] = $(this).find("input[name*='medicion']").val();
                        item2r["num_ptos_evalua"] = $(this).find("input[name*='num_ptos_evalua']").val();
                        item2r["num_supera"] = $(this).find("input[name*='num_supera']").val();
                        item2r["num_no_supera"] = $(this).find("input[name*='num_no_supera']").val();
                        DATA2R.push(item2r);
                        //setTimeout(function () { 
                            //if($("#ptos_incidencia_ref").val()!="")
                                //savecanvas3(2,k);
                            /*console.log("k: "+k);
                            console.log("num_ptos_evalua: "+item2r ["num_ptos_evalua"]);
                            console.log("num_supera: "+item2r ["num_supera"]);*/
                            graficaIluminacionIncidencia(2,k,item2r["num_ptos_evalua"],item2r["num_supera"],item2r["num_no_supera"],1);//solo se comenta esta linea
                        //}, 700);  
                        cont_time=cont_time+500;
                    });
                    array_incidencia_ref = JSON.stringify(DATA2R);

                    var DATA2I  = [];
                    var TABLA2I = $("#table_ilum_sin_reflex tbody > tr"); //tabla de iluminacion sin incidencia solar
                    var kk=0;
                    if($("#ptos_sin_incidencia_ref").val()!=""/* && $("#ptos_sin_incidencia_ref").val()!="0"*/){
                        TABLA2I.each(function(){    
                            kk++;    
                            item2i = {};
                            item2i["id_conclusion"]=id_conc;
                            item2i["tipo"] = 2;
                            item2i["tipo_incidencia"] = 2;
                            item2i["con_incidencia"] = $("#ptos_incidencia_ref").val();
                            item2i["sin_incidencia"] = $("#ptos_sin_incidencia_ref").val();

                            item2i["id"] = $(this).find("input[id*='idsr1']").val();
                            item2i["medicion"] = $(this).find("input[name*='medicion']").val();
                            item2i["num_ptos_evalua"] = $(this).find("input[name*='num_ptos_evalua']").val();
                            item2i["num_supera"] = $(this).find("input[name*='num_supera']").val();
                            item2i["num_no_supera"] = $(this).find("input[name*='num_no_supera']").val();
                            DATA2I.push(item2i);
                            //setTimeout(function () {
                                //if($("#ptos_sin_incidencia_ref").val()!="")
                                    //savecanvas4(22,kk);
                                console.log("kk: "+kk);
                                graficaIluminacionIncidencia(22,kk,item2i["num_ptos_evalua"],item2i["num_supera"],item2i["num_no_supera"],1);//solo se comenta esta linea
                            //}, 1200);
                            cont_time=cont_time+600;
                        });
                    }

                    array_sinincidencia_ref = JSON.stringify(DATA2I);
                    //console.log("array_sinincidencia_ref: "+array_sinincidencia_ref);
                    var datos='array_incidencia='+array_incidencia_ref+'&array_sinincidencia='+array_sinincidencia_ref;
                    $.ajax({
                        data: datos,
                        type: 'POST',
                        url: base_url+'Nom/submitDetallesConclusion',
                        success: function(data2){
                            //swal("¡Éxito!", "Puntos guardados correctamente", "success");
                            //comentado porque guarda en cambio de datos input
                            /*savecanvas(2,1);
                            savecanvas2(2,2);
                            savecanvas3(2,3);
                            savecanvas4(2,1);*/
                        }
                    });
                }//termina if de iluminacion
                else{
                    $.ajax({
                        data: { id_conc: id_conc,tipo:2 },
                        type: 'POST',
                        url: base_url+'Nom/submitSinReflexion',
                        success: function(data2){
                            //swal("¡Éxito!", "Puntos guardados correctamente", "success");
                        }
                    });
                }
                setTimeout(function () { 
                    toastr.success('Hecho!', 'Conclusiones guardadas correctamente');
                    $("#saveConclusion").attr("disabled",false);
                    $("#modal_conclusion").modal("hide");
                    loadtable();
                }, 5500);
                

            }//success
        });//ajax principal
    }
    if(nom==22){
        var form_register = $('#form_conclusiones22');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input    
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var valid = $("#form_conclusiones22").valid();
        if(valid) {
            $.ajax({
                data: form_register.serialize()+"&nom=22",
                type: 'POST',
                url : base_url+'Nom/submitConclusion',
                async: false,
                beforeSend: function(){
                    $("#saveConclusion22").attr("disabled",true);
                 },
                success: function(data){
                    var id_conc22=data;
                    if($("#total_electrodo_pt").val()!="" && $("#total_electrodo_pt").val()>0){
                        $(".chr_spt").show("slow");
                        graficaNom11(1,1,id_conc22,22);
                    }
                    if($("#total_electrodo_pr").val()!="" && $("#total_electrodo_pr").val()>0){
                        $(".chr_spr").show("slow");
                        graficaNom11(2,1,id_conc22,22);
                    }
                    if($("#total_ptos_continuidad").val()!="" && $("#total_ptos_continuidad").val()>0){
                        $(".chr_ce").show("slow");
                        graficaNom11(3,1,id_conc22,22);
                    }
                    setTimeout(function () { 
                        toastr.success('Hecho!', 'Conclusiones guardadas correctamente');
                        $("#saveConclusion22").attr("disabled",false);
                        $("#modal_conclusion").modal("hide");
                        loadtable();
                    }, 2000);
                }
            }); 
        }
    }
}

function savecanvas(tipo,med){ //ver como mandar para recibir el id del punto guardaro - acá me quedo
    if(tipo==1){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChart";
        }else if(med==2){
            charta="myChart2";
        }else if(med==3){
            charta="myChart3";
        }
        tipo=1;
        tipo_incidencia=1;
    }
    if(tipo==11){ //grafica de iluminacion sin incidecia
        charta="myChartsin";
        tipo=1;
        tipo_incidencia=2;
    }
    if(tipo==2){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChartr";
        }else if(med==2){
            charta="myChartr2";
        }else if(med==3){
            charta="myChartr3";
        }
        tipo=2;
        tipo_incidencia=1;
    }
    if(tipo==22){ //grafica de iluminacion sin incidecia
        charta="myChartsin_ref";
        tipo=2;
        tipo_incidencia=2;
    }

    //setTimeout(function () { 
        var canvasx= document.getElementById(charta);
        var dataURL = canvasx.toDataURL('image/jpg', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/saveCanvasConclusion',
            async:false,
            data: {
                id: $("#id_conclu").val(),
                med:med,
                tipo:tipo,
                tipo_incidencia:tipo_incidencia,
                grafica:dataURL,
                nom:25
            },
            success:function(data){
                //console.log("hecho");
            }
        });
    //}, 1000);
}

function savecanvas2(tipo,med){ //ver como mandar para recibir el id del punto guardaro - acá me quedo
    if(tipo==1){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChart";
        }else if(med==2){
            charta="myChart2";
        }else if(med==3){
            charta="myChart3";
        }
        tipo=1;
        tipo_incidencia=1;
    }
    if(tipo==11){ //grafica de iluminacion sin incidecia
        charta="myChartsin";
        tipo=1;
        tipo_incidencia=2;
    }
    if(tipo==2){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChartr";
        }else if(med==2){
            charta="myChartr2";
        }else if(med==3){
            charta="myChartr3";
        }
        tipo=2;
        tipo_incidencia=1;
    }
    if(tipo==22){ //grafica de iluminacion sin incidecia
        charta="myChartsin_ref";
        tipo=2;
        tipo_incidencia=2;
    }

    //setTimeout(function () { 
        var canvasx= document.getElementById(charta);
        var dataURL = canvasx.toDataURL('image/jpg', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/saveCanvasConclusion25',
            async:false,
            data: {
                id: $("#id_conclu").val(),
                med:med,
                tipo:tipo,
                tipo_incidencia:tipo_incidencia,
                grafica:dataURL,
                nom:25
            },
            success:function(data){
                //console.log("hecho 2");
            }
        });
    //}, 1000);
}

function savecanvas3(tipo,med){ //ver como mandar para recibir el id del punto guardaro - acá me quedo
    if(tipo==1){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChart";
        }else if(med==2){
            charta="myChart2";
        }else if(med==3){
            charta="myChart3";
        }
        tipo=1;
        tipo_incidencia=1;
    }
    if(tipo==11){ //grafica de iluminacion sin incidecia
        charta="myChartsin";
        tipo=1;
        tipo_incidencia=2;
    }
    if(tipo==2){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChartr";
        }else if(med==2){
            charta="myChartr2";
        }else if(med==3){
            charta="myChartr3";
        }
        tipo=2;
        tipo_incidencia=1;
    }
    if(tipo==22){ //grafica de iluminacion sin incidecia
        charta="myChartsin_ref";
        tipo=2;
        tipo_incidencia=2;
    }

    //setTimeout(function () { 
        var canvasx= document.getElementById(charta);
        var dataURL = canvasx.toDataURL('image/jpg', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/saveCanvasConclusion25_2',
            async:false,
            data: {
                id: $("#id_conclu").val(),
                med:med,
                tipo:tipo,
                tipo_incidencia:tipo_incidencia,
                grafica:dataURL,
                nom:25
            },
            success:function(data){
                //console.log("hecho 3");
            }
        });
    //}, 1000);
}

function savecanvas4(tipo,med){ //ver como mandar para recibir el id del punto guardaro - acá me quedo
    if(tipo==1){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChart";
        }else if(med==2){
            charta="myChart2";
        }else if(med==3){
            charta="myChart3";
        }
        tipo=1;
        tipo_incidencia=1;
    }
    if(tipo==11){ //grafica de iluminacion sin incidecia
        charta="myChartsin";
        tipo=1;
        tipo_incidencia=2;
    }
    if(tipo==2){ //grafica de iluminacion con incidecia
        if(med==1){
            charta="myChartr";
        }else if(med==2){
            charta="myChartr2";
        }else if(med==3){
            charta="myChartr3";
        }
        tipo=2;
        tipo_incidencia=1;
    }
    if(tipo==22){ //grafica de iluminacion sin incidecia
        charta="myChartsin_ref";
        tipo=2;
        tipo_incidencia=2;
    }

    //setTimeout(function () { 
        var canvasx= document.getElementById(charta);
        var dataURL = canvasx.toDataURL('image/jpg', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/saveCanvasConclusion25_3',
            async:false,
            data: {
                id: $("#id_conclu").val(),
                med:med,
                tipo:tipo,
                tipo_incidencia:tipo_incidencia,
                grafica:dataURL,
                nom:25
            },
            success:function(data){
                //console.log("hecho 4");
            }
        });
    //}, 1000);
}

function intro(nom,id,tipo){
    //console.log("tipo: "+tipo);
    $("#modal_intro").modal();
    $("#nom").val(nom);
    $("#id_nom").val(id);
    $("#tipo_txt").val(tipo);
    if(tipo==1){
        $(".title_modaltxt").html("Conclusiones de la evaluación");
        $("#subtitle_txt").html("Texto de conclusión");
    }else if(tipo==2){
        $(".title_modaltxt").html("Descripción de las condiciones de operación");
        $("#subtitle_txt").html("Texto de la descripción");
    }
    else if(tipo==3){
        $(".title_modaltxt").html("Criterios utilizados para seleccionar el método de evaluación");
        $("#subtitle_txt").html("Texto de los criterios");
    }
    else if(tipo==4){
        $(".title_modaltxt").html("Descripción del proceso de fabricación de centro de trabajo evaluado");
        $("#subtitle_txt").html("Texto del proceso de fabricación");
    }else if(tipo==5){
        $(".title_modaltxt").html("Eventualidades descriptivas");
        $("#subtitle_txt").html("Texto de las eventualidades descriptivas");
    }else if(tipo==6){
        $(".title_modaltxt").html("Adiciones, desviaciones o exclusiones del método");
        $("#subtitle_txt").html("Texto de las adiciones, desviaciones o exclusiones del método");
    }
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getIntro',
        data: { id_nom: id, tipo:tipo, nom:nom },
        async: false,
        success:function(data){
            //console.log("data: "+data);
            $("#cont_txt_area").html("");
            $("#cont_txt_area").html('<textarea id="intro" class="form-control toupper" rows="3">'+data+'</textarea>');
            $('#intro').richText({
                imageUpload: true,
                fileUpload: false,
                videoEmbed: false,
                urls: false,
                code: false
            });
            //$("#intro").text(data);
        }
    });
}

function loadtable(){
    var id_cliente=$("#id_cliente option:selected").val();
    if(id_cliente==undefined){
        id_cliente=0;
    }
	table = $('#table_nom').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "ajax": {
            url: base_url+"Nom/getlistnom",
            type: "post",
            "data":{
                personal:0, norma:$("#norma option:selected").val(),
                id_cliente:id_cliente, fi:$("#Fecha1").val(), ff:$("#Fecha2").val() 
            },
        },
        "columns": [
            {"data": "idnom"},
            {"data": "idordenes"},
            {"data": "servicio"},
            {"data": "cliente"},
            {"data": null,
                 render:function(data,type,row){
                    //var html='<div style="width:100px">';
                    //console.log("row.num_informe: "+row.num_informe);
                    //console.log("row.num_informe_rec: "+row.num_informe_rec);
                    var info = row.num_informe; // info de nom
                    var html='';
                    if(info=="" || info==null)
                        info = row.num_informe_rec;

                    html+='<input onchange="ingresarNumero('+row.idnom+',this.value)" type="text" id="informe_'+row.idnom+'" value="'+info+'">';
                    return html;
                }
            },
            //{"data": "fecha"},
            {"data": null,
                render:function(data,type,row){
                    if(row.norma!=15)
                        return row.fecha;
                    else
                        return row.reg;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    //var html='<div style="width:100px">';
                    var html='';
                    if(row.norma==11){
                        html+='<a title="Editar" href="'+base_url+'Nom/add'+row.norma+'/'+row.idordenes+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a> ';
                        html+='<a target="_blank" title="Ver Resultados" href="'+base_url+'Nom/add11view/'+row.idordenes+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-file"></i></a>';
                        html+=' <button onclick="conclusionesGraficas('+row.idnom+',11)" title="Ingresar Conclusión" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';                 
                        html+=' <button onclick="intro(11,'+row.idnom+',2)" title="Ingresar informe descriptivo de condiciones de operación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="intro(11,'+row.idnom+',3)" title="Ingresar Criterios utilizados para el método de evaluación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="insertPlanosNom11('+row.idnom+')" title="Insertar Planos" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="insertFichaTecnica('+row.idnom+')" title="Insertar Ficha Técnica de PPA" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="intro(11,'+row.idnom+',4)" title="Ingresar Descripcion del proceso de fabricación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="insertPrograma11('+row.idnom+')" title="Insertar Programa de Mantenimiento" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <a title="Generar PDF" href="'+base_url+'Nom/nompdfview11/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                        html+=' <a title="Generar Portada" onclick="portada('+row.idnom+','+row.idordenes+','+row.id_chs+',11)" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                    }else if(row.norma==25){
                        html+='<a title="Editar" href="'+base_url+'Nom/add'+row.norma+'/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a> ';
                        html+='<button title="Resolución del Equipo" onclick="modal_reso_equipo('+row.idnom+')" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-calculator fa-fw"></i></button> '; 
                        html+='<a target="_blank" title="Ver Resultados" href="'+base_url+'Nom/nom25view/'+row.idnom+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-file"></i></a>';
                        //html+=' <button onclick="num_info('+row.idnom+')" title="Asignar Número de Informe" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-tag"></i></button>';
                        html+=' <button onclick="conclusionesGraficas('+row.idnom+',25)" title="Ingresar Conclusión" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        //html+=' <button onclick="intro(25,'+row.idnom+',2)" title="Ingresar Descripción de condiciones de operación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        //html+=' <button onclick="intro(25,'+row.idnom+',3)" title="Ingresar Criterios utilizados para el método de evaluación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';

                        html+=' <button onclick="insertImgDescPro('+row.idnom+')" title="Ingresar Descripcion del proceso de fabricación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="insertPlanos('+row.idnom+')" title="Insertar Planos" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="insertPlanosLumi('+row.idnom+')" title="Insertar Planos de luminarias" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="insertPrograma('+row.idnom+')" title="Insertar Programa de Mantenimiento" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <a title="Generar PDF" href="'+base_url+'Nom/nompdfview/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                        html+=' <a title="Generar Portada" onclick="portada('+row.idnom+','+row.idordenes+','+row.id_chs+',25)" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                    }else if(row.norma==22){
                        html+='<a title="Editar" href="'+base_url+'Nom/add'+row.norma+'/'+row.idordenes+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a> ';
                        html+='<a target="_blank" title="Ver Resultados" href="'+base_url+'Nom/add22view/'+row.idordenes+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-file"></i></a>';
                        html+=' <button onclick="conclusionesGraficas('+row.idnom+',22)" title="Ingresar Conclusión" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';                 
                        html+=' <button onclick="insertPlanosNom22('+row.idnom+')" title="Insertar Planos" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <a title="Generar PDF" href="'+base_url+'Nom/nompdfview22/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                        html+=' <a title="Generar Portada" onclick="portada('+row.idnom+','+row.idordenes+','+row.id_chs+',22)" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                    }else if(row.norma==81){
                        html+='<a title="Editar" href="'+base_url+'Nom/add'+row.norma+'/'+row.idordenes+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a> ';
                        html+='<a target="_blank" title="Ver Resultados" href="'+base_url+'Nom/add81view/'+row.idordenes+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-file"></i></a>';
                        html+=' <button onclick="intro(81,'+row.idnom+',2)" title="Ingresar informe descriptivo de condiciones de operación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="intro(81,'+row.idnom+',5)" title="Ingresar eventualidades descriptivas" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="intro(81,'+row.idnom+',6)" title="Ingresar adiciones, desviaciones o exclusiones" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="insertPlanosNom81('+row.idnom+')" title="Insertar Planos" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <a title="Generar PDF" href="'+base_url+'Nom/nompdfview81/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                        html+=' <a title="Generar Portada" onclick="portada('+row.idnom+','+row.idordenes+','+row.id_chs+',81)" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                    }
                    else if(row.norma==15){
                        html+='<a title="Editar" href="'+base_url+'Nom/add'+row.norma+'/'+row.idordenes+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-edit"></i></a> ';
                        html+='<a target="_blank" title="Ver Resultados" href="'+base_url+'Nom/add15view/'+row.idordenes+'/'+row.id_chs+'" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="ft-file"></i></a>';
                        //html+=' <button onclick="intro(15,'+row.idnom+',4)" title="Ingresar descripción de fabricación del centro de trabajo evaluado" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-file-text-o"></i></button>';
                        html+=' <button onclick="insertImgDescPro15('+row.idnom+')" title="Ingresar Descripcion del proceso de fabricación" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <button onclick="insertPlanosNom15('+row.idnom+')" title="Insertar Planos" type="button" class="btn btn-sm btn-icon btn-success"><i class="fa fa-picture-o"></i></button>';
                        html+=' <a title="Generar PDF" href="'+base_url+'Nom/nompdfview15/'+row.idnom+'/'+row.idordenes+'/'+row.id_chs+'" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                        html+=' <a title="Generar Portada" onclick="portada('+row.idnom+','+row.idordenes+','+row.id_chs+',15)" target="_blank" type="button" class="btn btn-sm btn-icon btn-success edit"><i class="fa fa-file-pdf-o"></i></a>';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],    
    });
}

function portada(idnom,idordenes,id_chs,nom) {
    window.open(base_url+"Nom/portadaNoms/"+idnom+"/"+idordenes+"/"+id_chs+"/"+nom, 'imprimir',
                'width=700,height=800');
}

function cambiaTipoSelect(tipo_sel){
    if(tipo_sel==0){ //imagen
        $('.tipoimagetext').hide();
        $('#cont_imgs_text').hide();
        $("#tipoimagetext").val(1);
        $("#cont_imgs_desc").show();
    }else{
        $('.tipoimagetext').show();
        $('#cont_imgs_text').show();
        $("#tipoimagetext").val(0);
        $("#cont_imgs_desc").hide();
    }
}

function ingresarNumero(id,val){
    //console.log(val);
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de asignar el número de informe?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Nom/setNumOrden",
                    data: {
                        num:val, idnom:id
                    },
                    success:function(response){  
                        // notificacion de correcta asignacion
                        toastr.success('Hecho!', 'Número de informe asignado correctamente');
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function insertPlanos(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(3);
    $("#cont_tipo_plano11").hide();
    $('.tipoimagetext').hide();
    $('#cont_imgs_text').hide();
    $("#tipoimagetext").val(0);
    $("#cont_imgs_desc").show();

    cambiaTipoSelect(0);
    tipoimagetext(0);
    
    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#subtitle_imgs").html("Plano de ubicación de los puntos de medición");
    //$('.tipoimagetext').hide();
    //$('.tipoimagetext').hide();
    inputFileCharge(25);
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/2',
        data: { id_nom: id, nom:25 },
        async: false,
        success:function(data2){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data2);
            //$(".inputFile").fileinput({ });
        }
    });
}

function insertPlanosLumi(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(4);
    $("#cont_tipo_plano11").hide();
    $('.tipoimagetext').hide();
    $('#cont_imgs_text').hide();
    $("#tipoimagetext").val(0);
    $("#cont_imgs_desc").show();

    cambiaTipoSelect(0);
    tipoimagetext(0);

    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#subtitle_imgs").html("Plano de ubicación de las luminarias");
    inputFileCharge(25);
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/21',
        data: { id_nom: id, nom:25 },
        async: false,
        success:function(data2){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data2);
        }
    });
}

/* ***************************** */
function getPlanosNom(tipoa,tipoplano=0,nom){
    $("#cont_imgs_desc").html("");
    id=$("#id_nom_img_desc").val();
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/'+tipoa,
        data: { id_nom: id, nom:nom, tipoplano:tipoplano },
        async: false,
        success:function(data2){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data2);
            //$(".inputFile").fileinput({ });
        }
    });
}

function insertPlanosNom22(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(2);
    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano15").hide();
    inputFileCharge(22,0);
    getPlanosNom(2,0,22);
}

function insertPlanosNom81(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(2);
    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#subtitle_imgs").html("Planos");
    $("#cont_tipo_plano81").show("slow");
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano15").hide();
    tipo_plano_g = $("#tipo_plano81 option:selected").val();
    inputFileCharge(81,$("#tipo_plano81 option:selected").val());
    getPlanosNom(2,$("#tipo_plano81 option:selected").val(),81);
}

function insertPlanosNom11(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(2);
    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#cont_tipo_plano11").show("slow");
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano15").hide();
    inputFileCharge(11,$("#tipo_plano11 option:selected").val());
    getPlanosNom(2,$("#tipo_plano11 option:selected").val(),11);
}

function insertPlanosNom15(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(2);
    $(".txttitle_imgs").html("Planos: Anexo II");
    $("#subtitle_imgs").html("Planos");
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano15").show("slow");
    tipo_plano_g = $("#tipo_plano15 option:selected").val();
    inputFileCharge(15,$("#tipo_plano15 option:selected").val());
    getPlanosNom(2,$("#tipo_plano15 option:selected").val(),15);
}

function insertFichaTecnica(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(3);
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano15").hide();
    $(".txttitle_imgs").html("Ficha Técnica: Anexo IV");
    $("#subtitle_imgs").html("Ficha Técnica de la protección personal auditiva");
    inputFileCharge(11,0);
    getPlanosNom(3,0,11);
}

function insertPrograma11(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(4);
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano15").hide();
    $('.tipoimagetext').hide();
    $("#cont_imgs_desc").show();
    $(".txttitle_imgs").html("Programa: Anexo VI");
    $("#subtitle_imgs").html("Programa de mantenimiento a maquinaria y equipo generador de ruido");
    inputFileCharge(11,0);
    getPlanosNom(4,0,11);
}

/* *******************************/

function guardarIntro(){
    $.ajax({
        type:'POST',
        url: base_url+'Nom/submitIntro',
        data: { intro:$("#intro").val(), id_nom:$("#id_nom").val(), tipo:$("#tipo_txt").val(), nom:$("#nom").val() },
        async: false,
        beforeSend: function(){
            $("#save").attr("disabled",true);
        },
        success:function(data3){
            toastr.success('Hecho!', 'Guardado Correctamente');
            $("#modal_intro").modal("hide");
            loadtable();
            $("#save").attr("disabled",false);
        }
    });
}

function insertImgDescPro(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(1);
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano15").hide();

    //cambiaTipoSelect(1);
    tipoimagetext(1);
    $(".txttitle_imgs").html("Descripción: Anexo IV");
    $("#subtitle_imgs").html("Descripción del proceso de fabricación del centro de trabajo evaluado");
    inputFileCharge(25);
    $('.tipoimagetext').show();
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/1',
        data: { id_nom: id, nom:25 },
        async: false,
        success:function(data4){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data4);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImagesverifi/1',
        data: { id_nom: id, nom:25 },
        async: false,
        success:function(response){
            //console.log(response);
            var array = $.parseJSON(response);
            if(array.tipotext==1){
                $('#tipoimagetext').val(1).change();
                $('#text_info').val(array.datatext);
            }else{
                $('#tipoimagetext').val(0).change();
            }
        }
    });
}

function insertImgDescPro15(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(1);
    $("#cont_tipo_plano11").hide()
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano15").hide();
    $(".txttitle_imgs").html("Descripción: Anexo IV");
    $("#subtitle_imgs").html("Descripción del proceso de fabricación del centro de trabajo evaluado");
    inputFileCharge(15);
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/1',
        data: { id_nom: id, nom:15 },
        async: false,
        success:function(data4){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data4);
        }
    });
}
function insertPrograma(id){
    $("#modal_img_desc").modal();
    $("#id_nom_img_desc").val(id);
    $("#tipo_img_carga").val(2);
    $("#cont_tipo_plano11").hide();
    $("#cont_tipo_plano81").hide();
    $("#cont_tipo_plano15").hide();

    $('.tipoimagetext').hide();
    $('#cont_imgs_text').hide();
    $("#tipoimagetext").val(0);
    $("#cont_imgs_desc").show();
    cambiaTipoSelect(0);
    tipoimagetext(0);

    $(".txttitle_imgs").html("Programa: Anexo V");
    $("#subtitle_imgs").html("Programa de mantenimiento a luminarias");
    inputFileCharge(25);
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImages/3',
        data: { id_nom: id, nom:25 },
        async: false,
        success:function(data5){
            //console.log("data: "+data);
            $("#cont_imgs_desc").html(data5);
        }
    });
}

function num_info(id){
    $("#modal_info").modal();
    $("#id_nom_info").val(id);
}

function subirarchivo(){
    $('#inputFile').fileinput('upload');
}

/*function exportWord(id,norm){
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImgsDoc',
        data: { id_nom: id },
        async: false,
        success:function(data){
            var array = $.parseJSON(data);
            var imgs = array.html;
            var txt = array.txt_intro;
            var html2 = array.html2;
            var html3 = array.html3;
            //var table1 = array.table_generado1;
            //var table2 = array.table_generado2;
            //var grafica = array.grafica;
            //console.log("img_escan: "+imgs);
            $("#img_escan").html(imgs);
            $("#cont_conclu").html(txt);
            $("#img_result").html("<p>Resultados</p>" +html2+html3);

            //$("#img_tab2").attr("src",table2);
            //$("#img_grap").attr("src",grafica);

            $("#name_tec").html($("#btn_export").data("tec_"+id+""));
            $("#name_cli").html($("#btn_export").data("cli_"+id+""));
            $("#emp_head").html($("#btn_export").data("cli_"+id+""));

            $("#dir1").html($("#btn_export").data("dir1_"+id+""));
            $("#dir2").html("Col." +$("#btn_export").data("dir2_"+id+""));
            $("#dir3").html($("#btn_export").data("dir3_"+id+""));
            $("#dircp").html("CP. "+$("#btn_export").data("cp_"+id+""));

            $("#num_norma").val(norm);

            var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
            "xmlns:w='urn:schemas-microsoft-com:office:word' "+
            "xmlns='http://www.w3.org/TR/REC-html40'>"+
            "<head><meta charset='utf-8'><title>Documento generado con información del estudio aplicado</title></head><body>";
           var footer = "</body></html>";
           var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
           
           var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
           var fileDownload = document.createElement("a");
           document.body.appendChild(fileDownload);
           fileDownload.href = source;
           fileDownload.download = 'resultados_nom.doc';
           fileDownload.click();
           document.body.removeChild(fileDownload);
        }
    });
}*/
function modal_reso_equipo(idnom){
    $('#id_nom_re').val(idnom);
    $('#modal_reso_equipo').modal();
    var todos_ptos=2; var id_pto=0;
    
    if($("#todos_ptos").is(":checked")==true){
        todos_ptos=1;
        $("#cont_ptos").hide("slow");
    }else{
        id_pto=$("#id_punto option:selected").val();
        $.ajax({
            type:'POST',
            url: base_url+'Nom/getPuntosNom',
            data: { 
                idnom: $('#id_nom_re').val()
            },
            async: false,
            success:function(data){
                $('#id_punto').html(data);
            }
        });
    }

    $.ajax({
            type:'POST',
            url: base_url+'Nom/get_resolucion_e',
            data: { 
                idnom: idnom, todos:todos_ptos, id_pto:id_pto,
            },
            async: false,
            success:function(data){
                var array = $.parseJSON(data);
                if(array.datosnum>0){
                    $.each(array.datos, function(index, item) {
                        $('input:radio[name=escala1][value='+item.escala1+']').prop('checked',true);
                        $('input:radio[name=escala2][value='+item.escala2+']').prop('checked',true);
                        $('input:radio[name=escala3][value='+item.escala3+']').prop('checked',true);

                        $('#tipo_dis1').val(item.distribucion1);
                        $('#tipo_dis2').val(item.distribucion1);
                        $('#tipo_dis3').val(item.distribucion1);
                        $('#h1_coef_sensibilidad').val(item.sencibilidad1);
                        $('#h2_coef_sensibilidad').val(item.sencibilidad2);
                        $('#h3_coef_sensibilidad').val(item.sencibilidad3);
                        calcularre();
                    });
                }
            }
        });
}
function calcularre(){
    var escala1 = $('input:radio[name=escala1]:checked').val();
    if(escala1>0){
        switch (escala1) {
          case '1':
            var escala1_val=0.01;
            break;
          case '2':
            var escala1_val=0.1;
            break;
          default:
            var escala1_val=1;
        }
        $('.h1_resolicion').html(escala1_val);
        var u_estandar1=parseFloat(escala1_val)/Math.sqrt(12);
        $('.h1_u_estandar').html(u_estandar1.toFixed(4));

        var h1_contribuccion_rq=parseFloat($('#h1_coef_sensibilidad').val())*parseFloat(u_estandar1);
        $('.h1_contribuccion_rq').html(h1_contribuccion_rq.toFixed(4));

        var h1_uy2_rq=Math.pow(h1_contribuccion_rq, 2);
        $('.h1_uy2_rq').html(h1_uy2_rq.toFixed(4));


    }
    var escala2 = $('input:radio[name=escala2]:checked').val();
    if(escala2>0){
        switch (escala2) {
          case '1':
            var escala2_val=0.01;
            break;
          case '2':
            var escala2_val=0.1;
            break;
          default:
            var escala2_val=1;
        }
        $('.h2_resolicion').html(escala2_val);
        var u_estandar2=parseFloat(escala2_val)/Math.sqrt(12);
        $('.h2_u_estandar').html(u_estandar2.toFixed(4));

        var h2_contribuccion_rq=parseFloat($('#h2_coef_sensibilidad').val())*parseFloat(u_estandar2);
        $('.h2_contribuccion_rq').html(h2_contribuccion_rq.toFixed(4));

        var h2_uy2_rq=Math.pow(h2_contribuccion_rq, 2);
        $('.h2_uy2_rq').html(h2_uy2_rq.toFixed(4));
    }
    var escala3 = $('input:radio[name=escala3]:checked').val();
    if(escala3>0){
        switch (escala3) {
          case '1':
            var escala3_val=0.01;
            break;
          case '2':
            var escala3_val=0.1;
            break;
          default:
            var escala3_val=1;
        }
        $('.h3_resolicion').html(escala3_val);
        var u_estandar3=parseFloat(escala3_val)/Math.sqrt(12);
        $('.h3_u_estandar').html(u_estandar3.toFixed(4));

        var h3_contribuccion_rq=parseFloat($('#h3_coef_sensibilidad').val())*parseFloat(u_estandar3);
        $('.h3_contribuccion_rq').html(h3_contribuccion_rq.toFixed(4));

        var h3_uy2_rq=Math.pow(h3_contribuccion_rq, 2);
        $('.h3_uy2_rq').html(h3_uy2_rq.toFixed(4));
    }
}
function tipoimagetext(tipo){
    //var tipo=$('#tipoimagetext').val();
    console.log(tipo);
    if(tipo==1){
        $('#cont_inputFile').hide();
        $('#cont_imgs_desc').hide();
        $('#cont_imgs_text').show();
        funcionckeditor()
    }else{
        $('#cont_inputFile').show();
        $('#cont_imgs_desc').show();
         $('#cont_imgs_text').hide();
    }
}
function funcionckeditor(){
    $('#text_info').ckeditor();
    
}
function saveinfo(nom){
    $.ajax({
        type:'POST',
        url: base_url+'Nom/getImagestext/1',
        data: { 
            id_nom: $('#id_nom_img_desc').val(), 
            nom:25,
            text:$('#text_info').val(),
        },
        async: false,
        success:function(response){
            $("#modal_img_desc").modal("hide");
            swal("¡Éxito!", "Guardado correctamente", "success");
            loadtable();  
        }
    });
}