var base_url =$('#base_url').val();
$(document).ready(function($) {
    verificaCertificado();
    
    $("#inputFile").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png","webp"],
        browseLabel: 'Seleccionar Certificado',
        showDrag: true,
        dragTitle: 'Arrastra o selecciona certificado',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'webp': '<i class="fa fa-file-photo-o text-warning"></i>',
        },
    });
    $(".file-drop-zone-title").html("Arrastra o selecciona certificado");

    $("#inputFile").on("filepredelete", function(jqXHR) {
        verificaCertificado();
    });
    $("#kv-file-remove").on("click", function(){
        verificaCertificado();
    });

    $("#norma").on("change", function(){
        verificaCertificado();
    });

    var tama_perm = false;
    $('#inputFile').change( function() {
        filezise = this.files[0].size;                     
        if(filezise > 2548000) { // 512000 bytes = 500 Kb
            //console.log($('.img')[0].files[0].size);
            $(this).val('');
            swal("Error!", "El archivo supera el límite de peso permitido (2.5 mb)", "error");
        }else { //ok
            tama_perm = true; 
            var archivo = this.value;
            var name = this.id;

            extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
            extensiones_permitidas = new Array(".jpeg",".png",".jpg",".webp");
            permitida = false;
            if($('#'+name)[0].files.length > 0) {
                for (var i = 0; i < extensiones_permitidas.length; i++) {
                    if (extensiones_permitidas[i] == extension) {
                        permitida = true;
                    break;
                    }
                }  
                if(permitida==true && tama_perm==true){
                    //console.log("tamaño permitido");
                    var inputFileImage = document.getElementById(name);
                    name=document.getElementById('inputFile').files[0].name;
                    $("#name_cert").val(name);
                    var file = inputFileImage.files[0];
                    var data = new FormData();
                    //id_docs = $("#id").val();
                    data.append('foto',file);
                    data.append('norma',$("#norma option:selected").val());
                    $.ajax({
                      url:base_url+'Configuraciones/uploadImg/2',
                      type:'POST',
                      contentType:false,
                      data:data,
                      processData:false,
                      cache:false,
                      success: function(data) {
                        var array = $.parseJSON(data);
                        //console.log(array.ok);
                        if (array.ok=true) {
                            id_docs = array.id;
                            swal("¡Éxito!", "Cargado correctamente", "success");
                            $("#acept_cert").hide();
                            $('#inputFile').fileinput('reset');
                            $('#inputFile').val('');
                            verificaCertificado();
                        }else{
                          //swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        if(array.id==0) {
                            //swal("Éxito", "Imagen cargada Correctamente", "success");
                        }
                        $("#acept_cert").show();
                      },
                      error: function(jqXHR, textStatus, errorThrown) {
                          var data = JSON.parse(jqXHR.responseText);
                      }
                    });
                }else if(permitida==false){
                    swal("Error!", "Tipo de archivo no permitido", "error");
                } 
            }else{
              swal("Error!", "No se a selecionado un archivo", "error");
            } 
        }
    });
    if($("#id").val()>0){
        verificaCertificado();
    }
    $('#acept_cert').click( function() {
        swal("¡Éxito!", "Cargado correctamente", "success");
        $("#acept_cert").hide();
        $('#inputFile').fileinput('reset');
        $('#inputFile').val('');
        verificaCertificado();
    });

});

function verificaCertificado(){
    $.ajax({ // 
        type: "POST",
        url: base_url+"Configuraciones/verificaCert",
        data: { "norma": $("#norma option:selected").val(),tipo:2 },
        async: false,
        success: function (result) {
            $("#cont_cert").html(result);       
        }
    });
}

