<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);

        $this->load->model('Catalogos_model', 'model');
        $this->load->model('Servicios_model', 'servimodel');
        
    }

    public function index() {
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/servicios',$data);
        $this->load->view('footer');
    }

    public function alta_servicio() {
        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $id_emp = $this->session->userdata("empresa");  
        
        $data["familias"]=$this->model->getCatalogo("familias");
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_servicio',$data);
        $this->load->view('footer');
    }

     public function edicion_servicio($id) {
        $id_emp = $this->session->userdata("empresa");
        $from="servicios_ahisa";

        $data["sucursales"]=$this->model->getCatalogo("sucursales");
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["familias"]=$this->model->getCatalogo("familias");
        
        $data['servicio'] = $this->model->getItemCatalogo($from, $id);
        $data["documentos"]=$this->model->getCatalogo("documentos");
        $data["docs"]=$this->model->getDocsServicio(array("id_servicio"=>$id,"estatus"=>1),$id_emp);
        $data['precios_sucursal']= $this->servimodel->getPreciosSucursal($id);
        $data['precios_puntos']= $this->servimodel->getPreciosPuntos($id);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('catalogos/form_servicio', $data);
        $this->load->view('footer');
    }

    public function updateDescrip(){
        $id=$this->input->post('id');
        $descrip=$this->input->post('descrip');
        $id_emp = $this->session->userdata("empresa");

        //$data['descrip'] = $descrip;
        //log_message('error', 'id'.$id);
        //log_message('error', 'descrip'.$descrip);
        $descrip = str_replace('<div>', '', $descrip);
        $descrip = str_replace('</div>', '', $descrip);
        $descrip=preg_replace('/\n+/', ' <br>', $descrip);
        $result = $this->model->updateDescrip($descrip,$id,$id_emp);
    }

    public function insertUpdateServicio(){
        $data=$this->input->post();
        $docs= $this->input->post("documentos");
        $id_emp = $this->session->userdata("empresa");
        if(isset($data["precio"]))
            $precio_unico=$data["precio"]; 
        if(isset($data["sucursal_precio"]))
            $sucursal_precio=$data["sucursal_precio"];

        if(isset($data["por_puntos"]) && $data["por_puntos"]==1){
            if($id_emp==1 || $id_emp==2 || $id_emp==3 || $id_emp==6){
                /*$rango_inicio=$data["rango_inicio"]; 
                $rango_fin=$data["rango_fin"]; 
                $precio_rango=$data["precio_rango"]; 
                $sucursal_rango=$data["sucursal_rango"];*/
                $data["precio"]=0;
            }
        }

        unset($data["rango_inicio"]); unset($data["rango_fin"]); unset($data["precio_rango"]); unset($data["0"]);
        unset($data["precio"]); unset($data["sucursal_precio"]); unset($data["sucursal_rango"]);
        
        $from="servicios_ahisa";
        if (!isset($data['id'])) {
            //insert
            $this->model->insertToCatalogo($data,$from);
            $result=$this->db->insert_id();
            $id=$result;
        }
        else{
            //update
            $id=$data['id'];
            unset($data['id']);
            //print_r($data); die;
            $result = $this->model->updateCatalogo($data,$id,$from);
            $this->servimodel->deletePreciosServicio($id);
            
        }
        echo $id;
    }

    public function submit_docs_serv(){
        $datos = $this->input->post('data');
        $id_emp = $this->session->userdata("empresa");
        $from="servicios_documentos_ahisa";
        
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            //$id = $DATA[$i]->id;
            $data['id_servicio'] = $DATA[$i]->id_servicio;
            $data['id_documento'] = $DATA[$i]->id_documento;
            $data['id']= $DATA[$i]->id;
            $id = $DATA[$i]->id;

            if($DATA[$i]->id==0){
                $temp=array("id_servicio"=>$DATA[$i]->id_servicio,"id_documento"=>$DATA[$i]->id_documento);
                $id=$this->model->insertToCatalogo($temp,$from);
            }else{
                $temp=array("id_servicio"=>$DATA[$i]->id_servicio,"id_documento"=>$DATA[$i]->id_documento);
                $this->model->updateCatalogon($from,$temp,array("id"=>$id));
            }
            echo $id;          
        }
    }

    public function eliminar_servicio($id){
        $id_emp = $this->session->userdata("empresa");
        $from="servicios_ahisa";
        
        //if($this->session->userdata("id_usuario")>1){
           echo $this->model->updateCatalogo(array("status"=>0),$id,$from);
        /*}
        else{
            echo 0;
        }*/
        
    }
   
    

}
