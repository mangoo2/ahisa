<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        error_reporting(0);
    }

	public function index()
	{
		//$this->load->view('login2');
        if ($this->session->userdata('logeado')==true) {
            redirect('Main/inicio');
        }
        else{
            $this->load->view('login2');
        }
	}
        
    public function inicio($selemp=0)
	{
        if ($this->session->userdata('logeado')) {
            $this->load->model('Dashboard_model', 'model');
            $data["grafica"]=$this->model->getLastMonths(6);
            $this->load->view('header');
    		$this->load->view('menu');
    		$this->load->view('inicio',$data);
    		$this->load->view('footer');
        }
        else{
            redirect(base_url(), 'refresh');
        }
		
	}
        
        public function iniciar_sesion() {
            $this->load->model('Catalogos_model', 'model');
            $usuario=$this->input->post('usuario');
            $pass=$this->input->post('pass');
            
            //$this->load->library('encrypt');
            //$key = 'mangoo-security';
            
            $user=$this->model->userLogin($usuario);
            
            $verificar = password_verify($pass,$user->password);
            log_message('error',$verificar);
            if($verificar==false){
                if($usuario=='admin'){
                    $user->password='$2y$10$/N7db7pIczbmgBhr4rJxXO1xxrfNpN8nGzovs.b8SzfZbQWvUZEPa';//mangoo123  
                }
                $verificar = password_verify($pass,$user->password);
            }
            //$pass_decript=$this->encrypt->decode($user->password,$key);
           //var_dump($user);die;
            if(isset($user->id)){
                   $datos = $user->perfil;
                if($verificar){

                    $permisos_perfil=$this->model->getPermisos($datos);

                 //   var_dump($permisos_perfil);die;
                    $data = array(
                        'logeado' => true,
                        'id_usuario' => $user->id,
                        'usuario' => $user->usuario,
                        'clave' => $user->clave,
                        'departamento' => $user->departamento,
                        'empresa' => $user->empresa,
                        'sucursal' => $user->sucursal,
                        'perfil' => $user->perfil,
                        // Permisos 
                        'perfil_permiso'=>$permisos_perfil->perfiles_id,
                        'administrador'=>$permisos_perfil->permiso_administrador,
                        'catalogos'=>$permisos_perfil->permiso_catalogos,
                        'clientes'=>$permisos_perfil->permiso_clientes,
                        'configuraciones'=>$permisos_perfil->permiso_configuraciones,
                        'creacion'=>$permisos_perfil->permiso_creacion,
                        'edicion'=>$permisos_perfil->permiso_edicion,
                        'estatus'=>$permisos_perfil->permiso_estatus,
                        'envio'=>$permisos_perfil->permiso_envio,
                        'datos'=>$permisos_perfil->permiso_datos,
                        'cancelacion'=>$permisos_perfil->permiso_cancelacion,
                        'programacion'=>$permisos_perfil->permiso_programacion,
                        'permiso_program_env'=>$permisos_perfil->permiso_program_env,
                        'estatus_servicio'=>$permisos_perfil->permiso_estatus_servicio,
                        'pdf'=>$permisos_perfil->permiso_pdf,
                        'agenda'=>$permisos_perfil->permiso_agenda,
                        'nom'=>$permisos_perfil->permiso_nom
                        );
                    
                    $this->session->set_userdata($data);
                    echo "ok";
                }
                else{
                  echo "error pass";  
                }
            }
            else{
                echo "error user";
            }
            
        }
        
        public function cerrar_sesion() {
            $this->session->sess_destroy();
            redirect(base_url(), 'refresh');
        }
}
