<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        /*$logueo = $this->session->userdata('logeado');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }*/
        
        $this->load->model('Ordenes_model', 'model');
        $this->load->model('Catalogos_model');
        $this->load->model('ModeloCatalogos');
    }

    public function index() {
        $data["proveedores"]=$this->model->getCatalogo("proveedores");
        $data["usu"]=$this->Catalogos_model->getUsuarios();
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('ordenes/ordenes',$data);
        $this->load->view('footer');
        $this->load->view('ordenes/lista_ordenesjs');
    }
    
    public function orden($id){
        $data["orden"]=$this->model->getOrden($id);
        $data["cliente"]=$this->model->getCliente($data["orden"]->cliente_id);
        $data["servicios"]=$this->model->getServiciosCotizacion($data["orden"]->cotizacion_id);
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('ordenes/form_orden',$data);
        $this->load->view('footer');
    }
	
	public function update($id){
		$data=$this->input->post();
        //$data=$this->session->userdata("id_usuario");
		echo $this->model->updateOrden($id,$data);
	}
    
    
    public function getData_ordenes() { //tabla de ordenes de trabajo
        $params=$this->input->post();
        $ordenes = $this->model->getOrdenes($params);
        $totalRecords=$this->model->getTotalOrdenes($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ordenes->result(),
            "query"           =>$this->db->last_query()   
            );
        echo json_encode($json_data);
    }
    
    function ExportOrdenes($fecha1,$fecha2,$estatus,$emp){        
        $data["fecha1"] = $fecha1;
        $data["fecha2"] = $fecha2;
        $data["estatus"] = $estatus;
        $data["emp"] = $emp;

        $data["o"] = $this->Ordenes_model->getOrdenesExcel($fecha1,$fecha2,$estatus,$emp);
        $this->load->view('ordenes/excelReporte',$data);
    }

    public function cambiarEstatus(){
        $id=$this->input->post("id");
        $status=$this->input->post("status");
        $this->db->where("id",$id);
        echo $this->db->update('ordenes', array("status"=>$status));
    }
    
    public function cancelarServicio(){
        $id=$this->input->post("id");
        $this->db->where("id",$id);
        echo $this->db->update('cotizaciones_has_servicios', array("status"=>0));
    }
    
    public function programacion($id,$idcli,$unico){
        $data=$this->input->post();
        $tecnico = $data["tecnico"]; 
        $data["id_tecnico"] = $tecnico;
        $data["unico_tecnico"] = $unico;
        unset($data["tecnico"]);

        /*$this->db->where("cotizacion_id",$id);
        $this->db->update('cotizaciones_has_servicios', $data);*/
        $this->model->editaProgramacion($data,$id);

        //actualizar id_tecnico de orden
        $this->Catalogos_model->updateCatalogon("ordenes",array("id_tecnico"=>$tecnico),array("cotizacion_id"=>$id));

        /*$data["servicio"]=$this->model->getServicio($id);
        //$data["documento"]=$this->model->getDocsFam($id);
        $data["documentos"]=$this->model->getDocsConfig();
        $data["servicios"]=$this->model->getServiciosCotizacion($id);*/
        //$data["tecnico"]=$this->model->getEmailTecnico($data["servicio"]->proveedor);
        //$data["tecnico"]=$this->model->getEmailTecnico($servicio->proveedor);
        //$mail= $this->model->getMail($idcli); //email del cliente
        //obtener email del tecnico

        //print_r($data);        echo $mail; die;
        //echo $this->email_horario($data,$mail,$servicio->correo);
    }

    public function asignaTecnicos(){
        $data = $this->input->post('data');
        $DATA = json_decode($data);
        $tecnico=0;
        for ($i=0; $i<count($DATA); $i++) {
            $arra_cs=array(
                //"servicio"=>$DATA[$i]->servicio,
                //"cliente_orden"=>$DATA[$i]->cliente_orden,
                "hora_inicio" =>$DATA[$i]->hora_inicio,
                "hora_fin"=>$DATA[$i]->hora_fin,
                "fecha_inicio"=>$DATA[$i]->fecha_inicio,
                "fecha_fin"=>$DATA[$i]->fecha_fin,
                "comentarios"=>$DATA[$i]->comentarios,
                "id_tecnico"=>$DATA[$i]->id_tecnico,
                "unico_tecnico"=>$DATA[$i]->unico_tecnico,
            );
            $id=$DATA[$i]->id_chs;
            $tecnico=$DATA[$i]->id_tecnico;
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_servicios',$arra_cs,array('id'=>$id));
        }
    }

    public function getServsOrden(){
        $id=$this->input->post("id");
        $get=$this->model->getServiciosOrden($id);
        $usu=$this->Catalogos_model->getUsuarios();
        $html="";
        foreach($get as $s){
            $html.='
                    <div class="col-md-6 form-group">
                        <label>Servicio: '.$s->nombre.'</label>
                    </div>
                    <table id="table_servs">
                        <tr>
                            <td><input type="hidden" id="id_chs" value="'.$s->id.'"></td>
                            <td><div class="row col-md-12">
                                <label for="tecnico_multiple" id="lable_tec">Técnico a asignar:</label><br>
                                <select id="tecnico_multiple" class="form-control tecnico_'.$s->id.'">
                                    <option selected value="">Elige un técnico</option>';
                                    foreach ($usu as $u) {
                                        $sel="";
                                        if($s->id_tecnico==$u->id) $sel="selected";
                                        $html.='<option '.$sel.' value="'.$u->id.'">'.$u->nombre.'</option>';
                                    }
                                $html.='</select>
                                </div>  
                            </td>
                        </tr>                        
                    </table>';
        }
        echo $html;
    }

    public function getServsRecoTecnico(){
        $cotizacion_id=$this->input->post("cotizacion_id");
        $id_orden=$this->input->post("id_orden"); //id
        $idnom=$this->input->post("idnom"); 
        $idcli=$this->input->post("idcli");
        $get=$this->model->getServiciosOrden($cotizacion_id);
        $html=""; $norm="";
        foreach($get as $s){

            $html.='<div class="col-md-12">
                    <div class="col-md-12 form-group">
                        <label>Servicio: '.$s->nombre.'</label>
                    </div>
                    <div class="col-md-6 form-group">
                        <a href="'.base_url().'Nom/reconocimientoServicio/'.$id_orden.'/'.$idnom.'/'.$idcli.'/'.$s->id.'/" > <h3>Ir a reconocimiento</h3></a>
                    </div>
                </div>';
        }
        echo $html;
    }

    public function getServsTecnico(){
        $cotizacion_id=$this->input->post("cotizacion_id");
        $id_orden=$this->input->post("id_orden");
        $idnom=$this->input->post("idnom");
        $get=$this->model->getServiciosOrden($cotizacion_id);
        

        $html=""; $norm="";
        foreach($get as $s){
            $getnom=$this->Catalogos_model->selectWhere("nom",array("idordenes"=>$id_orden,"id_chs"=>$s->id));
            foreach($getnom as $gn){
                $id_orden=$gn->idordenes;
                $idnom=$gn->idnom;
            }
            //traer norma del servicio de chs
            if($s->norma==1){ 
                $norm="add11";
            }
            if($s->norma==2){
                $norm="add25";
            }
            if($s->norma==3){
                //Nom-033
            }
            if($s->norma==4){
                //Nom-044
            }
            if($s->norma==5){
                //Nom-054
            }
            $html.='<div class="col-md-12">
                    <div class="col-md-12 form-group">
                        <label>Servicio: '.$s->nombre.'</label>
                    </div>
                    <div class="col-md-6 form-group">
                        <a href="'.base_url().'Nom/'.$norm.'/'.$idnom.'/'.$id_orden.'/'.$s->id.'" > <h3>Ir a mediciones</h3></a>
                    </div>
                </div>';
        }
        echo $html;
    }

    public function avisarMailCli(){
        $data=$this->input->post();
        $idcli=$data["id_cli"]; unset($data["id_cli"]);
        $id=$data["servicio"]; unset($data["servicio"]);
        $getemp=$this->Catalogos_model->getItemCatalogo("cotizaciones",$id);

        $data["servicio"]=$this->model->getServicio($id);
        $data["documentos"]=$this->model->getDocsServicio($id);
        $data["servicios"]=$this->model->getServiciosCotizacion($id); //ordenes_model
        $mail= $this->model->getMail($id); //email del cliente
        $this->email_horario($data,$mail,$data["servicio"]->mail_tec);         
    }
    
    public function email_horario($data,$mail,$tec){
        
        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ahisa.mangoo.systems';                     
        $config['smtp_user'] = 'programaciones@ahisa.mangoo.systems';
        $mail_dom = 'programaciones@ahisa.mangoo.systems';
        $config['smtp_pass'] = 'glUNjFEtR@S$';

        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $config['validate'] = true;

        $this->email->initialize($config);
        $this->email->from($mail_dom, 'Programación');
        $this->email->to($mail);
        $this->email->cc($tec);

        $this->email->subject('Programación de servicio');
        $msj=$this->load->view('ordenes/mail', $data, true);
        $this->email->message($msj);
        $this->email->send();
    }
    
    public function verEmpresaOrden(){
        $id=$this->input->post("id");
        $id_emp=$this->input->post("id_emp");
        $getcs=$this->Catalogos_model->getServiciosEmpresa(array("cotizacion_id"=>$id,"cs.status"=>1));
        $cont_serv=0;
        $class="";
        foreach ($getcs as $k) {
            $class="style='font-size:11px; color:white' class='btn btn-warning' style='border-radius:20px' title='Empresa del servicio'";
            echo "<p ".$class."> ".$k->nombre." </p>";
        }
    }

    public function preimpresion($id,$id_cot) {
        $data["id_orden"]=$id;
        $data["id_cot"]=$id_cot;
        $data['orden'] = $this->model->getOrden($id);
        $data["serv"]=$this->model->getServiciosCotizacion($id_cot);
        $data["dets"]=$this->Catalogos_model->getCatalogoRow("detalles_orden_impre",array("id_orden"=>$id));

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('ordenes/preimpresion',$data);
        $this->load->view('footer');
        $this->load->view('ordenes/preimpresionjs');
    }

    function submit_detalleImpresion(){
        $data=$this->input->post();
        $id=$this->input->post("id");
        if($id==0){
            $this->Catalogos_model->Insert('detalles_orden_impre',$data); 
        }else{
            $this->Catalogos_model->updateCatalogo($data,$id,"detalles_orden_impre");
        }
    }

    public function editarServiciosOrden(){ 
        $data = $this->input->post('data');
        $DATA = json_decode($data);
        for ($i=0; $i<count($DATA); $i++) {
            $arra_cs=array(
                "comentarios"=>$DATA[$i]->coments,
            );

            $this->Catalogos_model->updateCatalogo($arra_cs,$DATA[$i]->id_chs,"cotizaciones_has_servicios");
        }
    }

    public function pdfOrden($id,$id_cot){
        $data["id"]=$id;
        $data['orden'] = $this->model->getOrden($id);
        $data["serv"]=$this->model->getServiciosCotizacion($id_cot,0);
        $data["dets"]=$this->Catalogos_model->getCatalogoRow("detalles_orden_impre",array("id_orden"=>$id));
        $this->load->view('reportes/orden',$data);
    }
    
}