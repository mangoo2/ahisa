<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class otro extends CI_Controller {
// Creamos nuestro constructor en este caso para cargar algún modelo
public function __construct() {
parent::__construct();
// $this->load->model('alumno_model');
}
  
public function index(){
$this->load->view('welcome_message');
}
  

// NUESTRO ARCHIVO EN WORD
public function test()
{
    $this->load->library('Phpword');
    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $phpWord->getCompatibility()->setOoxmlVersion(14);
    $phpWord->getCompatibility()->setOoxmlVersion(15);

    $filename = 'introduccion.docx';
    // add style settings for the title and paragraph
    $section = $phpWord->addSection();
    $section->addText('"Learn from yesterday, live for today, hope for tomorrow. '
		. 'The important thing is not to stop questioning." '
		. '(Albert Einstein)');
		  
	$section->addText('Great achievement is usually born of great sacrifice, '
	. 'and is never the result of selfishness. (Napoleon Hill)',
	array('name' => 'Tahoma', 'size' => 10));
	  
	$fontStyleName = 'oneUserDefinedStyle';
	$phpWord->addFontStyle($fontStyleName,
	array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true));
	          
	$section->addText('"The greatest accomplishment is not in never falling, '
	. 'but in rising again after you fall." '
	. '(Vince Lombardi)',
	$fontStyleName);
	  
	$fontStyle = new \PhpOffice\PhpWord\Style\Font();
	$fontStyle->setBold(true);
	$fontStyle->setName('Tahoma');
	$fontStyle->setSize(13);
	$myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
	$myTextElement->setFontStyle($fontStyle);

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save($filename);
    // send results to browser to download
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$filename);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    flush();
    readfile($filename);
    unlink($filename); // deletes the temporary file
    exit;
}
  
}