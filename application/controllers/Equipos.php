<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEquipos');
        $this->load->model('Catalogos_model');
        if($this->session->userdata('logeado')==true){
            
            
        }else{
            redirect('Main'); 
        }
    }

    function index(){
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('equipos/lis');
            $this->load->view('footer');
            $this->load->view('equipos/lisjs');
    }
    function add($id=0){
        $data['idequipo']=$id;
        $data['tipo']=0;
        if($id==0){
            $data['savebuttons']='Guardar';
        }else{
            $data['savebuttons']='Actualizar';
            $data["e"]=$this->Catalogos_model->getCatalogoRow('equipo',array("id"=>$id));
            $data['tipo']=$data["e"]->tipo;
        }
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('equipos/datoscal',$data);
        $this->load->view('footer');
        $this->load->view('equipos/datoscaljs');
    }
    function inserupdate(){
        $params = $this->input->post();
        $id = $params['id'];
        $luxometro = $params['luxometro'];
        $equipo = $params['equipo'];
        $marca = $params['marca'];
        $modelo = $params['modelo'];
        $no_informe_calibracion = $params['no_informe_calibracion'];
        $fecha_calibracion = $params['fecha_calibracion'];
        $tipo = $params['tipo'];
        //$name_cert = $params['certificado'];
        $arrayequipos = $params['arrayequipos'];

        if($id==0){
            $idequipo = $this->ModeloCatalogos->Insert('equipo',array('luxometro'=>$luxometro,'equipo'=>$equipo,'reg'=>$this->fechahoy,"marca"=>$marca,"modelo"=>$modelo,"no_informe_calibracion"=>$no_informe_calibracion,"fecha_calibracion"=>$fecha_calibracion,"tipo"=>$tipo));
        }else{
            $idequipo = $id;
            $this->ModeloCatalogos->updateCatalogo('equipo',array('luxometro'=>$luxometro,'equipo'=>$equipo,"marca"=>$marca,"modelo"=>$modelo,"no_informe_calibracion"=>$no_informe_calibracion,"fecha_calibracion"=>$fecha_calibracion,"tipo"=>$tipo),array('id'=>$idequipo));
        }
        $DATAc = json_decode($arrayequipos);  
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->id==0){
                $dataa = array(
                    'idnom'=>$idequipo,
                    'escala_ini'=>$DATAc[$i]->escala_ini,
                    'escala_fin'=>$DATAc[$i]->escala_fin,
                    'iluminancia_ref'=>$DATAc[$i]->iluminancia_ref,
                    'iluminancia_pro'=>$DATAc[$i]->iluminancia_pro,
                    'insertidumbre'=>$DATAc[$i]->insertidumbre,
                    'resolucion'=>$DATAc[$i]->resolucion
                );
                $this->ModeloCatalogos->Insert('equipo_detalle',$dataa);
            }else{
                $dataa = array(
                    'escala_ini'=>$DATAc[$i]->escala_ini,
                    'escala_fin'=>$DATAc[$i]->escala_fin,
                    'iluminancia_ref'=>$DATAc[$i]->iluminancia_ref,
                    'iluminancia_pro'=>$DATAc[$i]->iluminancia_pro,
                    'insertidumbre'=>$DATAc[$i]->insertidumbre,
                    'resolucion'=>$DATAc[$i]->resolucion
                );
                $this->ModeloCatalogos->updateCatalogo('equipo_detalle',$dataa,array('id'=>$DATAc[$i]->id));
            }
        }
    }

    function inserupdateSonometro(){
        $params = $this->input->post();
        $id = $params['id'];
        $num_calibra = $params['num_calibra'];
        $fecha_calibra = $params['fecha_calibra'];
        unset($params['num_calibra']);
        unset($params['fecha_calibra']);
        if($params["tipo"]==2){
            if($id==0){
                $idequipo = $this->ModeloCatalogos->Insert('equipo',$params);
                $this->ModeloCatalogos->Insert('equipo_sonometro_detalle',array("id_equipo"=>$idequipo,"num_calibra"=>$num_calibra,"fecha_calibra"=>$fecha_calibra));
            }else{
                $idequipo = $id;
                $this->ModeloCatalogos->updateCatalogo('equipo',$params,array('id'=>$idequipo));
                //VERIFICAR SI EXSITE DATO, SI NO INSERTARLO
                $cont_det=0;
                $veri_det=$this->ModeloCatalogos->getselectwheren('equipo_sonometro_detalle',array('id_equipo'=>$idequipo));
                foreach($veri_det as $vd){
                    $cont_det++;
                }
                if($cont_det==0){
                    $this->ModeloCatalogos->Insert('equipo_sonometro_detalle',array("id_equipo"=>$idequipo,"num_calibra"=>$num_calibra,"fecha_calibra"=>$fecha_calibra));
                }
                else {
                    $this->ModeloCatalogos->updateCatalogo('equipo_sonometro_detalle',array("id_equipo"=>$idequipo,"num_calibra"=>$num_calibra,"fecha_calibra"=>$fecha_calibra),array('id_equipo'=>$idequipo));
                }
            }
        }else{
            if($id==0){
                $idequipo = $this->ModeloCatalogos->Insert('equipo',$params);
            }else{
                $idequipo = $id;
                $this->ModeloCatalogos->updateCatalogo('equipo',$params,array('id'=>$idequipo));
            }
        }
    }

    function calculaInterPend($idequipo,$unidad,$tipo){ 
        $x=array();
        $y=array();
        $intercept=0; $slope=0;

        $datos=$this->Catalogos_model->selectWhereOrder('equipo_medidor_detalle',array('id_equipo'=>$idequipo,'unidad'=>$unidad),"unidad","asc","line","asc");
        foreach ($datos as $item) {
            $x[]=$item->prueba;
            $y[]=$item->referencia;
        }
        $n     = count($x);     // number of items in the array
        $x_sum = array_sum($x); // sum of all X values
        $y_sum = array_sum($y); // sum of all Y values

        $xx_sum = 0;
        $xy_sum = 0;
     
        for($i = 0; $i < $n; $i++) {
            $xy_sum += round( $x[$i]*$y[$i] ,6);
            $xx_sum += round( $x[$i]*$x[$i] ,6);
        }
        
        // Slope
        if(( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) )>0){
            $slope = round(( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) ),6);
        }else{
            $slope=0;
        }
        // calculate intercept
        $intercept = round(( $y_sum -  $slope * $x_sum ) / $n,6);    
        //log_message('error', 'slope: '.$slope);
        //log_message('error', 'intercept: '.$intercept);
        if($tipo==0){
            $viewdatos=$intercept;
            $this->ModeloCatalogos->updateCatalogo('equipo_medidor_detalle',array("interseccion"=>$viewdatos),array('id_equipo'=>$idequipo,"unidad"=>$unidad));
        }else{
            $viewdatos=$slope;
            $this->ModeloCatalogos->updateCatalogo('equipo_medidor_detalle',array("pendiente"=>$viewdatos),array('id_equipo'=>$idequipo,"unidad"=>$unidad));
        }
        //return $viewdatos;   
    }

    function inserUpdateTermo(){
        $params = $this->input->post();
        $id = $params['id'];
        $luxometro = $params['luxometro'];
        $equipo = $params['equipo'];
        $marca = $params['marca'];
        $modelo = $params['modelo'];
        $no_informe_calibracion = $params['no_informe_calibracion'];
        $fecha_calibracion = $params['fecha_calibracion'];
        $tipo = $params['tipo'];
        $tipo_equipo = $params['tipo_equipo'];
        //$name_cert = $params['certificado'];
        $arrayequipos = $params['arrayequipos'];

        //log_message('error', 'tipo_equipo: '.$tipo_equipo);
        $this->db->trans_start();
        if($id==0){
            $idequipo = $this->ModeloCatalogos->Insert('equipo',array('luxometro'=>$luxometro,'equipo'=>$equipo,'reg'=>$this->fechahoy,"marca"=>$marca,"modelo"=>$modelo,"no_informe_calibracion"=>$no_informe_calibracion,"fecha_calibracion"=>$fecha_calibracion,"tipo"=>$tipo));
        }else{
            $idequipo = $id;
            $this->ModeloCatalogos->updateCatalogo('equipo',array('luxometro'=>$luxometro,'equipo'=>$equipo,"marca"=>$marca,"modelo"=>$modelo,"no_informe_calibracion"=>$no_informe_calibracion,"fecha_calibracion"=>$fecha_calibracion,"tipo"=>$tipo),array('id'=>$idequipo));
        }
        $DATAc = json_decode($arrayequipos);  
        
        if($tipo_equipo==5){
            for ($i=0;$i<count($DATAc);$i++) {
                //$inter = calculaInterPend(0,$DATAc[$i]->referencia,$DATAc[$i]->prueba); //acá me quedo
                //$pend = calculaInterPend(0,$DATAc[$i]->referencia,$DATAc[$i]->prueba);
                
                $dataa = array(
                    'unidad'=>$DATAc[$i]->unidad,
                    'line'=>$DATAc[$i]->line,
                    'referencia'=>$DATAc[$i]->referencia,
                    'prueba'=>$DATAc[$i]->prueba,
                    'absoluto'=>$DATAc[$i]->absoluto,
                    'incerti'=>$DATAc[$i]->incerti,
                    'valfc'=>$DATAc[$i]->valfc,
                    'id_equipo'=>$idequipo
                );
                if($DATAc[$i]->id==0 && $DATAc[$i]->referencia!=""){
                    $this->ModeloCatalogos->Insert('equipo_medidor_detalle',$dataa);
                }if($DATAc[$i]->id!=0 && $DATAc[$i]->referencia!=""){
                    $id=$DATAc[$i]->id;
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_medidor_detalle',$dataa,array('id'=>$id));
                }if($DATAc[$i]->id!=0 && $DATAc[$i]->referencia==""){
                    $id=$DATAc[$i]->id;
                    $dataa2 = array(
                        'unidad'=>0,
                        'line'=>0,
                        'referencia'=>0,
                        'prueba'=>0,
                        'absoluto'=>0,
                        'incerti'=>0,
                        'valfc'=>0,
                        'id_equipo'=>$idequipo
                    );
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_medidor_detalle',$dataa2,array('id'=>$id));
                }
                if($DATAc[$i]->line%1==0 && $DATAc[$i]->line%5==0 && $DATAc[$i]->unidad!="20000" || $DATAc[$i]->line%1==0 && $DATAc[$i]->line%4==0 && $DATAc[$i]->unidad=="20000"){
                    //log_message('error', 'unidad para calcular pendiente: '.$DATAc[$i]->unidad);
                    $this->calculaInterPend($idequipo,$DATAc[$i]->unidad,0);
                    $this->calculaInterPend($idequipo,$DATAc[$i]->unidad,1);
                }
            }
        }
        else if($tipo_equipo==8){
            for ($i=0;$i<count($DATAc);$i++) {
                $dataa = array(
                    'temp_patron'=>$DATAc[$i]->temp_patron,
                    'temp_ibc'=>$DATAc[$i]->temp_ibc,
                    'error'=>$DATAc[$i]->error,
                    'u'=>$DATAc[$i]->dato_u,
                    'fc'=>$DATAc[$i]->fc,
                    'id_equipo'=>$idequipo
                );
                if($DATAc[$i]->id==0){
                    $this->ModeloCatalogos->Insert('equipo_termometro_detalle',$dataa);
                }else{
                    $id=$DATAc[$i]->id;
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_termometro_detalle',$dataa,array('id'=>$id));
                }
            }
        }else if($tipo_equipo==10){
            for ($i=0;$i<count($DATAc);$i++) {
                $dataa = array(
                    'vel_patron'=>$DATAc[$i]->vel_patron,
                    'vel_ibc'=>$DATAc[$i]->vel_ibc,
                    'error'=>$DATAc[$i]->error,
                    'u'=>$DATAc[$i]->dato_u,
                    'fc'=>$DATAc[$i]->fc,
                    'id_equipo'=>$idequipo
                );
                if($DATAc[$i]->id==0){
                    $this->ModeloCatalogos->Insert('equipo_anemometro_detalle',$dataa);
                }else{
                    $id=$DATAc[$i]->id;
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_anemometro_detalle',$dataa,array('id'=>$id));
                }
            }
        }else if($tipo_equipo==11 || $tipo_equipo==12 || $tipo_equipo==13){
            for ($i=0;$i<count($DATAc);$i++) {
                $dataa = array(
                    'patron'=>$DATAc[$i]->patron,
                    'libc'=>$DATAc[$i]->libc,
                    'error'=>$DATAc[$i]->error,
                    'incertidumbre'=>$DATAc[$i]->incertidumbre,
                    'tipo'=>$DATAc[$i]->tipo,
                    'id_equipo'=>$idequipo
                );
                if($DATAc[$i]->id==0){
                    $this->ModeloCatalogos->Insert('equipo_bulbo_detalle',$dataa);
                }else{
                    $id=$DATAc[$i]->id;
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_bulbo_detalle',$dataa,array('id'=>$id));
                }
            }
        }else if($tipo_equipo==14){ //Higrotermoanemometro
            for ($i=0;$i<count($DATAc);$i++) {
                $dataa = array(
                    'line'=>$DATAc[$i]->line,
                    'patron'=>$DATAc[$i]->patron,
                    'calibracion'=>$DATAc[$i]->calibracion,
                    'instrumento'=>$DATAc[$i]->instrumento,
                    'masmenosue'=>$DATAc[$i]->masmenosue,
                    'correccion'=>$DATAc[$i]->correccion,
                    'id_equipo'=>$idequipo
                );
                if($DATAc[$i]->id==0 && $DATAc[$i]->patron!=""){
                    $this->ModeloCatalogos->Insert('equipo_higro_detalle',$dataa);
                }else{
                    $id=$DATAc[$i]->id;
                    unset($dataa["id"]);
                    $this->ModeloCatalogos->updateCatalogo('equipo_higro_detalle',$dataa,array('id'=>$id));
                }
            }
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
    }

    function infodatosequipo(){
        $id = $this->input->post('idequipos');
        $tipo = $this->input->post('tipo');
        $idequipo=$id;
        $datosequipos=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id));
        $datosequipos=$datosequipos->result();
        $datosequipos=$datosequipos[0];

        //log_message('error', 'tipo: '.$tipo);
        if($tipo==1){
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_detalle',array('idnom'=>$id,'activo'=>1));
            $array = array(
                            'id'=>$datosequipos->id,
                            'luxometro'=>$datosequipos->luxometro,
                            'equipo'=>$datosequipos->equipo,
                            'reg'=>$datosequipos->reg,
                            'marca'=>$datosequipos->marca,
                            'modelo'=>$datosequipos->modelo,
                            'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                            'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                            'tipo'=>$datosequipos->tipo,
                            'equiposdetalle'=>$datosequiposdetalle->result(),
                            'intercept'=>$this->ModeloCatalogos->interseccion_pendiente($idequipo,0),
                            'slope'=>$this->ModeloCatalogos->interseccion_pendiente($idequipo,1)
                        );
        }else if($tipo==5){ //medidor de resistencias
            //$datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_medidor_detalle',array('id_equipo'=>$id));
            $datosequiposdetalle=$this->ModeloEquipos->getDetalleMedidor($id);
            $array = array(
                    'id'=>$datosequipos->id,
                    'luxometro'=>$datosequipos->luxometro,
                    'equipo'=>$datosequipos->equipo,
                    'reg'=>$datosequipos->reg,
                    'marca'=>$datosequipos->marca,
                    'modelo'=>$datosequipos->modelo,
                    'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                    'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                    'tipo'=>$datosequipos->tipo,
                    'equiposdetalle'=>$datosequiposdetalle,
                );
        }else if($tipo==8){
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_termometro_detalle',array('id_equipo'=>$id,'estatus'=>1));
            $array = array(
                            'id'=>$datosequipos->id,
                            'luxometro'=>$datosequipos->luxometro,
                            'equipo'=>$datosequipos->equipo,
                            'reg'=>$datosequipos->reg,
                            'marca'=>$datosequipos->marca,
                            'modelo'=>$datosequipos->modelo,
                            'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                            'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                            'tipo'=>$datosequipos->tipo,
                            'equiposdetalle'=>$datosequiposdetalle->result(),
                            'intercept'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,0,8),
                            'slope'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,1,8)
                        );
        }else if($tipo==10){
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_anemometro_detalle',array('id_equipo'=>$id,'estatus'=>1));
            $array = array(
                            'id'=>$datosequipos->id,
                            'luxometro'=>$datosequipos->luxometro,
                            'equipo'=>$datosequipos->equipo,
                            'reg'=>$datosequipos->reg,
                            'marca'=>$datosequipos->marca,
                            'modelo'=>$datosequipos->modelo,
                            'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                            'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                            'tipo'=>$datosequipos->tipo,
                            'equiposdetalle'=>$datosequiposdetalle->result(),
                            'intercept'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,0,10),
                            'slope'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,1,10)
                        );
        }else if($tipo==11 || $tipo==12 || $tipo==13){ //bulbo seco, humedo, globo
            if($tipo==11){
                $tipo_det=1;
            }else if($tipo==12){
                $tipo_det=2;
            }else if($tipo==13){
                $tipo_det=3;
            }
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_bulbo_detalle',array('id_equipo'=>$id,'tipo'=>$tipo_det,'estatus'=>1));
            $array = array(
                            'id'=>$datosequipos->id,
                            'luxometro'=>$datosequipos->luxometro,
                            'equipo'=>$datosequipos->equipo,
                            'reg'=>$datosequipos->reg,
                            'marca'=>$datosequipos->marca,
                            'modelo'=>$datosequipos->modelo,
                            'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                            'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                            'tipo'=>$datosequipos->tipo,
                            'equiposdetalle'=>$datosequiposdetalle->result(),
                            'intercept'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,0,$tipo),
                            'slope'=>$this->ModeloCatalogos->interseccion_pendiente2($idequipo,1,$tipo)
                    );
        }else if($tipo==14){ // Higrotermoanemometro
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_higro_detalle',array('id_equipo'=>$id));
            $array = array(
                    'id'=>$datosequipos->id,
                    'luxometro'=>$datosequipos->luxometro,
                    'equipo'=>$datosequipos->equipo,
                    'reg'=>$datosequipos->reg,
                    'marca'=>$datosequipos->marca,
                    'modelo'=>$datosequipos->modelo,
                    'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                    'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                    'tipo'=>$datosequipos->tipo,
                    'equiposdetalle'=>$datosequiposdetalle->result(),
                );
        }
        else{
            $datosequiposdetalle=$this->ModeloCatalogos->getselectwheren('equipo_sonometro_detalle',array('id_equipo'=>$id));
            $array = array(
                            'id'=>$datosequipos->id,
                            'luxometro'=>$datosequipos->luxometro,
                            'equipo'=>$datosequipos->equipo,
                            'reg'=>$datosequipos->reg,
                            'marca'=>$datosequipos->marca,
                            'modelo'=>$datosequipos->modelo,
                            'no_informe_calibracion'=>$datosequipos->no_informe_calibracion,
                            'fecha_calibracion'=>$datosequipos->fecha_calibracion,
                            'tipo'=>$datosequipos->tipo,
                            'equiposdetalle'=>$datosequiposdetalle->result(),
                        );
        }
        echo json_encode($array);
    }
    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->ModeloEquipos->getequipos($params);
        $totaldata= $this->ModeloEquipos->getequipos_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function deleteequipo(){
        $params = $this->input->post();
        $idequipo = $params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('equipo',array('activo'=>0),array('id'=>$idequipo));
    }
    function infodatosequipoxy($idequipo){
        $datos=$this->ModeloCatalogos->getselectwheren('equipo_detalle',array('idnom'=>$idequipo,'activo'=>1));
        $x=array();
        $y=array();
        foreach ($datos->result() as $item) {
            //$x[]=$item->iluminancia_ref;
            //$y[]=$item->iluminancia_pro;

            $x[]=$item->iluminancia_pro;
            $y[]=$item->iluminancia_ref;

        }

        $n     = count($x);     // number of items in the array
        $x_sum = array_sum($x); // sum of all X values
        $y_sum = array_sum($y); // sum of all Y values

        $xx_sum = 0;
        $xy_sum = 0;
     
        for($i = 0; $i < $n; $i++) {
            
            $xy_sum += round( $x[$i]*$y[$i] ,2);
            $xx_sum += round( $x[$i]*$x[$i] ,2);
        }

        // Slope
        $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
     
        // calculate intercept
        $intercept = ( $y_sum -  $slope * $x_sum ) / $n;
     
        $datos = array( 
            'slope'     => $slope,
            'intercept' => $intercept,
        );

        echo json_encode($datos);
    }
    function infodatosequipoxy2($idequipo){
        $datos = array( 
            'slope'     => $this->ModeloCatalogos->interseccion_pendiente($idequipo,1),
            'intercept' => $this->ModeloCatalogos->interseccion_pendiente($idequipo,0),
        );

        echo json_encode($datos);
    }
    
    /*function uploadImg(){
        $fecha=date('Ymd_His');
        $id = $_POST['id'];
        $img2 = [];
        $configUpload['upload_path'] = FCPATH.'uploads/imgEquipos/';
        $configUpload['allowed_types'] = 'jpg|jpeg|png|webp';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = $id.'_'.$fecha;
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('inputFile');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension
        $img = $file_name;
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('equipo',array('certificado'=>$img),array('id'=>$id));
        }
        $output = [];
        echo json_encode($output);   
    }*/

    public function uploadImg(){
        //$id = $this->input->post('id');
        $id_equipo = $this->input->post('id_equipo');
        //log_message('error', 'id: '.$id);
        $upload_folder ='uploads/imgEquipos';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
        $nombre_archivo=str_replace('-', '_', $nombre_archivo);
        $nombre_archivo=$this->eliminar_acentos($nombre_archivo);
        $newfile=$fecha."_".$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            if($id_equipo>0){
                //$this->ModeloCatalogos->updateCatalogo('equipo_certificado',array('certificado'=>$newfile,"id_equipo"=>$id_equipo),array('id'=>$id));
                $id=$this->ModeloCatalogos->Insert('equipo_certificado',array('certificado'=>$newfile,"id_equipo"=>$id_equipo));
                $return = Array('id'=>$id,'ok'=>TRUE);
            }else{
                $return = Array('id'=>0,'ok'=>TRUE);
            }
        }
        echo json_encode($return);
    }

    /*public function verificaCert(){
        $id = $this->input->post('id');
        $data=$this->ModeloCatalogos->getselectwheren('equipo_certificado',array('id'=>$id,'estatus'=>1));
        echo json_encode($data->result());
    }*/

    public function verificaCert(){
        $id = $this->input->post("id");
        $i=0;
        $getdat=$this->ModeloCatalogos->getselectwheren('equipo_certificado',array('id_equipo'=>$id,"estatus"=>1));

        foreach ($getdat->result() as $k) {
            $i++;
            $img = "<img src='".base_url()."uploads/imgEquipos/".$k->certificado."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '".base_url()."Equipos/deleteFile/".$k->id."', caption: '".$k->id."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/imgEquipos/".$k->certificado."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false";  
        
            echo '
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-md-12"> Certificado: '.$k->certificado.'</label>
                        <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                        <hr style="background-color: red;">
                    </div>

                </div>
                <script>
                $("#inputFile'.$i.'").fileinput({
                    overwriteInitial: false,
                    showClose: false,
                    showCaption: false,
                    showUploadedThumbs: false,
                    showBrowse: false,
                    removeTitle: "Cancel or reset changes",
                    elErrorContainer: "#kv-avatar-errors-1",
                    msgErrorClass: "alert alert-block alert-danger",
                    defaultPreviewContent: "'.$img.'",
                    layoutTemplates: {main2: "{preview} {remove}"},
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    initialPreview: [
                    "'.$imgp.'"
                    ],
                    initialPreviewAsData: '.$typePDF.',
                    initialPreviewFileType: "image",
                    initialPreviewConfig: [
                        '.$imgdet.'
                    ]
                }).on("filebeforedelete", function() {
                    verificaCertificado();
                });
                $("#inputFile'.$i.'").on("filedeleted", function(event, key, jqXHR, data) {
                    //console.log("Key = " + key);
                });
                </script>';
        }     
    }

    public function deleteFile($id){
        $this->ModeloCatalogos->updateCatalogo('equipo_certificado',array('estatus'=>0),array('id'=>$id));
    }

    public function eliminar_acentos($cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
        $cadena
        );

        return $cadena;
    }
}
?>
