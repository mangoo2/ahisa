<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->logueo = $this->session->userdata('logeado');
        if ($this->logueo!=1) {
            redirect(base_url(), 'refresh');
        }
        $this->load->model('Catalogos_model');
    }
    
    public function index() {
        
    }
    
    public function cliente($estado){
        if($estado!="0"){
            $array = array('estatus'=>1,"estado"=>$estado); 
        }else{
            $array = array('estatus'=>1);
        }
        $data['clientesrow']=$this->Catalogos_model->getselectwheren('clientes',$array);
        $this->load->view('reportes/clientesexport',$data);
    }
    
    
    
    
    
    
}
