<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Formatos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Cotizaciones_model', 'model');
        $this->load->model('Catalogos_model');
        $logueo = $this->session->userdata('logeado');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        
    }

    public function cotizacion($id) {
        
        $data['cotizacion'] = $this->model->getCotizacion($id);
        $data['contacto']=$this->model->getContactoCotizaciones($data['cotizacion']->contacto_id);
        $data['servicios'] = $this->model->getServiciosCotizacion($id,$data['cotizacion']->id_empresa);
        //$desc=$this->model->getDescuentos();
        // $data['descuentos']= array_merge(
        //         $this->descuentoCategorias($data['servicios'], $desc),
        //         $this->descuentoPuntos($data['servicios'], $desc),
        //         $this->descuentoRecipientes($data['servicios'], $desc)
        //         );
        //$this->printPDF("Cotizacion", 'cotizacion2.php', $data);
        $this->load->view('reportes/cotizacion2',$data);
    }
    
    public function orden($id) {
        $this->load->model('Cotizaciones_model', 'model_c');
        $this->load->model('Ordenes_model', 'model_o');

        $data['orden'] = $this->model_o->getOrden($id);
        $data['cotizacion'] = $this->model_c->getCotizacion($data['orden']->cotizacion_id);
        $data['cliente']=$this->model_o->getCliente($data["cotizacion"]->cliente_id);
        $data["servicios"]=$this->model_o->getServiciosCotizacion($data["orden"]->cotizacion_id,$data['orden']->id_empresa);
        $data["contactos"]=$this->Catalogos_model->selectWhere("personas_contacto",array("cliente_id"=>$data["cotizacion"]->cliente_id,"orden"=>1));
        
        $this->printPDF("Orden de Trabajo", 'orden.php', $data);
    }
    
    
    private function descuentoCategorias($cotiza,$desc){
        $result=array();
        $cat=array();
        foreach($cotiza as $c){
            array_push($cat, $c->categoria);
        }
        $valores = array_count_values($cat);
        foreach($valores as $key => $v){
            $categoria=$this->model->getCategoria($key);
           if($v>1 && $v<3){ 
                
                $precio=0;
                foreach($cotiza as $co){
                    if($co->categoria==$key){
                        $precio+=($co->precio*$co->cantidad);
                    }
                }
                $descuento=$precio*$desc->a_proveedor1/100;
                
                array_push($result,array(
                    "desc"=>"Descuento sobre categoría: $categoria->nombre",
                    "porc"=>$desc->a_proveedor1,
                    "cant"=>$descuento
                ));
            }
            elseif($v>=3) {
                $precio=0;
                foreach($cotiza as $co){
                    if($co->categoria==$key){
                        $precio+=($co->precio*$co->cantidad);
                    }
                }
                $descuento=$precio*$desc->a_proveedor2/100;
                
                array_push($result,array(
                    "desc"=>"Descuento sobre categoría: $categoria->nombre",
                    "porc"=>$desc->a_proveedor2,
                    "cant"=>$descuento
                ));
            }
               
        }
        
        return $result;
    }
    
    private function descuentoPuntos($cotiza,$desc){
        $result=array();
        
        foreach($cotiza as $c){
            
            $servicio=$this->model->getServicio($c->servicio_id);
            if($servicio->por_puntos>0){
                if($c->cantidad>0 && $c->cantidad<51){
                    $descuento=$c->precio*$desc->por_puntos1/100;
                    array_push($result,array(
                        "desc"=>"Descuento por puntos: ".$c->descripcion,
                        "porc"=>$desc->por_puntos1,
                        "cant"=>$descuento
                    ));
                }
                if($c->cantidad>50 && $c->cantidad<101){
                    $descuento=$c->precio*$desc->por_puntos2/100;
                    array_push($result,array(
                        "desc"=>"Descuento por puntos: ".$c->descripcion,
                        "porc"=>$desc->por_puntos2,
                        "cant"=>$descuento
                    )); 
                }
                if($c->cantidad>100){
                    $descuento=$c->precio*$desc->por_puntos3/100;
                    array_push($result,array(
                        "desc"=>"Descuento por puntos: ".$c->descripcion,
                        "porc"=>$desc->por_puntos3,
                        "cant"=>$descuento
                    ));
                }
            }
        }
        return $result;
    }
    
    private function descuentoRecipientes($cotiza,$desc){
        $result=array();
        
        foreach($cotiza as $c){
            
            $servicio=$this->model->getServicio($c->servicio_id);
            if($servicio->recipientes>0){
                if($c->cantidad>1 && $c->cantidad<6){
                    $descuento=$c->precio*$desc->por_equipo1/100;
                    array_push($result,array(
                        "desc"=>"Descuento equipo a presión: ".$c->descripcion,
                        "porc"=>$desc->por_equipo1,
                        "cant"=>$descuento
                    ));
                }
                if($c->cantidad>5 && $c->cantidad<11){
                    $descuento=$c->precio*$desc->por_equipo2/100;
                    array_push($result,array(
                        "desc"=>"Descuento equipo a presión: ".$c->descripcion,
                        "porc"=>$desc->por_equipo2,
                        "cant"=>$descuento
                    ));
                }
                if($c->cantidad>10 && $c->cantidad<51){
                    $descuento=$c->precio*$desc->por_equipo3/100;
                    array_push($result,array(
                        "desc"=>"Descuento equipo a presión: ".$c->descripcion,
                        "porc"=>$desc->por_equipo3,
                        "cant"=>$descuento
                    ));
                }
                if($c->cantidad>50 ){
                    $descuento=$c->precio*$desc->por_equipo4/100;
                    array_push($result,array(
                        "desc"=>"Descuento equipo a presión: ".$c->descripcion,
                        "porc"=>$desc->por_equipo4,
                        "cant"=>$descuento
                    ));
                }
                
            }
        }
        return $result;
    }
    
    public function email_cotizacion($id){
        //log_message('error', 'funcion email_cotizacion: '.$id);
        //obtencion de datos
        $data['cotizacion'] = $this->model->getCotizacion($id);
        $data['contacto']=$this->model->getContactoCotizacion($id);
        $data['servicios'] = $this->model->getServiciosCotizacion($id,$data['cotizacion']->id_empresa);
        
        $mail=$data['contacto']->email;

        $this->load->view('reportes/cotizacion2',$data);
        
        /*
        //creacion de PDF
        $this->load->library('Pdf');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Ahisa');
        $pdf->SetTitle("Cotizacion");

        $pdf->setPrintHeader(false);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetMargins(10, 10, 10, true);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $html = $this->load->view('formatos/' . "cotizacion2.php", $data, true);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        
        $pdf->lastPage();
        //ob_clean();
        $pdf->Output('./mail/cotizacion.pdf', 'F');
        */
        
        //Envio de Mail
        $this->load->library('email');

        $config = array();
        $config['protocol'] = 'smtp';  
        $id_emp = $data['cotizacion']->id_empresa;

        /*$config['smtp_host'] = 'mail.ahisa.mx';                     
        $config['smtp_user'] = 'cotizaciones@ahisa.mx';
        $mail_dom = 'cotizaciones@ahisa.mx';
        $config['smtp_pass'] = 'cotizaciones12345';*/
        
        $config['smtp_host'] = 'ahisa.mangoo.systems';                     
        $config['smtp_user'] = 'programaciones@ahisa.mangoo.systems';
        $mail_dom = 'programaciones@ahisa.mangoo.systems';
        $config['smtp_pass'] = 'glUNjFEtR@S$';

        $name="Cotización Grupo Ahisa ";

        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from($mail_dom, $name);
        $this->email->to($mail);
        $this->email->subject('Cotización de servicio ');
        $msj=$this->load->view('formatos/mail', $data, true);
        //echo $msj; die;
        $this->email->message($msj);

        $pdf = FCPATH.'mail/cotizacion_'.$id.'.pdf';
        $this->email->attach($pdf);

        $this->email->send();        
    }

    public function printPDF($name, $file, $data) {

        $this->load->library('Pdf');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Ahisa');
        $pdf->SetTitle($name);
        $pdf->SetFont('dejavusans', '', 11, '', false);
        $pdf->setPrintHeader(false);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetMargins(10, 9, 10, true);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        
        
        
        $html = $this->load->view('formatos/' . $file, $data, true);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        
        //$pdf->lastPage();
        $pdf->Output($name . '.pdf', 'I');
    }

}
