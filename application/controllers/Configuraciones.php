<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {

    //CONTROLADOR CATALOGOS: listados,altas y edicion

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
//        $logueo = $this->session->userdata('logeado');
//        if($logueo!=1){
//            redirect(base_url(), 'refresh');
//        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
//        
        $this->load->model('Catalogos_model', 'model');
        $this->load->model('ModeloCatalogos');
    }

    //VIEWS -------------------------------------------------------

    public function departamentos() {
        $data["departamentos"] = $this->getListado("departamentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/departamentos', $data);
        $this->load->view('footer');
    }
    
    public function descuentos() {
        $data["descuentos"] = $this->model->getCatalogo("descuentos")[0];
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/descuentos', $data);
        $this->load->view('footer');
    }
    
    public function documentos() {
        $data["documentos"] = $this->getListado("documentos");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/documentos', $data);
        $this->load->view('footer');
    }

    public function encuestas() {
        $data["pregs"]=$this->model->getselectwheren('preguntas_encuesta',array('id_empresa'=>1,'estatus'=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/encuestas',$data);
        $this->load->view('footer');
    }

    public function get_encuestas() {
        $id=$this->input->post('id');
        $pregs=$this->model->getselectwheren('preguntas_encuesta',array('id_empresa'=>$id,'estatus'=>1));
        $html="";
        foreach ($pregs->result() as $d) { 
            $sel="";
            $sel2="";
            if($d->tipo==1){ $sel="selected"; }
            else if($d->tipo==2){ $sel2="selected"; }
            $html.='<tr>
                <td width="90%"><input type="hidden" id="id" value="'.$d->id.'">   
                    <div class="row">
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="pregunta" value="'.$d->pregunta.'">
                        </div>
                        <div class="col-md-2">
                            <select id="tipo" class="form-control">
                                <option '.$sel.' value="1">Escala</option>
                                <option '.$sel2.' value="2">Si/No</option>
                            </select>
                        </div>
                    </div>
                </td>
                <td><button type="button" onclick="eliminarPreg('.$d->id.')" class="btn btn-raised btn-icon btn-pure danger mr-1"><i class="fa fa-times-circle"></i></button></td>
            </tr>';
        }
        echo $html;
    }   

    public function eliminar_preg() {
        $id=$this->input->post('id');
        $result = $this->model->updateCatalogo(array("estatus"=>0), $id,"preguntas_encuesta");
    } 

    public function submit_pregs(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            //$id = $DATA[$i]->id;
            $data['id_empresa'] = $DATA[$i]->id_empresa;
            $data['pregunta'] = $DATA[$i]->pregunta;
            $data['tipo'] = $DATA[$i]->tipo;
            $data['id']= $DATA[$i]->id;
            $id = $DATA[$i]->id; //aca me quedo
            $temp=array("id_empresa"=>$DATA[$i]->id_empresa,"pregunta"=>$DATA[$i]->pregunta,"tipo"=>$DATA[$i]->tipo);
            if($DATA[$i]->id==0){    
                $id=$this->model->insertToCatalogo($temp,"preguntas_encuesta");
            }else{
                $this->model->updateCatalogon("preguntas_encuesta",$temp,array("id"=>$id));
            }
            echo $id;          
        }
    }

    //Funciones genericas de insercion y edicion ------------------------
    public function insertUpdateToCatalogo($catalogo) {
        $data = array(
            "id" => $this->input->post("id"),
            "nombre" => strtoupper($this->input->post("nombre"))
        );
        if($catalogo=="descuentos_act"){
            $data=$this->input->post();
        }
        
        if ($data['id'] == null) {
            //insert
            $result = $this->model->insertToCatalogo($data, $catalogo);
        } else {
            //update
            $id = $data['id'];
            unset($data['id']);
            $result = $this->model->updateCatalogo($data, $id, $catalogo);
        }

        echo $result;
    }

    public function updatePorcentajes() {
        $data = $this->input->post();
        $result = $this->model->insertToCatalogo($data, "porcentajes");
        echo $result;
    }

    public function deleteFromCatalogo($catalogo) {
        $id = $this->input->post("id");
        $result = $this->model->deleteCatalogo($id, $catalogo);
        echo $result;
    }

    public function getListado($catalogo) {
        return $this->model->getCatalogo($catalogo);
    }

    public function getDescuentos() {
        $descuentos = $this->model->getDescuentos();
        $json_data = array("data" => $descuentos);
        echo json_encode($json_data);
    }

    public function deleteDescuento($id){
        echo $this->model->updateCatalogo(array(estatus=>0), $id, "descuentos_act");
        
    }

    /*****************DOCS DE ACREDIUTACION****************/ 
    public function documentosAcreditacion() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/documentosAcre');
        $this->load->view('footer');
        $this->load->view('configuraciones/documentosAcrejs');
    }

    function uploadImg($tip){
        $id_nom = $this->input->post('norma');
        //log_message('error', 'id_nom: '.$id_nom);
        $upload_folder ='uploads/imgAcre';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        $fecha=date('ymd-His');
        $nombre_archivo=str_replace(' ', '_', $nombre_archivo);
        $nombre_archivo=str_replace('-', '_', $nombre_archivo);
        $nombre_archivo=$this->eliminar_acentos($nombre_archivo);
        $newfile=$fecha."_".$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $id=$this->ModeloCatalogos->Insert('imagenes_acre_apro',array('idnom'=>$id_nom,'url_img'=>$newfile,"tipo"=>$tip,'reg'=>$this->fechahoy));
            $return = array('id'=>$id,'ok'=>TRUE);
        }
        
        echo json_encode($return);
    }

    public function verificaCert(){
        $norma = $this->input->post("norma");
        $tipo = $this->input->post("tipo");
        $i=0;
        if($norma=="4") //NOM-081
            $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom'=>$norma,"tipo"=>$tipo,"estatus"=>1));
        else
            $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom!='=>"4","tipo"=>$tipo,"estatus"=>1));

        foreach ($getdat->result() as $k) {
            $i++;
            $img = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' width='50px' alt='Sin Dato'>";
            $imgdet = "{type: 'image', url: '".base_url()."Configuraciones/deleteFile/".$k->id."', caption: '".$k->id."', key:".$i."}";
            $imgp = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
            $typePDF = "false";  
        
            echo '
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-md-12"> Certificado: '.$k->url_img.'</label>
                        <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                        <hr style="background-color: red;">
                    </div>

                </div>
                <script>
                $("#inputFile'.$i.'").fileinput({
                    overwriteInitial: false,
                    showClose: false,
                    showCaption: false,
                    showUploadedThumbs: false,
                    showBrowse: false,
                    removeTitle: "Cancel or reset changes",
                    elErrorContainer: "#kv-avatar-errors-1",
                    msgErrorClass: "alert alert-block alert-danger",
                    defaultPreviewContent: "'.$img.'",
                    layoutTemplates: {main2: "{preview} {remove}"},
                    allowedFileExtensions: ["jpg", "png", "jpeg"],
                    initialPreview: [
                    "'.$imgp.'"
                    ],
                    initialPreviewAsData: '.$typePDF.',
                    initialPreviewFileType: "image",
                    initialPreviewConfig: [
                        '.$imgdet.'
                    ]
                }).on("filebeforedelete", function() {
                    verificaCertificado();
                });
                $("#inputFile'.$i.'").on("filedeleted", function(event, key, jqXHR, data) {
                    //console.log("Key = " + key);
                });
                </script>';
        }     
    }

    public function eliminar_acentos($cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ç', 'ç'),
        array('C', 'c'),
        $cadena
        );

        return $cadena;
    }

    /* *********************************** */
    public function deleteFile($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_acre_apro',array('estatus'=>0),array('id'=>$id));
    }
    /*****************DOCS DE APROBACION****************/ 
    public function documentosAprobacion() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('configuraciones/documentosApro');
        $this->load->view('footer');
        $this->load->view('configuraciones/documentosAprojs');
    }
    /* *********************************** */
    

}
