<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nom extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloNom');
        $this->load->model('Catalogos_model');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoyc = date('Y-m-d');
        $this->id_usuario = $this->session->userdata("id_usuario");
        //require_once APPPATH.'third_party/phpexcel/PHPExcel.php';
        //$this->excel = new PHPExcel(); 
        if($this->session->userdata('logeado')==true && $this->session->userdata("perfil")==1 || $this->session->userdata('logeado')==true && $this->session->userdata("perfil")==5 || $this->session->userdata('logeado')==true && $this->session->userdata("perfil")==6){
        }else{
            redirect('Main'); 
        }
    }
    function index(){
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('nom/lis');
            $this->load->view('footer');
            $this->load->view('nom/lisjs');
    }
    public function getlistnom() {
        $params = $this->input->post();
        //log_message('error', 'params: '.$params["id_cliente"]);
        $getdata = $this->ModeloNom->getnom($params,$this->id_usuario);
        $totaldata= $this->ModeloNom->getnom_total($params,$this->id_usuario); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function add25($id=0,$cot=0,$id_chs=0){ //id de nom , $cot = id de orden
        $data['idnom']=$id;
        $data['orden']=$cot;
        $data['id_chs']=$id_chs;
        $data['numinfo']='AHISA/EP-02';
        //$data['razonsocial']='AHISA LABORATORIO DE PRUEBAS, S. DE R.L. DE C.V.';
        $data['razonsocial']="";
        $data['fecha']=$this->fechahoyc;
        $data["cliente"] = "";
        //if($id>0){
            $getcli=$this->ModeloCatalogos->getClienteOrden($cot);
            $data["cliente"]=$getcli->cliente;
            $data["razon_cli"]=$getcli->razoncli;

        //}
        $data['resultequipo']=$this->ModeloCatalogos->getselectwheren('equipo',array('activo'=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/cnom025',$data);
        $this->load->view('footer');
        $this->load->view('nom/cnom025js');
    }
    function add11($id=0,$cot=0,$id_chs=0){
        $data['idordenes']=$id;
        $data['idnom']="";
        $data['num_informe']="";
        $data['razon_social']="";
        $data['fecha']="";
        $data['veri_ini']="";
        $data['veri_fin']="";
        $data['criterio']="";
        $data['cumple_criterio']="";
        $data['poten_ini']="";
        $data['poten_fin']="";
        $data['condiciones_opera']="";
        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom11',array('idordenes'=>$id,"activo"=>1));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
            $data['id_sonometro']=$item->id_sonometro;
            $data['id_calibrador']=$item->id_calibrador;
            $data['veri_ini']=$item->veri_ini;
            $data['veri_fin']=$item->veri_fin;
            $data['criterio']=$item->criterio;
            $data['cumple_criterio']=$item->cumple_criterio;
            $data['poten_ini']=$item->poten_ini;
            $data['poten_fin']=$item->poten_fin;
            $data['condiciones_opera']=$item->condiciones_opera;
        }
        /*
        $recono=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom11',array('id_orden'=>$id));
        foreach ($recono->result() as $item) {
            $data['num_informe']=$item->num_informe_rec;
            $data['razon_social']=$item->cliente;
        }
        */

        $data["sono"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"2","activo"=>1));
        $data["calibra"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"4","activo"=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/cnom011',$data);
        $this->load->view('footer');
        $this->load->view('nom/cnom011js');
    }
    function add11personal($idordenes){
        $data['idordenes']=$idordenes;

        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom11',array('idordenes'=>$idordenes));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;

        }
        $data["e"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"3","activo"=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/cnom011personal',$data);
        $this->load->view('footer');
        $this->load->view('nom/cnom011personaljs');
    }
    function add11view($id=0){
        $data['idordenes']=$id;
        $data['idnom']="";
        $data['num_informe']="";
        $data['razon_social']="";
        $data['fecha']="";
        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom11',array('idordenes'=>$id));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
        }
        
        $this->load->view('nom/cnom011view',$data);
        $this->load->view('nom/cnom011viewjs');
    }
    /* *********************************************/
    function add22($id=0,$id_chs=0){
        $data['id_orden']=$id;
        $data['id_chs']=$id_chs;
        $data['idnom']="0";
        $data['num_informe']="";
        $data['razon_social']="";
        //$data['fecha']=$this->fechahoyc;
        $data['fecha']=date("d-m-Y");
        $data['id_medidor']="";
        $data['id_multimetro']="";
        $data['id_set_resis']="";
        $data['id_higro']="";
        $data['valor_ref']="";
        $data['veri_ini']="";
        $data['cumple_criterio']="";
        $data['veri_fin']="";
        $data['cumple_criterio_fin']="";
        $data['criterio']="";
        $data['valor_rk']="";
        
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom22',array('id_orden'=>$id,"id_chs"=>$id_chs));
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $data["num_informe"]=$datosrec->num_informe_rec;
            $data["razon_social"]=$datosrec->cliente;
        }

        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom22',array('idordenes'=>$id,"activo"=>1));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
            $data['id_medidor']=$item->id_medidor;
            $data['id_multimetro']=$item->id_multimetro;
            $data['id_set_resis']=$item->id_set_resis;
            $data['id_higro']=$item->id_higro;
            $data['valor_ref']=$item->valor_ref;
            $data['veri_ini']=$item->veri_ini;
            $data['cumple_criterio']=$item->cumple_criterio;
            $data['veri_fin']=$item->veri_fin;
            $data['cumple_criterio_fin']=$item->cumple_criterio_fin;
            $data['criterio']=$item->criterio;
            $data['valor_rk']=$item->valor_rk;
        }
        $data["med"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"5","activo"=>1));
        $data["mult"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"6","activo"=>1));
        $data["set"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"7","activo"=>1));
        $data["higro"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"14","activo"=>1));
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/cnom022',$data);
        $this->load->view('footer');
        $this->load->view('nom/cnom022js');
    }
    function submitNom22(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $num_informe = $params['num_informe'];
        $idordenes = $params['id_orden'];
        $razon_social = $params['razon_social'];
        $fecha = $params['fecha'];
        $id_medidor = $params['id_medidor'];
        $id_multimetro = $params['id_multimetro'];
        $id_set_resis = $params['id_set_resis'];
        $id_higro = $params['id_higro'];
        //$nom = $params['nom'];
        $valor_ref = $params['valor_ref'];
        $veri_ini = $params['veri_ini'];
        $cumple_criterio = $params['cumple_criterio'];
        $veri_fin = $params['veri_fin'];
        $cumple_criterio_fin = $params['cumple_criterio_fin'];
        $criterio = $params['criterio'];
        $valor_rk = $params['valor_rk'];
        $id_chs = $params['id_chs'];

        /*$arraypuntos = $params['arraypuntos'];
        //$arrayvalores = $params['arrayvalores'];
        $arrayfts = $params['arrayfts'];
        $arraymds = $params['arraymds'];*/

        $array_nom22= array('num_informe'=>$num_informe,'idordenes'=>$idordenes,'razon_social'=>$razon_social,'fecha'=>$fecha,'id_medidor'=>$id_medidor,'id_multimetro'=>$id_multimetro,"id_set_resis"=>$id_set_resis,"id_higro"=>$id_higro,"valor_ref"=>$valor_ref,"veri_ini"=>$veri_ini,"cumple_criterio"=>$cumple_criterio,"veri_fin"=>$veri_fin,"cumple_criterio_fin"=>$cumple_criterio_fin,"criterio"=>$criterio,"valor_rk"=>$valor_rk,"id_chs"=>$id_chs,'reg'=>$this->fechahoy);

        $this->db->trans_start();
        if($idnom==0){
            $array_nom22['insert_user']=$this->id_usuario;
            $idnom = $this->ModeloCatalogos->Insert('nom22',$array_nom22);
        }else{
            $idnom = $idnom;
            $array_nom22['update_user']=$this->id_usuario;
            $array_nom22['update_reg']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo('nom22',$array_nom22,array('idnom'=>$idnom));
        }

        /*$medicion_humedad="";
        $observaciones="";
        $DATAmd = json_decode($arraymds);  
        for ($m=0;$m<count($DATAmd);$m++) {
            $medicion_humedad=$DATAmd[$m]->medicion_humedad;
            $observaciones=$DATAmd[$m]->observaciones;
        }

        $id_nomd_ant=0;  $band_insert=0;$cont_fts=0;
        $DATAc = json_decode($arraypuntos);  
        for ($i=0;$i<count($DATAc);$i++) {
            $dataa = array(
                'idnom'=>$idnom,
                'area'=>$DATAc[$i]->area,
                'punto'=>$DATAc[$i]->punto,
                'fecha_pto'=>$DATAc[$i]->fecha_pto,
                'ubicacion'=>$DATAc[$i]->ubicacion,
                'desconectado'=>$DATAc[$i]->desconectado,
                'd1'=>$DATAc[$i]->d1,
                'd2'=>$DATAc[$i]->d2,
                'd3'=>$DATAc[$i]->d3,
                'd4'=>$DATAc[$i]->d4,
                'd5'=>$DATAc[$i]->d5,
                'd6'=>$DATAc[$i]->d6,
                'd7'=>$DATAc[$i]->d7,
                'd8'=>$DATAc[$i]->d8,
                'd9'=>$DATAc[$i]->d9,
                'r1'=>$DATAc[$i]->r1,
                'r2'=>$DATAc[$i]->r2,
                'r3'=>$DATAc[$i]->r3,
                'r4'=>$DATAc[$i]->r4,
                'r5'=>$DATAc[$i]->r5,
                'r6'=>$DATAc[$i]->r6,
                'r7'=>$DATAc[$i]->r7,
                'r8'=>$DATAc[$i]->r8,
                'r9'=>$DATAc[$i]->r9,
                'reg'=>$this->fechahoy
            );
            if($DATAc[$i]->id==0){ //no exsite detalles de la nom22
                $id_nomd=$this->ModeloCatalogos->Insert('nom22d',$dataa);
                $id_nomd_edit=0; $cont_fts=0;
                
            }else{ //ya existe detalles de nom22
                $id_nomd=$DATAc[$i]->id; $id_nomd_edit=$id_nomd;
                $this->ModeloCatalogos->updateCatalogo('nom22d',$dataa,array('id'=>$DATAc[$i]->id)); 
            }  
        }
        $DATAf = json_decode($arrayfts);
        for ($f=0;$f<count($DATAf);$f++) {
            $cont_fts++;
            $data = array(
                    'id_nom'=>$idnom,
                    //'id_nom22d'=>$id_nomd,
                    'punto'=>$DATAf[$f]->punto,
                    'fuente'=>$DATAf[$f]->fuente,
                    'valor'=>$DATAf[$f]->valor,
                    'tipo'=>$DATAf[$f]->tipo,
                    'caracteristica'=>$DATAf[$f]->caracteristica,
                    'medicion_humedad'=>$medicion_humedad,
                    'observaciones'=>$observaciones,
                    'reg'=>$this->fechahoy
            );
            if(isset($DATAf[$f]->fuente) && $DATAf[$f]->fuente!="0" && $DATAf[$f]->fuente!=""){
                log_message('error', 'id_fuente: '.$DATAf[$i]->id_fuente);
                log_message('error', 'punto de fuentes: '.$DATAf[$f]->punto);
                //log_message('error', 'id_nomd: '.$id_nomd);
                if($DATAf[$f]->id_fuente==0){
                    //log_message('error', 'inserto fuente: '.$DATAf[$f]->fuente);
                    $this->ModeloCatalogos->Insert('fuentes_genera_nom22',$data);
                }
                if($DATAf[$f]->id_fuente!=0){
                    //log_message('error', 'edito fuente: '.$DATAf[$f]->fuente);
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$data,array('id'=>$DATAf[$f]->id_fuente));
                }
            }
        }*/
            
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo json_encode(array("status"=>$status,"idnom"=>$idnom));
    }
    function submitNom22Detalle(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $arraypuntos = $params['arraypuntos'];

        //log_message('error','DATA: ' . $arraypuntos);

        $this->db->trans_start();
        $DATAc = json_decode($arraypuntos);  
        $ids_nomd = array();
        for ($i=0;$i<count($DATAc);$i++) {
            $dataa = array(
                'idnom'=>$idnom,
                'area'=>$DATAc[$i]->area,
                'punto'=>$DATAc[$i]->punto,
                'fecha_pto'=>$DATAc[$i]->fecha_pto,
                'ubicacion'=>$DATAc[$i]->ubicacion,
                'desconectado'=>$DATAc[$i]->desconectado,
                'pararrayo'=>$DATAc[$i]->pararrayo,
                'd1'=>$DATAc[$i]->d1,
                'd2'=>$DATAc[$i]->d2,
                'd3'=>$DATAc[$i]->d3,
                'd4'=>$DATAc[$i]->d4,
                'd5'=>$DATAc[$i]->d5,
                'd6'=>$DATAc[$i]->d6,
                'd7'=>$DATAc[$i]->d7,
                'd8'=>$DATAc[$i]->d8,
                'd9'=>$DATAc[$i]->d9,
                'r1'=>$DATAc[$i]->r1,
                'r2'=>$DATAc[$i]->r2,
                'r3'=>$DATAc[$i]->r3,
                'r4'=>$DATAc[$i]->r4,
                'r5'=>$DATAc[$i]->r5,
                'r6'=>$DATAc[$i]->r6,
                'r7'=>$DATAc[$i]->r7,
                'r8'=>$DATAc[$i]->r8,
                'r9'=>$DATAc[$i]->r9,
                'reg'=>$this->fechahoy
            );

            
            if($DATAc[$i]->id==0){ //no exsite detalles de la nom22
                $dataa['insert_user']=$this->id_usuario;
                $id_nomd=$this->ModeloCatalogos->Insert('nom22d',$dataa);
                array_push($ids_nomd, $id_nomd);
                
            }else{ //ya existe detalles de nom22
                $id_nomd=$DATAc[$i]->id;
                $dataa['update_user']=$this->id_usuario;
                $dataa['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('nom22d',$dataa,array('id'=>$DATAc[$i]->id)); 
                array_push($ids_nomd, $id_nomd);
            } 
        
        }
            
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo json_encode(array("status"=>$status,"id_nomd"=>$id_nomd,"ids_nomd"=>json_encode($ids_nomd)));
    }
    function submitNom22Fuentes(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $id_nomd = $params['id_nomd'];
        $arrayfts = $params['arrayfts'];
        $arraymds = $params['arraymds'];
        $id_fuente=0;
        $medicion_humedad="";
        $observaciones="";

        //log_message('error','DATAFTS: '.json_encode($arrayfts));
        //log_message('error','DATAMDS: '.json_encode($arraymds));

        $this->db->trans_start();
        $DATAmd = json_decode($arraymds);  
        for ($m=0;$m<count($DATAmd);$m++) {
            $medicion_humedad=$DATAmd[$m]->medicion_humedad;
            $observaciones=$DATAmd[$m]->observaciones;
        }

        $DATAf = json_decode($arrayfts);
        for ($f=0;$f<count($DATAf);$f++) {
            if($DATAf[$f]->id_fuente==0){
                $data = array(
                        'id_nom'=>$idnom,
                        'id_nom22d'=>$id_nomd,
                        'punto'=>$DATAf[$f]->punto,
                        'fuente'=>$DATAf[$f]->fuente,
                        'valor'=>$DATAf[$f]->valor,
                        'tipo'=>$DATAf[$f]->tipo,
                        'caracteristica'=>$DATAf[$f]->caracteristica,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy,
                        'insert_user'=>$this->id_usuario
                );
                //log_message('error','DATA: '.json_encode($data));
            }else{
                $dataEdit = array(
                        'id_nom'=>$idnom,
                        'punto'=>$DATAf[$f]->punto,
                        'fuente'=>$DATAf[$f]->fuente,
                        'valor'=>$DATAf[$f]->valor,
                        'tipo'=>$DATAf[$f]->tipo,
                        'caracteristica'=>$DATAf[$f]->caracteristica,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy,
                        'update_user'=>$this->id_usuario,
                        'update_reg'=>$this->fechahoy
                );   
                //log_message('error','DATAE: '.json_encode($dataEdit));
            }

            if(isset($DATAf[$f]->fuente) && $DATAf[$f]->fuente!="" || isset($DATAf[$f]->caracteristica) && $DATAf[$f]->caracteristica!=""){
                //log_message('error', 'id_fuente: '.$DATAf[$f]->id_fuente);
                //log_message('error', 'punto de fuentes: '.$DATAf[$f]->punto);
                //log_message('error', 'id_nomd: '.$id_nomd);
                if($DATAf[$f]->id_fuente==0){
                    //log_message('error', 'inserto fuente: '.$DATAf[$f]->fuente);
                    //log_message('error', 'inserto DATA: '.json_encode($data));
                    $id_fuente=$this->ModeloCatalogos->Insert('fuentes_genera_nom22',$data);
                }
                if($DATAf[$f]->id_fuente!=0){
                    //log_message('error', 'edito fuente: '.$DATAf[$f]->fuente);
                    $id_fuente=$DATAf[$f]->id_fuente;
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataEdit,array('id'=>$DATAf[$f]->id_fuente));
                }
            }
        }
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo json_encode(array("status"=>$status,"id_fuente"=>$id_fuente));
    }
    function submitNom22Detalles(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $arrayfts = $params['arrayfts'];
        $arraymds = $params['arraymds'];
        
        $this->db->trans_start();

        $medicion_humedad="";
        $observaciones="";
        $DATAmd = json_decode($arraymds);  
        for ($m=0;$m<count($DATAmd);$m++) {
            $medicion_humedad=$DATAmd[$m]->medicion_humedad;
            $observaciones=$DATAmd[$m]->observaciones;
        }

        // inserta valores_resistencia_nom22
        $DATAv = json_decode($arrayvalores);  
        for ($r=0;$r<count($DATAv);$r++) {
            $datav = array(
                    'id_nom'=>$idnom,
                    'id_nom22d'=>$DATAv[$r]->id_nom22d,
                    'd1'=>$DATAv[$r]->d1,
                    'd2'=>$DATAv[$r]->d2,
                    'd3'=>$DATAv[$r]->d3,
                    'd4'=>$DATAv[$r]->d4,
                    'd5'=>$DATAv[$r]->d5,
                    'd6'=>$DATAv[$r]->d6,
                    'd7'=>$DATAv[$r]->d7,
                    'd8'=>$DATAv[$r]->d8,
                    'd9'=>$DATAv[$r]->d9,
                    'r1'=>$DATAv[$r]->r1,
                    'r2'=>$DATAv[$r]->r2,
                    'r3'=>$DATAv[$r]->r3,
                    'r4'=>$DATAv[$r]->r4,
                    'r5'=>$DATAv[$r]->r5,
                    'r6'=>$DATAv[$r]->r6,
                    'r7'=>$DATAv[$r]->r7,
                    'r8'=>$DATAv[$r]->r8,
                    'r9'=>$DATAv[$r]->r9,
                    'reg'=>$this->fechahoy
                );
            //log_message('error', 'id de DATAv: '.$DATAv[$i]->id);
            if($DATAv[$r]->id==0){
                $datav['insert_user']=$this->id_usuario;
                $this->ModeloCatalogos->Insert('valores_resistencia_nom22',$datav);
            }else{
                $datav['update_user']=$this->id_usuario;
                $datav['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('valores_resistencia_nom22',$datav,array('id'=>$DATAv[$r]->id));
            }
        }

        // inserta fuentes_genera_nom22
        //$dataf=array();
        $DATAf = json_decode($arrayfts);  
        for ($f=0;$f<count($DATAf);$f++) {
            //log_message('error', 'valor: '.$DATAf[$f]->valor);
            if(isset($DATAf[$f]->fuente) && $DATAf[$f]->fuente!="0" && $DATAf[$f]->fuente!=""){
                //$dataf[] = array(
                $dataf = array(
                        'id_nom'=>$idnom,
                        'id_nom22d'=>$DATAf[$f]->id_nom22d,
                        'fuente'=>$DATAf[$f]->fuente,
                        'valor'=>$DATAf[$f]->valor,
                        'tipo'=>1,
                        'caracteristica'=>$DATAf[$f]->caracteristica,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy
                );
                if($DATAf[$f]->id_fuente==0 /*&& count($dataf)>0*/){
                    $this->ModeloCatalogos->Insert('fuentes_genera_nom22',$dataf);
                }else if($DATAf[$f]->id_fuente!=0 && count($dataf)>0){
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataf,array('id'=>$DATAf[$f]->id_fuente));
                }
            }
            if(isset($DATAf[$f]->fuente2) && $DATAf[$f]->fuente2!="0" && $DATAf[$f]->fuente2!=""){
                //$dataf[] = array(
                $dataf = array(
                        'id_nom'=>$idnom,
                        'id_nom22d'=>$DATAf[$f]->id_nom22d,
                        'fuente'=>$DATAf[$f]->fuente2,
                        'valor'=>$DATAf[$f]->valor2,
                        'tipo'=>2,
                        'caracteristica'=>$DATAf[$f]->caracteristica2,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy
                );
                if($DATAf[$f]->id_fuente2==0 /*&& count($dataf)>0*/){
                    $this->ModeloCatalogos->Insert('fuentes_genera_nom22',$dataf);
                }else if($DATAf[$f]->id_fuente2!=0 && count($dataf)>0){
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataf,array('id'=>$DATAf[$f]->id_fuente2));
                }
            }
            if(isset($DATAf[$f]->fuente3) && $DATAf[$f]->fuente3!="0" && $DATAf[$f]->fuente3!=""){
                //$dataf[] = array(
                $dataf = array(
                        'id_nom'=>$idnom,
                        'id_nom22d'=>$DATAf[$f]->id_nom22d,
                        'fuente'=>$DATAf[$f]->fuente3,
                        'valor'=>$DATAf[$f]->valor3,
                        'tipo'=>3,
                        'caracteristica'=>$DATAf[$f]->caracteristica3,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy
                );
                if($DATAf[$f]->id_fuente3==0 /*&& count($dataf)>0*/){
                    $this->ModeloCatalogos->Insert('fuentes_genera_nom22',$dataf);
                }else if($DATAf[$f]->id_fuente3!=0 && count($dataf)>0){
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataf,array('id'=>$DATAf[$f]->id_fuente3));
                }
            }
            if(isset($DATAf[$f]->fuente4) && $DATAf[$f]->fuente4!="0" && $DATAf[$f]->fuente4!=""){
                //$dataf[] = array(
                $dataf = array(
                        'id_nom'=>$idnom,
                        'id_nom22d'=>$DATAf[$f]->id_nom22d,
                        'fuente'=>$DATAf[$f]->fuente4,
                        'valor'=>$DATAf[$f]->valor4,
                        'tipo'=>4,
                        'caracteristica'=>$DATAf[$f]->caracteristica4,
                        'medicion_humedad'=>$medicion_humedad,
                        'observaciones'=>$observaciones,
                        'reg'=>$this->fechahoy
                );
                if($DATAf[$f]->id_fuente4==0 /*&& count($dataf)>0*/){
                    $this->ModeloCatalogos->Insert('fuentes_genera_nom22',$dataf);
                }else if($DATAf[$f]->id_fuente4!=0 && count($dataf)>0){
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataf,array('id'=>$DATAf[$f]->id_fuente4));
                }
            }
            /*if($DATAf[$f]->fuente!="0" || $DATAf[$f]->fuente2!="0"|| $DATAf[$f]->fuente3!="0" || $DATAf[$f]->fuente4!="0"){
                if($DATAf[$f]->id_fuente==0 && count($dataf)>0 || $DATAf[$f]->id_fuente2==0 && count($dataf)>0 || $DATAf[$f]->id_fuente3==0 && count($dataf)>0 || $DATAf[$f]->id_fuente4==0 && count($dataf)>0){
                    //$this->ModeloCatalogos->Insert('fuentes_genera_nom22',$dataf);
                    $this->ModeloCatalogos->insert_batch("fuentes_genera_nom22",$dataf);
                }else if($DATAf[$f]->id!=0 && count($dataf)>0){
                    $this->ModeloCatalogos->updateCatalogo('fuentes_genera_nom22',$dataf,array('id'=>$DATAf[$f]->id_fuente));
                }
            }*/
        }
       
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            echo "error";   
        }else{
            echo "ok";
        }
    }
    function infoDatosNom22(){
        $id = $this->input->post('idpunto');
        $id_chs = $this->input->post('id_chs');
        $datosnom=$this->ModeloCatalogos->getselectwheren('nom22',array('idnom'=>$id,"id_chs"=>$id_chs));
        $datosnom=$datosnom->result();
        
        $array = array(); $vac="vacio";
        $datosnomdetalle=$this->ModeloNom->getDatosNom22($id);

        //$datosnomdetalle=$this->ModeloNom->getDatosTotalNom22($id);
        
        /*$datosnomdetalle=$this->ModeloCatalogos->getselectwheren('nom22d',array('idnom'=>$id,'activo'=>1));
        $getdet=$datosnomdetalle->row();
        $valores=$this->ModeloCatalogos->getselectwheren('valores_resistencia_nom22',array('id_nom22d'=>$getdet->id,'activo'=>1));*/
        $fuentes=$this->ModeloNom->getFuentesNom22($id);
        if($datosnom){
            $datosnom=$datosnom[0];
            $array = array(
                'idnom'=>$datosnom->idnom,
                'idordenes'=>$datosnom->idordenes,
                'num_informe'=>$datosnom->num_informe,
                'razon_social'=>$datosnom->razon_social,
                'fecha'=>$datosnom->fecha,
                'id_medidor'=>$datosnom->id_medidor,
                'id_multimetro'=>$datosnom->id_multimetro,
                'id_set_resis'=>$datosnom->id_set_resis,
                'valor_ref'=>$datosnom->valor_ref,
                'veri_ini'=>$datosnom->veri_ini,
                'cumple_criterio'=>$datosnom->cumple_criterio,
                'veri_fin'=>$datosnom->veri_fin,
                'cumple_criterio_fin'=>$datosnom->cumple_criterio_fin,
                'criterio'=>$datosnom->criterio,
                'valor_rk'=>$datosnom->valor_rk,
                'id_chs'=>$datosnom->id_chs,
                'reg'=>$datosnom->reg,
                'detalle'=>$datosnomdetalle,
                //'valores'=>$valores->result(),
                'fuentes'=>$fuentes
            );
            $vac="novacio";
        }
        echo json_encode(array("data"=>$array,"vac"=>$vac));
        //echo json_encode($array);
    }
    function add22view($id=0){
        $data['idordenes']=$id;
        $data['idnom']="";
        $data['num_informe']="";
        $data['razon_social']="";
        $data['fecha']="";
        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom22',array('idordenes'=>$id));
        
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
            $data['id_medidor']=$item->id_medidor;
            $data['id_higro']=$item->id_higro;
        }
        
        $this->load->view('nom/cnom022view',$data);
        //$this->load->view('nom/cnom022viewjs');
    }
    function nominserupdatetableg22(){
        $datax = $this->input->post();
        $id=$datax['id'];
        $base64Image=$datax['grafica'];
        $params['grafica']=$base64Image;
        $this->ModeloCatalogos->updateCatalogo('nom22d',$params,array('id'=>$id));
    }
    function deletePunto22(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('nom22d',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('id'=>$id));
    }
    /* ******************* NOM081 ***************************/
    function add81($id=0,$id_chs=0){
        $data['id_orden']=$id;
        $data['id_chs']=$id_chs;
        $data['idnom']="0";
        $idnom=0;
        $data['num_informe']="";
        $data['razon_social']="";
        //$data['fecha']=$this->fechahoyc;
        $data['fecha']=date("d-m-Y");
        $data['lugar']="";
        $data['horario']="";
        $data['zona_critica']="";
        $data['id_sonometro']="";
        $data['id_calibrador']="";
        $data['veri_ini']="";
        $data['veri_fin']="";
        $data['criterio']="";
        $data['cumple_criterio']="";
        $data['pot_bat_ini']="";
        $data['pot_bat_fin']="";
        $data['ec_largo']="";
        $data['ec_ancho']="";
        $data['observaciones']="";
        
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom81',array('id_orden'=>$id,"id_chs"=>$id_chs));
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $data["num_informe"]=$datosrec->num_informe_rec;
            $data["razon_social"]=$datosrec->cliente;
        }

        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$id,"activo"=>1));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $idnom=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
            $data['lugar']=$item->lugar;
            $data['horario']=$item->horario;
            $data['zona_critica']=$item->zona_critica;
            $data['id_sonometro']=$item->id_sonometro;
            $data['id_calibrador']=$item->id_calibrador;
            $data['veri_ini']=$item->veri_ini;
            $data['veri_fin']=$item->veri_fin;
            $data['cumple_criterio']=$item->cumple_criterio;
            $data['criterio']=$item->criterio;
            $data['pot_bat_ini']=$item->pot_bat_ini;
            $data['pot_bat_fin']=$item->pot_bat_fin;
            $data['ec_largo']=$item->ec_largo;
            $data['ec_ancho']=$item->ec_ancho;
            $data['observaciones']=$item->observaciones;
        }
        $data["sono"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"2","activo"=>1));
        $data["cali"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"4","activo"=>1));

        /*$get_dist=$this->ModeloCatalogos->getselectwheren('distancias_nom81',array('id_nom'=>$idnom)); 
        if($get_dist->num_rows()>0){
            $gd = $get_dist->row();
            $data['id_dist']=$dg->id;
            $data["altura_fte"]=$dg->altura_fte; $data["distancia_fte"]=$dg->distancia_fte;
            $data["altura_fondo"]=$dg->altura_fondo; $data["distancia_fondo"]=$dg->distancia_fondo;
            $data["altura_reduc"]=$dg->altura_reduc; $data["distancia_reduc"]=$dg->distancia_reduc;
        }else{
            $data['id_dist']="0";
            $data["altura_fte"]=""; $data["distancia_fte"]="";
            $data["altura_fondo"]=""; $data["distancia_fondo"]="";
            $data["altura_reduc"]=""; $data["distancia_reduc"]="";
        }*/

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/cnom081',$data);
        $this->load->view('footer');
        $this->load->view('nom/cnom081js');
    }
    function submitNom81(){
        $params = $this->input->post();
        //$idnom = $params['idnom'];
        $array_nom = $params['array_nom'];
        $arrayfts = $params['arrayfts'];
        $arrayfondo = $params['arrayfondo'];
        $arrayaisla = $params['arrayaisla'];
        $arraydistft = $params['arraydistft'];
        $band_ins=false;
        $status="ok";
        $array_ids=array();
        //$this->db->trans_start();
        $cont_nvo=0; $id_nom_ins=0; $id_nom_iu=0;
        $DATAc = json_decode($array_nom);  
        for ($i=0;$i<count($DATAc);$i++) {
            $dataa = array(
                'idordenes'=>$DATAc[$i]->idordenes,
                'id_chs'=>$DATAc[$i]->id_chs,
                'num_informe'=>$DATAc[$i]->num_informe,
                'razon_social'=>$DATAc[$i]->razon_social,
                'fecha'=>$DATAc[$i]->fecha,
                'lugar'=>$DATAc[$i]->lugar,
                'horario'=>$DATAc[$i]->horario,
                'zona_critica'=>$DATAc[$i]->zona_critica,
                'veri_ini'=>$DATAc[$i]->veri_ini,
                'veri_fin'=>$DATAc[$i]->veri_fin,
                'criterio'=>$DATAc[$i]->criterio,
                'cumple_criterio'=>$DATAc[$i]->cumple_criterio,
                'pot_bat_ini'=>$DATAc[$i]->pot_bat_ini,
                'pot_bat_fin'=>$DATAc[$i]->pot_bat_fin,
                'id_sonometro'=>$DATAc[$i]->id_sonometro,
                'id_calibrador'=>$DATAc[$i]->id_calibrador,
                'observaciones'=>$DATAc[$i]->observaciones,
                'ec_largo'=>$DATAc[$i]->ec_largo,
                'ec_ancho'=>$DATAc[$i]->ec_ancho,
                'reg'=>$this->fechahoy
            );
            //log_message('error', 'id_nom recibido desde json: '.$DATAc[$i]->id);
            if($DATAc[$i]->id==0 && $DATAc[$i]->num_informe!=""){
                $cont_nvo++;
                $dataa['insert_user']=$this->id_usuario;
                $id_nom=$this->ModeloCatalogos->Insert('nom81',$dataa);
                $band_ins=true;
                $id_nom_ins=$id_nom;
            }else if($DATAc[$i]->id!=0 && $DATAc[$i]->num_informe!=""){ //ya existe detalles de nom81
                $id_nom=$DATAc[$i]->id;
                $dataa['update_user']=$this->id_usuario;
                $dataa['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('nom81',$dataa,array('idnom'=>$DATAc[$i]->id));
                $band_ins=true; 
            }  
            array_push($array_ids, $id_nom);
            //log_message('error', 'dato de submit81 id_nom: '.$id_nom);
            //echo json_encode(array("status"=>"ok","id_nom"=>$id_nom));

            //$this->submitDetallesNom81($id_nom,$arrayfts,1);
            /*$DATAf = json_decode($arrayfts);  
            for ($i=0;$i<count($DATAf);$i++) {
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAf[$i]->hora_inicio,
                    'hora_fin'=>$DATAf[$i]->hora_fin,
                    'no_lectura'=>$DATAf[$i]->no_lectura,
                    'a'=>$DATAf[$i]->a,
                    'b'=>$DATAf[$i]->b,
                    'c'=>$DATAf[$i]->c,
                    'd'=>$DATAf[$i]->d,
                    'e'=>$DATAf[$i]->e,
                    'reg'=>$this->fechahoy
                );
                //log_message('error', 'id de fuente: '.$DATAf[$i]->id);
                if($DATAf[$i]->hora_inicio!=""){
                    if($DATAf[$i]->id==0){ 
                        $dataaf['insert_user']=$this->id_usuario;
                        $id_nomd=$this->ModeloCatalogos->Insert('ruido_fte_nom81',$dataaf);
                    }else{ 
                        $id_nomd=$DATAf[$i]->id;
                        unset($dataaf["id_nom"]);
                        $dataaf['update_user']=$this->id_usuario;
                        $dataaf['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_fte_nom81',$dataaf,array('id'=>$DATAf[$i]->id)); 
                    }  
                }
            }*/
            
            //$this->submitDetallesNom81($id_nom,$arrayfondo,2);
            /*$DATAfd = json_decode($arrayfondo);  
            for ($i=0;$i<count($DATAfd);$i++) {
                $dataafd = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAfd[$i]->hora_inicio,
                    'hora_fin'=>$DATAfd[$i]->hora_fin,
                    'no_lectura'=>$DATAfd[$i]->no_lectura,
                    'uno'=>$DATAfd[$i]->uno,
                    'dos'=>$DATAfd[$i]->dos,
                    'tres'=>$DATAfd[$i]->tres,
                    'cuatro'=>$DATAfd[$i]->cuatro,
                    'cinco'=>$DATAfd[$i]->cinco,
                    'reg'=>$this->fechahoy
                );
                if($DATAfd[$i]->hora_inicio!=""){
                    if($DATAfd[$i]->id==0){ 
                        $dataafd['insert_user']=$this->id_usuario;
                        $id_nomd=$this->ModeloCatalogos->Insert('ruido_fondo_nom81',$dataafd);
                    }else{ 
                        $id_nomd=$DATAfd[$i]->id;
                        unset($dataafd["id_nom"]);
                        $dataafd['update_user']=$this->id_usuario;
                        $dataafd['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_fondo_nom81',$dataafd,array('id'=>$DATAfd[$i]->id)); 
                    }  
                }
            }*/

            //$this->submitDetallesNom81($id_nom,$arrayaisla,3);
            /*$DATAais = json_decode($arrayaisla);  
            for ($i=0;$i<count($DATAais);$i++) {
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAais[$i]->hora_inicio,
                    'hora_fin'=>$DATAais[$i]->hora_fin,
                    'no_lectura'=>$DATAais[$i]->no_lectura,
                    'a'=>$DATAais[$i]->a,
                    'b'=>$DATAais[$i]->b,
                    'c'=>$DATAais[$i]->c,
                    'd'=>$DATAais[$i]->d,
                    'e'=>$DATAais[$i]->e,
                    'reg'=>$this->fechahoy
                );
                if($DATAais[$i]->hora_inicio!=""){
                    if($DATAais[$i]->id==0){ 
                        $dataaf['insert_user']=$this->id_usuario;
                        $id_nomd=$this->ModeloCatalogos->Insert('ruido_aisla_nom81',$dataaf);
                    }else{ //ya existe detalles de nom22
                        $id_nomd=$DATAais[$i]->id;
                        unset($dataaf["id_nom"]);
                        $dataaf['update_user']=$this->id_usuario;
                        $dataaf['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_aisla_nom81',$dataaf,array('id'=>$DATAais[$i]->id)); 
                    }  
                }
            }*/

            //$this->submitDetallesNom81($id_nom,$arraydistft,4);
            /*$DATAdis = json_decode($arraydistft);  
            for ($i=0;$i<count($DATAdis);$i++) {
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'altura_fte'=>$DATAdis[$i]->altura_fte,
                    'distancia_fte'=>$DATAdis[$i]->distancia_fte,
                    'altura_fondo'=>$DATAdis[$i]->altura_fondo,
                    'distancia_fondo'=>$DATAdis[$i]->distancia_fondo,
                    'altura_reduc'=>$DATAdis[$i]->altura_reduc,
                    'distancia_reduc'=>$DATAdis[$i]->distancia_reduc,
                    'reg'=>$this->fechahoy
                );
              
                if($DATAdis[$i]->id==0 && $DATAdis[$i]->altura_fte!=""){ 
                    $dataaf['insert_user']=$this->id_usuario;
                    $id_nomd=$this->ModeloCatalogos->Insert('distancias_nom81',$dataaf);
                }else if($DATAdis[$i]->id!=0 && $DATAdis[$i]->altura_fte!=""){
                    $id_nomd=$DATAdis[$i]->id;
                    unset($dataaf["id_nom"]);
                    $dataaf['update_user']=$this->id_usuario;
                    $dataaf['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('distancias_nom81',$dataaf,array('id'=>$DATAdis[$i]->id)); 
                }    
            }*/

            //log_message('error', 'id_nom: '.$id_nom);
            //log_message('error', 'id_nom_ins: '.$id_nom_ins);
            //log_message('error', 'id_nom_ins: '.$id_nom_ins);
            if($id_nom_ins>0){
                $this->submitDetallesNom81($id_nom,$arrayfts,1);
                $this->submitDetallesNom81($id_nom,$arrayfondo,2);
                $this->submitDetallesNom81($id_nom,$arrayaisla,3);
                $this->submitDetallesNom81($id_nom,$arraydistft,4);
                $id_nom_ins=0;
            }
            $id_nom_iu=$id_nom;
        }
        /*$this->submitDetallesNom81($id_nom,$arrayfts,1);
        $this->submitDetallesNom81($id_nom,$arrayfondo,2);
        $this->submitDetallesNom81($id_nom,$arrayaisla,3);
        $this->submitDetallesNom81($id_nom,$arraydistft,4);*/

        /*$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }*/
        echo json_encode(array("status"=>$status,"array_ids"=>$array_ids));
    }
    function submitDetallesNom81($id_nom,$array_det,$tipo){
        //log_message('error', 'submitDetallesNom81 id_nom: '.$id_nom);
        //log_message('error', 'submitDetallesNom81 tipo: '.$tipo);
        $dataaf_batch=array(); $dataafd_batch=array(); $dataaf2_batch=array(); $dataaf3_batch=array();
        $cont1=0;
        if($tipo==1){
            $DATAf = json_decode($array_det);  
            for ($i=0;$i<count($DATAf);$i++) {
                $cont1++;
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAf[$i]->hora_inicio,
                    'hora_fin'=>$DATAf[$i]->hora_fin,
                    'no_lectura'=>$DATAf[$i]->no_lectura,
                    'a'=>$DATAf[$i]->a,
                    'b'=>$DATAf[$i]->b,
                    'c'=>$DATAf[$i]->c,
                    'd'=>$DATAf[$i]->d,
                    'e'=>$DATAf[$i]->e,
                    'reg'=>$this->fechahoy
                );
                //log_message('error', 'id de fuente: '.$DATAf[$i]->id);
                $dataaf_batch[]=$dataaf;
                if($DATAf[$i]->hora_inicio!=""){
                    if($DATAf[$i]->id==0 && $cont1<=35){ 
                        $dataaf['insert_user']=$this->id_usuario;
                        //$id_nomd=$this->ModeloCatalogos->Insert('ruido_fte_nom81',$dataaf);
                        //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fte_nom81",$dataaf_batch);
                    }else if($DATAf[$i]->id!=0 && $cont1<=35){ 
                        $id_nomd=$DATAf[$i]->id;
                        unset($dataaf["id_nom"]);
                        $dataaf['update_user']=$this->id_usuario;
                        $dataaf['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_fte_nom81',$dataaf,array('id'=>$DATAf[$i]->id)); 
                    }  
                }
            }
            if(count($dataaf_batch)>0){
                $id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fte_nom81",$dataaf_batch);
            }
        }
        $cont2=0;
        if($tipo==2){
            $DATAfd = json_decode($array_det);  
            for ($i=0;$i<count($DATAfd);$i++) {
                $cont2++;
                $dataafd = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAfd[$i]->hora_inicio,
                    'hora_fin'=>$DATAfd[$i]->hora_fin,
                    'no_lectura'=>$DATAfd[$i]->no_lectura,
                    'uno'=>$DATAfd[$i]->uno,
                    'dos'=>$DATAfd[$i]->dos,
                    'tres'=>$DATAfd[$i]->tres,
                    'cuatro'=>$DATAfd[$i]->cuatro,
                    'cinco'=>$DATAfd[$i]->cinco,
                    'reg'=>$this->fechahoy
                );
                $dataafd_batch[]=$dataafd;
                if($DATAfd[$i]->hora_inicio!=""){
                    if($DATAfd[$i]->id==0 && $cont2<=35){ 
                        $dataafd['insert_user']=$this->id_usuario;
                        //$id_nomd=$this->ModeloCatalogos->Insert('ruido_fondo_nom81',$dataafd);
                        //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fondo_nom81",$dataafd_batch);
                    }else if($DATAfd[$i]->id!=0 && $cont2<=35){ 
                        $id_nomd=$DATAfd[$i]->id;
                        unset($dataafd["id_nom"]);
                        $dataafd['update_user']=$this->id_usuario;
                        $dataafd['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_fondo_nom81',$dataafd,array('id'=>$DATAfd[$i]->id)); 
                    }  
                }
            }
            if(count($dataafd_batch)>0){
                $id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fondo_nom81",$dataafd_batch);
            }
        }
        $cont3=0;
        if($tipo==3){
            $DATAais = json_decode($array_det);  
            for ($i=0;$i<count($DATAais);$i++) {
                $cont3++;
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'hora_inicio'=>$DATAais[$i]->hora_inicio,
                    'hora_fin'=>$DATAais[$i]->hora_fin,
                    'no_lectura'=>$DATAais[$i]->no_lectura,
                    'a'=>$DATAais[$i]->a,
                    'b'=>$DATAais[$i]->b,
                    'c'=>$DATAais[$i]->c,
                    'd'=>$DATAais[$i]->d,
                    'e'=>$DATAais[$i]->e,
                    'reg'=>$this->fechahoy
                );
                $dataaf2_batch[]=$dataaf;
                if($DATAais[$i]->hora_inicio!=""){
                    if($DATAais[$i]->id==0 && $cont3<=35){ 
                        $dataaf['insert_user']=$this->id_usuario;
                        //$id_nomd=$this->ModeloCatalogos->Insert('ruido_aisla_nom81',$dataaf);
                        //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_aisla_nom81",$dataaf2_batch);
                    }else if($DATAais[$i]->id!=0 && $cont3<=35){ //ya existe detalles de nom22
                        $id_nomd=$DATAais[$i]->id;
                        unset($dataaf["id_nom"]);
                        $dataaf['update_user']=$this->id_usuario;
                        $dataaf['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('ruido_aisla_nom81',$dataaf,array('id'=>$DATAais[$i]->id)); 
                    }  
                }
            }
            if(count($dataaf2_batch)>0){
                $id_nomd=$this->ModeloCatalogos->insert_batch("ruido_aisla_nom81",$dataaf2_batch);
            }
        }
        $cont4=0;
        if($tipo==4){
            $DATAdis = json_decode($array_det);  
            for ($i=0;$i<count($DATAdis);$i++) {
                $cont4++;
                $dataaf = array(
                    'id_nom'=>$id_nom,
                    'altura_fte'=>$DATAdis[$i]->altura_fte,
                    'distancia_fte'=>$DATAdis[$i]->distancia_fte,
                    'altura_fondo'=>$DATAdis[$i]->altura_fondo,
                    'distancia_fondo'=>$DATAdis[$i]->distancia_fondo,
                    'altura_reduc'=>$DATAdis[$i]->altura_reduc,
                    'distancia_reduc'=>$DATAdis[$i]->distancia_reduc,
                    'reg'=>$this->fechahoy
                );
                $dataaf3_batch[]=$dataaf;
                if($DATAdis[$i]->id==0 && $DATAdis[$i]->altura_fte!=""){ 
                    $dataaf['insert_user']=$this->id_usuario;
                    //$id_nomd=$this->ModeloCatalogos->Insert('distancias_nom81',$dataaf);
                    //$id_nomd=$this->ModeloCatalogos->insert_batch("distancias_nom81",$dataaf3_batch);
                }else if($DATAdis[$i]->id!=0 && $DATAdis[$i]->altura_fte!=""){
                    $id_nomd=$DATAdis[$i]->id;
                    unset($dataaf["id_nom"]);
                    $dataaf['update_user']=$this->id_usuario;
                    $dataaf['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('distancias_nom81',$dataaf,array('id'=>$DATAdis[$i]->id)); 
                }    
            }
            if(count($dataaf3_batch)>0){
                $id_nomd=$this->ModeloCatalogos->insert_batch("distancias_nom81",$dataaf3_batch);
            }
        }
    }

    function submitDetallesNom81_indep(){ //ok
        $params = $this->input->post();
        $idnom = $params['idnom'];
        //$array_nom = $params['array_nom'];
        $arrayfts = $params['arrayfts'];
        $arrayfondo = $params['arrayfondo'];
        $arrayaisla = $params['arrayaisla'];
        $arraydistft = $params['arraydistft'];
        $band_ins=false;
        $cont1=0;
        $dataaf_batch=array(); $dataafd_batch=array(); $dataaf2_batch=array(); $dataaf3_batch=array();

        $this->db->trans_start();
        $DATAf = json_decode($arrayfts);  
        for ($i=0;$i<count($DATAf);$i++) {
            $cont1++;
            $dataaf = array(
                'id'=>$DATAf[$i]->id,
                //'id_nom'=>$idnom,
                'id_nom'=>$DATAf[$i]->id_nom,
                'hora_inicio'=>$DATAf[$i]->hora_inicio,
                'hora_fin'=>$DATAf[$i]->hora_fin,
                'no_lectura'=>$DATAf[$i]->no_lectura,
                'a'=>$DATAf[$i]->a,
                'b'=>$DATAf[$i]->b,
                'c'=>$DATAf[$i]->c,
                'd'=>$DATAf[$i]->d,
                'e'=>$DATAf[$i]->e,
                'reg'=>$this->fechahoy
            );
            //log_message('error', 'id de fuente: '.$DATAf[$i]->id);
            $dataaf_batch[]=$dataaf;
            if($DATAf[$i]->hora_inicio!=""){
                if($DATAf[$i]->id==0 && $cont1<=35){ 
                    $dataaf['insert_user']=$this->id_usuario;
                    $id_nomd=$this->ModeloCatalogos->Insert('ruido_fte_nom81',$dataaf);
                    //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fte_nom81",$dataaf_batch);
                }else if($DATAf[$i]->id!=0 && $cont1<=35){ 
                    $id_nomd=$DATAf[$i]->id;
                    unset($dataaf["id_nom"]);
                    unset($dataaf["id"]);
                    $dataaf['update_user']=$this->id_usuario;
                    $dataaf['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('ruido_fte_nom81',$dataaf,array('id'=>$DATAf[$i]->id)); 
                    //$this->ModeloCatalogos->updateCatalogo_batch('ruido_fte_nom81',$dataaf_batch,'id'); 
                }  
            }
        }
        $cont2=0;
        $DATAfd = json_decode($arrayfondo);  
        for ($i=0;$i<count($DATAfd);$i++) {
            $cont2++;
            $dataafd = array(
                'id'=>$DATAfd[$i]->id,
                //'id_nom'=>$idnom,
                'id_nom'=>$DATAfd[$i]->id_nom,
                'hora_inicio'=>$DATAfd[$i]->hora_inicio,
                'hora_fin'=>$DATAfd[$i]->hora_fin,
                'no_lectura'=>$DATAfd[$i]->no_lectura,
                'uno'=>$DATAfd[$i]->uno,
                'dos'=>$DATAfd[$i]->dos,
                'tres'=>$DATAfd[$i]->tres,
                'cuatro'=>$DATAfd[$i]->cuatro,
                'cinco'=>$DATAfd[$i]->cinco,
                'reg'=>$this->fechahoy
            );
            $dataafd_batch[]=$dataafd;
            if($DATAfd[$i]->hora_inicio!=""){
                if($DATAfd[$i]->id==0 && $cont2<=35){ 
                    $dataafd['insert_user']=$this->id_usuario;
                    $id_nomd=$this->ModeloCatalogos->Insert('ruido_fondo_nom81',$dataafd);
                    //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_fondo_nom81",$dataafd_batch);
                }else if($DATAfd[$i]->id!=0 && $cont2<=35){ 
                    $id_nomd=$DATAfd[$i]->id;
                    unset($dataafd["id_nom"]);
                    unset($dataafd["id"]);
                    $dataafd['update_user']=$this->id_usuario;
                    $dataafd['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('ruido_fondo_nom81',$dataafd,array('id'=>$DATAfd[$i]->id)); 
                    //$this->ModeloCatalogos->updateCatalogo_batch('ruido_fondo_nom81',$dataafd_batch,'id'); 
                }  
            }
        }
        $cont3=0;
        $DATAais = json_decode($arrayaisla);  
        for ($i=0;$i<count($DATAais);$i++) {
            $cont3++;
            $dataaf = array(
                'id'=>$DATAais[$i]->id,
                //'id_nom'=>$idnom,
                'id_nom'=>$DATAais[$i]->id_nom,
                'hora_inicio'=>$DATAais[$i]->hora_inicio,
                'hora_fin'=>$DATAais[$i]->hora_fin,
                'no_lectura'=>$DATAais[$i]->no_lectura,
                'a'=>$DATAais[$i]->a,
                'b'=>$DATAais[$i]->b,
                'c'=>$DATAais[$i]->c,
                'd'=>$DATAais[$i]->d,
                'e'=>$DATAais[$i]->e,
                'reg'=>$this->fechahoy
            );
            $dataaf2_batch[]=$dataaf;
            if($DATAais[$i]->hora_inicio!=""){
                if($DATAais[$i]->id==0 && $cont3<=35){ 
                    $dataaf['insert_user']=$this->id_usuario;
                    $id_nomd=$this->ModeloCatalogos->Insert('ruido_aisla_nom81',$dataaf);
                    //$id_nomd=$this->ModeloCatalogos->insert_batch("ruido_aisla_nom81",$dataaf2_batch);
                }else if($DATAais[$i]->id!=0 && $cont3<=35){ 
                    $id_nomd=$DATAais[$i]->id;
                    unset($dataaf["id_nom"]);
                    unset($dataaf["id"]);
                    $dataaf['update_user']=$this->id_usuario;
                    $dataaf['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('ruido_aisla_nom81',$dataaf,array('id'=>$DATAais[$i]->id));
                    //$this->ModeloCatalogos->updateCatalogo_batch('ruido_aisla_nom81',$dataaf2_batch,'id');  
                }  
            }
        }
        $cont4=0;
        $DATAdis = json_decode($arraydistft);  
        for ($i=0;$i<count($DATAdis);$i++) {
            $cont4++;
            $dataaf = array(
                'id'=>$DATAdis[$i]->id,
                //'id_nom'=>$idnom,
                'id_nom'=>$DATAdis[$i]->id_nom,
                'altura_fte'=>$DATAdis[$i]->altura_fte,
                'distancia_fte'=>$DATAdis[$i]->distancia_fte,
                'altura_fondo'=>$DATAdis[$i]->altura_fondo,
                'distancia_fondo'=>$DATAdis[$i]->distancia_fondo,
                'altura_reduc'=>$DATAdis[$i]->altura_reduc,
                'distancia_reduc'=>$DATAdis[$i]->distancia_reduc,
                'reg'=>$this->fechahoy
            );
            $dataaf3_batch[]=$dataaf;
            if($DATAdis[$i]->id==0 && $DATAdis[$i]->altura_fte!=""){ 
                $dataaf['insert_user']=$this->id_usuario;
                $id_nomd=$this->ModeloCatalogos->Insert('distancias_nom81',$dataaf);
                //$id_nomd=$this->ModeloCatalogos->insert_batch("distancias_nom81",$dataaf3_batch);
            }else if($DATAdis[$i]->id!=0 && $DATAdis[$i]->altura_fte!=""){
                $id_nomd=$DATAdis[$i]->id;
                unset($dataaf["id_nom"]);
                unset($dataaf["id"]);
                $dataaf['update_user']=$this->id_usuario;
                $dataaf['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('distancias_nom81',$dataaf,array('id'=>$DATAdis[$i]->id)); 
                //$this->ModeloCatalogos->updateCatalogo_batch('distancias_nom81',$dataaf3_batch,'id','id');  
            }    
        }
            
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo json_encode(array("status"=>$status));
    }
    function deleteZona081(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('nom81',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$id));
    }
    /* ********************************************* */
    // NOM-015
    function add15($id=0,$id_chs=0){
        $data['id_orden']=$id;
        $data['id_chs']=$id_chs;
        $data['idnom']="0";
        $idnom=0;
        $data['num_informe']="";
        $data['razon_social']="";
        $tipo_norma=1;
        //$data['fecha']=$this->fechahoyc;
        $data['fecha']=date("d-m-Y");
        
        $data['datosrec']=$this->ModeloNom->getRecoTipoNom15($id,$id_chs);
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $data["num_informe"]=$datosrec->num_informe_rec;
            $data["razon_social"]=$datosrec->cliente;
            $data['id_chs']=$datosrec->id_chs;
            $data['id_orden']=$datosrec->id_orden;
            $tipo_norma=$datosrec->tipo_norma;
        }

        if($tipo_norma==1){
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$id,"activo"=>1));
            foreach ($resultnorma->result() as $item) {
                $data['idnom']=$item->idnom;
                $idnom=$item->idnom;
                $data["num_informe"]=$datosrec->num_informe_rec;
                $data["razon_social"]=$datosrec->cliente;
                $data["pto"]=$this->ModeloCatalogos->getselectwheren('nom15_punto',array('id_nom'=>$item->idnom,"activo"=>1));
            }

            $data["termo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"8","activo"=>1));
            $data["termo_dig"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"9","activo"=>1));
            $data["anemo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"10","activo"=>1));
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('nom/cnom015',$data);
            $this->load->view('footer');
            $this->load->view('nom/cnom015js');
        }else if($tipo_norma==2){
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$id,"activo"=>1));
            foreach ($resultnorma->result() as $item) {
                $data['idnom']=$item->idnom;
                $idnom=$item->idnom;
                $data["num_informe"]=$datosrec->num_informe_rec;
                $data["razon_social"]=$datosrec->cliente;
                $data["pto"]=$this->ModeloCatalogos->getselectwheren('nom15_punto_elevada',array('id_nom'=>$item->idnom,"activo"=>1));
            }

            $data["termo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"9","activo"=>1));
            $data["seco"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"11","activo"=>1));
            $data["humedo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"12","activo"=>1));
            $data["globo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"13","activo"=>1));
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('nom/cnom015Elevada',$data);
            $this->load->view('footer');
            $this->load->view('nom/cnom015Elevadajs');
        }
    }
    function add15view($id=0,$id_chs=0){
        $data['id_orden']=$id;
        $data['id_chs']=$id_chs;
        $data['idnom']="0";
        $idnom=0;
        $data['num_informe']="";
        $data['razon_social']="";
        $tipo_norma=1;
        //$data['fecha']=$this->fechahoyc;
        $data['fecha']=date("d-m-Y");
        
        $data['datosrec']=$this->ModeloNom->getRecoTipoNom15($id,$id_chs);
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $data["num_informe"]=$datosrec->num_informe_rec;
            $data["razon_social"]=$datosrec->cliente;
            $data['id_chs']=$datosrec->id_chs;
            $data['id_orden']=$datosrec->id_orden;
            $tipo_norma=$datosrec->tipo_norma;
        }

        if($tipo_norma==1){
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$id,"activo"=>1));
            foreach ($resultnorma->result() as $item) {
                $data['idnom']=$item->idnom;
                $idnom=$item->idnom;
                $data["num_informe"]=$datosrec->num_informe_rec;
                $data["razon_social"]=$datosrec->cliente;
                $data["pto"]=$this->ModeloCatalogos->getselectwheren('nom15_punto',array('id_nom'=>$item->idnom,"activo"=>1));
            }

            $data["termo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"8","activo"=>1));
            $data["termo_dig"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"9","activo"=>1));
            $data["anemo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"10","activo"=>1));
            $this->load->view('nom/cnom015_view',$data);
        }else if($tipo_norma==2){
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$id,"activo"=>1));
            foreach ($resultnorma->result() as $item) {
                $data['idnom']=$item->idnom;
                $idnom=$item->idnom;
                $data["num_informe"]=$datosrec->num_informe_rec;
                $data["razon_social"]=$datosrec->cliente;
                $data["pto"]=$this->ModeloCatalogos->getselectwheren('nom15_punto_elevada',array('id_nom'=>$item->idnom,"activo"=>1));
            }

            $data["termo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"9","activo"=>1));
            $data["seco"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"11","activo"=>1));
            $data["humedo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"12","activo"=>1));
            $data["globo"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"13","activo"=>1));
            
            $this->load->view('nom/cnom015Elevada_view',$data);
            
        }
    }
    function nom15viewinserupdatechart(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $img=$datax['grafica'];
        $namefile=date('ymd_His').'.png';
        $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $img));
        file_put_contents(FCPATH.'public/charts/'.$namefile, $imagenBinaria);



        $this->ModeloCatalogos->updateCatalogo('nom15',array('grafica_abatida'=>$namefile),array('idnom'=>$codigo));   
    }
    function nom15viewinserupdatechart_2(){
        $datax = $this->input->post();
        $id=$datax['id'];
        $tipo=$datax['tipo'];
        $img=$datax['grafica'];

        $namefile=date('ymd_His').'_'.$id.'_'.$tipo.'.png';

        $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $img));
        file_put_contents(FCPATH.'public/charts/'.$namefile, $imagenBinaria);
        if($tipo=='i'){
            $params['chart_inicial']=$namefile;
        }
        if($tipo=='m'){
            $params['chart_mitad']=$namefile;
        }
        if($tipo=='f'){
            $params['chart_final']=$namefile;
        }


        $this->ModeloCatalogos->updateCatalogo('nom15_punto_elevada',$params,array('id'=>$id));   
    }
    function deletePunto015(){
        $id = $this->input->post('id');
        $tabla = $this->input->post('tabla');
        $this->ModeloCatalogos->updateCatalogo($tabla,array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('id'=>$id));
        /* Aqui me quede xxx */
    }
    function inserupdate015(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $idordenes = $params['idordenes'];
        $num_informe = $params['num_informe'];
        $razon_social = $params['razon_social'];
        $id_chs = $params['id_chs'];
        $arraypuntos = $params['arraypuntos'];
        $mediciones = $params['mediciones'];

        if($idnom==0){
            $idnom = $this->ModeloCatalogos->Insert('nom15',array('num_informe'=>$num_informe,'idordenes'=>$idordenes,'razon_social'=>$razon_social,'reg'=>$this->fechahoy,"id_chs"=>$id_chs,'insert_user'=>$this->id_usuario));
        }else{
            //$idnom = $idnom;
            $this->ModeloCatalogos->updateCatalogo('nom15',array('razon_social'=>$razon_social,'reg'=>$this->fechahoy,"id_chs"=>$id_chs,'update_user'=>$this->id_usuario,'update_reg'=>$this->fechahoy),array('idnom'=>$idnom));
        }

        $id_pto=0;
        $this->db->trans_start();
            $DATAc = json_decode($arraypuntos);  
            for ($i=0;$i<count($DATAc);$i++) {
                $id_pto=$DATAc[$i]->id;
                $dataa = array(
                        'id_nom'=>$idnom,
                        'id'=>$DATAc[$i]->id,
                        'fecha'=>$DATAc[$i]->fecha,
                        'id_termo'=>$DATAc[$i]->id_termo,
                        'id_termo_digi'=>$DATAc[$i]->id_termo_digi,
                        'id_anemo'=>$DATAc[$i]->id_anemo,
                        'area'=>$DATAc[$i]->area,
                        'num_punto'=>$DATAc[$i]->num_punto,
                        'ubicacion'=>$DATAc[$i]->ubicacion,
                        'fte_generadora'=>$DATAc[$i]->fte_generadora,
                        'controles_tec'=>$DATAc[$i]->controles_tec,
                        'controles_adm'=>$DATAc[$i]->controles_adm,
                        'nom_trabaja'=>$DATAc[$i]->nom_trabaja,
                        'tipo_evalua'=>$DATAc[$i]->tipo_evalua,
                        'casco'=>$DATAc[$i]->casco,
                        'tapones'=>$DATAc[$i]->tapones,
                        'botas'=>$DATAc[$i]->botas,
                        'lentes'=>$DATAc[$i]->lentes,
                        'guantes'=>$DATAc[$i]->guantes,
                        'peto'=>$DATAc[$i]->peto,
                        'respirador'=>$DATAc[$i]->respirador,
                        'otro'=>$DATAc[$i]->otro,
                        'puesto'=>$DATAc[$i]->puesto,
                        'tiempo_expo'=>$DATAc[$i]->tiempo_expo,
                        'num_ciclos'=>$DATAc[$i]->num_ciclos,
                        'cliclo'=>$DATAc[$i]->cliclo,
                        'desc_activ'=>$DATAc[$i]->desc_activ,
                        'observaciones'=>$DATAc[$i]->observaciones,
                        'reg'=>$this->fechahoy
                    );
                if($DATAc[$i]->id==0){ 
                    $dataa['insert_user']=$this->id_usuario;
                    $id_pto=$this->ModeloCatalogos->Insert('nom15_punto',$dataa);
                }else{
                    unset($dataa["id"]);
                    $dataa['update_user']=$this->id_usuario;
                    $dataa['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('nom15_punto',$dataa,array('id'=>$id_pto));
                }
            }
            $DATAc = json_decode($mediciones);  
            for ($i=0;$i<count($DATAc);$i++) {
                if($DATAc[$i]->ciclo_med!="" && $DATAc[$i]->temp_ini!="" && $DATAc[$i]->temp_fin!=""){
                    $dataf = array(
                        'id_nom_punto'=>$id_pto,
                        'ciclo_med'=>$DATAc[$i]->ciclo_med,
                        'temp_ini'=>$DATAc[$i]->temp_ini,
                        'temp_fin'=>$DATAc[$i]->temp_fin,
                        'altura_ini'=>$DATAc[$i]->altura_ini,
                        'hora_ini'=>$DATAc[$i]->hora_ini,
                        'tbs_ini'=>$DATAc[$i]->tbs_ini,
                        'velocidad_ini'=>$DATAc[$i]->velocidad_ini,

                        'altura_media'=>$DATAc[$i]->altura_media,
                        'hora_media'=>$DATAc[$i]->hora_media,
                        'tbs_media'=>$DATAc[$i]->tbs_media,
                        'velocidad_media'=>$DATAc[$i]->velocidad_media,
                        'altura_fin'=>$DATAc[$i]->altura_fin,
                        'hora_fin'=>$DATAc[$i]->hora_fin,
                        'tbs_fin'=>$DATAc[$i]->tbs_fin,
                        'velocidad_fin'=>$DATAc[$i]->velocidad_fin,
                        'reg'=>$this->fechahoy
                    );
                    if($DATAc[$i]->id==0){
                        $dataf['insert_user']=$this->id_usuario;
                        $this->ModeloCatalogos->Insert('medicion_temp_nom15',$dataf);
                    }else{
                        $dataf['update_user']=$this->id_usuario;
                        $dataf['update_reg']=$this->fechahoy;
                        $this->ModeloCatalogos->updateCatalogo('medicion_temp_nom15',$dataf,array('id'=>$DATAc[$i]->id));
                    }
                }
            }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
    }
    function inserupdate015Elevada(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $idordenes = $params['idordenes'];
        $num_informe = $params['num_informe'];
        $razon_social = $params['razon_social'];
        $id_chs = $params['id_chs'];
        $arraypuntos = $params['arraypuntos'];
        //$mediciones = $params['mediciones'];
        $ids_pto = array();
        //log_message('error','Mediciones: '.json_encode($mediciones));
        //log_message('error','Mediciones Count: '.count($mediciones));


        if($idnom==0){
            $idnom = $this->ModeloCatalogos->Insert('nom15',array('num_informe'=>$num_informe,'idordenes'=>$idordenes,'razon_social'=>$razon_social,'reg'=>$this->fechahoy,"id_chs"=>$id_chs,'insert_user'=>$this->id_usuario));
        }else{
            $idnom = $idnom;
            $this->ModeloCatalogos->updateCatalogo('nom15',array('razon_social'=>$razon_social,'reg'=>$this->fechahoy,"id_chs"=>$id_chs,'update_user'=>$this->id_usuario,'update_reg'=>$this->fechahoy),array('idnom'=>$idnom));
        }

        $this->db->trans_start();
        $id_d=0;
        $DATAc = json_decode($arraypuntos);

        //$DATAm = json_decode($mediciones); 
        $band_exis=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $id_pto=$DATAc[$i]->id;
            $dataa = array(
                    'id_nom'=>$idnom,
                    'id'=>$DATAc[$i]->id,
                    'fecha'=>$DATAc[$i]->fecha,
                    'id_bulbo'=>$DATAc[$i]->id_bulbo,
                    'id_bulbo_humedo'=>$DATAc[$i]->id_bulbo_humedo,
                    'id_globo'=>$DATAc[$i]->id_globo,
                    'id_termo'=>$DATAc[$i]->id_termo,

                    'th_bulbo'=>$DATAc[$i]->th_bulbo,
                    'ts_bulbo'=>$DATAc[$i]->ts_bulbo,
                    'tg_bulbo'=>$DATAc[$i]->tg_bulbo,
                    'acepta_bulbo'=>$DATAc[$i]->acepta_bulbo,
                    'th_bulbo2'=>$DATAc[$i]->th_bulbo2,
                    'ts_bulbo2'=>$DATAc[$i]->ts_bulbo2,
                    'tg_bulbo2'=>$DATAc[$i]->tg_bulbo2,
                    'acepta_bulbo2'=>$DATAc[$i]->acepta_bulbo2,

                    'th_bulbo_hum'=>$DATAc[$i]->th_bulbo_hum,
                    'ts_bulbo_hum'=>$DATAc[$i]->ts_bulbo_hum,
                    'tg_bulbo_hum'=>$DATAc[$i]->tg_bulbo_hum,
                    'acepta_bulbo_hum'=>$DATAc[$i]->acepta_bulbo_hum,
                    'th_bulbo_hum2'=>$DATAc[$i]->th_bulbo_hum2,
                    'ts_bulbo_hum2'=>$DATAc[$i]->ts_bulbo_hum2,
                    'tg_bulbo_hum2'=>$DATAc[$i]->tg_bulbo_hum2,
                    'acepta_bulbo_hum2'=>$DATAc[$i]->acepta_bulbo_hum2,

                    'th_globo'=>$DATAc[$i]->th_globo,
                    'ts_globo'=>$DATAc[$i]->ts_globo,
                    'tg_globo'=>$DATAc[$i]->tg_globo,
                    'acepta_globo'=>$DATAc[$i]->acepta_globo,
                    'th_globo2'=>$DATAc[$i]->th_globo2,
                    'ts_globo2'=>$DATAc[$i]->ts_globo2,
                    'tg_globo2'=>$DATAc[$i]->tg_globo2,
                    'acepta_globo2'=>$DATAc[$i]->acepta_globo2,

                    'area'=>$DATAc[$i]->area,
                    'num_punto'=>$DATAc[$i]->num_punto,
                    'carga_solar'=>$DATAc[$i]->carga_solar,
                    'fte_generadora'=>$DATAc[$i]->fte_generadora,
                    'controles_tec'=>$DATAc[$i]->controles_tec,
                    'controles_adm'=>$DATAc[$i]->controles_adm,
                    'tipo_evalua'=>$DATAc[$i]->tipo_evalua,

                    'nom_trabaja'=>$DATAc[$i]->nom_trabaja,
                    'tipo_mov'=>$DATAc[$i]->tipo_mov,
                    'temp_aux_ini'=>$DATAc[$i]->temp_aux_ini,
                    'temp_aux_fin'=>$DATAc[$i]->temp_aux_fin,
                    'casco'=>$DATAc[$i]->casco,
                    'tapones'=>$DATAc[$i]->tapones,
                    'botas'=>$DATAc[$i]->botas,
                    'lentes'=>$DATAc[$i]->lentes,
                    'guantes'=>$DATAc[$i]->guantes,
                    'peto'=>$DATAc[$i]->peto,
                    'respirador'=>$DATAc[$i]->respirador,
                    'otro'=>$DATAc[$i]->otro,
                    'puesto'=>$DATAc[$i]->puesto,
                    'tiempo_expo'=>$DATAc[$i]->tiempo_expo,
                    'num_ciclos'=>$DATAc[$i]->num_ciclos,
                    'porc_expo'=>$DATAc[$i]->porc_expo,
                    'porc_no_expo'=>$DATAc[$i]->porc_no_expo,
                    'regimen'=>$DATAc[$i]->regimen,
                    'desc_activ'=>$DATAc[$i]->desc_activ,
                    'observaciones'=>$DATAc[$i]->observaciones,
                    'reg'=>$this->fechahoy
                );
            if($DATAc[$i]->id==0){ 
                $dataa['insert_user']=$this->id_usuario;
                $id_pto=$this->ModeloCatalogos->Insert('nom15_punto_elevada',$dataa);
                $band_exis=0;
                array_push($ids_pto, $id_pto);
            }else{
                unset($dataa["id"]);
                $id_pto = $DATAc[$i]->id;
                $dataa['update_user']=$this->id_usuario;
                $dataa['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('nom15_punto_elevada',$dataa,array('id'=>$id_pto));
                $band_exis=1;
                array_push($ids_pto, $id_pto);
            }
            //log_message('error', 'id_pto: '.$id_pto);
            //$this->saveMediciones($id_pto,$mediciones,$band_exis);
            /*log_message('error', 'id_pto: '.$id_pto); //ACA ANDO
            for ($j=0;$j<count($DATAm);$j++) {
                if($DATAm[$j]->ciclo_med!="" && $DATAm[$j]->temp_ini!="" && $DATAm[$j]->temp_fin!="" && isset($DATAm[$j]->tipo)){
                    $dataf = array(
                        'id_nom_punto'=>$id_pto,
                        'ciclo_med'=>$DATAm[$j]->ciclo_med,
                        'temp_ini'=>$DATAm[$j]->temp_ini,
                        'temp_fin'=>$DATAm[$j]->temp_fin,
                        'tipo'=>$DATAm[$j]->tipo,
                        'tipo_med'=>$DATAm[$j]->tipo_med,
                        'altura'=>$DATAm[$j]->altura_ini,
                        'hora'=>$DATAm[$j]->hora_ini,
                        'tbh'=>$DATAm[$j]->tbh_ini,
                        'tbs'=>$DATAm[$j]->tbs_ini,
                        'tg'=>$DATAm[$j]->tg_ini,
                        'reg'=>date('Y-m-d H:i:s')
                    );
                    if($DATAm[$j]->id==0 || $DATAm[$j]->id==""){
                        $this->ModeloCatalogos->Insert('medicion_temp_nom15_elevadas2',$dataf);
                    }else if($DATAm[$j]->id>0){
                        unset($dataf["id_nom_punto"]);
                        $this->ModeloCatalogos->updateCatalogo('medicion_temp_nom15_elevadas2',$dataf,array('id'=>$DATAm[$j]->id));
                    }
                }
            }*/
        }
        /*$DATAc = json_decode($mediciones);   //acá me quedo
        //$dataf = array(); $dataf_pre = array('id_nom_punto'=>$id_pto);
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->ciclo_med!="" && $DATAc[$i]->temp_ini!="" && $DATAc[$i]->temp_fin!="" && isset($DATAc[$i]->tipo)){
                $dataf = array(
                    'id_nom_punto'=>$id_pto,
                    'ciclo_med'=>$DATAc[$i]->ciclo_med,
                    'temp_ini'=>$DATAc[$i]->temp_ini,
                    'temp_fin'=>$DATAc[$i]->temp_fin,
                    'tipo'=>$DATAc[$i]->tipo,
                    'tipo_med'=>$DATAc[$i]->tipo_med,
                    'altura'=>$DATAc[$i]->altura_ini,
                    'hora'=>$DATAc[$i]->hora_ini,
                    'tbh'=>$DATAc[$i]->tbh_ini,
                    'tbs'=>$DATAc[$i]->tbs_ini,
                    'tg'=>$DATAc[$i]->tg_ini,
                    'reg'=>date('Y-m-d H:i:s')
                );
                if($DATAc[$i]->id==0 || $DATAc[$i]->id==""){
                    $this->ModeloCatalogos->Insert('medicion_temp_nom15_elevadas2',$dataf);
                }else{
                    unset($dataf["id_nom_punto"]);
                    $this->ModeloCatalogos->updateCatalogo('medicion_temp_nom15_elevadas2',$dataf,array('id'=>$DATAc[$i]->id));
                }
            }
        }*/

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }

        echo json_encode(array("status"=>$status,"id_nom"=>$idnom,"id_pto"=>$id_pto,"band_exis"=>$band_exis,"ids_pto"=>json_encode($ids_pto)));

    }

    //function saveMediciones($id_pto,$mediciones,$band_exis){v
    function saveMediciones(){
        log_message('error','---------------------------------------------');
        //$mediciones = $params['mediciones'];
        //log_message('error', 'id_pto saveMediciones: '.$id_pto);
        //log_message('error', 'band_exis saveMediciones: '.$band_exis);

        $params = $this->input->post();
        $id_nom = $params['id_nom'];
        $id_pto = $params['id_pto'];
        $band_exis = $params['band_exis'];
        $mediciones = $params['mediciones'];

        $DATAc = json_decode($mediciones);

        //log_message('error','$Band: '.json_encode($band_exis));

        //log_message('error','$DATAc: '.json_encode($DATAc));
        //log_message('error','$DATAcount: '.count($DATAc));

        //$dataf = array(); $dataf_pre = array('id_nom_punto'=>$id_pto);

        $this->db->trans_start();
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->ciclo_med!="" && $DATAc[$i]->temp_ini!="" && $DATAc[$i]->temp_fin!="" && isset($DATAc[$i]->tipo)){
                //log_message('error','$Hora_ini: '.$DATAc[$i]->hora_ini);

                $dataf = array(
                    //'id_nom'=>$id_nom,
                    'id_nom_punto'=>$id_pto,
                    'ciclo_med'=>$DATAc[$i]->ciclo_med,
                    'temp_ini'=>$DATAc[$i]->temp_ini,
                    'temp_fin'=>$DATAc[$i]->temp_fin,
                    'tipo'=>$DATAc[$i]->tipo,
                    'tipo_med'=>$DATAc[$i]->tipo_med,
                    'altura'=>$DATAc[$i]->altura_ini,
                    'hora'=>$DATAc[$i]->hora_ini,
                    'tbh'=>$DATAc[$i]->tbh_ini,
                    'tbs'=>$DATAc[$i]->tbs_ini,
                    'tg'=>$DATAc[$i]->tg_ini,
                    'reg'=>date('Y-m-d H:i:s')
                );

                log_message('error','DataF: '.json_encode($dataf));

                if($DATAc[$i]->id==0 || $DATAc[$i]->id=="" ){//&& $band_exis==0  || && $band_exis==0
                    $dataf['insert_user']=$this->id_usuario;
                    log_message('error','Insert');
                    $this->ModeloCatalogos->Insert('medicion_temp_nom15_elevadas2',$dataf);
                }else if($DATAc[$i]->id!=0){
                    //unset($dataf["id_nom"]);
                    unset($dataf["id_nom_punto"]);
                    $dataf['update_user']=$this->id_usuario;
                    $dataf['update_reg']=$this->fechahoy;
                    log_message('error','Update');
                    $this->ModeloCatalogos->updateCatalogo('medicion_temp_nom15_elevadas2',$dataf,array('id'=>$DATAc[$i]->id));
                }
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
    }
    /* *********************************************/
    public function nom025($id=0) {
        $data['idid']=$id;
        $data['numinfo']='AHISA/EP-02';
        $data['razonsocial']='AHISA LABORATORIO DE PRUEBAS, S. DE R.L. DE C.V.';
        $data['fecha']=$this->fechahoyc;
        $data['idequipo']='M154879CM1371120';
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('nom/nom025',$data);
        $this->load->view('footer');
        $this->load->view('nom/nom025js');
    }
    function inserupdate25(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $idordenes = $params['idordenes'];
        $razon_social = $params['razon_social'];
        $equipo = $params['equipo'];
        $arraypuntos = $params['arraypuntos'];
        $nom = $params['nom'];
        $fecha = $params['fecha'];
        $veri_ini = $params['veri_ini'];
        $veri_fin = $params['veri_fin'];
        $criterio = $params['criterio'];
        $cumple_criterio = $params['cumple_criterio'];
        $observaciones = $params['observaciones'];
        $id_chs = $params['id_chs'];
        $fechas_ptos = $params['fechas_ptos'];
        $cont_nom=0;

        $this->db->trans_start();
        $datosnom=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$idnom,"id_chs"=>$id_chs));
        foreach($datosnom->result() as $d){
            $cont_nom++;
        }

        //log_message('error', 'cont_nom: '.$cont_nom); //ACA ANDO
        //if($cont_nom==0){
        if($cont_nom==0){
            $idnom = $this->ModeloCatalogos->Insert('nom',array('idordenes'=>$idordenes,'razon_social'=>$razon_social,'equipo'=>$equipo,'reg'=>$this->fechahoy,'norma'=>$nom,'fecha'=>$fecha,"veri_ini"=>$veri_ini,"veri_fin"=>$veri_fin,"criterio"=>$criterio,"cumple_criterio"=>$cumple_criterio,"observaciones"=>$observaciones,"id_chs"=>$id_chs,'insert_user'=>$this->id_usuario));
        }else{
            $idnom = $idnom;
            $this->ModeloCatalogos->updateCatalogo('nom',array('razon_social'=>$razon_social,'equipo'=>$equipo,'fecha'=>$fecha,"veri_ini"=>$veri_ini,"veri_fin"=>$veri_fin,"criterio"=>$criterio,"cumple_criterio"=>$cumple_criterio,"observaciones"=>$observaciones,'update_user'=>$this->id_usuario,'update_reg'=>$this->fechahoy),array('idnom'=>$idnom));
        }

        $DATAc = json_decode($arraypuntos);  
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->id==0){
                $dataa = array(
                    'idnom'=>$idnom,
                    'num_punto'=>$DATAc[$i]->num_punto,
                    'identificacion'=>$DATAc[$i]->identificacion,
                    'fecha'=>$DATAc[$i]->fecha,
                    'h1'=>$DATAc[$i]->h1,
                    'area'=>$DATAc[$i]->area,
                    'identifica'=>$DATAc[$i]->identifica1,
                    'plano_t1'=>$DATAc[$i]->plano_t1,
                    'h1_medicion_a'=>$DATAc[$i]->h1_medicion_a,
                    'h1_medicion_b'=>$DATAc[$i]->h1_medicion_b,
                    'h1_mdicion_c'=>$DATAc[$i]->h1_mdicion_c,
                    'h2'=>$DATAc[$i]->h2,
                    //'area2'=>$DATAc[$i]->area2,
                    'identifica2'=>$DATAc[$i]->identifica2,
                    //'plano_t2'=>$DATAc[$i]->plano_t2,
                    'h2_medicion_a'=>$DATAc[$i]->h2_medicion_a,
                    'h2_medicion_b'=>$DATAc[$i]->h2_medicion_b,
                    'h2_medicion_c'=>$DATAc[$i]->h2_medicion_c,
                    'h3'=>$DATAc[$i]->h3,
                    //'area3'=>$DATAc[$i]->area3,
                    //'identifica3'=>$DATAc[$i]->identifica3,
                    //'plano_t3'=>$DATAc[$i]->plano_t3,
                    'h3_medicion_a'=>$DATAc[$i]->h3_medicion_a,
                    'h3_medicion_b'=>$DATAc[$i]->h3_medicion_b,
                    'h3_medicion_c'=>$DATAc[$i]->h3_medicion_c,
                    'nmi'=>$DATAc[$i]->nmi,
                    'h1_e1a'=>$DATAc[$i]->h1_e1a,
                    'h1_e2a'=>$DATAc[$i]->h1_e2a,
                    'h2_e1a'=>$DATAc[$i]->h2_e1a,
                    'h2_e2a'=>$DATAc[$i]->h2_e2a,
                    'h3_e1a'=>$DATAc[$i]->h3_e1a,
                    'h3_e2a'=>$DATAc[$i]->h3_e2a,
                    'h1_e1b'=>$DATAc[$i]->h1_e1b,
                    'h1_e2b'=>$DATAc[$i]->h1_e2b,
                    'h2_e1b'=>$DATAc[$i]->h2_e1b,
                    'h2_e2b'=>$DATAc[$i]->h2_e2b,
                    'h3_e1b'=>$DATAc[$i]->h3_e1b,
                    'h3_e2b'=>$DATAc[$i]->h3_e2b,
                    'insert_user'=>$this->id_usuario,
                    'insert_reg'=>$this->fechahoy
                );
                $this->ModeloCatalogos->Insert('nom25_detalle',$dataa);
            }else{
                $dataa = array(
                    'identificacion'=>$DATAc[$i]->identificacion,
                    'num_punto'=>$DATAc[$i]->num_punto,
                    'fecha'=>$DATAc[$i]->fecha,
                    'h1'=>$DATAc[$i]->h1,
                    'area'=>$DATAc[$i]->area,
                    'identifica'=>$DATAc[$i]->identifica1,
                    'plano_t1'=>$DATAc[$i]->plano_t1,
                    'h1_medicion_a'=>$DATAc[$i]->h1_medicion_a,
                    'h1_medicion_b'=>$DATAc[$i]->h1_medicion_b,
                    'h1_mdicion_c'=>$DATAc[$i]->h1_mdicion_c,
                    'h2'=>$DATAc[$i]->h2,
                    //'area2'=>$DATAc[$i]->area2,
                    'identifica2'=>$DATAc[$i]->identifica2,
                    //'plano_t2'=>$DATAc[$i]->plano_t2,
                    'h2_medicion_a'=>$DATAc[$i]->h2_medicion_a,
                    'h2_medicion_b'=>$DATAc[$i]->h2_medicion_b,
                    'h2_medicion_c'=>$DATAc[$i]->h2_medicion_c,
                    'h3'=>$DATAc[$i]->h3,
                    //'area3'=>$DATAc[$i]->area3,
                    //'identifica3'=>$DATAc[$i]->identifica3,
                    //'plano_t3'=>$DATAc[$i]->plano_t3,
                    'h3_medicion_a'=>$DATAc[$i]->h3_medicion_a,
                    'h3_medicion_b'=>$DATAc[$i]->h3_medicion_b,
                    'h3_medicion_c'=>$DATAc[$i]->h3_medicion_c,
                    'nmi'=>$DATAc[$i]->nmi,
                    'h1_e1a'=>$DATAc[$i]->h1_e1a,
                    'h1_e2a'=>$DATAc[$i]->h1_e2a,
                    'h2_e1a'=>$DATAc[$i]->h2_e1a,
                    'h2_e2a'=>$DATAc[$i]->h2_e2a,
                    'h3_e1a'=>$DATAc[$i]->h3_e1a,
                    'h3_e2a'=>$DATAc[$i]->h3_e2a,
                    'h1_e1b'=>$DATAc[$i]->h1_e1b,
                    'h1_e2b'=>$DATAc[$i]->h1_e2b,
                    'h2_e1b'=>$DATAc[$i]->h2_e1b,
                    'h2_e2b'=>$DATAc[$i]->h2_e2b,
                    'h3_e1b'=>$DATAc[$i]->h3_e1b,
                    'h3_e2b'=>$DATAc[$i]->h3_e2b,
                    'update_user'=>$this->id_usuario,
                    'update_reg'=>$this->fechahoy
                );
                $this->ModeloCatalogos->updateCatalogo('nom25_detalle',$dataa,array('id'=>$DATAc[$i]->id));
            }
        }
        $DATAc = json_decode($fechas_ptos);  
        for ($i=0;$i<count($DATAc);$i++) {
            $dataf = array(
                    'id_nom25'=>$idnom,
                    //'id'=>$DATAc[$i]->id,
                    'condiciones'=>$DATAc[$i]->condiciones,
                    'fecha'=>$DATAc[$i]->fechas_conds,
            );
            if($DATAc[$i]->id==0){
                $dataf['insert_user']=$this->id_usuario;
                $dataf['insert_reg']=$this->fechahoy;
                $this->ModeloCatalogos->Insert('condiciones_nom25det',$dataf);
            }else{
                $dataf['update_user']=$this->id_usuario;
                $dataf['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('condiciones_nom25det',$dataf,array('id'=>$DATAc[$i]->id));
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo $idnom;
    }
    function infoPuntos025(){
        $id = $this->input->post('idpunto');
        $html_fechas="";
        $getFechas=$this->ModeloNom->getFechasNom25Detalles($id);
        $orden=0;
        $count_puntos=0;
        $count_puntos_n=0;
        foreach($getFechas->result() as $nd){
            $count_puntos=$count_puntos+$nd->puntos;
        }
        foreach($getFechas->result() as $nd){
            //log_message('error','fecha '.$nd->fecha);
            //$count_puntos=$count_puntos+$nd->puntos;
            $html_fechas.="<option value='".$nd->fecha."' data-puntos='".$nd->puntos."' data-orden='".$orden."' data-cpuntos='".$count_puntos."' >".$nd->fecha."</option>";
            
            $orden++;
        }
        echo $html_fechas;
    }
    function info025(){
        $id = $this->input->post('idpunto');
        $id_chs = $this->input->post('id_chs');
        $fecha = $this->input->post('fecha');
        
        $datosnom=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$id,"id_chs"=>$id_chs)); //tambien mandar el id de cotizacion servicios
        $datosnom=$datosnom->result();
        
        $array = array(); $vac="vacio";
        if($fecha!="0"){ //si pide fecha de consulta - ptos
            $fecha_cons=$fecha;
        }else{ //es fecha 0, 
            $fecha_cons=$datosnom[0]->fecha;
        }
        /*$html_fechas="";
        $getFechas=$this->ModeloNom->getFechasNom25Detalles($id);
        foreach($getFechas->result() as $nd){
            $html_fechas.="<option value='".$nd->fecha."'>".$nd->fecha."</option>";
        }*/

        //$datosnomdetalle=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('idnom'=>$id,"fecha"=>$fecha_cons,'activo'=>1));
        $datosnomdetalle=$this->ModeloNom->getPuntosDetalle25(array('idnom'=>$id,"fecha"=>$fecha_cons,'activo'=>1));
        $condiciones=$this->ModeloCatalogos->getselectwheren('condiciones_nom25det',array('id_nom25'=>$id,"fecha"=>$fecha_cons,'estatus'=>1));
        if($datosnom){
            $datosnom=$datosnom[0];
            $array = array(
                'idnom'=>$datosnom->idnom,
                'idordenes'=>$datosnom->idordenes,
                'razon_social'=>$datosnom->razon_social,
                'equipo'=>$datosnom->equipo,
                'reg'=>$datosnom->reg,
                'fecha'=>$datosnom->fecha,
                'fecha_cons'=>$fecha_cons,

                'veri_ini'=>$datosnom->veri_ini,
                'veri_fin'=>$datosnom->veri_fin,
                'criterio'=>$datosnom->criterio,
                'cumple_criterio'=>$datosnom->cumple_criterio,
                'observaciones'=>$datosnom->observaciones,
                //'html_fechas'=>$html_fechas,
                'detalle'=>$datosnomdetalle->result(),
                'condiciones'=>$condiciones->result()
            );
            $vac="novacio";
        }
        echo json_encode(array("data"=>$array,"vac"=>$vac));
        //echo json_encode($array);
    }
    function nom25view($idnom,$id_chs){
        $data['idnom']=$idnom;
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$idnom,"id_chs"=>$id_chs));
        $datosnom=$data['datosnom']->row();
        $idordenes=$datosnom->idordenes;
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento',array('id_orden'=>$idordenes,"id_chs"=>$id_chs));
        $datosrec=$data['datosrec']->row();
        $data["num_informe"]=$datosrec->num_informe_rec;
        $data["razon_cliente"]=$datosrec->cliente;
        //$data['datosnomdetalle']=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('idnom'=>$idnom,'activo'=>1));
        $data["cliente"]='';
        if($idordenes>0){
            //log_message('error','idordenes '.$idordenes);
            
            $getcli=$this->ModeloCatalogos->getClienteOrden($idordenes);
            $data["cliente"]=$getcli->cliente;
            $data["razon_cliente"]=$getcli->razoncli;
        }
        $data["getFechas"]=$this->ModeloNom->getFechasNom25Detalles($idnom);
        $this->load->view('nom/nom25view',$data);
    }
    function deletedetallepunto(){
        $id = $this->input->post('iddetalle');
        $id_nom = $this->input->post('id_nom');
        $fecha = $this->input->post('fecha');
        $num = $this->input->post('num');
        $this->ModeloCatalogos->updateCatalogo('nom25_detalle',array('activo'=>0),array('id'=>$id));
        $getult=$this->ModeloNom->getMaxPunto($id_nom);
        if($getult->ultimo!=$id){
            //$this->ModeloNom->ordenaPuntos($id_nom,$fecha,$id,$num);
        }
        $this->ModeloCatalogos->updateCatalogo('nom25_detalle',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('id'=>$id));
    }
    function nom25viewinserupdatechart(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom25_detalle',array('table_generado1'=>$base64Image),array('id'=>$codigo));
    }
    function nominserupdatetableg1(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom',array('tablageneral1'=>$base64Image),array('idnom'=>$codigo));
    }
    function nominserupdatetableg2(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom',array('tablageneral2'=>$base64Image),array('idnom'=>$codigo));
    }
    function nominserupdatetableg1char(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom',array('tablageneral1char'=>$base64Image),array('idnom'=>$codigo));
    }
    function nominserupdatetableg2char(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom',array('tablageneral2char'=>$base64Image),array('idnom'=>$codigo));
    }

    function portadaNoms($idnom,$idorden,$id_chs,$nom){
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data["nom"]=""; $data["sub"]="";
        if($nom==11){
            $data["nom"]="NOM-011-STPS-2001";
            $data["sub"]='"CONDICIONES DE NIVEL DE EXPOSICIÓN A RUIDO (NER) Y NIVEL DE PRESIÓN ACÚSTICA (NPA)"';
            $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom11',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
            $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom11',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        }else if($nom==25){
            $data["nom"]="NOM-025-STPS-2008";
            $data["sub"]='"CONDICIONES DE ILUMINACIÓN EN LOS CENTROS DE TRABAJO"';

            $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
            $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        }else if($nom==22){
            $data["nom"]="NOM-022-STPS-2015";
            $data["sub"]='"CONDICIONES DE ELECTRICIDAD ESTÁTICA EN LOS CENTROS DE TRABAJO"';
            $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom22',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
            $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom22',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        }else if($nom==81){
            $data["nom"]="NOM-081-SEMARNAT-1994";
            $data["sub"]='"LÍMITES MÁXIMOS PERMISIBLES DE EMISIÓN DE RUIDO DE LAS FUENTES FIJAS Y SU MÉTODO DE MEDICIÓN"';
            $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$idorden,"id_chs"=>$id_chs,'activo'=>1));
            $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom81',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        }else if($nom==15){
            $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$idorden,"id_chs"=>$id_chs,'activo'=>1));
            $data['datosrec']=$this->ModeloNom->getRecoTipoNom15($idorden,$id_chs);
            $datosnom=$data['datosnom']->row();
            if($data['datosrec']->num_rows()>0){
                $datosrec=$data['datosrec']->row();
                $tipo_norma=$datosrec->tipo_norma;
            }
            if($tipo_norma==1){ //abatidas
               $data["sub"]='"CONDICIONES DE TEMPERATURAS TÉRMICAS ABATIDAS"';
            }else { //elevadaS
                $data["sub"]='"CONDICIONES DE TEMPERATURAS TÉRMICAS ELEVADAS"';
            }
            $data["nom"]="NOM-015-STPS-2001";
        }
        $this->load->view('reportes/portadaNoms',$data);
    }

    function nompdfview2($idnom,$idorden,$id_chs){ //para pruebas de creacion de hoja exrtra - portada
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
        $data['datosnomiv']=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$idnom,"nom"=>2,'estatus'=>1));
        $data['datosnomv']=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$idnom,"nom"=>25,'estatus'=>1));
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"25",'estatus'=>1));
        $data['datosnomdetalle']=$this->ModeloCatalogos->getPtosFecha(array('idnom'=>$idnom,'activo'=>1));//ok
        $data['datosnomdetalle2']=$this->ModeloCatalogos->nom25_detalle_groupby($idnom);
        $datosri=$this->ModeloCatalogos->getselectwheren('reconocimiento_inicial',array('orden'=>$idorden,"id_chs"=>$id_chs,'activo'=>1)); //ok
        $data['datosri']=$datosri;
        $datosrir=$datosri->result();
        $data['id_reconocimientoinicial']=$datosrir[0]->id;
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $getrec=$data['datosrec']->row();
        $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
        $getname=$getname->row();
        $data["estado_name"]="";
        if(!is_numeric($data["estado_name"])){
            $data["estado_name"]=utf8_encode($getname->estado);
        }
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));
        $datosnom=$data['datosnom']->row();
        $data["txt_intro"] = $datosnom->txt_intro;
        $data["txt_criterios_metodo"]='';
        $datosri=$datosri->result();
        $data["txt_criterios_metodo"]=$datosri[0]->estrategia;
        $data['equi']=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$datosnom->equipo));
        $data["get_conclu"]=$this->ModeloNom->getPuntosConclusion($idnom);
        $getcli=$this->ModeloCatalogos->getClienteOrden($idorden);
        $data["cliente"]=$getcli->cliente;
        $data["id_nom_ctr"]=$idnom;
        $this->load->view('reportes/nompdfview2_2',$data);
    }

    function nompdfview($idnom,$idorden,$id_chs){
        //error_reporting(0);
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);

        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
        $data['datosnomiv']=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$idnom,"nom"=>2,'estatus'=>1));
        $data['datosnomv']=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$idnom,"nom"=>25,'estatus'=>1));
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"25",'estatus'=>1));
        $data['datosnomdetalle']=$this->ModeloCatalogos->getPtosFecha(array('idnom'=>$idnom,'activo'=>1));//ok
        $data['datosnomdetalle2']=$this->ModeloCatalogos->nom25_detalle_groupby($idnom);
        $datosri=$this->ModeloCatalogos->getselectwheren('reconocimiento_inicial',array('orden'=>$idorden,"id_chs"=>$id_chs,'activo'=>1)); //ok
        $data['datosri']=$datosri;
        $datosrir=$datosri->result();
        $data['id_reconocimientoinicial']=$datosrir[0]->id;
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $getrec=$data['datosrec']->row();
        $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
        $getname=$getname->row();
        $data["estado_name"]="";
        if(!is_numeric($data["estado_name"])){
            $data["estado_name"]=utf8_encode($getname->estado);
        }
        //idnom = 2 porque es para traer las imagenes de la norma 25 (verificar con arthur si esta es una funcion generica o existirá 1 por norma)
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));

        $datosnom=$data['datosnom']->row();
        $data["txt_intro"] = $datosnom->txt_intro;
        //log_message('error', 'txt_intro: '.$data["txt_intro"]);
        //$data["txt_desc_proc"] = $datosnom->txt_desc_proc;
        //$data['desc_proc']=$this->ModeloCatalogos->getselectwheren('condiciones_nom25det',array('id_nom25'=>$idnom,'estatus'=>1));
        //$data["txt_criterios_metodo"] = $datosnom->txt_criterios_metodo;
        $data["txt_criterios_metodo"]='';
        $datosri=$datosri->result();
        $data["txt_criterios_metodo"]=$datosri[0]->estrategia;
        //foreach ($datosri->result() as $item) {
        //    $data["txt_criterios_metodo"]=$item->estrategia;
        //}
        $data['equi']=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$datosnom->equipo));

        $data["get_conclu"]=$this->ModeloNom->getPuntosConclusion($idnom);
        $getcli=$this->ModeloCatalogos->getClienteOrden($idorden);
        $data["cliente"]=$getcli->cliente;
        $data["id_nom_ctr"]=$idnom;

        $this->load->view('reportes/nompdfview',$data);
    }

    function nompdfview11($idnom,$idorden,$id_chs){
        //error_reporting(0);
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom11',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
        $data['datosnomv']=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$idnom,"nom"=>11,'estatus'=>1));
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"11",'estatus'=>1));
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom11',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $getrec=$data['datosrec']->row();
        $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
        $getname=$getname->row();
        $data["estado_name"]="";
        if(!is_numeric($data["estado_name"])){
            $data["estado_name"]=utf8_encode($getname->estado);
        }
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));//son iguales para todas
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));//son iguales para todas

        $datosnom=$data['datosnom']->row();
        //$data["idnom"] = $idnom;
        $data["fecha"] = $datosnom->fecha;
        $data["folio"] = $datosnom->num_informe;
        $data["txt_desc_proc"] = $datosnom->txt_desc_proc;
        $data["txt_criterios_metodo"] = $datosnom->txt_criterios_metodo;
        $data["txt_proceso_fabrica"] = $datosnom->txt_proceso_fabrica;
        $data["get_conclu"]=$this->ModeloCatalogos->getselectwheren('conclusiones_nom11',array('id_nom'=>$idnom));
        $this->load->view('reportes/nompdfview11',$data);
    }

    function nompdfview22($idnom,$idorden,$id_chs){
        //error_reporting(0);
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom22',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom22',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $getrec=$data['datosrec']->row();
        $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
        $getname=$getname->row();
        $data["estado_name"]="";
        if(!is_numeric($data["estado_name"])){
            $data["estado_name"]=utf8_encode($getname->estado);
        }
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"22",'estatus'=>1));
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));//son iguales para todas
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));//son iguales para todas

        $datosnom=$data['datosnom']->row();
        $data["fecha"] = $datosnom->fecha;
        $data["folio"] = $datosnom->num_informe;
        $data["id_higro"] = $datosnom->id_higro;
        $data["get_conclu"]=$this->ModeloCatalogos->getselectwheren('conclusiones_nom22',array('id_nom'=>$idnom));
        $this->load->view('reportes/nompdfview22',$data);
    }

    function nompdfview81($idnom,$idorden,$id_chs){
        //error_reporting(0);
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);
        //$data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom81',array('idnom'=>$idnom,"id_chs"=>$id_chs)); //ok
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$idorden,"id_chs"=>$id_chs,'activo'=>1));
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom81',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $getrec=$data['datosrec']->row();
        $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
        $getname=$getname->row();
        $data["estado_name"]="";
        if(!is_numeric($data["estado_name"])){
            $data["estado_name"]=utf8_encode($getname->estado);
        }
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"81",'estatus'=>1));
        //$data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom'=>"4",'tipo'=>1,'estatus'=>1));//son iguales para todas
        //$data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom'=>"4",'tipo'=>2,'estatus'=>1));//son iguales para todas
        //Verificar con jesus
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));

        $datosnom=$data['datosnom']->row();
        $data["fecha"] = $datosnom->fecha;
        $data["folio"] = $datosnom->num_informe;
        $data["txt_desc_opera"] = $datosnom->txt_desc_opera;
        $data["txt_eventualidades"] = $datosnom->txt_eventualidades;
        $data["txt_adiciones_desv"] = $datosnom->txt_adiciones_desv;
        $data["id_chs"]=$id_chs;
        //$data["get_conclu"]=$this->ModeloCatalogos->getselectwheren('conclusiones_nom22',array('id_nom'=>$idnom));
        $this->load->view('reportes/nompdfview81',$data);
    }

    function nompdfview15($idnom,$idorden,$id_chs){
        //error_reporting(0);
        $data['idnom']=$idnom;
        $data['id_reconocimiento']=0;
        $data['datosoc']=$this->ModeloCatalogos->infoordencotizacion($idorden);
        $data['datosnom']=$this->ModeloCatalogos->getselectwheren('nom15',array('idordenes'=>$idorden,"id_chs"=>$id_chs,'activo'=>1));
        //$data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom15',array('id_orden'=>$idorden,"id_chs"=>$id_chs));
        $data['datosrec']=$this->ModeloNom->getRecoTipoNom15($idorden,$id_chs);
        $data['datosnomiv']=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$idnom,"nom"=>5,'estatus'=>1));
        $data['datosnomplanos']=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$idnom,'nom'=>"15",'estatus'=>1));
        $data['datosacre']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>1,'estatus'=>1));//son iguales para todas
        $data['datosacre2']=$this->ModeloCatalogos->getselectwheren('imagenes_acre_apro',array('idnom !='=>"4",'tipo'=>2,'estatus'=>1));//son iguales para todas

        $datosnom=$data['datosnom']->row();
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $tipo_norma=$datosrec->tipo_norma;
            $data["fecha"] = $datosrec->fecha;
            $getrec=$data['datosrec']->row();
            $getname=$this->ModeloCatalogos->getselectwheren('estados',array('id'=>$getrec->estado));
            $getname=$getname->row();
            $data["estado_name"]="";
            if(!is_numeric($data["estado_name"])){
                $data["estado_name"]=utf8_encode($getname->estado);
            }
        }

        //$data["fecha"] = $datosnom->fecha;
        $data["razon_social"]=$datosrec->cliente;
        $data["folio"] = $datosnom->num_informe;
        //$data["grafica_abatida"] = is_null($datosnom->grafica_abatida) ? '' : $datosnom->grafica_abatida;
        $data["grafica_abatida"] = $datosnom->grafica_abatida;
        $data["id_chs"]=$id_chs;
        if($tipo_norma==1){
            $data['ptos']=$this->ModeloCatalogos->getselectwheren('nom15_punto',array('id_nom'=>$idnom,'activo'=>1));//puntos levantamiento
            $this->load->view('reportes/nompdfview15',$data);
        }
        else{
            $data['ptos']=$this->ModeloCatalogos->getselectwheren('nom15_punto_elevada',array('id_nom'=>$idnom,'activo'=>1));//puntos levantamiento
            $this->load->view('reportes/nompdfview15Elevada',$data);
        }
    }

    function tablasnom(){
        $this->load->view('reportes/tablasnom');
    }

    function reconocimientoServicio($id,$idorden,$idcli=0,$id_chs){ //id reconocimiento, idnom es el de orden
        $data['id_orden']=$id;
        //$getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento',array("id_orden"=>$idorden,"id_chs"=>$id_chs));
        
        $getnom=$this->ModeloNom->getNormaChs($id_chs);
        $getcli=$this->Catalogos_model->getItemCatalogo('clientes',$idcli);
        
        $data["id_reco"]=0;
        $data["id_chs"]=$id_chs;
        $norma=$getnom->norma;
        $data['puestostrabajo']=$this->ModeloCatalogos->getselectwheren('puestos_trabajo',array('activo'=>1));
        $data["estados"]=$this->Catalogos_model->getCatalogo("estados");
        if($norma==1){
            $data["norma"]=11;
            $getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento_nom11',array("id_orden"=>$id,"id_chs"=>$id_chs));
        }else if($norma==2){
            $data["norma"]=25;
            $getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento',array("id_orden"=>$id,"id_chs"=>$id_chs));
        }else if($norma==3){
            $data["norma"]=22;
            $getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento_nom22',array("id_orden"=>$id,"id_chs"=>$id_chs));
        }else if($norma==4){
            $data["norma"]=81;
            $getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento_nom81',array("id_orden"=>$id,"id_chs"=>$id_chs));
        }else if($norma==5){
            $data["norma"]=15;
            $getrec=$this->Catalogos_model->getCatalogoRow('reconocimiento_nom15',array("id_orden"=>$id,"id_chs"=>$id_chs));
        }

        //log_message('error', 'norma: '.$norma);
        if(isset($getrec->id)){
            $data["cli"] = $getrec;
            $data["id"]=$getrec->id;
            $data["id_reco"]=$getrec->id;
            //$data["norma"]=25;
        }else{
            $data["cli"] = $getcli;
            $data["id"]=0;
            $getedo=$this->Catalogos_model->getCatalogoRow('estados',array("id"=>$getcli->estado));
            $data["edo"]=$getedo->estado;
        }
        $data["orden"]=$id;

        /*$cotizacion_id=0;
        $respuesta=$this->ModeloCatalogos->getselectwheren('ordenes',array('id'=>$id));
        foreach ($respuesta->result() as $item) {
            $cotizacion_id=$item->cotizacion_id;
        }
        $respuesta=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_servicios',array('cotizacion_id'=>$cotizacion_id));
        $servicio_id=0;
        $norma=0;
        $name="";
        foreach ($respuesta->result() as $item) {
            $servicio_id=$item->servicio_id;
        }
        if($servicio_id>0){
            $respuesta=$this->ModeloCatalogos->getselectwheren('servicios_ahisa',array('id'=>$servicio_id));
            
            foreach ($respuesta->result() as $item) {
                $norma=$item->norma;
            }*/
            if($norma==1){
                $name='norma11'; 
            }
            if($norma==2){
                $name='norma25'; 
            }
            if($norma==3){
                //Nom-022
                $name='norma22';
            }
            if($norma==4){
                //Nom-081
                $name="norma81";
            }
            if($norma==5){
                //Nom-015
                $name="norma15";
            }
        //}
        $data["id_reco_paso2"]=0; $data["id_reco_paso2_2"]=0;


        if($norma==1){ //para la nom 11
            $data["id_reco_paso2"]=0; $data["id_reco_paso2_2"]=0;
            if($data["id_reco"]>0){
                $respuesta=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$data["id_reco"],"estatus"=>1)); //para la primera tabla del paso 2
                foreach ($respuesta->result() as $item) {
                    $data["id_reco_paso2"]=$item->id;
                }
                $respuesta2=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso3_nom11',array('id_reconocimiento'=>$data["id_reco"],"estatus"=>1)); //para la segunda tabla - paso 3
                foreach ($respuesta2->result() as $item) {
                    $data["id_reco_paso2_2"]=$item->id;
                }
                $data["getp4"]=$this->Catalogos_model->getCatalogoRow('reconocimiento_paso4_nom11',array("id_reconocimiento"=>$data["id_reco"]));
            }
        }
        if($norma==2){ //para la nom 25
            $data["id_reco_paso2"]=0; $data["id_reco_paso2_2"]=0;
            if($data["id_reco"]>0){
                $respuesta=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom25',array('id_reconocimiento'=>$data["id_reco"],"tipo"=>1,"estatus"=>1)); //para la primera tabla del paso 2
                foreach ($respuesta->result() as $item) {
                    $data["id_reco_paso2"]=$item->id;
                }
                $respuesta2=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom25',array('id_reconocimiento'=>$data["id_reco"],"tipo_tabla"=>2,"estatus"=>1)); //para la segunda tabla - paso 3
                foreach ($respuesta2->result() as $item) {
                    $data["id_reco_paso2_2"]=$item->id;
                }
            }
        }

        if($norma==5){ //para la nom 15
            $data["id_reco_paso2"]=0;
            if($data["id_reco"]>0){
                $respuesta=$this->ModeloCatalogos->getselectwheren('reconocimiento_areas_nom15',array('id_reconocimiento'=>$data["id_reco"],"estatus"=>1)); //para la primera tabla del paso 2
                foreach ($respuesta->result() as $item) {
                    $data["id_reco_paso2"]=$item->id;
                }
            }
        }
        //log_message('error','NAME: '.$name);
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('reconocimiento/'.$name.'',$data); 
        $this->load->view('footer');
        $this->load->view('reconocimiento/'.$name.'js');
        if($norma==2){
            //$this->load->view('reconocimiento/paso2_'.$name.'js');
        }
    }

    function reconocimientoServicio2($id,$norma){
        $data["id_reco"]=$id;
        $data["norma"]=$norma;
        $data["id"]=0;
        if($norma==1){
            $name='norma11'; 
        }
        if($norma==2){
            $name='norma25'; 
        }
        if($norma==3){
            //Nom-022
            $name='norma22';
        }
        if($norma==4){
            //Nom-044
        }
        if($norma==5){
            //Nom-054
        }
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('reconocimiento/paso2_'.$name.'',$data); 
        $this->load->view('footer');
        $this->load->view('reconocimiento/paso2_'.$name.'js');
    }

    function submit_Reco1(){ //guardar primer paso de reconocimiento
        $data = $this->input->post();
        $id = $data['id'];

        if($data['nom']==11){
            $table="reconocimiento_nom11";
            unset($data["nom"]);
        }
        else if($data['nom']==25){
            $table="reconocimiento";
            unset($data["nom"]);
        }else if($data['nom']==22){
            $table="reconocimiento_nom22";
            unset($data["nom"]);
        }else if($data['nom']==81){
            $table="reconocimiento_nom81";
            unset($data["nom"]);
        }else if($data['nom']==15){
            $table="reconocimiento_nom15";
            unset($data["nom"]);
        }

        $data["reg"]=$this->fechahoy;
        if($id==0){
            $data['insert_user']=$this->id_usuario;
            $id = $this->ModeloCatalogos->Insert($table,$data);
        }else{
            $data['update_user']=$this->id_usuario;
            $data['update_reg']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo($table,$data,array('id'=>$id));
        }
        echo $id;
    }

    function submit_RecoP4(){ //guardar cuarto paso de reconocimiento
        $data = $this->input->post();
        $id = $data['id'];

        if($id==0){
            $data['insert_user']=$this->id_usuario;
            $data['insert_reg']=$this->fechahoy;
            $id = $this->ModeloCatalogos->Insert("reconocimiento_paso4_nom11",$data);
        }else{
            $data['update_user']=$this->id_usuario;
            $data['update_reg']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo("reconocimiento_paso4_nom11",$data,array('id'=>$id));
        }
        echo $id;
    }

    function submit_Reco2(){ //guardar segundo paso de reconocimiento
        $data = $this->input->post('data');

        $this->db->trans_start();

        $DATA = json_decode($data);
        for ($i=0; $i<count($DATA); $i++) {
            if($DATA[$i]->nom==11){
                $arra_cs=array(
                    "id"=>$DATA[$i]->id,
                    "id_reconocimiento"=>$DATA[$i]->id_reconocimiento,
                    "num" =>$DATA[$i]->num,
                    "area"=>$DATA[$i]->area,
                    "puesto"=>$DATA[$i]->puesto,
                    "num_trabaja" =>$DATA[$i]->num_trabaja,
                    "descrip"=>$DATA[$i]->descrip,
                    "tiempo_expo"=>$DATA[$i]->tiempo_expo,
                    "tipo"=>1
                );
                if($DATA[$i]->id==0 && $DATA[$i]->area!=""){
                    $arra_cs['insert_user']=$this->id_usuario;
                    $arra_cs['insert_reg']=$this->fechahoy;
                    $id=$this->ModeloCatalogos->Insert('reconocimiento_paso2_nom11',$arra_cs);     
                }else if($DATA[$i]->id>0 && $DATA[$i]->area!=""){
                    $id=$DATA[$i]->id;
                    $arra_cs['update_user']=$this->id_usuario;
                    $arra_cs['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('reconocimiento_paso2_nom11',$arra_cs,array('id'=>$id));
                }
            }
            else if($DATA[$i]->nom==25){
                $arra_cs=array(
                    "id"=>$DATA[$i]->id,
                    "id_reconocimiento"=>$DATA[$i]->id_reconocimiento,
                    "num" =>$DATA[$i]->num,
                    "area"=>$DATA[$i]->area,
                    "requiere_ilum"=>$DATA[$i]->requiere_ilum,
                    /*"puesto"=>$DATA[$i]->puesto,
                    "num_trabaja" =>$DATA[$i]->num_trabaja,
                    "tarea_visual"=>$DATA[$i]->tarea_visual,*/
                    "tipo_lumin"=>$DATA[$i]->tipo_lumin,
                    "num_lumin"=>$DATA[$i]->num_lumin,
                    "existe_incidencia"=>$DATA[$i]->existe_incidencia,
                    //"actividades"=>$DATA[$i]->actividades,
                    "piso" =>$DATA[$i]->piso,
                    "pared"=>$DATA[$i]->pared,
                    "techo"=>$DATA[$i]->techo,
                    "tipo"=>1
                );
                if($DATA[$i]->id==0 && $DATA[$i]->area!=""){
                    $arra_cs['insert_user']=$this->id_usuario;
                    $arra_cs['insert_reg']=$this->fechahoy;
                    $id=$this->ModeloCatalogos->Insert('reconocimiento_paso2_nom25',$arra_cs);     
                }else if($DATA[$i]->id>0 && $DATA[$i]->area!=""){
                    $id=$DATA[$i]->id;
                    $arra_cs['update_user']=$this->id_usuario;
                    $arra_cs['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('reconocimiento_paso2_nom25',$arra_cs,array('id'=>$id));
                }
            }else if($DATA[$i]->nom==15){
                $arra_cs=array(
                    "id"=>$DATA[$i]->id,
                    "area"=>$DATA[$i]->area,
                    "condicion_termica" =>$DATA[$i]->condicion_termica,
                    "tipo_area"=>$DATA[$i]->tipo_area,
                    "tipo_ventilacion"=>$DATA[$i]->tipo_ventilacion,
                    "generadores_condicion"=>$DATA[$i]->generadores_condicion,
                    "puesto"=>$DATA[$i]->puesto,
                    "tiempo_expo"=>$DATA[$i]->tiempo_expo,
                    "frecuencia"=>$DATA[$i]->frecuencia,
                    "id_reconocimiento" =>$DATA[$i]->id_reconocimiento
                );
                if($DATA[$i]->id==0 && $DATA[$i]->area!=""){
                    $arra_cs['insert_user']=$this->id_usuario;
                    $arra_cs['insert_reg']=$this->fechahoy;
                    $id=$this->ModeloCatalogos->Insert('reconocimiento_areas_nom15',$arra_cs);     
                }else if($DATA[$i]->id>0 && $DATA[$i]->area!=""){
                    $id=$DATA[$i]->id; unset($arra_cs["id"]);
                    $arra_cs['update_user']=$this->id_usuario;
                    $arra_cs['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('reconocimiento_areas_nom15',$arra_cs,array('id'=>$id));
                }
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo $id;
    }

    function submit_Reco2_2(){ //guardar segundo paso de reconocimiento - el nuevo paso agregado -3
        $data = $this->input->post('data');
        $this->db->trans_start();
        $DATA = json_decode($data);
        for ($i=0; $i<count($DATA); $i++) {
            if($DATA[$i]->nom==11){
                $arra_cs=array(
                    "id"=>$DATA[$i]->id,
                    "id_reconocimiento"=>$DATA[$i]->id_reconocimiento,
                    "num" =>$DATA[$i]->num,
                    "fuentes_genera"=>$DATA[$i]->fuentes_genera,
                    "min_db" =>$DATA[$i]->min_db,
                    "max_db"=>$DATA[$i]->max_db,
                    "tipo_ruido"=>$DATA[$i]->tipo_ruido,
                    "num_trabaja_fijo" =>$DATA[$i]->num_trabaja_fijo,
                    "num_trabaja_mov"=>$DATA[$i]->num_trabaja_mov,
                    "gradiente" =>$DATA[$i]->gradiente,
                    "puesto_fijo"=>$DATA[$i]->puesto_fijo,
                    "prioridad"=>$DATA[$i]->prioridad,
                    "personal"=>$DATA[$i]->personal
                );
                if($DATA[$i]->id==0 && $DATA[$i]->fuentes_genera!=""){
                    $arra_cs['insert_user']=$this->id_usuario;
                    $arra_cs['insert_reg']=$this->fechahoy;
                    $id=$this->ModeloCatalogos->Insert('reconocimiento_paso3_nom11',$arra_cs);     
                }else if($DATA[$i]->id>0 && $DATA[$i]->fuentes_genera!=""){
                    $id=$DATA[$i]->id;
                    $arra_cs['update_user']=$this->id_usuario;
                    $arra_cs['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('reconocimiento_paso3_nom11',$arra_cs,array('id'=>$id));
                }
            }
            else if($DATA[$i]->nom==25){
                $arra_cs=array(
                    "id"=>$DATA[$i]->id,
                    "id_reconocimiento"=>$DATA[$i]->id_reconocimiento,
                    "num" =>$DATA[$i]->num,
                    "puesto"=>$DATA[$i]->puesto,
                    "num_trabaja" =>$DATA[$i]->num_trabaja,
                    "actividades"=>$DATA[$i]->actividades,
                    "tarea_visual"=>$DATA[$i]->tarea_visual,
                    "tipo"=>1,
                    "tipo_tabla"=>2
                );
                if($DATA[$i]->id==0 && $DATA[$i]->puesto!=""){
                    $arra_cs['insert_user']=$this->id_usuario;
                    $arra_cs['insert_reg']=$this->fechahoy;
                    $id=$this->ModeloCatalogos->Insert('reconocimiento_paso2_nom25',$arra_cs);     
                }else if($DATA[$i]->id>0 && $DATA[$i]->puesto!=""){
                    $id=$DATA[$i]->id;
                    $arra_cs['update_user']=$this->id_usuario;
                    $arra_cs['update_reg']=$this->fechahoy;
                    $this->ModeloCatalogos->updateCatalogo('reconocimiento_paso2_nom25',$arra_cs,array('id'=>$id));
                }
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo $id;
    }

    function submit_Reco5(){ //guardar 5to paso de reconocimiento
        $data = $this->input->post('data');
        $id=0;
        $this->db->trans_start();
        $DATA = json_decode($data);
        for ($i=0; $i<count($DATA); $i++) {
            $arra_cs=array(
                "indice_area"=>$DATA[$i]->indice_area,
                "puesto_trabajo" =>$DATA[$i]->puesto_trabajo,
                "pasillo_escalera"=>$DATA[$i]->pasillo_escalera
            );

            $id=$DATA[$i]->id;
            $arra_cs['update_user']=$this->id_usuario;
            $arra_cs['update_reg']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo('reconocimiento_paso2_nom25',$arra_cs,array('id'=>$id));
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo $id;
    }

    function getDetalles25(){
        $id = $this->input->post('id_recon');
        $name = $this->input->post('name');
        $tipo = $this->input->post('tipo');
        $tabla = $this->input->post('tabla');
        $array=array('id_reconocimiento'=>$id,$name=>$tipo,'estatus'=>1);
        if($tabla=="reconocimiento_paso2_nom11" || $tabla=="reconocimiento_paso3_nom11" || $tabla=="reconocimiento_areas_nom15"){
            $array=array('id_reconocimiento'=>$id,'estatus'=>1);
        }

        $get_det=$this->ModeloCatalogos->getselectwheren($tabla,$array);
        //$get_det=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom25',array('id_reconocimiento'=>$id,'estatus'=>1));

        $array = array(
                    'detalle'=>$get_det->result()
                );
        echo json_encode($array);
    }

    function deleteDetalleNom25(){
        $id = $this->input->post('id');
        $tabla = $this->input->post('tabla');
        $this->ModeloCatalogos->updateCatalogo($tabla,array('estatus'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('id'=>$id));
    }

    function lentamientoServicio($id,$idnom,$id_chs){
        $respuesta=$this->ModeloCatalogos->getselectwheren('ordenes',array('id'=>$id));
        $cotizacion_id=0;
        $cotizacion_id=0;
        foreach ($respuesta->result() as $item) {
            $cotizacion_id=$item->cotizacion_id;
        }

        $respuesta=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_servicios',array('cotizacion_id'=>$cotizacion_id));
        $servicio_id=0;
        $norma=0;
        $tipo_norma=0;
        foreach ($respuesta->result() as $item) {
            $servicio_id=$item->servicio_id;   
        }
        if($servicio_id>0){
            $respuesta=$this->ModeloCatalogos->getselectwheren('servicios_ahisa',array('id'=>$servicio_id));
            
            foreach ($respuesta->result() as $item) {
                $norma=$item->norma;
                $tipo_norma=$item->tipo_norma;
            }
            if($norma==1){

                $respuesta=$this->ModeloCatalogos->getselectwheren('nom11',array('idordenes'=>$id));
                if($respuesta->num_rows()==0){
                   $this->inserpuntonom11(0,$id,$id_chs); 
                }
                redirect('Nom/add11/'.$id); 
            }
            if($norma==2){
                redirect('Nom/add25/'.$idnom.'/'.$id.'/'.$id_chs); 
            }
            if($norma==3){
                redirect('Nom/add22/'.$id.'/'.$id_chs); 
            }
            if($norma==4){
                redirect('Nom/add81/'.$id.'/'.$id_chs); 
            }
            if($norma==5){
                /*if($tipo_norma==1) //abatida
                    redirect('Nom/add15/'.$id.'/'.$id_chs); 
                else*/
                    redirect('Nom/add15/'.$id.'/'.$id_chs);
            }
        }
    }

    public function submitIntro(){
        $intro = $this->input->post("intro");
        $id_nom = $this->input->post("id_nom");
        $tipo = $this->input->post("tipo");
        $nom = $this->input->post("nom");
        
        if($nom==11) $tabla="nom11";
        else if($nom==25) $tabla="nom";
        else if($nom==22) $tabla="nom22";
        else if($nom==81) $tabla="nom81";

        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_intro'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==2 && $nom!="81"){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_desc_proc'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==2 && $nom=="81"){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_desc_opera'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_criterios_metodo'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_proceso_fabrica'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==5){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_eventualidades'=>$intro),array('idnom'=>$id_nom));
        }else if($tipo==6){
            $this->ModeloCatalogos->updateCatalogo($tabla,array('txt_adiciones_desv'=>$intro),array('idnom'=>$id_nom));
        }
    }

    /* ************************NUEVAS CONCLUSIONES*********************************/
    public function submitConclusion(){
        $data = $this->input->post();
        $data["fecha_reg"]=$this->fechahoy;
        $data["id_usuario"]=$this->id_usuario;
        if($data["nom"]==11) $tabla="conclusiones_nom11";
        if($data["nom"]==25) $tabla="conclusiones";
        if($data["nom"]==22) $tabla="conclusiones_nom22";
        unset($data["nom"]);

        if(isset($data["id"]) && $data["id"]>0){
            $id=$data["id"];
            $this->ModeloCatalogos->updateCatalogo($tabla,$data,array('id'=>$data["id"]));
        }else{
            $id=$this->ModeloCatalogos->Insert($tabla,$data);
        }
        echo $id;
    }

    function submitDetallesConclusion(){ //guardar puntos de las conclusiones
        $data = $this->input->post();

        $array_incidencia = $data['array_incidencia'];
        unset($data['array_incidencia']);

        $array_sinincidencia = $data['array_sinincidencia'];
        unset($data['array_sinincidencia']); 
        $id=0;

        $this->db->trans_start();
        
        $DATA = json_decode($array_incidencia);       
        for ($i=0; $i<count($DATA); $i++) {
            $arra_pc=array(
                "id"=>$DATA[$i]->id,
                "id_conclusion"=>$DATA[$i]->id_conclusion,
                "tipo" =>$DATA[$i]->tipo,
                "tipo_incidencia"=>$DATA[$i]->tipo_incidencia,
                "con_incidencia"=>$DATA[$i]->con_incidencia,
                "sin_incidencia"=>$DATA[$i]->sin_incidencia,
                "medicion"=>$DATA[$i]->medicion,
                "num_ptos_evalua"=>$DATA[$i]->num_ptos_evalua,
                "num_supera"=>$DATA[$i]->num_supera,
                "num_no_supera"=>$DATA[$i]->num_no_supera
            );
            if($DATA[$i]->id==0 && $DATA[$i]->num_ptos_evalua!=""){
                $arra_pc['insert_user']=$this->id_usuario;
                $arra_pc['insert_reg']=$this->fechahoy;
                $id=$this->ModeloCatalogos->Insert('puntos_conclusion',$arra_pc);     
            }else if($DATA[$i]->id>0 /*&& $DATA[$i]->num_ptos_evalua!=""*/){
                $id=$DATA[$i]->id;
                $arra_pc['update_user']=$this->id_usuario;
                $arra_pc['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',$arra_pc,array('id'=>$id));
            }
        }

        $DATAs = json_decode($array_sinincidencia);  
        for ($i=0;$i<count($DATAs);$i++) {
            //$id=$DATA[$i]->id;
            $arra_psin=array(
                "id"=>$DATAs[$i]->id,
                "id_conclusion"=>$DATAs[$i]->id_conclusion,
                "tipo" =>$DATAs[$i]->tipo,
                "tipo_incidencia"=>$DATAs[$i]->tipo_incidencia,
                "con_incidencia"=>$DATAs[$i]->con_incidencia,
                "sin_incidencia"=>$DATAs[$i]->sin_incidencia,
                "medicion"=>$DATAs[$i]->medicion,
                "num_ptos_evalua"=>$DATAs[$i]->num_ptos_evalua,
                "num_supera"=>$DATAs[$i]->num_supera,
                "num_no_supera"=>$DATAs[$i]->num_no_supera
            );
            if($DATAs[$i]->id==0 && $DATAs[$i]->num_ptos_evalua!=""){
                $arra_psin['insert_user']=$this->id_usuario;
                $arra_psin['insert_reg']=$this->fechahoy;
                $id=$this->ModeloCatalogos->Insert('puntos_conclusion',$arra_psin);     
            }else if($DATAs[$i]->id>0 /*&& $DATAs[$i]->num_ptos_evalua!=""*/){
                $id=$DATAs[$i]->id;
                $arra_psin['update_user']=$this->id_usuario;
                $arra_psin['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',$arra_psin,array('id'=>$id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok";
        }
        echo $id;
    }

    function submitSinReflexion(){
        $id = $this->input->post("id_conc");
        $tipo = $this->input->post("tipo");
        $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',array('con_incidencia'=>"","sin_incidencia"=>"","num_ptos_evalua"=>"","num_supera"=>"","num_no_supera"=>"","img_chart"=>""),array('id_conclusion'=>$id,"tipo"=>$tipo));
    }

    function getConclusiones(){
        $id = $this->input->post("id_nom");
        $nom = $this->input->post("nom");
        if($nom==11){
            $result=$this->ModeloCatalogos->getselectwheren('conclusiones_nom11',array('id_nom'=>$id));
            $datosresult=''; $array=array();
            if($result->num_rows()>0){
                $result=$result->row();

            }
            $array = array(
                        'conclusiones'=>$result
                    );
        }
        if($nom==25){
            $result=$this->ModeloCatalogos->getselectwheren('conclusiones',array('id_nom'=>$id));
            $datosresult=''; $array=array();
            if($result->num_rows()>0){
                $result=$result->row();
                $result2=$this->ModeloCatalogos->getselectwheren('puntos_conclusion',array('id_conclusion'=>$result->id));
                $datosresult=$result2->result();

            }
            $array = array(
                        'conclusiones'=>$result,
                        'puntos'=>$datosresult
                    );
        }
        if($nom==22){
            $result=$this->ModeloCatalogos->getselectwheren('conclusiones_nom22',array('id_nom'=>$id));
            $datosresult=''; $array=array();
            if($result->num_rows()>0){
                $result=$result->row();

            }
            $array = array(
                        'conclusiones'=>$result
                    );
        }

        echo json_encode($array);
    }

    function saveCanvasConclusion(){
        $datax = $this->input->post();
        $id=$datax['id'];
        if($datax['nom']==11){
            $charname=$datax['charname'];
            $base64Image=$datax['grafica'];
            $this->ModeloCatalogos->updateCatalogo('conclusiones_nom11',array($charname=>$base64Image),array('id'=>$id));
        }
        if($datax['nom']==25){
            $med=$datax['med'];
            $tipo=$datax['tipo'];
            $tipo_incidencia=$datax['tipo_incidencia'];
            $base64Image=$datax['grafica'];

            /*$result=$this->ModeloCatalogos->getselectwheren('puntos_conclusion',array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));
            $id_det=0;
            if($result->num_rows()>0){
                foreach ($result->result() as $k) {
                    $id_det=$k->id; 
                }
                if($id_det>0){*/
                    $namefile=$id.$med.$tipo.$tipo_incidencia.'.png';
                    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
                    file_put_contents(FCPATH.'public/conclusiones/'.$namefile, $imagenBinaria);
                    $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',array('img_chart'=>$namefile),array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));  
                /*}
            }*/
            if($id_det==0){
                //$this->ModeloCatalogos->Insert('puntos_conclusion',$datax);
            }   
        }
        if($datax['nom']==22){
            $charname=$datax['charname'];
            $base64Image=$datax['grafica'];
            $this->ModeloCatalogos->updateCatalogo('conclusiones_nom22',array($charname=>$base64Image),array('id'=>$id));
        }
    }

    function saveCanvasConclusion25(){
        $datax = $this->input->post();
        $id=$datax['id'];
        $med=$datax['med'];
        $tipo=$datax['tipo'];
        $tipo_incidencia=$datax['tipo_incidencia'];
        $base64Image=$datax['grafica'];

        /*$result=$this->ModeloCatalogos->getselectwheren('puntos_conclusion',array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));
        $id_det=0;
        if($result->num_rows()>0){
            foreach ($result->result() as $k) {
                $id_det=$k->id; 
            }
            if($id_det>0){*/
                $namefile=$id.$med.$tipo.$tipo_incidencia.'.png';
                $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
                file_put_contents(FCPATH.'public/conclusiones/'.$namefile, $imagenBinaria);
                $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',array('img_chart'=>$namefile),array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));  
            /*}
        }*/
    }
    function saveCanvasConclusion25_2(){
        $datax = $this->input->post();
        $id=$datax['id'];
        $med=$datax['med'];
        $tipo=$datax['tipo'];
        $tipo_incidencia=$datax['tipo_incidencia'];
        $base64Image=$datax['grafica'];

        $result=$this->ModeloCatalogos->getselectwheren('puntos_conclusion',array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));
        $id_det=0;
        if($result->num_rows()>0){
            foreach ($result->result() as $k) {
                $id_det=$k->id; 
            }
            if($id_det>0){
                $namefile=$id.$med.$tipo.$tipo_incidencia.'.png';
                $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
                file_put_contents(FCPATH.'public/conclusiones/'.$namefile, $imagenBinaria);
                $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',array('img_chart'=>$namefile),array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));  
            }
        }
    }
    function saveCanvasConclusion25_3(){
        $datax = $this->input->post();
        $id=$datax['id'];
        $med=$datax['med'];
        $tipo=$datax['tipo'];
        $tipo_incidencia=$datax['tipo_incidencia'];
        $base64Image=$datax['grafica'];

        $result=$this->ModeloCatalogos->getselectwheren('puntos_conclusion',array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));
        $id_det=0;
        if($result->num_rows()>0){
            foreach ($result->result() as $k) {
                $id_det=$k->id; 
            }
            if($id_det>0){
                $this->ModeloCatalogos->updateCatalogo('puntos_conclusion',array('img_chart'=>$base64Image),array('id_conclusion'=>$id,"medicion"=>$med,"tipo"=>$tipo,"tipo_incidencia"=>$tipo_incidencia));  
            }
        }
    }

    /* *****************************************/

    public function getIntro(){
        $id_nom = $this->input->post("id_nom");
        $tipo = $this->input->post("tipo");
        $nom = $this->input->post("nom");
        if($nom==11) $tabla="nom11";
        else if($nom==25) $tabla="nom";
        else if($nom==22) $tabla="nom22";
        else if($nom==81) $tabla="nom81";
        $getdat=$this->ModeloCatalogos->getselectwheren($tabla,array('idnom'=>$id_nom));
        if($tipo==1){
            foreach ($getdat->result() as $k) {
                echo $k->txt_intro; 
            }
        }else if($tipo==2 && $nom!="81"){
            foreach ($getdat->result() as $k) {
                echo $k->txt_desc_proc; 
            }
        }else if($tipo==2 && $nom=="81"){
            foreach ($getdat->result() as $k) {
                echo $k->txt_desc_opera; 
            }
        }else if($tipo==3){
            foreach ($getdat->result() as $k) {
                echo $k->txt_criterios_metodo; 
            }
        }else if($tipo==4){
            foreach ($getdat->result() as $k) {
                echo $k->txt_proceso_fabrica; 
            }
        }else if($tipo==5){
            foreach ($getdat->result() as $k) {
                echo $k->txt_eventualidades; 
            }
        }else if($tipo==6){
            foreach ($getdat->result() as $k) {
                echo $k->txt_adiciones_desv; 
            }
        }
    }

    public function getImages($tip){
        $id_nom = $this->input->post("id_nom");
        $nom = $this->input->post("nom");
        $i=0; 
        //log_message('error', 'tip: '.$tip);
        if($nom==11){
            $tipoplano = $this->input->post("tipoplano");
            //log_message('error', 'nom: '.$nom);
            //log_message('error', 'tipoplano: '.$tipoplano);
            if($tip==2){ //planos
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>$tipoplano,"estatus"=>1));
            }else if($tip==3){ //ficha tecnica 
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_fichas',array('id_nom'=>$id_nom,"nom"=>$nom,"estatus"=>1));
            }else if($tip==4){ //programa de mant. a maquinaria y equipo
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$id_nom,"nom"=>$nom,"estatus"=>1));
            }

        }
        if($nom==25){
            if($tip==1){ //descripcion de fabricacion del centro de trabajo
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id_nom,"estatus"=>1));
            }else if($tip==2){  //planos de medicion
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>1,"estatus"=>1));
            }else if($tip==21){ //planos de luminaria
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>2,"estatus"=>1));
            }else if($tip==3){ //planos de luminaria
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$id_nom,"nom"=>$nom,"estatus"=>1));
            }

            /*if($tip==1){ //descripcion de fabricacion del centro de trabajo
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id_nom,"estatus"=>1));
            }else if($tip==2){ //planos de descripcion de fabricacion del centro de trabajo
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_programa',array('idnom'=>$id_nom,"nom"=>$nom,"estatus"=>1));
            }else if($tip==3){ //planos de medicion
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>1,"estatus"=>1));
            }else if($tip==4){ //planos de luminaria
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>2,"estatus"=>1));
            }*/
        }
        if($nom==22){
            $tipoplano = $this->input->post("tipoplano");
            //log_message('error', 'nom: '.$nom);
            //log_message('error', 'tipoplano: '.$tipoplano);
            if($tip==2){ //planos
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"estatus"=>1));
            }
        }
        if($nom==81){
            $tipoplano = $this->input->post("tipoplano");
            if($tip==2){ //planos
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>$tipoplano,"estatus"=>1));
            }
        }if($nom==15){
            if($tip==1){ //descrip proceso de fabricacion
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id_nom,"nom"=>5,"estatus"=>1));
            }else if($tip==2){ //planos
                $tipoplano = $this->input->post("tipoplano");
                $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_planos',array('idnom'=>$id_nom,"nom"=>$nom,"tipo"=>$tipoplano,"estatus"=>1));
            }
        }
        //log_message('error', 'tip: '.$tip);

        foreach ($getdat->result() as $k) {
            $img=""; $imgdet=""; $imgp="";
            $i++;
            //log_message('error', 'i: '.$i);
            if($nom==11){
                if($tip==2){
                    $img = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFilePlano/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }else if($tip==3){
                    $img = "<img src='".base_url()."uploads/fichas/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFileFichas/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/fichas/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }else if($tip==4){
                    $img = "<img src='".base_url()."uploads/imgPrograma/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFileProg/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPrograma/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }
            }
            if($nom==25){
                if($tip==1){
                    if($k->url_img!=""){
                        $img = "<img src='".base_url()."uploads/imgIntro/".$k->url_img."' width='50px' alt='Sin Dato'>";
                        $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFile/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                        $imgp = "<img src='".base_url()."uploads/imgIntro/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                    }
                }else if($tip==2 || $tip==21){
                    //log_message('error', 'url_img: '.$k->url_img);
                    $img = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFilePlano/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }else if($tip==3){
                    $img = "<img src='".base_url()."uploads/imgPrograma/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFileProg/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPrograma/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }/*else if($tip==4){
                    $img = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFileAcre/".$k->id."', caption: '".$k->idnom."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }else if($tip==5){
                    $img = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFileAcre/".$k->id."', caption: '".$k->idnom."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgAcre/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }*/   
            }
            if($nom==22 || $nom==81){
                if($tip==2){
                    $img = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFilePlano/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }
            }
            if($nom==15){
                if($tip==1){
                    $img = "<img src='".base_url()."uploads/imgIntro/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFile/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgIntro/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }else if($tip==2){
                    $img = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' width='50px' alt='Sin Dato'>";
                    $imgdet = "{type: 'image', url: '".base_url()."Nom/deleteFilePlano/".$k->id."', caption: '".$k->url_img."', key:".$i."}";
                    $imgp = "<img src='".base_url()."uploads/imgPlanos/".$k->url_img."' class='kv-preview-data file-preview-image' width='80px'>"; 
                }
            } 

            $typePDF = "false";  
            echo '
                <div class="row">
                    <div class="col-md-7">
                        <label class="col-md-12"> Imagen: '.$k->url_img.'</label>
                        <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                        <hr style="background-color: red;">
                    </div>

                </div>
                <script> 
                $("#inputFile'.$i.'").fileinput({
                    overwriteInitial: false,
                    showClose: false,
                    showCaption: false,
                    showUploadedThumbs: false,
                    showBrowse: false,
                    removeTitle: "Cancel or reset changes",
                    elErrorContainer: "#kv-avatar-errors-1",
                    msgErrorClass: "alert alert-block alert-danger",
                    defaultPreviewContent: "'.$img.'",
                    layoutTemplates: {main2: "{preview} {remove}"},
                    allowedFileExtensions: ["jpg", "png", "jpeg","webp"],
                    initialPreview: [
                    "'.$imgp.'"
                    ],
                    initialPreviewAsData: '.$typePDF.',
                    initialPreviewFileType: "image",
                    initialPreviewConfig: [
                        '.$imgdet.'
                    ]
                }).on("filedeleted", function(event, files, extra) {
                    location.reload();
                });
                $("#inputFile'.$i.'").on("filedeleted", function(event, key, jqXHR, data) {
                    //console.log("Key = " + key);
                });
                </script>';
        }     
    }
    function getImagestext($tip){
        $id_nom = $this->input->post("id_nom");
        $nom = $this->input->post("nom");
        $text = $this->input->post("text");

        if($nom==25){
            if($tip==1){ //descripcion de fabricacion del centro de trabajo
                //$getdat=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id_nom,"estatus"=>1));
                //if($getdat->num_rows()==0){
                    $this->ModeloCatalogos->updateCatalogo('imagenes_nom',array('estatus'=>0),array('idnom'=>$id_nom));
                    $this->ModeloCatalogos->Insert('imagenes_nom',array('idnom'=>$id_nom,'nom'=>'2','text_info'=>$text));
                //}else{
                    
                //}
            }

  
        }
    }
    function getImagesverifi($tip){
        $id_nom = $this->input->post("id_nom");
        $nom = $this->input->post("nom");
        $getdat=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id_nom,"estatus"=>1));
        $tipotext=0;
        $datatext='';
        foreach ($getdat->result() as $item) {
            if($item->text_info!=null or $item->text_info!=''){
                $tipotext=1;
                $datatext=$item->text_info;    
            }
            
        }
        $datos = array('tipotext'=>$tipotext,'datatext'=>$datatext);
        echo json_encode($datos);
    }

    function uploadImg(){
        $fecha=date('Ymd_His');
        $id_nom = $_POST['id_nom'];
        $tip = $_POST['tipo'];
        $nom = $_POST['nom'];
        $tipo_plano = $_POST['tipo_plano'];
        //log_message('error', 'tipo_plano: '.$tipo_plano);
        if($nom==11){
            if($tip==2){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPlanos/';
            }else if($tip==3){
                $configUpload['upload_path'] = FCPATH.'uploads/fichas/';
            } else if($tip==4){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPrograma/';
            } 
        }
        if($nom==25){
            if($tip==1){
                $configUpload['upload_path'] = FCPATH.'uploads/imgIntro/';
            }else if($tip==2){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPrograma/';
            }else if($tip==3 || $tip==4){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPlanos/';
            }
        }
        if($nom==22 || $nom==81){
            if($tip==2){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPlanos/';
            }
        }
        if($nom==15){
            if($tip==1){
                $configUpload['upload_path'] = FCPATH.'uploads/imgIntro/';
            }else if($tip==2){
                $configUpload['upload_path'] = FCPATH.'uploads/imgPlanos/';
            }
        }
        
        /*else if($tip==4){
            $configUpload['upload_path'] = FCPATH.'uploads/imgAcre/';
        }*/
        //log_message('error', 'tip: '.$tip);
        //log_message('error', 'id_nom: '.$id_nom);
        $configUpload['allowed_types'] = 'jpg|jpeg|png|webp';
        $configUpload['max_size'] = '8000';
        $configUpload['file_name'] = $id_nom.'_'.$fecha;
        $this->load->library('upload', $configUpload);
        $nam_up="";
        $this->upload->do_upload('inputFile');
        $nam_up="inputFile"; 

        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension
        $img = $file_name;
        if ($this->upload->do_upload("inputFile"))
        {  
            if($nom==11){
                if($tip==2){
                    $this->ModeloCatalogos->Insert('imagenes_planos',array('idnom'=>$id_nom,'url_img'=>$img,'tipo'=>$tipo_plano,'reg'=>$this->fechahoy,"nom"=>"11"));
                }else if($tip==3){
                    $this->ModeloCatalogos->Insert('imagenes_fichas',array('id_nom'=>$id_nom,'url_img'=>$img,'reg'=>$this->fechahoy,"nom"=>"11"));
                }else if($tip==4){
                    $this->ModeloCatalogos->Insert('imagenes_programa',array('idnom'=>$id_nom,"nom"=>$nom,'url_img'=>$img,'reg'=>$this->fechahoy));
                }
            }
            if($nom==25){
                if($tip==1){
                    $this->ModeloCatalogos->updateCatalogo('imagenes_nom',array('estatus'=>0),array('idnom'=>$id_nom,'text_info !='=>''));
                    $this->ModeloCatalogos->Insert('imagenes_nom',array('idnom'=>$id_nom,"nom"=>2,'url_img'=>$img,'reg'=>$this->fechahoy));
                }else if($tip==3){
                    $this->ModeloCatalogos->Insert('imagenes_planos',array('idnom'=>$id_nom,'url_img'=>$img,'tipo'=>1,'reg'=>$this->fechahoy,"nom"=>"25"));
                }else if($tip==4){
                    $this->ModeloCatalogos->Insert('imagenes_planos',array('idnom'=>$id_nom,'url_img'=>$img,'tipo'=>2,'reg'=>$this->fechahoy,"nom"=>"25"));
                }else if($tip==2){
                    $this->ModeloCatalogos->Insert('imagenes_programa',array('idnom'=>$id_nom,"nom"=>$nom,'url_img'=>$img,'reg'=>$this->fechahoy));
                }/*else if($tip==4){
                    $this->ModeloCatalogos->Insert('imagenes_acre_apro',array('idnom'=>$id_nom,'url_img'=>$img,"tipo"=>1,'reg'=>$this->fechahoy));
                }else if($tip==5){
                    $this->ModeloCatalogos->Insert('imagenes_acre_apro',array('idnom'=>$id_nom,'url_img'=>$img,"tipo"=>2,'reg'=>$this->fechahoy));
                }*/
            }
            if($nom==22 || $nom==81){
                if($tip==2){
                    $this->ModeloCatalogos->Insert('imagenes_planos',array('idnom'=>$id_nom,'url_img'=>$img,'tipo'=>$tipo_plano,'reg'=>$this->fechahoy,"nom"=>$nom));
                }
            }
            if($nom==15){
                if($tip==1){
                    $this->ModeloCatalogos->Insert('imagenes_nom',array('idnom'=>$id_nom,"nom"=>5,'url_img'=>$img,'reg'=>$this->fechahoy));
                }else if($tip==2){
                    $this->ModeloCatalogos->Insert('imagenes_planos',array('idnom'=>$id_nom,'url_img'=>$img,'tipo'=>$tipo_plano,'reg'=>$this->fechahoy,"nom"=>$nom));
                }
            }
        }
        
        $output = [];
        echo json_encode($output);
    }

    public function deleteFile($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_nom',array('estatus'=>0),array('id'=>$id));
    }

    public function deleteFilePlano($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_planos',array('estatus'=>0),array('id'=>$id));
    }

    public function deleteFileProg($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_programa',array('estatus'=>0),array('id'=>$id));
    }
    public function deleteFileAcre($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_acre_apro',array('estatus'=>0),array('id'=>$id));
    }

    public function deleteFileFichas($id){
        $this->ModeloCatalogos->updateCatalogo('imagenes_fichas',array('estatus'=>0),array('id'=>$id));
    }

    public function generarDoc($id){
        $getdat=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$id));
        $txt_intro="";
        foreach ($getdat->result() as $k) {
            $txt_intro=$k->txt_intro; 
        }

        $this->load->library('Phpword');
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->getCompatibility()->setOoxmlVersion(14);
        $phpWord->getCompatibility()->setOoxmlVersion(15);

        $filename = 'introduccion.docx';
        // add style settings for the title and paragraph
        $section = $phpWord->addSection();
          
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(13);
        $paragraphStyle="Tahoma";
        $myTextElement = $section->addText('Introducción');
        $myTextElement->setFontStyle($fontStyle);

        //$section->addText('Intro: '.$txt_intro.'');
        //$section->addText($txt_intro);
        /*$rep = array("<div>","</div>",'<span lang="ES-MX">','lang="ES-MX"',"</span>","<o:p>","</o:p>","<!--[if !supportLists]-->","<!--[endif]-->",'<span ','style="font-family:Symbol;mso-fareast-font-family:Symbol;','mso-bidi-font-family:','Symbol">','style="font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">');
        $repf = array("","","","","","","","","","","","","","");*/

        $rep = array("<div>","</div>");
        $repf = array("","");

        $textlines=str_replace($rep, $repf, $txt_intro);
        //log_message('error', 'textlines: '.$textlines);
        /*$var_n= [$txt_intro];
        $textrun = $section->addTextRun();
        $textrun->addText($var_n);*/

        $section->addText(htmlspecialchars($textlines));

        /*$table1 = $section->addTable("table1");
        $table1->addRow();
        $table1->addCell(5000)->addText(htmlspecialchars($textlines));*/

        /*$fontStyleName = 'oneUserDefinedStyle';
        
        $phpWord->addFontStyle($fontStyleName,
        array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true));
                  
        $section->addText(htmlspecialchars($textlines),
        $textlines);*/

        //$section->setValue('texto de inicio', $txt_intro);

        $getimgs=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id,"estatus"=>1));
        foreach ($getimgs->result() as $k) {
            //log_message('error', 'url_img: '.$k->url_img);
            $section->addImage(base_url()."uploads/imgIntro/".$k->url_img, [
                "width" => 245, 'wrappingStyle' => 'behind',
            ]);
        }

        /*$fontStyleName = 'oneUserDefinedStyle';
        $phpWord->addFontStyle($fontStyleName,
        array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true));
                  
        $section->addText('"The greatest accomplishment is not in never falling, '
        . 'but in rising again after you fall." '
        . '(Vince Lombardi)',
        $fontStyleName);*/
        
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save($filename);
        // send results to browser to download
        header("Content-Type: text/html;charset=UTF-8");
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        flush();
        readfile($filename);
        unlink($filename); // deletes the temporary file
        exit;
    }

    public function generarDoc2($id){
        header("Content-type: application/vnd.ms-word");
        header("Content-Type: text/html;charset=UTF-8");
        header("Content-Disposition: attachment; filename= paquinimedapatras.doc");

        $getdat=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$id));
        $txt_intro="";
        foreach ($getdat->result() as $k) {
            $txt_intro=$k->txt_intro; 
        }
        echo utf8_encode($txt_intro);
        /*$rep = array("<div>","</div>",'<span lang="ES-MX">','lang="ES-MX"',"</span>","<o:p>","</o:p>","<!--[if !supportLists]-->","<!--[endif]-->");
        $repf = array("","","","","","","","","");

        $textlines=str_replace($rep, $repf, $txt_intro);*/
        
        $getimgs=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id,"estatus"=>1));
        foreach ($getimgs->result() as $k) {
            //log_message('error', 'url_img: '.$k->url_img);
            /*$section->addImage(base_url()."uploads/imgIntro/".$k->url_img, [
                "width" => 245,
            ]);*/
            //echo "<img src=".base_url()."uploads/imgIntro/".$k->url_img." width='245'>";
            echo base_url()."uploads/imgIntro/".$k->url_img;
        }
        
    }

    public function getImgsDoc(){
        $id = $this->input->post("id_nom");
        $html=""; $html2=""; $html3=""; $txt_intro=""; $tablageneral1=""; $tablageneral2=""; $tablageneral1char=""; $tablageneral2char="";

        $getimgs=$this->ModeloCatalogos->getselectwheren('imagenes_nom',array('idnom'=>$id,"estatus"=>1));
        foreach ($getimgs->result() as $k) {
            //$html .= '<div style="background-image: url('.base_url()."uploads/imgIntro/".$k->url_img.'); background-size: 450px; background-repeat: no-repeat; background-position: center;"></div>';
            $html.='<table width="100%">
                    <tr>
                        <th width="1%"></th>
                        <th width="98%"><img src="'.base_url()."uploads/imgIntro/".$k->url_img.'" width="600" height="auto"></th>
                        <th width="1%"></th>
                    </tr>                
                </table>';
        }

        $getdat=$this->ModeloCatalogos->getselectwheren('nom',array('idnom'=>$id));
        $html3.='<table width="100%">';
        foreach ($getdat->result() as $k) {
            $txt_intro = $k->txt_intro; 
            $html3.="";
            if($k->tablageneral1!="" && $k->tablageneral1!=null){
               $html3.='<tr>
                    <th width="1%"></th>
                    <th width="98%"><img src="'.$k->tablageneral1.'" class="img-fluid" alt="Responsive image" id="img_tab1" width="600" height="320"></th>
                    <th width="1%"></th>
                </tr>'; 
            }
                
            if($k->tablageneral2!="" && $k->tablageneral2!=null){
                $html3.='<tr>
                    <th width="1%"></th>
                    <th width="98%"><img src="'.$k->tablageneral2.'" class="img-fluid" alt="Responsive image" id="img_tab1" width="600" height="320"></th>
                    <th width="1%"></th>
                </tr>';
            }
            if($k->tablageneral1char!="" && $k->tablageneral1char!=null){
                $html3.='<tr>
                    <th width="1%"></th>
                    <th width="98%"><img src="'.$k->tablageneral1char.'" class="img-fluid" alt="Responsive image" id="img_tab1" width="600" height="320"> </th>
                    <th width="1%"></th>
                </tr>';
            }
            if($k->tablageneral2char!="" && $k->tablageneral2char!=null){
                $html3.='<tr>
                    <th width="1%"></th>
                    <th width="98%"><img src="'.$k->tablageneral2char.'" class="img-fluid" alt="Responsive image" id="img_tab1" width="600" height="320"></th>
                    <th width="1%"></th>
                </tr>';
            }
        }
        $html3.="</table>";

        $getdatos=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('idnom'=>$id));
        $html2.='<table width="100%">';
        foreach ($getdatos->result() as $k) {
            /*$table_generado1 = $k->table_generado1;
            $table_generado2 = $k->table_generado2; 
            $grafica = $k->grafica;*/
            $html2.='<tr>
                    <th width="1%"></th>
                    <th width="98%"><img src="'.$k->table_generado1.'" class="img-fluid" alt="Responsive image" id="img_tab1" width="600" height="250"></th>
                    <th width="1%"></th>
                </tr>';
        }
        $html2.="</table>";
        echo json_encode(array("html"=>$html,"txt_intro"=>$txt_intro,"html2"=>$html2,"html3"=>$html3));
    }

    public function getCliente(){
        $search = $this->input->get('search');
        $results=$this->ModeloNom->getSearchCli($search);
        echo json_encode($results);    
    }

    public function setNumOrden(){
        $num = $this->input->post('num');
        $idnom = $this->input->post('idnom');
        $this->ModeloCatalogos->updateCatalogo('nom',array('num_informe'=>$num),array('idnom'=>$idnom));   
    }
    function obtenerinfo_reconocimiento_ini(){
        $params = $this->input->post();
        $orden = $params['orden'];
        $result= $this->ModeloCatalogos->getselectwheren('reconocimiento_inicial',array('orden'=>$orden,'activo'=>1));
        if($result->num_rows()==0){
           $array_result = array(
                            'num'=>0,
                            ); 
        }else{
            $resultdata=$result->result();
            $resultdata=$resultdata[0];

            $resultdetalle= $this->ModeloCatalogos->getselectwheren('reconocimiento_inicial_detalle',array('id_recini'=>$resultdata->id,'activo'=>1));

            $array_result = array(
                            'num'=>1,
                            'table'=>$resultdata,
                            'detalle'=>$resultdetalle->result()
                            ); 
        }
        
        echo json_encode($array_result);  
    }
    function savereconocimientoini(){
        $params = $this->input->post();
        $id = $params['idri'];
        unset($params['idri']);
        $arraydetalles = $params['arraydetalles'];
        unset($params['arraydetalles']);

        $this->db->trans_start();

        if($id==0){
            $params['insert_user']=$this->id_usuario;
            $idri = $this->ModeloCatalogos->Insert('reconocimiento_inicial',$params);
        }else{
            $idri = $id;
            $params['update_user']=$this->id_usuario;
            $params['update_reg']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo('reconocimiento_inicial',$params,array('id'=>$idri));
        }
        $DATAc = json_decode($arraydetalles);  
        for ($i=0;$i<count($DATAc);$i++) {
            if($DATAc[$i]->id==0){
                $dataa = array(
                    'id_recini'=>$idri,
                    'largo'=>$DATAc[$i]->largo,
                    'ancho'=>$DATAc[$i]->ancho,
                    'altura'=>$DATAc[$i]->altura,
                    'ic'=>$DATAc[$i]->ic,
                    'minimo'=>$DATAc[$i]->minimo,
                    'limitacion'=>$DATAc[$i]->limitacion,
                    'puestos'=>$DATAc[$i]->puestos,
                    'puestos_menor'=>$DATAc[$i]->puestos_menor 
                );
                $dataa['insert_user']=$this->id_usuario;
                $dataa['insert_reg']=$this->fechahoy;
                $this->ModeloCatalogos->Insert('reconocimiento_inicial_detalle',$dataa);
            }else{
                $dataa = array(
                    'largo'=>$DATAc[$i]->largo,
                    'ancho'=>$DATAc[$i]->ancho,
                    'altura'=>$DATAc[$i]->altura,
                    'ic'=>$DATAc[$i]->ic,
                    'minimo'=>$DATAc[$i]->minimo,
                    'limitacion'=>$DATAc[$i]->limitacion,
                    'puestos'=>$DATAc[$i]->puestos,
                    'puestos_menor'=>$DATAc[$i]->puestos_menor
                );
                $dataa['update_user']=$this->id_usuario;
                $dataa['update_reg']=$this->fechahoy;
                $this->ModeloCatalogos->updateCatalogo('reconocimiento_inicial_detalle',$dataa,array('id'=>$DATAc[$i]->id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            $status="error";   
        }else{
            $status="ok"; 
        }
           
    }
    function deletedetallepuntori(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('reconocimiento_inicial_detalle',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('id'=>$id));
    }
    function inserupdate_resolucion_e(){
        $params = $this->input->post();
        $idnom=$params['idnom'];
        $todos_ptos=$params['todos_ptos'];
        $id_pto=$params['id_punto'];
        if($todos_ptos==1) //todos
            $result=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom));
        else{
            $result=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom,"id_punto"=>$id_pto));
        }

        if($result->num_rows()>0){
            if($todos_ptos==1)
                $this->ModeloCatalogos->updateCatalogo('nom25_resolucion_equipo',$params,array('idnom'=>$idnom));
            else
                $this->ModeloCatalogos->updateCatalogo('nom25_resolucion_equipo',$params,array('idnom'=>$idnom,"id_punto"=>$id_pto));
        }else{
            $this->ModeloCatalogos->Insert('nom25_resolucion_equipo',$params);
        }
    }
    function getPuntosNom(){
        $idnom = $this->input->post("idnom");
        $html='<select id="id_punto" class="form-control"><option value="0">Elige un punto:</option>';
        $get=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('activo'=>1,'idnom'=>$idnom));
        foreach($get->result() as $p){
            $html.="<option value='".$p->id."'>".$p->identificacion."</option>";
        }
        $html.="</select>";
        echo $html;
    }
    function get_resolucion_e(){
        $params = $this->input->post();
        $idnom=$params['idnom'];
        $todos=$params['todos'];
        $id_pto=$params['id_pto'];

        if($todos==1)
            $result=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom,"todos_ptos"=>$todos));
        else
            $result=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom,"id_punto"=>$id_pto));
        $datosresult='';
        if($result->num_rows()>0){
          $datosresult=$result->result();
          //$datosresult=$datosresult[0];  
        }
        
        $array = array(
                    'datosnum'=>$result->num_rows(),
                    'datos'=>$datosresult
                        );
        echo json_encode($array);
    }
    function inserupdate11(){
        $params = $this->input->post();

        $table=$params['table'];
        unset($params['table']);
        
        $params['update_user']=$this->id_usuario;
        $params['update_reg']=$this->fechahoy;
        //$datos=json_encode($params['datos']);
        if($table=='nom11'){
            $idnom=$params['idnom'];
            unset($params['idnom']);
            $this->ModeloCatalogos->updateCatalogo('nom11',$params,array('idnom'=>$idnom));
        }
        if($table=='nom11d'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11d',$params,array('idnomd'=>$idnomd));
        }
        if($table=='nom11d_lecturas'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11d_lecturas',$params,array('idnomd'=>$idnomd));
        }
        if($table=='nom11d_reg_espec_acustico'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11d_reg_espec_acustico',$params,array('idnomd'=>$idnomd));
        }
        if($table=='nom11d_faepa'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11d_faepa',$params,array('idnomd'=>$idnomd));
        }
        if($table=='nom11_personal'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11_personal',$params,array('idnomd'=>$idnomd));
        }
        if($table=='nom11_personal_ti'){
            $idnomd=$params['idnomd'];
            unset($params['idnomd']);
            $this->ModeloCatalogos->updateCatalogo('nom11_personal_ti',$params,array('idnomd'=>$idnomd));
        }
    }

    function updateCampoNom11(){
        $table = $this->input->post("table");
        $val = $this->input->post("val");
        $name = $this->input->post("name");
        $id = $this->input->post("id");

        $this->ModeloCatalogos->updateCatalogo($table,array($name=>$val),array('idnomd'=>$id));
        
    }
    function inserpuntonom11new($idnom,$id){
        $this->inserpuntonom11($idnom,$id,0);
        redirect('Nom/add11/'.$id);
    }
    function inserpuntonom11newlectura($idnom,$punto,$orden){
        $idnom=$this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        redirect('Nom/add11/'.$orden.'#lectura_'.$idnom);
    }
    function inserpuntonom11newrea($idnom,$punto,$orden){
        $idnom=$this->ModeloCatalogos->Insert('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        redirect('Nom/add11/'.$orden.'#rea_'.$idnom);
    }
    function insernom11personalmun($idnom,$id_orden){

        $nummax=$this->ModeloCatalogos->maxnumnom11_personal($idnom);
        $this->ModeloCatalogos->Insert('nom11_personal',array('idnom'=>$idnom,'num'=>$nummax,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        
        redirect('Nom/add11personal/'.$id_orden);
    }
    function insernom11personaltimun($idnom,$id_orden){
        $nummax=$this->ModeloCatalogos->maxnumnom11_personal_ti($idnom);
        $this->ModeloCatalogos->Insert('nom11_personal_ti',array('idnom'=>$idnom,'num'=>$nummax,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        redirect('Nom/add11personal/'.$id_orden);   
    }
    function inserpuntonom11($idnom,$id=0,$id_chs){
        if($idnom==0){
            $recono=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom11',array('id_orden'=>$id));
            $num_informe='';$razon_social='';
            foreach ($recono->result() as $item) {
                $num_informe=$item->num_informe_rec;
                $razon_social=$item->cliente;
            }

            $idnom=$this->ModeloCatalogos->Insert('nom11',array('num_informe'=>$num_informe,'idordenes'=>$id,'razon_social'=>$razon_social,'fecha'=>$this->fechahoyc,'id_chs'=>$id_chs,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
            $punto=1;
        }else{
            $punto=$this->ModeloCatalogos->maxpuntonom11($idnom);
        }
        $this->ModeloCatalogos->Insert('nom11d_puntos',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));

        $this->ModeloCatalogos->Insert('nom11d',array('idnom'=>$idnom,'punto'=>$punto,'tiempo_ruido'=>0,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        $this->ModeloCatalogos->Insert('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$punto,'periodo'=>1,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
        //$this->ModeloCatalogos->Insert('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$punto,'periodo'=>2));
        $this->ModeloCatalogos->Insert('nom11d_faepa',array('idnom'=>$idnom,'punto'=>$punto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));

        $datareconocimiento=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom11',array('id_orden'=>$id));
        $idreconocimineto=0;
        foreach ($datareconocimiento->result() as $itemr) {
            $idreconocimineto=$itemr->id;
        }
        if($idreconocimineto>0){
            $rpaso3=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso3_nom11',array('id_reconocimiento'=>$idreconocimineto,'estatus'=>1));

            foreach ($rpaso3->result() as $itemp3) {
                $rpaso2=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$idreconocimineto,'estatus'=>1,'num'=>$itemp3->num));
                $area='';
                $puesto='';
                foreach ($rpaso2->result() as $itemp2) {
                    $area=$itemp2->area;
                    $puesto=$itemp2->puesto;
                }
                $this->ModeloCatalogos->Insert('nom11_personal',array('idnom'=>$idnom,'num'=>$itemp3->num,'area'=>$area,'puesto'=>$puesto,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));
            }
        }
        $this->ModeloCatalogos->Insert('nom11_personal_ti',array('idnom'=>$idnom,'num'=>1,'insert_user'=>$this->id_usuario,'insert_reg'=>$this->fechahoy));

    }
    function deletedetallepunto11(){
        $params = $this->input->post();
        $idnom = $params['idnom'];
        $punto = $params['punto'];

        $this->ModeloCatalogos->updateCatalogo('nom11d_puntos',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$idnom,'punto'=>$punto));
        $this->ModeloCatalogos->updateCatalogo('nom11d',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$idnom,'punto'=>$punto));
        $this->ModeloCatalogos->updateCatalogo('nom11d_lecturas',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$idnom,'punto'=>$punto));
        $this->ModeloCatalogos->updateCatalogo('nom11d_reg_espec_acustico',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$idnom,'punto'=>$punto));
        $this->ModeloCatalogos->updateCatalogo('nom11d_faepa',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnom'=>$idnom,'punto'=>$punto));
    }
    function deletedetallepunto11lectura(){
        $params = $this->input->post();
        $idnomd = $params['idnomd'];
        $this->ModeloCatalogos->updateCatalogo('nom11d_lecturas',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnomd'=>$idnomd));
    }
    function deletedetallepunto11rea(){
        $params = $this->input->post();
        $idnomd = $params['idnomd'];
        $this->ModeloCatalogos->updateCatalogo('nom11d_reg_espec_acustico',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnomd'=>$idnomd));
    }
    function deletenom11_personal(){
        $params = $this->input->post();
        $idnomd = $params['idnomd'];
        $this->ModeloCatalogos->updateCatalogo('nom11_personal',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnomd'=>$idnomd));
    }
    function deletenom11_personal_ti(){
        $params = $this->input->post();
        $idnomd = $params['idnomd'];
        $this->ModeloCatalogos->updateCatalogo('nom11_personal_ti',array('activo'=>0,'delete_user'=>$this->id_usuario,'delete_reg'=>$this->fechahoy),array('idnomd'=>$idnomd));
    }
    function nominserupdatetableg11(){
        $datax = $this->input->post();
        $idnomd=$datax['id'];
        $tipo=$datax['tipo'];
        $base64Image=$datax['grafica'];
        if($tipo==1){
            $params['grafo1']=$base64Image;
        }else{
            $params['grafo2']=$base64Image;
        }
        $this->ModeloCatalogos->updateCatalogo('nom11d_puntos',$params,array('idnomd'=>$idnomd));
    }
    function add81view($id=0,$id_chs=0){
        $data['id_orden']=$id;
        $data['id_chs']=$id_chs;
        $data['idnom']="0";
        $idnom=0;
        $data['num_informe']="";
        $data['razon_social']="";
        //$data['fecha']=$this->fechahoyc;
        $data['fecha']=date("d-m-Y");
        $data['lugar']="";
        $data['horario']="";
        $data['zona_critica']="";
        $data['id_sonometro']="";
        $data['id_calibrador']="";
        $data['veri_ini']="";
        $data['veri_fin']="";
        $data['criterio']="";
        $data['cumple_criterio']="";
        $data['pot_bat_ini']="";
        $data['pot_bat_fin']="";
        $data['ec_largo']="";
        $data['ec_ancho']="";
        $data['observaciones']="";
        
        $data["estados"]=$this->Catalogos_model->getCatalogo("estados");
        $data['datosrec']=$this->ModeloCatalogos->getselectwheren('reconocimiento_nom81',array('id_orden'=>$id,"id_chs"=>$id_chs));
        if($data['datosrec']->num_rows()>0){
            $datosrec=$data['datosrec']->row();
            $data["num_informe"]=$datosrec->num_informe_rec;
            $data["razon_social"]=$datosrec->cliente;
        }

        $resultnorma=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$id,"activo"=>1));
        foreach ($resultnorma->result() as $item) {
            $data['idnom']=$item->idnom;
            $idnom=$item->idnom;
            $data['num_informe']=$item->num_informe;
            $data['razon_social']=$item->razon_social;
            $data['fecha']=$item->fecha;
            $data['lugar']=$item->lugar;
            $data['horario']=$item->horario;
            $data['zona_critica']=$item->zona_critica;
            $data['id_sonometro']=$item->id_sonometro;
            $data['id_calibrador']=$item->id_calibrador;
            $data['veri_ini']=$item->veri_ini;
            $data['veri_fin']=$item->veri_fin;
            $data['cumple_criterio']=$item->cumple_criterio;
            $data['criterio']=$item->criterio;
            $data['pot_bat_ini']=$item->pot_bat_ini;
            $data['pot_bat_fin']=$item->pot_bat_fin;
            $data['ec_largo']=$item->ec_largo;
            $data['ec_ancho']=$item->ec_ancho;
            $data['observaciones']=$item->observaciones;
        }
        $data["sono"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"2","activo"=>1));
        $data["cali"]=$this->ModeloCatalogos->getselectwheren('equipo',array('tipo'=>"4","activo"=>1));

        
        $this->load->view('nom/cnom081view',$data);
    }
    function nom84viewinserupdatechart(){
        $datax = $this->input->post();
        $codigo=$datax['id'];
        $base64Image=$datax['grafica'];
        $this->ModeloCatalogos->updateCatalogo('nom81',array('chart'=>$base64Image),array('idnom'=>$codigo));
    }
 
}
?>
