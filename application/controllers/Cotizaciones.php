<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
    //estatus de la cotizacion:
    // 0-> eliminada
    // 1-> creada /pendiente
    // 2-> aceptada
    // 3-> rechazada
    // 4-> modificada

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        $logueo = $this->session->userdata('logeado');
        if($logueo!=1){
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('Cotizaciones_model', 'model');
        $this->load->model('Catalogos_model');
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/cotizaciones');
        $this->load->view('footer');
        $this->load->view('cotizaciones/lista_cotizacionesjs');
    }

    public function nueva_cotizacion() {
        $data['clientes'] = $this->Catalogos_model->getDataClientes();
        $data['aux_auto'] = 0;
        $data["id_empresa"]=$this->session->userdata("empresa");
        $data["sucursal"]=$this->session->userdata("sucursal");
        $data['id']=0;
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/form_cotizacion',$data);
        $this->load->view('footer');
        $this->load->view('cotizaciones/cotizacionesjs');
    }
    
    public function modificar_cotizacion($id,$auto=0) {
        $data['clientes'] = $this->model->getselectwheren("clientes",array("status"=>1));  //ok 
        $data['cotizacion']=$this->model->getCotizacion($id); //ok
        $data['persona_contac']=$this->model->getContactoCotizaciones($data['cotizacion']->contacto_id); //ok
        $data["servicios"]=$this->model->getServiciosCotizacion($id,$data['cotizacion']->id_empresa);   //ok

        $data['cotizacion']->id=$id;
        $data['id']=$id;
        $data['aux_auto']=$auto;
        $data["id_empresa"]=$data['cotizacion']->id_empresa;
        $data["sucursal"]=$data['cotizacion']->sucursal;
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/form_cotizacion',$data);
        $this->load->view('footer');
        $this->load->view('cotizaciones/cotizacionesjs');
        
    }

    public function getCotizaciones() {
        $cotizaciones = $this->model_cotiza->getCotizaciones();
        $json_data = array("data" => $cotizaciones);
        echo json_encode($json_data);
    }
    
    public function searchContacto(){
        $id_cliente= $this->input->post("cliente");
        $contacto= $this->input->post("contacto");
        $contactos=$this->model->searchContactos($id_cliente);
        $result="";
        $sel="";
        foreach($contactos as $c){
            if($contacto==$c->id) {
                $sel="selected"; 
            }
            else{
                $sel="";
            }
            $result.="<option $sel value='$c->id'>$c->nombre</option>";
        }
        echo $result;
    }
   
    public function searchFamilias(){
        $tamaño=$this->input->post("tamaño");
        $id_emp = $this->session->userdata("empresa");
        
        $familias=$this->model->searchFamilias($tamaño,$id_emp);
        $result="";
        $emp ="";
        foreach($familias as $f){
            $result.="<option value='$f->id' data-emp='$id_emp'>$f->nombre</option>";
        } 
        
        echo $result;
    }
    
    public function searchServicios(){
        $familia=$this->input->post("familia");
        $tamaño=$this->input->post("tamaño");
        $empresa=$this->input->post("id_emp");
        //$empresa = $this->session->userdata("empresa");
        $result="";
        if($familia!=""){
            $servicios=$this->model->searchServicios($familia,$tamaño,$empresa);
            foreach($servicios as $s){
                $rec="";
                switch ($s->recipientes){
                    case 1: $rec="CAT1"; break;
                    case 2: $rec="CAT2"; break;
                    case 3: $rec="CAT3"; break;
                }
                $result.="<option value='$s->id'>$s->nombre $rec</option>";
            }
        }
        
        echo $result;
    }
    
    public function searchProducto() {
        $modelo = $this->input->post("modelo");
        $tipo = $this->input->post("tipo");
        $data["producto"]=$this->model->searchProducto($modelo);
        //print_r($data); die;
        $data["producto"]->precio=$this->calculaPrecio($data["producto"]->precio, $tipo);
        
        echo json_encode($data);
    }
    
    public function agregarServicio($id_servicio,$cantidad,$print,$sucursal,$id,$id_cli,$id_emp_serv=0){ 
        $getsuc = $this->Catalogos_model->getItemCatalogo("clientes",$id_cli);
        $tipo_cli = $getsuc->tipo_cli;
        $precio=0;
        
        $servicio=$this->model->getNRow("servicios_ahisa",$id_servicio); 
        //traer precio de servicio si el cliente es directo o consultor
        $precio=$this->validaPrecioOtra($id_servicio,$cantidad,$tipo_cli);
        
        echo json_encode(array("precio"=>$precio,"clave"=>$servicio->clave,"desc"=>$servicio->descripcion,"id_servicio"=>$id_servicio,"ad"=>$servicio->aplica_desc));
    }

    public function serviciosCotiza(){
        $id = $this->input->post("id");
        $servicios=$this->model->getServiciosCotizacion($id,1);   //ok
        $i=1; $tot=0; $html="";
        foreach($servicios as $s){
            $html .= "<tr class=ord_".$s->id." id='prodid_".$s->id."'>
                <td>".$i."</td>
                <input type='hidden' id='id_cot_serv' value='".$s->id."'>
                <td><input class='id_serv_".$s->servicio_id."' id='id_serv_add' type='hidden' value='".$s->servicio_id."'>".$s->clave2."</td>
                <td>".$s->descripcion2."</td>
                <td><input type='hidden' data-ad='".$s->ad."' id='serv_id' value='".$s->servicio_id."'>
                    <input type='number' class='cant cant_".$s->servicio_id." form-control form-control-sm width-150' value='".$s->cantidad."'></td>
                <td><input type='hidden' class='precio precio_".$i." form-control form-control-sm' value='".$s->precio."'> $".number_format($s->precio,2)."</td>
                <td class='sub' id='tot_".$id."'>$".number_format($s->precio*$s->cantidad,2)."</td>
                <td><button type='button' onclick='elimina_servicioAsignado(".$s->id.")' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-minus'></i></button></td>
            </tr>";
            $i++;
        }
        echo $html;
    }
    
    public function eliminaServicio(){ //elimina un servicio de cotizacion
        $id=$this->input->post("id");
        $this->Catalogos_model->updateCatalogo(array("status"=>0),$id,"cotizaciones_has_servicios");
    }
    
    private function validaPrecio($id, $cantidad, $sucursal){
        $precio=$this->model->getPrecio($id,$cantidad,$sucursal); //aqui manda a traer el precio por el usuario y sucursal logueado
        return $precio;
    }

    private function validaPrecioOtra($id, $cantidad, $tipo_cli){
        $precio=$this->model->getPrecioOtra($id,$cantidad,$tipo_cli); //aqui manda a traer el precio por el usuario y sucursal logueado
        return $precio;
    }
    
    /*private function descuentoEspecial($cotiza,$descuento,$id_empresa,$tipo_cli){
        $total=0;
        foreach($cotiza as $c){
            //log_message('error', 'aplicaDesc: '.$c["aplicaDesc"]);
            $total+=$c["precio"]*$c["cantidad"];
            //log_message('error', 'descuento: '.$descuento);
        }
        if($descuento>0){
            return "<tr>"
                        . "<td>Descuento sobre el total:</td>"
                        . "<td></td>"
                        . "<td>$descuento %</td>"
                        . "<td>$".number_format(($descuento*$total)/100,2)."</td></tr>";
        }else{
            return "";
        }
    }*/
    
    /*private function descuentoCategorias($cotiza,$desc){ //deprecated
        $result="";
        $cat=array();
        foreach($cotiza as $c){
            array_push($cat, $c["categoria"]);
        }
        $valores = array_count_values($cat); //obtiene el conteo de categorias vg -> (1,2) categoria 1 encontrada 2 veces
        foreach($valores as $key => $v){
            $categoria=$this->model->getCategoria($key);
            if($v>1 && $v<3){ // si hay mas de un servicio de una categoria se aplica descuento
                
                $precio=0;
                foreach($cotiza as $co){
                    if($co["categoria"]==$key){
                        $precio+=($co["precio"]*$co["cantidad"]);
                    }
                }
                $descuento=$precio*$desc->a_proveedor1/100;
                $result.="<tr>"
                        . "<td>Descuento sobre categoría: $categoria->nombre</td>"
                        . "<td>$v</td>"
                        . "<td>$desc->a_proveedor1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
            }
            elseif($v>=3) {
                $precio=0;
                foreach($cotiza as $co){
                    if($co["categoria"]==$key){
                        $precio+=($co["precio"]*$co["cantidad"]);
                    }
                }
                $descuento=$precio*$desc->a_proveedor2/100;
                $result.="<tr>"
                        . "<td>Descuento sobre categoría: $categoria->nombre</td>"
                        . "<td>$v</td>"
                        . "<td>$desc->a_proveedor2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
            }
               
        }
        return $result;
    }
    
    private function descuentoPuntos($cotiza,$desc){ //deprecated
        $result="";
        
        foreach($cotiza as $c){
            $servicio=$this->model->getServicio($c["id"]);
            //print_r($servicio); die;
            if($servicio->por_puntos>0){
                if($c["cantidad"]>0 && $c["cantidad"]<51){
                    $descuento=$c["precio"]*$desc->por_puntos1/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>50 && $c["cantidad"]<101){
                    $descuento=$c["precio"]*$desc->por_puntos2/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>100){
                    $descuento=$c["precio"]*$desc->por_puntos3/100;
                    $result.="<tr>"
                        . "<td>Descuento por puntos: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_puntos3 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
            }
        }
        return $result;
    }
    
    private function descuentosRecipientes($cotiza,$desc){ //deprecated
        $result="";
        
        foreach($cotiza as $c){
            $servicio=$this->model->getServicio($c["id"]);
            
            if($servicio->recipientes){
                if($c["cantidad"]>1 && $c["cantidad"]<6){
                    $descuento=$c["precio"]*$desc->por_equipo1/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo1 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>5 && $c["cantidad"]<11){
                    $descuento=$c["precio"]*$desc->por_equipo2/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo2 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>10 && $c["cantidad"]<51){
                    $descuento=$c["precio"]*$desc->por_equipo3/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo3 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                if($c["cantidad"]>50 ){
                    $descuento=$c["precio"]*$desc->por_equipo4/100;
                    $result.="<tr>"
                        . "<td>Descuento equipo a presión: ".$c["descripcion"]."</td>"
                        . "<td>".$c["cantidad"]."</td>"
                        . "<td>$desc->por_equipo4 %</td>"
                        . "<td>$".number_format($descuento,2)."</td></tr>"; 
                }
                
            }
        }
        return $result;
    }*/
    
    public function saveCotizacion(){
        $cliente=$this->input->post("cliente");
        $contacto=$this->input->post("contacto");
        $forma_pago=$this->input->post("forma_pago");
        $observ=$this->input->post("observaciones");
        $descuento=$this->input->post("descuento");
        $desc_aplicado=$this->input->post("desc_aplicado");
        $otro=$this->input->post("otro");
        $id_cotizacion=$this->input->post("id");
        $aux_auto=$this->input->post("aux_auto");
        $id_empresa=1;
        $subtotal=$this->input->post("subtotal");
        //log_message('error', 'descuento: '.$descuento);
        $data=array("cliente_id"=>$cliente,"contacto_id"=>$contacto,"forma_pago"=>$forma_pago,"otra_forma_pago"=>$otro,"observaciones"=>trim($observ));
        $total=0; $total2=0; $tot=0; $tot2=0;
        
        $data["subtotal"]=$subtotal; //monto sin desuento
        //log_message('error', 'subtotal de cotizacion: '.$subtotal);
        $total=$subtotal;
        if($descuento>0){
            $data["descuento"]=$descuento;
        }else{
            $data["descuento"]=0;
        }
        if($desc_aplicado>0){
            $data["descuento"]=$desc_aplicado;
        }
        if($data["descuento"]!=0){
            $tot=$total-($total*$data["descuento"])/100; //monto al que se le aplica el descuento - motno total con descuento
            $data["total"]=$tot+$total2;
        }else{
            $data["total"] = $subtotal;
        }
        if($id_cotizacion=="0"){ //es nueva cotizacion
            $data["vendedor_id"]= $this->session->userdata("id_usuario");
            $data["consecutivo"]=$this->model->getConsecutivo();
            //$data["id_empresa"]=$this->session->userdata("empresa"); //ya viene desde el form de guardado
            $data["id_empresa"] = $id_empresa;
            $id_cotizacion=$this->model->insertCotizacion($data);
        }
        else{ //se edita la cotizacion
            if($aux_auto==0){
                $data["status"]=4;
                $cotizacion=$this->model->getCotizacion($id_cotizacion);
                $padre=$id_cotizacion; 
                if($cotizacion->padre!=0){
                    $padre=$cotizacion->padre;
                }
                //log_message('error', 'descuento en data: '.$data["descuento"]);
                $data["no_modificacion"]=$this->model->getModificaciones($padre);
                $data["padre"]=$padre;
                $data["vendedor_id"]=$cotizacion->vendedor_id;
                $data["consecutivo"]=$cotizacion->consecutivo;
            }
            if($aux_auto==0){
                //$data["id_empresa"]=$this->session->userdata("empresa"); //ya viene desde el form de guardado
                $data["id_empresa"] = $id_empresa; 
                $id_cotizacion=$this->model->insertCotizacion($data);
            }else{
                $data["status"]=2;
                $this->Catalogos_model->updateCatalogo($data,$id_cotizacion,"cotizaciones");
            }
        }
        echo $id_cotizacion;
    }

    public function guardaServiciosCotiza(){ 
        $data = $this->input->post('data');
        $DATA = json_decode($data);

        for ($i=0; $i<count($DATA); $i++) {
            $arra_cs=array(
                "servicio_id"=>$DATA[$i]->servicio_id,
                "cantidad"=>$DATA[$i]->cantidad,
                "precio" =>$DATA[$i]->precio,
                "descuento"=>$DATA[$i]->descuento,
                "cotizacion_id"=>$DATA[$i]->cotizacion_id,
                "id_empresa_serv"=>1
            );
            if($DATA[$i]->id_cot_serv==0){
                $this->model->insertServicioCotiza($arra_cs);     
            }else{
                $this->Catalogos_model->updateCatalogo($arra_cs,$DATA[$i]->id_cot_serv,"cotizaciones_has_servicios");
            }
        }
    }
    
    public function getData_cotizaciones() {
        $params=$this->input->post();
        //log_message('error', 'status: '.$params["status"]);
        $cotizas = $this->model->getCotizaciones($params);
        //log_message('error','Cotizaciones: '. json_encode($cotizas->result()));
        $totalRecords=$this->model->totalCotizaciones($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $cotizas->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

     public function getData_cotizacionesEstatus($status) {

        $params=$this->input->post();
        $cotizas = $this->model->getCotizaciones($params,$status);

        $totalRecords=$this->model->totalCotizacionesEstatus($params,$status);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $cotizas->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

   
    public function cambiarEstatus(){
        $id=$this->input->post("id");
        $status=$this->input->post("status");
        $recibe=$this->input->post("recibe");
        $nom_tec=$this->input->post("nom_tec");

        //if(isset($recibe))
            //log_message('error', 'recibe: '.$recibe);
        if($status==2){
            $this->crearOrdenes($id,$recibe,$nom_tec); //crea la orden si se acepta 
        }
        $this->db->where("id",$id);
        echo $this->db->update('cotizaciones', array("status"=>$status));
    }
    
    public function crearOrdenes($id_cotizacion,$recibe,$nom_tec){
        $id_usuario=$this->session->userdata("id_usuario");
        $sql = "SELECT DISTINCT familia as id FROM servicios_ahisa s "
                . "INNER JOIN cotizaciones_has_servicios cs ON s.id=cs.servicio_id "
                . "WHERE cotizacion_id=$id_cotizacion";
        $query = $this->db->query($sql);
        $ordenes= $query->result();
        foreach ($ordenes as $o) {
            $this->Catalogos_model->Insert('ordenes', array("cotizacion_id"=>$id_cotizacion, "familia_id"=>$o->id,"id_usuario"=>$id_usuario,"nombre_atn"=>$recibe,"nombre_cargo"=>$nom_tec,"fecha_creacion"=>date("Y-m-d H:i:s"))); 
        }
    }
    
    public function validaPass(){
        $pass=$this->input->post("pass");
        $vende = $this->session->userdata("id_usuario");
        $usada=0; $fecha_uso=""; $id=0; $exist=0;
        $get_pass=$this->model->getselectwheren("pass_creadas",array("pass"=>$pass,"id_vendedor"=>$vende,"tipo"=>"1"));
        foreach ($get_pass as $k) {
            $exist++;
            $usada = $k->usada;
            $fecha_uso = $k->fecha_uso;
            $id=$k->id;
        }
        $return = Array('usada'=>$usada,'fecha_uso'=>$fecha_uso,"id"=>$id,"exist"=>$exist);
        echo json_encode($return);
    }

    public function autorizaModifica(){
        $pass=$this->input->post("pass");
        $id_cot = $this->input->post("id_cot");
        $vende = $this->session->userdata("id_usuario");
        $usada=0; $fecha_uso=""; $id=0; $exist=0;
        $get_pass=$this->model->getselectwheren("pass_creadas",array("id_cotizacion"=>$id_cot,"pass"=>$pass,"id_vendedor"=>$vende,"tipo"=>"2"));
        foreach ($get_pass as $k) {
            $exist++;
            $usada = $k->usada;
            $fecha_uso = $k->fecha_uso;
            $id=$k->id;
        }
        $return = Array('usada'=>$usada,'fecha_uso'=>$fecha_uso,"id"=>$id,"exist"=>$exist);
        echo json_encode($return);
    }
    
    public function changeStatusPass(){
        $id=$this->input->post("id");
        $id_cotiza=$this->input->post("id_cotiza");
        $this->Catalogos_model->updateCatalogo(array("id_cotizacion"=>$id_cotiza,"usada"=>1,"fecha_uso"=>date("Y-m-d H:i:s")),$id,"pass_creadas");
    }

    public function changeStatusPass2(){
        $id=$this->input->post("id");
        $this->Catalogos_model->updateCatalogo(array("usada"=>1,"fecha_uso"=>date("Y-m-d H:i:s")),$id,"pass_creadas");
    }

    public function preimpresion($id) {
        $data["id_cotizacion"]=$id;
        $data['cot']=$this->model->getCotizacion($id); //ok
        $data["serv"]=$this->model->getServiciosCotizacion($id,$data['cot']->id_empresa);
        $data["dets"]=$this->Catalogos_model->getCatalogoRow("detalles_cotiza_impre",array("id_cotizacion"=>$id));

        $data["fecha"]="Puebla de Zaragoza a ".date("d"). " de ".$this->nameMes(intval(date("m")))." de ".date("Y");
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('cotizaciones/preimpresion',$data);
        $this->load->view('footer');
        $this->load->view('cotizaciones/preimpresionjs');
    }

    function nameMes($m){
        $array_mes=array("","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
        return $array_mes[$m];
    }

    function submit_detalleImpresion(){
        $data=$this->input->post();
        $id=$this->input->post("id");
        if($id==0){
            $this->Catalogos_model->Insert('detalles_cotiza_impre',$data); 
        }else{
            $this->Catalogos_model->updateCatalogo($data,$id,"detalles_cotiza_impre");
        }
    }

    public function pdfCotizacion($id) {
        $data["id"]=$id;
        $data['cotizacion'] = $this->model->getCotizacion($id);
        $data["serv"]=$this->model->getServiciosCotizacion($id,0);
        $data["dets"]=$this->Catalogos_model->getCatalogoRow("detalles_cotiza_impre",array("id_cotizacion"=>$id));
        $this->load->view('reportes/cotizacion2',$data);
    }

}
