<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloNom extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getnom($params,$iduser){
        if($params["norma"]==11){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'11 as norma', 
                5=>"nom.num_informe",
                6=>'nom.fecha',
                7=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==25){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'eq.equipo',
                4=>'nom.reg', 
                5=>'nom.norma', 
                6=>'nom.txt_intro',
                7=>"nom.num_informe",
                8=>'nom.fecha',
                9=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==22){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'22 as norma', 
                5=>'eq.equipo',
                6=>"nom.num_informe",
                7=>'nom.fecha',
                8=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==81){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'81 as norma', 
                5=>'eq.equipo',
                6=>"nom.num_informe",
                7=>'nom.fecha',
                8=>'r.num_informe_rec',
                5=>'eq2.equipo',
            );
        }
        if($params["norma"]==15){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'15 as norma', 
                5=>"nom.num_informe",
                6=>'nom.fecha'
            );
        }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select.", em.nombre as tecnico, cli.empresa as cliente, concat(cli.calle, ' No. ',cli.no_ext, ' Int.' ,cli.no_int) as dir1, cli.colonia as dir2, concat(cli.poblacion,', ',es.estado) as dir3, cli.cp, s.nombre as servicio, chs.id as id_chs");
        if($params["norma"]==11){
            $this->db->from('nom11 as nom');
        }else if($params["norma"]==25){
            $this->db->from('nom');
        }else if($params["norma"]==22){
            $this->db->from('nom22 as nom');
        }else if($params["norma"]==81){
            $this->db->from('nom81 as nom');
        }else if($params["norma"]==15){
            $this->db->from('nom15 as nom');
        }

        $this->db->join('ordenes o', 'o.id=nom.idordenes',"left");
        $this->db->join('cotizaciones c', 'c.id=o.cotizacion_id',"left");
        $this->db->join('cotizaciones_has_servicios chs', 'chs.id=nom.id_chs');
        $this->db->join('servicios_ahisa s', 's.id=chs.servicio_id');
        if($params["norma"]==11){
            $this->db->join('reconocimiento_nom11 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==25){
            $this->db->join('equipo eq', 'eq.id=nom.equipo');
            $this->db->join('reconocimiento r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==22){
            $this->db->join('equipo eq', 'eq.id=nom.id_medidor');
            $this->db->join('reconocimiento_nom22 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==81){
            $this->db->join('equipo eq', 'eq.id=nom.id_sonometro');
            $this->db->join('equipo eq2', 'eq2.id=nom.id_calibrador');
            $this->db->join('reconocimiento_nom81 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }
        $this->db->join('clientes cli', 'cli.id=c.cliente_id',"left");
        $this->db->join('empleados em', 'em.id=o.id_tecnico',"left");
        $this->db->join('estados es', 'es.id=cli.estado',"left");

        $this->db->where(array('nom.activo'=>1));
        //$this->db->where("o.id_tecnico",$iduser);

        if($params["id_cliente"]!="0"){
            $this->db->where("c.cliente_id",$params["id_cliente"]);
        }
        if($params["fi"]!="" && $params["ff"]!=""){
           $this->db->where("nom.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        }
        if($params["norma"]!="0"){
            //$this->db->where("nom.norma",$params["norma"]);
        }

        
        if($params["norma"]==81){
            $this->db->group_by("nom.idordenes");
        }else{
            $this->db->group_by("nom.idnom");
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getnom_total($params,$iduser){
        if($params["norma"]==11){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'11 as norma', 
                5=>"nom.num_informe",
                6=>'nom.fecha',
                7=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==25){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'eq.equipo',
                4=>'nom.reg', 
                5=>'nom.norma', 
                6=>'nom.txt_intro',
                7=>"nom.num_informe",
                8=>'nom.fecha',
                9=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==22){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'22 as norma', 
                5=>'eq.equipo',
                6=>"nom.num_informe",
                7=>'nom.fecha',
                8=>'r.num_informe_rec'
            );
        }
        if($params["norma"]==81){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'81 as norma', 
                5=>'eq.equipo',
                6=>"nom.num_informe",
                7=>'nom.fecha',
                8=>'r.num_informe_rec',
                5=>'eq2.equipo',
            );
        }
        if($params["norma"]==15){
            $columns = array( 
                0=>'nom.idnom',
                1=>'nom.idordenes',
                2=>'nom.razon_social',
                3=>'nom.reg', 
                4=>'15 as norma', 
                5=>"nom.num_informe",
                6=>'nom.fecha'
            );
        }

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        if($params["norma"]==11){
            $this->db->from('nom11 as nom');
        }else if($params["norma"]==25){
            $this->db->from('nom');
        }else if($params["norma"]==22){
            $this->db->from('nom22 as nom');
        }else if($params["norma"]==81){
            $this->db->from('nom81 as nom');
        }else if($params["norma"]==15){
            $this->db->from('nom15 as nom');
        }

        $this->db->join('ordenes o', 'o.id=nom.idordenes',"left");
        $this->db->join('cotizaciones c', 'c.id=o.cotizacion_id');
        $this->db->join('cotizaciones_has_servicios chs', 'chs.id=nom.id_chs');
        if($params["norma"]==11){
            $this->db->join('reconocimiento_nom11 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==25){
            $this->db->join('equipo eq', 'eq.id=nom.equipo');
            $this->db->join('reconocimiento r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==22){
            $this->db->join('equipo eq', 'eq.id=nom.id_medidor');
            $this->db->join('reconocimiento_nom22 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }else if($params["norma"]==81){
            $this->db->join('equipo eq', 'eq.id=nom.id_sonometro');
            $this->db->join('equipo eq2', 'eq2.id=nom.id_calibrador');
            $this->db->join('reconocimiento_nom81 r', 'r.id_orden=o.id and r.id_chs=chs.id');
        }

        /*$this->db->join('equipo eq', 'eq.id=nom.equipo');
        
        $this->db->join('clientes cli', 'cli.id=c.cliente_id',"left");
        */

        $this->db->where(array('nom.activo'=>1));
        //$this->db->where("o.id_tecnico",$iduser);

        if($params["id_cliente"]!="0"){
            $this->db->where("c.cliente_id",$params["id_cliente"]);
        }
        if($params["fi"]!="" && $params["ff"]!=""){
           $this->db->where("nom.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        }
        if($params["norma"]!="0"){
            //$this->db->where("nom.norma",$params["norma"]);
        }
        if($params["norma"]==81){
            $this->db->group_by("nom.idordenes");
        }
        //$this->db->group_by("nom.idnom");
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        if(isset($query->row()->total))
            return $query->row()->total;
        else 
            return 0;
    }

    public function ordenaPuntos($id_nom,$fecha,$id,$num){
        /*$strq = "UPDATE nom25_detalle SET num_punto=num_punto-1 WHERE fecha='".$fecha."' and num_punto< ".$num."
        and idnom=".$id_nom." and id!=".$id." and num_punto>2 and activo=1";*/
        $strq = "UPDATE nom25_detalle SET num_punto=num_punto-1 WHERE fecha<='".$fecha."'
        and idnom=".$id_nom." and id!=".$id." and num_punto>2 and activo=1";
        $query = $this->db->query($strq);
        return $query;
    }

    public function getMaxPunto($id_nom){
        $this->db->select("num_punto as ultimo");
        $this->db->from("nom25_detalle");
        $this->db->where("idnom",$id_nom);
        $this->db->where("activo",1);
        $this->db->limit(1);
        $this->db->order_by("id","DESC");
        $query=$this->db->get(); 
        return $query->row();
    }

    public function getSearchCli($search){
        $strq = "SELECT * from clientes WHERE status=1 and (empresa like '%".$search."%' or alias like '%".$search."%' )";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getNormaChs($id){
        $this->db->select('s.norma');
        $this->db->from("cotizaciones_has_servicios chs");
        $this->db->join("servicios_ahisa s","s.id=chs.servicio_id");
        $this->db->where("chs.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }
    public function nomcondiciones($id_nom25,$fecha){
        $strq = "SELECT * from condiciones_nom25det WHERE estatus=1 and id_nom25=$id_nom25 and fecha='$fecha'";
        $query = $this->db->query($strq);
        return $query;
    }

    function getPuntosConclusion($id){
        $this->db->select('c.total_ptos, pc.*');
        $this->db->from("conclusiones c");
        $this->db->join("puntos_conclusion pc","pc.id_conclusion=c.id");
        $this->db->where("c.id_nom",$id);
        $this->db->order_by("tipo","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getFechasNom25Detalles($id){
        $this->db->select('fecha, COUNT(*) puntos');
        $this->db->from("nom25_detalle nd");
        $this->db->where("idnom",$id);
        $this->db->where("activo",1);
        $this->db->group_by("fecha");
        $this->db->order_by("fecha","asc");
        $query=$this->db->get(); 
        return $query;
    }

    function getPuntosDetalle25($where){
        $this->db->select('nd.*');
        $this->db->from("nom25_detalle nd");
        $this->db->where($where);
        $this->db->order_by("num_punto","asc");
        $this->db->order_by("fecha","asc");
        $query=$this->db->get(); 
        return $query;
    }

    function getPaso2_3($id){
        $this->db->select('p3.*, p2.area');
        $this->db->from("reconocimiento_paso3_nom11 p3");
        $this->db->join("reconocimiento_paso2_nom11 p2","p2.id_reconocimiento=p3.id_reconocimiento and p2.estatus=1 and p3.num=p2.num");
        $this->db->where("p3.id_reconocimiento",$id);
        $this->db->where("p3.estatus",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEquiposNom($id){
        $this->db->select('e.*, ed.num_calibra, ed.fecha_calibra, e2.marca as marcacal, e2.modelo as modelocal, e2.equipo as equipocal,
                            e2.no_informe_calibracion as no_informe_calibracioncal, e2.fecha_calibracion as fecha_calibracioncal,
                            n.id_sonometro, n.id_calibrador');
        $this->db->from("nom11 n");
        $this->db->join("equipo e","e.id=n.id_sonometro");
        $this->db->join("equipo e2","e2.id=n.id_calibrador");
        $this->db->join("equipo_sonometro_detalle ed","ed.id_equipo=e.id");
        $this->db->where("n.idnom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEquiposPersonalNom($id){
        $this->db->select('e.*, np.idnomd');
        $this->db->from("nom11_personal np");
        $this->db->join("equipo e","e.id=np.id_dosimetro and np.activo=1");
        $this->db->where("np.idnom",$id);
        $this->db->where("np.activo",1);
        $this->db->order_by("np.idnomd","asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getCertEquipos($id,$id2){
        $this->db->select('*');
        $this->db->from("equipo_certificado ce");
        $this->db->where("(id_equipo=".$id." or id_equipo=".$id2.")");
        $this->db->where("estatus",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getCertEquipos015($id,$id2,$id3){
        $this->db->select('*');
        $this->db->from("equipo_certificado ce");
        $this->db->where("(id_equipo=".$id." or id_equipo=".$id2." or id_equipo=".$id3.")");
        $this->db->where("estatus",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getCertEquiposPersonal($id){
        $this->db->select('ec.*');
        $this->db->from("nom11_personal np");
        $this->db->join("equipo_certificado ec","ec.id_equipo=np.id_dosimetro and ec.estatus=1");
        $this->db->where("idnom",$id);
        $this->db->where("np.activo",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    /*function getDatosNom22($id){
        $this->db->select('nd.*, IFNULL(vr.id,0) as id_valor,IFNULL(d1,"") as d1,IFNULL(d2,"") as d2,IFNULL(d3,"") as d3,IFNULL(d4,"") as d4,IFNULL(d5,"") as d5,IFNULL(d6,"") as d6,IFNULL(d7,"") as d7,IFNULL(d8,"") as d8,IFNULL(d9,"") as d9,
                                IFNULL(r1,"") as r1,IFNULL(r2,"") as r2,IFNULL(r3,"") as r3,IFNULL(r4,"") as r4,IFNULL(r5,"") as r5,IFNULL(r6,"") as r6,IFNULL(r7,"") as r7,IFNULL(r8,"") as r8,IFNULL(r9,"") as r9,
                                IFNULL(fg.id,0) as id_fuente, IFNULL(fg.fuente,"") as fuente, IFNULL(fg.valor,"") as valor, IFNULL(fg.caracteristica,"") as caracteristica, IFNULL(fg.medicion_humedad,"") as medicion_humedad, IFNULL(fg.observaciones,"") as observaciones,
                                IFNULL(fg2.id,0) as id_fuente2, IFNULL(fg2.fuente,"") as fuente2, IFNULL(fg2.valor,"") as valor2, IFNULL(fg2.caracteristica,"") as caracteristica2,
                                IFNULL(fg3.id,0) as id_fuente3, IFNULL(fg3.fuente,"") as fuente3, IFNULL(fg3.valor,"") as valor3, IFNULL(fg3.caracteristica,"") as caracteristica3,
                                IFNULL(fg4.id,0) as id_fuente4, IFNULL(fg4.fuente,"") as fuente4, IFNULL(fg4.valor,"") as valor4, IFNULL(fg4.caracteristica,"") as caracteristica4');
        $this->db->from("nom22d nd");
        $this->db->join("valores_resistencia_nom22 vr","vr.id_nom22d=nd.id and vr.activo=1","left");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom22d=nd.id and fg.activo=1 and fg.tipo=1","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom22d=nd.id and fg2.activo=1 and fg2.tipo=2","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom22d=nd.id and fg3.activo=1 and fg3.tipo=3","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom22d=nd.id and fg4.activo=1 and fg4.tipo=4","left");
        $this->db->where("nd.idnom",$id);
        $this->db->where("nd.activo",1);
        //$this->db->group_by("nd.idnom");
        $this->db->order_by("nd.punto","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }*/

    function getDatosNom22($id){
        $this->db->select('nd.*, 0 as id_valor,IFNULL(d1,"") as d1,IFNULL(d2,"") as d2,IFNULL(d3,"") as d3,IFNULL(d4,"") as d4,IFNULL(d5,"") as d5,IFNULL(d6,"") as d6,IFNULL(d7,"") as d7,IFNULL(d8,"") as d8,IFNULL(d9,"") as d9,
                                IFNULL(r1,"") as r1,IFNULL(r2,"") as r2,IFNULL(r3,"") as r3,IFNULL(r4,"") as r4,IFNULL(r5,"") as r5,IFNULL(r6,"") as r6,IFNULL(r7,"") as r7,IFNULL(r8,"") as r8,IFNULL(r9,"") as r9');
        $this->db->from("nom22d nd");
        $this->db->where("nd.idnom",$id);
        $this->db->where("nd.activo",1);
        //$this->db->group_by("nd.punto");
        $this->db->order_by("nd.punto","ASC");
        $this->db->order_by("nd.id","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getFuentesNom22($id){
        $this->db->select('nd.punto, nd.id,
                            IFNULL(fg.id,0) as id_fuente, IFNULL(fg.fuente,"") as fuente, IFNULL(fg.valor,"") as valor, IFNULL(fg.caracteristica,"") as caracteristica, IFNULL(fg.medicion_humedad,"") as medicion_humedad, IFNULL(fg.observaciones,"") as observaciones,
                            IFNULL(fg2.id,0) as id_fuente2, IFNULL(fg2.fuente,"") as fuente2, IFNULL(fg2.valor,"") as valor2, IFNULL(fg2.caracteristica,"") as caracteristica2,
                            IFNULL(fg3.id,0) as id_fuente3, IFNULL(fg3.fuente,"") as fuente3, IFNULL(fg3.valor,"") as valor3, IFNULL(fg3.caracteristica,"") as caracteristica3,
                            IFNULL(fg4.id,0) as id_fuente4, IFNULL(fg4.fuente,"") as fuente4, IFNULL(fg4.valor,"") as valor4, IFNULL(fg4.caracteristica,"") as caracteristica4');
        $this->db->from("nom22d nd");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom22d=nd.id and fg.activo=1 and fg.tipo=1","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom22d=nd.id and fg2.activo=1 and fg2.tipo=2","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom22d=nd.id and fg3.activo=1 and fg3.tipo=3","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom22d=nd.id and fg4.activo=1 and fg4.tipo=4","left");
        $this->db->where("nd.idnom",$id);
        $this->db->where("nd.activo",1);
        //$this->db->group_by("nd.punto");
        $this->db->order_by("nd.punto","ASC");
        $this->db->order_by("nd.id","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }

    /*function getFuentesNom22($id){
        $this->db->select('nd.punto, nd.id,
                            IFNULL(fg.id,0) as id_fuente, IFNULL(fg.fuente,"") as fuente, IFNULL(fg.valor,"") as valor, IFNULL(fg.caracteristica,"") as caracteristica, IFNULL(fg.medicion_humedad,"") as medicion_humedad, IFNULL(fg.observaciones,"") as observaciones,
                            IFNULL(fg2.id,0) as id_fuente2, IFNULL(fg2.fuente,"") as fuente2, IFNULL(fg2.valor,"") as valor2, IFNULL(fg2.caracteristica,"") as caracteristica2,
                            IFNULL(fg3.id,0) as id_fuente3, IFNULL(fg3.fuente,"") as fuente3, IFNULL(fg3.valor,"") as valor3, IFNULL(fg3.caracteristica,"") as caracteristica3,
                            IFNULL(fg4.id,0) as id_fuente4, IFNULL(fg4.fuente,"") as fuente4, IFNULL(fg4.valor,"") as valor4, IFNULL(fg4.caracteristica,"") as caracteristica4');
        $this->db->from("nom22d nd");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom=nd.idnom and fg.activo=1 and fg.tipo=1 and nd.punto=fg.punto","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom=nd.idnom and fg2.activo=1 and fg2.tipo=2 and nd.punto=fg2.punto","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom=nd.idnom and fg3.activo=1 and fg3.tipo=3 and nd.punto=fg3.punto","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom=nd.idnom and fg4.activo=1 and fg4.tipo=4 and nd.punto=fg4.punto","left");
        $this->db->where("nd.idnom",$id);
        $this->db->group_by("id_fuente");
        $this->db->order_by("fg.punto","asc");
        $query=$this->db->get(); 
        return $query->result();
    }*/

    function getDatosTotalNom22($id){
        $sql = '
            SELECT nd.*, 0 as id_valor
            FROM nom22d nd
            WHERE nd.idnom = 1
            AND nd.activo = 1
            UNION
            SELECT nd2.punto, nd2.id, 0 as  id_valor, 26 as com26,27 as com27, 28 as com28,
            IFNULL(fg.id, 0) as id_fuente, IFNULL(fg.fuente, "") as fuente, IFNULL(fg.valor, "") as valor, IFNULL(fg.tipo, "") as tipo, IFNULL(fg.caracteristica, "") as caracteristica,
            IFNULL(fg.medicion_humedad, "") as medicion_humedad, IFNULL(fg.observaciones, "") as observaciones,
    
            IFNULL(fg2.id, 0) as id_fuente2, IFNULL(fg2.fuente, "") as fuente2, IFNULL(fg2.valor, "") as valor2, IFNULL(fg2.tipo, "") as tipo2, 
            IFNULL(fg2.caracteristica, "") as caracteristica2,
            IFNULL(fg3.id, 0) as id_fuente3, IFNULL(fg3.fuente, "") as fuente3, IFNULL(fg3.valor, "") as valor3, IFNULL(fg3.tipo, "") as tipo3, 
            IFNULL(fg3.caracteristica, "") as caracteristica3,
            IFNULL(fg4.id, 0) as id_fuente4, IFNULL(fg4.fuente, "") as fuente4, IFNULL(fg4.valor, "") as valor4, IFNULL(fg4.tipo, "") as tipo4, 
            IFNULL(fg4.caracteristica, "") as caracteristica4
            FROM nom22d nd2
            LEFT JOIN fuentes_genera_nom22 fg ON fg.id_nom22d=nd2.id and fg.activo=1 and fg.tipo=1
            LEFT JOIN fuentes_genera_nom22 fg2 ON fg2.id_nom22d=nd2.id and fg2.activo=1 and fg2.tipo=2
            LEFT JOIN fuentes_genera_nom22 fg3 ON fg3.id_nom22d=nd2.id and fg3.activo=1 and fg3.tipo=3
            LEFT JOIN fuentes_genera_nom22 fg4 ON fg4.id_nom22d=nd2.id and fg4.activo=1 and fg4.tipo=4
            WHERE nd2.idnom = 1
            AND nd2.activo = 1
            group by nd2.punto';
        $query = $this->db->query($sql); 
        return $query;
        /*$sql = 'select * from (
            SELECT nd.*, 0 as id_valor, nd.id as id_nd
            FROM nom22d nd
            WHERE nd.idnom = 1
            AND nd.activo = 1
            UNION all
            SELECT nd2.punto, nd2.id, nd2.id as id_nd, 0 as  id_valor, 26 as com26,27 as com27, 28 as com28,
            IFNULL(fg.id, 0) as id_fuente, IFNULL(fg.fuente, "") as fuente, IFNULL(fg.valor, "") as valor, IFNULL(fg.tipo, "") as tipo, IFNULL(fg.caracteristica, "") as caracteristica,
            IFNULL(fg.medicion_humedad, "") as medicion_humedad, IFNULL(fg.observaciones, "") as observaciones,
    
            IFNULL(fg2.id, 0) as id_fuente2, IFNULL(fg2.fuente, "") as fuente2, IFNULL(fg2.valor, "") as valor2, IFNULL(fg2.tipo, "") as tipo2, 
            IFNULL(fg2.caracteristica, "") as caracteristica2,
            IFNULL(fg3.id, 0) as id_fuente3, IFNULL(fg3.fuente, "") as fuente3, IFNULL(fg3.valor, "") as valor3, IFNULL(fg3.tipo, "") as tipo3, 
            IFNULL(fg3.caracteristica, "") as caracteristica3,
            IFNULL(fg4.id, 0) as id_fuente4, IFNULL(fg4.fuente, "") as fuente4, IFNULL(fg4.valor, "") as valor4, IFNULL(fg4.tipo, "") as tipo4, 
            IFNULL(fg4.caracteristica, "") as caracteristica4
            FROM nom22d nd2
            LEFT JOIN fuentes_genera_nom22 fg ON fg.id_nom22d=nd2.id and fg.activo=1 and fg.tipo=1
            LEFT JOIN fuentes_genera_nom22 fg2 ON fg2.id_nom22d=nd2.id and fg2.activo=1 and fg2.tipo=2
            LEFT JOIN fuentes_genera_nom22 fg3 ON fg3.id_nom22d=nd2.id and fg3.activo=1 and fg3.tipo=3
            LEFT JOIN fuentes_genera_nom22 fg4 ON fg4.id_nom22d=nd2.id and fg4.activo=1 and fg4.tipo=4
            WHERE nd2.idnom = 1
            AND nd2.activo = 1
            group by nd2.punto
        ) a
        group by punto
        ORDER BY punto ASC';
        $query = $this->db->query($sql); 
        return $query;*/

        /*$this->db->select('
                nd.*, IFNULL(vr.id,0) as id_valor,IFNULL(d1,"") as d1,IFNULL(d2,"") as d2,IFNULL(d3,"") as d3,IFNULL(d4,"") as d4,IFNULL(d5,"") as d5,IFNULL(d6,"") as d6,IFNULL(d7,"") as d7,IFNULL(d8,"") as d8,IFNULL(d9,"") as d9,
                                IFNULL(r1,"") as r1,IFNULL(r2,"") as r2,IFNULL(r3,"") as r3,IFNULL(r4,"") as r4,IFNULL(r5,"") as r5,IFNULL(r6,"") as r6,IFNULL(r7,"") as r7,IFNULL(r8,"") as r8,IFNULL(r9,"") as r9');
        $this->db->from("nom22d nd");
        $this->db->join("valores_resistencia_nom22 vr","vr.id_nom22d=nd.id and vr.activo=1","left");
        $this->db->where("nd.idnom",$id);
        $this->db->where("nd.activo",1);
        //$this->db->order_by("nd.punto","ASC");
        $query1 = $this->db->get_compiled_select();

        $this->db->select('nd2.*, 0 as  comodin,
                            IFNULL(fg.id,0) as id_fuente, IFNULL(fg.fuente,"") as fuente, IFNULL(fg.valor,"") as valor, IFNULL(fg.caracteristica,"") as caracteristica, IFNULL(fg.medicion_humedad,"") as medicion_humedad, IFNULL(fg.observaciones,"") as observaciones,
                            IFNULL(fg2.id,0) as id_fuente2, IFNULL(fg2.fuente,"") as fuente2, IFNULL(fg2.valor,"") as valor2, IFNULL(fg2.caracteristica,"") as caracteristica2,
                            IFNULL(fg3.id,0) as id_fuente3, IFNULL(fg3.fuente,"") as fuente3, IFNULL(fg3.valor,"") as valor3, IFNULL(fg3.caracteristica,"") as caracteristica3,
                            IFNULL(fg4.id,0) as id_fuente4, IFNULL(fg4.fuente,"") as fuente4, IFNULL(fg4.valor,"") as valor4, IFNULL(fg4.caracteristica,"") as caracteristica4');
        $this->db->from("nom22d nd");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom22d=nd.id and fg.activo=1 and fg.tipo=1","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom22d=nd.id and fg2.activo=1 and fg2.tipo=2","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom22d=nd.id and fg3.activo=1 and fg3.tipo=3","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom22d=nd.id and fg4.activo=1 and fg4.tipo=4","left");
        $this->db->where("nd.idnom",$id);
        $this->db->where("nd.activo",1);
        $this->db->order_by("nd.punto","ASC"); 
        $query2 = $this->db->get_compiled_select(); 

        $queryunions=$query1." UNION ".$query2;
        return $this->db->query($queryunions)->result();*/
    }

    function getAllFuentes($id){
        $this->db->select('*');
        $this->db->from("fuentes_genera_nom22 fg");
        $this->db->where("fg.id_nom",$id);
        $this->db->where("fg.activo",1);
        $this->db->where("fg.fuente!=","");
        $this->db->order_by("fg.punto","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEquiposNom22($id){
        $this->db->select('e.*, e2.marca as marcacal, e2.modelo as modelocal, e2.equipo as equipocal,
                            e2.no_informe_calibracion as no_informe_calibracioncal, e2.fecha_calibracion as fecha_calibracioncal,
                            n.id_medidor, n.id_multimetro, e3.marca as marcare, e3.modelo as modelore, e3.equipo as equipore,
                            e3.no_informe_calibracion as no_informe_calibracionre, e3.fecha_calibracion as fecha_calibracionre,
                            n.id_medidor, n.id_multimetro, id_set_resis');
        $this->db->from("nom22 n");
        $this->db->join("equipo e","e.id=n.id_medidor");
        $this->db->join("equipo e2","e2.id=n.id_multimetro");
        $this->db->join("equipo e3","e3.id=n.id_set_resis");
        $this->db->where("n.idnom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getSisParaRayoNom22($id){
        $this->db->select('n.area, fg.caracteristica as tipo_sist, fg3.caracteristica as altura, fg2.caracteristica as ubicacion, fg4.caracteristica as area_cober');
        $this->db->from("nom22d n");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom22d=n.id and fg.tipo=1","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom22d=n.id and fg2.tipo=2","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom22d=n.id and fg3.tipo=3","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom22d=n.id and fg4.tipo=4","left");
        $this->db->where("n.idnom",$id);
        $this->db->where("n.pararrayo",1);
        $this->db->where("n.activo",1);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getFuentesPDFNom22($id,$idnomd,$punto){
        $this->db->select('IFNULL(fg.id,0) as id_fuente, IFNULL(fg.fuente,"") as fuente, IFNULL(fg.valor,"") as valor, IFNULL(fg.caracteristica,"") as caracteristica, IFNULL(fg.medicion_humedad,"") as medicion_humedad, IFNULL(fg.observaciones,"") as observaciones,
                            IFNULL(fg2.id,0) as id_fuente2, IFNULL(fg2.fuente,"") as fuente2, IFNULL(fg2.valor,"") as valor2, IFNULL(fg2.caracteristica,"") as caracteristica2,
                            IFNULL(fg3.id,0) as id_fuente3, IFNULL(fg3.fuente,"") as fuente3, IFNULL(fg3.valor,"") as valor3, IFNULL(fg3.caracteristica,"") as caracteristica3,
                            IFNULL(fg4.id,0) as id_fuente4, IFNULL(fg4.fuente,"") as fuente4, IFNULL(fg4.valor,"") as valor4, IFNULL(fg4.caracteristica,"") as caracteristica4');
        $this->db->from("nom22d n");
        $this->db->join("fuentes_genera_nom22 fg","fg.id_nom22d=n.id and fg.tipo=1 and fg.punto=$punto and fg.id_nom22d=$idnomd","left");
        $this->db->join("fuentes_genera_nom22 fg2","fg2.id_nom22d=n.id and fg2.tipo=2 and fg2.punto=$punto and fg2.id_nom22d=$idnomd","left");
        $this->db->join("fuentes_genera_nom22 fg3","fg3.id_nom22d=n.id and fg3.tipo=3 and fg3.punto=$punto and fg3.id_nom22d=$idnomd","left");
        $this->db->join("fuentes_genera_nom22 fg4","fg4.id_nom22d=n.id and fg4.tipo=4 and fg4.punto=$punto and fg4.id_nom22d=$idnomd","left");
        $this->db->where("n.idnom",$id);
        $this->db->where("n.activo",1);
        $this->db->order_by("fg.punto","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEquiposNom81($id){
        $this->db->select('e.*, e2.marca as marcacal, e2.modelo as modelocal, e2.equipo as equipocal,
                            e2.no_informe_calibracion as no_informe_calibracioncal, e2.fecha_calibracion as fecha_calibracioncal,
                            n.id_sonometro, n.id_calibrador');
        $this->db->from("nom81 n");
        $this->db->join("equipo e","e.id=n.id_sonometro");
        $this->db->join("equipo e2","e2.id=n.id_calibrador");
        $this->db->where("n.idnom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    /*function getEquiposNom15($id){
        $this->db->select('e.*, e2.tipo, e2.marca as marca_dig, e2.modelo as modelo_dig, e2.equipo as equipo_dig,
                            e2.no_informe_calibracion as no_informe_calibraciondig, e2.fecha_calibracion as fecha_calibraciondig,
                            e3.tipo, e3.marca as marca_anemo, e3.modelo as modeloanemo, e3.equipo as equipoanemo,
                            e3.no_informe_calibracion as no_informe_calibracionanemo, e3.fecha_calibracion as fecha_calibracionanemo,
                            n.id_termo, n.id_termo_digi, n.id_anemo');
        $this->db->from("nom15_punto n");
        $this->db->join("equipo e","e.id=n.id_termo");
        $this->db->join("equipo e2","e2.id=n.id_termo_digi");
        $this->db->join("equipo e3","e3.id=n.id_anemo");
        $this->db->where("n.id_nom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }*/
    function getEquiposNom15($id){
        $this->db->select('e.*,
                            e3.tipo, e3.marca as marca_anemo, e3.modelo as modeloanemo, e3.equipo as equipoanemo,
                            e3.no_informe_calibracion as no_informe_calibracionanemo, e3.fecha_calibracion as fecha_calibracionanemo,
                            n.id_termo, n.id_termo_digi, n.id_anemo');
        $this->db->from("nom15_punto n");
        $this->db->join("equipo e","e.id=n.id_termo");
        //$this->db->join("equipo e2","e2.id=n.id_termo_digi");
        $this->db->join("equipo e3","e3.id=n.id_anemo");
        $this->db->where("n.id_nom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getEquiposNom15Elevada($id){
        $this->db->select('e.*,
                            e2.tipo, e2.marca as marca2, e2.modelo as modelo2, e2.equipo as equipo2,
                            e2.no_informe_calibracion as no_informe_calibracion2, e2.fecha_calibracion as fecha_calibracion2,
                            e3.tipo, e3.marca as marca3, e3.modelo as modelo3, e3.equipo as equipo3,
                            e3.no_informe_calibracion as no_informe_calibracion3, e3.fecha_calibracion as fecha_calibracion3,
                            n.id_bulbo, n.id_bulbo_humedo, n.id_globo');
        $this->db->from("nom15_punto_elevada n");
        $this->db->join("equipo e","e.id=n.id_bulbo");
        $this->db->join("equipo e2","e2.id=n.id_bulbo_humedo");
        $this->db->join("equipo e3","e3.id=n.id_globo");
        $this->db->where("n.id_nom",$id);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getRecoTipoNom15($id,$id_chs){
        $this->db->select('r.*, s.tipo_norma');
        $this->db->from("reconocimiento_nom15 r");
        $this->db->join("cotizaciones_has_servicios chs","chs.id=r.id_chs");
        $this->db->join("servicios_ahisa s","s.id=chs.servicio_id");
        $this->db->where("r.id_orden",$id);
        $this->db->where("r.id_chs",$id_chs);
        $query=$this->db->get(); 
        return $query;
    }

}