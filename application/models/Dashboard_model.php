<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getLastMonths($meses){ //obtiene la sumatoria de cotizaciones de los ultimos 6 meses 
    	$meses=$meses-1; 
        //$where="and id_empresa=4"; 
        $where="and id_empresa=1"; 
    	$sql="SELECT year(fecha_creacion) as anio, month(fecha_creacion) as mes, sum(1) AS no_cotizaciones 
        FROM cotizaciones 
        WHERE fecha_creacion >= curdate() - interval $meses month ".$where."
        GROUP BY year(fecha_creacion), month(fecha_creacion)";
    	$query = $this->db->query($sql);
        return $query->result();

    }

}