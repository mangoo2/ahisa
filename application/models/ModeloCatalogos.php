<?php

class ModeloCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }

    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
        return $this->db->insert_id();
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);

        if ($this->db->affected_rows() > 0) {
            // La consulta afectó al menos una fila, se ejecutó correctamente
            log_message('error', "La consulta se ejecutó correctamente.") ;
        } else {
            // No se afectó ninguna fila, podría significar que la consulta no se ejecutó o no afectó datos existentes
            log_message('error', "No se realizaron cambios en la base de datos.") ;
        }
        //return $id;
    }
    function updateCatalogo_batch($Tabla,$data,$where){
        $this->db->update_batch($Tabla, $data, $where);
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }

    function getselectwherenOrder($table,$where,$order,$od){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($order,$od);
        $query=$this->db->get(); 
        return $query;
    }
    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function interseccion_pendiente($idequipo,$tipo){
        $datos=$this->getselectwheren('equipo_detalle',array('idnom'=>$idequipo,'activo'=>1));

        $x=array();
        $y=array();
        $intercept=0; $slope=0;
        foreach ($datos->result() as $item) {
            //$x[]=$item->iluminancia_ref;
            //$y[]=$item->iluminancia_pro;

            $x[]=$item->iluminancia_pro;
            $y[]=$item->iluminancia_ref;

        }

        $n     = count($x);     // number of items in the array
        $x_sum = array_sum($x); // sum of all X values
        $y_sum = array_sum($y); // sum of all Y values

        $xx_sum = 0;
        $xy_sum = 0;
     
        for($i = 0; $i < $n; $i++) {
            
            $xy_sum += round( $x[$i]*$y[$i] ,2);
            $xx_sum += round( $x[$i]*$x[$i] ,2);
        }

        if($x_sum>0 && $y_sum>0){
            // Slope
            if(( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) )>0){
                $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
            }else{
                $slope=0;
            }
            
         
            // calculate intercept
            $intercept = ( $y_sum -  $slope * $x_sum ) / $n;
        }

        if($tipo==0){
            $viewdatos=$intercept;
        }else{
            $viewdatos=$slope;
        }
        return $viewdatos;

    }

    function interseccion_pendiente2($idequipo,$tipo,$tipo_equipo){ //termometro
        $x=array();
        $y=array();
        $intercept=0; $slope=0;

        if($tipo_equipo==8){
            $datos=$this->getselectwheren('equipo_termometro_detalle',array('id_equipo'=>$idequipo,'estatus'=>1));
            foreach ($datos->result() as $item) {
                $y[]=$item->temp_patron;
                $x[]=$item->temp_ibc;
            }
        }
        else if($tipo_equipo==10){
           $datos=$this->getselectwheren('equipo_anemometro_detalle',array('id_equipo'=>$idequipo,'estatus'=>1));
            foreach ($datos->result() as $item) {
                $y[]=$item->vel_patron;
                $x[]=$item->vel_ibc;
            } 
        }else if($tipo_equipo==11){ //bulbo seco
           $datos=$this->getselectwheren('equipo_bulbo_detalle',array('id_equipo'=>$idequipo,'tipo'=>'1','estatus'=>1));
            foreach ($datos->result() as $item) {
                $y[]=$item->patron;
                $x[]=$item->libc;
            } 
        }else if($tipo_equipo==12){ //bulbo humedo
           $datos=$this->getselectwheren('equipo_bulbo_detalle',array('id_equipo'=>$idequipo,'tipo'=>'2','estatus'=>1));
            foreach ($datos->result() as $item) {
                $y[]=$item->patron;
                $x[]=$item->libc;
            } 
        }else if($tipo_equipo==13){ //globo
           $datos=$this->getselectwheren('equipo_bulbo_detalle',array('id_equipo'=>$idequipo,'tipo'=>'3','estatus'=>1));
            foreach ($datos->result() as $item) {
                $y[]=$item->patron;
                $x[]=$item->libc;
            } 
        }
        $n     = count($x);     // number of items in the array
        $x_sum = array_sum($x); // sum of all X values
        $y_sum = array_sum($y); // sum of all Y values

        $xx_sum = 0;
        $xy_sum = 0;
     
        for($i = 0; $i < $n; $i++) {
            $xy_sum += round( $x[$i]*$y[$i] ,6);
            $xx_sum += round( $x[$i]*$x[$i] ,6);
        }

        //if($x_sum>0 && $y_sum>0){
            // Slope
            if(( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) )>0){
                $slope = round(( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) ),6);
            }else{
                $slope=0;
            }
            // calculate intercept
            $intercept = round(( $y_sum -  $slope * $x_sum ) / $n,6);
            //log_message('error', 'intercept: '.$intercept);
        //}

        if($tipo==0){
            $viewdatos=$intercept;
        }else{
            $viewdatos=$slope;
        }
        return $viewdatos;
    }
    function interseccion_pendiente3($idequipo,$tipo,$shg){ //termometro
        //$shg 1 seco 2 bulbo humedo 3globo
        $x=array();
        $y=array();
        $intercept=0; $slope=0;

       $datos=$this->getselectwheren('equipo_bulbo_detalle',array('id_equipo'=>$idequipo,'tipo'=>$shg,'estatus'=>1));
        foreach ($datos->result() as $item) {
            $y[]=$item->patron;
            $x[]=$item->libc;
        } 
        $n     = count($x);     // number of items in the array
        $x_sum = array_sum($x); // sum of all X values
        $y_sum = array_sum($y); // sum of all Y values

        $xx_sum = 0;
        $xy_sum = 0;
     
        for($i = 0; $i < $n; $i++) {
            $xy_sum += round( $x[$i]*$y[$i] ,6);
            $xx_sum += round( $x[$i]*$x[$i] ,6);
        }

        //if($x_sum>0 && $y_sum>0){
            // Slope
            if(( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) )>0){
                $slope = round(( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) ),6);
            }else{
                $slope=0;
            }
            // calculate intercept
            //$intercept = round(( $y_sum -  $slope * $x_sum ) / $n,6);

            if ($n > 0) {
                $slope = round(( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) ),6);
                $intercept = round(( $y_sum -  $slope * $x_sum ) / $n,6);
            } else {
                $slope = 0;
                $intercept = 0;
            }

            //log_message('error', 'intercept: '.$intercept);
        //}

        if($tipo==0){
            $viewdatos=$intercept;
        }else{
            $viewdatos=$slope;
        }
        return $viewdatos;
    }

    function desvest($aValues){
        $fMean = array_sum($aValues) / count($aValues);
        $fVariance = 0.0;
        foreach ($aValues as $i){
            $fVariance += pow($i - $fMean, 2);
        }       
        $size = count($aValues) - 1;
        return (float) sqrt($fVariance)/sqrt($size);
    }
    public function infoordencotizacion($idorden) {
        $sql = "SELECT 
                    /*cli.empresa,
                    cli.calle,cli.no_ext,cli.no_int,cli.colonia,cli.estado,cli.cp_fiscal,cli.razon_social,cli.rfc,cli.giro,
                    cli.representa,*/
                    emple.nombre as tecnico,
                    emple.firma as firmaemple,
                    resp.firma as firmaresp,
                    emple.cedula
                FROM ordenes as ord 
                INNER JOIN cotizaciones as cot on cot.id=ord.cotizacion_id
                LEFT JOIN empleados as resp on resp.perfil=6
                /*INNER JOIN clientes as cli on cli.id=cot.cliente_id*/
                inner join empleados as emple on emple.id=ord.id_tecnico
                WHERE ord.id=$idorden";
        $query = $this->db->query($sql); 
        return $query;
    }

    function getClienteOrden($id){
        $this->db->select('cli.empresa as cliente, cli.razon_social as razon_cli, r.cliente as razoncli');
        //$this->db->from("nom n");
        //$this->db->join("ordenes o","o.id=n.idordenes");
        $this->db->from("ordenes o");
        $this->db->join("nom n","n.idordenes=o.id","left");
        $this->db->join("cotizaciones c","c.id=o.cotizacion_id");
        $this->db->join("clientes cli","cli.id=c.cliente_id");
        $this->db->join("reconocimiento r","r.id_orden=o.id");
        //$this->db->where("idnom",$id);
        $this->db->where("o.id",$id);
        $query=$this->db->get(); 
        return $query->row();
    }

    function getPtosFecha($where){
        $this->db->select('*');
        $this->db->from("nom25_detalle");
        $this->db->where($where);
        $this->db->order_by("fecha asc");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getPtosFechaGroup($where){
        $this->db->select('*');
        $this->db->from("nom25_detalle");
        $this->db->where($where);
        $this->db->group_by("fecha","ASC");
        $query=$this->db->get(); 
        return $query->result();
    }

    function getDataReco($id){
        $this->db->select('rp.*,rp2.puesto');
        $this->db->from("reconocimiento_paso2_nom25 rp");
        $this->db->join("reconocimiento_paso2_nom25 rp2","rp2.id_reconocimiento=rp.id_reconocimiento and rp2.num=rp.num","left");
        $this->db->where("rp.id_reconocimiento",$id);
        $this->db->where("rp.tipo",1);
        $this->db->where("rp.estatus",1);
        $this->db->group_by("rp.id");
        
        $query=$this->db->get(); 
        return $query->result();
    }

    function getDataReco2($id,$tipo){
        $this->db->select('rp.*,rp2.area,pt.puesto_trabajo,pt.identificacion');
        $this->db->from("reconocimiento_paso2_nom25 rp");
        $this->db->join("reconocimiento_paso2_nom25 rp2","rp2.id_reconocimiento=rp.id_reconocimiento and rp2.num=rp.num","left");
        $this->db->join("puestos_trabajo pt","pt.id=rp.tarea_visual","left");
        $this->db->where("rp.id_reconocimiento",$id);
        $this->db->where("rp.tipo_tabla",2);
        $this->db->where("rp.estatus",1);
        $this->db->where("rp.actividades!=","");
        $this->db->group_by("rp.id");
        
        $query=$this->db->get(); 
        return $query->result();
    }
    public function maxpuntonom11($idnom) {
        $sql = "SELECT max(punto) as punto FROM nom11d_puntos WHERE idnom=$idnom and activo=1";
        $query = $this->db->query($sql); 
        $punto=0;
        foreach ($query->result() as $item) {
            $punto=$item->punto;
        }
        $punto=$punto+1;
        return $punto;
    }
    public function maxnumnom11_personal($idnom) {
        $sql = "SELECT max(num) as num FROM nom11_personal WHERE idnom=$idnom and activo=1";
        $query = $this->db->query($sql); 
        $punto=0;
        foreach ($query->result() as $item) {
            $punto=$item->num;
        }
        $punto=$punto+1;
        return $punto;
    }
    public function maxnumnom11_personal_ti($idnom) {
        $sql = "SELECT max(num) as num FROM nom11_personal_ti WHERE idnom=$idnom and activo=1";
        $query = $this->db->query($sql); 
        $punto=0;
        foreach ($query->result() as $item) {
            $punto=$item->num;
        }
        $punto=$punto+1;
        return $punto;
    }

    public function table2a($idnom) {
        $sql = "SELECT 
              nomp.num, 
                nomp.area,nomp.puesto,nomp.trabajador,
                nompti.nrr,nompti.ti,nompti.tf
            FROM nom11_personal as nomp 
            INNER JOIN nom11_personal_ti as nompti on nompti.idnom=nomp.idnom and nompti.num=nomp.num and nompti.activo=1
            WHERE nomp.idnom=$idnom
            and nomp.activo=1";
        $query = $this->db->query($sql); 
        
        return $query;
    }
    function nom11personalequipos($idnom){
        $sql = "SELECT eq.* 
                FROM nom11_personal AS np 
                INNER JOIN equipo as eq on eq.id=np.id_dosimetro 
                WHERE np.idnom=$idnom and np.activo=1
                GROUP by eq.id";
        $query = $this->db->query($sql); 
        
        return $query;   
    }
    function nom11personalequiposti($idnom){
        $sql = "SELECT 
                    nop.num,eq.luxometro,nop.area,nop.trabajador,nop.puesto,nopt.ti,nopt.tf,nop.dosis 
                FROM nom11_personal as nop 
                INNER JOIN equipo as eq on eq.id=nop.id_dosimetro 
                LEFT JOIN nom11_personal_ti as nopt on nopt.idnom=nop.idnom and nopt.num=nop.num AND nopt.activo=1 
                WHERE nop.idnom=$idnom and nop.activo=1";
        $query = $this->db->query($sql); 
        
        return $query;   
    } 
    function infonomelevadas($id_nom){
        //temp.id=p.id no se si sea lo correcto
        $sql = "SELECT p.id,p.area,p.num_punto,p.num_ciclos,p.tiempo_expo,temp.temp_fin,temp.temp_fin,p.lvf_prom
                FROM nom15_punto as p 
                INNER JOIN medicion_temp_nom15 as temp on temp.id=p.id
                WHERE p.id_nom=$id_nom and p.activo=1";
        $query = $this->db->query($sql);
        return $query; 
    }
    function nom25_detalle_groupby($idnom){
        $sql = "SELECT idnom,fecha FROM `nom25_detalle` WHERE idnom=$idnom and activo=1 GROUP BY fecha ORDER BY `fecha` ASC";
        $query = $this->db->query($sql); 
        return $query;
    }
    function reconocimiento_paso2_nom25_anexo1($id_rec){
        $sql = "SELECT rec.*,pt.puesto_trabajo,pt.identificacion
                FROM reconocimiento_paso2_nom25 as rec 
                LEFT JOIN puestos_trabajo as pt on pt.id=rec.tarea_visual
                WHERE rec.id_reconocimiento='$id_rec' and rec.tipo_tabla=2 and rec.estatus=1";//WHERE rec.id_reconocimiento='$id_rec' and rec.tipo=2 and rec.estatus=1";
        $query = $this->db->query($sql); 
        return $query;
    }
    function mes($mes){
        switch ($mes) {
            case '1':
                $mesl='Enero';
                break;
            case '2':
                $mesl='Febrero';
                break;
            case '3':
                $mesl='Marzo';
                break;
            case '4':
                $mesl='Abril';
                break;
            case '5':
                $mesl='Mayo';
                break;
            case '6':
                $mesl='Junio';
                break;
            case '7':
                $mesl='Julio';
                break;
            case '8':
                $mesl='Agosto';
                break;
            case '9':
                $mesl='Septiembre';
                break;
            case '10':
                $mesl='Octubre';
                break;
            case '11':
                $mesl='Noviembre';
                break;
            case '12':
                $mesl='Diciembre';
                break;
            
            default:
                $mesl='';
                break;
        }
        return $mesl;
    }

}