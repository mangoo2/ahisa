<?php

class Ordenes_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    //funcion generica de obtencion de istado de un catalogo
    public function getCatalogo($catalogo) {
        $sql = "SELECT * FROM $catalogo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function getOrden($id){
        //$this->db->set("lc_time_names = 'es_ES'");
        //$this->db->select("SET o.fecha_creacion = 'es_ES'");
        $a=date('y');
        $this->db->select("o.*, MONTHNAME(o.fecha_creacion) as mes, MONTH(o.fecha_creacion) as mesn, YEAR(o.fecha_creacion) as anio, DAY(o.fecha_creacion) as dia, cs.hora_inicio, hora_fin, fecha_inicio, fecha_fin, e.nombre as vendedor, c.observaciones, IFNULL(pc.nombre,'') as contacto, IFNULL(pc.puesto,'') as puesto, IFNULL(pc.telefono,'') as telefono, cli.empresa as cliente, cli.rfc, cli.giro,
            concat(cli.calle,' ',cli.no_ext,' Colonia ',cli.colonia,' C.P. ',cli.cp,', ',cli.poblacion) as direcc,
            c.id_empresa,
            (CONCAT(clave,consecutivo,'-','".$a."',IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('empleados e', 'e.id = c.vendedor_id');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id=o.cotizacion_id');
        $this->db->join('clientes cli', 'cli.id=c.cliente_id');
        $this->db->join('personas_contacto pc', 'pc.id=c.contacto_id','left');
        $this->db->where("o.id",$id);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function getCliente($id){
        $this->db->select("*");
        $this->db->from('clientes c');
        $this->db->join('personas_contacto p', 'c.id = p.cliente_id');
        $this->db->where("c.id",$id);
        $this->db->where("c.estatus",1);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function updateOrden($id,$data){
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('ordenes');
    }
    
       
    public function getOrdenes($params){
        $id_usuario=$this->session->userdata("id_usuario");
        $sta=$params["status"];
        $columns = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            5 => 'e.usuario',
            6 => 'o.status',
            7 => 'o.id',
            8=> 'c.cliente_id',
            //9=>"GROUP_CONCAT(p.nombre SEPARATOR '<br>') as proveedor",
        );
        $columns2 = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'cl.alias',
            5 => 'e.nombre',
            6 => 'o.status',
            7 => 'o.id',
            8=> 'c.cliente_id',
            //9=> 'p.nombre'
        );
        
        $select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, e.usuario as nomVendedor, o.cotizacion_id as nombre, IFNULL(cs.fecha_inicio,'0') as fecha_inicio, cs.hora_inicio, cs.fecha_fin, cs.hora_fin, cs.comentarios, c.id_empresa, ";
        
        $select.="o.cotizacion_id as nombre, c.cliente_id, , max(o.id) as id_orden, o.id_tecnico, e2.correo as mail_tec, e2.nombre as tecnico, IFNULL(n.idnom,0) as idnom, IFNULL(r.id,0) as id_rec, (select count(cs.id) from cotizaciones_has_servicios cs where cs.cotizacion_id=c.id and status=1) as tot_chs, unico_tecnico, cs.id_tecnico as id_tec_gral, cs.id as id_chs, IFNULL(r2.id,0) as id_rec11, IFNULL(r3.id,0) as id_rec22, IFNULL(r4.id,0) as id_rec81, IFNULL(r5.id,0) as id_rec15";
      
        $this->db->select($select);
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        $this->db->join('empleados e2', 'e2.id = o.id_tecnico','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
        $this->db->join('reconocimiento r', 'r.id_orden=o.id','left');
        $this->db->join('reconocimiento_nom11 r2', 'r2.id_orden=o.id','left');
        $this->db->join('reconocimiento_nom22 r3', 'r3.id_orden=o.id','left');
        $this->db->join('reconocimiento_nom81 r4', 'r4.id_orden=o.id','left');
        $this->db->join('reconocimiento_nom15 r5', 'r5.id_orden=o.id','left');
        $this->db->join('nom n', 'n.idordenes = o.id and n.id_chs=cs.id','left');
        $this->db->join('nom n2', 'n2.idordenes = o.id and n2.id_chs=cs.id','left');
        $this->db->join('nom n3', 'n3.idordenes = o.id and n3.id_chs=cs.id','left');
        $this->db->join('nom n4', 'n4.idordenes = o.id and n4.id_chs=cs.id','left');
        
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y programacion
            $this->db->where('cs.status!=', 0);
            //$this->db->where('s.status!=', 0);
        }
        if($this->session->userdata("perfil")=='1' || $this->session->userdata("perfil")=='3'){ //admin y programacion
            $this->db->where('cs.status!=', 0);
        }
        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")=='2' && $this->session->userdata("perfil")!='5'){ //!=super admin e igual a vendedor
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        } //
        
        //cuando se logue un tecnico, vea el listado de sus servicios-ordenes asignados
        if($this->session->userdata("perfil")=='5'){ //técnico
            $this->db->where("o.id_tecnico",$this->session->userdata("id_usuario")); 
        }
        // ///////////////////////////////////////////////////////////////////////////

        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        }

        if($sta=="0"){
            $this->db->where('c.status!=',0);
        }

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns2 as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }

        $this->db->group_by('c.id');
        //$this->db->group_by('p.id');
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }

    public function getTotalOrdenes($params){
        $sta=$params["status"];
        $columns = array( 
            0 => 'o.id',
            1 => 'o.cotizacion_id',
            2 => 'o.cotizacion_id',
            3 => 'o.fecha_creacion',
            4 => 'alias',
            5 => 'e.usuario',
            6 => 'o.status',
            7 => 'o.id',
            8=> 'c.cliente_id',
            //9=> 'p.nombre'
        );
        $id_usuario=$this->session->userdata("id_usuario");

        $this->db->select("o.id");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados e', 'e.id = c.vendedor_id','left');
        $this->db->join('empleados e2', 'e2.id = o.id_tecnico','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');

        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y programacion
            $this->db->where('cs.status!=', 0);
        }
        if($this->session->userdata("perfil")=='1' || $this->session->userdata("perfil")=='3'){ //admin y programacion
            $this->db->where('cs.status!=', 0);
        }

        //cuando se logue un tecnico, vea el listado de sus servicios-ordenes asignados
        if($this->session->userdata("perfil")=='5'){ //técnico
            $this->db->where("o.id_tecnico",$this->session->userdata("id_usuario")); 
        }
        // ///////////////////////////////////////////////////////////////////////////

        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($params["fechai"]!="" && $params["fechaf"]!=""){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        }
        if($sta=="0"){
            $this->db->where('c.status!=',0);
        }

        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")=='2' && $this->session->userdata("perfil")!='5'){
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->group_by('c.id');
        $query=$this->db->get();
        //return $query->row()->total;
        //return $this->db->count_all_results();
        $get=$query->result();
        $total=0;
        foreach($get as $k){
            $total++;
        }
        return $total;
    }
    
    public function getServiciosCotizacion($id){
        $this->db->select("cotizaciones_has_servicios.*,
            s2.clave as clave2, s2.nombre as nombre2, s2.descripcion as descripcion2,prov2.nombre as empresa2");
        $this->db->join('servicios_ahisa s2', 's2.id = servicio_id');
        $this->db->join('proveedores as prov2', 's2.proveedor = prov2.id','left');

        $this->db->where("cotizacion_id",$id);
        if(!$this->session->userdata("administrador")){
            //$this->db->where("s.proveedor",$this->session->userdata("empresa"));
        }
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }
    
    public function getServicio($id){
        //$this->db->select("servicios.nombre as nombre, empleados.correo");
        $this->db->select("s.nombre as nombre, prov.correo, e.correo as mail_tec");

        $this->db->join('servicios_ahisa s', 's.id = servicio_id');
        $this->db->join('ordenes o', 'o.cotizacion_id = cotizaciones_has_servicios.cotizacion_id');
        $this->db->join('empleados e', 'e.id = o.id_tecnico');
        $this->db->join('proveedores prov', 'prov.id = s.proveedor');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row();
    }
    public function getDocsFam($id){
        $this->db->select("ordenes.familia_id, familias_has_documentos.documento");
        //$this->db->from("ordenes");
        $this->db->join('familias_has_documentos', 'familias_has_documentos.familia_id = ordenes.familia_id');
        $this->db->where("ordenes.id",$id);
        $query=$this->db->get("ordenes");
        return $query->row()->documento;
    }

    public function getDocsConfig(){
        $this->db->select("*");
        $this->db->from("documentos");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDocsServicio($id){
        $this->db->select("d.nombre");
        $this->db->from("cotizaciones_has_servicios chs");

        $this->db->join('servicios_documentos_ahisa sd', 'sd.id_servicio = chs.servicio_id');
        $this->db->join("documentos d","d.id=sd.id_documento");
        $this->db->where("chs.cotizacion_id",$id);
        $this->db->where("sd.estatus",1);
        $this->db->group_by("sd.id_documento");
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getMail($id){
        $this->db->select("email");
        $this->db->join('cotizaciones', 'cotizaciones.id = cotizacion_id');
        $this->db->join('personas_contacto', 'cotizaciones.cliente_id = personas_contacto.cliente_id');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        $this->db->where("orden",1);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->row()->email;
    }

    public function getMailContacto($id){
        $this->db->select("email");
        $this->db->join('cotizaciones', 'cotizaciones.id = cotizacion_id');
        $this->db->join('personas_contacto', 'cotizaciones.contacto_id = personas_contacto.id');
        $this->db->where("cotizaciones_has_servicios.cotizacion_id",$id);
        //$this->db->where("orden",1);
        $query=$this->db->get("cotizaciones_has_servicios");
        return $query->result();
    }

    public function getOrdenesPend(){           
        $this->db->select("o.id,cl.alias,o.fecha_creacion, c.id as id_cot, c.id_empresa, cs.id_empresa_serv");
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');

        $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
        $this->db->where("o.status",1);  //pendiente
        $this->db->where('c.status',2);   //aceptada

        $this->db->where('o.fecha_creacion >=','2022-01-01 00:00:00');   //aceptada
        $this->db->group_by('c.id');
        $this->db->order_by('o.id',"asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getOrdenesExcel($fecha1,$fecha2,$sta,$emp){
        $id_usuario=$this->session->userdata("id_usuario");   
        $sel_nombre = 'CONCAT(s2.nombre,"<br>",s2.descripcion) as nombre2, ';
        /*if(!$this->session->userdata("administrador")){
            $columns[2]='CONCAT(s.nombre,"<br>",s.descripcion) as nombre, CONCAT(s2.nombre,"<br>",s2.descripcion) as nombre2, CONCAT(s3.nombre,"<br>",s3.descripcion) as nombre3';
        }*/
        $select = "o.id,cl.alias,o.fecha_creacion,o.cotizacion_id,o.status, empleados.usuario as nomVendedor, o.cotizacion_id as nombre, IFNULL(cs.fecha_inicio,'0') as fecha_inicio, cs.hora_inicio, cs.fecha_fin, cs.hora_fin, cs.comentarios, c.id as id_cot, ";
        
        if(!$this->session->userdata("administrador") && $this->session->userdata("perfil")!='3'){
            $select.="cs.id as servicio_id,IFNULL(cs.fecha_inicio,'0') as fecha_inicio,fecha_fin,hora_inicio,hora_fin,cs.comentarios, c.cliente_id, , max(o.id) as id_orden, c.id as id_cot, 
            ".$sel_nombre.", c.id_empresa, ";
        }
        else{
            $select.="o.cotizacion_id as nombre, c.cliente_id, , max(o.id) as id_orden, ".$sel_nombre.", c.id_empresa,";
        }
             
        $this->db->select($select);
        $this->db->from('ordenes o');
        $this->db->join('cotizaciones c', 'c.id = o.cotizacion_id');
        $this->db->join('clientes cl', 'cl.id = c.cliente_id','left');
        $this->db->join('empleados', 'empleados.id = c.vendedor_id','left');
        $this->db->join('cotizaciones_has_servicios cs', 'cs.cotizacion_id = c.id');
        $this->db->join('servicios_ahisa s2', 's2.id=cs.servicio_id');

        //if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3'){ //admin y tecnico
            //$this->db->join('servicios s', 's.id = cs.servicio_id'); ahora se dividieron los servicios por tabla de empresa
            $this->db->where('cs.status!=', 0);
        //}
        /*if($emp!="0"){ //ya no va porque se dividieron las tablas se servicios
            $this->db->where('s.proveedor',$emp);
        }*/

        if($this->session->userdata("perfil")!='1' && $this->session->userdata("perfil")!='3' && $this->session->userdata("perfil")!='5'){
            //$this->db->where("o.id_usuario",$this->session->userdata("id_usuario")); 
            $this->db->where("c.vendedor_id",$this->session->userdata("id_usuario")); 
        }

        //cuando se logue un tecnico, vea el listado de sus servicios-ordenes asignados
        if($this->session->userdata("perfil")=='5'){ //técnico
            $this->db->where("o.id_tecnico",$this->session->userdata("id_usuario")); 
        }
        // ///////////////////////////////////////////////////////////////////////////
        
        if($sta!=0 && $sta!=4){
            $this->db->where("o.status",$sta); //por estatus modificada, aceptada, terminada
            if($sta==1){
                $this->db->where("cs.fecha_inicio IS NULL");  //pendiente
            }
        }else if($sta==0){ //todas
            $this->db->where("o.status!=","0");
        }
        if($sta==4){ //en proceso
           $this->db->where("o.status","1"); 
           $this->db->where("cs.fecha_inicio!=",""); 
        }
        if($fecha1!="0" && $fecha2!="0"){
            $this->db->where('o.fecha_creacion BETWEEN '.'"'.$fecha1.' 00:00:00"'.' AND '.'"'.$fecha2.' 23:59:59"');
        }
        $this->db->where('c.status!=',0);
        $this->db->group_by('c.id');
        $this->db->order_by('o.id',"desc");

        $query=$this->db->get();
        return $query->result();
    }

    public function getServiciosOrden($id){
        $this->db->select("chs.id, chs.id_tecnico, s.nombre, s.norma");
        $this->db->from('cotizaciones_has_servicios chs');
        $this->db->join('servicios_ahisa s', 's.id = chs.servicio_id');
        $this->db->where("chs.cotizacion_id",$id);
        $this->db->where("chs.status",1);
        $query=$this->db->get();
        return $query->result();
    }

    public function editaProgramacion($data,$id){
        $this->db->set($data);
        $this->db->where('cotizacion_id', $id);
        return $this->db->update('cotizaciones_has_servicios');
    }

}