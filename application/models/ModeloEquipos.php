<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloEquipos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getequipos($params){
        $columns = array( 
            0=>'id',
            1=>'luxometro',
            2=>'equipo',
            3=>'reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('equipo');
       
        $this->db->where(array('activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getequipos_total($params){
        $columns = array( 
            0=>'id',
            1=>'luxometro',
            2=>'equipo',
            3=>'reg'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('equipo');


        $this->db->where(array('activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }

    function getDetalleMedidor($id){
        $this->db->select('ed.*');
        $this->db->from("equipo_medidor_detalle ed");
        $this->db->where("id_equipo",$id);
        $this->db->order_by("unidad","ASC");
        $this->db->order_by("line","ASC");

        $query=$this->db->get(); 
        return $query->result();
    }

}