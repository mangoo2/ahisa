<?php

class ModeloReportes extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    //funcion generica de obtencion de istado de un catalogo

    public function GetVendedores(){
        $sql="SELECT * FROM empleados WHERE perfil = 1 OR perfil=2";
        $resutl= $this->db->query($sql);
        return $resutl;
    }

    public function GetCientes(){
        $sql="SELECT * FROM clientes";
        $resutl= $this->db->query($sql);
        return $resutl;
    }

    function GetData($params){
        //$params['vendedor']=null;
        /*$fechas="";
        $cliente="";
        $vendedor="";

        if ($data!="") {
            if ($data['fecha1']!="" && $data['fecha2']!="") {
                $fechas=" and co.fecha_creacion between '".$data['fecha1']."' and '".$data['fecha2']."' ";
            }else{
                $fechas="";
            }
            if ($data['cliente']!=""){
                $cliente=" and cli.id=".$data['cliente'];
            }else{
                $cliente="";
            }
            if ($data['vendedor']!="") {
                $vendedor=" and em.id=".$data['vendedor'];
            }else{
                $vendedor="";
            }
            
        }else{
            $fechas="";
            $cliente="";
            $vendedor="";
        }*/


        /*$sql="SELECT co.id, co.total as monto, co.forma_pago, cli.alias ,co.status, em.nombre, concat(em.clave,co.id,co.fecha_creacion) as clave FROM cotizaciones as co inner join empleados as em inner join clientes as cli where co.cliente_id = cli.id and co.vendedor_id=em.id ".$vendedor.$cliente.$fechas." and co.status!=0 ";*/
        $columns = array( 
            0 => "co.id",
            1 => 'co.total', 
            2 => 'co.forma_pago',
            3 => 'cli.alias',
            4 => 'co.status',
            5 => 'em.nombre',
            6 => 'em.clave',
            7 => 'co.fecha_creacion',
            8 => 'co.status',
            9 => "co.id"
            //10 => 'ord.cotizacion_id'
            /*11 => 'co.forma_pago',
            12 => 'cli.alias',
            13 => 'co.status',
            14 => 'em.nombre',
            15 => 'em.clave',
            16 => 'co.fecha_creacion',
            17 => 'co.status'*/
        );

        $select="ord.*, ord.fecha_creacion as fecha_creacion_ord, co.id, co.total as monto, co.forma_pago, cli.alias ,co.status, em.nombre, co.fecha_creacion as fecha_creacion_cot";
        $select.=", concat(em.clave,co.id,co.fecha_creacion) as clave";
        //$select.=", FORMAT(total,2) as importe, (CONCAT(clave,consecutivo,'-',$a,IF(no_modificacion=0, '', CONCAT('-',no_modificacion)))) as folio";

        //$select.=",ord.*";
        $this->db->select($select);
        if($params['tiposta']=="2"){
            $this->db->from('ordenes ord');
            $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');
        }
        else{
           $this->db->from('cotizaciones co');
            $this->db->join('ordenes ord', 'ord.cotizacion_id = co.id','left'); 
        }
        
        $this->db->join('clientes cli', 'cli.id = co.cliente_id','left');
        $this->db->join('empleados em', 'em.id = co.vendedor_id','left');

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            //$search.=$sta;
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end(); 
        }
        //$this->db->where("co.status!=",0);
        if ($params['vendedor']!="") {
            $this->db->where("co.vendedor_id", $params['vendedor']);
        }
        if ($params['cliente']!=""){
            $this->db->where("vn.id", $params['cliente']);
        }
        if ($params['fecha1']!="" && $params['fecha2']!="") {
            if($params['tiposta']=="2"){
                $this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                $this->db->where("ord.status",1);
            }
            else{
                $this->db->where("co.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
            }   
        }
        if ($params['tiposta']!="2"){
            $this->db->where("co.status", $params['tiposta']);
        }
        else{
            $this->db->where("ord.status!=",0);
        }
        
        //$this->db->group_by('co.id');

        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
        /*$res=$this->db->query($sql);
        return $res;*/
    }

    public function totalCotizaciones($params,$da){
        
        if($params['tiposta']=="2"){ //aceptada
            $this->db->select('COUNT(ord.id) as total, ord.fecha_creacion as fecha_creacion_ord');
            $this->db->from('ordenes ord');
            $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');
        }
        else{ //diferentes de aceptadas
            $this->db->select('COUNT(co.id) as total, co.fecha_creacion as fecha_creacion_cot');
            $this->db->from('cotizaciones co');
            $this->db->join('ordenes ord', 'ord.cotizacion_id = co.id','left'); 
        }

        /*$this->db->from('ordenes ord');
        $this->db->join('cotizaciones co', 'co.id = ord.cotizacion_id','left');*/
        $this->db->join('clientes cl', 'cl.id = co.cliente_id','left');
        $this->db->join('empleados e', 'e.id = co.vendedor_id','left');
        //$this->db->where('c.status!=','0');
        if($this->session->userdata("id_usuario")>1){
            $this->db->where("vendedor_id",$this->session->userdata("id_usuario"));
        }
        //if($da==1){
            if ($params['vendedor']!="") {
                $this->db->where("co.vendedor_id", $params['vendedor']);
            }
            if ($params['cliente']!=""){
                $this->db->where("cl.id", $params['cliente']);
            }
            if ($params['fecha1']!="" && $params['fecha2']!="") {
                if($params['tiposta']=="2"){
                    $this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                    $this->db->where("ord.status",1);
                }
                else{
                    $this->db->where("co.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
                }
                //$this->db->where("ord.fecha_creacion between '{$params['fecha1']} 00:00:00' AND '{$params['fecha2']} 23:59:59' ");
            }
            if ($params['tiposta']!="2"){ //no aceptada
                $this->db->where("co.status", $params['tiposta']);
            }
            else{ //aceptada
                $this->db->where("ord.status!=",0);
            }
        
        //}
        $query=$this->db->get();
        return $query->row()->total;
    }

}

?>