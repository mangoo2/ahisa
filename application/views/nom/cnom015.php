<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        font-size: 14px;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    .table td{
        vertical-align: middle;
    }
    @media (max-width:500px){
        .titulos{
            font-size: 11px;
        }
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }

</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_nom">
                            <input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
                            <input type="hidden" name="idordenes" value="<?php echo $id_orden; ?>">
                            <input type="hidden" name="id_chs" id="id_chs" value="<?php echo $id_chs; ?>">
                            <div class="row" id="cont_ocu1">
                                <div class="col-md-4">
                                    <label>No. de Informe</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control num_informe_princi" name="num_informe" id="num_informe" value="<?php echo $num_informe; ?>">
                                </div>
                            </div>
                            <div class="row" id="cont_ocu1">
                                <div class="col-md-4">
                                    <label>Razón Social</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control razon_social_princi" name="razon_social" id="razon_social" value="<?php echo $razon_social; ?>">
                                </div>
                            </div>
                        </form>
                             
                        <div id="cont_ptos_gral">
                            <div id="cont_ptos">
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()" ><i class="fa fa-plus"></i> Agregar punto</button>
                                <?php if(isset($pto) && $pto->num_rows()>0) {
                                    foreach ($pto->result() as $p) { ?>
                                        <div id="pto_princi" class="pto_princi pto_princifor_<?php echo $p->id; ?>">
                                        <hr>
                                        <input type="hidden" name="id_pto" id="id_pto" value="<?php echo $p->id; ?>">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                                <button type="button" class="btn btn-danger mr-1 mb-1" onclick="deletepunto(<?php echo $p->id; ?>)" ><i class="fa fa-trash-o"></i> Eliminar punto</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $p->fecha; ?>">
                                            </div>
                                        </div>    

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro liquido</label>
                                                    <select name="id_termo" id="id_termo" class="form-control">
                                                        <?php foreach($termo->result() as $eq){ ?>
                                                            <option <?php if($p->id_termo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro digital</label>
                                                    <select name="id_termo_digi" id="id_termo_digi" class="form-control">
                                                        <?php foreach($termo_dig->result() as $eq){ ?>
                                                            <option <?php if($p->id_termo_digi==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Anemómetro</label>
                                                    <select name="id_anemo" id="id_anemo" class="form-control">
                                                        <?php foreach($anemo->result() as $eq){ ?>
                                                            <option <?php if($p->id_anemo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td class="tdinput titulos" width="10%">
                                                    <label>ÁREA</label>
                                                </td>   
                                                <td class="tdinput" width="60%">
                                                    <input type="text" name="area" id="area" class="form-control" value="<?php echo $p->area; ?>">
                                                </td>   
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>No. PUNTO</label>  
                                                </td>  
                                                <td class="tdinput" width="20%">
                                                    <input type="text" name="num_punto" id="num_punto" class="form-control" value="<?php echo $p->num_punto; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>UBICACIÓN</label>
                                                </td>
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="ubicacion" id="ubicacion" class="form-control" value="<?php echo $p->ubicacion; ?>">
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</label>
                                                </td>  
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="fte_generadora" id="fte_generadora" class="form-control" value="<?php echo $p->fte_generadora; ?>">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES TÉCNICOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="controles_tec" id="controles_tec" class="form-control" value="<?php echo $p->controles_tec; ?>">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES ADMINISTRATIVOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="controles_adm" id="controles_adm" class="form-control" value="<?php echo $p->controles_adm; ?>">
                                                </td> 
                                            </tr>
                                        </table>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>TIPO DE EVALUACIÓN</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>POE
                                                        <input <?php if($p->tipo_evalua==1) echo "checked"; ?> type="radio" id="tipo_evalua" name="tipo_evalua<?php echo $p->id; ?>" class="form-control form-control-sm chk_form"></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>GEH
                                                        <input <?php if($p->tipo_evalua==2) echo "checked"; ?> type="radio" id="tipo_evalua2" name="tipo_evalua<?php echo $p->id; ?>" class="form-control form-control-sm chk_form"></label>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-8">
                                                <label>NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</label>
                                                <input type="text" name="nom_trabaja" id="nom_trabaja" class="form-control" value="<?php echo $p->nom_trabaja; ?>">
                                            </div>   
                                        </div>
                                           
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Casco:
                                                    <input <?php if($p->casco==1) echo "checked"; ?> type="checkbox" name="casco" id="casco" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Tapones acústicos:
                                                    <input <?php if($p->tapones==1) echo "checked"; ?> type="checkbox" name="tapones" id="tapones" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Botas de seguridad:
                                                    <input <?php if($p->botas==1) echo "checked"; ?> type="checkbox" name="botas" id="botas" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Lentes:
                                                    <input <?php if($p->lentes==1) echo "checked"; ?> type="checkbox" name="lentes" id="lentes" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Guantes:
                                                    <input <?php if($p->guantes==1) echo "checked"; ?> type="checkbox" name="guantes" id="guantes" class="form-control chk_form"></label>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Peto:
                                                    <input <?php if($p->peto==1) echo "checked"; ?> type="checkbox" name="peto" id="peto" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Respirador:
                                                    <input <?php if($p->respirador==1) echo "checked"; ?> type="checkbox" name="respirador" id="respirador" class="form-control chk_form"></label>
                                                </td>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Otro:
                                                    <input type="text" name="otro" id="otro" class="form-control" value="<?php echo $p->otro; ?>"></label>
                                                </td> 
                                                <td colspan="2" class="tdinput titulos" width="40%">

                                                </td> 
                                            </tr>
                                        </table>
                                        
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="25%" class="tdinput titulos"><label>Puesto de trabajo</label> </td>
                                                <td width="25%" class="tdinput"><input type="text" name="puesto" id="puesto" class="form-control" value="<?php echo $p->puesto; ?>"></td>
                                                <td width="25%" class="tdinput titulos"><label>Tiempo de exposición por ciclo en una h (min)</label></td>
                                                <td width="25%" class="tdinput"><input type="text" name="tiempo_expo" id="tiempo_expo" class="form-control" value="<?php echo $p->tiempo_expo; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td width="25%" class="tdinput titulos"><label>Número de ciclos por h</label></td>
                                                <td width="25%" class="tdinput"><input type="text" id="num_ciclos" name="num_ciclos" class="form-control" value="<?php echo $p->num_ciclos; ?>"></td>
                                                <td width="25%" class="tdinput titulos"><label>Ciclo evaluado</label></td>
                                                <td width="25%" class="tdinput"><input type="text" id="cliclo" name="cliclo" class="form-control" value="<?php echo $p->cliclo; ?>"></td>
                                            </tr>
                                        </table> 
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="40%" class="tdinput titulos"><label>Descripción de actividades del personal</label></td>
                                                <td width="60%" class="tdinput"><input type="text" id="desc_activ" name="desc_activ" class="form-control" value="<?php echo $p->desc_activ; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="tdinput titulos"><label>Observaciones</label></td>
                                                <td width="60%" class="tdinput"><input type="text" id="observaciones" name="observaciones" class="form-control" value="<?php echo $p->observaciones; ?>"></td>
                                            </tr>
                                        </table> 
                                        <?php $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15',array('id'=>$p->id,"activo"=>1)); { 
                                            if($med->num_rows()==0){ ?>
                                                <table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                                    <tr class="datos">
                                                        <td width="10%" class="tdinput titulos">
                                                            <input type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <label>Ciclo No.</label>
                                                        </td>
                                                        <td width="10%" class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value=""></td>
                                                        <td width="30%" class="tdinput titulos"><label>Temperatura auxiliar inicial (°C)</label></td>
                                                        <td width="10%" class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control"  value=""></td>
                                                        <td width="30%" class="tdinput titulos"><label>Tempertura auxiliar final (°C)</label></td>
                                                        <td width="10%" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                        <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                        <td colspan="2" class="tdinput titulos"><label>Velocidad del viento (km/h)</label></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Inicial</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_ini" name="altura_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_ini" name="velocidad_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Mitad</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_media" name="altura_media" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_media" name="hora_media" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_media" name="tbs_media" class="form-control" value=""></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_media" name="velocidad_media" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Final</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_fin" name="altura_fin" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_fin" name="hora_fin" class="form-control" value=""></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_fin" name="tbs_fin" class="form-control" value=""></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_fin" name="velocidad_fin" class="form-control" value=""></td>
                                                    </tr>
                                                </table> 
                                               <center><span style="font-size: 9px;">Tbs: Temperatura bulbo seco</span></center>
                                            <?php }
                                            foreach($med->result() as $m) {?>
                                                <table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                                    <tr>
                                                        <td width="10%" class="tdinput titulos">
                                                            <input type="hidden" name="id_medicion" id="id_medicion" value="<?php echo $m->id; ?>">
                                                            <label>Ciclo No.</label>
                                                        </td>
                                                        <td width="10%" class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value="<?php echo $m->ciclo_med; ?>"></td>
                                                        <td width="30%" class="tdinput titulos"><label>Temperatura auxiliar inicial (°C)</label></td>
                                                        <td width="10%" class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control" value="<?php echo $m->temp_ini; ?>"></td>
                                                        <td width="30%" class="tdinput titulos"><label>Tempertura auxiliar final (°C)</label></td>
                                                        <td width="10%" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value="<?php echo $m->temp_fin; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                        <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                        <td colspan="2" class="tdinput titulos"><label>Velocidad del viento (km/h)</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Inicial</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_ini" name="altura_ini" class="form-control" value="<?php echo $m->altura_ini; ?>"></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="<?php echo $m->hora_ini; ?>"></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_ini" name="tbs_ini" class="form-control" value="<?php echo $m->tbs_ini; ?>"></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_ini" name="velocidad_ini" class="form-control" value="<?php echo $m->velocidad_ini; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Mitad</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_media" name="altura_media" class="form-control" value="<?php echo $m->altura_media; ?>"></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_media" name="hora_media" class="form-control" value="<?php echo $m->hora_media; ?>"></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_media" name="tbs_media" class="form-control" value="<?php echo $m->tbs_media; ?>"></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_media" name="velocidad_media" class="form-control" value="<?php echo $m->velocidad_media; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Final</label></td>
                                                        <td class="tdinput"><label><input type="text" id="altura_fin" name="altura_fin" class="form-control" value="<?php echo $m->altura_fin; ?>"></td>
                                                        <td class="tdinput"><label><input type="time" id="hora_fin" name="hora_fin" class="form-control" value="<?php echo $m->hora_fin; ?>"></td>
                                                        <td class="tdinput"><label><input type="text" id="tbs_fin" name="tbs_fin" class="form-control" value="<?php echo $m->tbs_fin; ?>"></td>
                                                        <td colspan="2" class="tdinput"><label><input type="text" id="velocidad_fin" name="velocidad_fin" class="form-control" value="<?php echo $m->velocidad_fin; ?>"></td>
                                                    </tr>
                                                </table> 
                                               <center><span style="font-size: 9px;">Tbs: Temperatura bulbo seco</span></center>
                                    <?php   } 
                                        }
                                        echo "</div>";
                                    } 
                                    }else {?>
                                        <div id="pto_princi" class="pto_princi">
                                        <hr>
                                        <input type="hidden" name="id_pto" id="id_pto" value="0">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()" >Agregar punto</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="">
                                            </div>
                                        </div>    

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro liquido</label>
                                                    <select name="id_termo" id="id_termo" class="form-control">
                                                        <?php foreach($termo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro digital</label>
                                                    <select name="id_termo_digi" id="id_termo_digi" class="form-control">
                                                        <?php foreach($termo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Anemómetro</label>
                                                    <select name="id_anemo" id="id_anemo" class="form-control">
                                                        <?php foreach($anemo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td class="tdinput titulos" width="10%">
                                                    <label>ÁREA</label>
                                                </td>   
                                                <td class="tdinput" width="60%">
                                                    <input type="text" name="area" id="area" class="form-control" value="">
                                                </td>   
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>No. PUNTO</label>  
                                                </td>  
                                                <td class="tdinput" width="20%">
                                                    <input type="text" name="num_punto" id="num_punto" class="form-control" value="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>UBICACIÓN</label>
                                                </td>
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="ubicacion" id="ubicacion" class="form-control" value="">
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</label>
                                                </td>  
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="fte_generadora" id="fte_generadora" class="form-control" value="">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES TÉCNICOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="controles_tec" id="controles_tec" class="form-control" value="">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES ADMINISTRATIVOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="3" width="80%">
                                                    <input type="text" name="controles_adm" id="controles_adm" class="form-control" value="">
                                                </td> 
                                            </tr>
                                        </table>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>TIPO DE EVALUACIÓN</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>POE
                                                        <input type="radio" id="tipo_evalua" name="tipo_evalua" class="form-control form-control-sm chk_form"></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>GEH
                                                        <input type="radio" id="tipo_evalua2" name="tipo_evalua" class="form-control form-control-sm chk_form"></label>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-8">
                                                <label>NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</label>
                                                <input type="text" name="nom_trabaja" id="nom_trabaja" class="form-control" value="">
                                            </div>   
                                        </div>
                                           
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Casco:
                                                    <input type="checkbox" name="casco" id="casco" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Tapones acústicos:
                                                    <input type="checkbox" name="tapones" id="tapones" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Botas de seguridad:
                                                    <input type="checkbox" name="botas" id="botas" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Lentes:
                                                    <input type="checkbox" name="lentes" id="lentes" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Guantes:
                                                    <input type="checkbox" name="guantes" id="guantes" class="form-control chk_form"></label>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Peto:
                                                    <input type="checkbox" name="peto" id="peto" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Respirador:
                                                    <input type="checkbox" name="respirador" id="respirador" class="form-control chk_form"></label>
                                                </td>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Otro:
                                                    <input type="text" name="otro" id="otro" class="form-control"></label>
                                                </td> 
                                                <td colspan="2" class="tdinput titulos" width="40%">

                                                </td> 
                                            </tr>
                                        </table>
                                        
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="25%" class="tdinput titulos"><label>Puesto de trabajo</label> </td>
                                                <td width="25%" class="tdinput"><input type="text" name="puesto" id="puesto" class="form-control" value=""></td>
                                                <td width="25%" class="tdinput titulos"><label>Tiempo de exposición por ciclo en una h (min)</label></td>
                                                <td width="25%" class="tdinput"><input type="number" name="tiempo_expo" id="tiempo_expo" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td width="25%" class="tdinput titulos"><label>Número de ciclos por h</label></td>
                                                <td width="25%" class="tdinput"><input type="number" id="num_ciclos" name="num_ciclos" class="form-control" value=""></td>
                                                <td width="25%" class="tdinput titulos"><label>Ciclo evaluado</label></td>
                                                <td width="25%" class="tdinput"><input type="number" id="cliclo" name="cliclo" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="40%" class="tdinput titulos"><label>Descripción de actividades del personal</label></td>
                                                <td width="60%" class="tdinput"><input type="text" id="desc_activ" name="desc_activ" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="tdinput titulos"><label>Observaciones</label></td>
                                                <td width="60%" class="tdinput"><input type="text" id="observaciones" name="observaciones" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                        <table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                            <tr>
                                                <td width="10%" class="tdinput titulos">
                                                    <input type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <label>Ciclo No.</label>
                                                </td>
                                                <td width="10%" class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value=""></td>
                                                <td width="30%" class="tdinput titulos"><label>Temperatura auxiliar inicial (°C)</label></td>
                                                <td width="10%" class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control" value=""></td>
                                                <td width="30%" class="tdinput titulos"><label>Tempertura auxiliar final (°C)</label></td>
                                                <td width="10%" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                <td colspan="2" class="tdinput titulos"><label>Velocidad del viento (km/h)</label></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>Inicial</label></td>
                                                <td class="tdinput"><label><input type="number" id="altura_ini" name="altura_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                <td colspan="2" class="tdinput"><label><input type="number" id="velocidad_ini" name="velocidad_ini" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>Mitad</label></td>
                                                <td class="tdinput"><label><input type="number" id="altura_media" name="altura_media" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="time" id="hora_media" name="hora_media" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="number" id="tbs_media" name="tbs_media" class="form-control" value=""></td>
                                                <td colspan="2" class="tdinput"><label><input type="number" id="velocidad_media" name="velocidad_media" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>Final</label></td>
                                                <td class="tdinput"><label><input type="number" id="altura_fin" name="altura_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="time" id="hora_fin" name="hora_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><label><input type="number" id="tbs_fin" name="tbs_fin" class="form-control" value=""></td>
                                                <td colspan="2" class="tdinput"><label><input type="number" id="velocidad_fin" name="velocidad_fin" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                       <center><span style="font-size: 9px;">Tbs: Temperatura bulbo seco</span></center>
                                        </div>
                                    <?php } ?>
                                
                            </div>
                        </div>                 
                    

                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="saveDetalles()" ><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
