<link href="<?php echo base_url();?>public/boostrap/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="<?php echo base_url();?>public/boostrap/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 

<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    table th,table td{
        text-align: center;
    }
    .tablev{
        width: auto;
        margin-left: auto;
        margin-right: auto;
    }
    .tablev th{
        background-color: rgb(217, 217, 217);
    }
    .tablev th,.tablev td{
        border: 1px solid rgb(128, 128, 128);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body" style="width: 80%;">
                    <div class="card-block">
                        <table class="table table-bordered tablev">
                            <tr>
                                <th>No. de Informe</th>
                                <td><?php echo $num_informe;?></td>
                            </tr>
                            <tr>
                                <th>Razon Social</th>
                                <td><?php echo $razon_social;?></td>
                            </tr>
                            <tr>
                                <th>Fecha</th>
                                <td><?php echo $fecha;?></td>
                            </tr>
                        </table>                       

                        <?php 
                            $rowpuntostr=1;
                            $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
                            foreach ($rowpuntos->result() as $item) { 
                                $idpunto=$item->punto;
                                
                                $val_nrr=0;
                                $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto));
                                $itemnomd=$rownomd->row();
                                $val_nrr=$itemnomd->val_nrr;
                                ?>
                                <div id="grafo1_<?php echo $item->idnomd?>">
                                    <table class="table table-bordered tablev">
                                        <tr>
                                            <th>No. Punto</th>
                                            <td><?php echo $rowpuntostr;?></td>
                                            <th colspan="3">Tiempo  efectivo de exposición al ruido (h)</th>
                                            <td colspan="2"><?php echo $itemnomd->tiempo_ruido?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <th>Ti (minutos)</th>
                                            <td><?php echo $itemnomd->tiempo_ruido*60?></td>
                                            <th colspan="2">Te para 8 h en minutos </th>
                                            <td><?php if($itemnomd->te>0){ echo $itemnomd->te; }else{ echo '480';} ?></td>
                                        </tr>
                                        <tr>
                                            <th>Area </th>
                                            <td colspan="2"><?php echo $itemnomd->area?></td>
                                            <th>Identificacion</th>
                                            <td colspan="3"><?php echo $itemnomd->identificacion;?></td>
                                        </tr>
                                    </table>
                                    
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <table class="table table-bordered tablev">
                                                <tr>
                                                    <th colspan="2">Ruido</th>
                                                </tr>
                                                <tr>
                                                    <th>Estable</th>
                                                    <td><?php if($itemnomd->tipo_ruido==0){echo 'X';}?></td>
                                                </tr>
                                                <tr>
                                                    <th>Inestable</th>
                                                    <td><?php if($itemnomd->tipo_ruido==1){echo 'X';}?></td>
                                                </tr>
                                                <tr>
                                                    <th>Impulsivo</th>
                                                    <td><?php if($itemnomd->tipo_ruido==2){echo 'X';}?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-8">
                                            <table class="table table-bordered tablecaptura tablev" id="table_lectura" align="center">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Lectura
                                                </th>
                                                <th colspan="2">Periodo 1</th>
                                                <th colspan="2">Periodo 2</th>
                                                <th colspan="2">Periodo 3</th>
                                            </tr>
                                            <tr>
                                                <th >NSCE dB(A)</th>
                                                <th ><img src="<?php echo base_url();?>public/img/formula3.png"></th>
                                                <th >NSCE dB(A)</th>
                                                <th ><img src="<?php echo base_url();?>public/img/formula3.png"></th>
                                                <th >NSCE dB(A)</th>
                                                <th ><img src="<?php echo base_url();?>public/img/formula3.png"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); 
                                                    $rowlecturas_tr=1;
                                                    $sumaperiodos_123=0;
                                                    foreach ($rowlecturas->result() as $iteml) {
                                                        ?>
                                                             <tr  >
                                                                <th class="tdinput"><?php echo $rowlecturas_tr;?></th>
                                                                <td class="tdinput"><?php if($iteml->periodo1>0){echo $iteml->periodo1;}else{echo '---';};?></td>
                                                                <td><?php if($iteml->periodo1>0){
                                                                            $periodo1pow=pow(10, ($iteml->periodo1/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                                            echo $periodo1pow;
                                                                        }else{
                                                                            $periodo1pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                                            echo '---';
                                                                        };?></td>
                                                                <td class="tdinput"><?php if($iteml->periodo2>0){echo $iteml->periodo2;}else{echo '--';};?></td>
                                                                <td><?php if($iteml->periodo2>0){
                                                                            $periodo2pow=pow(10, ($iteml->periodo2/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                                            echo $periodo2pow;
                                                                        }else{
                                                                            $periodo2pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                                            echo '---';
                                                                        };?></td>
                                                                <td class="tdinput"><?php if($iteml->periodo3>0){echo $iteml->periodo3;}else{echo '--';};?></td>
                                                                <td><?php if($iteml->periodo3>0){
                                                                            $periodo3pow=pow(10, ($iteml->periodo3/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                                            echo $periodo3pow;
                                                                        }else{
                                                                            $periodo3pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                                            echo '---';
                                                                        };?></td>
                                                                
                                                              </tr>
                                                        <?php
                                                        $rowlecturas_tr++;
                                                    }
                                            ?>
                                                <tr>
                                                    <th>Suma</th>
                                                    <td colspan="6"><?php echo $sumaperiodos_123;?></td>
                                                </tr>
                                        </tbody> 
                                    </table>
                                        </div>
                                    </div>
                                    
                                    <table>
                                        <tr>
                                            <th colspan="3">NIVEL SONORO CONTINUO EQUIVALENTE</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><img src="<?php echo base_url();?>public/img/formula4.png"></td>
                                            <td><?php
                                                    //=SI(B16="X",10*LOG((1/20)*(F25)),SI(B17="X",10*LOG((1/30)*(F25)),10*LOG((1/45)*(F25))))
                                                    $nivel_sonoro_continuo=0;
                                                    if($itemnomd->tipo_ruido==0){
                                                        $num=(1/20)*$sumaperiodos_123;
                                                        echo '<!--'.$num.'-->';
                                                        echo '<!--'.log($num).'-->';
                                                        $nivel_sonoro_continuo=10*log10((1/20)*$sumaperiodos_123);
                                                    }elseif($itemnomd->tipo_ruido==1){
                                                        $nivel_sonoro_continuo=10*log10((1/30)*$sumaperiodos_123);
                                                    }else{
                                                        $nivel_sonoro_continuo=10*log10((1/45)*$sumaperiodos_123);
                                                    }
                                                ?>NSCEA,Ti dB(A)= <b><?php echo round($nivel_sonoro_continuo, 2);?></b></td>
                                        </tr>
                                        <tr>
                                            <th colspan="3">NIVEL DE EXPOSICIÓN AL RUIDO</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><img src="<?php echo base_url();?>public/img/formula5.png"></td>
                                            <td><?php
                                                    //=10*LOG((G5)*10^(I29/10))-10*LOG(L5)
                                                    if($nivel_sonoro_continuo>0 && $itemnomd->tiempo_ruido>0){
                                                        $nivel_expocion_ruido=10*log10(($itemnomd->tiempo_ruido*60)*pow(10, ($nivel_sonoro_continuo/10)) )-10*log10($itemnomd->te);
                                                    }else{
                                                        $nivel_expocion_ruido=0;
                                                    }
                                                ?>NER dB(A)=  <b><?php echo round($nivel_expocion_ruido,2);?></b></td>
                                        </tr>
                                        <tr>
                                            <td>TIEMPO MÁXIMO PERMISIBLE DE EXPOSICIÓN</td>
                                            <td><img src="<?php echo base_url();?>public/img/formula6.png"></td>
                                            <td><?php 
                                                    //=SI(I33<90,"No Aplica",8/(2^((I33-90)/3)))
                                                    if($nivel_expocion_ruido<90){
                                                        $t_max_permisible_exp=0;
                                                        $t_max_permisible_exp_l='No Aplica';
                                                    }else{
                                                        $t_max_permisible_exp=8/(pow(2,(($nivel_expocion_ruido-90)/3)));
                                                        $t_max_permisible_exp_l=round($t_max_permisible_exp,2);
                                                    }
                                                ?>TMPE dB(A)= <b><?php echo $t_max_permisible_exp_l;?></b></td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <canvas id="myChartgtg<?php echo $item->idnomd?>" width="500" height="300"></canvas>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function($) {
                                           graficageneral<?php echo $item->idnomd?>(); 
                                        });
                                        function graficageneral<?php echo $item->idnomd?>(){
                                            var ctx = document.getElementById('myChartgtg<?php echo $item->idnomd?>').getContext('2d');
                                            var myChart = new Chart(ctx, {
                                              type: 'bar',
                                              data: {
                                                labels: [<?php $rowtr=1;
                                                                foreach ($rowlecturas->result() as $iteml) { ?>
                                                                    <?php echo $rowtr.','; ?> 
                                                        <?php $rowtr++;
                                                                } ?>],
                                                datasets: [
                                                            

                                                        { 
                                                            data: [
                                                                    <?php foreach ($rowlecturas->result() as $iteml) {  ?>
                                                                        <?php if($iteml->periodo1>0){echo $iteml->periodo1;}else{echo '0';} ?>,
                                                                    <?php } ?>
                                                                  ],
                                                            label: "Periodo 1",
                                                            backgroundColor: "#2196f3",
                                                          }, 
                                                          { 
                                                            data: [
                                                                    <?php foreach ($rowlecturas->result() as $iteml) {  ?>
                                                                        <?php if($iteml->periodo2>0){echo $iteml->periodo2;}else{echo '0';} ?>,
                                                                    <?php } ?>
                                                                  ],
                                                            label: "Periodo 2",
                                                            backgroundColor: "#ff9800",
                                                          }, 
                                                          { 
                                                            data: [
                                                                    <?php foreach ($rowlecturas->result() as $iteml) {  ?>
                                                                        <?php if($iteml->periodo3>0){echo $iteml->periodo3;}else{echo '0';} ?>,
                                                                    <?php } ?>
                                                                  ],
                                                            label: "Periodo 3",
                                                            backgroundColor: "#c6c9cb",
                                                          } 
                                                          
                                                  
                                                ],

                                              },
                                              options: {
                                                    title: {
                                                        display: true,
                                                        text: 'NSCEA,TI',
                                                        fontSize:18,
                                                        position:'top'
                                                    },
                                                    subtitle: {
                                                        display: true,
                                                        text: 'Luxes',
                                                        fontSize:18,
                                                        position:'left'
                                                    },
                                                    scales: {
                                                      yAxes: [{
                                                        ticks: {
                                                          beginAtZero: true
                                                        }
                                                      }]
                                                    }
                                                  }
                                            });
                                    }
                                    </script>
                                </div>
                                <div id="grafo2_<?php echo $item->idnomd?>">
                                    <div class="row">
                                        <div class="col-md-12" style="text-align:center;"><b>REGISTRO DEL ESPECTRO ACÚSTICO</b></div>
                                    </div>
                                    <table class="table table-bordered tablecaptura rea tablev">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Periodo</th>
                                                <th colspan="2">Ponderación</th>
                                                <th colspan="9">Frecuencias Centrales (Hz)</th>
                                            </tr>
                                            <tr>
                                                <th>db(A)</th>
                                                <th>Lineal</th>
                                                <th>31.5</th>
                                                <th>63</th>
                                                <th>125</th>
                                                <th>250</th>
                                                <th>500</th>
                                                <th>1000</th>
                                                <th>2000</th>
                                                <th>4000</th>
                                                <th>8000</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                                                $rownomdreaperiodo=1;
                                                $rownomdrea_num=$rownomdrea->num_rows();

                                                $rea_db_t=0;$rea_lineal_t=0;$rea_31_5_t=0;$rea_63_t=0;$rea_125_t=0;$rea_250_t=0;$rea_500_t=0;$rea_1000_t=0;$rea_2000_t=0;$rea_4000_t=0;$rea_8000_t=0;

                                                foreach ($rownomdrea->result() as $itrea) { 
                                                    if($rownomdrea_num==1){
                                                        $rea_db_t=$itrea->rea_db;
                                                        $rea_lineal_t=$itrea->rea_lineal;
                                                        $rea_31_5_t=$itrea->rea_31_5;
                                                        $rea_63_t=$itrea->rea_63;
                                                        $rea_125_t=$itrea->rea_125;
                                                        $rea_250_t=$itrea->rea_250;
                                                        $rea_500_t=$itrea->rea_500;
                                                        $rea_1000_t=$itrea->rea_1000;
                                                        $rea_2000_t=$itrea->rea_2000;
                                                        $rea_4000_t=$itrea->rea_4000;
                                                        $rea_8000_t=$itrea->rea_8000;
                                                    }else{
                                                        $rea_db_t=$rea_db_t+(pow(10,($itrea->rea_db/10)));
                                                        $rea_lineal_t=$rea_lineal_t+(pow(10,($itrea->rea_lineal/10)));
                                                        $rea_31_5_t=$rea_31_5_t+(pow(10,($itrea->rea_31_5/10)));
                                                        $rea_63_t=$rea_63_t+(pow(10,($itrea->rea_63/10)));
                                                        $rea_125_t=$rea_125_t+(pow(10,($itrea->rea_125/10)));
                                                        $rea_250_t=$rea_250_t+(pow(10,($itrea->rea_250/10)));
                                                        $rea_500_t=$rea_500_t+(pow(10,($itrea->rea_500/10)));
                                                        $rea_1000_t=$rea_1000_t+(pow(10,($itrea->rea_1000/10)));
                                                        $rea_2000_t=$rea_2000_t+(pow(10,($itrea->rea_2000/10)));
                                                        $rea_4000_t=$rea_4000_t+(pow(10,($itrea->rea_4000/10)));
                                                        $rea_8000_t=$rea_8000_t+(pow(10,($itrea->rea_8000/10)));
                                                    }
                                                    ?>
                                                    <tr >
                                                        <th><?php echo $rownomdreaperiodo; ?></th>
                                                        <td><?php echo $itrea->rea_db;?></td>
                                                        <td><?php echo $itrea->rea_lineal;?></td>
                                                        <td><?php echo $itrea->rea_31_5;?></td>
                                                        <td><?php echo $itrea->rea_63;?></td>
                                                        <td><?php echo $itrea->rea_125;?></td>
                                                        <td><?php echo $itrea->rea_250;?></td>
                                                        <td><?php echo $itrea->rea_500;?></td>
                                                        <td><?php echo $itrea->rea_1000;?></td>
                                                        <td><?php echo $itrea->rea_2000;?></td>
                                                        <td><?php echo $itrea->rea_4000;?></td>
                                                        <td><?php echo $itrea->rea_8000;?></td>
                                                    </tr>
                                            <?php $rownomdreaperiodo++;
                                                }
                                            ?>
                                            <tr >
                                                <th>NPA (dB)</th>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_db_t0=$rea_db_t;    
                                                        }else{
                                                            $rea_db_t0=10*log10((1/2)*$rea_db_t);
                                                        }
                                                        echo $rea_db_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_lineal_t0=$rea_lineal_t;    
                                                        }else{
                                                            $rea_lineal_t0=10*log10((1/2)*$rea_lineal_t);
                                                        }
                                                        echo $rea_lineal_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_31_5_t0=$rea_31_5_t;    
                                                        }else{
                                                            $rea_31_5_t0=10*log10((1/2)*$rea_31_5_t);
                                                        }
                                                        echo $rea_31_5_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_63_t0=$rea_63_t;    
                                                        }else{
                                                            $rea_63_t0=10*log10((1/2)*$rea_63_t);
                                                        }
                                                        echo $rea_63_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_125_t0=$rea_125_t;    
                                                        }else{
                                                            $rea_125_t0=10*log10((1/2)*$rea_125_t);
                                                        }
                                                        echo $rea_125_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_250_t0=$rea_250_t;    
                                                        }else{
                                                            $rea_250_t0=10*log10((1/2)*$rea_250_t);
                                                        }
                                                        echo $rea_250_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_500_t0=$rea_500_t;    
                                                        }else{
                                                            $rea_500_t0=10*log10((1/2)*$rea_500_t);
                                                        }
                                                        echo $rea_500_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_1000_t0=$rea_1000_t;    
                                                        }else{
                                                            $rea_1000_t0=10*log10((1/2)*$rea_1000_t);
                                                        }
                                                        echo $rea_1000_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_2000_t0=$rea_2000_t;    
                                                        }else{
                                                            $rea_2000_t0=10*log10((1/2)*$rea_2000_t);
                                                        }
                                                        echo $rea_2000_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_4000_t0=$rea_4000_t;    
                                                        }else{
                                                            $rea_4000_t0=10*log10((1/2)*$rea_4000_t);
                                                        }
                                                        echo $rea_4000_t0;
                                                    ?></td>
                                                <td><?php 
                                                        if($rownomdrea_num==1){
                                                            $rea_8000_t0=$rea_8000_t;    
                                                        }else{
                                                            $rea_8000_t0=10*log10((1/2)*$rea_8000_t);
                                                        }
                                                        echo $rea_8000_t0;
                                                    ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align:center;"><b>FACTOR DE ATENUACIÓN DEL EQUIPO DE PROTECCIÓN AUDITIVA</b></div>
                                    </div>
                                    <table class="table table-bordered tablecaptura faepa tablev">
                                        <thead>
                                            <tr>
                                                <th>Frecuencia (Hz)</th>
                                                <th>125</th>
                                                <th>250</th>
                                                <th>500</th>
                                                <th>1000</th>
                                                <th>2000</th>
                                                <th>3000</th>
                                                <th>4000</th>
                                                <th>6000</th>
                                                <th>8000</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $rownomdfaep=$this->ModeloCatalogos->getselectwheren('nom11d_faepa',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                                                $itemfp=$rownomdfaep->row();
                                                //foreach ($rownomdfaep->result() as $itemfp) {
                                            ?>
                                                    <tr onchange="saveform('nom11d_faepa',<?php echo $itemfp->idnomd;?>)">
                                                        <th>Atenuación (dB)</th>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_125;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_250;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_500;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_1000;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_2000;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_3000;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_4000;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_6000;?></td>
                                                        <td class="tdinput"><?php echo $itemfp->faepa_8000;?></td>
                                                    </tr>
                                            <?php
                                                //}
                                            ?>
                                        </tbody>
                                        
                                    </table>
                                    <?php
                                    //----------------------------------------------
                                        $rrpa_125=16.2;
                                        $rrpa_250=8.7;
                                        $rrpa_500=3.3;
                                        $rrpa_1000=0;
                                        $rrpa_2000=-1.2;
                                        $rrpa_4000=-1;
                                        $rrpa_8000=1.1;
                                        $itrea2=$rownomdrea->row(0);
                                    //--------------------------------------------
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align:center;"><b>REDUCCIÓN DE RUIDO PARA LA PROTECCIÓN AUDITIVA</b></div>
                                        <div class="col-md-3"></div><div class="col-md-9">Correcciones para Q</div>
                                    </div>
                                    <table class="table table-bordered tablev">
                                        <thead>
                                            <tr>
                                                <th>125 Hz</th>
                                                <th>250 Hz</th>
                                                <th>500 Hz</th>
                                                <th>1000 Hz</th>
                                                <th>2000 Hz</th>
                                                <th>4000 Hz</th>
                                                <th>8000 Hz</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $rrpa_125; ?></td>
                                                <td><?php echo $rrpa_250; ?></td>
                                                <td><?php echo $rrpa_500; ?></td>
                                                <td><?php echo $rrpa_1000; ?></td>
                                                <td><?php echo $rrpa_2000; ?></td>
                                                <td><?php echo $rrpa_4000; ?></td>
                                                <td><?php echo $rrpa_8000; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php 
                                        if($itemfp->faepa_125==0){
                                            $aqf_q1=0;  
                                        }else{
                                            $aqf_q1=$itemfp->faepa_125+$rrpa_125;
                                        }
                                        if($itemfp->faepa_250==0){
                                            $aqf_q2=0;  
                                        }else{
                                            $aqf_q2=$itemfp->faepa_250+$rrpa_250;
                                        }
                                        if($itemfp->faepa_500==0){
                                            $aqf_q3=0;  
                                        }else{
                                            $aqf_q3=$itemfp->faepa_500+$rrpa_500;
                                        }
                                        if($itemfp->faepa_1000==0){
                                            $aqf_q4=0;  
                                        }else{
                                            $aqf_q4=$itemfp->faepa_1000+$rrpa_1000;
                                        }
                                        if($itemfp->faepa_2000==0){
                                            $aqf_q5=0;  
                                        }else{
                                            $aqf_q5=$itemfp->faepa_2000+$rrpa_2000;
                                        }
                                        if($itemfp->faepa_3000==0){
                                            $aqf_q6=0;  
                                        }else{
                                            $aqf_q6=($itemfp->faepa_3000+$itemfp->faepa_4000)/2+$rrpa_4000;
                                        }
                                        if($itemfp->faepa_6000==0){
                                            $aqf_q7=0;  
                                        }else{
                                            $aqf_q7=($itemfp->faepa_6000+$itemfp->faepa_8000)/2+$rrpa_8000;
                                        }
                                        
                                    ?>
                                    <table class="table table-bordered tablev">
                                        <tr>
                                            <th colspan="2">Atenuación Q Final</th>
                                            <th>Lj-Qj/10</th>
                                            <th><img src="<?php echo base_url();?>public/img/formula7.png"></th>
                                        </tr>
                                        <tr>
                                            <th>Q1</th>
                                            <td><?php if($aqf_q1>0){ echo $aqf_q1;}else{ echo '---';};?></td>
                                            <td><?php 
                                                    if($aqf_q1>0){ 
                                                        $aqf_q1_r1=($itrea2->rea_125-$aqf_q1)/10;
                                                        echo $aqf_q1_r1;
                                                    }else{ 
                                                        $aqf_q1_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q1_r1>0){ 
                                                        $aqf_q1_r2=pow(10,$aqf_q1_r1);
                                                        echo round($aqf_q1_r2,2);
                                                    }else{ 
                                                        $aqf_q1_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q2</th>
                                            <td><?php if($aqf_q2>0){ echo $aqf_q2;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q2>0){ 
                                                        $aqf_q2_r1=($itrea2->rea_250-$aqf_q2)/10;
                                                        echo round($aqf_q2_r1,2);
                                                    }else{ 
                                                        $aqf_q2_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q2_r1>0){ 
                                                        $aqf_q2_r2=pow(10,$aqf_q2_r1);
                                                        echo round($aqf_q2_r2,2);
                                                    }else{ 
                                                        $aqf_q2_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q3</th>
                                            <td><?php if($aqf_q3>0){ echo $aqf_q3;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q3>0){ 
                                                        $aqf_q3_r1=($itrea2->rea_500-$aqf_q3)/10;
                                                        echo round($aqf_q3_r1,2);
                                                    }else{ 
                                                        $aqf_q3_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q3_r1>0){ 
                                                        $aqf_q3_r2=pow(10,$aqf_q3_r1);
                                                        echo round($aqf_q3_r2,2);
                                                    }else{ 
                                                        $aqf_q3_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q4</th>
                                            <td><?php if($aqf_q4>0){ echo $aqf_q4;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q4>0){ 
                                                        $aqf_q4_r1=($itrea2->rea_1000-$aqf_q4)/10;
                                                        echo round($aqf_q4_r1,2);
                                                    }else{ 
                                                        $aqf_q4_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q4_r1>0){ 
                                                        $aqf_q4_r2=pow(10,$aqf_q4_r1);
                                                        echo round($aqf_q4_r2,2);
                                                    }else{ 
                                                        $aqf_q4_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q5</th>
                                            <td><?php if($aqf_q5>0){ echo $aqf_q5;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q5>0){ 
                                                        $aqf_q5_r1=($itrea2->rea_2000-$aqf_q5)/10;
                                                        echo round($aqf_q5_r1,2);
                                                    }else{ 
                                                        $aqf_q5_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q5_r1>0){ 
                                                        $aqf_q5_r2=pow(10,$aqf_q5_r1);
                                                        echo round($aqf_q5_r2,2);
                                                    }else{ 
                                                        $aqf_q5_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q6</th>
                                            <td><?php if($aqf_q6>0){ echo $aqf_q6;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q6>0){ 
                                                        $aqf_q6_r1=($itrea2->rea_4000-$aqf_q6)/10;
                                                        echo round($aqf_q6_r1,2);
                                                    }else{ 
                                                        $aqf_q6_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q6_r1>0){ 
                                                        $aqf_q6_r2=pow(10,$aqf_q6_r1);
                                                        echo round($aqf_q6_r2,2);
                                                    }else{ 
                                                        $aqf_q6_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th>Q7</th>
                                            <td><?php if($aqf_q7>0){ echo $aqf_q7;}else{ echo '--';};?></td>
                                            <td><?php 
                                                    if($aqf_q7>0){ 
                                                        $aqf_q7_r1=($itrea2->rea_8000-$aqf_q7)/10;
                                                        echo round($aqf_q7_r1,2);
                                                    }else{ 
                                                        $aqf_q7_r1=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                            <td><?php 
                                                    if($aqf_q7_r1>0){ 
                                                        $aqf_q7_r2=pow(10,$aqf_q7_r1);
                                                        echo round($aqf_q7_r2,2);
                                                    }else{ 
                                                        $aqf_q7_r2=0;
                                                        echo '---';
                                                    };
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">Suma</td>
                                            <td>
                                                <?php 
                                                    if($aqf_q7>0){
                                                        $aqf_q1_q7_suma=$aqf_q1_r2+$aqf_q2_r2+$aqf_q3_r2+$aqf_q4_r2+$aqf_q5_r2+$aqf_q6_r2+$aqf_q7_r2;
                                                        echo round($aqf_q1_q7_suma,2);
                                                    }else{
                                                        $aqf_q1_q7_suma=0;
                                                        echo '---';
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <th colspan="2">FACTOR DE REDUCCIÓN, MODELO POR BANDAS DE OCTAVAS</th>
                                        </tr>
                                        <tr>
                                            <td><img src="<?php echo base_url();?>public/img/formula8.png"></td>
                                            <td>Ri dB(A)=<b><?php 
                                                $Ri_dBa=$nivel_expocion_ruido-10*log10($aqf_q1_q7_suma)-10;
                                                echo round($Ri_dBa,1);
                                            ?></b></td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <th colspan="3">FACTOR DE REDUCCIÓN, MODELO CON MEDICIONES DE RUIDO</th>
                                        </tr>
                                        <tr>
                                            <td>NRR=<b><?php 
                                                    if($val_nrr==0){
                                                        $nrr=26;
                                                    }else{
                                                        $nrr=$val_nrr;
                                                    }
                                                    echo $nrr; 
                                                ?></b></td>
                                            <td><img src="<?php echo base_url();?>public/img/formula9.png"></td>
                                            <td>R dB(A)=<b><?php 
                                                $R_dBa=($nrr-7)/2;
                                                echo $R_dBa;
                                            ?></b></td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <th colspan="2">NIVEL DE RUIDO EFECTIVO</th>
                                        </tr>
                                        <tr>
                                            <td>NRE = dB(A) - R</td>
                                            <td>NRE dB(A)= <b><?php 
                                                $NRW_dBa=round($nivel_expocion_ruido,2)-$R_dBa;
                                                echo round($NRW_dBa,1);
                                            ?></b></td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <canvas id="myChartgtgb_<?php echo $item->idnomd?>" width="500" height="300"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function($) {
                                       graficageneralb<?php echo $item->idnomd?>(); 
                                    });
                                    function graficageneralb<?php echo $item->idnomd?>(){
                                        var ctx = document.getElementById('myChartgtgb_<?php echo $item->idnomd?>').getContext('2d');
                                        var myChart = new Chart(ctx, {
                                          type: 'bar',
                                          data: {
                                            labels: ['db(A)','Lineal','31.5','63','125','250','500','1000','2000','4000','8000'],
                                            datasets: [{ 
                                                        data: [
                                                                <?php echo $rea_db_t0;?>,<?php echo $rea_lineal_t0;?>,<?php echo $rea_31_5_t0;?>,<?php echo $rea_63_t0;?>,<?php echo $rea_125_t0;?>,<?php echo $rea_250_t0;?>,<?php echo $rea_500_t0;?>,<?php echo $rea_1000_t0;?>,<?php echo $rea_2000_t0;?>,<?php echo $rea_4000_t0;?>,<?php echo $rea_8000_t0;?>
                                                              ],
                                                        label: "NPA (dB)",
                                                        backgroundColor: "#2196f3",
                                                      }],
                                          },
                                          options: {
                                                title: {
                                                    display: true,text: 'FRECUENCIA',fontSize:18,position:'top'
                                                },
                                                subtitle: {
                                                    display: true,text: 'Luxes',fontSize:18,position:'left'
                                                },
                                                scales: {
                                                  yAxes: [{
                                                    ticks: {
                                                      beginAtZero: true
                                                    }
                                                  }]
                                                }
                                              }
                                        });
                                    }
                                </script>
                                <table class="table table-bordered tablecaptura tablev" id="table_lectura" align="center">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Periodo 1</th>
                                            <th colspan="2">Periodo 2</th>
                                            <th colspan="2">Periodo 3</th>
                                        </tr>
                                        <tr>
                                            <th>Lectura</th>
                                            <th >NSCEA,T  dBA</th>
                                            <th>Lectura</th>
                                            <th >NSCEA,T  dBA</th>
                                            <th>Lectura</th>
                                            <th >NSCEA,T  dBA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); 
                                            $rowlecturas_tr=1;
                                            $periodo1_t=0;
                                            $periodo2_t=0;
                                            $periodo3_t=0;
                                            $totallecturas=$rowlecturas->num_rows();
                                            foreach ($rowlecturas->result() as $iteml) { ?>
                                                <tr  >
                                                    <th class="tdinput"><?php echo $rowlecturas_tr;?></th>
                                                    <td class="tdinput"><?php 
                                                                            if($iteml->periodo1>0){
                                                                                $periodo1_t=$periodo1_t+$iteml->periodo1;
                                                                                echo $iteml->periodo1;
                                                                            }else{
                                                                                echo '---';
                                                                            };?></td>
                                                    <th class="tdinput"><?php echo $rowlecturas_tr;?></th>
                                                    <td class="tdinput"><?php 
                                                                            if($iteml->periodo2>0){
                                                                                $periodo2_t=$periodo2_t+$iteml->periodo2;
                                                                                echo $iteml->periodo2;
                                                                            }else{
                                                                                echo '---';
                                                                            };?></td>
                                                    <th class="tdinput"><?php echo $rowlecturas_tr;?></th>
                                                    <td class="tdinput"><?php 
                                                                            if($iteml->periodo3>0){
                                                                                $periodo3_t=$periodo3_t+$iteml->periodo3;
                                                                                echo $iteml->periodo3;
                                                                            }else{
                                                                                echo '---';
                                                                            };?></td>        
                                                </tr>
                                                <?php
                                                $rowlecturas_tr++;
                                            }
                                        ?>
                                                <tr>
                                                    <th>Promedio</th>
                                                    <td><?php 
                                                            $periodo1_prom=$periodo1_t/$totallecturas;
                                                            if($periodo1_prom>0){echo $periodo1_prom;}else{ echo '---';}
                                                        ?></td>
                                                    <td></td>
                                                    <td><?php 
                                                            $periodo2_prom=$periodo2_t/$totallecturas;
                                                            if($periodo2_prom>0){echo $periodo2_prom;}else{ echo '---';}
                                                        ?></td>
                                                    <td></td>
                                                    <td><?php 
                                                            $periodo3_prom=$periodo3_t/$totallecturas;
                                                            if($periodo3_prom>0){echo $periodo3_prom;}else{ echo '---';}
                                                        ?></td>
                                                </tr>
                                    </tbody> 
                                </table>
                                <div class="row">
                                    <div class="col-md-8">
                                        <table class="tablev">
                                            <tr>
                                                <th>Desviación estándar entre los periodos</th>
                                                <td><?php 
                                                        $desviacion_estandar=$this->ModeloCatalogos->desvest(array($periodo1_prom,$periodo2_prom,$periodo3_prom));
                                                        echo round($desviacion_estandar,2);
                                                    ?></td>
                                                <th>dB</th>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td>n= </td>
                                                <td><?php
                                                    $n_p_r_e=2;
                                                    echo $n_p_r_e;
                                                    ?></td>
                                                <td>Periodos para ruido estable</td>
                                            </tr>
                                            <tr>
                                                <td>n= </td>
                                                <td><?php
                                                    $n_p_r_i=3;
                                                    echo $n_p_r_i;
                                                    ?></td>
                                                <td>Periodos para ruido inestable</td>
                                            </tr>
                                        </table>
                                        <table class="table tablev table-bordered">
                                            <tr>
                                                <th colspan="4">Componentes de Incertidumbres</th>
                                            </tr>
                                            <tr>
                                                <th>Incertidumbre tipo A, por las lecturas, Repetibilidad en dB</th>
                                                <th>Resolución del sonometro (dB)</th>
                                                <th>Certificado de calibración del sonometro (dB)</th>
                                                <th>Certificado de calibración del calibrador acústico (dB)</th>
                                            </tr>
                                            <tr>
                                                <td><?php 
                                                    $ci_italr=(1/sqrt($n_p_r_e))*$desviacion_estandar;
                                                    echo round($ci_italr,2);
                                                ?></td>
                                                <td><?php
                                                    $ci_rs=0.10;
                                                    echo $ci_rs;
                                                ?></td>
                                                <td><?php
                                                    $ci_ccs=0.12;
                                                    echo $ci_ccs;
                                                ?></td>
                                                <td><?php
                                                    $ci_ccca=0.20;
                                                    echo $ci_ccca;
                                                ?></td>
                                            </tr>
                                            <tr>
                                                <th colspan="4">dB convertidos a valores relativos</th>
                                            </tr>
                                            <tr>
                                                <td><?php 
                                                    $ci_italr_vr=pow(10,($ci_italr/20))-1;
                                                    echo $ci_italr_vr;
                                                ?></td>
                                                <td><?php 
                                                    $ci_rs_vr=pow(10,($ci_rs/20))-1;
                                                    echo $ci_rs_vr;
                                                ?></td>
                                                <td><?php 
                                                    $ci_ccs_vr=pow(10,($ci_ccs/20))-1;
                                                    echo $ci_ccs_vr;
                                                ?></td>
                                                <td><?php 
                                                    $ci_ccca_vr=pow(10,($ci_ccca/20))-1;
                                                    echo $ci_ccca_vr;
                                                ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-4">
                                        <table class="tablev">
                                            <tr>
                                                <th>
                                                    <table border="0">
                                                        <tr>
                                                            <td colspan="2" style="border:0;">Convertir dB a Unidades Relativas</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="border:0;">(dB/20)</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border:0;">10</td>
                                                            <td style="border:0;">-1</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="border:0;">Donde: dB = valor en decibeles</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="border:0;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="border:0;">Convertir unidades relativas a dB</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="border:0;">20log(1 + valor relativo)</td>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <?php 
                                    $fi_cs_ci_l=1;
                                    $fi_cs_ci_r=1;
                                    $fi_cs_ci_s=1;
                                    $fi_cs_ci_c=1;
                                ?>
                                <table class="table tablev table-bordered" style="margin-top:20px;">
                                    <tr>
                                        <th>Fuentes de incertidumbre</th>
                                        <th>Incertidumbre estándar (rel)</th>
                                        <th>Tipo de<br>Distribución</th>
                                        <th>Incertidumbre<br>estándar u(xi)  [rel]</th>
                                        <th>Coeficiente de<br>sensibilidad Ci</th>
                                        <th>Contribución ui(y)  [rel]</th>
                                        <th>(ui(y))2    [rel2]</th>
                                    </tr>
                                    <tr>
                                        <th>Lecturas</th>
                                        <td><?php echo $ci_italr_vr;?></td>
                                        <td>A, normal k=1</td>
                                        <td><?php $ci_italr_vr_ie=$ci_italr_vr/1;
                                            echo $ci_italr_vr_ie;?></td>
                                        <td><?php echo $fi_cs_ci_l; ?></td>
                                        <td><?php 
                                            $fi_co_ur_l=$ci_italr_vr_ie*$fi_cs_ci_l;
                                            echo $fi_co_ur_l;
                                        ?></td>
                                        <td><?php 
                                            $fi_ui_rel_l=pow($fi_co_ur_l,2);
                                            echo $fi_ui_rel_l;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <th>Resolución</th>
                                        <td><?php echo $ci_rs_vr;?></td>
                                        <td>B, k=1</td>
                                        <td><?php
                                            $ci_rs_vr_ie=$ci_rs_vr/sqrt(12); 
                                            echo $ci_rs_vr_ie;?></td>
                                        <td><?php echo $fi_cs_ci_r; ?></td>
                                        <td><?php 
                                            $fi_co_ur_r=$ci_rs_vr_ie*$fi_cs_ci_r;
                                            echo $fi_co_ur_r;
                                        ?></td>
                                        <td><?php 
                                            $fi_ui_rel_r=pow($fi_co_ur_r,2);
                                            echo $fi_ui_rel_r;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <th>Sonómetro</th>
                                        <td><?php echo $ci_ccs_vr;?></td>
                                        <td>B, k=2</td>
                                        <td><?php 
                                            $ci_ccs_vr_ie=$ci_ccs_vr/2;
                                            echo $ci_ccs_vr_ie;?></td>
                                        <td><?php echo $fi_cs_ci_s; ?></td>
                                        <td><?php 
                                            $fi_co_ur_s=$ci_ccs_vr_ie*$fi_cs_ci_s;
                                            echo $fi_co_ur_s;
                                        ?></td>
                                        <td><?php 
                                            $fi_ui_rel_s=pow($fi_co_ur_s,2);
                                            echo $fi_ui_rel_s;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <th>Calibrador</th>
                                        <td><?php echo $ci_ccca_vr;?></td>
                                        <td>B, k=2</td>
                                        <td><?php 
                                            $ci_ccca_vr_ie=$ci_ccca_vr/2;
                                            echo $ci_ccca_vr_ie;?></td>
                                        <td><?php echo $fi_cs_ci_c; ?></td>
                                        <td><?php 
                                            $fi_co_ur_c=$ci_ccca_vr_ie*$fi_cs_ci_c;
                                            echo $fi_co_ur_c;
                                        ?></td>
                                        <td><?php 
                                            $fi_ui_rel_c=pow($fi_co_ur_c,2);
                                            echo $fi_ui_rel_c;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"></td>
                                        <th>SUMA(ui(y))2</th>
                                        <td><?php 
                                            $fi_ui_rel_suma=$fi_ui_rel_l+$fi_ui_rel_r+$fi_ui_rel_s+$fi_ui_rel_c;
                                            echo $fi_ui_rel_suma;
                                        ?></td>
                                    </tr>
                                </table>
                                <table class="table tablev table-bordered">
                                    <tr>
                                        <th>uc</th>
                                        <td><?php 
                                            $rc_fi_uc=sqrt($fi_ui_rel_suma);
                                            echo $rc_fi_uc;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <th>U</th>
                                        <td><?php 
                                            $rc_fi_u=$rc_fi_uc*2;
                                            echo $rc_fi_u;
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <th>U Convertido a dB</th>
                                        <td><?php 
                                            $rc_fi_u_c_db=20*log10(1+$rc_fi_u);
                                            echo round($rc_fi_u_c_db,2);
                                        ?></td>
                                    </tr>
                                </table>
                                <table class="table tablev table-bordered">
                                    <tr>
                                        <th>U [dB]</th>
                                        <td><u>+</u></td>
                                        <td><?php echo round($rc_fi_u_c_db,2);?></td>
                                    </tr>
                                </table>
                                <table class="table tablev table-bordered">
                                    <tr>
                                        <th>Factor de Cobertura</th>
                                        <th>k =2</th>
                                    </tr>
                                    <tr>
                                        <th>Nivel de Confianza</th>
                                        <th>95.50%</th>
                                    </tr>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function($) {
                                        setTimeout(function(){
                                            savegrafo(1,'myChartgtg<?php echo $item->idnomd?>',<?php echo $item->idnomd?>);
                                            savegrafo(2,'myChartgtgb_<?php echo $item->idnomd?>',<?php echo $item->idnomd?>);
                                        }, 1500);
                                        

                                    });
                                </script>
                        <?php $rowpuntostr++;
                            }
                        ?>
                        
                            
                        
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table tablev table-bordered tablecaptura">
                                    <tr>
                                        <th>No. de Informe</th>
                                        <td><?php echo $num_informe;?></td>
                                    </tr>
                                    <tr>
                                        <th>Razón Social</th>
                                        <td><?php echo $razon_social;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php $rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1)); ?>
                                <table class="table tablev table-bordered tablecaptura" id="table_lectura">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Id. Dosimetro</th>
                                            <th>Nombre del trabajador</th>
                                            <th>Area</th>
                                            <th>puesto</th>
                                            <th>% Dosis</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowpersonal->result() as $itemp) { ?>
                                            <tr>
                                                <th><?php echo $itemp->num;?></th>
                                                <td class="tdinput">
                                                    <?php echo $itemp->id_dosimetro;?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->trabajador;?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->area;?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->puesto;?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->dosis;
                                                                $dosis[$itemp->num]=$itemp->dosis;
                                                    ?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->fecha;?>
                                                </td>
                                                

                                            </tr>
                                        <?php } ?>
                                        </tbody> 
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                No= Número consecutivo de Dosimetría medida; %Dosis = Porcentaje de Dosis
                                <br><p> </p><br><p> </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php $rowperti=$this->ModeloCatalogos->getselectwheren('nom11_personal_ti',array('idnom'=>$idnom,'activo'=>1)); ?>
                                <table class="table tablev table-bordered tablecaptura" id="table_lectura">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Ti (h)</th>
                                            <th>Tf (h)</th>
                                            <th>T (h)</th>
                                            <th>NER <br>dB(A)</th>
                                            <th>TMPE (h)</th>
                                            <th>NRR</th>
                                            <th>R<br>dB(A)</th>
                                            <th>NRE<br>dB(A)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowperti->result() as $itemp) { ?>
                                            <tr >
                                                <th><?php echo $itemp->num;?></th>
                                                <td class="tdinput">
                                                    <?php echo $itemp->ti;?>
                                                </td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->tf;?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $difference = date_diff(date_create($itemp->ti), date_create($itemp->tf));
                                                        echo $difference->h;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if(isset($dosis[$itemp->num])){
                                                            $ner_db=90+9.97*(log10($dosis[$itemp->num]/(12.5*$difference->h)) );
                                                            echo round($ner_db,1);
                                                        }else{
                                                            $ner_db=0;
                                                            echo '---';
                                                        }
                                                    ?>
                                                </td>
                                                <td><?php 
                                                    if($ner_db<90){
                                                        $tmpe_h=0;
                                                        echo 'No Aplica';
                                                    }else{
                                                        $tmpe_h=8/pow(2,(($ner_db-90)/3 ));
                                                        echo $tmpe_h;
                                                    }
                                                ?></td>
                                                <td class="tdinput">
                                                    <?php echo $itemp->nrr;?>
                                                </td>
                                                <td><?php
                                                    $R_dBap=($itemp->nrr-7)/2;
                                                    echo round($R_dBap,1);
                                                ?></td>
                                                <td><?php
                                                    $nre_db_p=($ner_db-$R_dBap);
                                                    echo round($nre_db_p,1);
                                                ?></td>
                                                

                                            </tr>
                                        <?php } ?>
                                        </tbody> 
                                </table>
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                Ti = hora de inicio de medición; Tf = hora final de medición; T = Tiempo total de medición; NER = Nivel de Exposición al Ruido; TMPE = Tiempo Máximo Permisible de Exposición

                            </div>
                            <div class="col-md-12" style="text-align:center">
                                NRR = Factor de Nivel de Reducción a Ruido establecido por el fabricante; R = Factor de Reducción; NRE = Nivel de Ruido Efectivo

                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <h3>NIVEL DE EXPOSICIÓN AL RUIDO</h3>
                                <img src="<?php echo base_url();?>public/img/formulap1.png">
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <h3>TIEMPO MÁXIMO PERMISIBLE DE EXPOSICIÓN</h3>
                                <img src="<?php echo base_url();?>public/img/formulap2.png">
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <h3>FACTOR DE REDUCCIÓN</h3>
                                <img src="<?php echo base_url();?>public/img/formulap3.png">
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <h3>NIVEL DE RUIDO EFECTIVO</h3>
                                <img src="<?php echo base_url();?>public/img/formulap4.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    function savegrafo(tipo,idname,idnomd){
        var base_url = $('#base_url').val();
        var canvasx= document.getElementById(idname);
        var dataURL = canvasx.toDataURL('image/png', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/nominserupdatetableg11',
            data: {
                  id:idnomd,
                  grafica:dataURL,
                  tipo:tipo
            },
            statusCode:{
              404: function(data){
                  
              },
              500: function(){
                  
              }
          },
          success:function(data){
              console.log("hecho");
          }
      });
    }
</script>