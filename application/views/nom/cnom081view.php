<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>plugins/Chart.js/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/Chart.js/chartjs-plugin-datalabels.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }
    .table .table {background-color : transparent;}
    .table th{
        background-color: #F5F7FA;
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <input type="hidden" id="id_orden" value="<?php echo $id_orden; ?>">
                        <input type="hidden" id="id_aux" value="<?php echo $idnom; ?>">
                        <input type="hidden" id="id_chs" value="<?php echo $id_chs; ?>">
                        <!--<form class="form" id="form_nom">-->
                        <table class="table table-sm" id="tabla_nom81">
                            <tbody id="table_prin_nom81">
                            
                            
                            <?php $get_nom=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$id_orden,'activo'=>1)); 
                            $rowpuntoset=1500;
                            foreach($get_nom->result() as $n){
                                $horario=$n->horario;
                                $idnom=$n->idnom;
                            ?>
                                
                                <tr id="datos_nom_principal" class="datos_nom_principal_<?php echo $n->idnom;?>">
                                    <td>
                                        <div class="row" id="cont_ocu1">
                                            <div class="col-md-4">
                                                <label>No. de Informe</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control" name="num_informe" id="num_informe" value="<?php echo $n->num_informe;?>">
                                            </div>
                                        </div>
                                        <div class="row" id="cont_ocu1">
                                            <div class="col-md-4">
                                                <label>Razón Social</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control" name="razon_social" id="razon_social" value="<?php echo $n->razon_social;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $n->fecha;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Lugar</label>
                                                <input type="text" name="lugar" id="lugar" class="form-control" value="<?php echo $n->lugar;?>">
                                            </div>
                                            
                                            <div class="col-md-4"> 
                                                <label>Horario de medición</label>    
                                                <!--<input type="text" name="horario" id="horario" class="form-control" value="<?php echo $horario;?>">-->
                                                <select name="horario" id="horario" class="form-control">
                                                    <option value="1" <?php if($horario=="1"){ echo "selected"; } ?>>Diurno</option>
                                                    <option value="2" <?php if($horario=="2"){ echo "selected"; } ?>>Nocturno</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Zona crítica</label><br>
                                                <?php echo $n->zona_critica; $zona_critica=$n->zona_critica;?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Verificación inicial</label>
                                                <input type="text" name="veri_ini" id="veri_ini" class="form-control" value="<?php echo $n->veri_ini;?>">
                                            </div>
                                            
                                            <div class="col-md-3"> 
                                                <label>Verificación final</label>    
                                                <input type="text" name="veri_fin" id="veri_fin" class="form-control" value="<?php echo $n->veri_fin;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Criterio de aceptación</label>
                                                <input type="text" name="criterio" id="criterio" class="form-control" value="<?php echo $n->criterio;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Cumple criterio</label>
                                                <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="<?php echo $n->cumple_criterio;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Potencia batería inicio</label>
                                                <input type="text" name="pot_bat_ini" id="pot_bat_ini" class="form-control" value="<?php echo $n->pot_bat_ini;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Potencia batería fin</label>
                                                <input type="text" name="pot_bat_fin" id="pot_bat_fin" class="form-control" value="<?php echo $n->pot_bat_fin;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Sonómetro</label>
                                                    <select name="id_sonometro" id="id_sonometro" class="form-control">
                                                        <?php foreach($sono->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$n->id_sonometro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Calibrador Acústico</label>
                                                    <select name="id_calibrador" id="id_calibrador" class="form-control">
                                                        <?php foreach($cali->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$n->id_calibrador){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulosC"><label>Observaciones</label></div>
                                            <div class="col-md-12"><?php echo $n->observaciones; ?></div>
                                        </div>
                                        <table class="table table-responsive">
                                            <tr>
                                                <td class="tdinput titulos">Elemento constructivo</td>
                                                <td class="tdinput titulos">Largo (m)</td>
                                                <td class="tdinput"><?php echo $n->ec_largo;?></td>
                                                <td class="tdinput titulos">Ancho (m)</td>
                                                <td class="tdinput"><?php echo $n->ec_ancho;?></td>
                                                <td class="tdinput titulos">Área (m2)</td>
                                                <td class="tdinput"><?php echo $n->ec_largo*$n->ec_ancho;?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="datos_nom_detalles_<?php echo $n->idnom;?>" class="datos_nom_detalles_for datos_nom_detalles_<?php echo $n->idnom;?>">
                                    <td>
                                        
                                        <?php $hi=""; $hf=""; $get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                        if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }
                                        ?>
                                        <div class="row" id="cont_datos">
                                            <div class="col-md-6">
                                                <table cellpadding="1" class="table table-bordered tablecaptura ruido_fte" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE FUENTE (db "A")</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hi;?></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hf;?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>A</th>
                                                            <th>B</th>
                                                            <th>C</th>
                                                            <th>D</th>
                                                            <th>E</th>
                                                        </tr>
                                                    
                                                        <?php $cont=0;
                                                        if($get_fte->num_rows()>0){
                                                            $array_lectura_a=[];
                                                            $array_lectura_b=[];
                                                            $array_lectura_c=[];
                                                            $array_lectura_d=[];
                                                            $array_lectura_e=[];
                                                            foreach ($get_fte->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput">
                                                                        <?php echo $cont;?>
                                                                    </td>
                                                                    <td class="tdinput"><?php echo $f->a;
                                                                    						$array_lectura_a[]=$f->a;?>
                                                                    </td>
                                                                    <td class="tdinput"><?php echo $f->b;
                                                                    						$array_lectura_b[]=$f->b;?>
                                                                    </td>
                                                                    <td class="tdinput"><?php echo $f->c;
                                                                    						$array_lectura_c[]=$f->c;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->d;
                                                                        $array_lectura_d[]=$f->d; ?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->e;
                                                                        $array_lectura_e[]=$f->e; ?>
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $hi=""; $hf=""; $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                                if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
                                                ?>
                                                <table class="table table-bordered tablecaptura ruido_fondo" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE FONDO (db "A")</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hi;?></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hf;?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>I</th>
                                                            <th>II</th>
                                                            <th>III</th>
                                                            <th>IV</th>
                                                            <th>V</th>
                                                        </tr>
                                                        <?php $cont=0;
                                                        if($get_fondo->num_rows()>0){
                                                            $array_lectura_1=[];
                                                            $array_lectura_2=[];
                                                            $array_lectura_3=[];
                                                            $array_lectura_4=[];
                                                            $array_lectura_5=[];
                                                            foreach ($get_fondo->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput">
                                                                        <?php echo $cont;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php   echo $f->uno;
                                                                                $array_lectura_1[]=$f->uno;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->dos;
                                                                                $array_lectura_2[]=$f->dos;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->tres;
                                                                                $array_lectura_3[]=$f->tres;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->cuatro;
                                                                                $array_lectura_4[]=$f->cuatro;?>
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <?php echo $f->cinco;
                                                                                $array_lectura_5[]=$f->cinco;?>
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del nivel Percentil 50 (N<sub>50</sub>) de cada punto de medición de fuente y fondo. (PROMEDIO)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form1.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>n =  Número de observaciones en el punto i = 35</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6">Mediciones de fuente</th>
                                                        <th colspan="6"> Mediciones de fondo</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>Prom.</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                        <th>Prom.</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>50</sub></th>
                                                        <td><?php 
                                                                $medicion_fu_a = array_sum($array_lectura_a)/count($array_lectura_a); 
                                                                echo round($medicion_fu_a, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_b = array_sum($array_lectura_b)/count($array_lectura_b); 
                                                                echo round($medicion_fu_b, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_c = array_sum($array_lectura_c)/count($array_lectura_c); 
                                                                echo round($medicion_fu_c, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_d = array_sum($array_lectura_d)/count($array_lectura_d); 
                                                                echo round($medicion_fu_d, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_e = array_sum($array_lectura_e)/count($array_lectura_e); 
                                                                echo round($medicion_fu_e, 1);
                                                                 ?></td>
                                                        <td><?php $prom_medic_fuente=($medicion_fu_a+$medicion_fu_b+$medicion_fu_c+$medicion_fu_d+$medicion_fu_e)/5;
                                                            echo round($prom_medic_fuente,1);
                                                            ?></td>
                                                        <td><?php 
                                                                $medicion_fu_1 = array_sum($array_lectura_1)/count($array_lectura_1); 
                                                                echo round($medicion_fu_1, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_2 = array_sum($array_lectura_2)/count($array_lectura_2); 
                                                                echo round($medicion_fu_2, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_3 = array_sum($array_lectura_3)/count($array_lectura_3);
                                                                echo round($medicion_fu_3, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_4 = array_sum($array_lectura_4)/count($array_lectura_4); 
                                                                echo round($medicion_fu_4, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu_5 = array_sum($array_lectura_5)/count($array_lectura_5); 
                                                                echo round($medicion_fu_5, 1);
                                                                 ?></td>
                                                        <td><?php $prom_medic_fonto=($medicion_fu_1+$medicion_fu_2+$medicion_fu_3+$medicion_fu_4+$medicion_fu_5)/5;
                                                            echo round($prom_medic_fonto,1);
                                                            ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <?php 
                                            if($get_fte->num_rows()>0){
                                                $array_lectura_a_pow=[];
                                                $array_lectura_b_pow=[];
                                                $array_lectura_c_pow=[];
                                                $array_lectura_d_pow=[];
                                                $array_lectura_e_pow=[];

                                                $array_lectura_a_pow_10=[];
                                                $array_lectura_b_pow_10=[];
                                                $array_lectura_c_pow_10=[];
                                                $array_lectura_d_pow_10=[];
                                                $array_lectura_e_pow_10=[];
                                                foreach ($get_fte->result() as $f) { 
                                                    $array_lectura_a_pow[]=pow(($f->a-$medicion_fu_a),2);
                                                    $array_lectura_b_pow[]=pow(($f->b-$medicion_fu_b),2);
                                                    $array_lectura_c_pow[]=pow(($f->c-$medicion_fu_c),2);
                                                    $array_lectura_d_pow[]=pow(($f->d-$medicion_fu_d),2);
                                                    $array_lectura_e_pow[]=pow(($f->e-$medicion_fu_e),2);

                                                    $array_lectura_a_pow_10[]=pow(10,($f->a/10));
                                                    $array_lectura_b_pow_10[]=pow(10,($f->b/10));
                                                    $array_lectura_c_pow_10[]=pow(10,($f->c/10));
                                                    $array_lectura_d_pow_10[]=pow(10,($f->d/10));
                                                    $array_lectura_e_pow_10[]=pow(10,($f->e/10));
                                                }
                                            }
                                        ?>
                                        <?php $cont=0;
                                            if($get_fondo->num_rows()>0){
                                                $array_lectura_1_pow=[];
                                                $array_lectura_2_pow=[];
                                                $array_lectura_3_pow=[];
                                                $array_lectura_4_pow=[];
                                                $array_lectura_5_pow=[];

                                                $array_lectura_1_pow_10=[];
                                                $array_lectura_2_pow_10=[];
                                                $array_lectura_3_pow_10=[];
                                                $array_lectura_4_pow_10=[];
                                                $array_lectura_5_pow_10=[];

                                                foreach ($get_fondo->result() as $f) { 
                                                    $array_lectura_1_pow[]=pow(($f->uno-$medicion_fu_1),2);
                                                    $array_lectura_2_pow[]=pow(($f->dos-$medicion_fu_2),2);
                                                    $array_lectura_3_pow[]=pow(($f->tres-$medicion_fu_3),2);
                                                    $array_lectura_4_pow[]=pow(($f->cuatro-$medicion_fu_4),2);
                                                    $array_lectura_5_pow[]=pow(($f->cinco-$medicion_fu_5),2);

                                                    $array_lectura_1_pow_10[]=pow(10,($f->uno/10));
                                                    $array_lectura_2_pow_10[]=pow(10,($f->dos/10));
                                                    $array_lectura_3_pow_10[]=pow(10,($f->tres/10));
                                                    $array_lectura_4_pow_10[]=pow(10,($f->cuatro/10));
                                                    $array_lectura_5_pow_10[]=pow(10,($f->cinco/10));
                                                }
                                            }
                                        ?>

                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación de la desviación estándar (&sigma;) de cada punto de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form2.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>n =  Número de observaciones en el punto i = 35</p>
                                                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6">Mediciones de fuente</th>
                                                        <th colspan="6"> Mediciones de fondo</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>Prom.</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                        <th>Prom.</th>
                                                    </tr>
                                                    <tr>
                                                        <th>s</th>
                                                        <td><?php 
                                                                $medicion_fu2_a_pow = pow(array_sum($array_lectura_a_pow)/(count($array_lectura_a_pow)-1),0.5);
                                                                echo round($medicion_fu2_a_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_b_pow = pow(array_sum($array_lectura_b_pow)/(count($array_lectura_b_pow)-1),0.5);
                                                                echo round($medicion_fu2_b_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_c_pow = pow(array_sum($array_lectura_c_pow)/(count($array_lectura_c_pow)-1),0.5);
                                                                echo round($medicion_fu2_c_pow, 1).'<br>';
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_d_pow = pow(array_sum($array_lectura_d_pow)/(count($array_lectura_d_pow)-1),0.5);
                                                                echo round($medicion_fu2_d_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_e_pow = pow(array_sum($array_lectura_e_pow)/(count($array_lectura_e_pow)-1),0.5);
                                                                echo round($medicion_fu2_e_pow, 1).'<br>';
                                                                 ?></td>
                                                        <td><?php $prom_medic2_fuente=($medicion_fu2_a_pow+$medicion_fu2_b_pow+$medicion_fu2_c_pow+$medicion_fu2_d_pow+$medicion_fu2_e_pow)/5;
                                                            echo round($prom_medic2_fuente,1);
                                                            ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_1_pow = pow(array_sum($array_lectura_1_pow)/(count($array_lectura_1_pow)-1),0.5);
                                                                echo round($medicion_fu2_1_pow, 1);

                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_2_pow = pow(array_sum($array_lectura_2_pow)/(count($array_lectura_2_pow)-1),0.5);
                                                                echo round($medicion_fu2_2_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_3_pow = pow(array_sum($array_lectura_3_pow)/(count($array_lectura_3_pow)-1),0.5);
                                                                echo round($medicion_fu2_3_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_4_pow = pow(array_sum($array_lectura_4_pow)/(count($array_lectura_4_pow)-1),0.5);
                                                                echo round($medicion_fu2_4_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu2_5_pow = pow(array_sum($array_lectura_5_pow)/(count($array_lectura_5_pow)-1),0.5);
                                                                echo round($medicion_fu2_5_pow, 1);
                                                                 ?></td>
                                                        <td><?php $prom_medic2_fonto=($medicion_fu2_1_pow+$medicion_fu2_2_pow+$medicion_fu2_3_pow+$medicion_fu2_4_pow+$medicion_fu2_5_pow)/5;
                                                            echo round($prom_medic2_fonto,1);
                                                            ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del N<sub>10</sub> de cada punto y de la Zona Crítica de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form3.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>10</sub> = Percentil 10 del punto i = dB(A)</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6">Mediciones de fuente</th>
                                                        <th colspan="6"> Mediciones de fondo</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>Prom.</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                        <th>Prom.</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>10</sub></th>
                                                        <td><?php 
                                                                $medicion_fu3_a_pow = $medicion_fu_a+(1.2817*$medicion_fu2_a_pow);
                                                                echo round($medicion_fu3_a_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_b_pow = $medicion_fu_b+(1.2817*$medicion_fu2_b_pow);
                                                                echo round($medicion_fu3_b_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_c_pow = $medicion_fu_c+(1.2817*$medicion_fu2_c_pow);
                                                                echo round($medicion_fu3_c_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_d_pow = $medicion_fu_d+(1.2817*$medicion_fu2_d_pow);
                                                                echo round($medicion_fu3_d_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_e_pow = $medicion_fu_e+(1.2817*$medicion_fu2_e_pow);
                                                                echo round($medicion_fu3_e_pow,1); 
                                                                 ?></td>
                                                        <td><?php $prom_medic3_fuente=($medicion_fu3_a_pow+$medicion_fu3_b_pow+$medicion_fu3_c_pow+$medicion_fu3_d_pow+$medicion_fu3_e_pow)/5;
                                                            echo round($prom_medic3_fuente,1);
                                                            ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_1_pow = $medicion_fu_1+(1.2817*$medicion_fu2_1_pow);
                                                                echo round($medicion_fu3_1_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_2_pow = $medicion_fu_2+(1.2817*$medicion_fu2_2_pow);
                                                                echo round($medicion_fu3_2_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_3_pow = $medicion_fu_3+(1.2817*$medicion_fu2_3_pow);
                                                                echo round($medicion_fu3_3_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_4_pow = $medicion_fu_4+(1.2817*$medicion_fu2_4_pow);
                                                                echo round($medicion_fu3_4_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu3_5_pow = $medicion_fu_5+(1.2817*$medicion_fu2_5_pow);
                                                                echo round($medicion_fu3_5_pow,1); 
                                                                 ?></td>
                                                        <td><?php $prom_medic3_fonto=($medicion_fu3_1_pow+$medicion_fu3_2_pow+$medicion_fu3_3_pow+$medicion_fu3_4_pow+$medicion_fu3_5_pow)/5;
                                                            echo round($prom_medic3_fonto,1);
                                                        ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del Nivel Equivalente (N<sub>eqi</sub>) de cada punto de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form4.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Neq<sub>i</sub> = Nivel equivalente del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>m = Número de observaciones en el punto i = 35</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6">Mediciones de fuente</th>
                                                        <th colspan="6"> Mediciones de fondo</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>eqi</sub></th>
                                                        <td><?php 
                                                                $medicion_fu4_a_pow =10*(log10(array_sum($array_lectura_a_pow_10)/count($array_lectura_a_pow_10)));
                                                                echo round($medicion_fu4_a_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_b_pow = 10*(log10(array_sum($array_lectura_b_pow_10)/count($array_lectura_b_pow_10)));
                                                                echo round($medicion_fu4_b_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_c_pow = 10*(log10(array_sum($array_lectura_c_pow_10)/count($array_lectura_c_pow_10)));
                                                                echo round($medicion_fu4_c_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_d_pow = 10*(log10(array_sum($array_lectura_d_pow_10)/count($array_lectura_d_pow_10)));
                                                                echo round($medicion_fu4_d_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_e_pow = 10*(log10(array_sum($array_lectura_e_pow_10)/count($array_lectura_e_pow_10)));
                                                                echo round($medicion_fu4_e_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_1_pow = 10*(log10(array_sum($array_lectura_1_pow_10)/count($array_lectura_1_pow_10)));
                                                                echo round($medicion_fu4_1_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_2_pow = 10*(log10(array_sum($array_lectura_2_pow_10)/count($array_lectura_2_pow_10)));
                                                                echo round($medicion_fu4_2_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_3_pow = 10*(log10(array_sum($array_lectura_3_pow_10)/count($array_lectura_3_pow_10)));
                                                                echo round($medicion_fu4_3_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_4_pow = 10*(log10(array_sum($array_lectura_4_pow_10)/count($array_lectura_4_pow_10)));
                                                                echo round($medicion_fu4_4_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $medicion_fu4_5_pow = 10*(log10(array_sum($array_lectura_5_pow_10)/count($array_lectura_5_pow_10)));
                                                                echo round($medicion_fu4_5_pow,1); 
                                                                 ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del Nivel Equivalente de los niveles equivalentes Neq<sub>(eq)</sub> o Nivel equivalente de la Zona Critica para la medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-center">
                                                <table class="table table-bordered" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th colspan="2">Fuente</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">10^(Neq/10)</th>
                                                    </tr>
                                                    <tr><th>A</th><td><?php $nivel_equivalente_a=pow(10,($medicion_fu4_a_pow/10));
                                                                        echo round($nivel_equivalente_a,4);?></td></tr>
                                                    <tr><th>B</th><td><?php $nivel_equivalente_b=pow(10,($medicion_fu4_b_pow/10));
                                                                        echo round($nivel_equivalente_b,4);?></td></tr>
                                                    <tr><th>C</th><td><?php $nivel_equivalente_c=pow(10,($medicion_fu4_c_pow/10));
                                                                        echo round($nivel_equivalente_c,4);?></td></tr>
                                                    <tr><th>D</th><td><?php $nivel_equivalente_d=pow(10,($medicion_fu4_d_pow/10));
                                                                        echo round($nivel_equivalente_d,4);?></td></tr>
                                                    <tr><th>E</th><td><?php $nivel_equivalente_e=pow(10,($medicion_fu4_e_pow/10));
                                                                        echo round($nivel_equivalente_e,4);?></td></tr>
                                                    <tr><td></td><td><?php $nivel_equivalente_fuente=($nivel_equivalente_a+$nivel_equivalente_b+$nivel_equivalente_c+$nivel_equivalente_d+$nivel_equivalente_e)/5;
                                                                        echo round($nivel_equivalente_fuente,4);?></td></tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <table class="table table-bordered" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th colspan="2">Fondo</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">10^(Neq/10)</th>
                                                    </tr>
                                                    <tr><th>I</th><td><?php $nivel_equivalente_1=pow(10,($medicion_fu4_1_pow/10));
                                                                        echo round($nivel_equivalente_1,4);?></td></tr>
                                                    <tr><th>II</th><td><?php $nivel_equivalente_2=pow(10,($medicion_fu4_2_pow/10));
                                                                        echo round($nivel_equivalente_2,4);?></td></tr>
                                                    <tr><th>III</th><td><?php $nivel_equivalente_3=pow(10,($medicion_fu4_3_pow/10));
                                                                        echo round($nivel_equivalente_3,4);?></td></tr>
                                                    <tr><th>IV</th><td><?php $nivel_equivalente_4=pow(10,($medicion_fu4_4_pow/10));
                                                                        echo round($nivel_equivalente_4,4);?></td></tr>
                                                    <tr><th>V</th><td><?php $nivel_equivalente_5=pow(10,($medicion_fu4_5_pow/10));
                                                                        echo round($nivel_equivalente_5,4);?></td></tr>
                                                    <tr><td></td><td><?php $nivel_equivalente_fondo=($nivel_equivalente_1+$nivel_equivalente_2+$nivel_equivalente_3+$nivel_equivalente_4+$nivel_equivalente_5)/5;
                                                                        echo round($nivel_equivalente_fondo,4);?></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form5.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Neq<sub>(eq)</sub> = Nivel equivalente de los niveles equivalentes</p>
                                                <p>o Nivel equivalente de la Zona Critica = dB(A)</p>
                                                <p>Neqi = Nivel equivalente del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>j = Número de puntos en la Zona Critica = 5</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>* Ruido de Fuente:</th><th>Neq<sub>(eq)</sub></th><td><?php $ruido_fuente_neq=10*log10($nivel_equivalente_fuente); 
                                                            echo round($ruido_fuente_neq,1)?></td><td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>* Ruido de Fondo:</th><th>Neq<sub>(eq)</sub></th><td><?php $ruido_fondo_neq=10*log10($nivel_equivalente_fondo);
                                                            echo round($ruido_fondo_neq,1) ?></td><td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Correción por presencia de valores extremos</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form6.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Ce = Correción por valores extremos = dB(A)</p>
                                                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                                            </div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>Ce =</th>
                                                        <td><?php $correc_prese_v_extremos=0.9023*$prom_medic2_fuente;
                                                             echo round($correc_prese_v_extremos,2);?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del valor &Delta;<sub>50</sub></u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form7.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>50fuente</sub> = N<sub>50</sub> Promedio de los 5 puntos de medición de fuente = dB(A)</p>
                                                <p>N<sub>50fondo</sub> = N<sub>50</sub> Promedio de los 5 puntos de medición de fondo = dB(A)</p>
                                            </div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>&Delta;<sub>50</sub></th>
                                                        <td><?php 
                                                            $determinacion_valor_delta=$prom_medic_fuente-$prom_medic_fonto;
                                                             echo round($determinacion_valor_delta,2);?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-12 text-center">Conclusión de &Delta;<sub>50</sub>:</div>
                                            <div class="col-md-12 text-center red"><?php if($determinacion_valor_delta>0.75){
                                                echo 'La Fuente Fija Emite Nivel Sonoro';
                                            }else{
                                                echo 'La Fuente Fija No Emite Nivel Sonoro';
                                            }?></div>
                                            <div class="col-md-12 text-center"><br></div>
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-12 text-center"><u>Corrección de Ruido por fondo (Cf):</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form8.jpg"></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>Cf=</th>
                                                        <td><?php 
                                                            if($determinacion_valor_delta>0.75){
                                                                $corre_ruido_fondo=(-($determinacion_valor_delta+9))+(3*(sqrt((4*$determinacion_valor_delta)-3)));
                                                                echo round($corre_ruido_fondo,1); 
                                                            }else{
                                                                $corre_ruido_fondo=0;
                                                                echo 'No Aplica';
                                                            }
                                                        ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección del N50 medio por extremos o Emisión de la fuente fija hacia la zona Crítica (N´<sub>50</sub>)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form9.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Ce = Correción por valores extremos = [dB(A)]</p>
                                            </div>
                                            <div class="col-md-12">
                                                <!--u130-->
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>N´<sub>50</sub> = </th>
                                                        <td><?php 
                                                            if($determinacion_valor_delta>0.75){
                                                                $correccion_n50_medio_externo=$prom_medic_fuente+$correc_prese_v_extremos;
                                                                echo round($correccion_n50_medio_externo,1);
                                                            }else{
                                                                $correccion_n50_medio_externo=0;
                                                                echo 'No aplica';
                                                            }?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            
                                            <div class="col-md-12 text-center"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del Nivel de Fuente Fija (N<sub>ff</sub>)</u></div>
                                            <div class="col-md-12 text-center">El Valor de Nivel de fuente fija es el mayor de los determinados N´<sub>50</sub> y Neq<sub>(eq)</sub></div>
                                            <div class="col-md-12">
                                                <!--ad135-->
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>N´<sub>50</sub> = </th>
                                                        <td><?php echo round($correccion_n50_medio_externo,1);?></td>
                                                        <td>dB(A)</td>
                                                        <td> </td>
                                                        <th>Neq<sub>(eq)</sub></th>
                                                        <td><?php echo round($ruido_fuente_neq,1)?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-12">
                                                <!--u137-->
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>N<sub>ff</sub> =</th>
                                                        <td><?php if($determinacion_valor_delta>0.75){
                                                            $deter_nff=max($correccion_n50_medio_externo,$ruido_fuente_neq);
                                                            echo round($deter_nff,1);
                                                        }else{
                                                            $deter_nff=0;
                                                            echo 'No aplica';
                                                        }?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            
                                            <div class="col-md-12 text-center"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección del Nivel de Fuente Fija por Ruido de Fondo (N´)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form10.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N´<sub>ff</sub> = Nivel de Fuente Fija Corregido por Ruido de Fondo = [dB(A)]</p>
                                                <p>N<sub>ff</sub> = Nivel de Fuente Fija = [dB(A)]</p>
                                                <p>C<sub>f</sub> = Correción por ruido de fondo = [dB(A)]</p>
                                            </div>
                                            <div class="col-md-12">
                                                <!--u146-->
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>(N`<sub>ff</sub>) = </th>
                                                        <td><?php 
                                                            if($determinacion_valor_delta>0.75){
                                                                $correccion_nivel_fuente_fija=$deter_nff+$corre_ruido_fondo;
                                                                echo round($correccion_nivel_fuente_fija,1);
                                                            }else{
                                                                $correccion_nivel_fuente_fija=0;
                                                                echo 'No aplica';
                                                            }?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            
                                            <div class="col-md-12 text-center"><br></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            
                                            <div class="col-md-8">
                                                <?php $hi=""; $hf=""; $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                                if($get_aisla->num_rows()>0){ $gtas=$get_aisla->row(); $hi=$gtas->hora_inicio; $hf=$gtas->hora_fin; }
                                                ?>
                                                <table class="table table-bordered tablecaptura ruido_aisla" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE AISLAMIENTO</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hi;?></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><?php echo $hf;?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>a</th>
                                                            <th>b</th>
                                                            <th>c</th>
                                                            <th>d</th>
                                                            <th>e</th>
                                                        </tr>
                                                        <?php $cont=0;
                                                        if($get_aisla->num_rows()>0){
                                                            $b_array_lectura_a=[];
                                                            $b_array_lectura_b=[];
                                                            $b_array_lectura_c=[];
                                                            $b_array_lectura_d=[];
                                                            $b_array_lectura_e=[];
                                                            foreach ($get_aisla->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput"><?php echo $cont;?>
                                                                    </td>
                                                                    <td class="tdinput"><?php echo $f->a; $b_array_lectura_a[]=$f->a;?></td>
                                                                    <td class="tdinput"><?php echo $f->b; $b_array_lectura_b[]=$f->b;?></td>
                                                                    <td class="tdinput"><?php echo $f->c; $b_array_lectura_c[]=$f->c;?></td>
                                                                    <td class="tdinput"><?php echo $f->d; $b_array_lectura_d[]=$f->d;?></td>
                                                                    <td class="tdinput"><?php echo $f->e; $b_array_lectura_e[]=$f->e;?></td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del nivel Percentil 50 (N<sub>50</sub>) de cada punto de medición de fuente y fondo. (PROMEDIO)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form1.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>n =  Número de observaciones en el punto i = 35</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered" style="width:auto;margin-left:auto; margin-right: auto;">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6">Mediciones en eñ interior de la fuente</th>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>Prom.</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>50</sub></th>
                                                        <td><?php 
                                                                echo '<!--'.array_sum($b_array_lectura_a).'-->';
                                                                $b_medicion_fu_a = array_sum($b_array_lectura_a)/count($b_array_lectura_a); 
                                                                echo round($b_medicion_fu_a, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu_b = array_sum($b_array_lectura_b)/count($b_array_lectura_b); 
                                                                echo round($b_medicion_fu_b, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu_c = array_sum($b_array_lectura_c)/count($b_array_lectura_c); 
                                                                echo round($b_medicion_fu_c, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu_d = array_sum($b_array_lectura_d)/count($b_array_lectura_d); 
                                                                echo round($b_medicion_fu_d, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu_e = array_sum($b_array_lectura_e)/count($b_array_lectura_e); 
                                                                echo round($b_medicion_fu_e, 1);
                                                                 ?></td>
                                                        <td><?php $b_prom_medic_fuente=($b_medicion_fu_a+$b_medicion_fu_b+$b_medicion_fu_c+$b_medicion_fu_d+$b_medicion_fu_e)/5;
                                                            echo round($b_prom_medic_fuente,1);
                                                            ?></td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <?php $cont=0;
                                            if($get_aisla->num_rows()>0){
                                                $b_array_lectura_a_pow=[];
                                                $b_array_lectura_b_pow=[];
                                                $b_array_lectura_c_pow=[];
                                                $b_array_lectura_d_pow=[];
                                                $b_array_lectura_e_pow=[];

                                                $b_array_lectura_a_pow_10=[];
                                                $b_array_lectura_b_pow_10=[];
                                                $b_array_lectura_c_pow_10=[];
                                                $b_array_lectura_d_pow_10=[];
                                                $b_array_lectura_e_pow_10=[];
                                                foreach ($get_aisla->result() as $f) { 
                                                    $b_array_lectura_a_pow[]=pow(($f->a-$medicion_fu_a),2);
                                                    $b_array_lectura_b_pow[]=pow(($f->b-$b_medicion_fu_b),2);
                                                    $b_array_lectura_c_pow[]=pow(($f->c-$b_medicion_fu_c),2);
                                                    $b_array_lectura_d_pow[]=pow(($f->d-$b_medicion_fu_d),2);
                                                    $b_array_lectura_e_pow[]=pow(($f->e-$b_medicion_fu_e),2);

                                                    $b_array_lectura_a_pow_10[]=pow(10,($f->a/10));
                                                    $b_array_lectura_b_pow_10[]=pow(10,($f->b/10));
                                                    $b_array_lectura_c_pow_10[]=pow(10,($f->c/10));
                                                    $b_array_lectura_d_pow_10[]=pow(10,($f->d/10));
                                                    $b_array_lectura_e_pow_10[]=pow(10,($f->e/10));
                                                }
                                            }
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación de la desviación estándar (&sigma;) de cada punto de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form2.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>n =  Número de observaciones en el punto i = 35</p>
                                                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="7"> Mediciones en el interior de la fuente</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>a</th>
                                                        <th>b</th>
                                                        <th>c</th>
                                                        <th>d</th>
                                                        <th>e</th>
                                                        <th>Prom.</th>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <th>&sigma;</th>
                                                        <td><?php //P211
                                                                    if($b_medicion_fu_a>0){
                                                                        $b_medicion_fu2_a_pow = pow(array_sum($b_array_lectura_a_pow)/(count($b_array_lectura_a_pow)-1),0.5);
                                                                        echo round($b_medicion_fu2_a_pow, 1);
                                                                    }else{
                                                                        $b_medicion_fu2_a_pow=0;
                                                                        echo '0';
                                                                    }
                                                                
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu2_b_pow = pow(array_sum($b_array_lectura_b_pow)/(count($b_array_lectura_b_pow)-1),0.5);
                                                                echo round($b_medicion_fu2_b_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu2_c_pow = pow(array_sum($b_array_lectura_c_pow)/(count($b_array_lectura_c_pow)-1),0.5);
                                                                echo round($b_medicion_fu2_c_pow, 1).'<br>';
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu2_d_pow = pow(array_sum($b_array_lectura_d_pow)/(count($b_array_lectura_d_pow)-1),0.5);
                                                                echo round($b_medicion_fu2_d_pow, 1);
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu2_e_pow = pow(array_sum($b_array_lectura_e_pow)/(count($b_array_lectura_e_pow)-1),0.5);
                                                                echo round($b_medicion_fu2_e_pow, 1).'<br>';
                                                                 ?></td>
                                                        <td><?php $b_prom_medic2_fuente=($b_medicion_fu2_a_pow+$b_medicion_fu2_b_pow+$b_medicion_fu2_c_pow+$b_medicion_fu2_d_pow+$b_medicion_fu2_e_pow)/5;
                                                            echo round($b_prom_medic2_fuente,1);
                                                            ?></td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del N<sub>10</sub> de cada punto y de la Zona Crítica de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form3.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N<sub>10</sub> = Percentil 10 del punto i = dB(A)</p>
                                                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                                                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6"> Mediciones en el interior de la fuente</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>a</th>
                                                        <th>b</th>
                                                        <th>c</th>
                                                        <th>d</th>
                                                        <th>e</th>
                                                        <th>Prom.</th>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>10</sub></th>
                                                        <td><?php 
                                                                $b_medicion_fu3_a_pow = $b_medicion_fu_a+(1.2817*$b_medicion_fu2_a_pow);
                                                                echo round($b_medicion_fu3_a_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu3_b_pow = $b_medicion_fu_b+(1.2817*$b_medicion_fu2_b_pow);
                                                                echo round($b_medicion_fu3_b_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu3_c_pow = $b_medicion_fu_c+(1.2817*$b_medicion_fu2_c_pow);
                                                                echo round($b_medicion_fu3_c_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu3_d_pow = $b_medicion_fu_d+(1.2817*$b_medicion_fu2_d_pow);
                                                                echo round($b_medicion_fu3_d_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu3_e_pow = $b_medicion_fu_e+(1.2817*$b_medicion_fu2_e_pow);
                                                                echo round($b_medicion_fu3_e_pow,1); 
                                                                 ?></td>
                                                        <td><?php $b_prom_medic3_fuente=($b_medicion_fu3_a_pow+$b_medicion_fu3_b_pow+$b_medicion_fu3_c_pow+$b_medicion_fu3_d_pow+$b_medicion_fu3_e_pow)/5;
                                                            echo round($b_prom_medic3_fuente,1);
                                                            ?></td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del Nivel Equivalente (N<sub>eqi</sub>) de cada punto de medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form4.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Neq<sub>i</sub> = Nivel equivalente del punto i = dB(A)</p>
                                                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>m = Número de observaciones en el punto i = 35</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                            <div class="col-md-12 text-center">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th></th>
                                                        <th colspan="6"> Mediciones en el interior de la fuente</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Punto:</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>eqi</sub></th>
                                                        <td><?php 
                                                                $b_medicion_fu4_a_pow =10*(log10(array_sum($b_array_lectura_a_pow_10)/count($b_array_lectura_a_pow_10)));
                                                                echo round($b_medicion_fu4_a_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu4_b_pow = 10*(log10(array_sum($b_array_lectura_b_pow_10)/count($b_array_lectura_b_pow_10)));
                                                                echo round($b_medicion_fu4_b_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu4_c_pow = 10*(log10(array_sum($b_array_lectura_c_pow_10)/count($b_array_lectura_c_pow_10)));
                                                                echo round($b_medicion_fu4_c_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu4_d_pow = 10*(log10(array_sum($b_array_lectura_d_pow_10)/count($b_array_lectura_d_pow_10)));
                                                                echo round($b_medicion_fu4_d_pow,1); 
                                                                 ?></td>
                                                        <td><?php 
                                                                $b_medicion_fu4_e_pow = 10*(log10(array_sum($b_array_lectura_e_pow_10)/count($b_array_lectura_e_pow_10)));
                                                                echo round($b_medicion_fu4_e_pow,1); 
                                                                 ?></td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del Nivel Equivalente de los niveles equivalentes Neq<sub>(eq)</sub> o Nivel equivalente de la Zona Critica para la medición de fuente y fondo.</u></div>
                                            <div class="col-md-6 text-center">
                                                <table class="table table-bordered" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th colspan="2">Aislamiento</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">10^(Neq/10)</th>
                                                    </tr>
                                                    <tr><th>a</th><td><?php if($b_medicion_fu4_a_pow>0){
                                                                                $b_nivel_equivalente_a=pow(10,($b_medicion_fu4_a_pow/10));
                                                                                echo round($b_nivel_equivalente_a,4);
                                                                            }else{
                                                                                $b_nivel_equivalente_a=0;
                                                                                echo '--';
                                                                            }
                                                                            
                                                                        ?></td></tr>
                                                    <tr><th>b</th><td><?php if($b_medicion_fu4_b_pow>0){
                                                                                $b_nivel_equivalente_b=pow(10,($b_medicion_fu4_b_pow/10));
                                                                                echo round($b_nivel_equivalente_b,4);
                                                                            }else{
                                                                                $b_nivel_equivalente_b=0;
                                                                                echo '--';
                                                                            }?></td></tr>
                                                    <tr><th>c</th><td><?php if($b_medicion_fu4_c_pow>0){
                                                                            $b_nivel_equivalente_c=pow(10,($b_medicion_fu4_c_pow/10));
                                                                            echo round($b_nivel_equivalente_c,4);
                                                                        }else{
                                                                            $b_nivel_equivalente_c=0;
                                                                            echo '--';
                                                                        } 
                                                                        
                                                                        ?></td></tr>
                                                    <tr><th>d</th><td><?php if($b_medicion_fu4_d_pow>0){
                                                                            $b_nivel_equivalente_d=pow(10,($b_medicion_fu4_d_pow/10));
                                                                            echo round($b_nivel_equivalente_d,4);
                                                                        }else{
                                                                            $b_nivel_equivalente_d=0;
                                                                            echo '--';
                                                                        } 
                                                                        ?></td></tr>
                                                    <tr><th>e</th><td><?php if($b_medicion_fu4_e_pow>0){
                                                                            $b_nivel_equivalente_e=pow(10,($b_medicion_fu4_e_pow/10));
                                                                            echo round($b_nivel_equivalente_e,4);
                                                                        }else{
                                                                            $b_nivel_equivalente_e=0;
                                                                            echo '--';
                                                                        }
                                                                        ?></td></tr>
                                                    <tr><td></td><td><?php $b_nivel_equivalente_fuente=($b_nivel_equivalente_a+$b_nivel_equivalente_b+$b_nivel_equivalente_c+$b_nivel_equivalente_d+$b_nivel_equivalente_e)/5;
                                                                        echo round($b_nivel_equivalente_fuente,4);?></td></tr>
                                                </table>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form12.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>Nd = Nivel equivalente medido en el interior de la fuente fija  = dB(A)</p>
                                                <p>Nk = Nivel Sonoro A en la observación i = dB(A)</p>
                                                <p>k = puntos de medición en el interior de la fuente fija = 5</p>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>Nd = </th><td><?php $b_ruido_fuente_neq=10*log10($b_nivel_equivalente_fuente); 
                                                            echo round($b_ruido_fuente_neq,1)?></td><td>dB(A)</td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación de la Reducción Acústica (R)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form11.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>R = Reducción Acústica del elemento común = dB</p>
                                                <p>Nd = Nivel equivalente medido en el interior de la fuente fija  = dB(A)</p>
                                                <p>Neq(<sub>eq</sub>) = Nivel equivalente de la Zona Critica = dB(A)</p>
                                                <p>S = Área del elemento común = [m<sup>2</sup>]</p>
                                                <p>10 = Absorción acústica normalizada del recinto receptor</p>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>s =</th>
                                                        <td><?php echo '--'; $s_deter_de_la_reduccion=1;?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>R =</th>
                                                        <td><?php 
                                                            if($b_ruido_fuente_neq>0){
                                                                $deter_reduc_acustica=$b_ruido_fuente_neq-$ruido_fuente_neq+(10*log10($s_deter_de_la_reduccion/10));
                                                                if($deter_reduc_acustica>0){
                                                                    echo round($deter_reduc_acustica,1);    
                                                                }else{
                                                                    echo '--';
                                                                }
                                                            }else{
                                                                $deter_reduc_acustica=0;
                                                                echo 'No aplica';
                                                            }
                                                        ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección del Nivel de fuente fija por la existencia de un elemento constructivo Total (N´´ff)</u></div>
                                            <div class="col-md-6 text-right"><img src="<?php echo base_url();?>public/img/81_form13.jpg"></div>
                                            <div class="col-md-6">
                                                <p>Donde:</p>
                                                <p>N´´<sub>ff</sub> = Valor de fuente fija corregido por aislamiento de un elemento constructivo = dB</p>
                                                <p>N´<sub>ff</sub> = Nivel de Fuente Fija Corregido por Ruido de Fondo = dB(A)</p>
                                                <p>R = Reducción Acústica del elemento común = dB</p>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <!--u276-->
                                                <table class="table" style="width:auto;margin-left: auto; margin-right: auto;">
                                                    <tr>
                                                        <th>N´´<sub>ff</sub> =</th>
                                                        <td><?php 
                                                            if($b_ruido_fuente_neq>0){
                                                                $correccion_nivel_fuente_fija_nff=$correccion_nivel_fuente_fija+(0.5*$deter_reduc_acustica);
                                                                if($correccion_nivel_fuente_fija_nff>0){
                                                                    echo round($correccion_nivel_fuente_fija_nff,1);    
                                                                }else{
                                                                    echo '--';
                                                                }
                                                                
                                                            }else{
                                                                $correccion_nivel_fuente_fija_nff=0;
                                                                echo 'No aplica';
                                                            }
                                                        ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>Mediciones de Ruido de fuente</p>
                                                <table class="table table-bordered" style="width:auto">
                                                    <tr>
                                                        <th>Nivel</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                        <th>Promedio /  Neq<sub>(eq)</sub></th>
                                                        <th>Unidad</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>50</sub></th>
                                                        <td><?php echo round($medicion_fu_a,1);?></td>
                                                        <td><?php echo round($medicion_fu_b,1);?></td>
                                                        <td><?php echo round($medicion_fu_c,1);?></td>
                                                        <td><?php echo round($medicion_fu_d,1);?></td>
                                                        <td><?php echo round($medicion_fu_e,1);?></td>
                                                        <td><?php echo round($prom_medic_fuente,1);?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>10</sub></th>
                                                        <td><?php echo round($medicion_fu3_a_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu3_b_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu3_c_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu3_d_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu3_e_pow,1); ?></td>
                                                        <td><?php echo round($prom_medic3_fuente,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>&sigma;fuente</th>
                                                        <td><?php echo round($medicion_fu2_a_pow,1);?></td>
                                                        <td><?php echo round($medicion_fu2_b_pow,1);?></td>
                                                        <td><?php echo round($medicion_fu2_c_pow,1);?></td>
                                                        <td><?php echo round($medicion_fu2_d_pow,1);?></td>
                                                        <td><?php echo round($medicion_fu2_e_pow,1);?></td>
                                                        <td><?php echo round($prom_medic2_fuente,1);?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <th>N<sub>eq</sub></th>
                                                        <td><?php echo round($medicion_fu4_a_pow,1) ?></td>
                                                        <td><?php echo round($medicion_fu4_b_pow,1) ?></td>
                                                        <td><?php echo round($medicion_fu4_c_pow,1) ?></td>
                                                        <td><?php echo round($medicion_fu4_d_pow,1) ?></td>
                                                        <td><?php echo round($medicion_fu4_e_pow,1) ?></td>
                                                        <td><?php echo round($ruido_fuente_neq,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                                <p>Mediciones de Ruido de fondo</p>
                                                <table class="table table-bordered" style="width:auto">
                                                    <tr>
                                                        <th>Nivel</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                        <th>Promedio /  Neq(eq)</th>
                                                        <th>Unidad</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>50</sub></th>
                                                        <td><?php echo round($medicion_fu_1,2);?></td>
                                                        <td><?php echo round($medicion_fu_2,2);?></td>
                                                        <td><?php echo round($medicion_fu_3,2);?></td>
                                                        <td><?php echo round($medicion_fu_4,2);?></td>
                                                        <td><?php echo round($medicion_fu_5,2);?></td>
                                                        <td><?php echo round($prom_medic_fonto,2);?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>10</sub></th>
                                                        <td><?php echo round($medicion_fu3_1_pow,3); ?></td>
                                                        <td><?php echo round($medicion_fu3_2_pow,3); ?></td>
                                                        <td><?php echo round($medicion_fu3_3_pow,3); ?></td>
                                                        <td><?php echo round($medicion_fu3_4_pow,3); ?></td>
                                                        <td><?php echo round($medicion_fu3_5_pow,3); ?></td>
                                                        <td><?php echo round($prom_medic3_fonto,3); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>&sigma; Fondo</th>
                                                        <td><?php echo round($medicion_fu2_1_pow,2) ?></td>
                                                        <td><?php echo round($medicion_fu2_2_pow,2) ?></td>
                                                        <td><?php echo round($medicion_fu2_3_pow,2) ?></td>
                                                        <td><?php echo round($medicion_fu2_4_pow,2) ?></td>
                                                        <td><?php echo round($medicion_fu2_5_pow,2) ?></td>
                                                        <td><?php echo round($prom_medic2_fonto,2) ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <th>N<sub>eq</sub></th>
                                                        <td><?php echo round($medicion_fu4_1_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu4_2_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu4_3_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu4_4_pow,1); ?></td>
                                                        <td><?php echo round($medicion_fu4_5_pow,1); ?></td>
                                                        <td><?php echo round($ruido_fondo_neq,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                                <p>Mediciones para cálculo de reducción acústica</p>
                                                <table class="table table-bordered" style="width:auto">
                                                    <tr>
                                                        <th>Nivel</th>
                                                        <th>a</th>
                                                        <th>b</th>
                                                        <th>c</th>
                                                        <th>d</th>
                                                        <th>e</th>
                                                        <th>Promedio /Nd</th>
                                                        <th>Unidad</th>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>50</sub></th>
                                                        <td><?php echo round($b_medicion_fu_a,1) ?></td>
                                                        <td><?php echo round($b_medicion_fu_b,1) ?></td>
                                                        <td><?php echo round($b_medicion_fu_c,1) ?></td>
                                                        <td><?php echo round($b_medicion_fu_d,1) ?></td>
                                                        <td><?php echo round($b_medicion_fu_e,1) ?></td>
                                                        <td><?php echo round($b_prom_medic_fuente,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>10</sub></th>
                                                        <td><?php echo round($b_medicion_fu3_a_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu3_b_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu3_c_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu3_d_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu3_e_pow,1); ?></td>
                                                        <td><?php echo round($b_prom_medic3_fuente,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>&sigma;<sub>Ais.</sub></th>
                                                        <td><?php echo round($b_medicion_fu2_a_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu2_b_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu2_c_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu2_d_pow,1); ?></td>
                                                        <td><?php echo round($b_medicion_fu2_e_pow,1); ?></td>
                                                        <td><?php echo round($b_prom_medic2_fuente,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                    <tr>
                                                        <th>N<sub>d</sub></th>
                                                        <td><?php echo round($b_medicion_fu4_a_pow,1); ?> </td>
                                                        <td><?php echo round($b_medicion_fu4_b_pow,1); ?> </td>
                                                        <td><?php echo round($b_medicion_fu4_c_pow,1); ?> </td>
                                                        <td><?php echo round($b_medicion_fu4_d_pow,1); ?> </td>
                                                        <td><?php echo round($b_medicion_fu4_e_pow,1); ?> </td>
                                                        <td><?php echo round($b_ruido_fuente_neq,1); ?> </td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>

                                            </div>
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección por Ruido de Fondo</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>&Delta;50 =</th>
                                                        <td><?php echo round($determinacion_valor_delta,2); ?></td>
                                                        <td>[dB(A)]</td>
                                                        <td></td>
                                                        <th>Cf =</th>
                                                        <td><?php echo round($corre_ruido_fondo,1); ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección por presencia de extremos:</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>Ce =</th>
                                                        <td><?php echo round($correc_prese_v_extremos,2);?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Correción por aislamiento:</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>R =</th>
                                                        <td><?php echo round($deter_reduc_acustica,1);?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Emisión de la fuente fija hacia la Zona Crítica:</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>N´<sub>50</sub> = </th>
                                                        <td><?php echo round($correccion_n50_medio_externo,1);?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Determinación del mayor de N´<sub>50</sub> y (Neq)<sub>eq</sub> de la fuente y obtener N<sub>ff</sub>:</u></div>
                                            <div class="col-md-6">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>N´<sub>50</sub> = </th>
                                                        <td><?php echo round($correccion_n50_medio_externo,1);?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>(Neq)<sub>eq</sub> = </th>
                                                        <td><?php echo round($ruido_fuente_neq,1)?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>N<sub>ff</sub> = </th>
                                                        <td><?php echo round($deter_nff,1); ?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección por Ruido de Fondo</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>(N`<sub>ff</sub>) =</th>
                                                        <td><?php echo round($correccion_nivel_fuente_fija,1);?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>Corrección por Aislamiento</u></div>
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>N´´<sub>ff</sub> = </th>
                                                        <td><?php echo round($correccion_nivel_fuente_fija_nff,1); ?></td>
                                                        <td>dB(A)</td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <td>En la Zona Crítica No:</td>
                                                        <td colspan="2"><?php echo $zona_critica; ?></td>
                                                                                                                
                                                    </tr>
                                                    <tr>
                                                        <th>Presenta un valor de Nivel de Emisión de:</th>
                                                        <td><?php 
                                                            if($correccion_nivel_fuente_fija>0.75){
                                                                $corre_aslamiento=max($correccion_nivel_fuente_fija_nff,$correccion_nivel_fuente_fija);
                                                                echo round($corre_aslamiento,1);
                                                            }else{
                                                                $corre_aslamiento=0;
                                                                echo '0.00<br><span class="red">LA FUENTE FIJA NO EMITE NIVEL SONORO</span>';
                                                            }
                                                            $this->ModeloCatalogos->updateCatalogo('nom81',array('emision_ruido'=>$corre_aslamiento),array('idnom'=>$idnom));
                                                        ?></td>
                                                        <td>dB(A)</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center"><u>ESTIMACIÓN DE LA INCERTIDUMBRE PARA EL RUIDO DE FUENTE</u></div>
                                            <div class="col-md-12">Promedio de las lecturas del ruido de fuente y fondo</div>
                                            <div class="col-md-12">
                                                <table class="table table-bordered" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th colspan="5">Fuente</th>
                                                    </tr>
                                                    <tr>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                    </tr>
                                                    <tr>
                                                        <th>[dB(A)]</th>
                                                        <th>[dB(A)]</th>
                                                        <th>[dB(A)]</th>
                                                        <th>[dB(A)]</th>
                                                        <th>[dB(A)]</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo round($medicion_fu_a,1); ?></td>
                                                        <td><?php echo round($medicion_fu_b,1); ?></td>
                                                        <td><?php echo round($medicion_fu_c,1); ?></td>
                                                        <td><?php echo round($medicion_fu_d,1); ?></td>
                                                        <td><?php echo round($medicion_fu_e,1); ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">Desviación estándar de las mediciones <b><?php 
                                                    $desviacion_estandar=$this->ModeloCatalogos->desvest(array($medicion_fu_a,$medicion_fu_b,$medicion_fu_c,$medicion_fu_d,$medicion_fu_e));
                                                    echo round($desviacion_estandar,1);
                                                ?></b> </div>
                                            <div class="col-md-12">
                                                <table class="table table-bordered text-center" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <td colspan="4">Componentes de Incertidumbres</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Incertidumbre tipo A, por las lecturas, Repetibilidad en dB</th>
                                                        <th>Resolución del sonometro (dB)</th>
                                                        <th>Certificado de calibración del sonometro (dB)</th>
                                                        <th>Certificado de calibración del calibrador acústico (dB)</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php 
                                                            $inser_tipo_a=(1/sqrt(10))*$desviacion_estandar;
                                                            echo $inser_tipo_a;
                                                        ?></td>
                                                        <td><?php $resolucion_sonometro=0.1;
                                                                    echo $resolucion_sonometro;?></td>
                                                        <td><?php $certif_calibracion_sonometro=0.11;
                                                                    echo $certif_calibracion_sonometro;?></td>
                                                        <td><?php $certif_calibracion_calibrador_acustico=0.20;
                                                                    echo $certif_calibracion_calibrador_acustico;?></td>
                                                    </tr>
                                                    <tr><th colspan="4">dB convertidos a valores relativos</th></tr>
                                                    <tr>
                                                        <td><?php $v_relativo_inser_tipo_a=pow(10,$inser_tipo_a/20)-1;
                                                                echo $v_relativo_inser_tipo_a;
                                                        ?></td>
                                                        <td><?php $v_relativo_resolucion_sonometro=pow(10,$resolucion_sonometro/20)-1;
                                                                echo $v_relativo_resolucion_sonometro;?></td>
                                                        <td><?php $v_relativo_certif_calibracion_sonometro=pow(10,$certif_calibracion_sonometro/20)-1;
                                                                echo $v_relativo_certif_calibracion_sonometro;?></td>
                                                        <td><?php $v_relativo_certif_calibracion_calibrador_acustico=pow(10,$certif_calibracion_calibrador_acustico/20)-1;
                                                                echo $v_relativo_certif_calibracion_calibrador_acustico;?></td>
                                                    </tr>
                                                </table>
                                            </div>    
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                            <div class="col-md-12">
                                                <table class="table table-bordered text-center" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>Fuentes de Incertidumbre</th>
                                                        <th>Incertidumbre estándar (rel)</th>
                                                        <th>Tipo de Distribución</th>
                                                        <th>Incertidumbre estándar u(xi)  [rel]</th>
                                                        <th>Coeficiente de sensibilidad Ci</th>
                                                        <th>Contribución ui(y)  [rel]</th>
                                                        <th>(ui(y))2    [rel2]</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Lecturas</th>
                                                        <td><?php echo $v_relativo_inser_tipo_a;?></td>
                                                        <td>A, normal k=1</td>
                                                        <td><?php $ie_v_relativo_inser_tipo_a=$v_relativo_inser_tipo_a/1;
                                                            echo $ie_v_relativo_inser_tipo_a;?></td>
                                                        <td><?php echo $fi_lecturas=1;?></td>
                                                        <td><?php $contribucion_l=$ie_v_relativo_inser_tipo_a*$fi_lecturas;
                                                                echo $contribucion_l;?></td>
                                                        <td><?php $contribucion_l_pow=pow($contribucion_l,2);
                                                                echo round($contribucion_l_pow,5);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Resolución</th>
                                                        <td><?php echo $v_relativo_resolucion_sonometro;?></td>
                                                        <td>B, k=1</td>
                                                        <td><?php $ie_v_relativo_resolucion_sonometro=$v_relativo_resolucion_sonometro/sqrt(12);
                                                            echo $ie_v_relativo_resolucion_sonometro;?></td>
                                                        <td><?php echo $fi_resolucion=1;?></td>
                                                        <td><?php $contribucion_r=$ie_v_relativo_resolucion_sonometro*$fi_resolucion;
                                                            echo $contribucion_r;?></td>
                                                        <td><?php $contribucion_r_pow=pow($contribucion_r,2);
                                                                echo round($contribucion_r_pow,5);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sonómetro</th>
                                                        <td><?php echo $v_relativo_certif_calibracion_sonometro;?></td>
                                                        <td>B, k=2</td>
                                                        <td><?php $ie_v_relativo_certif_calibracion_sonometro=$v_relativo_certif_calibracion_sonometro/2;
                                                            echo $ie_v_relativo_certif_calibracion_sonometro;?></td>
                                                        <td><?php echo $fi_sonometro=1;?></td>
                                                        <td><?php $contribucion_s=$ie_v_relativo_certif_calibracion_sonometro*$fi_sonometro;
                                                            echo $contribucion_s;?></td>
                                                        <td><?php $contribucion_s_pow=pow($contribucion_s,2);
                                                                echo round($contribucion_s_pow,5);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Calibrador</th>
                                                        <td><?php echo $v_relativo_certif_calibracion_calibrador_acustico;?></td>
                                                        <td>B, k=2</td>
                                                        <td><?php $ie_v_relativo_certif_calibracion_calibrador_acustico=$v_relativo_certif_calibracion_calibrador_acustico/2;
                                                            echo $ie_v_relativo_certif_calibracion_calibrador_acustico;?></td>
                                                        <td><?php echo $fi_calibrador=1;?></td>
                                                        <td><?php $contribucion_c=$ie_v_relativo_certif_calibracion_calibrador_acustico*$fi_calibrador;
                                                            echo $contribucion_c;?></td>
                                                        <td><?php $contribucion_c_pow=pow($contribucion_c,2);
                                                                echo round($contribucion_c_pow,5);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6"></td>
                                                        <td><?php $fuentes_insertidumbre_sum=$contribucion_l_pow+$contribucion_r_pow+$contribucion_s_pow+$contribucion_c_pow; 
                                                            echo round($fuentes_insertidumbre_sum,4); ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <table class="table table-bordered text-center" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr>
                                                        <th>uc(rel)</th>
                                                        <td><?php $fu_in_sum_pow=sqrt($fuentes_insertidumbre_sum);
                                                            echo round($fu_in_sum_pow,5); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>U(rel)</th>
                                                        <td><?php $fu_in_sum_pow_u_rel=$fu_in_sum_pow*2;
                                                            echo round($fu_in_sum_pow_u_rel,5); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>U(rel) convertida a dB</th>
                                                        <td><?php $fu_in_log=20*log10(1+$fu_in_sum_pow_u_rel); 
                                                        echo round($fu_in_log,2); ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <table class="table table-bordered text-center" style="width:auto;margin-left:auto; margin-right:auto;">
                                                    <tr><td>U (dB)</td><td><?php echo round($fu_in_log,2); ?></td></tr>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <p>Factor de cobertura k=2</p>
                                                <p>Nivel de Confianza 95.45%</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7">
                                                <canvas id="myChartgtgb_<?php echo $idnom;?>" height="130"></canvas>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                        $(document).ready(function($) {
                                           graficageneral<?php echo $idnom;?>(); 
                                        });
                                        function graficageneral<?php echo $idnom;?>(){
                                            var ctx = document.getElementById('myChartgtgb_<?php echo $idnom;?>').getContext('2d');
                                            var barChart = new Chart(ctx, {
                                              type: 'bar',
                                              data: {
                                                labels: [""],
                                                datasets: [
                                                            {
                                                              label: 'EMISION DE RUIDO ',
                                                              data: [<?php echo round($corre_aslamiento,1); ?>],
                                                              backgroundColor: "#2196f3"
                                                            }, {
                                                              label: 'LMP',
                                                              data: [<?php if($horario==1){ echo '68';}else{ echo '65';}?>],
                                                              backgroundColor: "#ff9800"
                                                            },

                                                        ]
                                              },
                                              options: {
                                                title: {
                                                    display: true,
                                                    text: 'EVALUACIÓN <?php if($horario==1){ echo 'DIURNO';}else{ echo 'NOCTURNO';}?>',
                                                    fontSize:18,
                                                    position:'top'
                                                },
                                                scales: {
                                                  yAxes: [{
                                                    ticks: {
                                                      beginAtZero: true
                                                    }
                                                  }]
                                                }
                                              }
                                            });
                                    }
                                    setTimeout(function(){
                                        exportarpunto(<?php echo $idnom ;?>);
                                    }, <?php echo $rowpuntoset;?>);
                                    </script>


                                



                                    
                                    </td>
                                </tr>
                            <?php
                                    $rowpuntoset=$rowpuntoset+100; 
                                } ?>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script type="text/javascript">
    function exportarpunto(punto){
        var base_url = $('#base_url').val();
        var canvasx= document.getElementById("myChartgtgb_"+punto);
        var dataURL = canvasx.toDataURL('image/png', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/nom84viewinserupdatechart',
            data: {
                  id:punto,
                  grafica:dataURL
            },
            statusCode:{
              404: function(data){
                  
              },
              500: function(){
                  
              }
          },
          success:function(data){
              console.log("hecho");
          }
      });
    }
</script>