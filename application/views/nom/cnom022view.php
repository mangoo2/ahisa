<link href="<?php echo base_url(); ?>public/boostrap/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="<?php echo base_url(); ?>public/boostrap/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
<!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>plugins/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/Chart.js/chartjs-plugin-datalabels.js"></script>

<style type="text/css">
    .titulos {
        background: #E9ECEF;
        font-weight: bold;
    }

    .titulos-12 {
        font-size: 12px;
    }

    .camposi input {
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }

    .camposi {
        padding: 0px !important;
    }

    .padding0 {
        padding-right: 0px !important;
        padding-left: 0px !important;
    }

    .tdinput {
        padding: 6px !important;
    }

    .tdinput input {
        padding: 6px !important;
    }

    table th,
    table td {
        text-align: center;
    }

    .tablev {
        width: auto;
        margin-left: auto;
        margin-right: auto;
    }

    .tablev th {
        background-color: rgb(217, 217, 217);
    }

    .tablev th,
    .tablev td {
        border: 1px solid rgb(128, 128, 128);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body" style="width: 80%;">
                    <div class="card-block">
                        <table class="table table-bordered tablev">
                            <tr>
                                <th>No. de Informe</th>
                                <td><?php echo $num_informe; ?></td>
                            </tr>
                            <tr>
                                <th>Razon Social</th>
                                <td><?php echo $razon_social; ?></td>
                            </tr>
                            <tr>
                                <th>Fecha</th>
                                <td><?php echo $fecha; ?></td>
                            </tr>
                        </table>

                        <?php
                        //traer los del equipo
                        /*$unidad2=2; $inter2=0.0203; $pend2=1.0014;
                            $unidad20=20; $inter20=0.0070; $pend20=0.9977;
                            $unidad200=200; $inter200=-0.0256; $pend200=0.9967;
                            $unidad2000=2000; $inter2000=-2.2912; $pend2000=1.0059;
                            $unidad20000=20000; $inter20000=-25.9980; $pend20000=1.0069;*/
                        $unidad2 = 2;
                        $inter2 = 0;
                        $pend2 = 0;
                        $unidad20 = 20;
                        $inter20 = 0;
                        $pend20 = 0;
                        $unidad200 = 200;
                        $inter200 = 0;
                        $pend200 = 0;
                        $unidad2000 = 2000;
                        $inter2000 = 2;
                        $pend2000 = 0;
                        $unidad20000 = 20000;
                        $inter20000 = 0;
                        $pend20000 = 0;
                        $factor_corprom = 0;
                        $rowpuntostr = 1;
                        $rowpuntos = $this->ModeloCatalogos->getselectwherenOrder('nom22d', array('idnom' => $idnom, "r1>" => 0, "area!=" => "", 'activo' => 1), "punto", "asc");
                        //log_message('error','QUERYGraph: '.$this->db->last_query());
                        //log_message('error','RowPuntosGraph: '.json_encode($rowpuntos->result()));

                        if ($id_medidor > 0) {
                            $get_med = $this->ModeloCatalogos->getselectwheren('equipo_medidor_detalle', array('id_equipo' => $id_medidor));
                            //log_message('error','RESULTS: '.json_encode($resultnorma->result()));
                            foreach ($get_med->result() as $m) {
                                log_message('error','$m->unidad:'.$m->unidad.' $m->line:'.$m->line);// revisar en base de datos la unidad y duplicado de datos http://localhost/ahisa/Nom/add22view/54
                                if ($m->unidad == 2 && $m->line == 5) {
                                    $unidad2 = 2;
                                    $inter2 = $m->interseccion;
                                    $pend2 = $m->pendiente;
                                }
                                if ($m->unidad == 20 && $m->line == 5) {
                                    $unidad20 = 20;
                                    $inter20 = $m->interseccion;
                                    $pend20 = $m->pendiente;
                                }
                                if ($m->unidad == 200 && $m->line == 5) {
                                    $unidad200 = 200;
                                    $inter200 = $m->interseccion;
                                    $pend200 = $m->pendiente;
                                }
                                if ($m->unidad == 2000 && $m->line == 5) {
                                    $unidad2000 = 2000;
                                    $inter2000 = $m->interseccion;
                                    $pend2000 = $m->pendiente;
                                }
                                if ($m->unidad == 20000 && $m->line == 4) {
                                    $unidad20000 = 20000;
                                    $inter20000 = $m->interseccion;
                                    $pend20000 = $m->pendiente;
                                }
                                log_message('error', 'pend2: '.$pend2);
                                log_message('error', 'inter2: '.$inter2);

                                log_message('error', 'unidad20000: '.$unidad20000);
                                    log_message('error', 'inter20000: '.$inter20000);
                                    log_message('error', 'pend20000: '.$pend20000);
                                    log_message('error', 'unidad20000: '.$unidad20000);
                                    log_message('error', 'line: '.$m->line);
                            }
                        }
                        //log_message('error', 'id_higro: '.$id_higro);
                        $tot = array();
                        $factor_corprom = 0;
                        if ($id_higro > 0) {
                            $get_hidro = $this->ModeloCatalogos->getselectwheren('equipo_higro_detalle', array('id_equipo' => $id_higro));
                            foreach ($get_hidro->result() as $h) {
                                //log_message('error', 'correccion: '.$h->correccion);
                                //$tot=array_sum($h->correccion);
                                array_push($tot, round($h->correccion, 5));
                            }
                            //$tot=array_sum($tot);
                            //log_message('error', 'tot: '.json_encode($tot));
                            $factor_corprom = array_sum($tot) / count($tot);
                            //log_message('error', 'factor_corprom: '.$factor_corprom);
                        }
                        foreach ($rowpuntos->result() as $itemfp) {
                            $idpunto = $itemfp->punto;
                            $idnom = $itemfp->idnom;
                            $array_rrs = array();
                            $array_rrs_graf = array();
                        ?>
                            <table align="center" width="100%">
                                <tr>
                                    <th><br></th>
                                </tr>
                            </table>
                            <div id="grafo1_<?php echo $itemfp->id; ?>">
                                <table align="center" width="100%" class="table table-bordered tablev">
                                    <tr>
                                        <th>No. de Punto:</th>
                                        <td><?php echo $idpunto; ?></td>

                                        <td colspan="2"> </td>
                                        <th>Area: </th>
                                        <td colspan="4"><?php echo $itemfp->area; ?> / <?php echo $itemfp->ubicacion; ?></td>
                                    </tr>
                                </table>

                                <table class="table table-bordered tablecaptura faepa tablev">
                                    <thead>
                                        <tr>
                                            <th>Distancia (m)</th>
                                            <th>0</th>
                                            <th>1</th>
                                            <th>4</th>
                                            <th>7</th>
                                            <th>10</th>
                                            <th>13</th>
                                            <th>16</th>
                                            <th>19</th>
                                            <th>22</th>
                                            <th>25</th>
                                            <th>28</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        /*$rownomdfaep=$this->ModeloCatalogos->getselectwheren('nom22d',array('idnom'=>$idnom,'punto'=>$idpunto,"r1>"=>0,'activo'=>1));
                                                $itemfp=$rownomdfaep->row();*/
                                        $r1f = 0;
                                        $r2f = 0;
                                        $r3f = 0;
                                        $r4f = 0;
                                        $r5f = 0;
                                        $r6f = 0;
                                        $r7f = 0;
                                        $r8f = 0;
                                        $r9f = 0;
                                        $medicion = 0;
                                        /*$array_rrs[]=array_push($array_rrs,$itemfp->r1,$itemfp->r2,$itemfp->r3,$itemfp->r4,$itemfp->r5,$itemfp->r6,$itemfp->r7,$itemfp->r8,$itemfp->r9);*/
                                        ?>
                                        <tr>
                                            <th>Resistencia (Ω)</th>
                                            <td class="tdinput"> 0 </td>
                                            <td class="tdinput"><?php if ($itemfp->r1 > 0) echo $itemfp->r1;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r2 > 0) echo $itemfp->r2;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r3 > 0) echo $itemfp->r3;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r4 > 0) echo $itemfp->r4;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r5 > 0) echo $itemfp->r5;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r6 > 0) echo $itemfp->r6;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r7 > 0) echo $itemfp->r7;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r8 > 0) echo $itemfp->r8;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r9 > 0) echo $itemfp->r9;
                                                                else echo "--"; ?></td>
                                            <td class="tdinput"> -- </td>

                                        </tr>
                                        <tr>
                                            <th style="font-size:13px">Resistencia Corregida (Ω)</th>
                                            <td class="tdinput"> 0</td>
                                            <td class="tdinput"><?php if ($itemfp->r1 == 0) echo "--";
                                                                echo '<!--'.$itemfp->r1.'-->'; 
                                                                echo '<!--'.$unidad2.'-->'; 

                                                                echo '<!--'.$pend2.'-->'; 
                                                                echo '<!--'.$inter2.'-->'; 
                                                                if ($itemfp->r1 <= $unidad2) echo $r1f = number_format(($itemfp->r1 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r1 <= $unidad20 && $itemfp->r1 > $unidad2) echo $r1f = number_format(($itemfp->r1 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r1 <= $unidad200 && $itemfp->r1 > $unidad20) echo $r1f = number_format(($itemfp->r1 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r1 <= $unidad2000 && $itemfp->r1 > $unidad200) echo $r1f = number_format(($itemfp->r1 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r1 <= $unidad20000 && $itemfp->r1 > $unidad2000) echo $r1f = number_format(($itemfp->r1 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r2 == 0) echo "--";
                                                                else if ($itemfp->r2 <= $unidad2) echo $r2f = number_format(($itemfp->r2 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r2 <= $unidad20 && $itemfp->r2 > $unidad2) echo $r2f = number_format(($itemfp->r2 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r2 <= $unidad200 && $itemfp->r2 > $unidad20) echo $r2f = number_format(($itemfp->r2 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r2 <= $unidad2000 && $itemfp->r2 > $unidad200) echo $r2f = number_format(($itemfp->r2 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r2 <= $unidad20000 && $itemfp->r2 > $unidad2000) echo $r2f = number_format(($itemfp->r2 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r3 == 0) echo "--";
                                                                else if ($itemfp->r3 <= $unidad2) echo $r3f = number_format(($itemfp->r3 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r3 <= $unidad20 && $itemfp->r3 > $unidad2) echo $r3f = number_format(($itemfp->r3 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r3 <= $unidad200 && $itemfp->r3 > $unidad20) echo $r3f = number_format(($itemfp->r3 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r3 <= $unidad2000 && $itemfp->r3 > $unidad200) echo $r3f = number_format(($itemfp->r3 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r3 <= $unidad20000 && $itemfp->r3 > $unidad2000) echo $r3f = number_format(($itemfp->r3 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r4 == 0) echo "--";
                                                                else if ($itemfp->r4 <= $unidad2) echo $r4f = number_format(($itemfp->r4 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r4 <= $unidad20 && $itemfp->r4 > $unidad2) echo $r4f = number_format(($itemfp->r4 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r4 <= $unidad200 && $itemfp->r4 > $unidad20) echo $r4f = number_format(($itemfp->r4 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r4 <= $unidad2000 && $itemfp->r4 > $unidad200) echo $r4f = number_format(($itemfp->r4 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r4 <= $unidad20000 && $itemfp->r4 > $unidad2000) echo $r4f = number_format(($itemfp->r4 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r5 == 0) echo "--";
                                                                else if ($itemfp->r5 <= $unidad2) echo $r5f = number_format(($itemfp->r5 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r5 <= $unidad20 && $itemfp->r5 > $unidad2) echo $r5f = number_format(($itemfp->r5 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r5 <= $unidad200 && $itemfp->r5 > $unidad20) echo $r5f = number_format(($itemfp->r5 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r5 <= $unidad2000 && $itemfp->r5 > $unidad200) echo $r5f = number_format(($itemfp->r5 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r5 <= $unidad20000 && $itemfp->r5 > $unidad2000) echo $r5f = number_format(($itemfp->r5 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r6 == 0) echo "--";
                                                                else if ($itemfp->r6 <= $unidad2) echo $r6f = number_format(($itemfp->r6 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r6 <= $unidad20 && $itemfp->r6 > $unidad2) echo $r6f = number_format(($itemfp->r6 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r6 <= $unidad200 && $itemfp->r6 > $unidad20) echo $r6f = number_format(($itemfp->r6 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r6 <= $unidad2000 && $itemfp->r6 > $unidad200) echo $r6f = number_format(($itemfp->r6 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r6 <= $unidad20000 && $itemfp->r6 > $unidad2000) echo $r6f = number_format(($itemfp->r6 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r7 == 0) echo "--";
                                                                else if ($itemfp->r7 <= $unidad2) echo $r7f = number_format(($itemfp->r7 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r7 <= $unidad20 && $itemfp->r7 > $unidad2) echo $r7f = number_format(($itemfp->r7 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r7 <= $unidad200 && $itemfp->r7 > $unidad20) echo $r7f = number_format(($itemfp->r7 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r7 <= $unidad2000 && $itemfp->r7 > $unidad200) echo $r7f = number_format(($itemfp->r7 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r7 <= $unidad20000 && $itemfp->r7 > $unidad2000) echo $r7f = number_format(($itemfp->r7 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r8 == 0) echo "--";
                                                                else if ($itemfp->r8 <= $unidad2) echo $r8f = number_format(($itemfp->r8 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r8 <= $unidad20 && $itemfp->r8 > $unidad2) echo $r8f = number_format(($itemfp->r8 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r8 <= $unidad200 && $itemfp->r8 > $unidad20) echo $r8f = number_format(($itemfp->r6 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r8 <= $unidad2000 && $itemfp->r8 > $unidad200) echo $r8f = number_format(($itemfp->r8 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r8 <= $unidad20000 && $itemfp->r8 > $unidad2000) echo $r8f = number_format(($itemfp->r8 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"><?php if ($itemfp->r9 == 0) echo "--";
                                                                else if ($itemfp->r9 <= $unidad2) echo $r9f = number_format(($itemfp->r9 * $pend2) + $inter2, 3);
                                                                else if ($itemfp->r9 <= $unidad20 && $itemfp->r9 > $unidad2) echo $r9f = number_format(($itemfp->r9 * $pend20) + $inter20, 3);
                                                                else if ($itemfp->r9 <= $unidad200 && $itemfp->r9 > $unidad20) echo $r9f = number_format(($itemfp->r6 * $pend200) + $inter200, 3);
                                                                else if ($itemfp->r9 <= $unidad2000 && $itemfp->r9 > $unidad200) echo $r9f = number_format(($itemfp->r9 * $pend2000) + $inter2000, 3);
                                                                else if ($itemfp->r9 <= $unidad20000 && $itemfp->r9 > $unidad2000) echo $r9f = number_format(($itemfp->r9 * $pend20000) + $inter20000, 3); ?></td>
                                            <td class="tdinput"> -- </td>
                                        </tr>
                                    </tbody>
                                </table>


                                <div class="row">
                                    <div class="col-md-3">
                                        <table class="table table-bordered tablev">
                                            <tr>
                                                <th>Valor de la resistencia (Ω)</th>
                                            </tr>
                                            <tr>
                                                <td><?php $array_rrs[] = array_push($array_rrs, $r1f, $r2f, $r3f, $r4f, $r5f, $r6f, $r7f, $r8f, $r9f);
                                                    $array_rrs_graf = array_push($array_rrs_graf, $r1f, $r2f, $r3f, $r4f, $r5f, $r6f, $r7f, $r8f, $r9f);
                                                    $pref = array_values(array_diff_assoc($array_rrs, array_unique($array_rrs)));
                                                    echo $pref[0];
                                                    $this->ModeloCatalogos->updateCatalogo("nom22d", array("rcf1" => $r1f, "rcf2" => $r2f, "rcf3" => $r3f, "rcf4" => $r4f, "rcf5" => $r5f, "rcf6" => $r6f, "rcf7" => $r7f, "rcf8" => $r8f, "rcf9" => $r9f, 'resistencia_corregida' => $pref[0]), array('id' => $itemfp->id));
                                                    ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablev">
                                            <tr>
                                                <th>Humedad Relativa</th>
                                            </tr>
                                            <tr>
                                                <td><?php $getFts = $this->ModeloCatalogos->getselectwheren('fuentes_genera_nom22', array('id_nom' => $idnom, 'punto' => $idpunto, "medicion_humedad!=" => '', 'activo' => 1));
                                                    $ift = $getFts->row();
                                                    if ($getFts->num_rows() > 0 && $ift->medicion_humedad == "" || $getFts->num_rows() == 0) echo "--";
                                                    else if ($getFts->num_rows() > 0 && $ift->medicion_humedad != "") echo $medicion = $ift->medicion_humedad; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablev">
                                            <tr>
                                                <th>Humedad Relativa <br>Corregida (%)</th>
                                            </tr>
                                            <tr>
                                                <td><?php if ($medicion != 0) echo number_format($medicion * $factor_corprom, 3);
                                                    else echo "--"; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-9">
                                        <table class="">
                                            <tr>
                                                <td width="5%" style="color: transparent; transform: rotate(-90deg);">RESISTENCIA(Ω)</td>
                                                <td width="95%"><canvas id="myChartgtgb_<?php echo $itemfp->id; ?>" height="130"></canvas></td>
                                            </tr>
                                        </table>

                                        <!--<center><h5>DISTANCIA</h5></center>-->
                                    </div>
                                </div>


                                <script type="text/javascript">
                                    $(document).ready(function($) {
                                        graficageneral<?php echo $itemfp->id ?>();
                                    });

                                    function graficageneral<?php echo $itemfp->id ?>() {
                                        /*const labels = ['DISTANCIA (M)']
                                        const dataset1 = {
                                            label: "Dataset 1",
                                            data: <?php echo $array_rrs_graf; ?>,
                                            borderColor: 'rgba(248, 37, 37, 0.8)',
                                            fill: false,
                                            tension: 0.1
                                        };
                                        const graph = document.querySelector('myChartgtgb_<?php echo $itemfp->id ?>');
                                        const data = {
                                            labels: labels,
                                            datasets: [dataset1]
                                        };
                                        const config = {
                                            type: 'line',
                                            data: data,
                                        };
                                        new Chart(graph, config);*/

                                        var ctx = document.getElementById('myChartgtgb_<?php echo $itemfp->id ?>').getContext('2d');
                                        //Fondo Blanco.--->
                                        ctx.fillStyle = 'white';
                                        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                                        //Fondo Blanco.--->
                                        var labels = ['DISTANCIA (M)'];
                                        var values = [<?php echo $array_rrs_graf; ?>];

                                        const plugin = {
                                            id: 'customCanvasBackgroundColor',
                                            beforeDraw: (chart, args, options) => {
                                                const {ctx} = chart;
                                                ctx.save();
                                                ctx.globalCompositeOperation = 'destination-over';
                                                ctx.fillStyle = options.color || '#ffffff';
                                                ctx.fillRect(0, 0, chart.width, chart.height);
                                                ctx.restore();
                                            }
                                        };

                                        var myChart = new Chart(ctx, {
                                            type: 'line',
                                            /*data: {
                                                labels: labels,
                                                datasets: [{
                                                    label: "GRÁFICA DE RESULTADOS ",
                                                    data: values,
                                                        backgroundColor: [
                                                        'rgba(91, 155, 213, 0.2)',
                                                        'rgba(91, 155, 213, 0.8)'
                                                    ]
                                                }]
                                            },*/
                                            data: {
                                                //labels: labels, 
                                                labels: ['0', '1', '4', '7', '10', '13', '16', '19', '22', '25', '28'],
                                                datasets: [{
                                                    //label: 'Resistencia corregida (Ω)',
                                                    label: ['GRÁFICA DE RESULTADOS'],
                                                    data: [0, <?php echo $r1f; ?>, <?php echo $r2f; ?>, <?php echo $r3f; ?>, <?php echo $r4f; ?>, <?php echo $r5f; ?>, <?php echo $r6f; ?>, <?php echo $r7f; ?>, <?php echo $r8f; ?>, <?php echo $r9f; ?>, 0, 0],
                                                    borderColor: 'rgba(91, 155, 213, 0.9)',
                                                    fill: false,
                                                    lineTension: 0,
                                                    pointRadius: 10,
                                                }, ]
                                            },

                                            /*marker:{
                                                dataLabel:{
                                                    visible: true,
                                                    shape: 'none',
                                                    connectorLine: { type: 'bezier', color: 'black' },
                                                    font: { size: '19px' }
                                                 }
                                            },*/

                                            options: {
                                                /*title: {
                                                    display: true,
                                                    text: "DISTANCIA"
                                                },*/
                                        
                                                plugins: {
                                                    datalabels: {
                                                        anchor: "center",
                                                        formatter: (dato) => dato,
                                                        color: "black",
                                                        font: {
                                                            family: '"Times New Roman"',
                                                            size: "18",
                                                            weight: "bold",
                                                        },
                                                    },
                                                },

                                                scales: {
                                                    y: [{
                                                        //label: ["RESISTENCIA()"],
                                                        label: ['GRÁFICA DE RESULTADOS'],
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }],
                                                    x: [{
                                                        label: ["RESISTENCIA (Ω)"],
                                                        //label: ['GRÁFICA DE RESULTADOS'],
                                                        ticks: {
                                                            beginAtZero: true
                                                        }
                                                    }],
                                                    xAxes: [{
                                                        ticks: {
                                                            autoSkip: true,
                                                            maxTicksLimit: 50,
                                                            maxRotation: 0,
                                                            minRotation: 0
                                                        },
                                                        scaleLabel: {
                                                            display: true,
                                                            labelString: "DISTANCIA (M)",
                                                            fontColor: "black"
                                                        }
                                                    }],
                                                    yAxes: [{
                                                        scaleLabel: {
                                                            display: true,
                                                            labelString: "RESISTENCIA (Ω)",
                                                            fontColor: "black"
                                                        }
                                                    }]
                                                },
                                            },
                                            plugins: [plugin],
                                        });


                                    }
                                </script>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function($) {
                                    setTimeout(function() {
                                        savegrafo('myChartgtgb_<?php echo $itemfp->id ?>', <?php echo $itemfp->id ?>);
                                    }, 1500);
                                });
                            </script>
                        <?php $rowpuntostr++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    function savegrafo(idname, id) {
        console.log("idname: " + idname);
        var base_url = $('#base_url').val();
        var canvasx = document.getElementById(idname);

        //var dataURL = canvasx.toDataURL('image/png', 1);
        var dataURL = canvasx.toDataURL('image/jpeg', 0.8);

        $.ajax({
            type: 'POST',
            url: base_url + 'Nom/nominserupdatetableg22',
            data: {
                id: id,
                grafica: dataURL
            },
            success: function(data) {
                console.log("hecho");
                //console.log("GRAPH: "+dataURL);
            }
        });
    }
</script>