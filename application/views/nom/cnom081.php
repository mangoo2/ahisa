nu<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }

</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <input type="hidden" id="id_orden" value="<?php echo $id_orden; ?>">
                        <input type="hidden" id="id_aux" value="<?php echo $idnom; ?>">
                        <input type="hidden" id="id_chs" value="<?php echo $id_chs; ?>">
                        <!--<form class="form" id="form_nom">-->
                        <table class="table table-sm" id="tabla_nom81">
                            <tbody id="table_prin_nom81">
                            <tr class="datos_nom_principal padrespp" id="datos_nom_principal" <?php if($idnom!="0") //echo "style='display:none'"; ?>>
                                <td>
                                    <input type="hidden" name="idnom" id="idnom" value="0">
                                    <div class="row" id="cont_ocu1">
                                        <div class="col-md-4">
                                            <label>No. de Informe</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="text" class="form-control num_informe_princi" name="num_informe" id="num_informe" value="">
                                        </div>
                                    </div>
                                    <div class="row" id="cont_ocu1">
                                        <div class="col-md-4">
                                            <label>Razón Social</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="text" class="form-control razon_social_princi" name="razon_social" id="razon_social" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-6">
                                            <label>Fecha</label>
                                        </div>
                                        <div class="col-md-8 col-sm-6 col-6 form-group">
                                            <input type="date" class="form-control" id="fecha" name="fecha" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label>Lugar</label>
                                            <input type="text" name="lugar" id="lugar" class="form-control" value="">
                                        </div>
                                        
                                        <div class="col-md-4"> 
                                            <label>Horario de medición</label>    
                                            <select name="horario" id="horario" class="form-control">
                                                <option value="1">Diurno</option>
                                                <option value="2">Nocturno</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Zona crítica</label>
                                            <input type="number" name="zona_critica" id="zona_critica" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Verificación inicial</label>
                                            <input type="text" name="veri_ini" id="veri_ini" class="form-control" value="">
                                        </div>
                                        
                                        <div class="col-md-3"> 
                                            <label>Verificación final</label>    
                                            <input type="text" name="veri_fin" id="veri_fin" class="form-control" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Criterio de aceptación</label>
                                            <input type="text" name="criterio" id="criterio" class="form-control" value="+-1 dB(A)">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Cumple criterio</label>
                                            <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Potencia batería inicio</label>
                                            <input type="text" name="pot_bat_ini" id="pot_bat_ini" class="form-control" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Potencia batería fin</label>
                                            <input type="text" name="pot_bat_fin" id="pot_bat_fin" class="form-control" value="">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Sonómetro</label>
                                                <select name="id_sonometro" id="id_sonometro" class="form-control">
                                                    <?php foreach($sono->result() as $eq){ ?>
                                                        <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Calibrador Acústico</label>
                                                <select name="id_calibrador" id="id_calibrador" class="form-control">
                                                    <?php foreach($cali->result() as $eq){ ?>
                                                        <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 titulosC">
                                            <label>Observaciones</label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="observaciones" class="form-control" rows="2" value=""></textarea>
                                        </div>
                                    </div>
                                    <table class="table table-responsive">
                                        <tr>
                                            <td class="tdinput titulos">Elemento constructivo</td>
                                            <td class="tdinput titulos">Largo (m)</td>
                                            <td class="tdinput"><input type="number" name="ec_largo" id="ec_largo" class="form-control" value=""></td>
                                            <td class="tdinput titulos">Ancho (m)</td>
                                            <td class="tdinput"><input type="number" name="ec_ancho" id="ec_ancho" class="form-control" value=""></td>
                                            <td class="tdinput titulos">Área (m2)</td>
                                            <td class="tdinput"><input type="number" id="ec_area" class="form-control" value=""></td>
                                        </tr>
                                    </table>
                                <!--</form>-->
                                </td>
                            </tr>
                            <tr class="datos_nom_principal2 padrespp" id="datos_nom_detalles">
                                <td>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn-info mr-1 mb-1" id="add_zona">Agregar zona</button>
                                        </div>
                                    </div>

                                    <div class="row" id="cont_datos">
                                        <div class="col-md-6">
                                            <table cellpadding="1" class="table table-bordered tablecaptura ruido_fte ruido_fte_prin" style="text-align: center">
                                                <thead>
                                                    <tr>
                                                        <th colspan="6" style="text-align: center">RUIDO DE FUENTE (dB "A")</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr id="tr_horas">
                                                        <th width="10%">Hora inicio</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_ini" value="" class="cleans form-control"></th>
                                                        <th width="10%">Hora final</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_fin" value="" class="cleans form-control"></th>
                                                    </tr>
                                                    <tr>
                                                        <th>No. lectura</th>
                                                        <th>A</th>
                                                        <th>B</th>
                                                        <th>C</th>
                                                        <th>D</th>
                                                        <th>E</th>
                                                    </tr>
                                                
                                                    <?php
                                                    for($i=1; $i<=35; $i++){
                                                        echo '<tr>
                                                            <td class="tdinput">
                                                            <input type="hidden" id="id_fte" value="0" class="form-control">
                                                                <input readonly type="text" id="rfte_no" value="'.$i.'" class="form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_a" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_b" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_c" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_d" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_e" value="" class="cleans form-control">
                                                            </td>
                                                        </tr>';
                                                    }
                                                    
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-bordered tablecaptura ruido_fondo ruido_fondo_prin" style="text-align: center">
                                                <thead>
                                                    <tr>
                                                        <th colspan="6" style="text-align: center">RUIDO DE FONDO (dB "A")</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">Hora inicio</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_ini" value="" class="cleans form-control"></th>
                                                        <th width="10%">Hora final</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_fin" value="" class="cleans form-control"></th>
                                                    </tr>
                                                    <tr>
                                                        <th>No. lectura</th>
                                                        <th>I</th>
                                                        <th>II</th>
                                                        <th>III</th>
                                                        <th>IV</th>
                                                        <th>V</th>
                                                    </tr>
                                                
                                                    <?php
                                                    for($i=1; $i<=35; $i++){
                                                        echo '<tr>
                                                            <td class="tdinput">
                                                                <input type="hidden" id="id_fondo" value="0" class="form-control">
                                                                <input readonly type="text" id="rfte_no" value="'.$i.'" class="form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_1" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_2" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_3" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_4" value="" class="cleans form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_5" value="" class="cleans form-control">
                                                            </td>
                                                        </tr>';
                                                    }
                                                    
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-8">
                                            <table class="table table-bordered tablecaptura ruido_aisla ruido_aisla_prin" style="text-align: center">
                                                <thead>
                                                    <tr>
                                                        <th colspan="6" style="text-align: center">RUIDO DE AISLAMIENTO</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">Hora inicio</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_ini" value="" class="cleans form-control"></th>
                                                        <th width="10%">Hora final</th>
                                                        <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_fin" value="" class="cleans form-control"></th>
                                                    </tr>
                                                    <tr>
                                                        <th>No. lectura</th>
                                                        <th>a</th>
                                                        <th>b</th>
                                                        <th>c</th>
                                                        <th>d</th>
                                                        <th>e</th>
                                                    </tr>
                                                
                                                    <?php 
                                                    for($i=1; $i<=35; $i++){
                                                        echo '<tr>
                                                            <td class="tdinput">
                                                                <input type="hidden" id="id_aisla" value="0" class="form-control">
                                                                <input readonly type="text" id="rfte_no" value="'.$i.'" class="form-control">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_a" value="" data-fil="'.$i.'" class="cleans form-control a_'.$i.'">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_b" value="" data-fil="'.$i.'" class="cleans form-control b_'.$i.'">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_c" value="" data-fil="'.$i.'" class="cleans form-control c_'.$i.'">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_d" value="" data-fil="'.$i.'" class="cleans form-control d_'.$i.'">
                                                            </td>
                                                            <td class="tdinput">
                                                                <input type="text" id="rfte_e" value="" data-fil="'.$i.'" class="cleans form-control e_'.$i.'">
                                                            </td>
                                                        </tr>';
                                                    }
                                                    
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4">
                                            <table class="table table-bordered tablecaptura distancias_fte distancia_fte_prin" style="text-align: center">
                                                <thead>
                                                    <tr>
                                                        <th colspan="3" style="text-align: center">DISTANCIA DEL SONOMETRO</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" style="text-align: center">Fuente</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!--<input type="hidden" id="id_dist" value="0" class="form-control">-->
                                                    <tr class="tr_save">
                                                        <td>Altura:</td>
                                                        <td class="tdinput">
                                                            <input type="hidden" id="id_dist" value="0" class="form-control">
                                                            <input type="number" id="altura_fte" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                    <tr class="tr_save">
                                                        <td>Distancia:</td>
                                                        <td class="tdinput"><input type="number" id="distancia_fte" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                
                                                    <tr>
                                                        <th colspan="3" style="text-align: center">Fondo</th>
                                                    </tr>
                                                
                                                    <tr class="tr_save">
                                                        <td>Altura:</td>
                                                        <td class="tdinput"><input type="number" id="altura_fondo" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                    <tr class="tr_save">
                                                        <td>Distancia:</td>
                                                        <td class="tdinput"><input type="number" id="distancia_fondo" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                
                                                    <tr>
                                                        <th colspan="3" style="text-align: center">Reducción acústica</th>
                                                    </tr>
                                                
                                                    <tr class="tr_save">
                                                        <td>Altura:</td>
                                                        <td class="tdinput"><input type="number" id="altura_reduc" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                    <tr class="tr_save">
                                                        <td>Distancia:</td>
                                                        <td class="tdinput"><input type="number" id="distancia_reduc" value="" class="cleans form-control"></td>
                                                        <td>m</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $get_nom=$this->ModeloCatalogos->getselectwheren('nom81',array('idordenes'=>$id_orden,'activo'=>1)); 
                            foreach($get_nom->result() as $n){
                            ?>
                                <tr>
                                    <td class="col-md-12 cont_max_<?php echo $n->idnom;?>" style="display: none;">
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn-info mr-1 mb-1" onclick="minimizar(1,<?php echo $n->idnom;?>)" ><i class="fa fa-plus"></i> Maximizar</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="datos_nom_principal" class="datos_nom_principal_<?php echo $n->idnom;?>">
                                    <td>
                                        <input type="hidden" name="idnom" id="idnom" value="<?php echo $n->idnom;?>">
                                        <!--<div class="col-md-12 cont_max_<?php echo $n->idnom;?>" style="text-align: end; display: none;">
                                            <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="minimizar(1,<?php echo $n->idnom;?>)" ><i class="fa fa-plus"></i> Maximizar</button>
                                        </div>-->
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="minimizar(0,<?php echo $n->idnom;?>)" ><i class="fa fa-minus"></i> Minimizar</button>
                                                <button type="button" class="btn btn-danger mr-1 mb-1" id="delete_zona" onclick="eliminarZona(<?php echo $n->idnom;?>)"><i class="fa fa-trash-o"></i> Eliminar zona</button>
                                            </div>
                                        </div>
                                        <div class="row" id="cont_ocu1">
                                            <div class="col-md-4">
                                                <label>No. de Informe</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control" name="num_informe" id="num_informe" value="<?php echo $n->num_informe;?>">
                                            </div>
                                        </div>
                                        <div class="row" id="cont_ocu1">
                                            <div class="col-md-4">
                                                <label>Razón Social</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" class="form-control" name="razon_social" id="razon_social" value="<?php echo $n->razon_social;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $n->fecha;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Lugar</label>
                                                <input type="text" name="lugar" id="lugar" class="form-control" value="<?php echo $n->lugar;?>">
                                            </div>
                                            
                                            <div class="col-md-4"> 
                                                <label>Horario de medición</label>    
                                                <!--<input type="text" name="horario" id="horario" class="form-control" value="<?php echo $horario;?>">-->
                                                <select name="horario" id="horario" class="form-control">
                                                    <option value="1" <?php if($n->horario=="1"){ echo "selected"; } ?>>Diurno</option>
                                                    <option value="2" <?php if($n->horario=="2"){ echo "selected"; } ?>>Nocturno</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Zona crítica</label>
                                                <input type="number" name="zona_critica" id="zona_critica" class="form-control" value="<?php echo $n->zona_critica;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Verificación inicial</label>
                                                <input type="text" name="veri_ini" id="veri_ini" class="form-control" value="<?php echo $n->veri_ini;?>">
                                            </div>
                                            
                                            <div class="col-md-3"> 
                                                <label>Verificación final</label>    
                                                <input type="text" name="veri_fin" id="veri_fin" class="form-control" value="<?php echo $n->veri_fin;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Criterio de aceptación</label>
                                                <input type="text" name="criterio" id="criterio" class="form-control" value="<?php echo $n->criterio;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Cumple criterio</label>
                                                <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="<?php echo $n->cumple_criterio;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Potencia batería inicio</label>
                                                <input type="text" name="pot_bat_ini" id="pot_bat_ini" class="form-control" value="<?php echo $n->pot_bat_ini;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Potencia batería fin</label>
                                                <input type="text" name="pot_bat_fin" id="pot_bat_fin" class="form-control" value="<?php echo $n->pot_bat_fin;?>">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Sonómetro</label>
                                                    <select name="id_sonometro" id="id_sonometro" class="form-control">
                                                        <?php foreach($sono->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$n->id_sonometro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Calibrador Acústico</label>
                                                    <select name="id_calibrador" id="id_calibrador" class="form-control">
                                                        <?php foreach($cali->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$n->id_calibrador){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulosC">
                                                <label>Observaciones</label>
                                            </div>
                                            <div class="col-md-12">
                                                <textarea name="observaciones" class="form-control" rows="2" value="<?php echo $n->observaciones; ?>"><?php echo $n->observaciones; ?></textarea>
                                            </div>
                                        </div>
                                        <table class="table table-responsive">
                                            <tr>
                                                <td class="tdinput titulos">Elemento constructivo</td>
                                                <td class="tdinput titulos">Largo (m)</td>
                                                <td class="tdinput"><input type="number" name="ec_largo" id="ec_largo" class="form-control" value="<?php echo $n->ec_largo;?>"></td>
                                                <td class="tdinput titulos">Ancho (m)</td>
                                                <td class="tdinput"><input type="number" name="ec_ancho" id="ec_ancho" class="form-control" value="<?php echo $n->ec_ancho;?>"></td>
                                                <td class="tdinput titulos">Área (m2)</td>
                                                <td class="tdinput"><input type="number" id="ec_area" class="form-control" value="<?php echo $n->ec_largo*$n->ec_ancho;?>"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="datos_nom_detalles_<?php echo $n->idnom;?>" class="datos_nom_detalles_for datos_nom_detalles_<?php echo $n->idnom;?>">
                                    <td>
                                    <!--</form>-->
                                    
                                        <!--<div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn-info mr-1 mb-1" id="add_zona" onclick="addpuntos()">Agregar zona</button>
                                            </div>
                                        </div>-->

                                        <?php $hi=""; $hf=""; $get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                        if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }
                                        ?>
                                        <div class="row" id="cont_datos">
                                            <div class="col-md-6">
                                                <table cellpadding="1" class="table table-bordered tablecaptura ruido_fte ruido_fte_<?php echo $n->idnom; ?>" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE FUENTE (dB "A")</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_inihead" value="<?php echo $hi;?>" class="cleans form-control"></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_finhead" value="<?php echo $hf;?>" class="cleans form-control"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>A</th>
                                                            <th>B</th>
                                                            <th>C</th>
                                                            <th>D</th>
                                                            <th>E</th>
                                                        </tr>
                                                    
                                                        <?php $cont=0;
                                                        if($get_fte->num_rows()>0){
                                                            foreach ($get_fte->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput">
                                                                        <input type="hidden" id="id_fte" value="<?php echo $f->id;?>" class="form-control">
                                                                        <input type="hidden" id="hora_ini" value="<?php echo $hi;?>" class="cleans form-control">
                                                                        <input type="hidden" id="hora_fin" value="<?php echo $hf;?>" class="cleans form-control">
                                                                        <input readonly type="text" id="rfte_no" value="<?php echo $cont;?>" class="form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_a" value="<?php echo $f->a;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_b" value="<?php echo $f->b;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_c" value="<?php echo $f->c;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_d" value="<?php echo $f->d;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_e" value="<?php echo $f->e;?>" class="cleans form-control">
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $hi=""; $hf=""; $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                                if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
                                                ?>
                                                <table class="table table-bordered tablecaptura ruido_fondo ruido_fondo_<?php echo $n->idnom; ?>" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE FONDO (dB "A")</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_inihead" value="<?php echo $hi;?>" class="cleans form-control"></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_finhead" value="<?php echo $hf;?>" class="cleans form-control"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>I</th>
                                                            <th>II</th>
                                                            <th>III</th>
                                                            <th>IV</th>
                                                            <th>V</th>
                                                        </tr>
                                                        <?php $cont=0;
                                                        if($get_fondo->num_rows()>0){
                                                            foreach ($get_fondo->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput">
                                                                        <input type="hidden" id="id_fondo" value="<?php echo $f->id;?>" class="form-control">
                                                                        <input type="hidden" id="hora_ini" value="<?php echo $hi;?>" class="cleans form-control">
                                                                        <input type="hidden" id="hora_fin" value="<?php echo $hf;?>" class="cleans form-control">
                                                                        <input readonly type="text" id="rfte_no" value="<?php echo $cont;?>" class="form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_1" value="<?php echo $f->uno;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_2" value="<?php echo $f->dos;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_3" value="<?php echo $f->tres;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_4" value="<?php echo $f->cuatro;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_5" value="<?php echo $f->cinco;?>" class="cleans form-control">
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-8">
                                                <?php $hi=""; $hf=""; $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$n->idnom,'estatus'=>1)); 
                                                if($get_aisla->num_rows()>0){ $gtas=$get_aisla->row(); $hi=$gtas->hora_inicio; $hf=$gtas->hora_fin; }
                                                ?>
                                                <table class="table table-bordered tablecaptura ruido_aisla ruido_aisla_<?php echo $n->idnom; ?>" style="text-align: center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="6" style="text-align: center">RUIDO DE AISLAMIENTO</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th width="10%">Hora inicio</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_inihead" value="<?php echo $hi;?>" class="cleans form-control"></th>
                                                            <th width="10%">Hora final</th>
                                                            <th width="25%" class="tdinput" colspan="2"><input type="time" id="hora_finhead" value="<?php echo $hf;?>" class="cleans form-control"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No. lectura</th>
                                                            <th>a</th>
                                                            <th>b</th>
                                                            <th>c</th>
                                                            <th>d</th>
                                                            <th>e</th>
                                                        </tr>
                                                        <?php $cont=0;
                                                        if($get_aisla->num_rows()>0){
                                                            foreach ($get_aisla->result() as $f) { $cont++; ?>
                                                                <tr>
                                                                    <td class="tdinput">
                                                                        <input type="hidden" id="id_aisla" value="<?php echo $f->id;?>" class="form-control">
                                                                        <input type="hidden" id="hora_ini" value="<?php echo $hi;?>" class="cleans form-control">
                                                                        <input type="hidden" id="hora_fin" value="<?php echo $hf;?>" class="cleans form-control">
                                                                        <input readonly type="text" id="rfte_no" value="<?php echo $cont;?>" class="form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_a" value="<?php echo $f->a;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_b" value="<?php echo $f->b;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_c" value="<?php echo $f->c;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_d" value="<?php echo $f->d;?>" class="cleans form-control">
                                                                    </td>
                                                                    <td class="tdinput">
                                                                        <input type="text" id="rfte_e" value="<?php echo $f->e;?>" class="cleans form-control">
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <?php $get_dist=$this->ModeloCatalogos->getselectwheren('distancias_nom81',array('id_nom'=>$n->idnom));  
                                                if($get_dist->num_rows()>0){
                                                    $gd = $get_dist->row();
                                                    $id_dist=$gd->id;
                                                    $altura_fte=$gd->altura_fte; $distancia_fte=$gd->distancia_fte;
                                                    $altura_fondo=$gd->altura_fondo; $distancia_fondo=$gd->distancia_fondo;
                                                    $altura_reduc=$gd->altura_reduc; $distancia_reduc=$gd->distancia_reduc;
                                                }else{
                                                    $id_dist="0";
                                                    $altura_fte=""; $distancia_fte="";
                                                    $altura_fondo=""; $distancia_fondo="";
                                                    $altura_reduc=""; $distancia_reduc="";
                                                }
                                                ?>
                                                <table class="table table-bordered tablecaptura distancias_fte distancias_fte_<?php echo $n->idnom; ?>" style="text-align: center">
                                                    
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3" style="text-align: center">DISTANCIA DEL SONOMETRO</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="3" style="text-align: center">Fuente</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="tr_save">
                                                            <td>Altura:</td>
                                                            <td class="tdinput"><input type="hidden" id="id_dist" value="<?php echo $id_dist;?>" class="form-control">
                                                                <input type="number" id="altura_fte" value="<?php echo $altura_fte;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                        <tr class="tr_save">
                                                            <td>Distancia:</td>
                                                            <td class="tdinput"><input type="number" id="distancia_fte" value="<?php echo $distancia_fte;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                   
                                                        <tr>
                                                            <th colspan="3" style="text-align: center">Fondo</th>
                                                        </tr>
                                                    
                                                        <tr class="tr_save">
                                                            <td>Altura:</td>
                                                            <td class="tdinput"><input type="number" id="altura_fondo" value="<?php echo $altura_fondo;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                        <tr class="tr_save">
                                                            <td>Distancia:</td>
                                                            <td class="tdinput"><input type="number" id="distancia_fondo" value="<?php echo $distancia_fondo;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                    
                                                        <tr>
                                                            <th colspan="3" style="text-align: center">Reducción acústica</th>
                                                        </tr>
                                                    
                                                        <tr class="tr_save">
                                                            <td>Altura:</td>
                                                            <td class="tdinput"><input type="number" id="altura_reduc" value="<?php echo $altura_reduc;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                        <tr class="tr_save">
                                                            <td>Distancia:</td>
                                                            <td class="tdinput"><input type="number" id="distancia_reduc" value="<?php echo $distancia_reduc;?>" class="cleans form-control"></td>
                                                            <td>m</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="saveDetalles()" ><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
