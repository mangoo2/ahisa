<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }

</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url();?>index.php/Nom/add11/<?php echo $idordenes;?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered tablecaptura">
                                    <tr>
                                        <th>No. de Informe</th>
                                        <td><?php echo $num_informe;?></td>
                                    </tr>
                                    <tr>
                                        <th>Razón Social</th>
                                        <td><?php echo $razon_social;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <?php $rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1)); ?>
                                <table class="table table-bordered tablecaptura" id="table_lectura">
                                    <thead>
                                        <tr>
                                            <th>No.<br>
                                                <a type="button" class="btn btn-info mr-1 mb-1 btn-lectura-add" href="<?php echo base_url();?>index.php/Nom/insernom11personalmun/<?php echo $idnom;?>/<?php echo $idordenes;?>"><i class="fa fa-plus"></i><span></span></a>
                                            </th>
                                            <th>Id. Dosimetro</th>
                                            <th>Nombre del trabajador</th>
                                            <th>Área</th>
                                            <th>Puesto</th>
                                            <th>% Dosis</th>
                                            <th>Fecha</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowpersonal->result() as $itemp) { ?>
                                            <tr onchange="saveform('nom11_personal',<?php echo $itemp->idnomd;?>)">
                                                <td><?php echo $itemp->num;?></td>
                                                <td class="tdinput">
                                                    <!--<input type="text" id="id_dosimetro_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->id_dosimetro;?>" class="form-control">-->                                               
                                                    <div class="form-group">
                                                        <select name="id_dosimetro_" id="id_dosimetro_<?php echo $itemp->idnomd;?>" class="form-control">
                                                            <?php foreach($e->result() as $eq){ ?>
                                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$itemp->id_dosimetro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>             
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" id="trabajador_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->trabajador;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" id="area_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->area;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" id="puesto_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->puesto;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" id="dosis_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->dosis;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="date" id="fecha_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->fecha;?>" class="form-control">
                                                </td>
                                                <td><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletep(<?php echo $itemp->idnomd;?>)" style="margin-bottom: 0px;"><i class="fa fa-trash-o"></i></button></td>

                                            </tr>
                                        <?php } ?>
                                        </tbody> 
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php $rowperti=$this->ModeloCatalogos->getselectwheren('nom11_personal_ti',array('idnom'=>$idnom,'activo'=>1)); ?>
                                <table class="table table-bordered tablecaptura" id="table_lectura">
                                    <thead>
                                        <tr>
                                            <th>No.<br>
                                                <a type="button" class="btn btn-info mr-1 mb-1 btn-lectura-add" href="<?php echo base_url();?>index.php/Nom/insernom11personaltimun/<?php echo $idnom;?>/<?php echo $idordenes;?>"><i class="fa fa-plus"></i><span></span></a>
                                            </th>
                                            <th>Ti (h)</th>
                                            <th>Tf (h)</th>
                                            <th>NRR</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowperti->result() as $itemp) { ?>
                                            <tr onchange="saveform('nom11_personal_ti',<?php echo $itemp->idnomd;?>)">
                                                <td><?php echo $itemp->num;?></td>
                                                <td class="tdinput">
                                                    <input type="time" id="ti_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->ti;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="time" id="tf_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->tf;?>" class="form-control">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" id="nrr_<?php echo $itemp->idnomd;?>" value="<?php echo $itemp->nrr;?>" class="form-control">
                                                </td>
                                                <td><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletept(<?php echo $itemp->idnomd;?>)" style="margin-bottom: 0px;"><i class="fa fa-trash-o"></i></button></td>

                                            </tr>
                                        <?php } ?>
                                        </tbody> 
                                </table>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url();?>index.php/Nom/add11/<?php echo $idordenes;?>"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</a>
                            </div>
                        </div>
                            
                        
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
