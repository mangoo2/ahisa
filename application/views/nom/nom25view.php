<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>app-assets/vendors/js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<style type="text/css">
    .titulos{
        background: #e9ecef;
        font-style: italic;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #c6c9cb;
    }
    .table thead th {
        
        border-bottom: 2px solid #c6c9cb;
    }
    .table{
        /*margin-top: 5px;
        width: auto;
        margin-left: auto;
        margin-right: auto;*/
    }
    .table td{
        padding: 0.5rem;
        font-size: 15px;
        vertical-align: middle;
        text-align: center;
        font-weight: bold;
    }
    .table tr{
        text-align: center;
    }
    .curs{
        font-style: italic;
    }

    .canvasmt,
    
    <?php for($cc=0; $cc<=250; $cc++) {
         echo "#canvasmt_".$cc.",";
    } ?>
    
    #canvastg1,#canvastg2{
        display: none;
    }
    @media (min-width:770px){
        .graficadiv{
            height: 50px;
        }
    }
    .title_tb{
        color: #548235; font-weight: bold;
    }
</style>
<script type="text/javascript">
    
</script>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
<input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
<!--<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row" id="cont_fechas_realiza">
                            <div class="col-md-3 col-sm-6 col-6">
                                <label>Fechas de puntos realizados</label>
                            </div>
                            <div class="col-md-3 col-sm-6 col-6 form-group">
                                <select class="form-control" id="fechas_realiza">
                                    <option value='0' disabled selected></option>
                                    <?php foreach($getFechas->result() as $nd){
                                        echo "<option value='".$nd->fecha."'>".$nd->fecha."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>-->


<?php 
    
    foreach ($datosnom->result() as $item) { 
        $datosequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$item->equipo));
        $idequipo=0;
        $equipo=0;
        foreach ($datosequipo->result() as $itemeq) {
            $idequipo=$itemeq->id;
            $equipo=$itemeq->luxometro;
        }
        $intercept=$this->ModeloCatalogos->interseccion_pendiente($idequipo,0);//E29
        $pendiente=$this->ModeloCatalogos->interseccion_pendiente($idequipo,1);//E30
    ?>
    <table class="table table-bordered">
        <tr>
            <td colspan="2"></td>
            <td class="titulos"><strong>No. de Informe</strong></td>
            <td class="title_tb"><?php echo $num_informe;?></td>
        </tr>
        <tr>
            <td class="titulos"><strong>Razón Social</strong></td>
            <td class="title_tb" colspan="3"><?php echo $razon_cliente;?></td>
        </tr>
        <!--<tr>
            <td class="titulos"><strong>Fecha</strong></td>
            <td class="title_tb"><?php echo $item->reg;?></td>
            <td class="titulos"><strong>Id. del equipo</strong></td>
            <td class="title_tb"><?php echo $equipo;?></td>
        </tr>-->
    </table>
<?php } 
    $arrayniveliluminacion=array();
    $rowpunto=1;
    $rowpuntoset=1000;

        //$datosnomdetalle=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('idnom'=>$idnom/*,"fecha"=>$fecha*/,'activo'=>1));
        $datosnomdetalle=$this->ModeloNom->getPuntosDetalle25(array('idnom'=>$idnom,'activo'=>1));
        foreach ($datosnomdetalle->result() as $item) { 
            $h1promedio=($item->h1_medicion_a+$item->h1_medicion_b+$item->h1_mdicion_c)/3;
            $h2promedio=($item->h2_medicion_a+$item->h2_medicion_b+$item->h2_medicion_c)/3;
            $h3promedio=($item->h3_medicion_a+$item->h3_medicion_b+$item->h3_medicion_c)/3;

            $h1_pcorr=($pendiente*$h1promedio)+$intercept;
            if($h1promedio==0){
                $h1_pcorr=0;
            }
            $h2_pcorr=($pendiente*$h2promedio)+$intercept;
            if($h2promedio==0){
                $h2_pcorr=0;
            }
            $h3_pcorr=($pendiente*$h3promedio)+$intercept;
            if($h3promedio==0){
                $h3_pcorr=0;
            }

            $h1_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h1_medicion_a,$item->h1_medicion_b,$item->h1_mdicion_c));
            $h2_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h2_medicion_a,$item->h2_medicion_b,$item->h2_medicion_c));
            $h3_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h3_medicion_a,$item->h3_medicion_b,$item->h3_medicion_c));

            $h1_tipo_distribucion='A';
            $h2_tipo_distribucion='A';
            $h3_tipo_distribucion='A';

            $h1_coef_sensibilidad=1;
            $h2_coef_sensibilidad=1;
            $h3_coef_sensibilidad=1;

            $h1_contribuccion=$h1_desv_estandar*$h1_coef_sensibilidad;
            $h2_contribuccion=$h2_desv_estandar*$h2_coef_sensibilidad;
            $h3_contribuccion=$h3_desv_estandar*$h3_coef_sensibilidad;

            $h1_uy2=pow($h1_contribuccion, 2);
            $h2_uy2=pow($h2_contribuccion, 2);
            $h3_uy2=pow($h3_contribuccion, 2);

                $escala_1_a='';
                $escala_1_b='';
                $escala_1_c='';
                $escala_2_a='';
                $escala_2_b='';
                $escala_2_c='';
                $escala_3_a='';
                $escala_3_b='';
                $escala_3_c='';
                $distribucion1='B, rectangular';
                $distribucion2='B, rectangular';
                $distribucion3='B, rectangular';

            $resultre=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom));
            if($resultre->num_rows()>0){
                foreach ($resultre->result() as $itemre) {
                    if($itemre->todos_ptos=="1"){
                        if($itemre->escala1==1){
                            $escala_1_a='X';
                        }elseif($itemre->escala1==2){
                            $escala_2_a='X';
                        }else{
                            $escala_3_a='X';
                        }
                        
                        if($itemre->escala2==1){
                            $escala_1_b='X';
                        }elseif($itemre->escala2==2){
                            $escala_2_b='X';
                        }else{
                            $escala_3_b='X';
                        }

                        if($itemre->escala3==1){
                            $escala_1_c='X';
                        }elseif($itemre->escala3==2){
                            $escala_2_c='X';
                        }else{
                            $escala_3_c='X';
                        }
                        $distribucion1=$itemre->distribucion1;
                        $distribucion2=$itemre->distribucion2;
                        $distribucion3=$itemre->distribucion3;
                        $h1_coef_sensibilidad=$itemre->sencibilidad1;
                        $h2_coef_sensibilidad=$itemre->sencibilidad2;
                        $h3_coef_sensibilidad=$itemre->sencibilidad3;
                    }else{
                        $resultre=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom,"id_punto"=>$item->id));
                        if($resultre->num_rows()>0){
                            foreach ($resultre->result() as $itemre) {
                                    $escala_1_a='';
                                    $escala_1_b='';
                                    $escala_1_c='';
                                    $escala_2_a='';
                                    $escala_2_b='';
                                    $escala_2_c='';
                                    $escala_3_a='';
                                    $escala_3_b='';
                                    $escala_3_c='';

                                if($itemre->escala1==1){
                                    $escala_1_a='X';
                                }elseif($itemre->escala1==2){
                                    $escala_2_a='X';
                                }else{
                                    $escala_3_a='X';
                                }
                                
                                if($itemre->escala2==1){
                                    $escala_1_b='X';
                                }elseif($itemre->escala2==2){
                                    $escala_2_b='X';
                                }else{
                                    $escala_3_b='X';
                                }

                                if($itemre->escala3==1){
                                    $escala_1_c='X';
                                }elseif($itemre->escala3==2){
                                    $escala_2_c='X';
                                }else{
                                    $escala_3_c='X';
                                }
                                $distribucion1=$itemre->distribucion1;
                                $distribucion2=$itemre->distribucion2;
                                $distribucion3=$itemre->distribucion3;
                                $h1_coef_sensibilidad=$itemre->sencibilidad1;
                                $h2_coef_sensibilidad=$itemre->sencibilidad2;
                                $h3_coef_sensibilidad=$itemre->sencibilidad3;  
                            }
                        }
                    }
                }
            }else{
                $escala_1_a='';
                $escala_1_b='';
                $escala_1_c='';
                $escala_2_a='X';
                $escala_2_b='X';
                $escala_2_c='X';
                $escala_3_a='';
                $escala_3_b='';
                $escala_3_c='';
            }
            if($escala_1_a=='X'){
                $h1_resolicion=0.01;
            }elseif ($escala_2_a=='X') {
                $h1_resolicion=0.1;
            }else{
                $h1_resolicion=1;
            }
            if($escala_1_b=='X'){
                $h2_resolicion=0.01;
            }elseif ($escala_2_c=='X') {
                $h2_resolicion=0.1;
            }else{
                $h2_resolicion=1;
            }
            if($escala_1_c=='X'){
                $h3_resolicion=0.01;
            }elseif ($escala_2_c=='X') {
                $h3_resolicion=0.1;
            }else{
                $h3_resolicion=1;
            }

            $h1_u_estandar=$h1_resolicion/sqrt(12);
            $h2_u_estandar=$h2_resolicion/sqrt(12);
            $h3_u_estandar=$h3_resolicion/sqrt(12);

            $h1_contribuccion_rq=$h1_u_estandar*$h1_coef_sensibilidad;
            $h2_contribuccion_rq=$h2_u_estandar*$h2_coef_sensibilidad;
            $h3_contribuccion_rq=$h3_u_estandar*$h3_coef_sensibilidad;

            $h1_uy2_rq=pow($h1_contribuccion_rq, 2);
            $h2_uy2_rq=pow($h2_contribuccion_rq, 2);
            $h3_uy2_rq=pow($h3_contribuccion_rq, 2);

            $h1_u_declarada_en_el_IR=0;
            $h2_u_declarada_en_el_IR=0;
            $h3_u_declarada_en_el_IR=0;        
            
            $h1_u_declarada_en_el_IR_lx=$h1promedio*($h1_u_declarada_en_el_IR/100);
            $h2_u_declarada_en_el_IR_lx=$h2promedio*($h2_u_declarada_en_el_IR/100);
            $h3_u_declarada_en_el_IR_lx=$h3promedio*($h3_u_declarada_en_el_IR/100);

            $h1_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;
            $h2_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;
            $h3_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;

            $h1_uy2_c=pow($h1_contribucion_c, 2);
            $h2_uy2_c=pow($h2_contribucion_c, 2);
            $h3_uy2_c=pow($h3_contribucion_c, 2);

            $h1_uy2_suma=$h1_uy2+$h1_uy2_rq+$h1_uy2_c;
            $h2_uy2_suma=$h2_uy2+$h2_uy2_rq+$h2_uy2_c;
            $h3_uy2_suma=$h3_uy2+$h3_uy2_rq+$h3_uy2_c;

            $h1_uc_lx=sqrt($h1_uy2_suma);
            $h2_uc_lx=sqrt($h2_uy2_suma);
            $h3_uc_lx=sqrt($h3_uy2_suma);

            $h1_u_lx=$h1_uc_lx*2;
            $h2_u_lx=$h2_uc_lx*2;
            $h3_u_lx=$h3_uc_lx*2;

            if($h1_pcorr<$item->nmi){
                $h1_conclusion='No se supera el NMI';
                $h1_conclusion_style='style="color: red;font-weight: bolder;"';
            }elseif($h1promedio>$item->nmi){
                $h1_conclusion='Se supera el NMI';
                $h1_conclusion_style='style="color: green;font-weight: bolder;"';
            }else{
                $h1_conclusion='';
                $h1_conclusion_style='';
            }
            if($h2_pcorr<$item->nmi){
                $h2_conclusion='No se supera el NMI';
                $h2_conclusion_style='style="color: red;font-weight: bolder;"';
            }elseif($h2promedio>$item->nmi){
                $h2_conclusion='Se supera el NMI';
                $h2_conclusion_style='style="color: green;font-weight: bolder;"';
            }else{
                $h2_conclusion='';
                $h2_conclusion_style='';
            }
            if($h3_pcorr<$item->nmi){
                $h3_conclusion='No se supera el NMI';
                $h3_conclusion_style='style="color: red;font-weight: bolder;"';
            }elseif($h3promedio>$item->nmi){
                $h3_conclusion='Se supera el NMI';
                $h3_conclusion_style='style="color: green;font-weight: bolder;"';
            }else{
                $h3_conclusion='';
                $h3_conclusion_style='';
            }
            $h1_e1a_c=($item->h1_e1a*$pendiente)+$intercept;
            $h2_e1a_c=($item->h2_e1a*$pendiente)+$intercept;
            $h3_e1a_c=($item->h3_e1a*$pendiente)+$intercept;

            $h1_e2a_c=($item->h1_e2a*$pendiente)+$intercept;
            $h2_e2a_c=($item->h2_e2a*$pendiente)+$intercept;
            $h3_e2a_c=($item->h3_e2a*$pendiente)+$intercept;

            
            if($h1_e1a_c>0 && $h1_e2a_c>0){
                $h1_kf_c=($h1_e1a_c/$h1_e2a_c)*100;
            }else{
                $h1_kf_c=0;
            }
            
            if($h2_e1a_c>0 && $h2_e2a_c>0){
                $h2_kf_c=($h2_e1a_c/$h2_e2a_c)*100;
            }else{
                $h2_kf_c=0;
            }
            
            if($h3_e1a_c>0 && $h3_e2a_c>0){
                $h3_kf_c=($h3_e1a_c/$h3_e2a_c)*100;
            }else{
                $h3_kf_c=0;
            }
            $lmp=50;

            if($h1_kf_c<$lmp){
                $h1_t2_c='No se supera el NMP';
                $h1_t2_c_style='style="color: green;font-weight: bolder;"';
            }elseif ($h1_kf_c>$lmp) {
                $h1_t2_c='Se supera el NMP';
                $h1_t2_c_style='style="color: red;font-weight: bolder;"';

            }else{
                $h1_t2_c='';
                $h1_t2_c_style='';

            }
            if($h2_kf_c<$lmp){
                $h2_t2_c='No se supera el NMP';
                $h2_t2_c_style='style="color: green;font-weight: bolder;"';
            }elseif ($h2_kf_c>$lmp) {
                $h2_t2_c='Se supera el NMP';
                $h2_t2_c_style='style="color: red;font-weight: bolder;"';
            }else{
                $h2_t2_c='';
                $h2_t2_c_style='';
            }
            if($h3_kf_c<$lmp){
                $h3_t2_c='No se supera el NMP';
                $h3_t2_c_style='style="color: green;font-weight: bolder;"';
            }elseif ($h3_kf_c>$lmp) {
                $h3_t2_c='Se supera el NMP';
                $h3_t2_c_style='style="color: red;font-weight: bolder;"';
            }else{
                $h3_t2_c='';
                $h3_t2_c_style='';
            }

            if($item->h1_e1b>0){
                $h1_e1b_c=($item->h1_e1b*$pendiente)+$intercept;
            }else{
                $h1_e1b_c=0;
            }
            if($item->h2_e1b>0){
                $h2_e1b_c=($item->h2_e1b*$pendiente)+$intercept;
            }else{
                $h2_e1b_c=0;
            }
            if($item->h3_e1b>0){
                $h3_e1b_c=($item->h3_e1b*$pendiente)+$intercept;
            }else{
                $h3_e1b_c=0;
            }

            if($item->h1_e2b>0){
                $h1_e2b_c=($item->h1_e2b*$pendiente)+$intercept;
            }else{
                $h1_e2b_c=0;
            }
            if($item->h2_e2b>0){
                $h2_e2b_c=($item->h2_e2b*$pendiente)+$intercept;
            }else{
                $h2_e2b_c=0;
            }
            if($item->h3_e2b>0){
                $h3_e2b_c=($item->h3_e2b*$pendiente)+$intercept;
            }else{
                $h3_e2b_c=0;
            }

            if($h1_e1b_c>0 && $h1_e2b_c>0){
                $h1_kf2_c=($h1_e1b_c/$h1_e2b_c)*100;
            }else{
                $h1_kf2_c=0;
            }
            if($h2_e1b_c>0 && $h2_e2b_c>0){
                $h2_kf2_c=($h2_e1b_c/$h2_e2b_c)*100;  
            }else{
                $h2_kf2_c=0;
            }
            if($h3_e1b_c>0 && $h3_e2b_c>0){
                $h3_kf2_c=($h3_e1b_c/$h3_e2b_c)*100;  
            }else{
                $h3_kf2_c=0;
            }
            
            $lmp2=60;

            if($h1_kf2_c<$lmp2){
                $h1_t2_c2='No se supera el NMP';
                $h1_t2_c2_style='style="color: green;font-weight: bolder;"';
            }elseif ($h1_kf2_c>$lmp2) {
                $h1_t2_c2='Se supera el NMP';
                $h1_t2_c2_style='style="color: red;font-weight: bolder;"';
            }else{
                $h1_t2_c2='';
                $h1_t2_c2_style='';
            }
            if($h2_kf2_c<$lmp2){
                $h2_t2_c2='No se supera el NMP';
                $h2_t2_c2_style='style="color: green;font-weight: bolder;"';
            }elseif ($h2_kf2_c>$lmp2) {
                $h2_t2_c2='Se supera el NMP';
                $h2_t2_c2_style='style="color: red;font-weight: bolder;"';
            }else{
                $h2_t2_c2='';
                $h2_t2_c2_style='';
            }
            if($h3_kf2_c<$lmp2){
                $h3_t2_c2='No se supera el NMP';
                $h3_t2_c2_style='style="color: green;font-weight: bolder;"';
            }elseif ($h3_kf2_c>$lmp2) {
                $h3_t2_c2='Se supera el NMP';
                $h3_t2_c2_style='style="color: red;font-weight: bolder;"';
            }else{
                $h3_t2_c2='';
                $h3_t2_c2_style='';
            }
            //================================================
            $arrayniveliluminacion[] = array(
                                            'punto'=>$item->num_punto,
                                            'identificacion'=>$item->identificacion,
                                            'nmi'=>$item->nmi,
                                            'h1'=>$item->h1,
                                            'h1_n'=>round($h1_pcorr,2),
                                            'h1_u'=>round($h1_u_lx,2),
                                            //'h1_nu'=>round($h1_pcorr,2)+round($h1_u_lx,2),
                                            'h1_nu'=>round($h1_pcorr,2),
                                            'h1_c'=>$h1_conclusion,
                                            'h1_c_style'=>$h1_conclusion_style,
                                            'h2'=>$item->h2,
                                            'h2_n'=>round($h2_pcorr,2),
                                            'h2_u'=>round($h2_u_lx,2),
                                            //'h2_nu'=>round($h2_pcorr,2)+round($h2_u_lx,2),
                                            'h2_nu'=>round($h2_pcorr,2),
                                            'h2_c'=>$h2_conclusion,
                                            'h2_c_style'=>$h2_conclusion_style,
                                            'h3'=>$item->h3,
                                            'h3_n'=>round($h3_pcorr,2),
                                            'h3_u'=>round($h3_u_lx,2),
                                            //'h3_nu'=>round($h3_pcorr,2)+round($h3_u_lx,2),
                                            'h3_nu'=>round($h3_pcorr,2),
                                            'h3_c'=>$h3_conclusion,
                                            'h3_c_style'=>$h3_conclusion_style,
                                            'h1_kf1'=>round($h1_kf_c,2),
                                            'h2_kf1'=>round($h2_kf_c,2),
                                            'h3_kf1'=>round($h3_kf_c,2),
                                            'h1_t2_c'=>$h1_t2_c,
                                            'h2_t2_c'=>$h2_t2_c,
                                            'h3_t2_c'=>$h3_t2_c,
                                            'h1_t2_c_style'=>$h1_t2_c_style,
                                            'h2_t2_c_style'=>$h2_t2_c_style,
                                            'h3_t2_c_style'=>$h3_t2_c_style,
                                            'lmp1'=>$lmp,
                                            'h1_kf2'=>round($h1_kf2_c,2),
                                            'h2_kf2'=>round($h2_kf2_c,2),
                                            'h3_kf2'=>round($h3_kf2_c,2),
                                            'lmp2'=>$lmp2,
                                            'h1_t2_c2'=>$h1_t2_c2,
                                            'h2_t2_c2'=>$h2_t2_c2,
                                            'h3_t2_c2'=>$h3_t2_c2,
                                            'h1_t2_c2_style'=>$h1_t2_c2_style,
                                            'h2_t2_c2_style'=>$h2_t2_c2_style,
                                            'h3_t2_c2_style'=>$h3_t2_c2_style

                                        );
        ?>
        <div class="exportar_punto_<?php echo $item->id ;?>" id="exportar_punto_<?php echo $rowpunto; ?>">
            <div class="row">
                <table class="table table-bordered">
                    <tr>
                        <td class="titulos"><strong>Fecha</strong></td>
                        <td class="title_tb"><?php echo $item->fecha;?></td>
                        <td class="titulos"><strong>Id. del equipo</strong></td>
                        <td class="title_tb"><?php echo $equipo;?></td>
                    </tr>
                </table>
                <div class="col-md-3" >
                    <table class="table table-bordered">
                        <tr>
                            <td class="titulos titulos-12"><strong>No. Punto</strong></td>
                            <td class="camposi title_tb">
                                <?php echo $item->num_punto;?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-1" ></div>
                <div class="col-md-4 padding0" >
                    <table class="table table-bordered">
                        <tr>
                            <td class="titulos titulos-12"><strong>Identificación</strong></td>
                            <td class="camposi title_tb">
                                <?php echo $item->identificacion;?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3 graficadiv">
                    <canvas style="margin-left:115px; margin-top: 1px" id="myChartg<?php echo $rowpunto;?>" height="190px"></canvas>
                </div>
                <div class="col-md-9">
                    <table class="table table-bordered" >
                        <thead>
                            <tr class="titulos">
                                <th>Horario</th>
                                <th colspan="3">Medición plano de trabajo (lx)</th>
                                <th>Prom. (lx)</th>
                                <th>P Corr (lx)</th>
                                <th>U (lx)</th>
                                <th>NMI</th>
                                <th>Conclusión</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="title_tb"><?php if(date("H:i", strtotime($item->h1))!="00:00") echo date("H:i", strtotime($item->h1)); else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h1_medicion_a>0) echo $item->h1_medicion_a; else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h1_medicion_b>0) echo $item->h1_medicion_b; else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h1_mdicion_c>0) echo $item->h1_mdicion_c; else echo "--";?></td>
                                <td><?php if($h1promedio>0) echo round($h1promedio,2); else echo "--"; ?></td>
                                <td><?php if($h1_pcorr>0) echo round($h1_pcorr,2); else echo "--"; ?></td>
                                <td><?php if($h1_pcorr>0) echo round($h1_u_lx,2); else echo "--"; ?></td>
                                <td class="title_tb" rowspan="3"><strong><?php echo $item->nmi ;?></strong></td>
                                <td ><?php if($h1_pcorr>0) echo $h1_conclusion; else echo "--"; ?></td>
                            </tr>
                            <tr>
                                <td class="title_tb"><?php if(date("H:i", strtotime($item->h2))!="00:00") echo date("H:i", strtotime($item->h2)); else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h2_medicion_a>0) echo $item->h2_medicion_a; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h2_medicion_b>0) echo $item->h2_medicion_b; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h2_medicion_c>0) echo $item->h2_medicion_c; else echo "--"; ?></td>
                                <td><?php if($h2promedio>0) echo round($h2promedio,2); else echo "--"; ?></td>
                                <td><?php if($h2_pcorr>0) echo round($h2_pcorr,2); else echo "--"; ?></td>
                                <td><?php if($h2_pcorr>0) echo round($h2_u_lx,2); else echo "--";?></td>
                                <td ><?php if($h2_pcorr>0) echo $h2_conclusion; else echo "--"; ?></td>
                            </tr>
                            <tr>
                                <td class="title_tb"><?php if(date("H:i", strtotime($item->h3))!="00:00") echo date("H:i", strtotime($item->h3)); else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h3_medicion_a>0) echo $item->h3_medicion_a; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h3_medicion_b>0) echo $item->h3_medicion_b; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h3_medicion_c>0) echo $item->h3_medicion_c; else echo "--"; ?></td>
                                <td><?php if($h3promedio>0) echo round($h3promedio,2); else echo "--"; ?></td>
                                <td><?php if($h3_pcorr>0) echo round($h3_pcorr,2); else echo "--"; ?></td>
                                <td><?php if($h3_pcorr>0) echo round($h3_u_lx,2); else echo "--";?></td>
                                <td ><?php if($h3_pcorr>0) echo $h3_conclusion; else echo "--";?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="titulos" rowspan="2">Horario</th>
                                <th class="titulos" colspan="2">R - PT  (lx)</th>
                                <th class="titulos" colspan="2">R -PT Corregido (lx)</th>
                                <th class="titulos" rowspan="2">Kf (%)</th>
                                <th class="titulos" rowspan="2">NMP (%) </th>
                                <th class="titulos" rowspan="2">Conclusión</th>
                                <th class="titulos" colspan="2">R - P  (lx)</th>
                                <th class="titulos" colspan="2">R - P  Corregido (lx)</th>
                                <th class="titulos" rowspan="2">Fc (%)</th>
                                <th class="titulos" rowspan="2">NMP (%)</th>
                                <th class="titulos" rowspan="2">Conclusión</th>
                            </tr>
                            <tr>
                                <th class="titulos">E1</th>
                                <th class="titulos">E2</th>
                                <th class="titulos">E1</th>
                                <th class="titulos">E2</th>
                                <th class="titulos">E1</th>
                                <th class="titulos">E2</th>
                                <th class="titulos">E1</th>
                                <th class="titulos">E2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php if(date("H:i", strtotime($item->h1))!="00:00") echo date("H:i", strtotime($item->h1)); else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h1_e1a>0) echo $item->h1_e1a; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h1_e1a>0) echo $item->h1_e2a; else echo "--"; ?></td>
                                <td><?php if($item->h1_e1a>0) echo round($h1_e1a_c,2); else echo "--";?></td>
                                <td><?php if($item->h1_e1a>0) echo round($h1_e2a_c,2); else echo "--";?></td>
                                
                                <td><?php echo round($h1_kf_c,1)?></td>
                                <td rowspan="3" class="titulos"><strong><?php echo $lmp;?></strong></td>
                                <td ><?php if($item->h1_e1a>0) echo $h1_t2_c; else echo "--";?></td>
                                <td><?php if($item->h1_e1b>0) echo $item->h1_e1b; else echo "--";?></td>
                                <td><?php if($item->h1_e1b>0) echo $item->h1_e2b; else echo "--";?></td>
                                <td ><?php if($item->h1_e1b>0) echo round($h1_e1b_c,2); else echo "--";?></td>
                                <td ><?php if($item->h1_e1b>0) echo round($h1_e2b_c,2); else echo "--";?></td>
                                <td ><?php if($item->h1_e1b>0) echo round($h1_kf2_c,2); else echo "--";?></td>
                                <td class="titulos" rowspan="3"><strong><?php echo $lmp2;?></strong></td>
                                <td ><?php if($item->h1_e1b>0) echo $h1_t2_c2; else echo "--"; ?></td>
                            </tr>
                            <tr>
                                <td><?php if(date("H:i", strtotime($item->h2))!="00:00") echo date("H:i", strtotime($item->h2)); else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h2_e1a>0) echo $item->h2_e1a ; else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h2_e1a>0) echo $item->h2_e2a ; else echo "--";?></td>
                                <td><?php if($item->h2_e1a>0) echo round($h2_e1a_c,2); else echo "--";?></td>
                                <td><?php if($item->h2_e1a>0) echo round($h2_e2a_c,2); else echo "--";?></td>
                                <td><?php if($item->h2_e1a>0) echo round($h2_kf_c,2); else echo "--";?></td>
                                <td><?php if($item->h2_e1a>0) echo $h2_t2_c; else echo "--";?></td>
                                <td><?php if($item->h2_e1b>0) echo $item->h2_e1b; else echo "--";?></td>
                                <td><?php if($item->h2_e1b>0) echo $item->h2_e2b; else echo "--";?></td>
                                <td ><?php if($item->h2_e1b>0) echo round($h2_e1b_c,2); else echo "--";?></td>
                                <td ><?php if($item->h2_e1b>0) echo round($h2_e2b_c,2); else echo "--";?></td>
                                <td ><?php if($item->h2_e1b>0) echo round($h2_kf2_c,2); else echo "--";?></td>
                                <td ><?php if($item->h2_e1b>0) echo $h2_t2_c2;  else echo "--";?></td>
                            </tr>
                            <tr>
                                <td><?php if(date("H:i", strtotime($item->h3))!="00:00") echo date("H:i", strtotime($item->h3)); else echo "--";?></td>
                                <td class="title_tb"><?php if($item->h3_e1a>0) echo $item->h3_e1a; else echo "--"; ?></td>
                                <td class="title_tb"><?php if($item->h3_e1a>0) echo $item->h3_e2a; else echo "--"; ?></td>
                                <td><?php if($item->h3_e1a>0) echo round($h3_e1a_c,2); else echo "--"; ?></td>
                                <td><?php if($item->h3_e1a>0) echo round($h3_e2a_c,2); else echo "--"; ?></td>
                                <td><?php if($item->h3_e1a>0) echo round($h3_kf_c,2); else echo "--"; ?></td>
                                <td ><?php if($item->h3_e1a>0) echo $h3_t2_c; else echo "--"; ?></td>
                                <td><?php if($item->h3_e1b>0) echo $item->h3_e1b; else echo "--"; ?></td>
                                <td><?php if($item->h3_e1b>0) echo $item->h3_e2b; else echo "--"; ?></td>
                                <td ><?php if($item->h3_e1b>0) echo round($h3_e1b_c,2); else echo "--"; ?></td>
                                <td ><?php if($item->h3_e1b>0) echo round($h3_e2b_c,2); else echo "--"; ?></td>
                                <td ><?php if($item->h3_e1b>0) echo round($h3_kf2_c,2); else echo "--"; ?></td>
                                <td ><?php if($item->h3_e1b>0) echo $h3_t2_c2;  else echo "--"; ?></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" >
                <table class="table table-bordered">
                    <tr>
                        <td class="titulos titulos-12">No. Punto</td>
                        <td class="camposi">
                            <?php echo $item->num_punto;?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12" style="text-align:center;">
                Repetibilidad                                                               
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr class="titulos">
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>Prom. (lx)</th>
                            <th>Prom. Corregido (lx)</th>
                            <th>Desv. Estándar (lX)</th>
                            <th>Tipo de distribucíon</th>
                            <th>Coef. Sensibilidad (Ci)</th>
                            <th>Contribución (U(y))</th>
                            <th>(U(y))2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php if($item->h1_medicion_a>0) echo $item->h1_medicion_a ; else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo $item->h1_medicion_b ; else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo $item->h1_mdicion_c ; else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo round($h1promedio,2); else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo round($h1_pcorr,2);  else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo round($h1_desv_estandar,4); else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo $h1_tipo_distribucion; else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo $h1_coef_sensibilidad;  else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo round($h1_contribuccion,4); else echo "--";?></td>
                            <td><?php if($item->h1_medicion_a>0) echo round($h1_uy2,4); else echo "--";?></td>
                        </tr>
                        <tr>
                            <td><?php if($item->h2_medicion_a>0) echo $item->h2_medicion_a ; else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo $item->h2_medicion_b ; else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo $item->h2_medicion_c ; else echo"--";?></td>
                            <td><?php if($item->h2_medicion_a>0) echo round($h2promedio,2); else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo round($h2_pcorr,2);  else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo round($h2_desv_estandar,4); else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo $h2_tipo_distribucion; else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo $h2_coef_sensibilidad;  else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo round($h2_contribuccion,4); else echo "--"; ?></td>
                            <td><?php if($item->h2_medicion_a>0) echo round($h2_uy2,4); else echo "--"; ?></td>
                        </tr>
                        <tr>
                            <td><?php if($item->h3_medicion_a>0) echo $item->h3_medicion_a ; else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo $item->h3_medicion_b ; else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo $item->h3_medicion_c ; else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo round($h3promedio,2); else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo round($h3_pcorr,2);  else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo round($h3_desv_estandar,4); else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo $h3_tipo_distribucion; else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo $h3_coef_sensibilidad;  else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo round($h3_contribuccion,4); else echo"--";?></td>
                            <td><?php if($item->h3_medicion_a>0) echo round($h3_uy2,4); else echo"--";?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                Resolución del Equipo                                                               
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr class="titulos">
                            <th>Resolución (lx)</th>
                            <th>Tipo de distribución</th>
                            <th>U estándar U(Xi) (lx)</th>
                            <th>Coef. Sensibilidad (Ci)</th>
                            <th>Contribución (U(y))</th>
                            <th>(U(y))2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $h1_resolicion;?></td>
                            <td><?php echo $distribucion1;?></td>
                            <td><?php echo round($h1_u_estandar,4); ?></td>
                            <td><?php echo $h1_coef_sensibilidad; ?></td>
                            <td><?php echo round($h1_contribuccion_rq,4);?></td>
                            <td><?php echo round($h1_uy2_rq,4);?></td>
                        </tr>
                        <tr>
                            <td><?php echo $h2_resolicion;?></td>
                            <td><?php echo $distribucion2;?></td>
                            <td><?php echo round($h2_u_estandar,4); ?></td>
                            <td><?php echo $h2_coef_sensibilidad; ?></td>
                            <td><?php echo round($h2_contribuccion_rq,4);?></td>
                            <td><?php echo round($h2_uy2_rq,4);?></td>
                        </tr>
                        <tr>
                            <td><?php echo $h3_resolicion;?></td>
                            <td><?php echo $distribucion3;?></td>
                            <td><?php echo round($h3_u_estandar,4); ?></td>
                            <td><?php echo $h3_coef_sensibilidad; ?></td>
                            <td><?php echo round($h3_contribuccion_rq,4);?></td>
                            <td><?php echo round($h3_uy2_rq,4);?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr class="titulos">
                            <th>Escala</th>
                            <th>1ra</th>
                            <th>2da</th>
                            <th>3ra</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>0-99.99</td>
                            <td><?php echo $escala_1_a; ?></td>
                            <td><?php echo $escala_1_b; ?></td>
                            <td><?php echo $escala_1_c; ?></td>
                        </tr>
                        <tr>
                            <td>100-999.9</td>
                            <td><?php echo $escala_2_a; ?></td>
                            <td><?php echo $escala_2_b; ?></td>
                            <td><?php echo $escala_2_c; ?></td>
                        </tr>
                        <tr>
                            <td>1000-9999.9</td>
                            <td><?php echo $escala_3_a; ?></td>
                            <td><?php echo $escala_3_b; ?></td>
                            <td><?php echo $escala_3_c; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12" style="text-align:center;">
                Calibración
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr class="titulos">
                            <th>U declarada en el IR (%)</th>
                            <th>U declarada en el IR (lx)</th>
                            <th>Tipo de distribución</th>
                            <th>U estándar U(Xi) (lx)</th>
                            <th>Coef. Sensibilidad (Ci)</th>
                            <th>Contribución (U(y))</th>
                            <th>(U(y))2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo round($h1_u_declarada_en_el_IR,2);?></td>
                            <td><?php echo $h1_u_declarada_en_el_IR_lx;?></td>
                            <td>B, normal, k=2</td>
                            <td><?php echo $h1_u_declarada_en_el_IR_lx/2;?></td>
                            <td><?php echo $h1_coef_sensibilidad; ?></td>
                            <td><?php echo $h1_contribucion_c;?></td>
                            <td><?php echo $h1_uy2_c;?></td>
                        </tr>
                        <tr>
                            <td><?php echo round($h2_u_declarada_en_el_IR,2);?></td>
                            <td><?php echo $h2_u_declarada_en_el_IR_lx;?></td>
                            <td>B, normal, k=2</td>
                            <td><?php echo $h2_u_declarada_en_el_IR_lx/2;?></td>
                            <td><?php echo $h2_coef_sensibilidad; ?></td>
                            <td><?php echo $h2_contribucion_c;?></td>
                            <td><?php echo $h2_uy2_c;?></td>
                        </tr>
                        <tr>
                            <td><?php echo round($h3_u_declarada_en_el_IR,2);?></td>
                            <td><?php echo $h3_u_declarada_en_el_IR_lx;?></td>
                            <td>B, normal, k=2</td>
                            <td><?php echo $h3_u_declarada_en_el_IR_lx/2;?></td>
                            <td><?php echo $h3_coef_sensibilidad; ?></td>
                            <td><?php echo $h3_contribucion_c;?></td>
                            <td><?php echo $h3_uy2_c;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr class="titulos">
                            <th>SUMA (U(y))2</th>
                            <th>Uc (lx)</th>
                            <th>U (lx)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo round($h1_uy2_suma,4);?></td>
                            <td><?php echo round($h1_uc_lx,4);?></td>
                            <td><?php echo round($h1_u_lx,2); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo round($h2_uy2_suma,4);?></td>
                            <td><?php echo round($h2_uc_lx,4);?></td>
                            <td><?php echo round($h2_u_lx,2); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo round($h3_uy2_suma,4);?></td>
                            <td><?php echo round($h3_uc_lx,4);?></td>
                            <td><?php echo round($h3_u_lx,2); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function($) {
                
                var ctx = document.getElementById("myChartg<?php echo $rowpunto;?>").getContext('2d');
                var barChart = new Chart(ctx, {
                  type: 'bar',
                  data: {
                    labels: [""],
                    datasets: [
                                {
                                  label: '1ra Lectura',
                                  data: [<?php echo round($h1_pcorr,2); ?>],
                                  backgroundColor: "#548235"
                                }, {
                                  label: 'NMI',
                                  data: [<?php echo $item->nmi ;?>],
                                  backgroundColor: "rgba(255,0,0,1)"
                                },
                                {
                                  label: '2da Lectura',
                                  data: [<?php echo round($h2_pcorr,2); ?>],
                                  backgroundColor: "#A5A5A5"
                                }, {
                                  label: 'NMI',
                                  data: [<?php echo $item->nmi ;?>],
                                  backgroundColor: "rgba(255,0,0,1)"
                                },
                                {
                                  label: '3ra Lectura',
                                  data: [<?php echo round($h3_pcorr,2); ?>],
                                  backgroundColor: "#2F528F"
                                }, {
                                  label: 'NMI',
                                  data: [<?php echo $item->nmi ;?>],
                                  backgroundColor: "rgba(255,0,0,1)"
                                },

                            ]
                  },
                  options: {
                    animation: {
                        duration:0
                    },
                    title: {
                        display: true,
                        text: 'Luxes',
                        fontSize:18,
                        position:'left'
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          beginAtZero: true,
                        }
                      }]
                    }
                  }
                });

                setTimeout(function(){
                    exportarpunto(<?php echo $rowpunto ;?>,<?php echo $item->id ;?>);
                }, <?php echo $rowpuntoset;?>);
            });
            
            /*setTimeout(function(){ //guarda las tablas de conclusion del inciso A del PDF
                html2canvas(document.querySelector("#tablageneral1")).then(canvas => {
                document.body.appendChild(canvas).setAttribute('id','canvastg1');
                    savecanvastg1();
                    //savecanvastg1char();
                });
                html2canvas(document.querySelector("#tablageneral2")).then(canvas => {
                document.body.appendChild(canvas).setAttribute('id','canvastg2');
                    savecanvastg2();
                    //savecanvastg2char();
                });
            }, 6500);*/
     
        </script>
    <?php
        $rowpunto++; 
        $rowpuntoset=$rowpuntoset+950;
        } 
    ?>
    <div class="row">
        <div class="col-md-12" id="tablageneral1">
            <table class="table-bordered" style="text-align:center; font-weight: bold; font-size: 15px;" width="100%">
                <thead>
                    <tr>
                        <th width="5%" class="titulos titulos-12">No. de Punto</th>
                        <th width="40%" class="titulos titulos-12">Identificación del punto de medición (área / identificación)</th>
                        <th width="10%" class="titulos titulos-12">Horario <br>de <br>medición</th>
                        <th width="10%" class="titulos titulos-12">Nivel de iluminación</th>
                        <th width="10%" class="titulos titulos-12">U exp (lx)</th>
                        <!--<th width="10%" class="titulos titulos-12">Nivel de iluminación + U exp (lx)</th>-->
                        <th width="5%" class="titulos titulos-12">NMI (lx)</th>
                        <th width="20%" class="titulos titulos-12">Conclusión</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($arrayniveliluminacion as $item) { ?>
                        <tr>
                            <td rowspan="3"><?php echo $item['punto']?></td>
                            <td rowspan="3"><?php echo $item['identificacion']?></td>
                            <td><?php if(date("H:i", strtotime($item['h1']))!="00:00") echo date("H:i", strtotime($item['h1'])); else echo "---"; ?></td>
                            <?php if($item['h1_n']==0){ ?><td>---</td><td>---</td><td rowspan="3"><?php echo $item['nmi']?></td><td>---</td><?php }else{ ?>
                            <td><?php echo number_format($item['h1_n'], 2, '.', '');?></td>
                            <td><?php echo number_format($item['h1_u'], 2, '.', '');?></td>
                            <!--<td><?php echo number_format($item['h1_nu'], 2, '.', '');?></td>-->
                            <td rowspan="3"><?php echo $item['nmi']?></td>
                            <td  <?php echo $item['h1_c_style'];?>><?php echo $item['h1_c'];?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?php if(date("H:i", strtotime($item['h2']))!="00:00") echo date("H:i", strtotime($item['h2'])); else echo "---"; ?></td>
                            <?php if($item['h2_n']==0){ ?><td>---</td><td>---</td><td>---</td><?php }else{ ?>
                            <td><?php echo number_format($item['h2_n'], 2, '.', '');?></td>
                            <td><?php echo number_format($item['h2_u'], 2, '.', '');?></td>
                            <!--<td><?php echo number_format($item['h2_nu'], 2, '.', '');?></td>-->
                            <td <?php echo $item['h2_c_style'];?>><?php echo $item['h2_c'];?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?php if(date("H:i", strtotime($item['h3']))!="00:00") echo date("H:i", strtotime($item['h3'])); else echo "---"; ?></td>
                            <?php if($item['h3_n']==0){ ?><td>---</td><td>---</td><td>---</td><?php }else{ ?>
                            <td><?php echo number_format($item['h3_n'], 2, '.', '');?></td>
                            <td><?php echo number_format($item['h3_u'], 2, '.', '');?></td>
                            <!--<td><?php echo number_format($item['h3_nu'], 2, '.', '');?></td>-->
                            <td <?php echo $item['h3_c_style'];?> ><?php echo $item['h3_c'];?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    
                </tbody>
            </table> 
        </div>
        <!--<div class="col-md-4">
            <canvas id="myChartgtg1" width="400" height="300"></canvas>
        </div>-->
    </div>
    <div class="row">
        <div class="col-md-12" id="tablageneral2">
            <br>
            <table class=" table-bordered" style="text-align:center; font-weight: bold; font-size: 15px;">
                <thead>
                    <tr>
                        <th width="5%" class="titulos titulos-12" rowspan="2">No. de Punto</th>
                        <th width="20%" class="titulos titulos-12" rowspan="2">Identificación del punto de medición (área/identificación)</th>
                        <th width="10%" class="titulos titulos-12" rowspan="2">Horario de medición</th>
                        <th width="32%" class="titulos titulos-12" colspan="3">Reflexión plano de trabajo</th>
                        <th width="33%" class="titulos titulos-12" colspan="3">Reflexión pared</th>
                    </tr>
                    <tr>
                        <th class="titulos titulos-12">Kf (%)</th>
                        <th class="titulos titulos-12">NMP (%)</th>
                        <th class="titulos titulos-12">Conclusión</th>
                        <th class="titulos titulos-12">Kf (%)</th>
                        <th class="titulos titulos-12">NMP (%)</th>
                        <th class="titulos titulos-12">Conclusión</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($arrayniveliluminacion as $item) { ?>
                        <tr>
                            <td rowspan="3"><?php echo $item['punto']?></td>
                            <td rowspan="3"><?php echo $item['identificacion']?></td>
                            <td><?php if(date("H:i", strtotime($item['h1']))!="00:00") echo date("H:i", strtotime($item['h1'])); else echo "---";?></td>
                            <td><?php if($item['h1_n']==0){ echo '---';}else{echo number_format($item['h1_kf1'], 2, '.', '');}?></td>
                            <td rowspan="3"><?php echo $item['lmp1']?></td>
                            <?php if($item['h1_n']==0){ echo '<td>---</td>';}else{ ?> <td <?php echo $item['h1_t2_c_style'];?>><?php echo $item['h1_t2_c'];?></td> <?php } ?>

                            <td><?php if($item['h1_kf2']==0){ echo '---';}else{echo number_format($item['h1_kf2'], 2, '.', '');}?></td>
                            <td rowspan="3"><?php echo $item['lmp2']?></td>
                            <?php if($item['h1_kf2']==0){ echo '<td>---</td>';}else{ ?> <td <?php echo $item['h1_t2_c2_style'];?>><?php echo $item['h1_t2_c2'];?></td><?php } ?>
                        </tr>
                        <tr>
                            <td><?php if(date("H:i", strtotime($item['h2']))!="00:00") echo date("H:i", strtotime($item['h2'])); else echo "---";?></td>
                            <td><?php if($item['h2_n']==0){ echo '---';}else{echo number_format($item['h2_kf1'], 2, '.', '');}?></td>
                            <?php if($item['h2_n']==0){ echo '<td>---</td>';}else{ ?><td <?php echo $item['h2_t2_c_style'];?>><?php echo $item['h2_t2_c'];?></td><?php } ?>

                            <td><?php if($item['h2_kf2']==0){ echo '---';}else{echo number_format($item['h2_kf2'], 2, '.', '');}?></td>
                            <?php if($item['h2_kf2']==0){ echo '<td>---</td>';}else{ ?><td <?php echo $item['h2_t2_c2_style'];?>><?php echo $item['h2_t2_c2'];?></td><?php } ?>
                        </tr>
                        <tr>
                            <td><?php if(date("H:i", strtotime($item['h3']))!="00:00") echo date("H:i", strtotime($item['h3'])); else echo "---";?></td>
                            <td><?php if($item['h3_n']==0){ echo '---';}else{ echo number_format($item['h3_kf1'], 2, '.', '');}?></td>
                            <?php if($item['h3_n']==0){ echo '<td>---</td>';}else{ ?><td <?php echo $item['h3_t2_c_style'];?>><?php echo $item['h3_t2_c'];?></td><?php } ?>

                            <td><?php if($item['h3_kf2']==0){ echo '---';}else{echo number_format($item['h3_kf2'], 2, '.', '');}?></td>
                            <?php if($item['h3_kf2']==0){ echo '<td>---</td>';}else{ ?><td <?php echo $item['h3_t2_c2_style'];?>><?php echo $item['h3_t2_c2'];?></td><?php } ?>
                        </tr>
                    <?php } ?>
                    
                </tbody>
            </table> 
        </div>
        <div class="col-md-4 divmyChartgtg2" id="divmyChartgtg2">
            <!--<canvas id="myChartgtg2" width="400" height="300"></canvas>
            <canvas id="myChartgtg3" width="400" height="300"></canvas>-->
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function($) {
            $.blockUI({ 
                message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
                    <i class="fa fa-spinner fa-spin"></i>\
                  </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            });
            setTimeout(function(){
                $.unblockUI();
            }, <?php echo $rowpuntoset;?>);   
        });

        function exportarpunto(punto,id){ //modificar para guardar solo la grafica, 
            console.log("exportarpunto: "+punto);
            /*html2canvas(document.querySelector("#exportar_punto_"+punto)).then(canvas => {
            document.body.appendChild(canvas).setAttribute('id','canvasmt_'+punto);
                savecanvas(punto);
            });*/
            savecanvas(punto,id);
        }

        function savecanvas(punto,id){
            var base_url = $('#base_url').val();
            var canvasx= document.getElementById("myChartg"+punto);
            var dataURL = canvasx.toDataURL('image/png', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/nom25viewinserupdatechart',
                async:false,
                data: {
                      id:id, //mandar id
                      grafica:dataURL
                },
                statusCode:{
                  404: function(data){
                      
                  },
                  500: function(){
                      
                  }
              },
              success:function(data){
                  console.log("hecho, punto: "+punto);
              }
          });
        }
        function savecanvastg1(){
            var base_url = $('#base_url').val();
            var canvasx= document.getElementById("canvastg1");
            var dataURL = canvasx.toDataURL('image/jpeg', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/nominserupdatetableg1',
                async:false,
                data: {
                      id:$('#idnom').val(),
                      grafica:dataURL
                },
                statusCode:{
                  404: function(data){
                      
                  },
                  500: function(){
                      
                  }
              },
              success:function(data){
                  console.log("hecho");
              }
          });
        }
        function savecanvastg2(){
            var base_url = $('#base_url').val();
            var canvasx= document.getElementById("canvastg2");
            var dataURL = canvasx.toDataURL('image/jpeg', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/nominserupdatetableg2',
                async:false,
                data: {
                      id:$('#idnom').val(),
                      grafica:dataURL
                },
                statusCode:{
                  404: function(data){
                      
                  },
                  500: function(){
                      
                  }
              },
              success:function(data){
                  console.log("hecho");
              }
          });
        }
        function graficageneral1(){
            /*var ctx = document.getElementById('myChartgtg1').getContext('2d');
            var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: [
                            <?php 
                                
                            foreach ($arrayniveliluminacion as $item) { 
                                
                            ?>
                                "Punto <?php echo $item['punto']?>",
                            <?php } ?>
                        ],
                datasets: [
                        

                        { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h1_nu'] ?>,
                                    <?php } ?>
                                  ],
                            label: "1ra",
                            backgroundColor: "#2F528F",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h2_nu'] ?>,
                                    <?php } ?>
                                  ],
                            label: "2da",
                            backgroundColor: "#ff9800",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h3_nu'] ?>,
                                    <?php } ?>
                                  ],
                            label: "3ra",
                            backgroundColor: "#A5A5A5",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['nmi'] ?>,
                                    <?php } ?>
                                  
                            ],
                            label: "NMI",
                            backgroundColor: "#548235",
                          }, 
                  
                ],

              },
              options: {
                    title: {
                        display: true,
                        text: 'Niveles de iluminación',
                        fontSize:18,
                        position:'top'
                    },
                    subtitle: {
                        display: true,
                        text: 'Luxes',
                        fontSize:18,
                        position:'left'
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          beginAtZero: true
                        }
                      }]
                    }
                  }
            });*/
        }
        function graficageneral2(){
            /*var ctx = document.getElementById('myChartgtg2').getContext('2d');
            var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: [
                            <?php 
                                
                            foreach ($arrayniveliluminacion as $item) { 
                                
                            ?>
                                "Punto <?php echo $item['punto']?>",
                            <?php } ?>
                        ],
                datasets: [
                        

                        { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h1_kf1'] ?>,
                                    <?php } ?>
                                  ],
                            label: "1ra",
                            backgroundColor: "#2F528F",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h2_kf1'] ?>,
                                    <?php } ?>
                                  ],
                            label: "2da",
                            backgroundColor: "#ff9800",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h3_kf1'] ?>,
                                    <?php } ?>
                                  ],
                            label: "3ra",
                            backgroundColor: "#A5A5A5",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['lmp1'] ?>,
                                    <?php } ?>
                                  
                            ],
                            label: "NMP",
                            backgroundColor: "#548235",
                          }, 
                  
                ],

              },
              options: {
                    title: {
                        display: true,
                        text: 'Niveles de Reflexión',
                        fontSize:18,
                        position:'top'
                    },
                    subtitle: {
                        display: true,
                        text: 'Luxes',
                        fontSize:18,
                        position:'left'
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          beginAtZero: true
                        }
                      }]
                    }
                  }
            });

            var ctx = document.getElementById('myChartgtg3').getContext('2d');
            var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: [
                            <?php 
                                
                            foreach ($arrayniveliluminacion as $item) { 
                                
                            ?>
                                "Punto <?php echo $item['punto']?>",
                            <?php } ?>
                        ],
                datasets: [
                        

                        { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h1_kf2'] ?>,
                                    <?php } ?>
                                  ],
                            label: "1ra",
                            backgroundColor: "#2F528F",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h2_kf2'] ?>,
                                    <?php } ?>
                                  ],
                            label: "2da",
                            backgroundColor: "#ff9800",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['h3_kf2'] ?>,
                                    <?php } ?>
                                  ],
                            label: "3ra",
                            backgroundColor: "#A5A5A5",
                          }, 
                          { 
                            data: [
                                    <?php foreach ($arrayniveliluminacion as $item) {  ?>
                                        <?php echo $item['lmp2'] ?>,
                                    <?php } ?>
                                  
                            ],
                            label: "NMP",
                            backgroundColor: "#548235",
                          }, 
                  
                ],

              },
              options: {
                    title: {
                        display: true,
                        text: 'Niveles de Reflexión',
                        fontSize:18,
                        position:'top'
                    },
                    subtitle: {
                        display: true,
                        text: 'Luxes',
                        fontSize:18,
                        position:'left'
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          beginAtZero: true
                        }
                      }]
                    }
                  }
            });*/
        }
        function savecanvastg1char(){
            var base_url = $('#base_url').val();
            var canvasx= document.getElementById("myChartgtg1");
            var dataURL = canvasx.toDataURL('image/png', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/nominserupdatetableg1char',
                async:false,
                data: {
                      id:$('#idnom').val(),
                      grafica:dataURL
                },
                statusCode:{
                  404: function(data){
                      
                  },
                  500: function(){
                      
                  }
              },
              success:function(data){
                  console.log("hecho");
              }
          });
        }
        function savecanvastg2char(){
            var base_url = $('#base_url').val();
            var canvasx= document.getElementById("canvasmyChartgtg2");
            var dataURL = canvasx.toDataURL('image/png', 1);
            $.ajax({
                type:'POST',
                url: base_url+'Nom/nominserupdatetableg2char',
                async:false,
                data: {
                      id:$('#idnom').val(),
                      grafica:dataURL
                },
                statusCode:{
                  404: function(data){
                      
                  },
                  500: function(){
                      
                  }
              },
              success:function(data){
                  console.log("hecho");
              }
          });
        }
        
    </script>