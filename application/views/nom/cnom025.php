<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
        padding: 6px !important;
        font-size: 10px;
    }
    textarea{  
        overflow: hidden;
        white-space: nowrap;
    }

    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 8.5px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 8.5px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reco1{
            margin-top: 0px;
            font-size: 8.5px !important;
        }
    }
    .chk_form {
        margin-top: 15%;
        transform: scale(1.1);
    }

    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 8.5px !important;
        }
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_nom">
                            <input type="hidden" id="idnom" name="idnom" value="<?php echo $idnom;?>">
                            <input type="hidden" name="idordenes" value="<?php echo $orden;?>">
                            <input type="hidden" name="id_chs" id="id_chs" value="<?php echo $id_chs;?>">
                            <input type="hidden" name="nom" value="25">

                            <div class="row" id="cont_fechas_realiza">
                                <div class="col-md-3 col-sm-6 col-6">
                                    <label>Fechas realizadas</label>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6 form-group">
                                    <select class="form-control" id="fechas_realiza">
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="display: none;">
                                <div class="col-md-4" >
                                    <label>Razon Social</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" name="razon_social" id="razon_social" value="<?php echo $razonsocial;?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Razón Social</label>
                                </div>
                                <div class="col-md-9 form-group">
                                    <input type="text" class="form-control" id="cliente" value="<?php echo $razon_cli;?>" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-6">
                                    <label>Fecha</label>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6 form-group">
                                    <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $fecha;?>">
                                </div>
                                <div class="col-md-3 col-sm-6 col-6">
                                    <label>No. de serie de equipo</label>
                                </div>
                                <div class="col-md-3 col-sm-6 col-6 form-group">
                                    <select class="form-control" id="equipo" name="equipo">
                                        <?php foreach ($resultequipo->result() as $item) { ?>
                                                <option value="<?php echo $item->id;?>"><?php echo $item->equipo;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>   
                            <div class="row">
                                <label class="col-md-3">Verificación inicial</label>
                                <div class="col-md-3">
                                    <input type="text" name="veri_ini" id="veri_ini" class="form-control">
                                </div>
                                <label class="col-md-3">Verificación final</label>
                                <div class="col-md-3">     
                                    <input type="text" name="veri_fin" id="veri_fin" class="form-control">
                                </div>
                                <div class="col-md-12">     
                                    <br>
                                </div>
                                <label class="col-md-3">Criterio de aceptación</label>
                                <div class="col-md-3"> 
                                    <input type="text" name="criterio" id="criterio" class="form-control" value="+- 5% respecto al valor de referencia">
                                </div>
                                <label class="col-md-3">Cumple criterio</label>
                                <div class="col-md-3">
                                    <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control">
                                </div>
                                <div class="col-md-12">     
                                    <br>
                                </div>
                                <label class="col-md-3">Observaciones</label>
                                <div class="col-md-9">
                                    <input type="text" name="observaciones" id="observaciones" class="form-control">
                                </div>
                            </div>  
                        </form>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <br>
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()"><i class="fa fa-plus" aria-hidden="true"></i> Agregar punto</button>
                            </div>
                        </div>
                        <table class="table table-responsive" id="table_puntos">
                            <tbody class="trbody">
                                
                            </tbody>
                        </table>
                        <table class="table" id="table_condiciones">
                            <tbody class="trbody_cond">
                                <!--<tr>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>CONDICIONES DE OPERACIÓN AL MOMENTO DE REALIZAR LA EVALUACIÓN</label>
                                            <textarea class="form-control condiciones" id="condiciones" name="condiciones"></textarea>      
                                        </div>
                                    </div>
                                </tr>-->
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12" style="text-align: left;">
                                <br>
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()"><i class="fa fa-plus" aria-hidden="true"></i> Agregar punto</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="save()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                                <button type="button" class="btn btn-success buttonsave" onclick="finalizarProceso()">Finalizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
