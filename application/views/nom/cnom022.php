<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .mg_cmp{
        margin-bottom: 30px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
        padding: 6px !important;
    }
    .tdinput input{
        padding: 6px !important;
    }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
        .mg_cmp{
            margin-bottom: 1px;
        }
    }

</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_nom">
                            <input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
                            <input type="hidden" name="id_orden" value="<?php echo $id_orden;?>">
                            <input type="hidden" name="id_chs" id="id_chs" value="<?php echo $id_chs;?>">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>No. de Informe</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" name="num_informe" id="num_informe" value="<?php echo $num_informe;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Razón Social</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" name="razon_social" id="razon_social" value="<?php echo $razon_social;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-4">
                                    <label>Fecha</label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-8 form-group">
                                    <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $fecha;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Valor de referencia</label>
                                    <input type="number" name="valor_ref" id="valor_ref" class="form-control" value="<?php echo $valor_ref;?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Verificación inicial</label>
                                    <input type="number" name="veri_ini" id="veri_ini" class="form-control" value="<?php echo $veri_ini;?>">
                                </div>
                                <div class="col-md-1">
                                    <label class="mg_cmp">Cumple</label>
                                    <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="<?php echo $cumple_criterio;?>">
                                </div>
                                <div class="col-md-2"> 
                                    <label>Verificación final</label>    
                                    <input type="number" name="veri_fin" id="veri_fin" class="form-control" value="<?php echo $veri_fin;?>">
                                </div>
                                <div class="col-md-1">
                                    <label class="mg_cmp">Cumple</label>
                                    <input type="text" name="cumple_criterio_fin" id="cumple_criterio_fin" class="form-control" value="<?php echo $cumple_criterio_fin;?>">
                                </div>
                                <div class="col-md-2">
                                    <label>Criterio</label>
                                    <input type="text" name="criterio" id="criterio" class="form-control" value="+- 5% respecto al valor de referencia">
                                </div>
                                <div class="col-md-2">
                                    <label>Valor RK</label>
                                    <input type="number" name="valor_rk" id="valor_rk" class="form-control" value="<?php echo $valor_rk;?>">
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Medidor de resistencia de tierras</label>
                                        <select name="id_medidor" id="id_medidor" class="form-control">
                                            <?php foreach($med->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_medidor){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Multímetro</label>
                                        <select name="id_multimetro" id="id_multimetro" class="form-control">
                                            <?php foreach($mult->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_multimetro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Set de resistencias</label>
                                        <select name="id_set_resis" id="id_set_resis" class="form-control">
                                            <?php foreach($set->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_set_resis){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Higrotermoanemometro</label>
                                        <select name="id_higro" id="id_higro" class="form-control">
                                            <option value="0">Elije una opción:</option>
                                            <?php foreach($higro->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_higro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">     
                                    <br>
                                </div>
                            </div>  
                        </form>

                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto(1)">Agregar punto</button>
                            </div>
                        </div>
                        <!--<table class="table table-bordered tablecaptura" id="table_puntos">
                            <tbody class="trbody">
                                
                            </tbody>
                        </table>-->
                        <div id="cont_mast">
                            
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto(1)">Agregar punto</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="save()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
