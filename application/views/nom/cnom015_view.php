<link href="<?php echo base_url();?>public/boostrap/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="<?php echo base_url();?>public/boostrap/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<script src="<?php echo base_url(); ?>plugins/Chart.js/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/Chart.js/chartjs-plugin-datalabels.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 

<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        font-size: 14px;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    .table td{
        vertical-align: middle;
    }
    @media (max-width:500px){
        .titulos{
            font-size: 11px;
        }
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }

    .tablestyle{
        margin: 0px;
    }
    .tablestyle th{
        background-color: #f4f4f4;
    }
    .table_width_auto{
        width: auto;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
    .border_66_29,.border_64_29,.border_56_28,.border_48_26,.border_40_23,.border_32_28,.border_24_31,.border_16_26,.border_8_29,.border_64_56,.border_56_55,.border_48_53,.border_40_55,.border_32_58,.border_66_56,.border_64_55,.border_48_50,.border_56_53{
        border-right: 3px solid #c4c5c7 !important;
    }
    .border_32_28,.border_24_31,.border_16_26,.border_8_29,.border_40_55,.border_32_58,.border_24_57{
        border-bottom: 3px solid #c4c5c7 !important;
    }
</style>
<?php $t_temp=$this->ModeloCatalogos->getselectwheren('tabla_temperaturas',array("activo"=>1)); ?>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                            <input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
                            <input type="hidden" name="idordenes" value="<?php echo $id_orden; ?>">
                            <input type="hidden" name="id_chs" id="id_chs" value="<?php echo $id_chs; ?>">
                           
                        
                             
                        <div id="cont_ptos_gral">
                            <div id="cont_ptos">
                                
                                <?php 
                                    $lvf_prom=array();
                                    foreach ($pto->result() as $p) { 
                                        $id_anemo=$p->id_anemo;
                                        $an_intercepcion=$this->ModeloCatalogos->interseccion_pendiente2($id_anemo,0,10);
                                        $an_pendiente=$this->ModeloCatalogos->interseccion_pendiente2($id_anemo,1,10);

                                        $id_termo=$p->id_termo;
                                        $ter_intercepcion=$this->ModeloCatalogos->interseccion_pendiente2($id_termo,0,8);
                                        $ter_pendiente=$this->ModeloCatalogos->interseccion_pendiente2($id_termo,1,8);

                                        ?>
                                        <div id="pto_princi" class="pto_princi pto_princifor_<?php echo $p->id; ?>">
                                        <input type="hidden" name="id_pto" id="id_pto" value="<?php echo $p->id; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th>No. de Informe</th>
                                                        <td><?php echo $num_informe; ?></td>
                                                        <th>Fecha</th>
                                                        <td><?php echo $p->fecha; ?></td>
                                                    </tr>
                                                </table>
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th>Razón Social</th>
                                                        <td><?php echo $razon_social; ?></td>
                                                    </tr>
                                                </table>
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th>Número de Punto</th>
                                                        <td><?php echo $p->num_punto; ?></td>
                                                        <th>Área</th>
                                                        <td><?php echo $p->area; ?></td>
                                                        <th>Puesto</th>
                                                        <td><?php echo $p->puesto; ?></td>
                                                    </tr>
                                                </table>
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th width="30%">Identificación del Punto</th>
                                                        <td width="70%"><?php echo $p->ubicacion; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fuentes Generadoras de la Condición Términca</th>
                                                        <td><?php echo $p->fte_generadora; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nombre del POE O GEH</th>
                                                        <td><?php echo $p->nom_trabaja; ?></td>
                                                    </tr>
                                                </table>
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th>Tipo de Evaluación</th>
                                                        <td><?php if($p->tipo_evalua==1){echo "POE";}
                                                                  if($p->tipo_evalua==2){echo "GEH";} ?> </td>
                                                        <th>Tiempo de exposición por ciclo (min)</th>
                                                        <td><?php echo $p->tiempo_expo; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Número de Ciclos Por Hora</th>
                                                        <td><?php echo $p->num_ciclos; ?></td>
                                                        <th>Ciclo Evaluado</th>
                                                        <td><?php echo $p->cliclo; ?></td>
                                                    </tr>
                                                    
                                                </table>
                                                <?php $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15',array('id'=>$p->id,"activo"=>1)); 
                                                    foreach($med->result() as $m) { ?>
                                                <table class="table table-bordered tablestyle">
                                                    <tr>
                                                        <th>Temperatura axilar</th>
                                                        <th>Inicial (ºC)</th>
                                                        <td><?php echo $m->temp_ini; ?></td>
                                                        <th>Final (ºC)</th>
                                                        <td><?php echo $m->temp_fin; ?></td>
                                                    </tr>
                                                    
                                                </table>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                            <div class="col-md-12">
                                                <?php $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15',array('id'=>$p->id,"activo"=>1)); 
                                                    $i_vt=0; $m_vt=0; $f_vt=0; $i_v=0; $i_t=0; $i_c_v=0; $i_c_t=0; $m_v=0; $m_t=0; $m_c_v=0; $m_c_t=0; $f_v=0; $f_t=0; $f_c_v=0; $f_c_t=0; $vt_promedio=0;
                                                    foreach($med->result() as $m) {
                                                        $id_temp=$m->id;
                                                        $i_v=$m->velocidad_ini;
                                                        $i_t=$m->tbs_ini;
                                                        $i_c_v=($an_pendiente*$i_v)+$an_intercepcion;
                                                        $i_c_t=($ter_pendiente*$i_t)+$ter_intercepcion;
                                                        $m_v=$m->velocidad_media;
                                                        $m_t=$m->tbs_media;
                                                        $m_c_v=($an_pendiente*$m_v)+$an_intercepcion;
                                                        $m_c_t=($ter_pendiente*$m_t)+$ter_intercepcion;
                                                        $f_v =$m->velocidad_fin; 
                                                        $f_t =$m->tbs_fin; 
                                                        $f_c_v=($an_pendiente*$f_v)+$an_intercepcion;
                                                        $f_c_t=($ter_pendiente*$f_t)+$ter_intercepcion;

                                                        foreach ($t_temp->result() as $item) {
                                                            $item->v_min;
                                                            $item->velocidad;
                                                            $item->v_max;
                                                            $item->t_10;
                                                            $t_10_max=($item->t_10+$item->t_4)/2;
                                                            $item->t_4;
                                                            $t_4_max=($item->t_4+$item->t_m1)/2;
                                                            $item->t_m1;
                                                            $t_m1_max=($item->t_m1+$item->t_m1)/2;
                                                            $item->t_m7 ;
                                                            $t_m7_max=($item->t_m7+$item->t_m12)/2;
                                                            $item->t_m12 ;
                                                            $t_m12_max=($item->t_m12+$item->t_m18)/2;
                                                            $item->t_m18 ;
                                                            $t_m18_max=($item->t_m18+$item->t_m23)/2;
                                                            $item->t_m23 ;
                                                            $t_m23_max=($item->t_m23+$item->t_m29)/2;
                                                            $item->t_m29 ;
                                                            $t_m29_max=($item->t_m29+$item->t_m34)/2;
                                                            $item->t_m34 ;
                                                            $t_m34_max=($item->t_m34+$item->t_m40)/2;
                                                            $item->t_m40 ;
                                                            if($i_v>=$item->v_min and $i_v<$item->v_max){
                                                                if($i_t>$t_10_max){
                                                                    $i_vt=$item->t_10;
                                                                }elseif($i_t<$t_10_max and $i_t>=$t_m1_max){
                                                                    $i_vt=$item->t_4;
                                                                }elseif($i_t<$t_4_max and $i_t>=$t_m1_max){
                                                                    $i_vt=$item->t_m1;
                                                                }elseif($i_t<$t_m1_max and $i_t>=$t_m7_max){
                                                                    $i_vt=$item->t_m7;
                                                                }elseif($i_t<$t_m7_max and $i_t>=$t_m12_max){
                                                                    $i_vt=$item->t_m12;
                                                                }elseif($i_t<$t_m12_max and $i_t>=$t_m18_max){
                                                                    $i_vt=$item->t_m18;
                                                                }elseif($i_t<$t_m18_max and $i_t>=$t_m23_max){
                                                                    $i_vt=$item->t_m23;
                                                                }elseif($i_t<$t_m23_max and $i_t>=$t_m29_max){
                                                                    $i_vt=$item->t_m29;
                                                                }elseif($i_t<$t_m29_max and $i_t>=$t_m34_max){
                                                                    $i_vt=$item->t_m34;
                                                                }elseif($i_t<$t_m40 ){
                                                                    $i_vt=$item->t_m40;
                                                                }
                                                            }
                                                            if($m_v>=$item->v_min and $m_v<$item->v_max){
                                                                if($m_t>$t_10_max){
                                                                    $m_vt=$item->t_10;
                                                                }elseif($m_t<$t_10_max and $m_t>=$t_m1_max){
                                                                    $m_vt=$item->t_4;
                                                                }elseif($m_t<$t_4_max and $m_t>=$t_m1_max){
                                                                    $m_vt=$item->t_m1;
                                                                }elseif($m_t<$t_m1_max and $m_t>=$t_m7_max){
                                                                    $m_vt=$item->t_m7;
                                                                }elseif($m_t<$t_m7_max and $m_t>=$t_m12_max){
                                                                    $m_vt=$item->t_m12;
                                                                }elseif($m_t<$t_m12_max and $m_t>=$t_m18_max){
                                                                    $m_vt=$item->t_m18;
                                                                }elseif($m_t<$t_m18_max and $m_t>=$t_m23_max){
                                                                    $m_vt=$item->t_m23;
                                                                }elseif($m_t<$t_m23_max and $m_t>=$t_m29_max){
                                                                    $m_vt=$item->t_m29;
                                                                }elseif($m_t<$t_m29_max and $m_t>=$t_m34_max){
                                                                    $m_vt=$item->t_m34;
                                                                }elseif($m_t<$t_m40 ){
                                                                    $m_vt=$item->t_m40;
                                                                }
                                                            }
                                                            if($f_v>=$item->v_min and $f_v<$item->v_max){
                                                                echo '<!--'.$f_v.'>='.$item->v_min.' and '.$f_v.'<'.$item->v_max.'   ='.$item->velocidad.'-->';
                                                                $f_vt='';
                                                                if($f_t>$t_10_max){
                                                                    $f_vt=$item->t_10;
                                                                }elseif($f_t<$t_10_max and $f_t>=$t_m1_max){
                                                                    $f_vt=$item->t_4;
                                                                }elseif($f_t<$t_4_max and $f_t>=$t_m1_max){
                                                                    echo '<!--$t_m1 '.$f_t.'<'.$t_4_max.' and '.$f_t.'>='.$t_m1_max.'-->
                                                                    ';
                                                                    $f_vt=$item->t_m1;
                                                                }elseif($f_t<$t_m1_max and $f_t>=$t_m7_max){
                                                                    echo '<!--$t_m7 ('.$f_t.'<'.$t_m1_max.' and '.$f_t.'>='.$t_m7_max.')='.$item->t_m7.'-->
                                                                    ';
                                                                    $f_vt=$item->t_m7;
                                                                }elseif($f_t<$t_m7_max and $f_t>=$t_m12_max){
                                                                    $f_vt=$item->t_m12;
                                                                }elseif($f_t<$t_m12_max and $f_t>=$t_m18_max){
                                                                    $f_vt=$item->t_m18;
                                                                }elseif($f_t<$t_m18_max and $f_t>=$t_m23_max){
                                                                    $f_vt=$item->t_m23;
                                                                }elseif($f_t<$t_m23_max and $f_t>=$t_m29_max){
                                                                    $f_vt=$item->t_m29;
                                                                }elseif($f_t<$t_m29_max and $f_t>=$t_m34_max){
                                                                    $f_vt=$item->t_m34;
                                                                }elseif($f_t<$t_m40 ){
                                                                    echo '<!--$t_m40 '.$f_t.'<'.$t_m40.'-->';
                                                                    $f_vt=$item->t_m40;
                                                                }
                                                            }
                                                            $vt_promedio=($i_vt+$m_vt+$f_vt)/3;

                                                            $lvf_prom[$p->id]=$vt_promedio;
                                                            $this->ModeloCatalogos->updateCatalogo('nom15_punto',array('lvf_prom'=>$vt_promedio),array('id'=>$p->id));
                                                            $this->ModeloCatalogos->updateCatalogo('medicion_temp_nom15',array('tbs_ini_c'=>$i_c_t,'velocidad_ini_c'=>$i_c_v,'lvf_ini_c'=>$i_vt,'tbs_media_c'=>$m_c_t,'velocidad_media_c'=>$m_c_v,'lvf_media_c'=>$m_vt,'tbs_fin_c'=>$f_c_t,'velocidad_fin_c'=>$f_c_v,'lvf_fin_c'=>$f_vt),array('id'=>$id_temp));
                                                        } 
                                                ?>
                                                <table class="table table-bordered tablestyle table_width_auto">
                                                    <tr>
                                                        <th rowspan="4">I<br>n<br>i<br>c<br>i<br>a<br>l</th>
                                                        <th>Velocidad del viento (km/s)</th><th>Temperatura bs (°C)</th>
                                                        <th>𝐼<sub>𝑣𝑓</sub></th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $i_v; ?></td><td><?php echo $i_t; ?></td>
                                                        <td rowspan="3"><?php echo $i_vt;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Corregida</th><th>Corregida</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo round($i_c_v,1);?></td><td><?php echo round($i_c_t,1);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="4">M<br>i<br>t<br>a<br>d</th>
                                                        <th>Velocidad del viento (km/s)</th><th>Temperatura bs (°C)</th>
                                                        <th>𝐼<sub>𝑣𝑓</sub></th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $m_v; ?></td><td><?php echo $m_t; ?></td>
                                                        <td rowspan="3"><?php echo $m_vt;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Corregida</th><th>Corregida</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo round($m_c_v,1);?></td><td><?php echo round($m_c_t,1);?></td>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="4">F<br>i<br>n<br>a<br>l</th>
                                                        <th>Velocidad del viento (km/s)</th><th>Temperatura bs (°C)</th>
                                                        <th>𝐼<sub>𝑣𝑓</sub></th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $f_v;?></td><td><?php echo $f_t;?></td>
                                                        <td rowspan="3"><?php echo $f_vt;?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Corregida</th><th>Corregida</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo round($f_c_v,1); ?></td><td><?php echo round($f_c_t,1); ?></td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <table class="table table-bordered tablestyle table_width_auto">
                                                    <tr>
                                                        <th>𝐼<sub>𝑣𝑓</sub> Promedio</th>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $vt_promedio;?></td>
                                                    </tr>
                                                </table>
                                                <p style="text-align: center;"><img src="<?php echo base_url()?>public/img/vf_promedio.png" style="width: 400px;"></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br>
                                                <table class="table table-bordered tablestyle table_width_auto">
                                                    <tr>
                                                        <td rowspan="2">Velocidad<br>del viento en<br>km/h</td>
                                                        <td colspan="10">Temperatura leida en el termometro en °C</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 90px;">10</td>
                                                        <td style="width: 90px;">4</td>
                                                        <td style="width: 90px;">-1</td>
                                                        <td style="width: 90px;">-7</td>
                                                        <td style="width: 90px;">-12</td>
                                                        <td style="width: 90px;">-18</td>
                                                        <td style="width: 90px;">-23</td>
                                                        <td style="width: 90px;">-29</td>
                                                        <td style="width: 90px;">-34</td>
                                                        <td style="width: 90px;">-40</td>
                                                    </tr>
                                                    <?php foreach ($t_temp->result() as $item) { ?>
                                                        <tr>
                                                            <td><?php echo $item->velocidad;?></td>
                                                            <td><?php echo $item->t_10;?></td>
                                                            <td><?php echo $item->t_4;?></td>
                                                            <td><?php echo $item->t_m1;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m7) ;?>"><?php echo $item->t_m7 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m12) ;?>"><?php echo $item->t_m12 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m18) ;?>"><?php echo $item->t_m18 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m23) ;?>"><?php echo $item->t_m23 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m29) ;?>"><?php echo $item->t_m29 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m34) ;?>"><?php echo $item->t_m34 ;?></td>
                                                            <td class="border_<?php echo $item->velocidad.'_'.abs($item->t_m40) ;?>"><?php echo $item->t_m40 ;?></td>
                                                        </tr>
                                                    <?php } ?>
                                                        <tr>
                                                            <td rowspan="2"></td>
                                                            <td colspan="4" rowspan="2">PELIGRO ESCASO EN UNA HORA DE EXPOSICION (PARA UNA PERSONA ADECUADAMENTE VESTIDA)</td>
                                                            <td colspan="3">AUMENTO DE PELIGRO EN UN MONUTO DE EXPOSICION</td>
                                                            <td colspan="3">GRAN PELIGRO EN 30 SEGUNDOS DE EXPOSICION</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">PELIGRO DE CONGELACION DE LAS ZONAS EXPUESTAS</td>
                                                        </tr>
                                                </table>
                                                <br>
                                            </div>
                                        </div>



                                        


                                        
                                    <?php } ?>
                                
                            </div>
                        </div>                 
                    
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered tablestyle table_width_auto">
                                    <tr>
                                        <th>No. de punto</th>
                                        <th>Identificación del punto de medición (área/identificación)</th>
                                        <th>Ciclo</th>
                                        <th>IVF<sub>Prom</sub> °C</th>
                                        <th>T.E (h)</th>
                                        <th>LMPE</th>
                                        <th>T.A Inicial (°C)</th>
                                        <th>T.A Final (°C)</th>
                                        <th>Conclusión</th>
                                    </tr>
                                    <?php
                                    $punto=1; 
                                    $pton=$this->ModeloCatalogos->infonomelevadas($idnom);
                                    $te_supera_lmp=0;
                                    $te_nosupera_lmp=0;
                                    foreach ($pton->result() as $p) { 
                                        $tiempo_expo=round($p->tiempo_expo/60,2);
                                        $lmpe=8.0;
                                        ?>
                                    <tr>
                                        <td><?php echo $punto;?></td>
                                        <td><?php echo $p->area;?></td>
                                        <td><?php echo $p->num_ciclos;?></td>
                                        <td><?php if(isset($lvf_prom[$p->id])) echo $lvf_prom[$p->id];?></td>
                                        <td><?php echo $tiempo_expo;?></td>
                                        <td><?php echo $lmpe;?></td>
                                        <td><?php echo $p->temp_fin;?></td>
                                        <td><?php echo $p->temp_fin;?></td>
                                        <td><?php
                                            if($tiempo_expo>$lmpe){
                                                echo "El TE supera el LMP";
                                                $te_supera_lmp++;
                                            }else{
                                                echo "El TE no supera el LMP";
                                                $te_nosupera_lmp++;
                                            }
                                        ?></td>
                                    </tr>
                                    <?php $punto++;
                                    } 

                                    ?>
                                </table>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function($) {
                                    new Chart(document.getElementById("pie-chart"), {
                                        type: 'pie',
                                        data: {
                                          labels: ["Supera el LMPE", "No Supera el LMPE"],
                                          datasets: [{
                                            label: "Population (millions)",
                                            backgroundColor: ["#003958", "#e76300","#3cba9f","#e8c3b9","#c45850"],
                                            data: [<?php echo ($te_nosupera_lmp/2)*100;?>,<?php echo ($te_supera_lmp/2)*100;?>]
                                          }]
                                        },
                                        options: {
                                          title: {
                                            display: true,
                                            text: 'Evaluación de temperaturas abatidas'
                                          },
                                          plugins: {
                                              datalabels: {
                                                formatter: (value) => {
                                                  return value + '%';
                                                },
                                                color: '#fff',
                                              },
                                            },
                                        }
                                    });
                                    setTimeout(function(){ exportarchart(); }, 2000);
                                });
                                function exportarchart(){
                                    var base_url = $('#base_url').val();
                                    var canvasx= document.getElementById("pie-chart");
                                    var dataURL = canvasx.toDataURL('image/png', 1);
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+'Nom/nom15viewinserupdatechart',
                                        data: {
                                              id:<?php echo $idnom;?>,
                                              grafica:dataURL
                                        },
                                        statusCode:{
                                          404: function(data){
                                              
                                          },
                                          500: function(){
                                              
                                          }
                                      },
                                      success:function(data){
                                          console.log("hecho");
                                      }
                                  });
                                }
                            </script>
                            <div class="col-md-6">
                                <canvas id="pie-chart" width="800" height="450"></canvas>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
