<link href="<?php echo base_url();?>public/boostrap/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="<?php echo base_url();?>public/boostrap/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<script src="<?php echo base_url(); ?>plugins/Chart.js/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/Chart.js/chartjs-plugin-datalabels.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 
<script type="text/javascript">
    var valorset=1000;
</script>
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        font-size: 14px;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    .table td{
        vertical-align: middle;
    }
    @media (max-width:500px){
        .titulos{
            font-size: 11px;
        }
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 10px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
        label{
          font-size: 11px;  
        }
    }
    .tablestyle{
        margin: 0px;
    }
    .tablestyle th{
        background-color: #f4f4f4;
    }
    .table_width_auto{
        width: auto;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
</style>
<?php 
        $interseccion_16_2 = $this->ModeloCatalogos->interseccion_pendiente3(16,0,2);//bulbo humedo(G11)
        $pendiente_16_2 = $this->ModeloCatalogos->interseccion_pendiente3(16,1,2);//bulbo humedo(G12)
        //echo $interseccion_16_2.'/'.$pendiente_16_2.'<br>';
        $interseccion_17_2 = $this->ModeloCatalogos->interseccion_pendiente3(17,0,2);//bulbo humedo(O11)
        $pendiente_17_2 = $this->ModeloCatalogos->interseccion_pendiente3(17,1,2);//bulbo humedo(O12)
        //echo $interseccion_17_2.'/'.$pendiente_17_2.'<br>';
        $interseccion_18_2 = $this->ModeloCatalogos->interseccion_pendiente3(18,0,2);//bulbo humedo(W11)
        $pendiente_18_2 = $this->ModeloCatalogos->interseccion_pendiente3(18,1,2);//bulbo humedo(W12)
        //echo $interseccion_18_2.'/'.$pendiente_18_2.'<br>';
        //echo '---<br>';
        $interseccion_16_3 = $this->ModeloCatalogos->interseccion_pendiente3(16,0,3);//globo(G19)
        $pendiente_16_3 = $this->ModeloCatalogos->interseccion_pendiente3(16,1,3);//globo(G20)
        //echo $interseccion_16_3.'/'.$pendiente_16_3.'<br>';
        $interseccion_17_3 = $this->ModeloCatalogos->interseccion_pendiente3(17,0,3);//globo(O11)
        $pendiente_17_3 = $this->ModeloCatalogos->interseccion_pendiente3(17,1,3);//globo(O12)
        //echo $interseccion_17_3.'/'.$pendiente_17_3.'<br>';
        $interseccion_18_3 = $this->ModeloCatalogos->interseccion_pendiente3(18,0,3);//globo(W11)
        $pendiente_18_3 = $this->ModeloCatalogos->interseccion_pendiente3(18,1,3);//globo(W12)
        //echo $interseccion_18_3.'/'.$pendiente_18_3.'<br>';
        //echo '---<br>';
        $interseccion_16_1 = $this->ModeloCatalogos->interseccion_pendiente3(16,0,1);//Bulbo seco(G3)
        $pendiente_16_1 = $this->ModeloCatalogos->interseccion_pendiente3(16,1,1);//Bulbo seco(G4)
        //echo $interseccion_16_1.'/'.$pendiente_16_1.'<br>';
        $interseccion_17_1 = $this->ModeloCatalogos->interseccion_pendiente3(17,0,1);//Bulbo seco(O3)
        $pendiente_17_1 = $this->ModeloCatalogos->interseccion_pendiente3(17,1,1);//Bulbo seco(O4)
        //echo $interseccion_17_1.'/'.$pendiente_17_1.'<br>';
        $interseccion_18_1 = $this->ModeloCatalogos->interseccion_pendiente3(18,0,1);//Bulbo seco(W3)
        $pendiente_18_1 = $this->ModeloCatalogos->interseccion_pendiente3(18,1,1);//Bulbo seco(W4)
        //echo $interseccion_18_1.'/'.$pendiente_18_1.'<br>';
?>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        
                             
                        <div id="cont_ptos_gral">
                            <div id="cont_ptos">
                                <?php if(isset($pto) && $pto->num_rows()>0) {
                                    foreach ($pto->result() as $p) { ?>
                                        <table class="table table-bordered tablestyle" style="width:auto; margin-left: auto;">
                                            <tr>
                                                <th>No. de Informe</th>
                                                <td><?php echo $num_informe; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Razón Social</th>
                                                <td><?php echo $razon_social; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Fecha de Medición</th>
                                                <td><?php echo $p->fecha; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Número de Punto </th>
                                                <td><?php echo $p->num_punto; ?></td>
                                                <th>Área</th>
                                                <td><?php echo $p->area; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th width="20%">Identificación del Punto</th>
                                                <td width="80%"><?php echo $p->area; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Fuentes Generadoras de la Condición Términca</th>
                                                <td><?php echo $p->fte_generadora; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Tipo de Evaluación</th>
                                                <td><?php 
                                                    if($p->tipo_evalua==1){ echo "POE";}
                                                    if($p->tipo_evalua==2){ echo "GEH";}
                                                ?></td>
                                                <th>Tipo de puesto de Trabajo</th>
                                                <td><?php if($p->tipo_mov==1){ echo "Movimiento";}
                                                          if($p->tipo_mov==2){ echo "Fijo";} ?></td>
                                            </tr>
                                        </table>
                                        <br>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Nombre del POE</th>
                                                <td><?php echo $p->nom_trabaja; ?></td>
                                            </tr>
                                            
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Puesto  </th>
                                                <td><?php echo $p->puesto; ?></td>
                                                <th>Tiempo de exposición por ciclo (min)</th>
                                                <td><?php echo $p->tiempo_expo; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Número de Ciclos Por Hora</th>
                                                <td><?php echo $p->num_ciclos; ?></td>
                                                <th>Ciclo Evaluado</th>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Régimen de trabajo</th>
                                                <td><?php echo $p->regimen; ?></td>
                                                <th>% de Exposición</th>
                                                <td><?php echo $p->porc_expo; ?></td>
                                                <th>% de Recuperación</th>
                                                <td><?php echo $p->porc_no_expo; ?></td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered tablestyle">
                                            <tr>
                                                <th>Sin carga solar</th>
                                                <td><?php if($p->carga_solar==1){ echo "X"; }?></td>
                                                <th>Con carga solar</th>
                                                <td><?php if($p->carga_solar==2){ echo "X"; }?></td>
                                            </tr>
                                            
                                        </table>
                                        <?php $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15_elevadas2',array('id_nom_punto'=>$p->id,"activo"=>1)); { 
                                           
                                            $html=""; 
                                            $inical_promedio_sin=array();
                                            $inical_promedio=0;
                                            $mitad_promedio_sin=array();
                                            $mitad_promedio=0;
                                            $final_promedio_sin=array();
                                            $final_promedio=0;

                                            $inical_promedio_con=array();                                        
                                            $mitad_promedio_con=array();                                        
                                            $final_promedio_con=array();
                                        
                                            foreach($med->result() as $m) { 
                                                
                                                
                                                if($m->tipo==1 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $inical_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $inical_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                        
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 
                                                    } 
                                                    
                                                }
                                                if($m->tipo==2 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $inical_promedio_sin[]=$ltgbh_sin*2;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $inical_promedio_con[]=$ltgbh_con*2;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==3 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $inical_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $inical_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==1 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $mitad_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $mitad_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==2 && $m->tipo_med==2) {
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $mitad_promedio_sin[]=$ltgbh_sin*2;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $mitad_promedio_con[]=$ltgbh_con*2;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==3 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $mitad_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $mitad_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==1 && $m->tipo_med==3) { //ultimos registros
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $final_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $final_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                   
                                                }
                                                if($m->tipo==2 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $final_promedio_sin[]=$ltgbh_sin*2;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $final_promedio_con[]=$ltgbh_con*2;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                if($m->tipo==3 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            $final_promedio_sin[]=$ltgbh_sin;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            $final_promedio_con[]=$ltgbh_con;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    
                                                }
                                                
                                            }
                                            //las formulas del foreach de arriba y la de abajo tienen que ser iguales, se hizo de esa forma para sacar el promedio
                                            $cont_temps=0;
                                            foreach($med->result() as $m) { 
                                                $cont_temps++;
                                                if($cont_temps==1){
                                                    ?>
                                                        <table class="table table-bordered tablestyle">
                                                            <tr>
                                                                <th>Temperatura auxilar (jornada laboral)</th>
                                                                <th>inicial (°C)</th>
                                                                <td><?php echo $m->temp_ini;?></td>
                                                                <th>final (°C)</th>
                                                                <td><?php echo $m->temp_fin;?></td>
                                                            </tr>
                                                            
                                                        </table>
                                                        <table class="table table-bordered tablestyle">
                                                            <tr>
                                                                <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>Regíón</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                                <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                                                <td colspan="3" class="tdinput titulos"><label>Mediciones Corregidas</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>L <sub>tgbh</sub> Sin carga solar °C</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>L <sub>tgbh</sub> con carga solar °C</label></td>
                                                                <td rowspan="2" class="tdinput titulos"><label>L <sub>tgbh</sub> Promedio °C</label></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdinput titulos"><label>Tbh °C</label></td>
                                                                <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                                <td class="tdinput titulos"><label>Tg °C</label></td>
                                                                <td class="tdinput titulos"><label>Tbh °C</label></td>
                                                                <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                                <td class="tdinput titulos"><label>Tg °C</label></td>
                                                            </tr>
                                                    <?php 
                                                }
                                                
                                                if($m->tipo==1 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            if(array_sum($inical_promedio_sin)>0){
                                                                $inical_promedio=array_sum($inical_promedio_sin)/4;
                                                            }
                                                            
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            if(array_sum($inical_promedio_con)>0){
                                                                $inical_promedio=array_sum($inical_promedio_con)/4;
                                                            }
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                        
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 
                                                    } 
                                                    $html.='<tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Inicial</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                        <td rowspan="3">'.round($inical_promedio,1).'</td>

                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='<tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==1 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            if(array_sum($mitad_promedio_sin)>0){
                                                                $mitad_promedio=array_sum($mitad_promedio_sin)/4;
                                                            }
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            if(array_sum($mitad_promedio_con)>0){
                                                                $mitad_promedio=array_sum($mitad_promedio_con)/4;
                                                            }
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Mitad</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                        <td rowspan="3">'.round($mitad_promedio,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==2) {
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==1 && $m->tipo_med==3) { //ultimos registros
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_16_2*$m->tbh)+$interseccion_16_2;
                                                        $tbs_c=($pendiente_16_3*$m->tbs)+$interseccion_16_3;
                                                        $tg_c=($pendiente_16_1*$m->tg)+$interseccion_16_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                            if(array_sum($final_promedio_sin)>0){
                                                                $final_promedio=array_sum($final_promedio_sin)/4;
                                                            }
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                            if(array_sum($final_promedio_con)>0){
                                                                $final_promedio=array_sum($final_promedio_con)/4;
                                                            }
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Final</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td>'.$hora.'</td>
                                                        <td>'.$m->tbh.'</td>
                                                        <td>'.$m->tbs.'</td>
                                                        <td>'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                        <td rowspan="3">'.round($final_promedio,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_17_2*$m->tbh)+$interseccion_17_2;
                                                        $tbs_c=($pendiente_17_3*$m->tbs)+$interseccion_17_3;
                                                        $tg_c=($pendiente_17_1*$m->tg)+$interseccion_17_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00"){
                                                        $hora=$m->hora;
                                                        $tbh_c=($pendiente_18_2*$m->tbh)+$interseccion_18_2;
                                                        $tbs_c=($pendiente_18_3*$m->tbs)+$interseccion_18_3;
                                                        $tg_c=($pendiente_18_1*$m->tg)+$interseccion_18_1;
                                                        if($p->carga_solar==1){
                                                            $ltgbh_sin=0.7*$tbh_c+0.3*$tbs_c;
                                                        }else{
                                                            $ltgbh_sin=0;    
                                                        }
                                                        if($p->carga_solar==2){
                                                            $ltgbh_con=0.7*$tbh_c+0.2*$tbs_c+0.1*$tg_c;
                                                        }else{
                                                            $ltgbh_con=0;    
                                                        }
                                                    }else{
                                                        $hora="";
                                                        $tbh_c=0;
                                                        $tbs_c=0;
                                                        $tg_c=0;
                                                        $ltgbh_sin=0;
                                                        $ltgbh_con=0; 

                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">'.$m->altura.'</td>
                                                        <td class="tdinput">'.$hora.'</td>
                                                        <td class="tdinput">'.$m->tbh.'</td>
                                                        <td class="tdinput">'.$m->tbs.'</td>
                                                        <td class="tdinput">'.$m->tg.'</td>
                                                        <td>'.round($tbh_c,1).'</td>
                                                        <td>'.round($tbs_c,1).'</td>
                                                        <td>'.round($tg_c,1).'</td>
                                                        <td>'.round($ltgbh_sin,1).'</td>
                                                        <td>'.round($ltgbh_con,1).'</td>
                                                    </tr>';
                                                }
                                                
                                            } //foreach
                                            $html.='</table> 
                                               <center><span style="font-size: 9px;">Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</span></center>';
                                            echo $html;
                                        }
                                        echo "</div>";
                                        ?>
                                            <div class="row">
                                                <div class="col-md-12" style="text-align:center;">
                                                    I<sub>tgbh</sub> Índice de la temperatura de globo bulbo húmedo promedio                                                    

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="text-align:center;">
                                                    INDÍCE DE TEMPERATURA DE GLOBO BÚLBO HÚMEDO (sin carga solar)<br><img src="<?php echo base_url().'public/img/form_elevada1.png';?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="text-align:center;">
                                                    INDÍCE DE TEMPERATURA DE GLOBO BÚLBO HÚMEDO (con carga solar)<br><img src="<?php echo base_url().'public/img/form_elevada2.png';?>">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="text-align:center;">
                                                    TEMPERATURA DE GLOBO BÚLBO HÚMEDO PROMEDIO<br><img src="<?php echo base_url().'public/img/form_elevada3.png';?>">
                                                </div>
                                            </div>
                                        <?php
                                            if($inical_promedio>0){
                                                $this->ModeloCatalogos->updateCatalogo('nom15_punto_elevada',array('calc_prom_ini'=>round($inical_promedio,1)),array('id'=>$p->id));
                                                ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function($) {
                                                            new Chart(document.getElementById("bar-chart_i_<?php echo $p->id; ?>"), {
                                                                type: 'bar',
                                                                data: {
                                                                  labels: ["ITGBH"],
                                                                  datasets: [{
                                                                    label: "ITGBH",
                                                                    backgroundColor: "#003958",
                                                                    data: [<?php echo round($inical_promedio,1);?>]
                                                                  },
                                                                  {
                                                                    label: "TM ITGBH",
                                                                    backgroundColor: "#e76300",
                                                                    data: [31.1]
                                                                  }]
                                                                },
                                                                options: {
                                                                  title: {
                                                                    display: true,
                                                                    text: 'ITGBH INICIAL'
                                                                  },
                                                                  plugins: {
                                                                      datalabels: {
                                                                        formatter: (value) => {
                                                                          return value + '%';
                                                                        },
                                                                        color: '#fff',
                                                                      },
                                                                    },
                                                                }
                                                            });
                                                            setTimeout(function(){ exportarchart('i',<?php echo $p->id; ?>); }, valorset+500);
                                                        });
                                                    </script>
                                                    <div class="col-md-6">
                                                        <canvas id="bar-chart_i_<?php echo $p->id; ?>" width="800" height="450"></canvas>
                                                    </div>
                                                <?php
                                            }
                                            if($mitad_promedio>0){
                                                $this->ModeloCatalogos->updateCatalogo('nom15_punto_elevada',array('calc_prom_med'=>round($mitad_promedio,1)),array('id'=>$p->id));
                                                ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function($) {
                                                            new Chart(document.getElementById("bar-chart_m_<?php echo $p->id; ?>"), {
                                                                type: 'bar',
                                                                data: {
                                                                  labels: ["ITGBH"],
                                                                  datasets: [{
                                                                    label: "ITGBH",
                                                                    backgroundColor: "#003958",
                                                                    data: [<?php echo round($mitad_promedio,1);?>]
                                                                  },
                                                                  {
                                                                    label: "TM ITGBH",
                                                                    backgroundColor: "#e76300",
                                                                    data: [31.1]
                                                                  }]
                                                                },
                                                                options: {
                                                                  title: {
                                                                    display: true,
                                                                    text: 'ITGBH MITAD'
                                                                  },
                                                                  plugins: {
                                                                      datalabels: {
                                                                        formatter: (value) => {
                                                                          return value;
                                                                        },
                                                                        color: '#fff',
                                                                      },
                                                                    },
                                                                }
                                                            });
                                                            setTimeout(function(){ exportarchart('m',<?php echo $p->id; ?>); }, valorset+500);
                                                        });
                                                    </script>
                                                    <div class="col-md-6">
                                                        <canvas id="bar-chart_m_<?php echo $p->id; ?>" width="800" height="450"></canvas>
                                                    </div>
                                                <?php
                                            }
                                            if($final_promedio>0){
                                                $this->ModeloCatalogos->updateCatalogo('nom15_punto_elevada',array('calc_prom_fin'=>round($final_promedio,1)),array('id'=>$p->id));
                                                ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function($) {
                                                            new Chart(document.getElementById("bar-chart_f_<?php echo $p->id; ?>"), {
                                                                type: 'bar',
                                                                data: {
                                                                  labels: ["ITGBH"],
                                                                  datasets: [{
                                                                    label: "ITGBH",
                                                                    backgroundColor: "#003958",
                                                                    data: [<?php echo round($final_promedio,1);?>]
                                                                  },
                                                                  {
                                                                    label: "TM ITGBH",
                                                                    backgroundColor: "#e76300",
                                                                    data: [31.1]
                                                                  }]
                                                                },
                                                                options: {
                                                                  title: {
                                                                    display: true,
                                                                    text: 'ITGBH FINAL'
                                                                  },
                                                                  plugins: {
                                                                      datalabels: {
                                                                        formatter: (value) => {
                                                                          return value;
                                                                        },
                                                                        color: '#fff',
                                                                      },
                                                                    },
                                                                }
                                                            });
                                                            setTimeout(function(){ exportarchart('f',<?php echo $p->id; ?>); }, valorset+500);
                                                        });
                                                    </script>
                                                    <div class="col-md-6">
                                                        <canvas id="bar-chart_f_<?php echo $p->id; ?>" width="800" height="450"></canvas>
                                                    </div>
                                                <?php
                                            }
                                    } 
                                    }?>
                                    
                            </div>
                        </div>                 
                    

                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    function exportarchart(tipo,id){
        var base_url = $('#base_url').val();
        var canvasx= document.getElementById("bar-chart_"+tipo+"_"+id);
        var dataURL = canvasx.toDataURL('image/png', 1);
        $.ajax({
            type:'POST',
            url: base_url+'Nom/nom15viewinserupdatechart_2',
            data: {
                  id:id,
                  tipo:tipo,
                  grafica:dataURL
            },
            statusCode:{
              404: function(data){
                  
              },
              500: function(){
                  
              }
          },
          success:function(data){
              console.log("hecho");
          }
      });
    }
</script>