<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        font-size: 14px;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    .table td{
        vertical-align: middle;
    }
    @media (max-width:500px){
        .titulos{
            font-size: 11px;
        }
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 10px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
        label{
          font-size: 11px;  
        }
    }

</style>

<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_nom">
                            <input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
                            <input type="hidden" name="idordenes" value="<?php echo $id_orden; ?>">
                            <input type="hidden" name="id_chs" id="id_chs" value="<?php echo $id_chs; ?>">
                            <div class="row" id="cont_ocu1">
                                <div class="col-md-4">
                                    <label>No. de Informe</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control num_informe_princi" name="num_informe" id="num_informe" value="<?php echo $num_informe; ?>">
                                </div>
                            </div>
                            <div class="row" id="cont_ocu1">
                                <div class="col-md-4">
                                    <label>Razón Social</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control razon_social_princi" name="razon_social" id="razon_social" value="<?php echo $razon_social; ?>">
                                </div>
                            </div>
                        </form>
                             
                        <div id="cont_ptos_gral">
                            <div id="cont_ptos">
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()" ><i class="fa fa-plus"></i> Agregar punto</button>
                                <?php if(isset($pto) && $pto->num_rows()>0) {
                                    foreach ($pto->result() as $p) { ?>
                                        <div id="pto_princi" class="pto_princi pto_princifor_<?php echo $p->id; ?>">
                                        <hr>
                                        <input type="hidden" name="id_pto" id="id_pto" value="<?php echo $p->id; ?>">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                                <button type="button" class="btn btn-danger mr-1 mb-1" onclick="deletepunto(<?php echo $p->id; ?>)" ><i class="fa fa-trash-o"></i> Eliminar punto</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $p->fecha; ?>">
                                            </div>
                                        </div>    

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Bulbo Seco</label>
                                                    <select name="id_bulbo" id="id_bulbo" class="form-control">
                                                        <?php foreach($seco->result() as $eq){ ?>
                                                            <option <?php if($p->id_bulbo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo" id="th_bulbo" class="form-control" value="<?php echo $p->th_bulbo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo" id="ts_bulbo" class="form-control" value="<?php echo $p->ts_bulbo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo" id="tg_bulbo" class="form-control" value="<?php echo $p->tg_bulbo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo" id="acepta_bulbo" class="form-control" value="<?php echo $p->acepta_bulbo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo2" id="th_bulbo2" class="form-control" value="<?php echo $p->th_bulbo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo2" id="ts_bulbo2" class="form-control" value="<?php echo $p->ts_bulbo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo2" id="tg_bulbo2" class="form-control" value="<?php echo $p->tg_bulbo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo2" id="acepta_bulbo2" class="form-control" value="<?php echo $p->acepta_bulbo2; ?>">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Bulbo Húmedo</label>
                                                    <select name="id_bulbo_humedo" id="id_bulbo_humedo" class="form-control">
                                                        <?php foreach($humedo->result() as $eq){ ?>
                                                            <option <?php if($p->id_bulbo_humedo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo_hum" id="th_bulbo_hum" class="form-control" value="<?php echo $p->th_bulbo_hum; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo_hum" id="ts_bulbo_hum" class="form-control" value="<?php echo $p->ts_bulbo_hum; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo_hum" id="tg_bulbo_hum" class="form-control" value="<?php echo $p->tg_bulbo_hum; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo_hum" id="acepta_bulbo_hum" class="form-control" value="<?php echo $p->acepta_bulbo_hum; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo_hum2" id="th_bulbo_hum2" class="form-control" value="<?php echo $p->th_bulbo_hum2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo_hum2" id="ts_bulbo_hum2" class="form-control" value="<?php echo $p->ts_bulbo_hum2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo_hum2" id="tg_bulbo_hum2" class="form-control" value="<?php echo $p->tg_bulbo_hum2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo_hum2" id="acepta_bulbo_hum2" class="form-control" value="<?php echo $p->acepta_bulbo_hum2; ?>">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Globo</label>
                                                    <select name="id_globo" id="id_globo" class="form-control">
                                                        <?php foreach($globo->result() as $eq){ ?>
                                                            <option <?php if($p->id_globo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_globo" id="th_globo" class="form-control" value="<?php echo $p->th_globo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_globo" id="ts_globo" class="form-control" value="<?php echo $p->ts_globo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_globo" id="tg_globo" class="form-control" value="<?php echo $p->tg_globo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_globo" id="acepta_globo" class="form-control" value="<?php echo $p->acepta_globo; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_globo2" id="th_globo2" class="form-control" value="<?php echo $p->th_globo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_globo2" id="ts_globo2" class="form-control" value="<?php echo $p->ts_globo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_globo2" id="tg_globo2" class="form-control" value="<?php echo $p->tg_globo2; ?>">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_globo2" id="acepta_globo2" class="form-control" value="<?php echo $p->acepta_globo2; ?>">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro digital</label>
                                                    <select name="id_termo" id="id_termo" class="form-control">
                                                        <?php foreach($termo->result() as $eq){ ?>
                                                            <option <?php if($p->id_termo==$eq->id) echo "selected"; ?> value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="table_det" width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td class="tdinput titulos" width="10%">
                                                    <label>ÁREA</label>
                                                </td>   
                                                <td class="tdinput" width="40%">
                                                    <input type="text" name="area" id="area" class="form-control" value="<?php echo $p->area; ?>">
                                                </td>   
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>No. PUNTO</label>  
                                                </td>  
                                                <td class="tdinput" width="15%">
                                                    <input type="text" name="num_punto" id="num_punto" class="form-control" value="<?php echo $p->num_punto; ?>">
                                                </td>
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>¿EXISTE CARGAR SOLAR?</label>  
                                                </td>  
                                                <td class="tdinput" width="15%">
                                                    <label>SI
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,1,1)" <?php if($p->carga_solar==1) echo "checked"; ?> type="radio" id="carga_solar" name="carga_solar_<?php echo $p->id;?>" class="carga_solar_<?php echo $p->id;?> form-control form-control-sm chk_form">
                                                    </label>
                                                    <label>NO
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,1,2)" <?php if($p->carga_solar==2) echo "checked"; ?> type="radio" id="carga_solar2" name="carga_solar_<?php echo $p->id;?>" class="carga_solar2_<?php echo $p->id;?> form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</label>
                                                </td>  
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="fte_generadora" id="fte_generadora" class="form-control" value="<?php echo $p->fte_generadora; ?>">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES TÉCNICOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="controles_tec" id="controles_tec" class="form-control" value="<?php echo $p->controles_tec; ?>">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES ADMINISTRATIVOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="controles_adm" id="controles_adm" class="form-control" value="<?php echo $p->controles_adm; ?>">
                                                </td> 
                                            </tr>
                                        </table>
                                        <table id="table_det" width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td width="20%" colspan="2" class="tdinput titulos">TIPO DE EVALUACIÓN</td>
                                                <td width="50%" class="tdinput titulos">NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</td>
                                                <td width="15%" class="tdinput titulos">Temperatura axiliar inicial de la jornada (°C)</td>
                                                <td width="15%" class="tdinput titulos">Temperatura axiliar final de la jornada (°C)</td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput">
                                                    <label>POE
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,2,1)" <?php if($p->tipo_evalua==1) echo "checked"; ?> type="radio" id="tipo_evalua" name="tipo_evalua_<?php echo $p->id; ?>" class="tipo_evalua_<?php echo $p->id; ?> form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <label>GEH
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,2,2)" <?php if($p->tipo_evalua==2) echo "checked"; ?> type="radio" id="tipo_evalua2" name="tipo_evalua_<?php echo $p->id; ?>" class="tipo_evalua2_<?php echo $p->id; ?>form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="nom_trabaja" id="nom_trabaja" class="form-control" value="<?php echo $p->nom_trabaja; ?>">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="temp_aux_ini" id="temp_aux_ini" class="form-control" value="<?php echo $p->temp_aux_ini; ?>">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="temp_aux_fin" id="temp_aux_fin" class="form-control" value="<?php echo $p->temp_aux_fin; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput">
                                                    <label>Movimiento
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,3,1)" <?php if($p->tipo_mov==1) echo "checked"; ?> type="radio" id="tipo_mov" name="tipo_mov_<?php echo $p->id; ?>" class="tipo_mov_<?php echo $p->id; ?> form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <label>Fijo
                                                        <input onchange="cambiaRadio(<?php echo $p->id;?>,3,2)" <?php if($p->tipo_mov==2) echo "checked"; ?> type="radio" id="tipo_mov2" name="tipo_mov_<?php echo $p->id; ?>" class="tipo_mov2_<?php echo $p->id; ?> form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Casco:
                                                    <input <?php if($p->casco==1) echo "checked"; ?> type="checkbox" name="casco" id="casco" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Tapones acústicos:
                                                    <input <?php if($p->tapones==1) echo "checked"; ?> type="checkbox" name="tapones" id="tapones" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Botas de seguridad:
                                                    <input <?php if($p->botas==1) echo "checked"; ?> type="checkbox" name="botas" id="botas" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Lentes:
                                                    <input <?php if($p->lentes==1) echo "checked"; ?> type="checkbox" name="lentes" id="lentes" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Guantes:
                                                    <input <?php if($p->guantes==1) echo "checked"; ?> type="checkbox" name="guantes" id="guantes" class="form-control chk_form"></label>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Peto:
                                                    <input <?php if($p->peto==1) echo "checked"; ?> type="checkbox" name="peto" id="peto" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Respirador:
                                                    <input <?php if($p->respirador==1) echo "checked"; ?> type="checkbox" name="respirador" id="respirador" class="form-control chk_form"></label>
                                                </td>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Otro:
                                                    <input type="text" name="otro" id="otro" class="form-control" value="<?php echo $p->otro; ?>"></label>
                                                </td> 
                                                <td colspan="2" class="tdinput titulos" width="40%">

                                                </td> 
                                            </tr>
                                        </table>
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="10%" class="tdinput titulos"><label>Puesto de trabajo</label> </td>
                                                <td width="25%" class="tdinput"><input type="text" name="puesto" id="puesto" class="form-control" value="<?php echo $p->puesto; ?>"></td>
                                                <td width="25%" class="tdinput titulos"><label>Tiempo de exposición por ciclo en cada h (min)</label></td>
                                                <td width="10%" class="tdinput"><input type="number" name="tiempo_expo" id="tiempo_expo" class="form-control" value="<?php echo $p->tiempo_expo; ?>"></td>
                                                <td width="20%" class="tdinput titulos"><label>Número de ciclos por h</label></td>
                                                <td width="10%" class="tdinput"><input type="number" id="num_ciclos" name="num_ciclos" class="form-control" value="<?php echo $p->num_ciclos; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>% exposición</label></td>
                                                <td class="tdinput"><input type="number" id="porc_expo" name="porc_expo" class="form-control" value="<?php echo $p->porc_expo; ?>"></td>
                                                <td class="tdinput titulos"><label>% no exposición</label></td>
                                                <td class="tdinput"><input type="number" id="porc_no_expo" name="porc_no_expo" class="form-control" value="<?php echo $p->porc_no_expo; ?>"></td>
                                                <td class="tdinput titulos"><label>Régimen de trabajo</label></td>
                                                <td class="tdinput"><input type="text" id="regimen" name="regimen" class="form-control" value="<?php echo $p->regimen; ?>"></td>
                                            </tr>
                                        </table>     
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="20%" class="tdinput titulos"><label>Descripción de actividades del personal</label></td>
                                                <td width="80%" class="tdinput"><input type="text" id="desc_activ" name="desc_activ" class="form-control" value="<?php echo $p->desc_activ; ?>"></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" class="tdinput titulos"><label>Observaciones</label></td>
                                                <td width="80%" class="tdinput"><input type="text" id="observaciones" name="observaciones" class="form-control" value="<?php echo $p->observaciones; ?>"></td>
                                            </tr>
                                        </table> 

                                        <?php $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15_elevadas2',array('id_nom_punto'=>$p->id,"activo"=>1)); { 
                                            //log_message('error','MED: '.json_encode($med->result()));
                                            if($med->num_rows()==0){ ?>
                                                <div id="cont_ciclos_med">
                                                <table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                                    <tr class="datos">
                                                        <td class="tdinput titulos">
                                                            <label>Ciclo No.</label>
                                                        </td>
                                                        <td class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value=""></td>
                                                        <td class="tdinput titulos"><label>Temperatura axiliar inicial (°C)</label></td>
                                                        <td class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control" value=""></td>
                                                        <td class="tdinput titulos"><label>Tempertura axiliar final (°C)</label></td>
                                                        <td colspan="2" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Regíón</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                        <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Tbh °C</label></td>
                                                        <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                        <td class="tdinput titulos"><label>Tg °C</label></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Inicial</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>

                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Mitad</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>

                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Final</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_fin" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3"><input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                                    </tr>
                                                </table> 
                                                </div>
                                               <center><span style="font-size: 9px;">Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</span></center>
                                            <?php }
                                            $html=""; $cont_temps=0;
                                            foreach($med->result() as $m) { 
                                                $cont_temps++;
                                                if($cont_temps==1){
                                                    $html.='<table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                                    <tr class="datos">
                                                        <td class="tdinput titulos">
                                                            <label>Ciclo No.</label>
                                                        </td>
                                                        <td class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value="'.$m->ciclo_med.'"></td>
                                                        <td class="tdinput titulos"><label>Temperatura axiliar inicial (°C)</label></td>
                                                        <td class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control" value="'.$m->temp_ini.'"></td>
                                                        <td class="tdinput titulos"><label>Tempertura axiliar final (°C)</label></td>
                                                        <td colspan="2" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value="'.$m->temp_fin.'"></td>
                                                    </tr>
                                                
                                                    <tr>
                                                        <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Regíón</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                        <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                        <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput titulos"><label>Tbh °C</label></td>
                                                        <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                        <td class="tdinput titulos"><label>Tg °C</label></td>
                                                    </tr>';
                                                }
                                                
                                                if($m->tipo==1 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='<tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Inicial</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='<tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==1) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==1 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Mitad</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==2) {
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==2) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==1 && $m->tipo_med==3) { //ultimos registros
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td rowspan="3" class="tdinput titulos"><label>Final</label></td>
                                                        <td class="tdinput titulos"><label>Cabeza</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==2 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Abdomen</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                if($m->tipo==3 && $m->tipo_med==3) { 
                                                    if($m->hora!="00:00:00")
                                                        $hora=$m->hora;
                                                    else{
                                                        $hora="";
                                                    } 
                                                    $html.='
                                                    <tr class="datos">
                                                        <td class="tdinput titulos"><label>Tobillo</label></td>
                                                        <td class="tdinput">
                                                            <input data-id_medicion="'.$m->id.'" type="hidden" name="id_medicion" id="id_medicion" value="'.$m->id.'">
                                                            <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                            <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                            <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="'.$m->altura.'">
                                                        </td>
                                                        <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value="'.$hora.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value="'.$m->tbh.'"></td>
                                                        <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value="'.$m->tbs.'"></td>
                                                        <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value="'.$m->tg.'"></td>
                                                    </tr>';
                                                }
                                                
                                            } //foreach
                                            $html.='</table> 
                                               <center><span style="font-size: 9px;">Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</span></center>';
                                            echo $html;
                                        }
                                        echo "</div>";
                                    } 
                                    }else {?>
                                        <div id="pto_princi" class="pto_princi">
                                        <hr>
                                        <input type="hidden" name="id_pto" id="id_pto" value="0">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()" >Agregar punto</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-6">
                                                <label>Fecha</label>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-6 form-group">
                                                <input type="date" class="form-control" id="fecha" name="fecha" value="">
                                            </div>
                                        </div>    

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Bulbo Seco</label>
                                                    <select name="id_bulbo" id="id_bulbo" class="form-control">
                                                        <?php foreach($seco->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo" id="th_bulbo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo" id="ts_bulbo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo" id="tg_bulbo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo" id="acepta_bulbo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo2" id="th_bulbo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo2" id="ts_bulbo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo2" id="tg_bulbo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo2" id="acepta_bulbo2" class="form-control" value="">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Bulbo Húmedo</label>
                                                    <select name="id_bulbo_humedo" id="id_bulbo_humedo" class="form-control">
                                                        <?php foreach($humedo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo_hum" id="th_bulbo_hum" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo_hum" id="ts_bulbo_hum" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo_hum" id="tg_bulbo_hum" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo_hum" id="acepta_bulbo_hum" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_bulbo_hum2" id="th_bulbo_hum2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_bulbo_hum2" id="ts_bulbo_hum2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_bulbo_hum2" id="tg_bulbo_hum2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_bulbo_hum2" id="acepta_bulbo_hum2" class="form-control" value="">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Globo</label>
                                                    <select name="id_globo" id="id_globo" class="form-control">
                                                        <?php foreach($globo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                                    <tr>
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td> 
                                                        <td class="tdinput titulos"><label>TH 12.7 °C</label></td> 
                                                        <td class="tdinput titulos"><label>TS 46.4 °C</label></td>   
                                                        <td class="tdinput titulos"><label>TH 69.8 °C</label></td> 
                                                        <td class="tdinput titulos"><label>¿SE ACEPTA?</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_globo" id="th_globo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_globo" id="ts_globo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_globo" id="tg_globo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_globo" id="acepta_globo" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="th_globo2" id="th_globo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="ts_globo2" id="ts_globo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="number" name="tg_globo2" id="tg_globo2" class="form-control" value="">
                                                        </td>
                                                        <td class="tdinput">
                                                            <input type="text" name="acepta_globo2" id="acepta_globo2" class="form-control" value="">
                                                        </td>

                                                    </tr>   
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Termómetro digital</label>
                                                    <select name="id_termo" id="id_termo" class="form-control">
                                                        <?php foreach($termo->result() as $eq){ ?>
                                                            <option value="<?php echo $eq->id; ?>"><?php echo $eq->luxometro; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td class="tdinput titulos" width="10%">
                                                    <label>ÁREA</label>
                                                </td>   
                                                <td class="tdinput" width="40%">
                                                    <input type="text" name="area" id="area" class="form-control" value="">
                                                </td>   
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>No. PUNTO</label>  
                                                </td>  
                                                <td class="tdinput" width="15%">
                                                    <input type="text" name="num_punto" id="num_punto" class="form-control" value="">
                                                </td>
                                                <td class="tdinput titulos" width="10%"> 
                                                    <label>¿EXISTE CARGAR SOLAR?</label>  
                                                </td>  
                                                <td class="tdinput" width="15%">
                                                    <label>SI
                                                        <input type="radio" id="carga_solar" name="carga_solar" class="form-control form-control-sm chk_form">
                                                    </label>
                                                    <label>NO
                                                        <input type="radio" id="carga_solar2" name="carga_solar" class="form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</label>
                                                </td>  
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="fte_generadora" id="fte_generadora" class="form-control" value="">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES TÉCNICOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="controles_tec" id="controles_tec" class="form-control" value="">
                                                </td>   
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>CONTROLES ADMINISTRATIVOS</label>
                                                </td>   
                                                <td class="tdinput" colspan="5" width="80%">
                                                    <input type="text" name="controles_adm" id="controles_adm" class="form-control" value="">
                                                </td> 
                                            </tr>
                                        </table>
                                        <table width="100%" class="table table-responsive table-bordered" cellpadding="1">
                                            <tr>
                                                <td width="20%" colspan="2" class="tdinput titulos">TIPO DE EVALUACIÓN</td>
                                                <td width="50%" class="tdinput titulos">NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</td>
                                                <td width="15%" class="tdinput titulos">Temperatura axiliar inicial de la jornada (°C)</td>
                                                <td width="15%" class="tdinput titulos">Temperatura axiliar final de la jornada (°C)</td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput">
                                                    <label>POE
                                                        <input type="radio" id="tipo_evalua" name="tipo_evalua" class="form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <label>GEH
                                                        <input type="radio" id="tipo_evalua2" name="tipo_evalua" class="form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="nom_trabaja" id="nom_trabaja" class="form-control" value="">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="temp_aux_ini" id="temp_aux_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput">
                                                    <input type="text" name="temp_aux_fin" id="temp_aux_fin" class="form-control" value="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput">
                                                    <label>Movimiento
                                                        <input type="radio" id="tipo_mov" name="tipo_mov" class="form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                                <td class="tdinput">
                                                    <label>Fijo
                                                        <input type="radio" id="tipo_mov2" name="tipo_mov" class="form-control form-control-sm chk_form">
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Casco:
                                                    <input type="checkbox" name="casco" id="casco" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Tapones acústicos:
                                                    <input type="checkbox" name="tapones" id="tapones" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Botas de seguridad:
                                                    <input type="checkbox" name="botas" id="botas" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Lentes:
                                                    <input type="checkbox" name="lentes" id="lentes" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Guantes:
                                                    <input type="checkbox" name="guantes" id="guantes" class="form-control chk_form"></label>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Peto:
                                                    <input type="checkbox" name="peto" id="peto" class="form-control chk_form"></label>
                                                </td> 
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Respirador:
                                                    <input type="checkbox" name="respirador" id="respirador" class="form-control chk_form"></label>
                                                </td>
                                                <td class="tdinput titulos" width="20%">
                                                    <label>Otro:
                                                    <input type="text" name="otro" id="otro" class="form-control"></label>
                                                </td> 
                                                <td colspan="2" class="tdinput titulos" width="40%">

                                                </td> 
                                            </tr>
                                        </table>
                                        
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="10%" class="tdinput titulos"><label>Puesto de trabajo</label> </td>
                                                <td width="25%" class="tdinput"><input type="text" name="puesto" id="puesto" class="form-control" value=""></td>
                                                <td width="25%" class="tdinput titulos"><label>Tiempo de exposición por ciclo en cada h (min)</label></td>
                                                <td width="10%" class="tdinput"><input type="number" name="tiempo_expo" id="tiempo_expo" class="form-control" value=""></td>
                                                <td width="20%" class="tdinput titulos"><label>Número de ciclos por h</label></td>
                                                <td width="10%" class="tdinput"><input type="number" id="num_ciclos" name="num_ciclos" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>% exposición</label></td>
                                                <td class="tdinput"><input type="number" id="porc_expo" name="porc_expo" class="form-control" value=""></td>
                                                <td class="tdinput titulos"><label>% no exposición</label></td>
                                                <td class="tdinput"><input type="number" id="porc_no_expo" name="porc_no_expo" class="form-control" value=""></td>
                                                <td class="tdinput titulos"><label>Régimen de trabajo</label></td>
                                                <td class="tdinput"><input type="text" id="regimen" name="regimen" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                        <table class="table table-responsive" width="100%">
                                            <tr>
                                                <td width="20%" class="tdinput titulos"><label>Descripción de actividades del personal</label></td>
                                                <td width="80%" class="tdinput"><input type="text" id="desc_activ" name="desc_activ" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" class="tdinput titulos"><label>Observaciones</label></td>
                                                <td width="80%" class="tdinput"><input type="text" id="observaciones" name="observaciones" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                        <table id="ciclos_mediciones" class="table table-responsive" width="100%" style="text-align:center;">
                                            <tr class="datos">
                                                <td class="tdinput titulos">
                                                    <input type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <label>Ciclo No.</label>
                                                </td>
                                                <td class="tdinput"><input type="text" id="ciclo_med" name="ciclo_med" class="form-control" value=""></td>
                                                <td class="tdinput titulos"><label>Temperatura axiliar inicial (°C)</label></td>
                                                <td class="tdinput"><input type="text" id="temp_ini" name="temp_ini" class="form-control" value=""></td>
                                                <td class="tdinput titulos"><label>Tempertura axiliar final (°C)</label></td>
                                                <td colspan="2" class="tdinput"><input type="text" id="temp_fin" name="temp_fin" class="form-control" value=""></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" class="tdinput titulos"><label>Evaluación</label></td>
                                                <td rowspan="2" class="tdinput titulos"><label>Regíón</label></td>
                                                <td rowspan="2" class="tdinput titulos"><label>Altura (m)</label></td>
                                                <td rowspan="2" class="tdinput titulos"><label>Hora</label></td>
                                                <td colspan="3" class="tdinput titulos"><label>Medición</label></td>
                                            </tr>
                                            <tr>
                                                <td class="tdinput titulos"><label>Tbh °C</label></td>
                                                <td class="tdinput titulos"><label>Tbs °C</label></td>
                                                <td class="tdinput titulos"><label>Tg °C</label></td>
                                            </tr>
                                            <tr class="datos">
                                                <td rowspan="3" class="tdinput titulos"><label>Inicial</label></td>
                                                <td class="tdinput titulos"><label>Cabeza</label></td>
                                                <td class="tdinput"><label>
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                    <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">

                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Abdomen</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                    <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Tobillo</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                    <input data-tipo_med="1" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="1">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_ini" name="hora_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_ini" name="tbh_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_ini" name="tbs_ini" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_ini" name="tg_ini" class="form-control" value=""></td>
                                            </tr>

                                            <tr class="datos">
                                                <td rowspan="3" class="tdinput titulos"><label>Mitad</label></td>
                                                <td class="tdinput titulos"><label>Cabeza</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                    <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_media" name="hora_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_media" name="tbh_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_media" name="tbs_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_media" name="tg_media" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Abdomen</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                    <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_media" name="hora_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_media" name="tbh_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_media" name="tbs_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_media" name="tg_media" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Tobillo</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                    <input data-tipo_med="2" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="2">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_media" name="hora_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_media" name="tbh_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_media" name="tbs_media" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_media" name="tg_media" class="form-control" value=""></td>
                                            </tr>

                                            <tr class="datos">
                                                <td rowspan="3" class="tdinput titulos"><label>Final</label></td>
                                                <td class="tdinput titulos"><label>Cabeza</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="1" type="hidden" id="tipo" name="tipo" class="form-control" value="1">
                                                    <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_fin" name="hora_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_fin" name="tbh_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_fin" name="tbs_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_fin" name="tg_fin" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Abdomen</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="2" type="hidden" id="tipo" name="tipo" class="form-control" value="2">
                                                    <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_fin" name="hora_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_fin" name="tbh_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_fin" name="tbs_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_fin" name="tg_fin" class="form-control" value=""></td>
                                            </tr>
                                            <tr class="datos">
                                                <td class="tdinput titulos"><label>Tobillo</label></td>
                                                <td class="tdinput">
                                                    <input data-id_medicion="0" type="hidden" name="id_medicion" id="id_medicion" value="0">
                                                    <input data-tipo="3" type="hidden" id="tipo" name="tipo" class="form-control" value="3">
                                                    <input data-tipo_med="3" type="hidden" id="tipo_med" name="tipo_med" class="form-control" value="3">
                                                    <input type="number" id="altura_ini" name="altura_ini" class="form-control" value="">
                                                </td>
                                                <td class="tdinput"><input type="time" id="hora_fin" name="hora_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbh_fin" name="tbh_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tbs_fin" name="tbs_fin" class="form-control" value=""></td>
                                                <td class="tdinput"><input type="number" id="tg_fin" name="tg_fin" class="form-control" value=""></td>
                                            </tr>
                                        </table> 
                                       <center><span style="font-size: 9px;">Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</span></center>
                                       </div>
                                    <?php } ?>
                            </div>
                        </div>                 
                    

                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="saveDetalles()" ><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
