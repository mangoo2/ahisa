<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    .tdinput{
            padding: 6px !important;
        }
    .tdinput input{
            padding: 6px !important;
        }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
        .tablecaptura th{
            padding: 5px;
            font-size: 13px;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .reabtn, .btn-lectura-add{
            font-size: 11px;
        }
        .reabtn span , .btn-lectura-add span{
            display: none;
        }
    }

</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url();?>index.php/Nom/add11personal/<?php echo $idordenes;?>">Captura personal</a>
                            </div>
                        </div>
                        <form class="form" id="form_nom" onchange="saveform('nom11',<?php echo $idnom;?>)">
                            <input type="hidden" name="idnom" id="idnom" value="<?php echo $idnom;?>">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>No. de Informe</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" name="num_informe" id="num_informe" value="<?php echo $num_informe;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Razón Social</label>
                                </div>
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" name="razon_social" id="razon_social" value="<?php echo $razon_social;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-6">
                                    <label>Fecha</label>
                                </div>
                                <div class="col-md-8 col-sm-6 col-6 form-group">
                                    <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $fecha;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Verificación inicial</label>
                                    <input type="text" name="veri_ini" id="veri_ini" class="form-control" value="<?php echo $veri_ini;?>">
                                </div>
                                
                                <div class="col-md-3"> 
                                    <label>Verificación final</label>    
                                    <input type="text" name="veri_fin" id="veri_fin" class="form-control" value="<?php echo $veri_fin;?>">
                                </div>
                                <div class="col-md-3">
                                    <label>Criterio de aceptación</label>
                                    <input type="text" name="criterio" id="criterio" class="form-control" value="<?php if($criterio!="") echo $criterio; else echo "+- 1db(A)"; ?>">
                                </div>
                                <div class="col-md-3">
                                    <label>Cumple criterio</label>
                                    <input type="text" name="cumple_criterio" id="cumple_criterio" class="form-control" value="<?php echo $cumple_criterio;?>">
                                </div>
                                <div class="col-md-3">
                                    <label>Potencia batería inicio (V)</label>
                                    <input type="text" name="poten_ini" id="poten_ini" class="form-control" value="<?php echo $poten_ini;?>">
                                </div>
                                <div class="col-md-3">
                                    <label>Potencia batería fin (V)</label>
                                    <input type="text" name="poten_fin" id="poten_fin" class="form-control" value="<?php echo $poten_fin;?>">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Sonómetro</label>
                                        <select name="id_sonometro" id="id_sonometro" class="form-control">
                                            <?php foreach($sono->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_sonometro){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Calibrador Acústico</label>
                                        <select name="id_calibrador" id="id_calibrador" class="form-control">
                                            <?php foreach($calibra->result() as $eq){ ?>
                                                <option value="<?php echo $eq->id; ?>" <?php if($eq->id==$id_calibrador){ echo "selected"; } ?>><?php echo $eq->luxometro; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">     
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <label >CONDICIONES DE OPERACIÓN AL MOMENTO DE REALIZAR LA EVALUACIÓN</label>
                                    <input type="text" name="condiciones_opera" id="condiciones_opera" class="form-control" value="<?php echo $condiciones_opera;?>">
                                </div>
                                <div class="col-md-12">     
                                    <br>
                                </div>
                            </div>  
                        </form>
                                           
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url().'Nom/inserpuntonom11new/'.$idnom.'/'.$idordenes;?>"><i class="fa fa-plus" aria-hidden="true"></i> Agregar punto</a>
                            </div>
                        </div>
                        <?php 
                            $rowpuntostr=1;
                            $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
                            foreach ($rowpuntos->result() as $item) { 
                                $idpunto=$item->punto;
                                
                                $buttondelete='<button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletepunto('.$idnom.','.$idpunto.')"><i class="fa fa-trash-o"></i></button>';

                                $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto,"activo"=>1));
                                foreach ($rownomd->result() as $itemnomd) { 
                                    $idnomd=$itemnomd->idnomd; 
                                    $horai1=$itemnomd->horai1; 
                                    $horaf1=$itemnomd->horaf1;
                                    $horai2=$itemnomd->horai2; 
                                    $horaf2=$itemnomd->horaf2;
                                    $horai3=$itemnomd->horai3; 
                                    $horaf3=$itemnomd->horaf3; 
                                    $val_nrr=$itemnomd->val_nrr; 
                                    ?>
                                    <form id="form_nom11d_<?php echo $itemnomd->idnomd;?>" onchange="saveform('nom11d',<?php echo $itemnomd->idnomd;?>)">
                                        <input type="hidden" name="idnomd" value="<?php echo $itemnomd->idnomd;?>">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-5 col-5">
                                                <?php echo $buttondelete;?> No. Punto
                                            </div>
                                            <div class="col-md-1 col-sm-7 col-7">
                                                <?php echo $rowpuntostr;?>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-5">
                                                Tiempo efectivo de exposición al ruido (h)
                                            </div>
                                            <div class="col-md-3 col-sm-7 col-7 form-group">
                                                <input type="number" name="tiempo_ruido" id="tiempo_ruido" value="<?php echo $itemnomd->tiempo_ruido?>" class="form-control">
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5">
                                                Ti (minutos)
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5 form-group">
                                                <input type="number" name="ti" id="ti" value="<?php echo $itemnomd->tiempo_ruido*60?>" class="form-control">
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5">
                                                Te para 8 h en minutos 
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5 form-group">
                                                <input type="number" name="te" id="te" value="<?php if($itemnomd->te>0){ echo $itemnomd->te; }else{ echo '480';} ?>" class="form-control">
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5">
                                                Área 
                                            </div>
                                            <div class="col-md-3 col-sm-5 col-5 form-group">
                                                <input type="text" name="area" id="area" value="<?php echo $itemnomd->area?>" class="form-control">
                                            </div>
                                            <div class="col-md-2 col-sm-5 col-5">
                                                Identificación
                                            </div>
                                            <div class="col-md-4 col-sm-7 col-7 form-group">
                                                <input type="text" name="identificacion" id="identificacion" value="<?php echo $itemnomd->identificacion?>" class="form-control">
                                            </div>
                                            <div class="col-md-2 col-sm-5 col-5">
                                                Ruido
                                            </div>
                                            <div class="col-md-4 col-sm-7 col-7 form-group">
                                                <label><input type="radio" name="tipo_ruido" id="ruido" value="0" <?php if($itemnomd->tipo_ruido==0){echo 'checked';}?> > Estable</label><br>
                                                <label><input type="radio" name="tipo_ruido" id="ruido" value="1" <?php if($itemnomd->tipo_ruido==1){echo 'checked';}?>> Inestable</label><br>
                                                <label><input type="radio" name="tipo_ruido" id="ruido" value="2" <?php if($itemnomd->tipo_ruido==2){echo 'checked';}?>> Impulsivo</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label>OBSERVACIONES</label>
                                                <input type="text" name="observaciones" id="observaciones" class="form-control" value="<?php echo $itemnomd->observaciones;?>">
                                            </div>
                                        </div>
                                    </form>
                                    <?php 
                                } 
                                ?>
                                <?php $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); ?>
                                <table class="table table-bordered tablecaptura" id="table_lectura" style="text-align: center;">
                                    <thead>
                                        <tr onchange="updateHoras(<?php echo $idnomd;?>)">
                                            <th rowspan="2">Lectura<br>
                                                <?php if($rowlecturas->num_rows()<15){ ?>
                                                    <a type="button" class="btn btn-info mr-1 mb-1 btn-lectura-add" href="<?php echo base_url().'Nom/inserpuntonom11newlectura/'.$idnom.'/'.$idpunto.'/'.$idordenes;?>" ><i class="fa fa-plus"></i> <span>Lectura</span></a>
                                                <?php } ?>
                                            </th>
                                            <th >Periodo 1
                                                <br><label>Hora inicio</label><input type="time" id="hora_inicio1_<?php echo $idnomd;?>" value="<?php echo $horai1; ?>" class="form-control hrp">
                                                <br><label>Hora fin</label><input type="time" id="hora_fin1_<?php echo $idnomd;?>" value="<?php echo $horaf1; ?>" class="form-control hrp">
                                            </th>
                                            <th >Periodo 2
                                                <br><label>Hora inicio</label><input type="time" id="hora_inicio2_<?php echo $idnomd;?>" value="<?php echo $horai2; ?>" class="form-control hrp">
                                                <br><label>Hora fin</label><input type="time" id="hora_fin2_<?php echo $idnomd;?>" value="<?php echo $horaf2; ?>" class="form-control hrp">
                                            </th>
                                            <th >Periodo 3
                                                <br><label>Hora inicio</label><input type="time" id="hora_inicio3_<?php echo $idnomd;?>" value="<?php echo $horai3; ?>" class="form-control hrp">
                                                <br><label>Hora fin</label><input type="time" id="hora_fin3_<?php echo $idnomd;?>" value="<?php echo $horaf3; ?>" class="form-control hrp">
                                            </th>
                                            <th rowspan="2"></th>
                                        </tr>
                                        <tr>
                                            <th >NSCE dB(A)</th>
                                            <th >NSCE dB(A)</th>
                                            <th >NSCE dB(A)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
                                            $rowlecturas_tr=1;
                                            foreach ($rowlecturas->result() as $iteml) { ?>
                                                <tr onchange="saveform('nom11d_lecturas',<?php echo $iteml->idnomd;?>)" >
                                                    <td class="tdinput"><?php echo $rowlecturas_tr;?><div id="lectura_<?php echo $iteml->idnomd;?>"></div></td>
                                                    <td class="tdinput">
                                                        <input type="text" id="periodo1_<?php echo $iteml->idnomd;?>" value="<?php echo $iteml->periodo1;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="periodo2_<?php echo $iteml->idnomd;?>" value="<?php echo $iteml->periodo2;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="periodo3_<?php echo $iteml->idnomd;?>" value="<?php echo $iteml->periodo3;?>" class="form-control">
                                                    </td>
                                                    <th><button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deletel(<?php echo $iteml->idnomd;?>)"><i class="fa fa-trash-o"></i></button></th>
                                                </tr>
                                            <?php
                                            $rowlecturas_tr++;
                                            }
                                        ?>
                                    </tbody> 
                                </table>
                                <div class="row">
                                    <div class="col-md-12" style="text-align:center;">REGISTRO DEL ESPECTRO ACÚSTICO</div>
                                </div>
                                <?php $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); ?>
                                <table class="table table-bordered tablecaptura rea" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Periodo<br>
                                                <?php if($rownomdrea->num_rows()<=1){ ?>
                                                    <a type="button" class="btn btn-info mr-1 mb-1 reabtn" href="<?php echo base_url().'Nom/inserpuntonom11newrea/'.$idnom.'/'.$idpunto.'/'.$idordenes;?>" ><i class="fa fa-plus"></i> <span>Periodo</span></a>
                                                <?php } ?>
                                                </th>
                                            <th colspan="2">Hora</th>
                                            <th colspan="2">Ponderación</th>
                                            <th colspan="10">Frecuencias Centrales (Hz)</th>
                                        </tr>
                                        <tr>
                                            <th>Inicial</th>
                                            <th>Final</th>
                                            <th>db(A)</th>
                                            <th>Lineal</th>
                                            <th>31.5</th>
                                            <th>63</th>
                                            <th>125</th>
                                            <th>250</th>
                                            <th>500</th>
                                            <th>1000</th>
                                            <th>2000</th>
                                            <th>4000</th>
                                            <th>8000</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            
                                            $rownomdreaperiodo=1;
                                            foreach ($rownomdrea->result() as $itrea) { ?>
                                                <tr onchange="saveform('nom11d_reg_espec_acustico',<?php echo $itrea->idnomd;?>)">
                                                    <td><?php echo $rownomdreaperiodo; ?><div id="rea_<?php echo $itrea->idnomd;?>"></div></td>
                                                    <td class="tdinput">
                                                        <input type="time" id="horai_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->horai;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="time" id="horaf_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->horaf;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_db_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_db;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_lineal_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_lineal;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_31_5_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_31_5;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_63_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_63;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_125_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_125;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_250_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_250;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_500_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_500;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_1000_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_1000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_2000_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_2000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_4000_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_4000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="rea_8000_<?php echo $itrea->idnomd;?>" value="<?php echo $itrea->rea_8000;?>" class="form-control">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-raised gradient-ibiza-sunset white" onclick="deleterea(<?php echo $itrea->idnomd;?>)"><i class="fa fa-trash-o"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $rownomdreaperiodo++;
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-12" style="text-align:center;">FACTOR DE ATENUACIÓN DEL EQUIPO DE PROTECCIÓN AUDITIVA</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="text-align:center;"><label>NRR = </label> <input type="number" id="nrr_val_<?php echo $idnomd; ?>" name="val_nrr" class="form-control-sm" onchange="updateValor('nom11d',this.value,this.name,<?php echo $idnomd;?>)" value="<?php echo $val_nrr;?>"></div>
                                </div>

                                <table class="table table-bordered tablecaptura faepa" style="text-align: center;">
                                    <thead>
                                        <tr>
                                            <th>Frecuencia (Hz)</th>
                                            <th>125</th>
                                            <th>250</th>
                                            <th>500</th>
                                            <th>1000</th>
                                            <th>2000</th>
                                            <th>3000</th>
                                            <th>4000</th>
                                            <th>6000</th>
                                            <th>8000</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $rownomdfaep=$this->ModeloCatalogos->getselectwheren('nom11d_faepa',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                                            foreach ($rownomdfaep->result() as $itemfp) {
                                                ?>
                                                <tr onchange="saveform('nom11d_faepa',<?php echo $itemfp->idnomd;?>)">
                                                    <th>Atenuación (dB)</th>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_125_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_125;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_250_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_250;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_500_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_500;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_1000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_1000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_2000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_2000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_3000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_3000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_4000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_4000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_6000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_6000;?>" class="form-control">
                                                    </td>
                                                    <td class="tdinput">
                                                        <input type="text" id="faepa_8000_<?php echo $itemfp->idnomd;?>" value="<?php echo $itemfp->faepa_8000;?>" class="form-control">
                                                    </td>
                                                </tr>
                                                <?php
                                                // code...
                                            }
                                        ?>
                                    </tbody>
                                    
                                </table>
                        <?php $rowpuntostr++;
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-12" style="text-align: left;">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url().'Nom/inserpuntonom11new/'.$idnom.'/'.$idordenes;?>"><i class="fa fa-plus" aria-hidden="true"></i> Agregar punto</a>
                            </div>
                        </div>
                            
                        <div class="row"> 
                            <div class="col-md-12" style="text-align: end;"> 
                                <button type="button" class="btn btn-info mr-1 mb-1 buttonsave" onclick="save()"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button> 
                            </div> 
                        </div> 
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
