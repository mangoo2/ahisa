<style type="text/css">
    @media (max-width:500px){
        .content-wrapper {
            padding: 0 10px;
        }
        .colmd12responsive{
            padding: 0;
        }
        #table_nom td, #table_nom th{
            font-size: 12px;
        }
        .header_doc{
            text-align:right;
        }
        p{
            text-align: justify;
        }
    }
    .txt_div_p p{
        font-weight: normal !important;
    }
</style>
<input type="hidden" id="tipo_img_carga">
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">
                        NOM 
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="id_cliente">Norma</label>
                                    <select id="norma" class="form-control">
                                        <!--<option value="0">Todas</option>-->
                                        <option value="11">Nom-011</option>
                                        <option value="25">Nom-025</option>
                                        <option value="22">Nom-022</option>
                                        <option value="81">Nom-081</option>
                                        <option value="15">Nom-015</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="Fecha1">Desde</label>
                                <input id="Fecha1" type="date" class="form-control">
                            </div>
                   
                            <div class="col-md-3 form-group">
                            <label for="Fecha2">Hasta</label>
                                <input id="Fecha2" type="date" class="form-control">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="id_cliente">Cliente</label>
                                <select id="id_cliente" class="form-control">
                                </select>
                            </div>
                            <!--<div class="col-md-12" style="text-align: end;">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url();?>Nom/add" >Agregar</a>
                            </div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-12 colmd12responsive">
                                <table class="table table-bordered" id="table_nom" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="6%">#</th>
                                            <th width="6%">Orden</th>
                                            <th width="10%">Servicio</th>
                                            <th width="20%">Razón social</th>
                                            <th width="13%">No. Informe</th>
                                            <th width="15%">Fecha</th>
                                            <th width="30%"></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade text-left" id="modal_conclusion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_modaltxt" id="myModalLabel1">Conclusiones de la evaluación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- DATOS PARA LA NOM 11 -->
            <div class="modal-body">
                <div class="row" id="cont_conc11">
                    <div class="col-12">
                        <input type="hidden" id="tipo_txt" value="1">
                        <form class="form" id="form_conclusiones11">
                            <input type="hidden" id="id_nom_conc11" name="id_nom" value="">
                            <input type="hidden" id="id_conclu11" name="id" value="0">
                            <div class="row">
                   
                                <div class="col-md-5 form-group">
                                    <label>Total de puntos ambientales:</label>
                                    <input type="number" min="0" class="form-control" name="total_ptos_amb" id="total_ptos_amb">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>No superan (TMPE):</label>
                                    <input type="number" min="0" class="form-control" id="no_superan_amb" name="no_superan_amb">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Superan (TMPE):</label>
                                    <input type="number" min="0" class="form-control" id="superan_amb" name="superan_amb">
                                </div>
                                <div class="col-lg-7 mx-auto chr_amb" style="display:none">
                                    <canvas id="chartAmb" style="width:100%;"></canvas>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label>Total de puntos personales:</label>
                                    <input type="number" min="0" class="form-control" name="total_ptos_pers" id="total_ptos_pers">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>No superan (TMPE):</label>
                                    <input type="number" min="0" class="form-control" id="no_superan_pers" name="no_superan_pers">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Superan (TMPE):</label>
                                    <input type="number" min="0" class="form-control" id="superan_pers" name="superan_pers">
                                </div>
                                <div class="col-lg-7 mx-auto chr_pers" style="display:none">
                                    <canvas id="chartPers" style="width:100%;"></canvas>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="foot_cont_conc11">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="saveConclusion11" class="btn btn-outline-primary btn-success">Guardar</button>
            </div>
            <!-- TERMINA DATOS PARA LA NOM 11 -->

            <!-- DATOS PARA LA NOM 22 -->
            <div class="modal-body">
                <div class="row" id="cont_conc22">
                    <div class="col-12">
                        <input type="hidden" id="tipo_txt" value="1">
                        <form class="form" id="form_conclusiones22">
                            <input type="hidden" id="id_nom_conc22" name="id_nom" value="">
                            <input type="hidden" id="id_conclu22" name="id" value="0">
                            <div class="row">
                   
                                <div class="col-md-5 form-group">
                                    <label>Total de electrodos en sistema de puesta a tierra:</label>
                                    <input type="number" min="0" class="form-control" name="total_electrodo_pt" id="total_electrodo_pt">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>No superan (LMP) de 25 Ω:</label>
                                    <input type="number" min="0" class="form-control" id="no_superan_pt" name="no_superan_pt">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Superan (LMP) de 25 Ω:</label>
                                    <input type="number" min="0" class="form-control" id="supera_pt" name="supera_pt">
                                </div>
                                <div class="col-lg-7 mx-auto chr_spt" style="display:none">
                                    <canvas id="chartSPT" style="width:100%;"></canvas>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label>Total de electrodos en sistema de pararrayos:</label>
                                    <input type="number" min="0" class="form-control" name="total_electrodo_pr" id="total_electrodo_pr">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>No superan (LMP): de 10 Ω</label>
                                    <input type="number" min="0" class="form-control" id="no_supera_pr" name="no_supera_pr">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Superan (LMP) de 10 Ω:</label>
                                    <input type="number" min="0" class="form-control" id="supera_pr" name="supera_pr">
                                </div>
                                <div class="col-lg-7 mx-auto chr_spr" style="display:none">
                                    <canvas id="chartSPR" style="width:100%;"></canvas>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label>Total de puntos de continuidad eléctrica:</label>
                                    <input type="number" min="0" class="form-control" name="total_ptos_continuidad" id="total_ptos_continuidad">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>Existe continuidad:</label>
                                    <input type="number" min="0" class="form-control" id="existe_conti" name="existe_conti">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label>No existe continuidad:</label>
                                    <input type="number" min="0" class="form-control" id="no_existe_conti" name="no_existe_conti">
                                </div>
                                <div class="col-lg-7 mx-auto chr_ce" style="display:none">
                                    <canvas id="chartCE" style="width:100%;"></canvas>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="foot_cont_conc22">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="saveConclusion22" class="btn btn-outline-primary btn-success">Guardar</button>
            </div>
            <!-- TERMINA DATOS PARA LA NOM 11 -->

            <!-- DATOS PARA LA NOM 25 -->
            <div class="modal-body">
                <div class="row" id="cont_conc25">
                    <div class="col-12">
                        <input type="hidden" id="tipo_txt" value="1">
                        <form class="form" id="form_conclusiones">
                            <input type="hidden" id="id_nom_conc" name="id_nom" value="">
                            <input type="hidden" id="id_conclu" name="id" value="0">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Total de puntos</label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input type="number" min="1" class="form-control" name="total_ptos" id="total_ptos" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="pull-left">Iluminación</h5>  
                                </div>
                                <div class="col-md-3">
                                    <label class="switch">
                                        <input type="checkbox" checked id="ilumina">
                                        <span class="sliderN round"></span>
                                    </label>
                                </div>
                                <div class="col-md-2" style="display:none;">
                                    <h5 class="pull-left">Reflexión</h5>  
                                </div>
                                <div class="col-md-3" style="display:none;">
                                    <label class="switch">
                                        <input type="checkbox" checked id="reflexion" disabled>
                                        <span class="sliderN round"></span>
                                    </label>
                                </div>
                            </div>
                        </form>
                        <div id="cont_table_ilum">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Con incidencia solar</label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input type="number" min="1" class="form-control" id="ptos_incidencia">
                                </div>
                                <div class="col-md-3">
                                    <label>Sin incidencia solar</label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input value="0" type="number" min="0" class="form-control" id="ptos_sin_incidencia">
                                </div>
                            </div>
                            <table id="table_ilum" class="table table-bordered table-responsive" width="100%">
                                <thead style="text-align:center">
                                    <tr>
                                        <th colspan="3">Con incidencia solar</th>
                                    </tr>
                                    <tr>
                                        <th>No. de puntos evaluados</th>
                                        <th>No. de puntos que superan el NMI</th>
                                        <th>No. de puntos que no superan el NMI</th>
                                    </tr>
                                </thead>
                                <tbody class="trbody" id="cont_tabla_ilum">
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Primera Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="id1" name="id" value="0">
                                            <input type="hidden" name="medicion" value="1">
                                            <input data-tipo="1" data-med="1" type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph" id="num_ptos_evalua1"></td>
                                        <td><input data-tipo="1" data-med="1" type="number" id="num_supera1" name="num_supera" class="form-control form-control-sm puntos_graph"> </td>
                                        <td><input data-tipo="1" data-med="1" type="number" id="num_no_supera1" name="num_no_supera" class="form-control form-control-sm puntos_graph"></td>
                                    </tr>
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Segunda Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="id2" name="id" value="0">
                                            <input type="hidden" name="medicion" value="2">
                                            <input data-tipo="1" data-med="2" type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph" id="num_ptos_evalua2"></td>
                                        <td><input data-tipo="1" data-med="2" type="number" id="num_supera2" name="num_supera" class="form-control form-control-sm puntos_graph"> </td>
                                        <td><input data-tipo="1" data-med="2" type="number" id="num_no_supera2" name="num_no_supera" class="form-control form-control-sm puntos_graph"></td>
                                    </tr>
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Tercera Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="id3" name="id" value="0">
                                            <input type="hidden" name="medicion" value="3">
                                            <input data-tipo="1" data-med="3" type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph" id="num_ptos_evalua3"></td>
                                        <td><input data-tipo="1" data-med="3" type="number" id="num_supera3" name="num_supera" class="form-control form-control-sm puntos_graph"> </td>
                                        <td><input data-tipo="1" data-med="3" type="number" id="num_no_supera3" name="num_no_supera" class="form-control form-control-sm puntos_graph"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-7 mx-auto chr chr1" style="display:none">
                                <canvas id="myChart" style="width:100%;"></canvas>
                            </div>
                            <div class="col-lg-7 mx-auto chr chr2" style="display:none">
                                <canvas id="myChart2" style="width:100%;"></canvas>
                            </div>
                            <div class="col-lg-7 mx-auto chr chr3" style="display:none">
                                <canvas id="myChart3" style="width:100%;"></canvas>
                            </div>
                            <div class="col-md-12">
                                <br>
                            </div>
                            <div id="cont_table_ilum_sin">
                                <table id="table_ilum_sin" class="table table-bordered table-responsive" width="100%">
                                    <thead style="text-align:center">
                                        <tr>
                                            <th colspan="3">Sin incidencia solar</th>
                                        </tr>
                                        <tr>
                                            <th>No. de puntos evaluados</th>
                                            <th>No. de puntos que superan el NMP</th>
                                            <th>No. de puntos que no superan el NMP</th>
                                        </tr>
                                    </thead>
                                    <tbody class="trbody" id="cont_tabla_ilum_sin">
                                        <tr>
                                            <td><input type="hidden" id="ids1" name="id" value="0">
                                                <input type="hidden" name="medicion" value="1">
                                                <input data-tipo="11" data-med="1" type="number" id="num_ptos_sin_evalua1" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua_sin puntos_graph"></td>
                                            <td><input data-tipo="11" data-med="1" type="number" id="num_sin_supera1" name="num_supera" class="form-control form-control-sm puntos_graph"> </td>
                                            <td><input data-tipo="11" data-med="1" type="number" id="num_sin_no_supera1" name="num_no_supera" class="form-control form-control-sm puntos_graph"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-7 mx-auto chrs chrs1" style="display:none">
                                <canvas id="myChartsin" style="width:100%;"></canvas>
                            </div>
                            <div class="col-md-12">
                                <br>
                            </div>
                        </div>

                        <div id="cont_table_reflex" >
                            <div class="col-md-12">
                                <br>
                                <hr>
                                <h4 class="modal-title">Puntos de reflexión</h4>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Con incidencia solar</label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input type="number" min="1" class="form-control" id="ptos_incidencia_ref">
                                </div>
                                <div class="col-md-3">
                                    <label>Sin incidencia solar</label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <input value="0" type="number" min="0" class="form-control" id="ptos_sin_incidencia_ref">
                                </div>
                            </div>
                            <table id="table_ilum_reflex" class="table table-bordered table-responsive" width="100%">
                                <thead style="text-align:center">
                                    <tr>
                                        <th colspan="3">Con incidencia solar</th>
                                    </tr>
                                    <tr>
                                        <th>No. de puntos evaluados</th>
                                        <th>No. de puntos que superan el NMP</th>
                                        <th>No. de puntos que no superan el NMP</th>
                                    </tr>
                                </thead>
                                <tbody class="trbody" id="cont_tabla_ilum_ref">
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Primera Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="idr1" name="id" value="0">
                                            <input type="hidden" name="medicion" value="1">
                                            <input data-tipo="2" data-med="1" type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph_sin" id="num_ptos_evaluar1"></td>
                                        <td><input data-tipo="2" data-med="1" type="number" id="num_superar1" name="num_supera" class="form-control form-control-sm puntos_graph_sin"> </td>
                                        <td><input data-tipo="2" data-med="1" type="number" id="num_no_superar1" name="num_no_supera" class="form-control form-control-sm puntos_graph_sin"></td>
                                    </tr>
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Segunda Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="idr2" name="id" value="0">
                                            <input type="hidden" name="medicion" value="2">
                                            <input data-tipo="2" data-med="2"type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph_sin" id="num_ptos_evaluar2"></td>
                                        <td><input data-tipo="2" data-med="2"type="number" id="num_superar2" name="num_supera" class="form-control form-control-sm puntos_graph_sin"> </td>
                                        <td><input data-tipo="2" data-med="2"type="number" id="num_no_superar2" name="num_no_supera" class="form-control form-control-sm puntos_graph_sin"></td>
                                    </tr>
                                    <!--<tr>
                                        <th style="text-align:center" colspan="3">Tercera Medición</th>
                                    </tr>-->
                                    <tr>
                                        <td><input type="hidden" id="idr3" name="id" value="0">
                                            <input type="hidden" name="medicion" value="3">
                                            <input data-tipo="2" data-med="3"type="number" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evalua puntos_graph_sin" id="num_ptos_evaluar3"></td>
                                        <td><input data-tipo="2" data-med="3"type="number" id="num_superar3" name="num_supera" class="form-control form-control-sm puntos_graph_sin"> </td>
                                        <td><input data-tipo="2" data-med="3"type="number" id="num_no_superar3" name="num_no_supera" class="form-control form-control-sm puntos_graph_sin"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-7 mx-auto chr_ref chr_ref1" style="display:none">
                                <canvas id="myChartr" style="width:100%;"></canvas>
                            </div>
                            <div class="col-lg-7 mx-auto chr_ref chr_re2" style="display:none">
                                <canvas id="myChartr2" style="width:100%;"></canvas>
                            </div>
                            <div class="col-lg-7 mx-auto chr_ref chr_re3" style="display:none">
                                <canvas id="myChartr3" style="width:100%;"></canvas>
                            </div>
                            <div class="col-md-12">
                                <br>
                            </div>
                            <div id="cont_table_ilum_sin_reflex">
                                <table id="table_ilum_sin_reflex" class="table table-bordered table-responsive" width="100%">
                                    <thead style="text-align:center">
                                        <tr>
                                            <th colspan="3">Sin incidencia solar</th>
                                        </tr>
                                        <tr>
                                            <th>No. de puntos evaluados</th>
                                            <th>No. de puntos que superan el NMP</th>
                                            <th>No. de puntos que no superan el NMP</th>
                                        </tr>
                                    </thead>
                                    <tbody class="trbody" id="cont_tabla_ilum_ref_sin">
                                        <tr>
                                            <td><input type="hidden" id="idsr1" name="id" value="0">
                                                <input type="hidden" name="medicion" value="1">
                                                <input data-tipo="22" data-med="1" type="number" id="num_ptos_sin_evaluar1" name="num_ptos_evalua" class="form-control form-control-sm num_ptos_evaluaref_sin puntos_graph_sin"></td>
                                            <td><input data-tipo="22" data-med="1" type="number" id="num_sin_superar1" name="num_supera" class="form-control form-control-sm puntos_graph_sin"> </td>
                                            <td><input data-tipo="22" data-med="1" type="number" id="num_sin_no_superar1" name="num_no_supera" class="form-control form-control-sm puntos_graph_sin"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-7 mx-auto chrs_ref">
                                <canvas id="myChartsin_ref" style="width:100%;"></canvas>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="foot_cont_conc25">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="saveConclusion" class="btn btn-outline-primary btn-success">Guardar</button>
            </div>
            <!-- TERMINA DATOS PARA LA NOM 25 -->
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_intro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title title_modaltxt" id="myModalLabel1">Conclusiones de la evaluación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <input type="hidden" id="id_nom">
                    <input type="hidden" id="tipo_txt" value="1">
                    <input type="hidden" id="nom" value="">
                    <div class="form-group">
                        <p id="subtitle_txt">Texto de conclusión </p>
                        <div class="controls">
                            <div class="controls" id="cont_txt_area">
                                <textarea id="txt_intro" class="form-control toupper" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" id="save" class="btn btn-outline-primary btn-success">Guardar</button>
        </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_img_desc" tabindex="-1" role="dialog" aria-labelledby="descrips">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title txttitle_imgs" id="descrips">Descripción: Anexo IV</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <input type="hidden" id="id_nom_img_desc">

                   <div class="form-group">
                        <div class="col-md-8" id="cont_tipo_plano11" style="display:none">
                            <div class="form-group">
                                <label for="tipo_plano11">Tipo de plano</label>
                                <select id="tipo_plano11" class="form-control">
                                    <option value="1">Plano de distribución de las áreas en que exista el ruido</option>
                                    <option value="2">Plano de ubicación de la maquinaria y equipos generadores de ruido</option>
                                    <option value="3">Plano de identificación de los puntos de medición</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8" id="cont_tipo_plano81" style="display:none">
                            <div class="form-group">
                                <label for="tipo_plano81">Tipo de plano</label>
                                <select id="tipo_plano81" class="form-control">
                                    <option value="1">Plano de ubicación de la fuente fija y sus colindancias</option>
                                    <option value="2">Plano de ubicación de la maquinaria y/o procesos emisores de ruido y niveles sonoros detectados en el reconocimiento inicial</option>
                                    <option value="3">Plano de ubicación de la(s) zona(s) crítica(s) de los puntos de medición</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8" id="cont_tipo_plano15" style="display:none">
                            <div class="form-group">
                                <label for="tipo_plano15">Tipo de plano</label>
                                <select id="tipo_plano15" class="form-control">
                                    <option value="1">Plano de identificación de las fuentes generadoras de la condición térmica extrema</option>
                                    <option value="2">Plano de identificación de los puntos de medición</option>
                                </select>
                            </div>
                        </div>
                        <p id="subtitle_imgs">Plano de identificación de los puntos de medición. </p>
                        <div class="row">
                            <div class="col-md-12 tipoimagetext" style="display: none;">
                                <select id="tipoimagetext" class="form-control" onchange="tipoimagetext(this.value)">
                                    <option value="0">Imagen</option>
                                    <option value="1">Texto</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="controls">
                            <div class="controls">
                                <div class="col-md-12" id="cont_inputFile">
                                   <input type="file" name="inputFile" id="inputFile" class="form-control"> 
                                </div>
                            </div>
                            <div class="controls" id="cont_imgs_desc">
                                <div class="col-md-12">
                                    
                                </div>
                            </div>
                            <div class="controls" id="cont_imgs_text" style="display:none;">
                                <div class="col-md-12 txt_div_p">
                                    <textarea id="text_info"></textarea>
                                    <button type="button" onclick="saveinfo(25)" class="btn btn-outline-primary btn-success">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            
        </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_reso_equipo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
  <div class="modal-dialog modal-lg" role="document" style="max-width: 75%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title title_modaltxt" id="myModalLabel1">Resolución de Equipo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <input type="hidden" id="id_nom_re">
            <div class="row">
                <div class="col-md-2">
                    <h5 class="pull-right">Todos los puntos</h5>  
                </div>
                <div class="col-md-3">
                    <label class="switch">
                        <input type="checkbox" checked id="todos_ptos">
                        <span class="sliderN round"></span>
                    </label>
                </div>
                <div class="col-md-3" id="cont_ptos" style="display:none">
                    <div class="form-group">
                        <label for="id_punto">Punto</label>
                        <select id="id_punto" class="form-control">

                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <table class="table table-bordered" style="font-size:12px">
                    <thead>
                        <tr>
                            <th>Escala</th>
                            <th>1ra</th>
                            <th>2da</th>
                            <th>3ra</th>
                        </tr>
                    </thead>
                    <tbody >
                        <tr>
                            <td>0-99.99</td>
                            <td><input type="radio" name="escala1" id="escala1" value="1" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala2" id="escala2" value="1" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala3" id="escala3" value="1" onchange="calcularre()"></td>
                        </tr>
                        <tr>
                            <td>100-999.9</td>
                            <td><input type="radio" name="escala1" id="escala1" value="2" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala2" id="escala2" value="2" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala3" id="escala3" value="2" onchange="calcularre()"></td>
                        </tr>
                        <tr>
                            <td>1000-9999.9</td>
                            <td><input type="radio" name="escala1" id="escala1" value="3" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala2" id="escala2" value="3" onchange="calcularre()"></td>
                            <td><input type="radio" name="escala3" id="escala3" value="3" onchange="calcularre()"></td>
                        </tr>
                    </tbody>
                </table>
                    
                </div>
                <div class="col-8">
                    <table class="table table-bordered" style="font-size:12px">
                    <thead>
                        <tr>
                            <th>Resolución (lx)</th>
                            <th>Tipo de distribución</th>
                            <th>U estándar U(Xi) (lx)</th>
                            <th>Coef. Sensibilidad (Ci)</th>
                            <th>Contribución (U(y))</th>
                            <th>(U(y))2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="h1_resolicion"></td>
                            <td><input type="text" id="tipo_dis1" value="B, rectangular"></td>
                            <td class="h1_u_estandar"></td>
                            <td class="h1_coef_sensibilidad">
                                <input type="number" id="h1_coef_sensibilidad" value="1" style="max-width:90px" onchange="calcularre()">
                            </td>
                            <td class="h1_contribuccion_rq"></td>
                            <td class="h1_uy2_rq"></td>
                        </tr>
                        <tr>
                            <td class="h2_resolicion"></td>
                            <td><input type="text" id="tipo_dis2" value="B, rectangular"></td>
                            <td class="h2_u_estandar"></td>
                            <td class="h2_coef_sensibilidad">
                                <input type="number" id="h2_coef_sensibilidad" value="1" style="max-width:90px" onchange="calcularre()">
                            </td>
                            <td class="h2_contribuccion_rq"></td>
                            <td class="h2_uy2_rq"></td>
                        </tr>
                        <tr>
                            <td class="h3_resolicion"></td>
                            <td><input type="text" id="tipo_dis3" value="B, rectangular"></td>
                            <td class="h3_u_estandar"></td>
                            <td class="h3_coef_sensibilidad">
                                <input type="number" id="h3_coef_sensibilidad" value="1" style="max-width:90px" onchange="calcularre()">
                            </td>
                            <td class="h3_contribuccion_rq"></td>
                            <td class="h3_uy2_rq"></td>
                        </tr>
                    </tbody>
                </table>
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" id="save_re" class="btn btn-outline-primary btn-success">Guardar</button>
        </div>
    </div>
  </div>
</div>

<?php 
    function nameMes(){
        setlocale(LC_ALL, 'es_ES');
        $monthNum  = date("m");
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $monthName = strftime('%B', $dateObj->getTimestamp()); 
        return $monthName;
    }
?>