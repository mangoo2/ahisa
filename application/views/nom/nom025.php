<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">AH-LX-EV-</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        
                        
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="titulos">No. de Informe</td>
                                        <td><?php echo $numinfo;?></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Razón Social</td>
                                        <td colspan="3"><?php echo $razonsocial;?></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Fecha</td>
                                        <td><?php echo $fecha;?></td>
                                        <td class="titulos">Id. del equipo</td>
                                        <td><?php echo $idequipo;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <button type="button" class="btn btn-info mr-1 mb-1" onclick="addpunto()" >Agregar punto</button>
                            </div>
                        </div>
                        <div class="addpuntos">
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
