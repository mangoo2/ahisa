<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Perfiles</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Perfiles</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_perfil" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="ft-user-plus"></i> Agregar Perfil </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <!--<th>#</th>-->
                                    <th>Nombre</th>
                                    <th>Permisos</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="eliminarModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
            <div class="modal-content ">
              <div class="modal-header">
                <h4 class="modal-title">¡Atención!</h4>
              </div>
                   <!---->
                  <div class="col-md-12">
                           <div class="conteiner">
                            <p class="col">¿Desea eliminar el Perfil?</p>
                           </div>

                         <br>
                             <div class="modal-footer">
                                    <button type="button" id="btneliminar" class="btn btn-danger shadow-z-1 white" style="width: 35%; margin-left: 33%;">Eliminar <i class="ft-trash-2"></i></button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                             </div>         
                  
                     </div>   
       </div>
  </div>
</div>
<div id="visualModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
            <div class="modal-content ">
              <div class="modal-header">
                <h4 class="modal-title">Permisos del Perfil</h4>
              </div>
                   <!---->
                  <div class="col-md-12"> 
                 <form class="form" id="form_perfil" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre del Perfil<span class="required">*</span></h5>
                                    <div class="controls">
                                        <input disabled type="text" name="nombre" class="form-control form-control-sm toupper">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section"><i class="ft-unlock"></i> Seleccione los Permisos del Perfil</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Administrador</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled name="permiso_administrador" id="permiso_administrador" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                             </div>
                        </div>        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Catálogos</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_catalogos"name=" permiso_catalogos" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">        
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Configuraciones</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_configuraciones" name="permiso_configuraciones" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                         </div>   
                         <br>
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 style="font-weight: 600;" class="pull-right">Cotizaciones</h5>  
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-12">    
                                <div class="row">            
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Creación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_creacion" name="permiso_creacion" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Edición</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_edicion" name="permiso_edicion" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cambio estatus</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_estatus" name="permiso_estatus" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Envío a cliente</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_envio" name="permiso_envio" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 style="font-weight: 600;" class="pull-right">Ordenes de Trabajo</h5>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">           
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Datos generales</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_datos" name="permiso_datos" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>           
                         <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">                 
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cancelación de servicios</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_cancelacion" name="permiso_cancelacion" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                              </div>
                            </div>
                        </div>           
                         <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">  
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Programación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_programacion" name="permiso_programacion" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                              </div>
                            </div>
                        </div>           
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">              
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Cambio de estatus (Servicio)</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_estatus_servicio" name="permiso_estatus_servicio" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">              
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Enviar Programación</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_program_env" name="permiso_program_env" type="checkbox">
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>           
                         <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">              
                                    <div class="col-md-6">
                                        <h5 class="pull-right">PDF orden de trabajo</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_pdf" name="permiso_pdf" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>         
                        <br>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">         
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Agenda</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_agenda" name="permiso_agenda" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <div class="row">         
                                    <div class="col-md-6">
                                        <h5 class="pull-right">Listado Nom</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <label class="switch">
                                            <input disabled id="permiso_nom" name="permiso_nom" type="checkbox" >
                                            <span class="sliderN round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    
                         <br>
                             <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                             </div>         
                            </form>  
                        </div>     
       </div>
  </div>
</div>

<script>
   
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_perfil/'+data.id;
        });
        $('#tabla tbody').on('click', 'button.visua', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/vista_perfil/'+data.id;
        });

     
      $('#tabla').on('click', 'button.visual', function(){
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dato = row.data();

        $('#visualModal').modal('show'); 
        $('input[type*="checkbox"]').attr('checked',false);
          $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>index.php/catalogos/visual_perfil',
          data: {id:dato.id},
          async: false,
          dataType: 'json',
          success: function(data)
          {
            console.log("permiso_nom: "+data.permiso_nom);
          $('input[name=nombre]').val(data.nombre);  
      
          if(data.permiso_administrador == 'on')
           {
            $('#permiso_administrador').attr('checked','checked');
           }
          if(data.permiso_catalogos == 'on')
           { 
            $('#permiso_catalogos').attr('checked','checked');
           }
           if(data.permiso_configuraciones == 'on')
           {
            $('#permiso_configuraciones').attr('checked','checked');
           }
           if(data.permiso_creacion == 'on')
           {
            $('#permiso_creacion').attr('checked','checked');
           }
           if(data.permiso_edicion == 'on')
           {
            $('#permiso_edicion').attr('checked','checked');
           }
           if(data.permiso_estatus == 'on')
           {
            $('#permiso_estatus').attr('checked','checked');
           }
           if(data.permiso_envio == 'on')
           {
            $('#permiso_envio').attr('checked','checked');
           }
           if(data.permiso_datos == 'on')
           {
            $('#permiso_datos').attr('checked','checked');
           }
           if(data.permiso_cancelacion == 'on')
           {
            $('#permiso_cancelacion').attr('checked','checked');
           }
           if(data.permiso_programacion == 'on')
           {
            $('#permiso_programacion').attr('checked','checked');
           }
           if(data.permiso_estatus_servicio == 'on')
           {
            $('#permiso_estatus_servicio').attr('checked','checked');
           }
           if(data.permiso_program_env == 'on')
           {
            $('#permiso_program_env').attr('checked','checked');
           }
           if(data.permiso_pdf == 'on')
           {
            $('#permiso_pdf').attr('checked','checked');
           }
           if(data.permiso_agenda == 'on')
           {
            $('#permiso_agenda').attr('checked','checked');
           } 
           if(data.permiso_nom == 'on')
           {
            $('#permiso_nom').attr('checked','checked');
           } 
          },
          error: function(){
            alert('no se muestran los datos');
          }
      });
      });   

      

      $('#tabla').on('click', '.eliminar', function(){
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dato = row.data();
      $('#eliminarModal').modal('show');
          $('#btneliminar').unbind().click(function(){
             var datos = {id:dato.id}; 
               $.ajax({
                type: 'POST',
                async: false,
                url: '<?php echo base_url(); ?>index.php/catalogos/deletePerfil',
                data: datos,
                dataType: 'json',
                success: function(response){
                   if(response.success){
                            $('#eliminarModal').modal('hide');
                            table.ajax.reload();
                        }else{
                            alert('Error');
                        }
                  },
                error: function(jqXHR,estado,error){
                  console.log(jqXHR);
                  console.log(estado);
                  console.log(error);
                }
               });
               //fin ajax
          });
      });

    load();


    });

 function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getPerfiles"
            },
            "columns": [
                //{"data": "id"},
                {"data": "nombre"},
                {"data": null,  "defaultContent": "<button type='button' class='btn btn-sm btn-icon btn-success visual'><i class='icon-magnifier'></i></button>"},
                {"data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon btn-success edit'><i class='ft-edit'></i></button> <button type='button' class='btn btn-sm btn-icon btn-danger white eliminar'><i class='ft-trash-2'></i></button>"
                }
            ],language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });

    }

</script>