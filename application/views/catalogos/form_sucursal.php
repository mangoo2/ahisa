
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($sucursal)){
                    $title="Edición";
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Sucursal</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <form class="form" id="form-sucursal" method="post">
                        <h4 class="form-section"><i class="ft-plus-circle"></i> Datos del Generales</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Nombre <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="toupper form-control" <?php if(isset($sucursal)){ echo "value='$sucursal->nombre'";} ?> >
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p>Porcentaje de Variación (%) <span class="required">*</span></p>
                                    <div class="controls">
                                        <input type="number" name="variacion" class="form-control" <?php if(isset($sucursal)){ echo "value='$sucursal->variacion'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Télefono </p>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control" <?php if(isset($sucursal)){ echo "value='$sucursal->telefono'";} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Dirección </p>
                                    <div class="controls">
                                        <div class="controls">
                                            <textarea name="direccion" class="toupper form-control" rows="3"><?php if(isset($sucursal)){ echo $sucursal->direccion;} ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/sucursales" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {
        // VALIDACION
        
        $(".toupper").on("change",function(){
            $(this).val($(this).val().toUpperCase());
        });

        $("#form-sucursal")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($sucursal)){
                        echo "fd.append('id', $sucursal->id);";
                    }?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateToCatalogo/sucursales",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data==1)
                            {
                                var texto="Se insertó la Sucursal";
                                var url="<?php echo base_url(); ?>index.php/catalogos/sucursales";
                                
                                <?php if(isset($sucursal)){ // Si es Edicion
                                    echo "texto='Se actualizó la Sucursal'; ";
                                    echo "url='".base_url()."index.php/catalogos/edicion_sucursal/".$sucursal->id."';";
                                } ?>
  
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        },
                        varacion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Porcentaje"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    });
    
    

</script>
