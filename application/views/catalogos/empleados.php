<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Catálogo de Personal</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-file-text"></i> Listado de Empleados</h5><hr>
                        <a href="<?php echo base_url(); ?>index.php/catalogos/alta_empleado" class=" pull-right btn gradient-green-teal shadow-z-1 white"><i class="ft-user-plus"></i> Agregar Empleado </a><br><br>
                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <!--
                                    <th></th>
                                    <th>#</th>
                                    -->
                                    <th>Nombre</th>
                                    <th>Perfil</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/catalogos/getEmpleados"
            },
            "columns": [
                /*
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id"},
                */
                {"data": "nombre"},
                //{"data": "direccion"},
                //{"data": "telefono"},
                {"data": "perfil_name"},
                {"data": null,
                    render:function(data,type,row){
                        var html='';
                            html+=" <button type='button' class='btn btn-sm btn-icon btn-success edit'><i class='ft-edit'></i></button>";
                            <?php  if($id_usuario==1){ ?>
                            html+=" <button type='button' class='btn btn-sm btn-raised gradient-ibiza-sunset white' onclick='deleteemple("+row.id+")'><i class='fa fa-trash-o'></i></button>";
                            <?php  } ?>

                        return html;
                    }
                }
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });

    }
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para edicion
        $('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = '<?php echo base_url(); ?>index.php/catalogos/edicion_empleado/'+data.id;
        });

        load();


/*
        $('#tabla').on('click', 'button.visual', function(){
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var dato = row.data();

      $('#visualModal').modal('show'); 
          $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>index.php/catalogos/visual_empleado',
          data: {id:dato.id},
          async: false,
          dataType: 'json',
          success: function(data)
          {
            
          $('input[name=nombre]').val(data.nombre);  
          $('input[name=correo]').val(data.correo);  
          $('input[name=telefono]').val(data.telefono);  
          $('input[name=direccion]').val(data.direccion);  
          $('input[name=perfil]').val(data.perfil);  
          $('input[name=clave]').val(data.clave);  
          $('input[name=empresa]').val(data.empresa);  
          $('input[name=departamento]').val(data.nombre);  
          $('input[name=zona]').val(data.zona);  
          $('input[name=usuario]').val(data.usuario);  
          },
          error: function(){
            alert('no se muestran los datos');
          }
      });
      });
 */     
    });
    function deleteemple(id){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el empleado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url() ?>index.php/Catalogos/eliminaremple",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        console.log(response);
                        
                            swal("Éxito!", "Se ha Eliminado con exito", "success");
                            load();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
    }
</script>