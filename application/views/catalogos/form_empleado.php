
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <?php $title="Alta"; 
                if(isset($empleado)){
                    $title="Edición";
                }
            ?>
            <div class="content-header"><?php echo $title; ?> de Empleado</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <input type="hidden" id="id" value='<?php if(isset($empleado)) echo $empleado->id; else echo "0" ?>'>
                    <input type="hidden" id="firma" value='<?php if(isset($empleado)) echo $empleado->firma;?>'>
                    <form class="form" id="form-empleado" method="post">
                        <h4 class="form-section"><i class="ft-file-text"></i> Datos del Empleado</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>Nombre <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nombre" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->nombre'";} ?> >
                                    </div>
                                </div>
                                 <div class="form-group" style="display:none;">
                                    <h5>Clave <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="clave" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->clave'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Correo Electrónico <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="email" name="correo" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->correo'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;">
                                    <h5>Teléfono <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="telefono" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->telefono'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;">
                                    <h5>Dirección <span class="required">*</span></h5>
                                    <div class="controls">
                                        <textarea name="direccion" class="form-control form-control-sm toupper" rows="3"><?php if(isset($empleado)){ echo $empleado->direccion;} ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Perfil <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="perfil" id="perfil" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($perfiles as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->perfil==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="display:none;">
                                    <h5>Empresa <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="empresa" class="form-control form-control-sm" >
                                            <?php 											
											foreach ($proveedores as $p) {
												$sel="";
												if(isset($empleado) && $empleado->empresa==$p->id){
													$sel="selected";
												}
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group" style="display:none;">
                                    <h5>Sucursal <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="sucursal" class="form-control form-control-sm" >
                                            <?php                                           
                                            foreach ($sucursales as $p) {
                                                $sel="";
                                                if(isset($empleado) && $empleado->sucursal==$p->id){
                                                    $sel="selected";
                                                }
                                                echo "<option value='$p->id' $sel>$p->nombre</option>";
                                            } ?>
                                        </select>
                                    </div>
                                </div>
								<div class="form-group" style="display:none;">
                                    <h5>Departamento <span class="required">*</span></h5>
                                    <div class="controls">
                                        <select type="text" name="departamento" class="form-control form-control-sm" >
                                            <?php 											
											foreach ($departamentos as $d) {
												$sel="";
												if(isset($empleado) && $empleado->departamento==$d->nombre){
													$sel="selected";
												}
                                                echo "<option $sel>$d->nombre</option>";
                                            } ?>
											
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;">
                                    <h5>Zona <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="zona" class="form-control form-control-sm toupper" <?php if(isset($empleado)){ echo "value='$empleado->zona'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Usuario <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="usuario" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->usuario'";} ?>>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Password <span class="required">*</span></h5>
                                    <div class="controls">
                                        <input type="password" name="password" class="form-control form-control-sm" <?php if(isset($empleado)){ echo "value='$empleado->password'";} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" id="cont_otra" style="text-align:center">           
                                <div class="col-md-2">
                                    <h5 class="pull-right">Capturar otra</h5>  
                                </div>
                                <div class="col-md-3">
                                    <label class="switch">
                                        <input id="otra_firma" type="checkbox">
                                        <span class="sliderN round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6" id="cont_firma_actual" style="display: none;">
                                <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                    <?php 
                                    if(isset($empleado->firma) && $empleado->firma!=""){
                                        $firm_txt = FCPATH."uploads/firmas/empleado_".$empleado->id.".txt";
                                        $handle = @fopen($firm_txt, 'r');
                                        if($handle){
                                            $fh = fopen($firm_txt, 'r') or die("Se produjo un error al abrir el archivo");
                                            $linea = fgets($fh);
                                            fclose($fh);   
                                        }else{
                                            $linea="#";
                                        }
                                    }else{
                                        $linea='#';
                                    }
                                    ?>
                                    <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Firma capturada</label><br>
                                    <img src="<?php echo $linea;?>" width="355" height="160" style="border:dotted 1px black;">
                                </div>
                            </div>
                            
                            <div class="col-md-6" id="cont_firma" style="display: none;">
                                <div id="aceptance">
                                    <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                        <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                                        <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                                            
                                        </canvas>
                                        <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                            <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignature" 
                                              data-signature="patientSignature">Limpiar</a>
                                            <a class="btn btn-success waves-effect waves-green green btn-flat firma_empleado">Aceptar</a>
                                        </div>
                                    </div>  
                                          
                                </div>
                            </div>
                            <div class="col-md-6" id="cont_carga" style="display: none;">
                                <h5 class="pull-center">Cédula Profesional</h5> 
                                <input type="hidden" id="cedula_aux" <?php if(isset($empleado)){ echo "value='$empleado->cedula'";} ?>>
                                <input type="hidden" name="cedula" <?php if(isset($empleado)){ echo "value='$empleado->cedula'";} ?>>
                                <div class="col-md-10">
                                    <br>
                                   <span><input type="file" id="cedula" class="form-control" value="" > </span>
                                </div>
                            </div>
                        </div>
                                         
                        <br><hr>
                        <div class="row">
                            <a href="<?php echo base_url(); ?>index.php/catalogos/empleados" class="btn btn-icon btn-secondary"><i class="ft-chevron-left"></i> Regresar</a>
                            <button type="submit" class="btn gradient-green-teal shadow-z-1 white" style="width: 50%; margin-left: 15%;">Guardar <i class="fa fa-save"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/canvas.js?v=<?php echo date('YmdGis'); ?>"></script>

<script>
     $(document).ready(function () {
        $(".toupper").on("change",function(){
            $(this).val($(this).val().toUpperCase()); 
        });
        verificaContFirma();
        $("#perfil").on("change",function(){
            verificaContFirma();
        });
        $('.firma_empleado').click(function(event) {
            if($("#id").val()>0){
                firmaEmpleado($("#id").val(),1);
            }else{
                swal("Éxito!", "Firma registrada!", "success");
            }
        });
        $('#otra_firma').click(function(event) {
            if($("#otra_firma").is(":checked")==true){
                $("#cont_firma").show("slow");
                $("#cont_firma_actual").hide("slow");
            }else{
                $("#cont_firma").hide("slow");
                $("#cont_firma_actual").show("slow");
            }
        });

        if($("#perfil option:selected").val()=="5"){
            verificaCedula();
        }
        // VALIDACION
        $("#form-empleado")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    <?php if(isset($empleado)){
                        echo "fd.append('id', $empleado->id);";
                    }?>
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/catalogos/insertUpdateEmpleado",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data>0)
                            {
                                firmaEmpleado(data,0);
                                var texto="Se insertó el Empleado";
                                var url="<?php echo base_url(); ?>index.php/catalogos/empleados";
                                
                                <?php if(isset($empleado)){ // Si es Edicion
                                    echo "texto='Se actualizó el Empleado'; ";
                                    echo "url='".base_url()."index.php/catalogos/empleados';";
                                } ?>
  
                                swal({
                                    title: 'Exito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = url;
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        nombre: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Nombre"
                                }
                            }
                        },
                        clave: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese Clave"
                                }
                            }
                        },
                        telefono: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Teléfono"
                                }
                            }
                        },
                        direccion: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese una Dirección"
                                }
                            }
                        },
                        correo: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese un Correo"
                                }
                            }
                        },
                        usuario: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese el nombre de Usuario"
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la Contraseña"
                                }
                            }
                        }
                    }
                });
                // FIN VALIDADOR
    
        var tama_perm = false;
        $('#cedula').change( function() {
            filezise = this.files[0].size;                     
            if(filezise > 254800) { // 512000 bytes = 500 Kb
                //console.log($('.img')[0].files[0].size);
                $(this).val('');
                swal("Error!", "El archivo supera el límite de peso permitido", "error");
            }else { //ok
                tama_perm = true; 
                var archivo = this.value;
                var name = this.id;

                extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
                extensiones_permitidas = new Array(".jpeg",".png",".jpg",".webp");
                permitida = false;
                if($('#'+name)[0].files.length > 0) {
                    for (var i = 0; i < extensiones_permitidas.length; i++) {
                        if (extensiones_permitidas[i] == extension) {
                            permitida = true;
                        break;
                        }
                    }  
                    if(permitida==true && tama_perm==true){
                        //console.log("tamaño permitido");
                        var inputFileImage = document.getElementById(name);
                        name=document.getElementById('cedula').files[0].name;
                        //$("#name_cert").val(name);
                        var file = inputFileImage.files[0];
                        var data = new FormData();
                        //id_docs = $("#id").val();
                        data.append('foto',file);
                        data.append('id',$('#id').val());
                        $.ajax({
                          url:base_url+'Catalogos/uploadImg',
                          type:'POST',
                          contentType:false,
                          data:data,
                          processData:false,
                          cache:false,
                          success: function(data) {
                            var array = $.parseJSON(data);
                            //console.log(array.ok);
                            $("input[name*='cedula']").val(array.newfile);
                            if (array.ok=true) {
                                //$("input[name*='cedula']").val(array.newfile);
                                //console.log("id_docs: "+id_docs);
                                swal("Éxito", "Cédula cargada Correctamente", "success");
                            }else{
                                $("input[name*='cedula']").val(array.newfile);
                                //swal("Error!", "No Se encuentra el archivo", "error");
                            }
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              var data = JSON.parse(jqXHR.responseText);
                          }
                        });
                    }else if(permitida==false){
                        swal("Error!", "Tipo de archivo no permitido", "error");
                    } 
                }else{
                  swal("Error!", "No se a selecionado un archivo", "error");
                } 
            }
        });

    });

    function verificaCedula(){
        var imgprev1=""; var img1=""; imgp1="";
        var imgdet1=""; var typePDF1="";
        cedula = $("#cedula_aux").val();
        if(cedula!=""){
            imgprev1 = base_url+"uploads/cedulas/"+cedula;
            imgp1 = base_url+"uploads/cedulas/"+cedula;

            imgdet1 = {type:"image", url: base_url+"Catalogos/deleteCedula/"+$("#id").val(), caption: cedula, key:1};
            typePDF1 = "false"
        }

        /*$.ajax({
            type: "POST",
            url: base_url+"Catalogos/verificaCedula",
            data: { "id": $("#id").val()},
            async: false,
            success: function (result) {
                var data= JSON.parse(result);
                $.each(data, function (key, dato){
                    $("#name11").html(dato.documento);
                    $("#id_doc11").val(dato.id);
                    if(dato.validado=="1"){
                        $("#valid11").attr("checked",true);
                    }else{
                       $("#valid11").attr("checked",false); 
                    }
                    img1 = dato.documento;
                    imgprev1 = base_url+"uploads/expedientes/"+img1;
                    imgp1 = base_url+"uploads/expedientes/"+img1;
                    //imgdet1 = {type:"pdf", url: base_url+"Expedientes/file_delete/0", caption: img1, key:11};
                    //typePDF1 = "true"

                    var ext=img1.split(".").pop();
                    //if(ext=="pdf"){
                        //imgdet1 = {type:"pdf", url: base_url+"Expedientes/file_delete/"+dato.id, caption: img1, key:11};
                        //typePDF1 = "true"
                    //}else{
                        imgdet1 = {type:"image", url: base_url+"Expedientes/file_delete/"+dato.id, caption: img1, key:11};
                        typePDF1 = "false"
                    //}
                });
           }
        });*/
        $("#cedula").fileinput({
            overwriteInitial: true,
            showClose: true,
            showCaption: false,
            showUploadedThumbs: false,
            showBrowse: true,
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg","png","jpeg","bmp"],
            initialPreview: [
                ''+imgprev1+'',    
            ],
            initialPreviewAsData: typePDF1,
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewConfig: [
                imgdet1
            ],
        });
    }
    
    function verificaContFirma(){
        if($("#perfil").val()==1  && $("#firma").val()=="" || $("#perfil").val()==5 && $("#firma").val()=="" || $("#perfil").val()==6 && $("#firma").val()==""){
            $("#cont_firma").show("slow");
            $("#cont_otra").show("slow");
            if($("#perfil").val()==5)
                $("#cont_carga").show("slow");
        }else if($("#perfil").val()==1  && $("#firma").val()!="" || $("#perfil").val()==5 && $("#firma").val()!="" || $("#perfil").val()==6 && $("#firma").val()!=""){
            $("#cont_firma_actual").show("slow");
            $("#cont_firma").hide("slow");
            $("#cont_otra").show("slow");
            if($("#perfil").val()==5)
                $("#cont_carga").show("slow");
        }
        else if($("#perfil").val()!=1 && $("#perfil").val()!=5 && $("#perfil").val()!=6){
            $("#cont_firma").hide("slow");
            $("#cont_otra").hide("slow");
            $("#cont_firma_actual").hide("slow");
            if($("#perfil").val()!=5)
                $("#cont_carga").hide("slow");
        }
    }

    function firmaEmpleado(id,band){ //con band=1 sale alerta de guardado
        var canvas = document.getElementById('patientSignature');
        var dataURL = canvas.toDataURL();
        $.ajax({
            type:'POST',
            url: base_url+'Catalogos/guardarFirma',
            async:false,
            data: {
                firma:dataURL, id:id
            },
            success:function(data){
                if(band==1){
                    swal("Éxito!", "Firma registrada!", "success");
                }
            }
        });
    }


</script>
