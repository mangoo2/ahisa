
<div class="main-content">
    <div class="content-wrapper">
        <div class="col-sm-12">
            <div class="content-header">Documentación de Acreditación</div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    
                    <h4 class="form-section"><i class="ft-file-text"></i> Listado de Documentos</h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="norma">Norma</label>
                                <select id="norma" class="form-control">
                                    <option value="1">Nom-011</option>
                                    <option value="2">Nom-025</option>
                                    <option value="3">Nom-022</option>
                                    <option value="4">Nom-081</option>
                                    <option value="5">Nom-015</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-6" id="cont_carga">
                            <div class="col-md-10">
                                <br>
                               <span><input type="file" name="inputFile" id="inputFile" class="form-control" value="" > </span>
                               <!--<br><a type="button" id="acept_cert" class="btn btn-info mr-1 mb-1" ><i class="fa fa-upload" aria-hidden="true"></i> Cargar Certificado</a>-->
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div id="cont_cert"></div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
