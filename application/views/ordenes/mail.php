<?php
$color = "#CCFF99";
function mes(){
    $mes="enero";
    switch (date('m')) {
        case 2: $mes="febrero"; break;
        case 3: $mes="marzo"; break;
        case 4: $mes="abril"; break;
        case 5: $mes="mayo"; break;
        case 6: $mes="junio"; break;
        case 7: $mes="julio"; break;
        case 8: $mes="agosto"; break;
        case 9: $mes="septiembre"; break;
        case 10: $mes="octubre"; break;
        case 11: $mes="noviembre"; break;
        case 12: $mes="diciembre"; break;
    }
    return $mes;
}
$razon="";
$logo =  base_url()."app-assets/img/ahisa_blanco.png";
$razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V.";  
?>
<table style="width: 100%">
    <tr>
        <td width="30%" align="center"><img height="60px" src="<?php echo $logo; ?>"><br></td>
        <td width="70%" align="right" style="font-size: 12px"><br><br>
            <strong><?php echo $razon; ?></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="font-size: 14px; background-color: #058e1c; color: white"><br><strong>PROGRAMACIÓN DE SERVICIO</strong></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><strong><?php echo date('d')." de ".mes()." de ".date("Y"); ?>
                
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Se ha programado <?php foreach($servicios as $s) { echo $s->nombre; } ?> por parte de <?php echo $razon; ?> para las siguientes fechas:<br><br>
        </td>
    </tr>
    <tr>
        <td>Fecha de Inicio: </td>
        <td><strong><?php echo $fecha_inicio." ".$hora_inicio;?></strong> </td>
    </tr>
    <tr>
        <td>Fecha de Fin: </td>
        <td><strong><?php echo $fecha_fin." ".$hora_fin;?></strong></td>
    </tr>
    <tr>
        <td>Comentarios: </td>
        <td><strong><?php echo $comentarios;?></strong></td>
    </tr>
    <tr>
        <td>Documentos: </td>
        <td><strong>
            <?php 
            foreach ($documentos as $d){
                echo $d->nombre."<br>";
            } ?>
            </strong>
        </td>
    </tr>
</table>

