
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_orden" method="post">
                            <input type="hidden" name="id" id="id" value="<?php if(isset($dets)) echo $dets->id; else echo 0; ?>">
                            <input type="hidden" name="id_orden" id="id_orden" value="<?php echo $id_orden; ?>">
                            <input type="hidden" id="id_cot" value="<?php echo $id_cot; ?>">
                            <div class="row mt-4">
                                <div class="row col-md-12">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>Fecha de solicitud </p>
                                            <input class="form-control" type="date" name="fecha_solicita" value="<?php if(isset($dets)) echo $dets->fecha_solicita; ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>Fecha de servicio </p>
                                            <input class="form-control" type="date" name="fecha_servicio" value="<?php if(isset($dets)) echo $dets->fecha_servicio; ?>">
                                        </div>
                                    </div>
                              
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>Ref. No. de cotización</p>
                                            <input readonly class="form-control" type="text" id="folio" value="<?php echo $orden->folio; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>No. de orden</p>
                                            <input readonly class="form-control" type="text" name="id_orden" value="<?php echo $id_orden; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Ingeniero(s) de campo asignado(s)</p>
                                            <input class="form-control" type="text" name="ing_asignado" value="<?php if(isset($dets)) echo $dets->ing_asignado; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Nombre o razón social:</p>
                                            <input class="form-control" type="text" name="nom_razon" value="<?php if(isset($dets)) echo $dets->nom_razon; else echo $orden->cliente; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>RFC</p>
                                            <input class="form-control" type="text" name="rfc" value="<?php if(isset($dets)) echo $dets->rfc; else echo $orden->rfc; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Dirección:</p>
                                            <input class="form-control" type="text" name="direc" value="<?php if(isset($dets)) echo $dets->direc; else echo $orden->direcc; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Giro de la empresa:</p>
                                            <input class="form-control" type="text" name="giro" value="<?php if(isset($dets)) echo $dets->giro; else echo $orden->giro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Nombre del contacto:</p>
                                            <input class="form-control" type="text" name="contacto" value="<?php if(isset($dets)) echo $dets->contacto; else echo $orden->contacto; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>Cargo:</p>
                                            <input class="form-control" type="text" name="cargo" value="<?php if(isset($dets)) echo $dets->cargo; else echo $orden->puesto; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p>Teléfono:</p>
                                            <input class="form-control" type="text" name="tel" value="<?php if(isset($dets)) echo $dets->tel; else echo $orden->telefono; ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>
                                                        
                            <hr>
           
                            <table class="table table-sm" id="tabla_servs" width="100%">
                                <thead>
                                    <tr><th colspan="3">ALCANCE</th></tr>
                                    <tr>
                                        <th width="50%">Servicio</th>
                                        <th width="10%">Cantidad</th>
                                        <th width="40%">Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody id="table_servicios">
                                    <?php
                                    foreach($serv as $s){
                                        echo "<tr>
                                            <td width='50%'><input type='hidden' id='id_chs' value='".$s->id."'>".$s->clave2." / ".$s->nombre2."</td>
                                            <td width='10%'>".$s->cantidad."</td>
                                            <td width='40%'><textarea rows='4' id='coments' type='text' value='".$s->comentarios."'>".$s->comentarios."</textarea></td>
                                        </tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-sm" id="tabla_eval">
                                <thead>
                                    <tr><th colspan="6">No. DE INFORMES DE SERVICIOS</th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>RL</td>
                                        <th>
                                            <input type="text" name="info_rl" class="form-control" value="<?php if(isset($dets)) echo $dets->info_rl; ?>">
                                        </th>
                                        <td>TE</td>
                                        <th>
                                            <input type="text" name="info_te" class="form-control" value="<?php if(isset($dets)) echo $dets->info_te; ?>">
                                        </th>
                                        <td>TA</td>
                                        <th>
                                            <input type="text" name="info_ta" class="form-control" value="<?php if(isset($dets)) echo $dets->info_ta; ?>">
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>TF</td>
                                        <th>
                                            <input type="text" name="info_tf" class="form-control" value="<?php if(isset($dets)) echo $dets->info_tf; ?>">
                                        </th>
                                        <td>IL</td>
                                        <th>
                                            <input type="text" name="info_il" class="form-control" value="<?php if(isset($dets)) echo $dets->info_il; ?>">
                                        </th>
                                        <td>RP</td>
                                        <th>
                                            <input type="text" name="info_rp" class="form-control" value="<?php if(isset($dets)) echo $dets->info_rp; ?>">
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <table class="table table-sm" id="tabla_decla">
                                <thead>
                                    <tr><th colspan="6">DOCUMENTACIÓN DE ACCESO</th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Alta en el IMSS</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_imss==1) echo "checked"; ?> id="doc_imss" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <td>Copia de pago de SUA</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_sua==1) echo "checked"; ?> id="doc_sua" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <td>Certificado médico</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_certif==1) echo "checked"; ?> id="doc_certif" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Análisis clínicos</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_analisis==1) echo "checked"; ?> id="doc_analisis" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <td>Listado de equipo</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_listado==1) echo "checked"; ?> id="doc_listado" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                        <td>Permiso vehicular</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->doc_permiso==1) echo "checked"; ?> id="doc_permiso" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Otro</td>
                                        <th colspan="5">
                                            <input type="text" name="doc_otro" class="form-control" value="<?php if(isset($dets)) echo $dets->doc_otro; ?>">
                                        </th>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-sm" id="tabla_pays">
                                <thead>
                                    <tr><th colspan="6">EQUIPO DE PROTECCIÓN PERSONAL</th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Botas de seguridad</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->botas==1) echo "checked"; ?> id="botas" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Casco de seguridad</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->casco==1) echo "checked"; ?> id="casco" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Chaleco de seguridad</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->chaleco==1) echo "checked"; ?> id="chaleco" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lentes de seguridad</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->lentes==1) echo "checked"; ?> id="lentes" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Tapones auditivos</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->tapones==1) echo "checked"; ?> id="tapones" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Guantes de protección</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->guantes==1) echo "checked"; ?> id="guantes" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Otro</td>
                                        <th colspan="5">
                                            <input type="text" name="otro_equipo" class="form-control" value="<?php if(isset($dets)) echo $dets->otro_equipo; ?>">
                                        </th>
                                    </tr>
                                </tbody>
                            </table>

                            <br><hr>
                            <div class="row">
                                <button type="button" onclick="save_detalles()" class="btn btn-success shadow-z-1" style="width: 40%; margin-left: 30%;" id="save">Aceptar e imprimir <i class="ft-save"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>