<?php
    $id_usuario = $this->session->userdata("id_usuario");
    $empresa = $this->session->userdata("empresa");
    $perfil = $this->session->userdata("perfil");
    $perm_pe = $this->session->userdata("permiso_program_env");
    $departamento = $this->session->userdata("permiso_program_env");
?>
<input type="hidden" id="idus" value="<?php echo $id_usuario; ?>">
<input type="hidden" id="perm_pe" value="<?php echo $perm_pe; ?>">
<input type="hidden" id="perfil" value="<?php echo $perfil; ?>">

<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Ordenes de trabajo</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-mail"></i> Listado de ordenes</h5><hr>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="modal-title text-text-bold-600">Busqueda por estatus</label>
                                <div class="position-relative has-icon-right">
                                    <select class="form-control" id="tipostock" name="tipostock">
                                        <option value="0">Todas:</option>
                                        <option value="1">Pendiente</option>
                                        <option value="4">En Proceso</option>
                                        <option value="2">Realizado</option>
                                        <option value="3">Cancelado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="position-relative has-icon-right">
                                    <label for="Fecha1">Desde</label>
                                    <input id="Fecha1" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="position-relative has-icon-right">
                                    <label for="Fecha2">Hasta</label>
                                    <input id="Fecha2" type="date" class="form-control">
                                </div>
                            </div>
                            <!--<div class="col-md-3 form-group">
                                <label class="modal-title text-text-bold-600">Empresa</label>
                                <div class="position-relative has-icon-right">
                                    <select type="text" id="empresa" class="form-control">
                                        <?php                                        
                                        foreach ($proveedores as $p) {
                                            echo "<option value='$p->id'>$p->nombre</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>-->
                            <div class="col-md-2 form-group">
                                <label style="color: transparent;">exportar<br></label>
                                <button class="btn btn-success" type="button" id="export" >Exportar Excel</button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped" id="tabla">
                                <thead>
                                    <tr>
                                        <th># Orden</th>
                                        <th>Vendedor</th>
                                        <th>Cotización</th>
                                        <!--<th>Servicio</th>-->
                                        <th>Fecha creación de la Orden</th>
                                        <th>Cliente</th>
                                        <!--<th>Empresa</th>-->
                                        <th>Estatus</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL Programacion ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_program" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Datos para fecha de programación</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Inicio:</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_inicio" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_inicio" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                </div>
                <p>Fin:</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar-o"></span>
                                </span>
                                <input type='text' id="fecha_fin" class="form-control form-control-sm pickadate" placeholder="Selecciona la fecha" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                                <input type='text' id="hora_fin" class="form-control form-control-sm timepicker" placeholder="Selecciona la hora" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6 form-group">
                        <span class="text-bold-500">Unico Técnico</span>
                        <div class="">
                            <div class="">
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                    <input id="unico_tecnico" checked="checked" type="checkbox" class="custom-control-input"><span class="custom-control-indicator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-group" id="cont_tec_unico">
                        <label for="estatus" id="lable_tec">Técnico a asignar:</label><br>
                        <select id="tecnico" class="form-control id_tecnico_unico">
                            <option selected value="">Elige un técnico</option>
                            <?php foreach ($usu as $u) {
                                    echo "<option value='".$u->id."'>".$u->nombre."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="row" id="cont_servs_padre" style="display: none;">
                    <div class="row" id="cont_servs" style="margin-left:2px; margin-right: 2px;">
                    </div>

                </div>
                <!--<div class="row">
                    <div class="col-md-6 form-group">
                        <label for="estatus" id="lable_tec">Técnico a asignar:</label><br>
                        <select id="tecnico" class="form-control">
                            <option selected value="">Elige un técnico</option>
                            <?php foreach ($usu as $u) {
                                    echo "<option value='".$u->id."'>".$u->nombre."</option>";
                            } ?>
                        </select>
                    </div>
                </div>-->
                <p>Comentarios:</p>
                <textarea class="form-control" id="comentarios" rows="3"></textarea>
                <input id="servicio" type="hidden" value="0">
                <input id="cliente_orden" type="hidden" value="0">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <?php if($this->session->userdata("id_usuario")=="1" || $this->session->userdata("perfil")=="3"){ ?>
                    <button type="button" onclick="programacion()" class="btn btn-success">Guardar</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<div class="modal fade text-left" id="modal_empresa" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Empresa</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="cont_emp"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="modal_serv_tec" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Seleccione el servicio a cumplir</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="cont_serv_ir">
      
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="aceptar()" class="btn btn-success">Aceptar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
