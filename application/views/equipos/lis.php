
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Equipos</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12" style="text-align: end;">
                                <a type="button" class="btn btn-info mr-1 mb-1" href="<?php echo base_url();?>Equipos/add" >Agregar</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="table_equipo">
                                    <thead>
                                        <tr>
                                            <!--<th>#</th>-->
                                            <th>Id. del equipo</th>
                                            <th>No. de Serie</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
