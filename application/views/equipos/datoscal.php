<style type="text/css">
    th,td{
        font-size: 12px;
    }
    td{
        padding: 5px !important;
    }
    .escala_ini,
    .escala_fin,
    .iluminancia_ref,
    .iluminancia_pro,
    .insertidumbre,.resolucion{
        width: 68px;
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Equipo</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-6" id="cont_carga" style="display: none;">
                                <div class="col-md-10">
                                    <br>
                                   <span><input type="file" name="inputFile" id="inputFile" class="form-control" value="" > </span>
                                   <a style="display: none;" type="button" id="acept_cert" class="btn btn-info mr-1 mb-1" >Cargar Certificado</a>
                                </div>
                            </div>
                            <!--<div class="container">
                                <div class="row">
                                    <div class="col-6">
                                        <div id="cont_cert"></div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-md-12">
                                <div id="cont_cert"></div>
                            </div>
                            <div class="col-md-12">
                                <form id="form_equipo">
                                    <input type="hidden" name="id" id="id" value="<?php echo $idequipo;?>">
                                    <input type="hidden" name="certificado" id="name_cert" value="0">
                                    <div class="col-md-12">
    
                                        <!--<div class="col-md-3 form-group">
                                            <label for="luxometro">Luxometro</label>
                                            <input type="text" name="luxometro" id="luxometro" class="form-control" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Equipo</label>
                                            <input type="text" name="equipo" id="equipo" class="form-control" required>
                                        </div>-->
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="luxometro">Id. del equipo</label>
                                                    <div class="controls">
                                                        <input type="text" name="luxometro" id="luxometro" value="<?php if(isset($e)) echo "$e->luxometro"; ?>" class="form-control form-control-sm">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>No. de serie</label>
                                                    <div class="controls">
                                                        <input type="text" name="equipo" id="equipo" value="<?php if(isset($e)) echo "$e->equipo"; ?>" class="form-control form-control-sm toupper">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="marca">Marca</label>
                                                    <div class="controls">
                                                        <input type="text" name="marca" id="marca" value="<?php if(isset($e)) echo "$e->marca"; ?>" class="form-control form-control-sm">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Modelo</label>
                                                    <div class="controls">
                                                        <input type="text" name="modelo" id="modelo" value="<?php if(isset($e)) echo "$e->modelo"; ?>" class="form-control form-control-sm toupper">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="no_informe_calibracion">No. informe calibración</label>
                                                    <div class="controls">
                                                        <input type="text" name="no_informe_calibracion" value="<?php if(isset($e)) echo "$e->no_informe_calibracion"; ?>" id="no_informe_calibracion" class="form-control form-control-sm">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Fecha de calibración</label>
                                                    <div class="controls">
                                                        <input type="date" name="fecha_calibracion" value="<?php if(isset($e)) echo "$e->fecha_calibracion"; ?>" id="fecha_calibracion" class="form-control form-control-sm toupper">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Tipo de Equipo</label>
                                                    <select name="tipo" id="tipo" class="form-control">
                                                        <option value="0" <?php if(isset($e) && $e->tipo==0){ echo "selected"; } ?>>N/A</option>
                                                        <option value="1" <?php if(isset($e) && $e->tipo==1){ echo "selected"; } ?>>Luxometro</option>
                                                        <option value="2" <?php if(isset($e) && $e->tipo==2){ echo "selected"; } ?>>Sonómetro</option>
                                                        <option value="3" <?php if(isset($e) && $e->tipo==3){ echo "selected"; } ?>>DosÍmetro</option>
                                                        <option value="4" <?php if(isset($e) && $e->tipo==4){ echo "selected"; } ?>>Calibrador Acústico</option>
                                                        <option value="5" <?php if(isset($e) && $e->tipo==5){ echo "selected"; } ?>>Medidor de resistencia de tierras</option>
                                                        <option value="6" <?php if(isset($e) && $e->tipo==6){ echo "selected"; } ?>>Multímetro</option>
                                                        <option value="7" <?php if(isset($e) && $e->tipo==7){ echo "selected"; } ?>>Set de resistencias</option>
                                                        <option value="8" <?php if(isset($e) && $e->tipo==8){ echo "selected"; } ?>>Termómetro liquido </option>
                                                        <option value="9" <?php if(isset($e) && $e->tipo==9){ echo "selected"; } ?>>Termómetro digital</option>
                                                        <option value="10" <?php if(isset($e) && $e->tipo==10){ echo "selected"; } ?>>Anemómetro</option>
                                                        <option value="11" <?php if(isset($e) && $e->tipo==11){ echo "selected"; } ?>>Bulbo Seco</option>
                                                        <option value="12" <?php if(isset($e) && $e->tipo==12){ echo "selected"; } ?>>Bulbo Húmedo</option>
                                                        <option value="13" <?php if(isset($e) && $e->tipo==13){ echo "selected"; } ?>>Globo</option>
                                                        <option value="14" <?php if(isset($e) && $e->tipo==14){ echo "selected"; } ?>>Higrotermoanemometro</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>         
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div> 
                        <div class="row" id="cont_luxo" style="display:none;">
                            <div class="col-md-12" style="text-align: end;">
                                <a type="button" class="btn btn-info mr-1 mb-1" onclick="adddetalle(0)" >Agregar detalle</a>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered" id="equipos_detalle">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th colspan="2">Escala</th>
                                            <th>Iluminancia de referencia (lx)</th>
                                            <th>Iluminancia promedio obtenida (lx)</th>
                                            <th>Error (lx)</th>
                                            <th>Error Relativo(%)</th>
                                            <th>Incertidumbre</th>
                                            <th>Factor de corrección (Fc)</th>
                                            <th>Resolución</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="equipos_detalle_class">
                                        
                                    </tbody>
                                </table>
                            </div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 cal_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 cal_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row" id="cont_sono" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de filtro en bandas de octava</h3>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_informe_calibracion">No. informe calibración</label>
                                    <div class="controls">
                                        <input type="text" name="num_calibra" id="num_calibra" class="form-control form-control-sm">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Fecha de calibración</label>
                                    <div class="controls">
                                        <input type="date" name="fecha_calibra" id="fecha_calibra" class="form-control form-control-sm toupper">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="cont_termo" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Termómetro</h3>
                            </div>
                            <table class="table table-bordered" id="termo_detalle">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Temp. Patrón °C</th>
                                        <th>Tem. IBC °C</th>
                                        <th>Error °C</th>
                                        <th>U °C</th>
                                        <th>FC</th>
                                    </tr>
                                </thead>
                                <tbody class="termo_detalle_class">
                                    
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 termo_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 termo_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>
                        
                        <div class="row" id="cont_anemo" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Anemómetro</h3>
                            </div>
                            <table class="table table-bordered" id="anemo_detalle">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Vel. Patrón m/s</th>
                                        <th>Vel. IBC m/s</th>
                                        <th>Error m/s</th>
                                        <th>U m/s</th>
                                        <th>FC</th>
                                    </tr>
                                </thead>
                                <tbody class="anemo_detalle_class">
                                    
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 anemo_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 anemo_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>

                        <div class="row" id="cont_bulbo_seco" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Bulbo Seco</h3>
                            </div>
                            <table class="table table-bordered" id="bulbo_seco_detalle">
                                <thead>
                                    <tr>
                                        <th>Punto</th>
                                        <th>Patrón</th>
                                        <th>LIBC</th>
                                        <th>Error</th>
                                        <th>Incertidumbre</th>
                                    </tr>
                                </thead>
                                <tbody class="bulbos_detalle_class">
                                    
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 bulbos_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 bulbos_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>

                        <div class="row" id="cont_bulbo_humedo" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Bulbo Húmedo</h3>
                            </div>
                            <table class="table table-bordered" id="bulbo_humedo_detalle">
                                <thead>
                                    <tr>
                                        <th>Punto</th>
                                        <th>Patrón</th>
                                        <th>LIBC</th>
                                        <th>Error</th>
                                        <th>Incertidumbre</th>
                                    </tr>
                                </thead>
                                <tbody class="bulboh_detalle_class">
                                    
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 bulboh_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 bulboh_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>

                        <div class="row" id="cont_globo" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Globo</h3>
                            </div>
                            <table class="table table-bordered" id="globo_detalle">
                                <thead>
                                    <tr>
                                        <th>Punto</th>
                                        <th>Patrón</th>
                                        <th>LIBC</th>
                                        <th>Error</th>
                                        <th>Incertidumbre</th>
                                    </tr>
                                </thead>
                                <tbody class="globo_detalle_class">
                                    
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Intersección (b) =</div>
                            <div class="col-md-3 globo_interseccion">0.0</div>
                            <div class="col-md-3"></div>
   
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">Pendiente (m) =</div>
                            <div class="col-md-3 globo_pendiente">0.0</div>
                            <div class="col-md-3"></div>
                        </div>
                        <!-- para equipo med de tierras -->
                        <div class="row" id="cont_med_tierra" style="display:none;">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="medidor_detalle">
                                    <thead style="text-align:center;">
                                        <tr>
                                            <th>VALOR MÁXIMO DEL <br>INTERVALO DE MEDIDA</th>
                                            <th>VALOR DE <br>REFERENCIA</th>
                                            <th>VALOR <br>BAJO PRUEBA </th>
                                            <th>ERROR <br>ABSOLUTO </th>
                                            <th>INCERTIDUMBRE <br>EXPANDIDA</th>
                                            <th rowspan="2">FC</th>
                                            <th rowspan="2"></th>
                                        </tr>
                                        <tr>
                                            <th>UNIDAD (Ω)</th>
                                            <th>Ω</th>
                                            <th>Ω</th>
                                            <th>Ω</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody class="medidor_detalle_class">
                                        <tr>
                                            <th style="text-align:center; vertical-align: middle;" rowspan="5">2 Ω</th>
                                            <td><input class="id1_2" type="hidden" id="id" value="0"><input type="hidden" id="unidad" value="2">
                                                <input type="hidden" id="line" value="1">
                                                <input type="text" data-line="1" data-om="2" id="ref2_1" class="referencia val_ch form-control form-control-sm">
                                            </td>
                                            <td><input type="text" data-line="1" data-om="2" id="prueba2_1" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" readonly id="absoluto2_1" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2_1" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" readonly id="fc2_1" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="2">Intersección (b) = <input readonly type="text" id="inter2" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id2_2" id="id" value="0"><input type="hidden" id="unidad" value="2">
                                                <input type="hidden" id="line" value="2">
                                                <input type="text" data-line="2" data-om="2" id="ref2_2" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="2" data-om="2" id="prueba2_2" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2_2" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2_2" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2_2" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id3_2" id="id" value="0"><input type="hidden" id="unidad" value="2">
                                                <input type="hidden" id="line" value="3">
                                                <input type="text" data-line="3" data-om="2" id="ref2_3" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="3" data-om="2" id="prueba2_3" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2_3" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2_3" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2_3" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="3">Pendiente (m) = <input readonly type="text" id="pend2" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id4_2" id="id" value="0"><input type="hidden" id="unidad" value="2">
                                                <input type="hidden" id="line" value="4">
                                                <input type="text" data-line="4" data-om="2" id="ref2_4" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="4" data-om="2" id="prueba2_4" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2_4" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2_4" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2_4" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id5_2" id="id" value="0"><input type="hidden" id="unidad" value="2">
                                                <input type="hidden" id="line" value="5">
                                                <input type="text" data-line="5" data-om="2" id="ref2_5" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="5" data-om="2" id="prueba2_5" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2_5" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2_5" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2_5" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <th style="text-align:center; vertical-align: middle;" rowspan="5">20 Ω</th>
                                            <td><input type="hidden" class="id1_20" id="id" value="0"><input type="hidden" id="unidad" value="20">
                                                <input type="hidden" id="line" value="1">
                                                <input type="text" data-line="1" data-om="20" id="ref20_1" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="1" data-om="20" id="prueba20_1" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20_1" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20_1" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20_1" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="2">Intersección (b) = <input readonly type="text" id="inter20" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id2_20" id="id" value="0"><input type="hidden" id="unidad" value="20">
                                                <input type="hidden" id="line" value="2">
                                                <input type="text" data-line="2" data-om="20" id="ref20_2" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="2" data-om="20" id="prueba20_2" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20_2" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20_2" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20_2" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id3_20" id="id" value="0"><input type="hidden" id="unidad" value="20">
                                                <input type="hidden" id="line" value="3">
                                                <input type="text" data-line="3" data-om="20" id="ref20_3" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="3" data-om="20" id="prueba20_3" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20_3" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20_3" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20_3" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="3">Pendiente (m) = <input readonly type="text" id="pend20" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id4_20" id="id" value="0"><input type="hidden" id="unidad" value="20">
                                                <input type="hidden" id="line" value="4">
                                                <input type="text" data-line="4" data-om="20" id="ref20_4" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="4" data-om="20" id="prueba20_4" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20_4" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20_4" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20_4" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id5_20" id="id" value="0"><input type="hidden" id="unidad" value="20">
                                                <input type="hidden" id="line" value="5">
                                                <input type="text" data-line="5" data-om="20" id="ref20_5" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="5" data-om="20" id="prueba20_5" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20_5" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20_5" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20_5" class="valfc form-control form-control-sm"></td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center; vertical-align: middle;" rowspan="5">200 Ω</th>
                                            <td><input type="hidden" class="id1_200" id="id" value="0"><input type="hidden" id="unidad" value="200">
                                                <input type="hidden" id="line" value="1">
                                                <input type="text" data-line="1" data-om="200" id="ref200_1" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="1" data-om="200" id="prueba200_1" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto200_1" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert200_1" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc200_1" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="2"> Intersección (b) = <input readonly type="text" id="inter200" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id2_200" id="id" value="0"><input type="hidden" id="unidad" value="200">
                                                <input type="hidden" id="line" value="2">
                                                <input type="text" data-line="2" data-om="200" id="ref200_2" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="2" data-om="200" id="prueba200_2" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto200_2" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert200_2" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc200_2" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id3_200" id="id" value="0"><input type="hidden" id="unidad" value="200">
                                                <input type="hidden" id="line" value="3">
                                                <input type="text" data-line="3" data-om="200" id="ref200_3" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="3" data-om="200" id="prueba200_3" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto200_3" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert200_3" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc200_3" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="3">Pendiente (m) = <input readonly type="text" id="pend200" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id4_200" id="id" value="0"><input type="hidden" id="unidad" value="200">
                                                <input type="hidden" id="line" value="4">
                                                <input type="text" data-line="4" data-om="200" id="ref200_4" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="4" data-om="200" id="prueba200_4" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto200_4" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert200_4" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc200_4" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id5_200" id="id" value="0"><input type="hidden" id="unidad" value="200">
                                                <input type="hidden" id="line" value="5">
                                                <input type="text" data-line="5" data-om="200" id="ref200_5" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="5" data-om="200" id="prueba200_5" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto200_5" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert200_5" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc200_5" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <th style="text-align:center; vertical-align: middle;" rowspan="5">2000 Ω</th>
                                            <td><input type="hidden" class="id1_2000" id="id" value="0"><input type="hidden" id="unidad" value="2000">
                                                <input type="hidden" id="line" value="1">
                                                <input type="text" data-line="1" data-om="2000" id="ref2000_1" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="1" data-om="2000" id="prueba2000_1" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2000_1" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2000_1" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2000_1" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="2">Intersección (b) = <input readonly type="text" id="inter2000" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id2_2000" id="id" value="0"><input type="hidden" id="unidad" value="2000">
                                                <input type="hidden" id="line" value="2">
                                                <input type="text" data-line="2" data-om="2000" id="ref2000_2" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="2" data-om="2000" id="prueba2000_2" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2000_2" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2000_2" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2000_2" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id3_2000" id="id" value="0"><input type="hidden" id="unidad" value="2000">
                                                <input type="hidden" id="line" value="3">
                                                <input type="text" data-line="3" data-om="2000" id="ref2000_3" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="3" data-om="2000" id="prueba2000_3" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2000_3" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2000_3" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2000_3" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="3">Pendiente (m) = <input readonly type="text" id="pend2000" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id4_2000" id="id" value="0"><input type="hidden" id="unidad" value="2000">
                                                <input type="hidden" id="line" value="4">
                                                <input type="text" data-line="4" data-om="2000" id="ref2000_4" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="4" data-om="2000" id="prueba2000_4" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2000_4" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2000_4" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2000_4" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id5_2000" id="id" value="0"><input type="hidden" id="unidad" value="2000">
                                                <input type="hidden" id="line" value="5">
                                                <input type="text" data-line="5" data-om="2000" id="ref2000_5" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="5" data-om="2000" id="prueba2000_5" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto2000_5" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert2000_5" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc2000_5" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                            <th style="text-align:center; vertical-align: middle;" rowspan="5">20000 Ω</th>
                                            <td><input type="hidden" class="id1_20000" id="id" value="0"><input type="hidden" id="unidad" value="20000">
                                                <input type="hidden" id="line" value="1">
                                                <input type="text" data-line="1" data-om="20000" id="ref20000_1" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="1" data-om="20000" id="prueba20000_1" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20000_1" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20000_1" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20000_1" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="2">Intersección (b) = <input readonly type="text" id="inter20000" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id2_20000" id="id" value="0"><input type="hidden" id="unidad" value="20000">
                                                <input type="hidden" id="line" value="2">
                                                <input type="text" data-line="2" data-om="20000" id="ref20000_2" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="2" data-om="20000" id="prueba20000_2" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20000_2" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20000_2" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20000_2" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id3_20000" id="id" value="0"><input type="hidden" id="unidad" value="20000">
                                                <input type="hidden" id="line" value="3">
                                                <input type="text" data-line="3" data-om="20000" id="ref20000_3" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="3" data-om="20000" id="prueba20000_3" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20000_3" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20000_3" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20000_3" class="valfc form-control form-control-sm"></td>
                                            <td rowspan="3">Pendiente (m) = <input readonly type="text" id="pend20000" class="form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id4_20000" id="id" value="0"><input type="hidden" id="unidad" value="20000">
                                                <input type="hidden" id="line" value="4">
                                                <input type="text" data-line="4" data-om="20000" id="ref20000_4" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="4" data-om="20000" id="prueba20000_4" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20000_4" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20000_4" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20000_4" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                        <tr>
                                            <td><input type="hidden" class="id5_20000" id="id" value="0"><input type="hidden" id="unidad" value="20000">
                                                <input type="hidden" id="line" value="5">
                                                <input type="text" data-line="5" data-om="20000" id="ref20000_5" class="referencia val_ch form-control form-control-sm"></td>
                                            <td><input type="text" data-line="5" data-om="20000" id="prueba20000_5" class="prueba val_ch form-control form-control-sm"></td>
                                            <td><input type="text" id="absoluto20000_5" class="absoluto form-control form-control-sm"></td>
                                            <td><input type="text" id="insert20000_5" class="incerti form-control form-control-sm"></td>
                                            <td><input type="text" id="fc20000_5" class="valfc form-control form-control-sm"></td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                         </div>

                         <div class="row" id="cont_higro" style="display:none;">
                            <div class="col-sm-12">
                                <h3>Datos de Higrotermoanemometro</h3>
                            </div>
                            <table class="table table-bordered" id="higro_detalle">
                                <thead>
                                    <tr>
                                        <th>DEL INSTRUMENTO PATRÓN (%)</th>
                                        <th>DEL INSTRUMENTO BAJO CALIBRACIÓN (%)</th>
                                        <th>CORRECCIÓN DEL INSTRUMENTO</th>
                                        <th>±UE</th>
                                        <th>FACTOR DE CORRECCIÓN</th>
                                    </tr>
                                </thead>
                                <tbody class="higro_detalle_class">
                                    <tr>
                                        <td>
                                            <input type="hidden" id="id" value="0" class="idc1">
                                            <input type="hidden" id="line" value="1">
                                            <input data-lineval="1" class="patron1 form-control change_higro" type="text" id="patron">
                                        </td>
                                        <td><input data-lineval="1" class="calibracion1 change_higro form-control" type="text" id="calibracion"></td>
                                        <td><input class="instrumento1 form-control" type="text" id="instrumento"></td>
                                        <td><input class="masmenosue1 form-control" type="text" id="masmenosue"></td>
                                        <td><input class="correccion1 form-control" type="text" id="correccion"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" id="id" value="0" class="idc2">
                                            <input type="hidden" id="line" value="2">
                                            <input data-lineval="2" class="patron2 form-control change_higro" type="text" id="patron">
                                        </td>
                                        <td><input data-lineval="2" class="calibracion2 change_higro form-control" type="text" id="calibracion"></td>
                                        <td><input class="instrumento2 form-control" type="text" id="instrumento"></td>
                                        <td><input class="masmenosue2 form-control" type="text" id="masmenosue"></td>
                                        <td><input class="correccion2 form-control" type="text" id="correccion"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" id="id" value="0" class="idc3">
                                            <input type="hidden" id="line" value="3">
                                            <input data-lineval="3" class="patron3 form-control change_higro" type="text" id="patron">
                                        </td>
                                        <td><input data-lineval="3" class="calibracion3 change_higro form-control" type="text" id="calibracion"></td>
                                        <td><input class="instrumento3 form-control" type="text" id="instrumento"></td>
                                        <td><input class="masmenosue3 form-control" type="text" id="masmenosue"></td>
                                        <td><input class="correccion3 form-control" type="text" id="correccion"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" id="id" value="0" class="idc4">
                                            <input type="hidden" id="line" value="4">
                                            <input data-lineval="4" class="patron4 form-control change_higro" type="text" id="patron">
                                        </td>
                                        <td><input data-lineval="4" class="calibracion4 change_higro form-control" type="text" id="calibracion"></td>
                                        <td><input class="instrumento4 form-control" type="text" id="instrumento"></td>
                                        <td><input class="masmenosue4 form-control" type="text" id="masmenosue"></td>
                                        <td><input class="correccion4 form-control" type="text" id="correccion"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-md-3"></div>
                            <div class="col-md-3" style="text-align:right;">FACTOR DE CORRECCIÓN PROMEDIO:</div>
                            <div class="col-md-3 prom_higro">0.0</div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <a href="<?php echo base_url();?>Equipos" type="button" class="btn btn-success mr-1 mb-1"  >Regresar</a>
                            </div>
                            <div class="col-md-10" style="text-align: end;">
                                <button id="buttonsave" type="button" class="btn btn-info mr-1 mb-1" onclick="saveequipo()" ><?php echo $savebuttons;?></button>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
