<?php
$color = "#CCFF99";
$razon="";
$logo="";
if($empre=="1" || $empre=="2" || $empre=="3" || $empre=="6") {
    $logo =  base_url()."app-assets/img/logo2.png";
    $razon = "Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V."; 
}
else if($empre=="4") {
    $logo =  base_url()."app-assets/img/logo_ahisa.png";
    $razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V."; 
}
else if($empre=="5") {
    $logo =  base_url()."app-assets/img/logo_auven.png";
    $razon = "AUVEN S. de R.L. de C.V.";
}
?>
<table style="width: 100%">
    <tr>
        <td width="30%" align="center"><img height="60px" src="<?php echo $logo; ?>"><br></td>
        <td width="70%" align="right" style="font-size: 12px"><br><br>
            <strong><?php echo $razon; ?></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" style="font-size: 14px; background-color: #058e1c; color: white"><br><strong>ENCUESTA DE SATISFACCIÓN</strong></td>
    </tr>
    <tr>
        <td align="right" colspan="2">
                
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Buen día,<br>
            En <?php echo $razon; ?>, nos interesa conocer su opinión y satisfacción respecto a la calidad de nuestros servicios, por lo que le pedimos nos apoye contestando las preguntas del siguiente link: <a href="<?php echo $url; ?>" class="form-control" target="_blank"> Encuesta de satisfacción</a><br><br>
        </td>
    </tr>
</table>

