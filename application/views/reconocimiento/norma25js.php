<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/pages/form-wizard.css">
<script type="text/javascript">
	$(document).ready(function($) {
		wizard=$(".icons-tab-steps").steps({
		    headerTag: "h6",
		    bodyTag: "fieldset",
		    transitionEffect: "fade",
		    titleTemplate: '<span class="step">#index#</span> #title#',
		    enableAllSteps: true,
		    labels: {
		      next: '<button type="button" class="btn btn btn-success siguientepaso">Siguiente <i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
		      previous: '<button type="button" class="btn btn btn-success"><i class="fa fa-arrow-left" aria-hidden="true"></i> Anterior</button>',
		      finish: '<button type="button" onclick="finalizarProceso()" class="btn btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Finalizar</button>'
		    },
		    onStepChanging: function (event, currentIndex, newIndex) {
		      
		            return true; 
		        
		    },
		    onFinished: function (event, currentIndex,newIndex) {
		      return true;
		    }
		});

		$("#icons-tab-steps-t-2").on("click",function(){
			if($("#id_reco_paso2").val()=="0"){
				$("#savep2_2").attr("disabled",true);
			}
		});
		$("#icons-tab-steps-t-3").on("click",function(){
			if($("#id_reco_paso2").val()=="0"){
				$(".savereconocimientoini").attr("disabled",true);
			}
		});
		$("#icons-tab-steps-t-4").on("click",function(){
			if($("#id_reco_paso2").val()=="0"){
				$("#savep5").attr("disabled",true);
			}
		});
	});
</script>

<script src="<?php echo base_url(); ?>public/js/reconocimiento/norma25.js?v=<?php echo date('YmdGis') ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/reconocimiento/norma25_2.js?v=<?php echo date('YmdGis') ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/reconocimiento/paso2_norma25.js?v=2<?php echo date('YmdGis') ?>" type="text/javascript"></script>