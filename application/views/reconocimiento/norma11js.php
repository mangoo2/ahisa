<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/pages/form-wizard.css">
<script type="text/javascript">
	$(document).ready(function($) {
		wizard=$(".icons-tab-steps").steps({
	    headerTag: "h6",
	    bodyTag: "fieldset",
	    transitionEffect: "fade",
	    titleTemplate: '<span class="step">#index#</span> #title#',
	    enableAllSteps: true,
	    labels: {
	      next: '<button type="button" class="btn btn btn-success">Siguiente <i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
	      previous: '<button type="button" class="btn btn btn-success"><i class="fa fa-arrow-left" aria-hidden="true"></i> Anterior</button>',
	      finish: '<button type="button" onclick="finalizarProceso()" class="btn btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Finalizar</button>'
	    },
	    onStepChanging: function (event, currentIndex, newIndex) {
	      
	            return true; 
	        
	    },
	    onFinished: function (event, currentIndex,newIndex) {
	      return true;
	    }
	  });
	});
</script>




<script src="<?php echo base_url(); ?>public/js/reconocimiento/norma11.js?v=<?php echo date('YmdGis') ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/reconocimiento/paso2_norma11.js?v=<?php echo date('YmdGis') ?>" type="text/javascript"></script>