<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 12px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">AH-LX-EV-</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12">
                                <table class="table table-bordered" width="100%">
                                    <tr>
                                        <td width="20%" class="titulos">No. de Informe</td>
                                        <td width="40%"><input type="text" name="num_informe" class="form-control form-control-sm"> </td>
                                        <td width="10%" class="titulos">Fecha</td>
                                        <td width="20%"><input type="date" name="fecha" class="form-control form-control-sm" value="<?php echo date("Y-m-d");?>"> </td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Razón Social</td>
                                        <td colspan="3"> <input type="text" name="cliente" class="form-control form-control-sm" value="<?php echo $cli->razon_social;?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Calle y número</td>
                                        <td><input type="text" name="calle_num" class="form-control form-control-sm" value="<?php echo $cli->calle.' No. Int '.$cli->no_int;?>"></td>
                                        <td class="titulos">Colonia</td>
                                        <td><input type="text" name="colonia" class="form-control form-control-sm" value="<?php echo $cli->colonia;?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Municipio</td>
                                        <td><input type="text" name="poblacion" class="form-control form-control-sm" value="<?php echo $cli->poblacion;?>"></td>
                                        <td class="titulos">Estado</td>
                                        <td><input type="text" name="estado" class="form-control form-control-sm" value="<?php echo $cli->estado;?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Código Postal</td>
                                        <td><input type="text" name="cp" class="form-control form-control-sm" value="<?php echo $cli->cp;?>"></td>
                                        <td class="titulos">RFC</td>
                                        <td><input type="text" name="rfc" class="form-control form-control-sm" value="<?php echo $cli->rfc;?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Giro de la Empresa</td>
                                        <td><input type="text" name="giro" class="form-control form-control-sm" value="<?php echo $cli->giro;?>"></td>
                                        <td class="titulos">Teléfono</td>
                                        <td><input type="text" name="telefono" class="form-control form-control-sm" value=""></td>
                                    </tr>
                                    <tr>
                                        <td class="titulos">Representante Legal</td>
                                        <td colspan="3"><input type="text" name="representa" class="form-control form-control-sm" value="<?php echo $cli->representa;?>"></td>
                                    </tr>
                                </table>
                                <table class="table table-bordered" width="100%">
                                    <tr>
                                        <td width="50%" class="titulos">Nombre y cargo a quien se dirigirá el informe de resultados</td>
                                        <td colspan="3"><input type="text" name="representa" class="form-control form-control-sm" value="<?php echo $cli->representa;?>"></td>
                                    </tr>
                                </table>

                                <div class="col-md-12">
                                <table class="table table-bordered" width="100%">
                                    <tr>
                                        <td colspan="6" align="center" width="100%" class="titulos">HORARIOS DE TRABAJO</td>  
                                    </tr>
                                    <tr>
                                        <td class="titulos">1ero</td>
                                        <td><input type="text" name="giro" class="form-control form-control-sm" value="<?php echo $cli->giro;?>"></td>
                                        <td class="titulos">2do</td>
                                        <td><input type="text" name="telefono" class="form-control form-control-sm" value=""></td>
                                        <td class="titulos">3ero</td>
                                        <td><input type="text" name="telefono" class="form-control form-control-sm" value=""></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
