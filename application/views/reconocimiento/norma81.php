<input type="hidden" id="id_orden" value="<?php echo $id_orden;?>">
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .titulosC{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
    }
    .titulos-12{
        font-size: 10px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    thead{
       font-size: 11px !important; 
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(2.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
    @media (max-width:500px){
        .content-header{
            margin-top: 5px;
            margin-bottom: 5px !important;
            text-align: center;
        }
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 9px !important; 
            min-width: 45px !important;  
        }
        .chk_form {
            margin-top: 15px !important;
            transform: scale(1.3);
        }
        .chk_form2 {
            margin-top: 5px;
            transform: scale(1.5);
        }
    }

    @media (max-width:680px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 10px !important;
            min-width: 55px !important;  
        }
        .chk_form {
            margin-top: 30%;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            transform: scale(1.5);
        }
    }
    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 11px !important;
            min-width: 65px !important;  
        }
        .chk_form {
            margin-top: 30%;
            margin-left: -7px;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            margin-left: -7px;
            transform: scale(1.5);
        }
    }
    #table_evaluar th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    #table_det th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    .tdinput{
            padding: 0px !important;
        }
    .tdinput input {    
        min-width: 62px;
    }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
            min-width: 50px;
        }
        #table_evaluar th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        #table_det th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .form input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(2.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reconocimiento NOM-081</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_reco1">
                            <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
                            <input type="hidden" name="id_orden" value="<?php echo $orden;?>">
                            <input type="hidden" name="id_chs" value="<?php echo $id_chs;?>">
                            <input type="hidden" id="nom" value="81">
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    No. de Informe
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="num_informe_rec" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->num_informe_rec;?>">
                                </div>
                                <div class="col-md-2 titulos">
                                    Fecha
                                </div>
                                <div class="col-md-2">
                                    <input type="date" name="fecha" class="form-control form-control-sm" value="<?php if($id==0) echo date("Y-m-d"); else echo $cli->fecha?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Razón Social
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="cliente" class="form-control form-control-sm" value="<?php if($id==0) $cli->razon_social; else echo $cli->cliente;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Calle y número
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="calle_num" class="form-control form-control-sm" value="<?php if($id==0) $cli->calle." ".$cli->no_ext.' No. Int '.$cli->no_int; else echo $cli->calle_num;?>">
                                </div>
                                <div class="col-md-3 titulos">
                                    Colonia
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="colonia" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colonia;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Municipio
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="poblacion" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->poblacion;?>">
                                </div>
                                <div class="col-md-3 titulos">
                                    Estado
                                </div>
                                <div class="col-md-3">
                                    <!--<input type="text" name="estado" class="form-control form-control-sm" value="<?php if($id==0) echo $edo; else echo $cli->estado;?>">-->
                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="estado" class="form-control form-control-sm" >
                                                <?php
                                                foreach ($estados as $e) {
                                                    $sel="";
                                                    /*if($id==0 && $e->estado==$edo || $id==0 && $edo==$cli->estado || $id>0 && mb_strtoupper($e->estado,"UTF-8")==mb_strtoupper($cli->estado,"UTF-8")){
                                                        $sel="selected"; 
                                                    }*/
                                                    if($e->id==$cli->estado){
                                                        $sel="selected"; 
                                                    }
                                                    //echo "<option ".$sel." value='".mb_strtoupper($e->estado,"UTF-8")."'>$e->estado</option>";
                                                    echo "<option ".$sel." value='".$e->id."'>".$e->estado."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Código Postal
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="cp" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->cp;?>">
                                </div>
                                <div class="col-md-3 titulos">
                                    RFC
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="rfc" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->rfc;?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Giro de la Empresa
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="giro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->giro;?>">
                                </div>
                                <div class="col-md-3 titulos">
                                    Teléfono
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="telefono" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->telefono; ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 titulos">
                                    Representante Legal
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="representa" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->representa;?>">
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12"><br></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 titulos">
                                    Nombre / Cargo a quien se dirigirá el informe de resultados
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="nom_cargo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nom_cargo;?>">
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12"><br></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulos" style="text-align:center;">TURNOS, HORARIOS Y DÍAS DE OPERACIÓN DE LA FUENTE FIJA</div>
                            </div>
                            <div class="row">
                                <table class="table table-responsive">
                                    <tbody>
                                        <tr>
                                            <td class="titulosC">1 ro</td>
                                            <td><input type="text" name="priero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->priero; ?>"></td>
                                            <td class="titulosC">2 do</td>
                                            <td><input type="text" name="segdo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->segdo; ?>"></td>
                                            <td class="titulosC">3 ro</td>
                                            <td><input type="text" name="tercero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->tercero; ?>"></td>
                                            <td class="titulosC">Otro</td>
                                            <td><input type="text" name="otro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->otro; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="titulosC">Días</td>
                                            <td><input type="text" name="pero_dias" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->pero_dias; ?>"></td>
                                            <td class="titulosC">Días</td>
                                            <td><input type="text" name="sdo_dias" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->sdo_dias; ?>"></td>
                                            <td class="titulosC">Días</td>
                                            <td><input type="text" name="tero_dias" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->tero_dias; ?>"></td>
                                            <td class="titulosC">Días</td>
                                            <td><input type="text" name="otro_dias" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->otro_dias; ?>"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulosC">
                                    HORARIO CRÍTICO DONDE LA FUENTE FIJA PRODUZCA LOS NIVELES MÁXIMOS DE EMISIÓN
                                </div>
                                <div class="col-md-12 titulos">
                                    <textarea name="horario_critico" class="form-control" rows="2" value="<?php if($id>0) echo $cli->horario_critico; ?>"><?php if($id>0) echo $cli->horario_critico; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulosC">
                                    ZONA DONDE SE UBICA LA FUENTE FIJA
                                </div>
                                <div class="col-md-12 titulos">
                                    <textarea name="zona_ubica" class="form-control" rows="2" value="<?php if($id>0) echo $cli->zona_ubica; ?>"><?php if($id>0) echo $cli->zona_ubica; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th colspan="4" style="text-align: center">DESCRIPCIÓN DE COLINDANCIAS Y FUENTES EMISORAS DE RUIDO EN LA PERIFERIA DE LA FUENTE FIJA</th>
                                        </tr>
                                        <tr>
                                            <th>DIRECCIÓN</th>
                                            <th>DESCRIPCIÓN DE LA COLINDANCIA</th>
                                            <th>NIVEL SONORO MÁXIMO ENCONTRADO EN LA COLINDANCIA (db "A")</th>
                                            <th>DESCRIPCIÓN DE LAS ACTIVIDADES POTENCIALMENTE RUIDOSAS (EQUIPO, MÁQUINARIA Y/O PROCESOS)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="titulosC">NORTE</td>
                                            <td><input type="text" name="desc_norte" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_norte; ?>"></td>
                                            <td><input type="number" name="nivel_sonoro_norte" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nivel_sonoro_norte; ?>"></td>
                                            <td><input type="text" name="desc_actividades_norte" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_actividades_norte; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="titulosC">SUR</td>
                                            <td><input type="text" name="desc_sur" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_sur; ?>"></td>
                                            <td><input type="number" name="nivel_sonoro_sur" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nivel_sonoro_sur; ?>"></td>
                                            <td><input type="text" name="desc_actividades_sur" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_actividades_sur; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="titulosC">ESTE</td>
                                            <td><input type="text" name="desc_este" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_este; ?>"></td>
                                            <td><input type="number" name="nivel_sonoro_este" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nivel_sonoro_este; ?>"></td>
                                            <td><input type="text" name="desc_actividades_este" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_actividades_este; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="titulosC">OESTE</td>
                                            <td><input type="text" name="desc_oeste" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_oeste; ?>"></td>
                                            <td><input type="number" name="nivel_sonoro_oeste" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nivel_sonoro_oeste; ?>"></td>
                                            <td><input type="text" name="desc_actividades_oeste" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->desc_actividades_oeste; ?>"></td>
                                        </tr>
                                        <tr>
                                            <th class="titulosC" colspan="4" style="text-align: center">SECCIÓN DE LA(S) ZONA(S) CRÍTICA(S) </th>
                                        </tr>
                                        <tr>
                                            <td class="titulosC">No. ZC</td>
                                            <td class="titulosC">COLINDANCIA</td>
                                            <td colspan="2" class="titulosC">JUSTIFICACIÓN DE LA SELECCIÓN DE LA(S) ZONA(S) CRÍTICA(S)</td>
                                        </tr>
                                        <tr>
                                            <td><input type="number" name="no_zc1" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->no_zc1; ?>"></td>
                                            <td><input type="text" name="colinda_zc1" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colinda_zc1; ?>"></td>
                                            <td colspan="2" rowspan="4"><input style="height: 200px;" type="text" name="justifica_zc" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->justifica_zc; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td><input type="number" name="no_zc2" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->no_zc2; ?>"></td>
                                            <td><input type="text" name="colinda_zc2" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colinda_zc2; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td><input type="number" name="no_zc3" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->no_zc3; ?>"></td>
                                            <td><input type="text" name="colinda_zc3" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colinda_zc3; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td><input type="number" name="no_zc4" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->no_zc4; ?>"></td>
                                            <td><input type="text" name="colinda_zc4" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colinda_zc4; ?>"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulosC">
                                    CROQUIS DE LA UBICACIÓN DE LA FUENTE FIJA Y SUS COLINDANCIAS, MÁQUINARIA Y/O PROCESOS EMISORES DE RUIDO,
                                    VALORES OBTENIDOS EN EL RECONOCIMIENTO INICIAL, UBICACIÓN DE LA(S) ZONA(S) CRÍTICAS Y PUNTOS DE MEDICIÓN
                                </div>
                                <div class="col-md-12 titulos">
                                    <textarea name="croquis" class="form-control" rows="3" value="<?php if($id>0) echo $cli->croquis; ?>"><?php if($id>0) echo $cli->croquis; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulosC">
                                    JUSTIFICACIÓN DE LA UBICACIÓN Y SEPARACIÓN DE LOS PUNTOS DE MEDICIÓN DE RUIDO DE FUENTE Y FONDO EN CADA ZONA CRÍTICA
                                </div>
                                <div class="col-md-12 titulos">
                                    <textarea name="justifica_ubica" class="form-control" rows="3" value="<?php if($id>0) echo $cli->justifica_ubica; ?>"><?php if($id>0) echo $cli->justifica_ubica; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 titulosC">
                                    OBSERVACIONES / EVENTUALIDADES DESCRIPTTIVAS
                                </div>
                                <div class="col-md-12 titulos">
                                    <textarea name="observaciones" class="form-control" rows="3" value="<?php if($id>0) echo $cli->observaciones; ?>"><?php if($id>0) echo $cli->observaciones; ?></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" style="text-align: end;"><br></div>
                                <div class="col-md-12" style="text-align: end;">
                                    <button type="button" class="btn btn btn-success" id="savep1">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
