<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
    }
    .titulos-12{
        font-size: 10px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    thead{
       font-size: 11px !important; 
    }

    @media (max-width:500px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 9px !important; 
            min-width: 45px !important;  
        }
    }

    @media (max-width:680px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 10px !important;
            min-width: 55px !important;  
        }
    }
    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 11px !important;
            min-width: 65px !important;  
        }
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reconocimiento - Paso 2</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row" style="margin-top: 10px;">
                            <form class="form" id="form_reco2">
                                <input type="hidden" id="id_reconocimiento" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                <input type="hidden" id="norma" value="<?php echo $norma; ?>">
                                <div class="col-md-12 reco1 table-responsive">
                                    <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                    <table id="table_det" class="table table-bordered table-responsive" width="100%">
                                        <thead style="text-align:center">
                                            <tr>
                                                <th colspan="13">DESCRIPCIÓN DE LAS ÁREAS Y TAREAS VISUALES</th>
                                            </tr>
                                            <tr>
                                                <th rowspan="2">No.</th>
                                                <th rowspan="2">ÁREA</th>
                                                <th rowspan="2">¿REQUIERE ILUMINACIÓN LOCALIZADA?</th>
                                                <th rowspan="2">PUESTO DE TRABAJO</th>
                                                <th rowspan="2">No. DE TRABAJADORES</th>
                                                <th rowspan="2">TAREA VISUAL / ÁREA DE TRABAJO</th>
                                                <th rowspan="2">TIPO DE LUMINARIA</th>
                                                <th rowspan="2">NÚMERO DE LUMINARIAS/ POTENCIA DE LAMPARAS</th>
                                                <th rowspan="2">¿EXISTE INCIDENCIA DE LA LUZ NATURAL?</th>
                                                <th colspan="3">COLOR / TIPO DE SUPERFICIE</th>
                                                <th ></th>
                                            </tr>
                                            <tr>
                                                <th>PISO</th>
                                                <th>PARED</th>
                                                <th>TECHO</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody class="trbody" id="cont_tabla_det">
                                            <tr>
                                                <td width="8%"><input type="text" name="num" class="form-control form-control-sm" value="1"></td>
                                                <td width="12%"><input type="text" name="area" class="form-control form-control-sm"> </td>
                                                <td width="5%"><input type="text" name="requiere_ilum" class="form-control form-control-sm"></td>
                                                <td width="10%"><input type="text" name="puesto" class="form-control form-control-sm"> </td>
                                                <td width="5%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>
                                                <td width="5%"><input type="text" name="tarea_visual" class="form-control form-control-sm"> </td>
                                                <td width="10%"><input type="text" name="tipo_lumin" class="form-control form-control-sm"></td>
                                                <td width="10%"><input type="text" name="num_lumin" class="form-control form-control-sm"> </td>
                                                <td width="5%"><input type="text" name="existe_incidencia" class="form-control form-control-sm"></td>
                                                <td width="10%"><input type="text" name="piso" class="form-control form-control-sm"></td>
                                                <td width="10%"><input type="text" name="pared" class="form-control form-control-sm"> </td>
                                                <td width="9%"><input type="text" name="techo" class="form-control form-control-sm"></td>
                                                <td width="1%"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                    </div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" class="btn btn btn-success" id="save">Siguiente</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
