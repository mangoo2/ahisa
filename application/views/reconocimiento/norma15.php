<input type="hidden" id="id_orden" value="<?php echo $id_orden;?>">
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .titulosC{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
    }
    .titulos-12{
        font-size: 10px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    thead{
       font-size: 11px !important; 
    }
    .chk_form {
        margin-top: 5px;
        transform: scale(1.2);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
    @media (max-width:500px){
        .content-header{
            margin-top: 5px;
            margin-bottom: 5px !important;
            text-align: center;
        }
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 9px !important; 
            min-width: 45px !important;  
        }
        .chk_form {
            margin-top: 5px !important;
            transform: scale(1.3);
        }
        .chk_form2 {
            margin-top: 5px;
            transform: scale(1.5);
        }
    }

    @media (max-width:680px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 10px !important;
            min-width: 55px !important;  
        }
        .chk_form {
            margin-top: 5px;
            transform: scale(1.2);
        }
        .chk_form2 {
            margin-top: 12%;
            transform: scale(1.5);
        }
    }
    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 11px !important;
            min-width: 65px !important;  
        }
        .chk_form {
            margin-top: 5px;
            margin-left: -7px;
            transform: scale(1.2);
        }
        .chk_form2 {
            margin-top: 12%;
            margin-left: -7px;
            transform: scale(1.5);
        }
    }
    #table_evaluar th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    #table_det th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    .tdinput{
            padding: 4px !important;
        }
    .tdinput input {    
        min-width: 60px;
    }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 2px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
            min-width: 50px;
        }
        #table_evaluar th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        #table_det th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .wizard > .steps > ul > li {
            width: 25% !important;
        }
        fieldset{
            padding: 0px !important;
        }
        .form input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
    }
    .chk_form {
        margin-top: 5px;
        transform: scale(1.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reconocimiento NOM-015</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="icons-tab-steps" id="icons-tab-steps">
                            <h6>Paso 1</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_reco1">
                                        <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
                                        <input type="hidden" name="id_orden" value="<?php echo $orden;?>">
                                        <input type="hidden" name="id_chs" value="<?php echo $id_chs;?>">
                                        <input type="hidden" id="nom" value="11">
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                No. de Informe
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="num_informe_rec" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->num_informe_rec;?>">
                                            </div>
                                            <div class="col-md-2 titulos">
                                                Fecha
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" name="fecha" class="form-control form-control-sm" value="<?php if($id==0)  date("Y-m-d"); else echo $cli->fecha?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Razón Social
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="cliente" class="form-control form-control-sm" value="<?php if($id==0)  $cli->razon_social; else echo $cli->cliente;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Calle y número
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="calle_num" class="form-control form-control-sm" value="<?php if($id==0)  $cli->calle." ".$cli->no_ext.' No. Int '.$cli->no_int; else echo $cli->calle_num;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Colonia
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="colonia" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colonia;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Municipio
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="poblacion" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->poblacion;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Estado
                                            </div>
                                            <div class="col-md-3">
                                                <!--<input type="text" name="estado" class="form-control form-control-sm" value="<?php if($id==0) echo $edo; else echo $cli->estado;?>">-->
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <select name="estado" class="form-control form-control-sm" >
                                                            <?php
                                                            foreach ($estados as $e) {
                                                                $sel="";
                                                                /*if($id==0 && $e->estado==$edo || $id==0 && $edo==$cli->estado || $id>0 && mb_strtoupper($e->estado,"UTF-8")==mb_strtoupper($cli->estado,"UTF-8")){
                                                                    $sel="selected"; 
                                                                }*/
                                                                if($e->id==$cli->estado){
                                                                    $sel="selected"; 
                                                                }
                                                                //echo "<option ".$sel." value='".mb_strtoupper($e->estado,"UTF-8")."'>$e->estado</option>";
                                                                echo "<option ".$sel." value='".$e->id."'>".$e->estado."</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Código Postal
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="cp" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->cp;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                RFC
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="rfc" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->rfc;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Giro de la Empresa
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="giro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->giro;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Teléfono
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="telefono" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->telefono; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Representante Legal
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="representa" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->representa;?>">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 titulos">
                                                Nombre / Cargo a quien se dirigirá el informe de resultados
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="nom_cargo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nom_cargo;?>">
                                            </div>
                                            
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12"><br></div>
                                    </div>

                                    <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                    <table id="table_det" class="table table-bordered table-responsive" width="100%">
                                        <thead style="text-align:center">
                                            <tr>
                                                <th rowspan="2" width="20%">ÁREA</th>
                                                <th colspan="2" width="5%">CONDICIÓN TÉRMICA</th>
                                                <th colspan="2" width="5%">TIPO DE ÁREA</th>
                                                <th colspan="2" width="5%">TIPO DE VENTILACIÓN</th>
                                                <th rowspan="2" width="20%">GENERADOR(ES) DE LA CONDICIÓN TÉRMICA EXTREMA</th>
                                                <th rowspan="2" width="20%">PUESTO DE TRABAJO EXPUESTO</th>
                                                <th rowspan="2" width="10%">TIEMPO DE EXPOSICIÓN POR CICLO EN UNA HORA (min)</th>
                                                <th rowspan="2" width="10%">FRECUENCIA (No. de cilcos en una hora)</th>
                                                <th rowspan="2" width="5%"></th>
                                            </tr>
                                            <tr>
                                                <th>ABATIDA</th>
                                                <th>ELEVADA</th>
                                                <th>ABIERTA</th>
                                                <th>CERRADA</th>
                                                <th>ARTIFICIAL</th>
                                                <th>NATURAL</th>
                                            </tr>
                                        </thead>
                                        <tbody class="trbody" id="cont_tabla_det">
                                            <?php if($id_reco_paso2==0){ ?>
                                                <tr>
                                                    <td class="tdinput" width="20%">
                                                        <input type="hidden" id="id" class="form-control form-control-sm" value="0">
                                                        <input type="text" id="area" class="form-control form-control-sm">
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="condicion_termica" name="condicion_termica" class="form-control form-control-sm chk_form"></td>
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="condicion_termica2" name="condicion_termica" class="form-control form-control-sm chk_form">
                                                    </td>
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="tipo_area" name="tipo_area" class="form-control form-control-sm chk_form"></td>
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="tipo_area2" name="tipo_area" class="form-control form-control-sm chk_form">
                                                    </td>
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="tipo_ventilacion" name="tipo_ventilacion" class="form-control form-control-sm chk_form"></td>
                                                    <td class="tdinput" width="5%">
                                                        <input type="radio" id="tipo_ventilacion2" name="tipo_ventilacion" class="form-control form-control-sm chk_form">
                                                    </td>
                                                    <td class="tdinput" width="20%"><input type="text" id="generadores_condicion" class="form-control form-control-sm"></td>
                                                    <td class="tdinput" width="20%"><input type="text" id="puesto" class="form-control form-control-sm"> </td>
                                                    <td class="tdinput" width="10%"><input type="text" id="tiempo_expo" class="form-control form-control-sm"></td>
                                                    <td class="tdinput" width="10%"><input type="text" id="frecuencia" class="form-control form-control-sm"></td>
                                                    <td class="tdinput" width="5%"></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-12" style="text-align: end;"><br></div>
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep1">Guardar</button>
                                        </div>
                                    </div>
 
                                <!-------------------------------------------------->
                            </fieldset>

                            <h6>Paso 2</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_reco_2">
                                        <input type="hidden" id="id_reconocimiento2" name="id" value="<?php echo $id_reco;?>">
                                        <div class="row">
                                            <label class="col-md-12 titulosC">DESCRIPCIÓN DEL PROCESO</label>
                                            <div class="col-md-12">
                                                <textarea type="text" rows="7" name="desc_proceso" id="desc_proceso" class="form-control"><?php if(isset($cli) && $id>0) echo $cli->desc_proceso; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <br>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-12 titulosC">PLANO DE UBICACIÓN DE ÁREAS, FUENTES GENERADORAS DE LA CONDICIÓN TÉRMICA EXTREMA Y UBICACIÓN DE LOS PUNTOS DE MEDICIÓN</label>
                                            <div class="col-md-12">
                                                <textarea type="text" rows="10" name="plano_ubica" id="plano_ubica" class="form-control"><?php if(isset($cli) && $id>0) echo $cli->plano_ubica; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <br>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-12 titulosC">OBSERVACIONES</label>
                                            <div class="col-md-12">
                                                <textarea type="text" rows="3" name="observaciones" id="observaciones" class="form-control"><?php if(isset($cli)) echo $cli->observaciones; ?></textarea>
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                            </div>
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn btn-success" onclick="guardarp2()" >Guardar</button>
                                            </div>
                                        </div> 
                                    </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <!--<h6>Paso 4</h6>
                            <fieldset>-->
                                <!-------------------------------------------------->
                                    <!--<div class="col-md-12" style="text-align: center;">
                                        <a href="<?php echo base_url().'Nom/lentamientoServicio/0/2'; ?>" type="button" class="btn btn btn-success">Levantamiento de Información</a>
                                    </div>-->
                                <!-------------------------------------------------->
                            <!--</fieldset>-->
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
