f<input type="hidden" id="id_orden" value="<?php echo $id_orden;?>">
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .titulos-12{
        font-size: 10px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    thead{
       font-size: 11px !important; 
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(1.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(1.5);
    }
    @media (max-width:500px){
        .content-header{
            margin-top: 5px;
            margin-bottom: 5px !important;
            text-align: center;
        }
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 9px !important; 
            min-width: 45px !important;  
        }
        .chk_form {
            margin-top: 15px !important;
            transform: scale(1.3);
        }
        .chk_form2 {
            margin-top: 5px;
            transform: scale(1.5);
        }
    }

    @media (max-width:680px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 10px !important;
            min-width: 55px !important;  
        }
        .chk_form {
            margin-top: 30%;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            transform: scale(1.5);
        }
    }
    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 11px !important;
            min-width: 65px !important;  
        }
        .chk_form {
            margin-top: 30%;
            margin-left: -7px;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            margin-left: -7px;
            transform: scale(1.5);
        }
    }
    #table_evaluar th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    #table_det th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    .tdinput{
            padding: 0px !important;
        }
    .tdinput input {    
        min-width: 62px;
    }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
            min-width: 50px;
        }
        #table_evaluar th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        #table_det th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .wizard > .steps > ul > li {
            width: 25% !important;
        }
        fieldset{
            padding: 0px !important;
        }
        .form input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(1.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(1.5);
    }
    .chk_form3 {
        margin-top: 5%;
        transform: scale(1.5);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reconocimiento</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="icons-tab-steps" id="icons-tab-steps">
                            <h6>Paso 1</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_reco1">
                                        <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
                                        <input type="hidden" name="id_orden" value="<?php echo $orden;?>">
                                        <input type="hidden" name="id_chs" value="<?php echo $id_chs;?>">
                                        <input type="hidden" id="nom" value="25">
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                No. de Informe
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="num_informe_rec" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->num_informe_rec;?>">
                                            </div>
                                            <div class="col-md-2 titulos">
                                                Fecha
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" name="fecha" class="form-control form-control-sm" value="<?php if($id==0)  date("Y-m-d"); else echo $cli->fecha?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Razón Social
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="cliente" class="form-control form-control-sm" value="<?php if($id==0)  $cli->razon_social; else echo $cli->cliente;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Calle y número
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="calle_num" class="form-control form-control-sm" value="<?php if($id==0) $cli->calle." ".$cli->no_ext.' No. Int '.$cli->no_int; else echo $cli->calle_num;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Colonia
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="colonia" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colonia;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Municipio
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="poblacion" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->poblacion;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Estado
                                            </div>
                                            <div class="col-md-3">
                                                <!--<input type="text" name="estado" class="form-control form-control-sm" value="<?php if($id==0) echo $edo; else echo $cli->estado;?>">-->
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <select name="estado" class="form-control form-control-sm" >
                                                            <?php
                                                            foreach ($estados as $e) {
                                                                $sel="";
                                                                /*if($id==0 && $e->estado==$edo || $id==0 && $edo==$cli->estado || $id>0 && mb_strtoupper($e->estado,"UTF-8")==mb_strtoupper($cli->estado,"UTF-8")){
                                                                    $sel="selected"; 
                                                                }*/
                                                                if($e->id==$cli->estado){
                                                                    $sel="selected"; 
                                                                }
                                                                //echo "<option ".$sel." value='".mb_strtoupper($e->estado,"UTF-8")."'>$e->estado</option>";
                                                                echo "<option ".$sel." value='".$e->id."'>".$e->estado."</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Código Postal
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="cp" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->cp;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                RFC
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="rfc" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->rfc;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Giro de la Empresa
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="giro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->giro;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Teléfono
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="telefono" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->telefono; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Representante Legal
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="representa" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->representa;?>">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 titulos">
                                                Nombre / Cargo a quien se dirigirá el informe de resultados
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="nom_cargo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nom_cargo;?>">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulos" style="text-align:center;">HORARIOS DE TRABAJO</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1 titulos">1ero</div>
                                            <div class="col-md-3 ">
                                                <input type="text" name="priero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->priero; ?>">
                                            </div>
                                            <div class="col-md-1 titulos">2do</div>
                                            <div class="col-md-3 ">
                                                <input type="text" name="segdo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->segdo; ?>">
                                            </div>
                                            <div class="col-md-1 titulos">3ero</div>
                                            <div class="col-md-3 ">
                                                <input type="text" name="tercero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->tercero; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                ADMINISTRATIVO
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="hora_admin" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->hora_admin; ?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                OTRO
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="hora_otro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->hora_otro; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div style="font-size: 11px;" class="col-md-4 titulos">
                                                SELECCIONAR EL TURNO QUE SE PRESENTA LAS CONDICIONES CRÍTICAS DE ILUMINACIÓN
                                            </div>
                                            <div class="col-md-1 titulos">
                                                1ero
                                            </div>
                                            <div class="col-md-1">
                                                <input type="checkbox" id="pero_turno" name="pero_turno" class="form-control form-control-sm chk_form" <?php if($id>0 && $cli->pero_turno==1) echo "checked"; ?>>
                                            </div>
                                            <div class="col-md-1 titulos">
                                                2do
                                            </div>
                                            <div class="col-md-1">
                                                <input type="checkbox" id="sdo_turno" name="sdo_turno" class="form-control form-control-sm chk_form" <?php if($id>0 && $cli->sdo_turno==1) echo "checked"; ?>>
                                            </div>
                                            <div class="col-md-1 titulos">
                                                3ero
                                            </div>
                                            <div class="col-md-1">
                                                <input type="checkbox" id="tero_turno" name="tero_turno" class="form-control form-control-sm chk_form" <?php if($id>0 && $cli->tero_turno==1) echo "checked"; ?>>
                                            </div>
                                            <div class="col-md-1 titulos">
                                                OTRO
                                            </div>
                                            <div class="col-md-1">
                                                <input type="checkbox" id="otro_turno" name="otro_turno" class="form-control form-control-sm chk_form" <?php if($id>0 && $cli->otro_turno==1) echo "checked"; ?>>
                                            </div>
                                            <div class="col-md-10 titulos">

                                            </div>
                                            <div class="col-md-1 titulos">
                                                Adm
                                            </div>
                                            <div class="col-md-1">
                                                <input type="checkbox" id="adm_turno" name="adm_turno" class="form-control form-control-sm chk_form" <?php if($id>0 && $cli->adm_turno==1) echo "checked"; ?>>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulos">
                                                JUSTIFICACIÓN DEL HORARIO EN EL QUE SE REALIZARÁN LAS MEDICIONES
                                            </div>
                                            <div class="col-md-12 titulos">
                                                <textarea name="justifica_horario" class="form-control" rows="3" value="<?php if($id>0) echo $cli->justifica_horario; ?>"><?php if($id>0) echo $cli->justifica_horario; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulos">
                                                DESCRIPCIÓN DEL PROCESO PRODUCTIVO
                                            </div>
                                            <div class="col-md-12 titulos">
                                                <textarea name="desc_proceso" class="form-control" rows="3" value="<?php if($id>0) echo $cli->desc_proceso; ?>"><?php if($id>0) echo $cli->desc_proceso; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row" style="display: none;">
                                            <div class="col-md-12 titulos">
                                                PERCEPCIÓN DE LAS CONDICIONES DE ILUMINACIÓN POR PARTE DEL TRABAJADOR
                                            </div>
                                            <div class="col-md-12 titulos">
                                                <textarea name="percep_condi" class="form-control" rows="3" value="<?php if($id>0) echo $cli->percep_condi; ?>"><?php if($id>0) echo $cli->percep_condi; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulos">
                                                PLAN DE MANTENIMIENTO DE LUMINARIAS
                                            </div>
                                            <div class="col-md-12 titulos">
                                                <textarea name="plan_mante" class="form-control" rows="3" value="<?php if($id>0) echo $cli->plan_mante; ?>"><?php if($id>0) echo $cli->plan_mante; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 titulos">
                                                ¿EXISTEN ÁREAS CUYAS LUMINARIAS SEAN NUEVAS Y TENGA UN PERIODO DE OPERACIÓN MENOR A 100 HORAS?
                                            </div>
                                            <div class="col-md-1 titulos">
                                                <div>SI</div>
                                                <div><input type="checkbox" id="si" name="si" class="form-control form-control-sm chk_form2" <?php if($id>0 && $cli->si==1) echo "checked"; ?>></div>
                                            </div>
                                            <div class="col-md-1 titulos">
                                                <div>NO</div>
                                                <div><input type="checkbox" id="no" name="no" class="form-control form-control-sm chk_form2" <?php if($id>0 && $cli->no==1) echo "checked"; ?>></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 titulos">
                                                EN CASO AFIRMATIVO, MENCIONAR CUALES SON ESAS ÁREAS
                                            </div>
                                            <div class="col-md-7">
                                                <textarea name="cuales_areas" class="form-control" rows="3" value="<?php if($id>0) echo $cli->cuales_areas; ?>"><?php if($id>0) echo $cli->cuales_areas; ?></textarea>
                                            </div>
                                            
                                        </div>

                                        
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;"><br></div>
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn btn-success" id="savep1">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <h6>Paso 2</h6>
                            <fieldset style="padding: 0px;">
                                <!-------------------------------------------------->
                                <form class="form" id="form_reco2">
                                    <input type="hidden" id="id_reconocimiento" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                    <input type="hidden" id="norma" value="<?php echo $norma; ?>">
                                    <input type="hidden" id="id_reco_paso2" value="<?php echo $id_reco_paso2; ?>">
                                    <div class="col-md-12 reco1 table-responsive" style="padding: 0px;">
                                        <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        <table id="table_det" class="table table-bordered table-responsive" width="100%">
                                            <thead style="text-align:center">
                                                <tr>
                                                    <th colspan="10">DESCRIPCIÓN DE LAS ÁREAS</th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="2">No.</th>
                                                    <th rowspan="2">ÁREA</th>
                                                    <th rowspan="2">¿REQUIERE ILUMINACIÓN LOCALIZADA? / PERCEPCIÓN DE LAS CONDICIONES DE ILUMINACIÓN</th>
                                                    <!--<th rowspan="2">PUESTO DE TRABAJO</th>
                                                    <th rowspan="2">No. DE TRABAJADORES</th>
                                                    <th rowspan="2">TAREA VISUAL / ÁREA DE TRABAJO</th>-->
                                                    <th rowspan="2">TIPO DE LUMINARIA</th>
                                                    <th rowspan="2">NÚMERO DE LUMINARIAS/ POTENCIA DE LAMPARAS (W)</th>
                                                    <th rowspan="2">¿EXISTE INCIDENCIA DE LA LUZ NATURAL?</th>
                                                    <!--<th rowspan="2">ACTIVIDAD(ES) CONFORME A LA TABLA 1 DE LA NOM-025-STPS-2008</th>-->
                                                    <th colspan="3">COLOR / TIPO DE SUPERFICIE</th>
                                                    <th ></th>
                                                </tr>
                                                <tr>
                                                    <th>PISO</th>
                                                    <th>PARED</th>
                                                    <th>TECHO</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody class="trbody" id="cont_tabla_det">
                                                <?php if($id_reco_paso2==0){ ?>
                                                    <tr>
                                                        <td class="tdinput" width="10%">
                                                            <input type="hidden" name="id" class="form-control form-control-sm" value="0">
                                                            <input type="text" name="num" class="form-control form-control-sm" value="1"></td>
                                                        <td class="tdinput" width="15%"><input type="text" name="area" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="15%"><input type="text" name="requiere_ilum" class="form-control form-control-sm"></td>
                                                        <!--<td class="tdinput" width="5%"><input type="text" name="puesto" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="5%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="5%"><input type="text" name="tarea_visual" class="form-control form-control-sm"> </td>-->
                                                        <td class="tdinput" width="10%"><input type="text" name="tipo_lumin" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="10%"><input type="text" name="num_lumin" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="10%"><input type="text" name="existe_incidencia" class="form-control form-control-sm"></td>
                                                        <!--<td class="tdinput" width="15%"><input type="text" name="actividades" class="form-control form-control-sm"></td>-->
                                                        <td class="tdinput" width="10%"><input type="text" name="piso" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="10%"><input type="text" name="pared" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="9%"><input type="text" name="techo" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="1%"></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        </div>
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep2">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <h6>Paso 3</h6>
                            <fieldset>
                               <!-------------------------------------------------->
                                <form class="form" id="form_reco2_2">
                                    <input type="hidden" id="id_reconocimiento2" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                    <input type="hidden" id="norma2" value="<?php echo $norma; ?>">
                                    <input type="hidden" id="id_reco_paso2_2" value="<?php echo $id_reco_paso2_2; ?>">
                                    <div class="col-md-12 reco1 table-responsive" style="padding: 0px;">
                                        <button type="button" class="btn btn-sm gradient-green-teal white pull-right add2_2"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        <table id="table_det2" class="table table-bordered table-responsive" width="100%">
                                            <thead style="text-align:center">
                                                <tr>
                                                    <th colspan="6">DESCRIPCIÓN DE LAS ACTIVIDADES Y TAREAS VISUALES</th>
                                                </tr>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>PUESTO DE TRABAJO</th>
                                                    <th>No. DE TRABAJADORES</th>
                                                    <th>ACTIVIDAD(ES) DEL PUESTO DE TRABAJO</th>
                                                    <th>IDENTIFICACIÓN DE LA TAREA VISUAL Y/O ÁREA DE TRABAJO</th>
                                                    <th ></th>
                                                </tr>
                                            </thead>
                                            <tbody class="trbody" id="cont_tabla_det_2">
                                                <?php if($id_reco_paso2_2==0){ ?>
                                                    <tr>
                                                        <td class="tdinput" width="5%">
                                                            <input type="hidden" name="id" class="form-control form-control-sm" value="0">
                                                            <input type="text" name="num" class="form-control form-control-sm" value="1">
                                                        </td>
                                                        <td class="tdinput" width="25%"><input type="text" name="puesto" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="15%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="25%"><input type="text" name="actividades" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="25%"><!--<input type="text" name="tarea_visual" class="form-control form-control-sm">-->
                                                            <select name="tarea_visual" class="form-control form-control-sm">
                                                                <?php foreach ($puestostrabajo->result() as $itempt) { ?>
                                                                    <option value="<?php echo $itempt->id;?>" title="<?php echo $itempt->puesto_trabajo;?>"><?php echo $itempt->identificacion;?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                        <td class="tdinput" width="5%"></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-sm gradient-green-teal white pull-right add2_2"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        </div>
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep2_2">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                                <!--------------------------------------------------> 
                            </fieldset>
                            <h6>Paso 4</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_rec_ini">
                                        <input type="hidden" id="idri" name="idri" value="0">
                                        <input type="hidden" id="orden" name="orden" value="<?php echo $id_orden;?>">
                                        <input type="hidden" name="id_chs" value="<?php echo $id_chs;?>">
                                        <div class="row">
                                            <label class="col-md-3">Nombre del Documento</label>
                                            <div class="col-md-3">
                                                <input type="text" name="nom_documento" id="nom_documento" class="form-control" value="RECONOCIMIENTO INICIAL PARA ILUMINACION" readonly>
                                            </div>
                                            <label class="col-md-3">Identificación del documento</label>
                                            <div class="col-md-3">     
                                                <input type="text" name="iden_documento" id="iden_documento" class="form-control" value="REG-TEC/05-01">
                                            </div>
                                            <label class="col-md-3">Versión</label>
                                            <div class="col-md-3"> 
                                                <input type="number" name="version" id="version" class="form-control" value="1">
                                            </div>
                                            <label class="col-md-3">No copia controlada</label>
                                             <div class="col-md-3">
                                                 <input type="text" name="no_copia_controlada" id="no_copia_controlada" class="form-control" value="Original">
                                             </div>
                                        </div>   
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                    </div> 
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: end;">
                                            <a type="button" class="btn btn-info mr-1 mb-1" onclick="adddetalleri(0)" >Agregar detalle</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="overflow:auto;">
                                            <table class="table table-bordered" id="table_evaluar" align="center">
                                                <thead>
                                                        <th colspan="10">DETERMINACIÓN DE PUNTOS A EVALUAR</th>
                                                    <tr>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="3">No.</th>
                                                        <th colspan="6">INDICE DE AREA</th>
                                                        <th rowspan="3">PUESTO DE TRABAJO</th>
                                                        <th rowspan="3">No. DE PUNTOS A MEDIR</th>
                                                        <th rowspan="3"></th>
                                                    </tr>
                                                    <tr>
                                                        <th rowspan="2">LARGO(m)</th>
                                                        <th rowspan="2">ANCHO(m)</th>
                                                        <th rowspan="2">ALTURA(m)</th>
                                                        <th rowspan="2">IC</th>
                                                        <th colspan="2">ZONAS A EVALUAR</th>
                                                    </tr>
                                                    <tr>
                                                        <th>MINIMO</th>
                                                        <th>POR LIMITACION DEL AREA</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="table_evaluar_tbody">
                                                    
                                                </tbody>
                                            </table>        
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label>Estrategia de medición y criterios para la determinación de puntos</label>
                                            <textarea rows="6" class="form-control" id="estrategia" name="estrategia"></textarea>      
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn-info mr-1 mb-1 savereconocimientoini" onclick="savereconocimientoini()" >Guardar</button>
                                        </div>
                                    </div>
                                    
                                <!-------------------------------------------------->
                            </fieldset>
                            <h6>Paso 5</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <table id="table_det_puesto_area" class="table table-bordered table-responsive" width="100%">
                                        <thead style="text-align:center">
                                            <tr>
                                                <th rowspan="2">No.</th>
                                                <th rowspan="2">ÁREA</th>
                                                <th rowspan="2">INDICE DE ÁREA</th>
                                                <th rowspan="2">PUESTO DE TRABAJO</th>
                                                <th rowspan="2">PASILLO Y/O ESCALERAS</th>
                                            </tr>
                                        </thead>
                                        <tbody class="trbody" id="cont_tabla_det_puesto_area">
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep5">Guardar</button>
                                        </div>
                                    </div>
                                <!-------------------------------------------------->
                            </fieldset>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    var puestostrabajo=[];
    <?php foreach ($puestostrabajo->result() as $itempt) { ?>
        puestostrabajo.push({'id':<?php echo $itempt->id;?>,'identificacion':'<?php echo $itempt->identificacion;?>','puesto_trabajo':'<?php echo $itempt->puesto_trabajo;?>'}); 
    <?php } ?>;
</script>