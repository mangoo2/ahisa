<input type="hidden" id="id_orden" value="<?php echo $id_orden;?>">
<style type="text/css">
    .titulos{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .titulosC{
        background: #E9ECEF;
        font-weight: bold;
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
    }
    .titulos-12{
        font-size: 10px;
    }
    .camposi input{
        border: 0px !important;
        border-radius: 0px !important;
        font-size: 12px;
        padding-left: 3px;
        padding-right: 3px;
    }
    .camposi{
        padding: 0px !important;
    }
    .padding0{
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    thead{
       font-size: 11px !important; 
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(2.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
    @media (max-width:500px){
        .content-header{
            margin-top: 5px;
            margin-bottom: 5px !important;
            text-align: center;
        }
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 9px !important; 
            min-width: 45px !important;  
        }
        .chk_form {
            margin-top: 15px !important;
            transform: scale(1.3);
        }
        .chk_form2 {
            margin-top: 5px;
            transform: scale(1.5);
        }
    }

    @media (max-width:680px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 10px !important;
            min-width: 55px !important;  
        }
        .chk_form {
            margin-top: 30%;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            transform: scale(1.5);
        }
    }
    @media (max-width:880px){
        .reco1{
            margin-top: 0px;
            font-size: 9px !important;
        }
        input{
            margin-top: 0px;
            font-size: 11px !important;
            min-width: 65px !important;  
        }
        .chk_form {
            margin-top: 30%;
            margin-left: -7px;
            transform: scale(1.5);
        }
        .chk_form2 {
            margin-top: 12%;
            margin-left: -7px;
            transform: scale(1.5);
        }
    }
    #table_evaluar th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    #table_det th{
        vertical-align: middle;
        text-align: center;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    .tdinput{
            padding: 0px !important;
        }
    .tdinput input {    
        min-width: 62px;
    }
    @media (max-width:500px){
        .card .card-block {
            padding: 3px 3px 3px;
        }
        .content-wrapper {
            padding: 0 10px;
        }
        .tdinput{
            padding: 0px !important;
        }
        .tdinput input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
            min-width: 50px;
        }
        #table_evaluar th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        #table_det th{
            padding: 5px;
            font-size: 10px;
            line-height: normal;
        }
        .divtablecaptura{
            padding: 0px;
        }
        .card{
            margin-top: 0px;
        }
        .wizard > .steps > ul > li {
            width: 25% !important;
        }
        fieldset{
            padding: 0px !important;
        }
        .form input {
            border-radius: 2px !important;
            padding: 0.5rem 4px;
            font-size: 12px;
        }
    }
    .chk_form {
        margin-top: 35%;
        transform: scale(2.5);
    }
    .chk_form2 {
        margin-top: 15%;
        transform: scale(2.5);
    }
</style>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reconocimiento NOM-011</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="icons-tab-steps" id="icons-tab-steps">
                            <h6>Paso 1</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_reco1">
                                        <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
                                        <input type="hidden" name="id_orden" value="<?php echo $orden;?>">
                                        <input type="hidden" name="id_chs" value="<?php echo $id_chs;?>">
                                        <input type="hidden" id="nom" value="11">
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                No. de Informe
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" name="num_informe_rec" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->num_informe_rec;?>">
                                            </div>
                                            <div class="col-md-2 titulos">
                                                Fecha
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" name="fecha" class="form-control form-control-sm" value="<?php if($id==0) echo date("Y-m-d"); else echo $cli->fecha?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Razón Social
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="cliente" class="form-control form-control-sm" value="<?php if($id==0)  $cli->razon_social; else echo $cli->cliente;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Calle y número
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="calle_num" class="form-control form-control-sm" value="<?php if($id==0) $cli->calle." ".$cli->no_ext.' No. Int '.$cli->no_int; else echo $cli->calle_num;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Colonia
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="colonia" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->colonia;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Municipio
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="poblacion" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->poblacion;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Estado
                                            </div>
                                            <div class="col-md-3">
                                                <!--<input type="text" name="estado" class="form-control form-control-sm" value="<?php if($id==0) echo $edo; else echo $cli->estado;?>">-->
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <select name="estado" class="form-control form-control-sm" >
                                                            <?php
                                                            foreach ($estados as $e) {
                                                                $sel="";
                                                                /*if($id==0 && $e->estado==$edo || $id==0 && $edo==$cli->estado || $id>0 && mb_strtoupper($e->estado,"UTF-8")==mb_strtoupper($cli->estado,"UTF-8")){
                                                                    $sel="selected"; 
                                                                }*/
                                                                if($e->id==$cli->estado){
                                                                    $sel="selected"; 
                                                                }
                                                                //echo "<option ".$sel." value='".mb_strtoupper($e->estado,"UTF-8")."'>$e->estado</option>";
                                                                echo "<option ".$sel." value='".$e->id."'>".$e->estado."</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Código Postal
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="cp" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->cp;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                RFC
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="rfc" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->rfc;?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Giro de la Empresa
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="giro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->giro;?>">
                                            </div>
                                            <div class="col-md-3 titulos">
                                                Teléfono
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" name="telefono" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->telefono; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 titulos">
                                                Representante Legal
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="representa" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->representa;?>">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 titulos">
                                                Nombre / Cargo a quien se dirigirá el informe de resultados
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="nom_cargo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->nom_cargo;?>">
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><br></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulos" style="text-align:center;">TURNOS DE TRABAJO</div>
                                        </div>
                                        <div class="row">
                                            <table class="table">
                                                <thead>
                                                    <tr style="font-size: 12pt; color: black; font-weight: bold;">
                                                        <td width="30%" class="titulosC">TURNOS</td>
                                                        <td width="35%" class="titulosC">HORARIOS</td>
                                                        <td width="35%" class="titulosC">TIEMPO DE COMIDA (min)</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="titulosC">1er</td>
                                                        <td><input type="text" name="priero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->priero; ?>"></td>
                                                        <td><input type="text" name="pero_comida" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->pero_comida; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titulosC">2do</td>
                                                        <td><input type="text" name="segdo" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->segdo; ?>"></td>
                                                        <td><input type="text" name="sdo_comida" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->sdo_comida; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titulosC">3er</td>
                                                        <td><input type="text" name="tercero" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->tercero; ?>"></td>
                                                        <td><input type="text" name="tero_comida" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->tero_comida; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titulosC">ADMINISTRATIVO</td>
                                                        <td><input type="text" name="hora_admin" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->hora_admin; ?>"></td>
                                                        <td><input type="text" name="adm_comida" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->adm_comida; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titulosC">OTRO</td>
                                                        <td><input type="text" name="hora_otro" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->hora_otro; ?>"></td>
                                                        <td><input type="text" name="otro_comida" class="form-control form-control-sm" value="<?php if($id>0) echo $cli->otro_comida; ?>"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 titulosC">
                                                DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN
                                            </div>
                                            <div class="col-md-12 titulos">
                                                <textarea name="desc_proceso" class="form-control" rows="3" value="<?php if($id>0) echo $cli->desc_proceso; ?>"><?php if($id>0) echo $cli->desc_proceso; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8 titulosC">
                                                PLAN DE MANTENIMIENTO DE MAQUINARIA Y EQUIPO GENERADORES DE RUIDO
                                            </div>
                                            <div class="col-md-4 titulosC">
                                                REGISTROS DE PRODUCCIÓN
                                            </div>
                                            <div class="col-md-8 titulos">
                                                <textarea name="plan_mante" class="form-control" rows="3" value="<?php if($id>0) echo $cli->plan_mante; ?>"><?php if($id>0) echo $cli->plan_mante; ?></textarea>
                                            </div>
                                            <div class="col-md-4 titulos">
                                                <textarea name="registros_prod" class="form-control" rows="3" value="<?php if($id>0) echo $cli->registros_prod; ?>"><?php if($id>0) echo $cli->registros_prod; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;"><br></div>
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn btn-success" id="savep1">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <h6>Paso 2</h6>
                            <fieldset style="padding: 0px;">
                                <!-------------------------------------------------->
                                <form class="form" id="form_reco2">
                                    <input type="hidden" id="id_reconocimiento" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                    <input type="hidden" id="norma" value="<?php echo $norma; ?>">
                                    <input type="hidden" id="id_reco_paso2" value="<?php echo $id_reco_paso2; ?>">
                                    <div class="col-md-12 reco1 table-responsive" style="padding: 0px;">
                                        <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        <table id="table_det" class="table table-bordered table-responsive" width="100%">
                                            <thead style="text-align:center">
                                                <tr>
                                                    <th width="10%">No.</th>
                                                    <th width="20%">NOMBRE DEL ÁREA</th>
                                                    <th width="20%">PUESTO DE TRABAJO</th>
                                                    <th width="10%">No. DE TRABAJADORES</th>
                                                    <th width="25%">DESCRIPCIÓN DE ACTIVIDADES</th>
                                                    <th width="10%">TIEMPO DE EXPOSICIÓN REAL AL RUIDO (h)</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="trbody" id="cont_tabla_det">
                                                <?php if($id_reco_paso2==0){ ?>
                                                    <tr>
                                                        <td class="tdinput" width="10%">
                                                            <input type="hidden" name="id" class="form-control form-control-sm" value="0">
                                                            <input type="text" name="num" class="form-control form-control-sm" value="1"></td>
                                                        <td class="tdinput" width="20%"><input type="text" name="area" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="20%"><input type="text" name="puesto" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="10%"><input type="text" name="num_trabaja" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="25%"><input type="text" name="descrip" class="form-control form-control-sm"> </td>
                                                        <td class="tdinput" width="10%"><input type="text" name="tiempo_expo" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" width="5%"></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-sm gradient-green-teal white pull-right add"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        </div>
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep2">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <h6>Paso 3</h6>
                            <fieldset>
                               <!-------------------------------------------------->
                                <form class="form" id="form_reco2_2">
                                    <input type="hidden" id="id_reconocimiento2" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                    <input type="hidden" id="norma2" value="<?php echo $norma; ?>">
                                    <input type="hidden" id="id_reco_paso2_2" value="<?php echo $id_reco_paso2_2; ?>">
                                    <div class="col-md-12 reco1 table-responsive" style="padding: 0px;">
                                        <button type="button" class="btn btn-sm gradient-green-teal white pull-right add2_2"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        <table id="table_det2" class="table table-bordered table-responsive" width="100%">
                                            <thead style="text-align:center">
                                                <tr>
                                                    <th width="5%" rowspan="2">No.</th>
                                                    <th width="15%" rowspan="2">FUENTES GENERADORAS DE RUIDO</th>
                                                    <th width="5%" colspan="2">NIVEL SONORO ENCONTRADO</th>
                                                    <th width="5%" rowspan="2">TIPO DE RUIDO (E, I, IM)</th>
                                                    <th width="5%" rowspan="2">No. DE TRABAJADORES FIJOS</th>
                                                    <th width="5%" rowspan="2">No. DE TRABAJADORES MÓVILES</th>
                                                    <th width="55%" colspan="4">NÚMERO DE PUNTOS A MUESTRA POR MÉTODO DE EVALUACIÓN</th>
                                                    <th width="5%"></th>
                                                </tr>
                                                <tr>
                                                    <th>Min. dB(A)</th>
                                                    <th>Max. dB(A)</th>
                                          
                                                    <th>GRADIENTE DE PRESIÓN SONORA</th>
                                                    <th>PUESTO FIJO DE TRABAJO</th>
                                                    <th>PRIORIDAD DE ÁREAS DE EVALUACIÓN</th>
                                                    <th>PERSONAL</th>
                                                    <th ></th>
                                                </tr>
                                            </thead>
                                            <tbody class="trbody" id="cont_tabla_det_2">
                                                <?php if($id_reco_paso2_2==0 ){ ?>
                                                    <tr>
                                                        <td class="tdinput" >
                                                            <input type="hidden" name="id" class="form-control form-control-sm" value="0">
                                                            <input type="text" name="num" class="form-control form-control-sm" value="1">
                                                        </td>
                                                        <td class="tdinput"><input type="text" name="fuentes_genera" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="min_db" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" ><input type="text" name="max_db" class="form-control form-control-sm"></td>
                                                        <td class="tdinput" ><input type="text" name="tipo_ruido" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="num_trabaja_fijo" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="num_trabaja_mov" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="gradiente" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="puesto_fijo" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="prioridad" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"><input type="text" name="personal" class="form-control form-control-sm"></td>
                                                        <td class="tdinput"></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-sm gradient-green-teal white pull-right add2_2"><i class="fa fa-plus"></i> Agregar detalle</button>
                                        </div>
                                        <div class="col-md-12" style="text-align: end;">
                                            <button type="button" class="btn btn btn-success" id="savep2_2">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                                <!--------------------------------------------------> 
                            </fieldset>
                            <h6>Paso 4</h6>
                            <fieldset>
                                <!-------------------------------------------------->
                                    <form class="form" id="form_reco_4">
                                        <input type="hidden" id="id_reconocimiento4" name="id_reconocimiento" value="<?php echo $id_reco;?>">
                                        <input type="hidden" id="id_recp4" name="id" value="<?php if(isset($getp4)) echo $getp4->id; else echo '0' ?>">
                                        <div class="row">
                                            <label class="col-md-3 titulos">JUSTIFICACIÓN TÉCNICA PARA LA DETERMINACIÓN DE PUNTOS</label>
                                            <div class="col-md-9">
                                                <textarea type="text" rows="3" name="justificacion" id="justificacion" class="form-control"><?php if(isset($getp4)) echo $getp4->justificacion; ?></textarea>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <br>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 titulos">EQUIPO DE PROTECCIÓN PERSONAL UTILIZADO (MARCA, MODELO)</label>
                                            <div class="col-md-9">
                                                <textarea type="text" rows="3" name="equipo_protec" id="equipo_protec" class="form-control"><?php if(isset($getp4)) echo $getp4->equipo_protec; ?></textarea>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <br>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-12 titulosC">PLANO DE DISTRIBUCIÓN DE ÁREAS, MAQUINARIA, EQUIPO QUE GENEREN RUIDO Y UBICACIÓN DE LOS PUNTOS DE EVALUACIÓN</label>
                                            <div class="col-md-12">
                                                <textarea type="text" rows="10" name="plano_distri" id="plano_distri" class="form-control"><?php if(isset($getp4)) echo $getp4->plano_distri; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <br>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-12 titulosC">OBSERVACIONES</label>
                                            <div class="col-md-12">
                                                <textarea type="text" rows="3" name="observaciones" id="observaciones" class="form-control"><?php if(isset($getp4)) echo $getp4->observaciones; ?></textarea>
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: end;">
                                                <br>
                                            </div>
                                            <div class="col-md-12" style="text-align: end;">
                                                <button type="button" class="btn btn btn-success" onclick="guardarp4()" >Guardar</button>
                                            </div>
                                        </div> 
                                    </form>
                                <!-------------------------------------------------->
                            </fieldset>
                            <!--<h6>Paso 4</h6>
                            <fieldset>-->
                                <!-------------------------------------------------->
                                    <!--<div class="col-md-12" style="text-align: center;">
                                        <a href="<?php echo base_url().'Nom/lentamientoServicio/0/2'; ?>" type="button" class="btn btn btn-success">Levantamiento de Información</a>
                                    </div>-->
                                <!-------------------------------------------------->
                            <!--</fieldset>-->
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
