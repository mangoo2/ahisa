<?php

    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $margin_left=15;
    $margin_top=27;
    $margin_right=15;
    function mes($m){
        $mes="enero";
        switch ($m) {
            case 2: $mes="febrero"; break;
            case 3: $mes="marzo"; break;
            case 4: $mes="abril"; break;
            case 5: $mes="mayo"; break;
            case 6: $mes="junio"; break;
            case 7: $mes="julio"; break;
            case 8: $mes="agosto"; break;
            case 9: $mes="septiembre"; break;
            case 10: $mes="octubre"; break;
            case 11: $mes="noviembre"; break;
            case 12: $mes="diciembre"; break;
        }
        return $mes;
    }

    $time = strtotime($cotizacion->fecha_creacion);
    $dia=date('d',$time);
    $mes=mes(date('m',$time));
    $anio=date('Y',$time);

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logo.jpg';
        $html = '<table border="1" cellpadding="5" align="center" style="border-collapse: separate; font-size:9px;">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.$logos.'" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>MEDICIÓN DE CAMPO PARA LA MEDICIÓN DE LA ILUMINACIÓN</td>
                <td>REG-TEC/05-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }

}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('cotizacion');
$pdf->SetTitle('cotizacion');
$pdf->SetSubject('cotizacion');
$pdf->SetKeywords('cotizacion');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins($margin_left, $margin_top, $margin_right);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(25);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 25);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 12);
// add a page
$pdf->AddPage('P', 'A4');
$htmlg='';
$htmlg.='<style type="text/css">

<style type="text/css">
    .borderbottom{
        border-bottom:1px solid black;
    }
    .backg{
        background-color:#c6c9cb;
    }
    .txt_just{
        text-align: justify;
    }
</style>

<div style="font-size: 11px">
    <table width="100%">
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><span style="color: #002060">Puebla, Puebla a '.$dia.' de '.$mes.' del año '.$anio.'
                <br>Cotización No: 
                '.$cotizacion->id.'
                </span>
            </td>
        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td colspan="2" align="justify">Estimado <b>'.$contacto->nombre.'</b> en atención a su solicitud, se le hace llegar la presente cotización mencionando los servicios de su interés. Le agradecemos su confianza y estamos para atender cualquier tema
                relacionado con el presente documento.
            </td>
        </tr>
        <tr>
            <td align="left" style="color: #002060">
                <dl>
                    <dd><div><strong>'.$cotizacion->empresa.'</strong></div></dd>
                    <dd><img style="margin-top: 8px;" src="'.base_url().'public/img/marker.png" width="15px"><strong> '.$cotizacion->calle." ".$cotizacion->no_ext.', '.$cotizacion->no_int.'  
                        '.$cotizacion->colonia.',  
                        '.$cotizacion->poblacion.', '.$cotizacion->estado.', C.P. '.$cotizacion->cp.'
                        </strong>
                    </dd>
                    <dd><img style="margin-top: 8px;" src="'.base_url().'public/img/tel.png" width="15px"><strong> '.$contacto->telefono.'</strong></dd>
                    <dd><img style="margin-top: 8px;" src="'.base_url().'public/img/mail.png" width="15px"><strong> '.$contacto->email.'</strong></dd>
                </dl>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table  align="center" style="font-weight: bold; font-size:10px; font-family: Calibri; color: #002060; border: 1px solid black">
        <thead>
            <tr align="center">
                <td rowspan="2" width="10%">PARTIDA</td>
                <td rowspan="2" width="52%">DESCRIPCIÓN</td>
                <td rowspan="2" width="10%">CANT.</td>
                <td colspan="2" width="28%">COSTOS</td>
            </tr>
            <tr class="tr-sv">
                <td>UNITARIO</td>
                <td>TOTAL</td>
            </tr>
        </thead>
    </table>';
       
        $i=1; $total=0;
        foreach ($servicios as $s) {
            $htmlg.='<table border="1" align="center" style="font-family: Calibri; color: #002060; font-size: 9.5px !important;">
                <tr nobr="true" style="border: 1px solid black; white-space: pre" >
                    <td width="10%">'.$i.'</td>
                    <td width="52%" style="font-size: 9.5px; text-align: justify;"> 
                        '.$s->nombre.'<br>
                        '.$s->descripcion2.'
                    </td>
                    <td width="10%">'.$s->cantidad.'</td>
                    <td width="14%">$ '.number_format($s->precio,2).'</td>
                    <td width="14%">$ '.number_format($s->precio*$s->cantidad,2) .'</td>
                </tr>
            </table>';
            $total+=$s->precio*$s->cantidad;
            $i++;
        }
        
        $htmlg.='<table style="color:#002060" nobr="true" align="center">
            <tr>
                <td width="72%"></td>
                <td border="1" width="14%" class="tr-sv">SUBTOTAL</td>
                <td border="1" width="14%" class="tr-sv">$ '.number_format($cotizacion->subtotal,2).'</td>
            </tr>
            <tr>
                <td width="72%"></td>
                <td border="1" width="14%" class="tr-sv">IVA</td>
                <td border="1" width="14%" class="tr-sv">$ '.number_format($cotizacion->subtotal*.16,2).'</td>
            </tr>
            <tr>
                <td width="72%"></td>
                <td border="1" width="14%" class="tr-sv">TOTAL</td>
                <td border="1" width="14%" class="tr-sv">$ '.number_format($cotizacion->subtotal*1.16,2).'</td>
            </tr>
        </table>';
        if($cotizacion->descuento!=0){ 
            $desc_cantp = $cotizacion->subtotal*$cotizacion->descuento/100;
            $desc_cant = $cotizacion->subtotal - $desc_cantp;
            $htmlg.='<table border="1" nobr="true" align="center">
                <tr>
                    <td width="10%"></td>
                    <td width="52%">Descuento sobre el total<br>*puede no aplicar sobre todos los servicios</td>
                    <td width="10%"></td>
                    <td width="14%">% '.$cotizacion->descuento.'</td>
                    <td width="14%">$ '.number_format($desc_cantp,2).'</td>
                </tr>
                 <tr>
                    <td colspan="3"></td><td class="tr-sv">CON DESCUENTO</td><td class="tr-sv">$ '.number_format($desc_cant,2).'</td>
                </tr>
            </table>';    
        }
    $htmlg.='<br><br>
    <p style="text-align: justify;">Esta cotización considera el alcance de lo mencionado acordado entre el solicitante y Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.; al ser aceptada y autorizada por ambas partes se considera como un contrato legal en los términos que apliquen según la ley mercantil y sus reglamentos.</p>
    <table align="center">
        <tr nobr="true" class="tr-sv"><td style="font-weight: bold; color:#002060">Acuerdos Comerciales</td></tr>
        <tr><td><br></td></tr>
        <tr nobr="true">
            <td style="text-align: justify;"><ol>
                    <li>La forma de pago será en común acuerdo con el cliente.</li>
                    <li>La presente cotización tiene una vigencia de 90 días naturales.</li>
                    <li style="text-align: justify;">El tiempo de entrega de los resultados será de 20 días hábiles, contados a partir de que el cliente proporcione toda la información correspondiente que le compete respecto al servicio solicitado.</li>
                    <li style="text-align: justify;">Si se realizará un viaje en falso, es decir, que por circunstancias ajenas a Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. no se pueda realizar el servicio, se cobrará al solicitante de esta cotización un monto $5,000.00 MXN.</li>
                    <li style="text-align: justify;">Si al momento de realizar el servicio no se proporciona información correspondiente al cliente (programas de mantenimiento, información técnico-administrativa, etc.) éste contará con hasta 5 días hábiles posteriores a la conclusión del servicio en campo para entregar dicha información.</li>
                </ol>
            </td>
        </tr>
    </table>
    <table>
        <tr><td></td></tr>
    </table>
    <table align="center">
        <tr nobr="true" class="tr-sv"><td style="font-weight: bold; color:#002060">Evaluación de la Capacidad Técnica</td></tr>
        <tr><td><br></td></tr>
        <tr nobr="true">
            <td style="text-align: justify;"><ol>
                    <li>¿Se cuenta con la acreditación y aprobación vigente de los métodos solicitados?
                    <br>Si No</li>
                    <li>¿Los equipos y patrones involucrados en el servicio cuentan con calibración vigente?   
                    <br>Si No</li>
                    <li>¿Los métodos solicitados por el cliente son los adecuados para cubrir sus necesidades?
                    <br>Si No</li>
                    <li>¿Se cuenta con personal calificado para desarrollar el servicio?   
                    <br>Si No</li>
                    <li>¿La capacidad instalada no se ve comprometida con este servicio?  
                    <br>Si No</li>
                </ol>
            </td>
        </tr>
    </table>
</div>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->Output('cotizacion.pdf', 'I');
//$pdf->Output(FCPATH . 'mail/cotizacion_'.$cotizacion->id.'.pdf', 'FI');