<?php
$color = "#CCFF99";
function mes($m){
    $mes="enero";
    switch ($m) {
        case 2: $mes="febrero"; break;
        case 3: $mes="marzo"; break;
        case 4: $mes="abril"; break;
        case 5: $mes="mayo"; break;
        case 6: $mes="junio"; break;
        case 7: $mes="julio"; break;
        case 8: $mes="agosto"; break;
        case 9: $mes="septiembre"; break;
        case 10: $mes="octubre"; break;
        case 11: $mes="noviembre"; break;
        case 12: $mes="diciembre"; break;
    }
    return $mes;
}

$time = strtotime($cotizacion->fecha_creacion);

$dia=date('d',$time);
$mes=mes(date('m',$time));
$anio=date('Y',$time);

$logo =  base_url()."app-assets/img/ahisa.png";
$name_firma ="Grupo Ahisa"; 
$razon = "AHISA Laboratorio de Pruebas S. de R.L. de C.V.";
$color = "background-color: #e86300";

?>

<style>
    .tr-sv {
        color: white;
        background-color: #404244;
    }
    .tr-sv2 {
        background-color: #13bf0d;
    }

    .tr-svAh {
        background-color: #e86300;
    }
    .tr-svAu {
        background-color: #0a3971;
    }
    
    .firma{
        width: 10%;
        margin-top: 100px;
        
        margin-left: 25%;
    }
</style>


<div style="font-size: 11px">
    <table>
        <tr>
            <td width="30%" align="center"><img height="60px" src="<?php echo $logo;?>"><br></td>
            <td width="70%" align="right" style="font-size: 12px"><br><br>
                <strong><?php echo $razon; ?></strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="font-size: 14px"><br><strong>COTIZACIÓN DE SERVICIO</strong></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><strong>Fecha: <?php echo $dia." de ".$mes." de ".$anio; ?>
                    <br>Folio Cotización: 
                    <?php echo $cotizacion->folio; ?> 
                    <br>No.Cotización: 
                    <?php echo $cotizacion->id; ?>
                    </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left"><strong>
                <br><br><?php echo $cotizacion->empresa; ?>
                <br><br><?php echo $cotizacion->calle." ".$cotizacion->no_ext; ?>,  
                <br><?php echo $cotizacion->colonia; ?>,  
                <br><?php echo $cotizacion->poblacion; ?>, <?php echo $cotizacion->estado; ?>, C.P. <?php echo $cotizacion->cp; ?> 
                <br><br>AT´N: <?php echo $contacto->nombre; ?> / <?php echo strtoupper($contacto->puesto); ?> 
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="justify">
                <br><br>En atención a su amable solicitud de cotización, le presento nuestra propuesta técnico-económica, conforme a los lineamientos establecidos por las Normas Oficiales Mexicanas correspondientes. Quedamos a sus órdenes para cualquier duda o aclaración, esperando ser favorecidos con su elección. 
                <br><br><strong>Objetivo.-</strong> Cumplir las medidas Ambientales y de Seguridad e Higiene que establecen las Normas Mexicanas para los centros de trabajo, a fin de proveer un ambiente seguro y saludable en la realización de las tareas que desarrollen los trabajadores. 
                <br><br><strong>Beneficio.-</strong> El cliente estará dando cumplimiento a sus obligaciones en materia de Seguridad Industrial, así como a los requerimientos que establecen las diferentes dependencias. 
            </td>
        </tr>
    </table>
    <br><br><br>
    <table  align="center" nobr="true" style="border: 1px solid black">
        <tr align="center" valign="middle" class="tr-sv">
            <td rowspan="2" width="10%">PARTIDA</td>
            <td rowspan="2" width="52%">DESCRIPCIÓN</td>
            <td rowspan="2" width="10%">CANT.</td>
            <td colspan="2" width="28%">COSTOS</td>
        </tr>
        <tr class="tr-sv">
            <td>UNITARIO</td>
            <td>TOTAL</td>
        </tr>
    </table>
        <?php 
        $i=1; $total=0;




        $vowels = array(
                     '<div style="text-align: center;"><span style="mso-bidi-font-size:12.0pt;line-height:115%;font-family:" century="" gothic",sans-serif;="" mso-bidi-font-family:arial"=""><font face="Helvetica"></font></span></div>',
                    '<div><div><span style="font-family:" century="" gothic",sans-serif;mso-bidi-font-family:arial"=""><font face="Arial"></font></span></div></div>',
                    '<div><font face="Arial"><span style="font-size:14px;"></span></font><div><span style="font-size: 9pt; letter-spacing: 0.3px;"><font face="Arial Black"></font></span></div><div><span style="font-family: Arial, sans-serif; font-size: 9pt; letter-spacing: 0.3px;"><br></span></div><div><span lang="EN-US"><o:p></o:p></span></div></div>',
                    '<div style="font-size: 14px; text-align: left;"><span style="font-size: 10.5pt;"><span style="font-size:12px;"><font face="Arial"><br></font></span></span></div>',
                    '<div><font face="Arial"><span style="color: rgb(0, 0, 0); font-size: medium;"><b></b></span></font></div>',
                    '<div><span style="font-size:10.0pt;line-height:107%;font-family:
" century="" gothic",sans-serif"=""><font face="Arial"></font></span></div>',
                    '<span style="font-size:12px;"></span>',
                    '<span lang="EN-US"><o:p></o:p></span>',
                    
                    '<font face="Arial"><br></font>',
                    '<span style="font-size: 10.5pt;"><b><br></b></span>',
                    '<div style="font-size: 14px; text-align: left;"><br></div>',
                    '<div style="font-size: 14px; text-align: left;"><br></div><div style="font-size: 14px;"><span style="font-size: 10.5pt;"><b><br></b></span></div></div>',
                    '<div style="font-size: 14px;"></div>',
                    '<span style="font-family: Arial; font-size: 12px;"><br></span>',
                    '<span style="font-size: 12px;"><br></span>',
                    '<span style="font-family:" century="" gothic",sans-serif;="" mso-bidi-font-family:arial"=""><font face="Tahoma"></font></span>',
                    '<span style="font-size:10.0pt;line-height:107%;font-family:
" century="" gothic",sans-serif"=""><b><font face="Arial"></font></b></span>',
                    '<span style="font-size:12px;"><br></span>',
                    '<div><br></div>',
                    '<div><div></div></div>',
                    '<div></div>',
                    '<b></b>',
                    '<div><br></div>',
                );
        $rowfila=1;
        foreach ($servicios as $s) { 
            if($rowfila>1){
                $nobrrow='nobr="true"';
            }else{
                $nobrrow='';
            }
            ?>
            <table border="1" <?php echo $nobrrow;?> align="center" style="font-size: 10.5px !important;">
                <tr nobr='true' style="border: 1px solid black; white-space: pre" >
                    <td width="10%"><?php echo $i ?></td>
                    <td width="52%" style="font-size: 9.5px; text-align: left; text-align: justify;">
                        <?php 
                            str_replace("<br>","", $s->nombre2);
                            str_replace("<div>","", $s->descripcion2); str_replace("</div>","", $s->descripcion2);
                            str_replace("<br>","", $s->descripcion2); str_replace("<p>","", $s->descripcion2);
                            echo '<FONT FACE="arial">'.preg_replace("/[\r\n|\n|\r]+/","", $s->nombre2).".".preg_replace("/[\r\n|\n|\r]+/","", $s->descripcion2).'</FONT>';
                        ?>
                            
                    </td>
                    <td width="10%"><?php echo $s->cantidad; ?></td>
                    <td width="14%">$ <?php echo number_format($s->precio,2); ?></td>
                    <td width="14%">$ <?php echo number_format($s->precio*$s->cantidad,2) ?></td>
                </tr>
            </table>
            <?php 
            $total+=$s->precio*$s->cantidad;
            $i++;
            $rowfila++;
        }
        ?> 
        <table border="1" nobr="true" align="center">
        <tr>
            <td width="70%"></td>
            <td width="15%" class="tr-sv">TOTAL</td>
            <td width="15%" class="tr-sv">$<?php echo number_format($cotizacion->subtotal,2); ?></td>
        </tr>
        </table>
        <?php if($cotizacion->descuento!=0){ 
            $desc_cantp = $cotizacion->subtotal*$cotizacion->descuento/100;
            $desc_cant = $cotizacion->subtotal - $desc_cantp; ?>
            <table border="1" nobr="true" align="center">
                <tr>
                    <td width="10%"></td>
                    <td  width="50%">Descuento sobre el total<br>*puede no aplicar sobre todos los servicios</td>
                    <td width="10%"></td>
                    <td width="15%">%<?php echo $cotizacion->descuento; ?></td>
                    <td width="15%">$<?php echo number_format($desc_cantp,2);?></td>
                </tr>
                 <tr>
                    <td colspan="3"></td><td class="tr-sv">CON DESCUENTO</td><td class="tr-sv">$<?php echo number_format($desc_cant,2); ?></td>
                </tr>
            </table>
        <?php
            
        }
        
        ?>

       
    
    <!--
    <table border="1" nobr="true">
        <tr>
            <td width="10%"></td>
            <td width="40%"></td>
            <td width="10%"></td>
            <td width="20%"></td>
            <td width="20%"></td>
        </tr>
    </table>-->
    <br><br>
    <h3>Notas: Los precios NO incluyen el 16% de I.V.A</h3>
    <p><?php echo $cotizacion->observaciones; ?></p>
    <table align="center">
        <tr nobr='true' class="tr-sv"><td>1.Fecha de programación</td></tr>
        <tr nobr='true' style="<?php echo $color; ?>"><td>Máximo dos días habiles para dar respuesta desde que se otorga número de orden de trabajo</td></tr>
        <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
        
        <tr nobr='true' class="tr-sv"><td>2.Trabajo de campo</td></tr>
        <tr nobr='true' style="<?php echo $color; ?>"><td>Dependerá del alcance y/o magnitud de los trabajos</td></tr>
        <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
        
        <tr nobr='true' class="tr-sv"><td>3.Elaboración de carpeta de resultados</td></tr>
        <tr nobr='true' style="<?php echo $color; ?>"><td>20 a 35 días hábiles a partir del último día de trabajo de campo, siempre y cuando la informacíon solicitada al cliente esté completa, de los contrario contáran a partir de que este punto esté cubierto</td></tr>
        <tr nobr='true'><td><img height="20px" width="35px" src="/app-assets/img/down-arrow.png"></td></tr>
        
        <tr nobr='true' class="tr-sv"><td>4. Entrega de carpeta de resultados al cliente</td></tr>
        <tr nobr='true' style="<?php echo $color; ?>"><td>De 20 días hábiles</td></tr>
    </table>
    
    <p align="center">Nota: Los tiempos pueden variar con algunos proyectos, en ese caso se indicará desde la programación. <br> <?php echo $cotizacion->observaciones;?></p>
    <br>
    <h4>Forma de Pago:</h4>
    <p>
        <?php
            switch($cotizacion->forma_pago){
                case 1: echo "50% de anticipo y 50% contraentrega"; break;
                case 2: echo "Crédito 30 días"; break;
                case 3: echo "Crédito 60 días"; break;
                case 4: echo "100% contraentrega"; break;
                case 5: echo "En común acuerdo con el cliente"; break;
            }
        ?> 
    </p>
    <br><br>
    <h4>Aclaraciones generales:</h4>
    <ul>
        <li>Los precios NO incluyen el 16% de I.V.A.</li>
        <li>Después de los 20 días de la realización de los trabajos, si el cliente no proporciona
        la información y el servicio se demora, se enviará la factura correspondiente para
        su trámite de pago.</li>
        <li>Si se realizara un viaje en falso, es decir que el cliente no tenga las condiciones
        para la realización de los trabajos y estos no se puedan realizar, se hará un cobro de $ 5,000 por la visita.
        En caso de cancelaciones o cambios de fecha, estos deberán ser con 7 días hábiles de anticipación para que no se realice el cobro de viaje en falso.</li>
        <li>Cualquier cambio que se realice después de entregadas las carpetas y/o dictámenes, tendrá un costo adicional de $ 2,000.00</li>
        <li>La vigencia de la presente cotización es de 30 días naturales posteriores a la fecha de elaboración.</li>
        <li>En caso de aceptación de la propuesta favor de remitir a <?php echo $name_firma; ?> ya sea por medio electrónico o bien por medio físico, la presente cotización firmada, o bien orden de compra mediante la cual se consideran aceptados los términos y clausulas especificadas en la presente cotización.</li>
        <li>En caso de tener alguna queja o corrección sobre los servicios entregados, el responsable del proyecto por parte del cliente debe de realizar una carta membretada dirigida a: Área de Calidad de <?php echo $name_firma; ?>, explicando la situación del porqué de su inconformidad junto con la evidencia del hecho hasta 5 días hábiles después de la entrega de la documentación de los servicios, para que en caso de realizar algún cambio en su informe, dictamen o constancias de capacitaciones, se analice la situación y se lleven a cabo las acciones pertinentes.</li>
    </ul>
    <p align="justify">Sin otro particular, agradecemos la oportunidad que nos brindan de ofrecerles nuestra propuesta misma que esperamos cumpla con sus expectativas para la realización de este trabajo. </p>
    <br><br><br><br><br><br>
    <table align="center">
        <tr nobr='true'>
            <td width="5%"></td>
            <td width="42%"><br><br><br><br><strong>ATENTAMENTE</strong></td>
            <td width="6%"></td>
            <td width="42%"><br><br><br><br><strong>ACEPTO COTIZACIÓN</strong></td>
            <td width="5%"></td>
        </tr>
        <tr nobr='true'>
            <td></td>
            <td><br><br><p style="border-bottom: solid 2px black; "></p><br><strong><?php echo $cotizacion->vendedor;?><br><?php echo $name_firma; ?></strong></td>
            <td></td>
            <td><br><br><p style="border-bottom: solid 2px black; "></p><br><strong><?php echo $contacto->nombre."<br>".$cotizacion->empresa; ?></strong></td>
            <td></td>
        </tr>
    </table>
</div>