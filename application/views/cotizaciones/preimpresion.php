
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_cotizacion" method="post">
                            <input type="hidden" name="id" id="id" value="<?php if(isset($dets)) echo $dets->id; else echo 0; ?>">
                            <input type="hidden" name="id_cotizacion" id="id_cotizacion" value="<?php echo $id_cotizacion; ?>">
                            <div class="row mt-4">
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="fecha" value="<?php if(isset($dets)) echo $dets->fecha; else echo $fecha; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <p>Cotización:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" disabled type="text" name="cotizacion" value="<?php echo $cot->id; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <p>Empresa:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="empresa" value="<?php if(isset($dets)) echo $dets->empresa; else echo $cot->empresa; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="direccion" value="<?php if(isset($dets)) echo $dets->direccion; else echo $cot->direc_cli; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="edo_cp" value="<?php if(isset($dets)) echo $dets->edo_cp; else echo $cot->poblacion." C.P. ".$cot->cp; ?> ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="tel" value="<?php if(isset($dets)) echo $dets->tel; else echo "5529648218 Ext. 12" ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="email" name="mail" value="<?php if(isset($dets)) echo $dets->mail; else echo "gerencia@ahisa.mx" ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="atencion" value="<?php if(isset($dets)) echo $dets->atencion; else echo "A/A: Ing. Christian Uriel Fabian Romero" ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                        
                            <hr>
                            <p>Servicios añadidos a la cotización:</p>
                            <table class="table table-sm" id="tabla_servs">
                                <thead>
                                    <tr>
                                        <th>Partida</th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Precio/U</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="table_servicios">
                                    <?php $i=1;
                                    foreach($serv as $s){
                                        echo "<tr>
                                            <td>".$i."</td>
                                            <td>".$s->clave2." / ".$s->descripcion2."</td>
                                            <td>".$s->cantidad."</td>
                                            <td>$".number_format($s->precio,2)."</td>
                                            <td>$".number_format($s->precio*$s->cantidad,2)."</td>
                                        </tr>";
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-sm" id="tabla_eval">
                                <thead>
                                    <tr><th colspan="2">EVALUACIÓN DE LA CAPACIDAD TÉCNICA E IMPARCIALIDAD</th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1. ¿Se cuenta con la acreditación y aprobación vigente de la(s) norma(s) solicitada(s)? </td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg1==1) echo "checked"; ?> type="radio" id="preg1" name="preg1" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg1==2) echo "checked"; ?> type="radio" id="preg1_2" name="preg1" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>2. ¿Los equipos y patrones involucrados en el(los) servicio(s) cuentan con calibración vigente?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg2==1) echo "checked"; ?> type="radio" id="preg2" name="preg2" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg2==2) echo "checked"; ?> type="radio" id="preg2_2" name="preg2" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>3. ¿La(s) norma(s) solicitada(s) por el cliente es(son) la(s) adecuada(s) para cubrir sus necesidades?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg3==1) echo "checked"; ?> type="radio" id="preg3" name="preg3" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg3==2) echo "checked"; ?> type="radio" id="preg3_2" name="preg3" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>4. ¿Se cuenta con personal calificado para desarrollar el(los) servicio(s)?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg4==1) echo "checked"; ?> type="radio" id="preg4" name="preg4" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg4==2) echo "checked"; ?> type="radio" id="preg4_2" name="preg4" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>5. ¿La capacidad instalada del laboratorio no se ve comprometida con la realización del(los) servicio(s)?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg5==1) echo "checked"; ?> type="radio" id="preg5" name="preg5" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg5==2) echo "checked"; ?> type="radio" id="preg5_2" name="preg5" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>6. ¿Se identifican riesgos a la imparcialidad en donde se ve comprometido el(los) servicio(s)?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg6==1) echo "checked"; ?> type="radio" id="preg6" name="preg6" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg6==2) echo "checked"; ?> type="radio" id="preg6_2" name="preg6" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>7. ¿Se identifican algún conflicto de intereses para la realización del(los) servicio(s)?</td>
                                        <th>
                                            <label>SI
                                                <input <?php if(isset($dets) && $dets->preg7==1) echo "checked"; ?> type="radio" id="preg7" name="preg7" class="form-control form-control-sm chk_form">
                                            </label>
                                            <label>NO
                                                <input <?php if(isset($dets) && $dets->preg7==2) echo "checked"; ?> type="radio" id="preg7_2" name="preg7" class="form-control form-control-sm chk_form">
                                            </label>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <table class="table table-sm" id="tabla_decla">
                                <thead>
                                    <tr><th colspan="2">DECLARACIÓN DE CONFORMIDAD</th></tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="2">Los resultados que se derivan de los servicios que Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. son comparados contra niveles permisibles establecidos en Normas Oficiales Mexicanas (NOM´s), por lo que para los servicios solicitados en la presente cotización se realizará una declaración de conformidad con las siguientes normas:</td></tr>
                                    <tr>
                                        <td>NOM-011-STPS-2001 / Condiciones de seguridad e higiene en los centros de trabajo donde se genere ruido</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->declara1==1) echo "checked"; ?> id="declara1" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>NOM-015-STPS-2001 / Condiciones térmicas elevadas o abatidas - Condiciones de seguridad e higiene</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->declara2==1) echo "checked"; ?> id="declara2" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>NOM-022-STPS-2015 / Electricidad estática en los centros de trabajo - Condiciones de seguridad</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->declara3==1) echo "checked"; ?> id="declara3" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>NOM-025-STPS-2008 / Condiciones de iluminación en los centros de trabajo</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->declara4==1) echo "checked"; ?> id="declara4" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>NOM-081-SEMARNAT-1994 / Que establece los límites máximos permisibles de emisión de ruido de las fuentes fijas y su método de medición</td>
                                        <th>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->declara5==1) echo "checked"; ?> id="declara5" type="checkbox">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-sm" id="tabla_pays">
                                <thead>
                                    <tr><th colspan="8">CONDICIONES DE PAGO</th></tr>
                                </thead>
                                <tbody>
                                    <tr><td colspan="8">Plazo de pago. Se acordará con el cliente el plazo de pago a otorgar, pudiendo ser:</td></tr>
                                    <tr>
                                        <td>50% de anticipo, 50% contra entrega de servicio(s)</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->metodo1==1) echo "checked"; ?> id="metodo1" type="checkbox" onclick="checkSlidersPago(this, 1)">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Pago contra entrega de servicio(s)</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->metodo2==1) echo "checked"; ?> id="metodo2" type="checkbox" onclick="checkSlidersPago(this, 2)">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Pago anticipado del 100% por servicio(s)</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->metodo3==1) echo "checked"; ?> id="metodo3" type="checkbox" onclick="checkSlidersPago(this, 3)">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Otro</td>
                                        <td>
                                            <div class="col-md-6">
                                                <label class="switch">
                                                    <input <?php if(isset($dets) && $dets->metodo4==1) echo "checked"; ?> id="metodo4" type="checkbox" onclick="checkSlidersPago(this, 4)">
                                                    <span class="sliderN round"></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <br><hr>
                            <div class="row">
                                <button type="button" onclick="save_detalles()" class="btn btn-success shadow-z-1" style="width: 40%; margin-left: 30%;" id="save">Aceptar e imprimir <i class="ft-save"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>