<input type="hidden" id="perf" value="<?php echo $this->session->userdata("perfil"); ?>">
<input type="hidden" id="logeoEd" value="<?php echo $this->session->userdata("edicion"); ?>">

<?php
    $logueoCr = $this->session->userdata('creacion');
?>

<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Cotizaciones</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                       <?php //echo $this->session->userdata("perfil"); ?>
                        <h5><i class="ft-mail"></i> Listado de Cotizaciones</h5><hr>
                        <div class="col-md-3">
                            <label class="modal-title text-text-bold-600">Busqueda por estatus</label>
                            <div class="position-relative has-icon-right">
                                <select class="form-control" id="tipostock" name="tipostock">
                                    <option value="0">Todas:</option>
                                    <option value="1">Pendiente</option>
                                    <option value="2">Aceptada</option>
                                    <option value="3">Rechazada</option>
                                    <option value="4">Modificada</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                            <?php if($logueoCr){ ?>
                                <a href="<?php echo base_url(); ?>index.php/cotizaciones/nueva_cotizacion" class="btn btn-success  pull-right">Nueva Cotización</a>
                            <?php } ?>
                            </div>
                            
                        </div>
                        <div class="table-responsive" style="overflow: scroll;">
                            <table class="table table-striped" id="tabla">
                                <thead>
                                    <tr>
                                        <!--<th>Folio</th>-->
                                        <th>ID</th>
                                        <th>Cliente</th>
                                        <th>Fecha</th>
                                        <th>Importe</th>
                                        <th>Descuento%</th>
                                        <th>Forma de Pago</th>
                                        <th>Vendedor</th>
                                        <th>Estatus</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_productos" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Productos</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Descripción</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_resultados">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<div class="modal fade text-left" id="modal_pass" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Autorización de Modificación</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="id_cotiza_auto" class="form-control" >
                    <div class="col-md-8">
                        <div class="form-group">
                            <p>Contraseña: </p>
                            <div class="controls">
                                <input type="text" id="pass_insert" class="form-control" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="aceptar_pass"> Aceptar</button>
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
