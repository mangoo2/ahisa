
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">
                        <?php
                        $titulo = "Nueva";
                        if (isset($cotizacion)) {
                            $titulo = "Editar";
                        }
                        echo $titulo;
                        ?>
                        Cotización</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <form class="form" id="form_cotizacion" method="post">
                             <input type="text" value="<?php if($titulo=='Editar') echo 1; else echo 0;?> " name="tipo_cotiza" id="tipo_cotiza" class="form-control" hidden="">
                             <input type="hidden" id="id" value="<?php echo $id; ?>">
                             <input type="hidden" id="aux_desc" value="0">
                             <input type="hidden" id="aux_auto" value="<?php echo $aux_auto; ?>">
                             <input type="hidden" id="id_pass" value="0">
                             <input type="hidden" id="id_empresa" value="<?php echo $id_empresa; ?>">
                             <input type="hidden" id="sucursal" value="<?php echo $sucursal; ?>">
                            <!--<h4 class="form-section"><i class="ft-file-text"></i> Datos de Cotización</h4>-->
                            <div class="row mt-4">
                                <div class="col-8">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Empresa:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control select2" name="cliente" id="cliente">
                                                <?php
                                                    foreach ($clientes as $c) {
                                                        $sel="";
                                                        if(isset($cotizacion) && $cotizacion->cliente_id==$c->id){
                                                            $sel="selected";
                                                        }
                                                        echo "<option data-tipoc='$c->tipo_cli' value='$c->id' $sel>$c->empresa - $c->alias</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <p>Persona de Contacto:</p>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="contacto" id="contacto">
                                                <option value=''>Contacto:</option>
                                               <?php if($titulo=="Editar") echo "<option value='$persona_contac->id' selected>$persona_contac->nombre</option>"; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <h4 class="form-section"><i class="fa fa-plus"></i> Servicios</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <p>Tamaño </p>
                                        <select id="slc_tamaño" class="form-control form-control-sm"  >
                                            <option value="0">N/A</option>
                                            <option value="1">Chica</option>
                                            <option value="2">Mediana</option>
                                            <option value="3">Grande</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <p>Categoria </p>
                                        <select id="slc_familia" class="form-control form-control-sm"  >

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <p>Servicio </p>
                                        <select id="slc_servicio" class="form-control form-control-sm"  >

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" onclick="agregar_servicio()" class="btn btn-sm mt-4 btn-success" <?php /*if($this->session->userdata("administrador")){ echo "disabled"; }*/?> >Agregar <i class="ft-plus"></i></button>
                                </div>
                            </div>
                            <hr>
                            <p>Servicios añadidos a la cotización:</p>
                            <table class="table table-sm" id="tabla_servs">
                                <thead>
                                    <tr>
                                        <th style="display:none;">ID</th>
                                        <th>Partida</th>
                                        <th>Clave</th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Precio/U</th>
                                        <th>Precio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="table_servicios">
                                    <?php $i=1; $tot=0; $sub=0;
                                    if($id>0){
                                        foreach($servicios as $s){
                                            $btn = "<td><button type='button' onclick='elimina_servicioAsignado(".$s->id.")' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-minus'></i></button></td>";

                                            echo "<tr class=ord_".$s->id." id='prodid_".$s->id."'>
                                                <td style='display:none;'>".$s->id."</tD>
                                                <td>".$i."</td>
                                                <input type='hidden' id='id_cot_serv' value='".$s->id."'>
                                                <td><input class='id_serv_".$s->servicio_id."' id='id_serv_add' type='hidden' value='".$s->servicio_id."'>".$s->clave2."</td>
                                                <td>".$s->descripcion2."</td>
                                                <td><input type='hidden' data-ad='".$s->ad."' id='serv_id' value='".$s->servicio_id."'>
                                                    <input type='number' class='cant cant_".$s->servicio_id." form-control form-control-sm width-150' value='".$s->cantidad."'></td>
                                                <td><input type='hidden' class='precio precio_".$s->servicio_id." form-control form-control-sm' value='".$s->precio."'> $".number_format($s->precio,2)."</td>
                                                <td class='sub sub_".$s->servicio_id."' id='tot_".$id."'>$".number_format($s->precio*$s->cantidad,2)."</td>
                                                ".$btn."
                                            </tr>";
                                            $sub = ($s->precio*$s->cantidad);
                                            $i++;
                                        }
                                    }
                                    ?>
                                    <!--<tr type="hidden"><td><input type="hidden" id="tot" value="<?php echo $sub; ?>"></td></tr>-->
                                </tbody>
                            </table>
                            <input type="hidden" id="tot" value="<?php echo $sub; ?>">
                            <h4 class="form-section"><i class="fa fa-percent"></i> Descuentos</h4>
                            
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <p class="text-bold-600">Descuento especial:</p>
                                            <input  <?php if($this->session->userdata("id_usuario")>1){ echo "disabled"; }?> type="number" name="descuento" id="descuento" class="form-control" <?php if(isset($cotizacion)){ echo "value='$cotizacion->descuento'"; } ?>>
                                            <input type="hidden" id="desc_aplicado" value="0">
                                            <input type="hidden" id="desc_guarda" <?php if(isset($cotizacion)){ echo "value='$cotizacion->descuento'"; } else{ echo "value='0'"; } ?> >
                                        </div>
                                    </div>
                                    <?php if($this->session->userdata("perfil")==1 || $this->session->userdata("perfil")==2){ ?>
                                        <div class="col-md-2">
                                            <label style="color: transparent;">ingresar pass</label>
                                            <button title="Ingresar Contraseña para activar descuento" id="ingresar_pass" type="button" class="btn btn-success btn-icon"><i class="ft-gear"></i> Ingresar Contraseña</button>
                                        </div>
                                    <?php } ?>
                                </div>
                                
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Porcentaje Aplicado %</th>
                                        <th>Monto a Descontar $</th>
                                    </tr>
                                </thead>
                                <tbody id="table_descuentos">
                                    
                                </tbody>
                            </table>
                            
                            <h4 class="form-section"><i class="fa fa-credit-card"></i> Forma de pago</h4>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <p>Seleccione la forma de pago:</p>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" id="forma_pago" name="forma_pago">
                                        <option value="1" <?php if(isset($cotizacion) && $cotizacion->forma_pago=='1'){ echo "selected"; } ?> >50% de anticipo y 50% contraentrega</option>
                                        <option value="2"<?php if(isset($cotizacion) && $cotizacion->forma_pago=='2'){ echo "selected"; } ?>>Crédito 30 días</option>
                                        <option value="3" <?php if(isset($cotizacion) && $cotizacion->forma_pago=='3'){ echo "selected"; } ?>>Crédito 60 días</option>
                                        <option value="4" <?php if(isset($cotizacion) && $cotizacion->forma_pago=='4'){ echo "selected"; } ?>>100% contraentrega</option>
                                        <option value="5" <?php if(isset($cotizacion) && $cotizacion->forma_pago=='5'){ echo "selected"; } ?>>En común acuerdo con el cliente</option>
                                    </select>
                                </div>
                            </div>
                            <h4 class="form-section"><i class="fa fa-search"></i> Observaciones </h4>
                            
                            
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <p class="text-bold-600">Observaciones:</p>
                                        <textarea id="observaciones" class="form-control" rows="2" ><?php if(isset($cotizacion)){ echo $cotizacion->observaciones; }?></textarea>
                                    </div>
                                </div>
                            </div>
                            <br><hr>
                            <div class="row">
                                <button type="button" onclick="save_cotizacion($(this))" class="btn btn-success shadow-z-1" style="width: 40%; margin-left: 30%;">Guardar <i class="ft-save"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade text-left" id="modal_pass" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Activación de Descuento</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <p>Contraseña: </p>
                            <div class="controls">
                                <input type="text" id="pass_insert" class="form-control" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="aceptar_pass"> Aceptar</button>
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
