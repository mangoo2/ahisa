<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Listado de Ordenes de Compra</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-mail"></i> Listado de Ordenes de Compra</h5><hr>
                        <div style="overflow: scroll;">
                            <table class="table table-striped table-responsive" id="tabla">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th># OC</th>
                                        <th>Proveedor</th>
                                        <th>Vendedor</th>
                                        <th>Cliente</th>
                                        <th>Productos</th>
                                        <th>Importe Total</th>
                                        <th>Tipo de Pago</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_productos" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Productos</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Descripcion</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_resultados">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<script>

    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/ordenescompra/getOrdenes"
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<i class='ft-chevron-down' aria-hidden='true'></i>"
                },
                {"data": "id", "sType": 'numeric'},
                {"data": "proveedor"},
                {"data": "vendedor"},
                {"data": "cliente"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon btn-success desc'>Productos</button>"
                },
                {"data": "importe"},
                {"data": "tipo_pago"},
                {
                    "data": null,
                    "defaultContent": "<button type='button' class='btn btn-sm btn-icon btn-success view'><i class='ft-eye'></i></button>"
                }
            ],
            "order": [[ 1, "asc" ]]
        });

    }
    
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        //Listener para vista
        $('#tabla tbody').on('click', 'button.view', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = "<?php echo base_url(); ?>index.php/ordenescompra/view_orden/"+data.id;
        });
        
        //         Listener para informacion detalla de fila
        $('#tabla tbody').on('click', 'button.desc', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            productos_orden(data.id);
        });

        function format(d) {
            // 'd' son los datos originales de la tabla (json)
            return d.info_secundaria;
        }

        load();
    });
    
    function productos_orden(id) {
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/ordenescompra/productosOrden",
            data: {id: id},
            success: function (data) {
                $("#modal_productos").modal("show");
                $("#tabla_resultados").html(data);
            }
        });
    }
</script>