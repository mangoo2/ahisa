<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Expedientes</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <br>
                        <h5><i class="ft-sun"></i> Listado expedientes</h5><hr>
                        <div class="overflow-scroll">
                            <table class="table table-striped table-responsive" id="tabla">
                                <thead>
                                    <tr>
                                        <th># Cotizacion</th>
                                        <th># Nota de Consumo</th>
                                        <th>Cliente</th>
                                        <th>Productos</th>
                                        <th>Estatus</th>
                                        <th>Tipo</th>
                                        <th>Costo</th>
                                        <th>Comisión</th>
                                        <th>Utilidad</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->
<div class="modal fade text-left" id="modal_productos" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600">Productos</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Descripcion</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_resultados">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL DE PRODUCTOs ---------------------------------------------------------------------->

<script>

    function load() {
        table.destroy();
        table = $('#tabla').DataTable({
            "ajax": {
                "url": "<?php echo base_url(); ?>index.php/expedientes/getExpediente"
            },
            "columns": [
                {"data": "id_cotizacion", "type": 'num'},
                {"data": "id_nota", "type": 'num'},
                {"data": "cliente"},
                {
                    "data": null,
                    "defaultContent": '<button class="btn gradient-cyan-dark-green btn-sm mb-0">Productos</button>'
                },
                {"data": "estatus"},
                {"data": "tipo"},
                {"data": "costo"},
                {"data": "comision"},
                {"data": "utilidad"}
            ],
            "order": [[ 0, "asc" ]]
        });

    }
    
    $(document).ready(function () {
        table = $('#tabla').DataTable();

        
        //         Listener para informacion detalla de fila
        $('#tabla tbody').on('click', 'button.desc', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            productos_orden(data.id);
        });

        function format(d) {
            // 'd' son los datos originales de la tabla (json)
            return d.info_secundaria;
        }

        load();
    });
    
    function productos_orden(id) {
        $.ajax({
            type: "POST",
            traditional: true,
            url: "<?php echo base_url(); ?>index.php/ordenescompra/productosOrden",
            data: {id: id},
            success: function (data) {
                $("#modal_productos").modal("show");
                $("#tabla_resultados").html(data);
            }
        });
    }
</script>