<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $norma_name='NOM-011-STPS-2001';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);

    setlocale(LC_ALL, 'es_MX');
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());

    $firmaemple=""; $cedula="";
    foreach ($datosoc->result() as $item) {
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $cedula=$item->cedula;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $direccion = $item->calle_num.', <br> '.$item->colonia.', <br>'.$item->poblacion.', '.$item->estado.',<br> C.P.'.$item->cp.'';
        $direccion2 = $item->calle_num.', '.$item->colonia.', '.$item->poblacion.', '.$item->estado.',<br> C.P.'.$item->cp.'';
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $logos = base_url().'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
  }
    // Page footer
  public function Footer() {
      $html = '
        <style type="text/css">
            .cmac{font-size:9px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
            .cma{font-size:15px; color:rgb(231,99,0);}
            .cmafolio{font-size:9px; background-color: rgb(231,99,0); font-weight: bold; margin-top:10px;}
            .footerpage{font-size:9px;font-style: italic;}
        </style> 
      <table width="100%" cellpadding="2">
        <tr><td align="center" colspan="2" class="footerpage">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr>
        <tr>
            <td align="center" class="cmac" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx
            </td>
            <td align="center" class="cmafolio" width="16%"><span class="cma">•</span>'.$GLOBALS['folio'].'</td>
        </tr>
        <tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$GLOBALS['folio'].' </td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
      $this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
  }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-11');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-11');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,25,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page
$pdf->AddPage('P', 'A4'); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style>
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:14px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
    .sangria{text-indent: 45px;}
    .fontbold{font-weight: bold;}
    .algc{text-align:center; }
    .ft09{font-size:9px;}
    .ft18{font-size:18px;}
    .ft14{font-size:14px;}
    .ft12{font-size:12px;}
    .ft11{font-size:11px;}
</style>';
$htmlg.='
        <table border="0">
            <tr >
                <td colspan="2" align="right" class="ft18 fontbold">'.$empresa.'</td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td width="50%" ></td>
                <td width="50%" align="right" class="ft12 fontbold">'.$direccion.'</td>
            </tr>
        </table>
        <table border="0">
            <tr><td></td></tr>
            
            <tr>
                <td class="fontbold ft14"><b>'.$nom_cargo.'</b></td>
            </tr>
            <tr>
                <td class="fontbold ft11"><b>'.$cargo_cargo.'</b></td>
            </tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes al Nivel de Exposición a Ruido (NER) y el Nivel de Presión Acústica (NPA) determinados en su centro de trabajo; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fecha.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="180px" colspan="2"></td></tr>
            <tr>
                <td>Ing. Christian Uriel Fabian Romero</td>
                <td>Ing. Tanya Alejandra Valle Tello</td>
            </tr>
            <tr>
                <td>Aprobó - Gerente General</td>
                <td>Revisó – Procesamiento de Información</td>
            </tr>
            <tr><td colspan="2" height="170px"></td></tr>
            <tr>
                <td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación', 0, 0, '', 'B', array(0,0,0));
$rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1));
foreach ($rowpersonal->result() as $itemp) { 
    $dosis[$itemp->num]=$itemp->dosis;                                                   
}

$htmlg='<style >
            td{font-family:calibri;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
            .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;}
            .ft09{font-size:9px;}
            .tableb td{
                border:1px solid #808080;
                font-family:Arial;text-align: center;font-size:11px;
            }
            .tableb th{
                background-color:rgb(217,217,217);
                border:1px solid #808080;  
                text-align:center; 
            }
            .txt_td{background-color:rgb(217,217,217);}
            .sangria{text-indent: 45px;}
            b{font-family:Arialb;}
        </style>
            <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
            
            <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>'.$empresa.'</b>, el día '.$fecha.'.</p>
            <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p>
            <p class="algc"><i>•  Nivel de Exposición al Ruido (NER) y Tiempo Máximo Permisible de Exposición (TMPE) de cada punto de medición ambiental.</i></p>';
            $htmlg.='<table class="tableb">
                        <tr>
                            <th width="10%">No. de punto</th>
                            <th width="30%">Identificación del punto de medición (área/ Identificación)</th>
                            <th width="10%">NER (dB(A))</th>
                            <th width="10%">Te</th>
                            <th width="10%">TMPE</th>
                            <th width="30%">Conclusión</th>
                        </tr>';
                $rowpuntostr=1;
                $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
                            foreach ($rowpuntos->result() as $item) {
                                $idpunto=$item->punto;
                                $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto,"activo"=>1));
                                foreach ($rownomd->result() as $itemnomd) {
                                    $numeronosabemos=8.3;
                                    $ner_db=90+9.97*(log10($numeronosabemos/(12.5*$itemnomd->tiempo_ruido)) );
                                    if($ner_db<90){
                                        $tmpe_h=0;
                                        $ner_db_result='No Aplica';
                                    }else{
                                        $tmpe_h=8/pow(2,(($ner_db-90)/3 ));
                                        $ner_db_result= $tmpe_h;
                                    }
                                    if($ner_db>80){
                                        $conclusion_ner='Te Supera el TMPE';
                                    }else{
                                        $conclusion_ner='Te no Supera el TMPE';
                                    }
                            $htmlg.='<tr>
                                        <td>'.$rowpuntostr.'</td>
                                        <td>'.$itemnomd->area.' / '.$itemnomd->identificacion.'</td>
                                        <td>'.round($ner_db,1).'</td>
                                        <td>'.$itemnomd->tiempo_ruido.'</td>
                                        <td>'.$ner_db_result.'</td>
                                        <td>'.$conclusion_ner.'</td>
                                     </tr>';
                                $rowpuntostr++;
                                }
                            }
            $htmlg.='</table>';
    $htmlg.='<p class="ft09 algc">De acuerdo al punto 7.2 de la NOM-011-STPS-2001 el cálculo del TMPE debe realizarse cuando el NER se encuentre entre dos de las magnitudes consignadas en la tabla 1 de la misma (90 y 105 dBA).<br>
                NER = Nivel de Exposición a Ruido<br>
                U exp = Incertidumbre expandida (K=2, confianza del 95.45%).<br>
                Te = Tiempo de Exposición Real a Ruido del Trabajador<br>
                TMPE = Tiempo Máximo Permisible de Exposición</p>
            <p class="ft09 algc"><u><i>Regla de Decisión</i></u></p>
            <p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';
        $result2=$this->ModeloCatalogos->table2a($idnom);
        $htmlg.='<p class="algc"><i>•  Nivel de Exposición al Ruido (NER) y Tiempo Máximo Permisible de Exposición (TMPE) de cada punto de medición personal(dosimetrias).</i></p><table class="tableb">
                        <tr>
                            <th width="10%">No. de punto</th>
                            <th width="30%">Identificación del punto de medición (nombre del trabajador/ puesto)</th>
                            <th width="10%">NER (dB(A))</th>
                            <th width="10%">Te</th>
                            <th width="10%">TMPE</th>
                            <th width="30%">Conclusión</th>
                        </tr>';
                    foreach ($result2->result() as $item) {
                        $difference = date_diff(date_create($item->ti), date_create($item->tf));
                        $ner_db=90+9.97*(log10($dosis[$item->num]/(12.5*$difference->h)) );
                        if($ner_db<90){
                            $tmpe_h=0;
                            $ner_db_result='No Aplica';
                        }else{
                            $tmpe_h=8/pow(2,(($ner_db-90)/3 ));
                            $ner_db_result= $tmpe_h;
                        }
                        if($ner_db>80){
                            $conclusion_ner='Te Supera el TMPE';
                        }else{
                            $conclusion_ner='Te no Supera el TMPE';
                        }
                        $htmlg.='<tr>
                                    <td class="tdtext1">'.$item->num.'</td>
                                    <td class="tdtext1">'.$item->trabajador.' / '.$item->puesto.'</td>
                                    <td class="tdtext1">'.round($ner_db,1).'</td>
                                    <td class="tdtext1">'.$difference->h.'</td>
                                    <td class="tdtext1">'.$ner_db_result.'</td>
                                    <td class="tdtext1">'.$conclusion_ner.'</td>
                                </tr>';
                    }
        $htmlg.='</table>
            <p class="algc"><i>• Nivel de Presión Acústica Promedio (NPAi), de cada punto de medición.</i></p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ', 0, 0, '', 'B', array(0,0,0));
$htmlgtab="";
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .sangria{text-indent: 45px;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p>';

        $cont_concamb=0;
        foreach ($get_conclu->result() as $c) {
            if($c->total_ptos_amb>0){
                $cont_concamb++;
                $htmlg.='<p>1. Se evalúa un total de <strong>'.$c->total_ptos_amb.'</strong> puntos <strong>ambientales</strong>, de los cuales: </p>
                    <p class="tdtext sangria">a) <strong>'.$c->no_superan_amb.'</strong> puntos no superan el tiempo máximo permisible de exposición (TMPE).
                    </p>
                    <p class="tdtext sangria">b) <strong>'.$c->superan_amb.'</strong> puntos superan el tiempo máximo permisible de exposición (TMPE).
                    </p> 
                <table border="0" align="center">
                    <tr><td><br></td></tr>
                    <tr>
                        <td width="30%"></td>
                        <td width="40%"><img src="'.$c->chartAmb.'"></td>
                        <td width="30%"></td>
                    </tr>
                </table>';
            }            
            if($c->total_ptos_pers>0){
                if($cont_concamb>0){
                    $ina="c)";
                    $inb="d)";
                }else{
                    $ina="a)";
                    $inb="b)";
                }
                $htmlg.='<p>'.$cont_concamb.'. Se evalúa un total de <strong>'.$c->total_ptos_pers.'</strong> puntos <strong>personales</strong>, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina.' <strong>'.$c->no_superan_pers.'</strong> puntos no superan el tiempo máximo permisible de exposición (TMPE).
                    </p>
                    <p class="tdtext sangria">'.$inb.' <strong>'.$c->superan_pers.'</strong> puntos superan el tiempo máximo permisible de exposición (TMPE).
                    </p> 
                <table border="0" align="center">
                    <tr><td><br></td></tr>
                    <tr>
                        <td width="30%"></td>
                        <td width="40%"><img src="'.$c->chartPers.'"></td>
                        <td width="30%"></td>
                    </tr>
                </table>';
            }
        }//foreach
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{font-family:Arial;text-align: justify;font-size:15px;}
        </style>
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
        <p class="tdtext">El ruido es uno de los peligros laborales más comunes dentro de las industrias y comercios; diariamente los trabajadores se ven expuestos a niveles de ruido medios de 85 decibeles ponderados A (en adelante, dB”A”). Estos niveles de ruido son potencialmente peligrosos para su audición y pueden producir además otros efectos perjudiciales como, por ejemplo: estrés y ansiedad.</p>
        <p class="tdtext">Para prevenir los efectos perjudiciales del ruido para los trabajadores, es preciso elegir con cuidado instrumentos, métodos de medición y procedimientos que permitan evaluar el ruido al que se ven expuestos. Es importante evaluar correctamente los diferentes tipos de ruido (estable, inestable o impulsivo), distinguir los ambientes ruidosos con diferentes espectros de frecuencias, y considerar asimismo las diversas situaciones laborales. Los principales objetivos de la medición del ruido en ambientes laborales son:</p>
        <p class="tdtext sangria">a) identificar a los trabajadores sometidos a exposiciones excesivas.</p>
        <p class="tdtext sangria">b) valorar la necesidad de implantar controles técnicos del ruido y demás tipos de control indicados en la NOM-011-STPS-2001.</p>

        <p class="tdtext">La prevención de las pérdidas auditivas en el trabajo beneficia al trabajador porque preserva las capacidades auditivas que son cruciales para disfrutar de una buena calidad de vida: comunicación interpersonal, disfrute de la música, detección de sonidos y muchas más. El Programa de Conservación de la Audición (PCA) proporciona un beneficio en términos de chequeo sanitario, ya que las pérdidas auditivas de carácter no laboral y las enfermedades auditivas con posible tratamiento suelen detectarse por medio de audiometrías anuales. La reducción de la exposición al ruido también reduce el estrés y la fatiga relacionados con el ruido.</p>
        <p class="tdtext">La empresa se beneficia directamente de la implantación de un PCA eficaz que mantenga a sus trabajadores en buenas condiciones de audición, ya que éstos serán más productivos y versátiles si no se deterioran sus capacidades de comunicación.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas', 0, 0, '', 'B', array(0,0,0));

$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
        </style>
        <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>
        <p>Determinar el Nivel de Exposición a Ruido (NER), el Tiempo Máximo Permisible de Exposición (TMPE) y Nivel de Presión Acústica (NPA) al que están expuestos los trabajadores de las áreas que presenten un NSA (ruido) igual o superior a 80 dB(A), lo anterior en el centro de trabajo denominado <b>'.$empresa.'</b>
        </p>
        <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>
        <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Condiciones de seguridad e higiene en los centros de trabajo donde se genere ruido".</p>
        <table>
                <tr><td class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO*</b></td></tr>
                <tr><td ></td></tr>
        </table>
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="44%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class="cet" width="56%">'.$empresa.'</td>
            </tr>
            <tr>
                <td class="txt_td"><br><br><i>b)  Domicilio</i></td>
                <td class="cet">'.$direccion2.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class="cet">'.$rfccli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d)  Giro y/o actividad principal</i></td>
                <td class="cet">'.$girocli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e)  Responsable de la empresa</i></td>
                <td class="cet">'.$representacli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f)  Teléfono</i></td>
                <td class="cet"> '.$telefonocli.'</td>
            </tr>
        </table>
        <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr>
                <tr><td ></td></tr>
                <tr><td class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></td></tr>
                <tr><td ></td></tr>
        </table>
        
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="44%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class=" cet" width="56%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td>
            </tr>
            <tr>
                <td class="txt_td"><i>b)  Domicilio</i></td>
                <td class=" cet">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class=" cet">ALP160621FD6</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d) Teléfono</i></td>
                <td class=" cet">(01) 222 2265395</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e) e-mail </i></td>
                <td class="cet">gerencia@ahisa.mx</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f) Número de acreditación </i></td>
                <td class="cet">AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td>
            </tr>
            <tr>
                <td class="txt_td"><i>g) Número de aprobación</i></td>
                <td class="cet">LPSTPS-153/2022</td>
            </tr>
            <tr>
                <td class="txt_td"><i>h) Lugar de expedición del informe </i></td>
                <td class="cet">Puebla, Puebla - México </td>
            </tr>
            <tr>
                <td class="txt_td"><i>i) Fecha de expedición del informe de resultados.</i></td>
                <td class="cet">'.$fechah.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>j) Signatario responsable de la evaluación</i></td>
                <td class="cet">'.$tecnico.'</td>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$datosdet=$this->ModeloNom->getPaso2_3($GLOBALS['id_rec']);
$pdf->Bookmark('H. Metodología de para la evaluación', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
            .checked{font-size:17px;font-family:dejavusans;}
        </style>
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
        <p><u><i>Reconocimiento.</i></u></p>
        <p class="tdtext tejus">Esta actividad debe realizarse previamente a la evaluación y consiste en recabar toda aquella información técnica y administrativa que permita seleccionar el método de evaluación y la prioridad de las zonas y puestos por evaluar. Esta información debe comprender:</p>
        <p class="tdtext tejus">a) planos de distribución de las áreas en que exista ruido y de la maquinaria y equipo generadores de ruido;</p>
        <p class="tdtext tejus">b) descripción del proceso de fabricación;</p>
        <p class="tdtext tejus">c) descripción de los puestos de trabajo expuestos a ruidos;</p>
        <p class="tdtext tejus">d) programas de mantenimiento de maquinaria y equipo generadores de ruidos;</p>
        <p class="tdtext tejus">e) registros de producción;</p>
        <p class="tdtext tejus">f) número de trabajadores expuestos a ruidos por área y por proceso de fabricación, incluyendo el tiempo de exposición;</p>
        <p class="tdtext tejus">g) reporte del reconocimiento sensorial de las zonas por evaluar, con el objeto de determinar las características del ruido (estable, inestable o impulsivo).</p>

        <p><u><i>Condiciones para la evaluación.</i></u></p>
        <p class="tdtext tejus">a) La evaluación de los NSA o NSCEA,T, debe realizarse bajo condiciones normales de operación.</p>
        <p class="tdtext tejus">b) La evaluación debe realizarse como mínimo durante una jornada laboral de 8 horas y en aquella jornada que, bajo condiciones normales de operación, presente la mayor emisión de ruido.</p>
        <p class="tdtext tejus">b) Si la evaluación dura más de una jornada laboral, en todas las jornadas en que se realice se deben conservar las condiciones normcles de operación.</p>
        <p class="tdtext tejus">d) Se debe usar pantalla contra viento en el micrófono de los instrumentos de medición, durante todo el tiempo que dure la evaluación.</p>

        <table><tr><td><br><br><br><br><br><br></td></tr></table>

        <p><u><i>Métodos de evaluación.</i></u></p>
        <p class="tdtext sangria"><u><i>Puntos de medición</i>.</u></p>
        <p class="tdtext tejus">Los puntos de medición deben seleccionarse de tal manera que describan el entorno ambiental de manera confiable, determinando su número, entre otros factores, por la ubicación de los puestos de trabajo o posiciones de control de la maquinaria y equipo del local de trabajo, el proceso de producción y las facilidades para su ubicación.</p>
        <p class="tdtext tejus">Todos los puntos de medición de una zona de evaluación deben identificarse con un número progresivo y registrar su posición en el plano correspondiente, según lo establecido en el inciso a) del Apartado B.4. de la norma NOM-011-STPS-2001.</p>


        <p><u><i>Calibración del equipo de medición en campo.</i></u></p>
        <p class="tdtext sangria">El equipo de medición debe ser calibrado al inicio y final de cada evaluación con un calibrador acústico. La calibración en campo deberá cumplir con el criterio de aceptación de ± 1 dB entre la calibración inicial y la calibración final.</p>

        <p><u><i>Ubicación.</i></u></p>
        <p class="tdtext tejus">La ubicación de los puntos de medición en función de las necesidades y características físicas y acústicas de cada local de trabajo debe efectuarse seleccionando el método conforme se indica en la tabla siguiente:</p>
        <table align="center" class="tableb">
            <thead>
                <tr class="txt_td">
                    <td rowspan="2"><i>TIPO DE RUIDO</i></td>
                    <td colspan="3" align="center"><i>Método para la ubicación de los puntos de medición</i></td>
                </tr>
                <tr class="txt_td">
                    <td><i>Gradiente de Presión Sonora</i></td>
                    <td><i>Prioridad de Áreas de Evaluación</i></td>
                    <td><i>Puesto Fijo de Trabajo</i></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="txt_td"><i>Ruido estable</i></td>
                    <td>SI</td>
                    <td>NO</td>
                    <td>NO</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Ruido inestable</i></td>
                    <td>SI</td>
                    <td>SI</td>
                    <td>SI</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Ruido impulsivo</i></td>
                    <td>SI</td>
                    <td>SI</td>
                    <td>SI</td>
                </tr>
            </tbody>
        </table>
        <p class="tdtext tejus">La evaluación de los puntos presentados en este informe se realizó bajo el(los) siguiente(s) método(s):</p>
        <table align="center" class="tableb">
            <thead >
                <tr class="txt_td">
                    <td width="40%" rowspan="2"><i>Área</i></td>
                    <td width="60%" colspan="4" align="center"><i>Método de evaluación </i></td>
                </tr>
                <tr class="txt_td">
                    <td><i>G.P.S.</i></td>
                    <td><i>P.A.E.</i></td>
                    <td><i>P.F.T.</i></td>
                    <td><i>E.P.</i></td>
                </tr>
            </thead>
            <tbody>';
           
            foreach ($datosdet as $i) {
                $chpgra=" --"; $chppri="--"; $chp="--"; $chppf="--";
                if($i->gradiente>0){
                    $chpgra='<p class="checked">✔</p>';
                }
                if($i->prioridad>0){
                    $chppri='<p class="checked">✔</p>';
                }
                if($i->puesto_fijo>0){
                    $chppf='<p class="checked">✔</p>';
                }
                if($i->personal>0){
                    $chp='<p class="checked">✔</p>';
                }
                $htmlg.='<tr>
                    <td width="40%">'.$i->area.'</td>
                    <td width="15%">'.$chpgra.'</td>
                    <td width="15%">'.$chppri.'</td>
                    <td width="15%">'.$chppf.'</td>
                    <td width="15%">'.$chp.'</td>
                </tr>';
            }
            $htmlg.='</tbody>
        </table>
        <p style="color: rgb(0,32,96); font-size:8pt;" align="center">G.P.S.: Gradiente de Presión Sonora; P.A.E.: Prioridad de Área de Evaluación; P.F.T.: Puesto Fijo de Trabajo; E.P.: Evaluación Personal (dosimetrías)</p>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->AddPage('P', 'A4');   

        $htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
        </style>
        <p><u><i>El método de gradiente de presión sonora:</i></u></p>
        <p class="tdtext tejus">a) el punto inicial debe fijarse al centro de la zona de evaluación, registrándose el NSA máximo (el cual debe utilizarse como referencia para iniciar la evaluación);</p>
        <p class="tdtext tejus">b) el observador se debe desplazar con el sonómetro en una trayectoria previamente determinada, hasta encontrar un NSA que difiera ± 3 dB(A), respecto al punto de referencia, marcando en el plano de distribución este punto. El procedimiento se repite a lo largo de esa trayectoria, hasta cubrir completamente la trayectoria de evaluación. Los puntos de medición son aquellos que registren su NSA, con diferencia de ± 3 dB(A) del punto de medición contiguo;</p>
        <p class="tdtext tejus">c) una vez concluida esa trayectoria, se procede de la forma descrita anteriormente, pero en forma transversal;</p>
        <p class="tdtext tejus">d) las trayectorias de ubicación de puntos de medición deben hacerse en función de las características del local de trabajo y de la distribución espacial del campo sonoro, pero siempre debe garantizarse que se ha cubierto toda la zona de trabajo; </p>
        <p class="tdtext tejus">e) la distancia entre puntos de medición no debe ser mayor de 12 metros;</p>
        <p class="tdtext tejus">f) cuando se han identificado todos los puntos de medición debe procederse a su evaluación.</p>

        <p><u><i>Método de Prioridad de Áreas de Evaluación:</i></u></p>
        <p class="tdtext">a) del análisis de la información, realizado en el reconocimiento sensorial, deben determinarse las zonas de evaluación; </p>
        <p class="tdtext tejus">b) las zonas de trabajo identificadas con NSA superior o igual a 80 dB(A), deben dividirse en áreas, guiándose por los ejes de columnas del plano de distribución de planta y cuidando que éstas no sean superiores a 6 metros por lado. No deben incluirse las áreas o pasillos de circulación; </p>
        <p class="tdtext tejus">c) una vez efectuada la división, deben identificarse aquellas áreas en las que existan trabajadores, a las que se les denominará áreas de evaluación; </p>
        <p class="tdtext tejus">d) las áreas de evaluación pueden ser jerarquizadas, exponiendo las razones en el registro de evaluación del estudio de niveles sonoros; </p>
        <p class="tdtext tejus">e) los puntos de medición en las áreas de evaluación deben ubicarse en las zonas de mayor densidad de trabajadores. De no ser posible esta ubicación, deben localizarse en el centro geométrico de cada área.</p>

        <table><tr><td><br><br><br><br></td></tr></table>
        <p><u><i>Método Puesto Fijo de Trabajo:</i></u></p>
        <p class="tdtext tejus">Para evaluar ruido en puesto fijo de trabajo, el punto de medición debe ubicarse en el lugar que habitualmente ocupa el trabajador o, de no ser posible, lo más cercano a él, sin interferir en sus labores.</p><br>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->Bookmark('I. Instrumento de medición utilizado ', 0, 0, '', 'B', array(0,0,0)); //trar los intrumentos utilizados en levantamiento
        $htmlg='<style type="text/css">
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td{border:1px solid #808080;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
                .ft11{font-size:11px;} 
            </style>
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table cellpadding="5" align="center" class="tableb">';

        //trae los equipos utilizados de tipo sonometro
        $getson = $this->ModeloNom->getEquiposNom($idnom);
        foreach($getson as $s){
            $htmlg.='
                <tr class="txt_td">
                    <td></td><td><i>Sonómetro Integrador</i></td>
                    <td><i>Filtro en bandas de octava</i></td>
                    <td><i>Calibrador Acústico</i></td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>
                    <td >'.$s->marca.'</td>
                    <td >'.$s->marcacal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>
                    <td >'.$s->modelo.'</td>
                    <td >'.$s->modelocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>
                    <td >'.$s->equipo.'</td>
                    <td >'.$s->equipocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>No. de Informe de calibración</i></td><td class="ft11">'.$s->no_informe_calibracion.'</td>
                    <td class="ft11" >'.$s->num_calibra.'</td>
                    <td class="ft11" >'.$s->no_informe_calibracioncal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>
                    <td >'.$s->fecha_calibra.'</td>
                    <td >'.$s->fecha_calibracioncal.'</td>
                </tr>';
        }
        $htmlg.='</table><p></p>';

        //trae los equipos utilizados para personal
        $getper = $this->ModeloNom->getEquiposPersonalNom($idnom);
        $contp=0;
        foreach($getper as $s){
            $contp++;
            if($contp==1){
                $htmlg.='<table cellpadding="5" align="center" class="tableb">';
            }
            $htmlg.='
                <tr class="txt_td">';
                    if($contp==1)
                        $htmlg.='<td></td><td><i>Dosímetro de ruido</i></td>';
                    else
                        $htmlg.='<td><i>Dosímetro de ruido</i></td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1)
                        $htmlg.='<td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>';
                    else 
                        $htmlg.='<td >'.$s->marca.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1)
                        $htmlg.='<td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>';
                    else
                        $htmlg.='<td >'.$s->modelo.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1)
                        $htmlg.='<td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>';
                    else 
                        $htmlg.='<td >'.$s->equipo.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1)
                        $htmlg.='<td class="txt_td"><i>No. de Informe de calibración</i></td><td class="ft11">'.$s->no_informe_calibracion.'</td>';
                    else
                        $htmlg.='<td class="ft11">'.$s->no_informe_calibracion.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1)
                        $htmlg.='<td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>';
                    else
                        $htmlg.='<td >'.$s->fecha_calibracion.'</td>';
                $htmlg.='</tr>';

            if($contp==3){
                $htmlg.='</table>';
            }
        }
        if($contp==1){
            $htmlg.='</table>';
        }

        $pdf->Bookmark('J. Informe descriptivo de las condiciones de operación', 0, 0, '', 'B', array(0,0,0));
        $htmlg.='<p class="titulosa"><b>J. INFORME DESCRIPTIVO DE LAS CONDICIONES DE OPERACIÓN</b></p>
        <table>
            <tr><td class="tdtext"> '.$txt_desc_proc.' </td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg2=""; $htmlg3="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('K. Descripción del proceso de fabricación del centro de trabajo evaluado', 0, 0, '', '', array(0,0,0));
$pdf->Bookmark('L. Descripción de los puestos de trabajo', 0, 0, '', '', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{text-align: justify;font-size:13px;}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
        </style>
        <p class="titulosa"><b>K.  DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</b></p>
        <p>Ver anexo V.</p>
        <p class="titulosa"><b>L.  DESCRIPCIÓN DE LOS PUESTOS DE TRABAJO</b></p>
        <table cellpadding="5" align="center" class="tableb">
            <tr class="txt_td">
                <td width="30%">Área</td>
                <td width="28%">Puesto de Trabajo</td>
                <td width="42%">Actividad(es)</td>
            </tr>';
            foreach($datosrec->result() as $d){
                $id_reconocimiento=$d->id;
            }
            //log_message('error', 'id_reconocimiento: '.$id_reconocimiento);
            $cont_td=0;
            $datosdetalle=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$id_reconocimiento,"estatus"=>1));
            foreach ($datosdetalle->result() as $p2) {
                $cont_td++;
                //if($p2->actividades!=""){
                    $htmlg.='<tr>
                        <td>'.$p2->area.'</td><td>'.$p2->puesto.'</td><td>'.$p2->descrip.'</td>
                    </tr>';
                //} 
                $htmlg2.='<tr>
                        <td>'.$p2->area.'</td><td>'.$p2->puesto.'</td><td>'.$p2->num_trabaja.'</td>
                    </tr>';
                $htmlg3.='<tr>
                        <td>'.$p2->area.'</td><td>'.$p2->puesto.'</td><td>'.$p2->tiempo_expo.'</td>
                    </tr>';
            }
            $pdf->Bookmark('M. Número de trabajadores por área y puesto de trabajo', 0, 0, '', '', array(0,0,0));
            //log_message('error', 'cont_td: '.$cont_td);
            if($cont_td>=15){
                $htmlg.='</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');
                $pdf->AddPage('P', 'A4');
              
                $htmlg='<style type="text/css">
                    .txt_td{background-color:rgb(217,217,217);}
                    .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                    .tableb td{border:1px solid #808080;}
                    .cet{text-align:center;}
                </style>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <tr class="txt_td">
                        <td>Área</td><td>Puesto de Trabajo</td><td>No. de Trabajadores</td>
                    </tr>';
                    $htmlg.= $htmlg2
                .'</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');  
            }
            else{
                $htmlg.='</table>
                <p></p>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <tr class="txt_td">
                        <td>Área</td><td>Puesto de Trabajo</td><td>No. de Trabajadores</td>
                    </tr>';
                    $htmlg.= $htmlg2
                .'</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');   
            }

        
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('N. Tiempo de exposición a ruido por áreas y puesto de trabajo', 0, 0, '', '', array(0,0,0)); 
$pdf->Bookmark('Ñ. Criterios utilizados para seleccionar el método de evaluación', 0, 0, '', '', array(0,0,0)); 
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{text-align: justify;font-size:14px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{
                border:1px solid #808080;
            }
        </style>
        <p class="titulosa"><b>N. TIEMPO DE EXPOSICIÓN A RUIDO POR ÁREAS Y PUESTO DE TRABAJO</b></p>
        <table cellpadding="5" align="center" class="tableb">
            <tr class="txt_td">
                <td>Área</td><td>Puesto de Trabajo</td><td>Tiempo de Exposición (h)</td>
            </tr>';
            $htmlg.= $htmlg3
        .'</table>
        <p class="titulosa"><b>Ñ. CRITERIOS UTILIZADOS PARA SELECCIONAR EL MÉTODO DE EVALUACIÓN</b></p>
        <p class="tdtext">Derivado del reconocimiento se identificaron las áreas susceptibles a evaluación, donde se determinó el tipo de ruido con base a los siguientes datos:</p>

        <table cellpadding="5" align="center" class="tableb">
            <tr class="txt_td">
                <td>Área</td>
                <td>NSA mínimo (dB(A))</td>
                <td>NSA máximo (dB(A))</td>
                <td>Tipo de Ruido</td>
            </tr>';
            $da3=$this->ModeloNom->getPaso2_3($id_reconocimiento);
            foreach ($da3 as $p2) {
                $tipo_ruido=$p2->tipo_ruido;
                if($p2->tipo_ruido=="E")
                    $tipo_ruido="Estable";
                else if($p2->tipo_ruido=="I")
                    $tipo_ruido="Inestable";
                else if($p2->tipo_ruido=="IM")
                    $tipo_ruido="Impulsivo";
                $htmlg.='<tr>
                            <td>'.$p2->area.'</td><td>'.$p2->min_db.'</td><td>'.$p2->max_db.'</td><td>'.$tipo_ruido.'</td>
                </tr>';
            }
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('O. Vigencia del informe', 0, 0, '', '', array(0,0,0)); 
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>O. VIGENCIA DEL INFORME</b></p>
        <p class="tdtext">La vigencia de este informe de resultados será de dos años, a menos que se modifique la maquinaría, el equipo, su distribución o las condiciones de operación, de tal manera que puedan ocasionar variaciones en los resultados de la evaluación del ruido.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('ANEXOS', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo', 1, 0, '', '', array(0,0,0)); 
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';

$pdf->AddPage('P', 'A4');
$dc=$datosrec->row();
$htmlg='<style type="text/css"> 
    .borderbottom{border-bottom:1px solid black;}
    .backg{background-color:rgb(217,217,217);}
    .alg{ text-align:center; }
    .ft9{ font-size:9px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
 </style>';
$htmlg.='<table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>

        <table border="0" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="20%"><b>No. DE INFORME</b></td>
                <td width="50%" class="borderbottom">'.$dc->num_informe_rec.'</td>
                <td width="10%"><b>Fecha:</b></td>
                <td width="20%" class="borderbottom">'.$dc->fecha.'</td>
            </tr>
            <tr>
                <td width="20%"><b>RAZÓN SOCIAL</b></td>
                <td width="80%" class="borderbottom">'.$dc->cliente.'</td>
            </tr>
            <tr class="borderbottom">
                <td width="60%" >'.$dc->calle_num.'</td>
                <td width="40%" >'.$dc->colonia.'</td>
            </tr>
            <tr>
                <td width="60%" ><b>CALLE Y NUMERO</b></td>
                <td width="40%" ><b>COLONIA</b></td>
            </tr>
            <tr class="borderbottom">
                <td width="34%" >'.$dc->poblacion.'</td>
                <td width="33%" >'.$dc->estado.'</td>
                <td width="33%" >'.$dc->cp.'</td>
            </tr>
            <tr>
                <td width="34%" ><b>MUNICIPIO</b></td>
                <td width="33%" ><b>ESTADO</b></td>
                <td width="33%" ><b>CÓDIGO POSTAL</b></td>
            </tr>
            <tr class="borderbottom">
                <td width="50%" >'.$dc->rfc.'</td>
                <td width="50%" >'.$dc->giro.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>RFC</b></td>
                <td width="50%" ><b>GIRO DE LA EMPRESA</b></td>
            </tr>
            <tr class="borderbottom">
                <td width="50%" >'.$dc->telefono.'</td>
                <td width="50%" >'.$dc->representa.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>NUMERO TELEFONICO</b></td>
                <td width="50%" ><b>REPRESENTANTE LEGAL</b></td>
            </tr>
            <tr>
                <td width="100%" class="borderbottom">'.$dc->nom_cargo.'</td>
            </tr>
            <tr>
                <td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td>
            </tr>
        </table>        
        
        <p></p>

        <table class="table ft9" border="1" align="center"> 
           
                <tr style="color: black;" class="ft9 fontbold alg">
                    <td colspan="3" class="backg">TURNOS DE TRABAJO</td>
                </tr>
                <tr style="color: black;" class="ft9 fontbold alg">
                    <td width="30%" class="backg">TURNOS</td>
                    <td width="35%" class="backg">HORARIOS</td>
                    <td width="35%" class="backg">TIEMPO DE COMIDA (min)</td>
                </tr>

                <tr>
                    <td class="backg">1er</td>
                    <td>'.$dc->priero.'</td>
                    <td>'.$dc->pero_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">2do</td>
                    <td>'.$dc->segdo.'</td>
                    <td>'.$dc->sdo_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">3er</td>
                    <td>'.$dc->tercero.'</td>
                    <td>'.$dc->tero_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">ADMINISTRATIVO</td>
                    <td>'.$dc->hora_admin.'</td>
                    <td>'.$dc->adm_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">OTRO</td>
                    <td>'.$dc->hora_otro.'</td>
                    <td>'.$dc->otro_comida.'</td>
                </tr>
            
        </table>
        <p></p>
        <table class="table ft9" border="1">
            <tr align="center">
                <td class="backg"><b>DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN</b></td>
            </tr>
            <tr>
                <td height="80px">'.$dc->desc_proceso.'</td>
            </tr>
        </table>

        <table class="table ft9" border="1" align="center">
            <tr align="center">
                <td class="backg"><b>PLAN DE MANTENIMIENTO DE MAQUINARIA Y EQUIPO GENERADORES DE RUIDO</b></td>
                <td class="backg"><b>REGISTROS DE PRODUCCIÓN</b></td>
            </tr>
            <tr>
                <td height="40px">'.$dc->plan_mante.'</td>
                <td height="40px">'.$dc->registros_prod.'</td>
            </tr>
        </table>
    
        <p class="ft9"><b>MÉTODOS DE UBICACIÓN DE LOS PUNTOS DE MUESTREO</b></p>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr class="backg">
                <td>TIPO DE RUIDO</td>
                <td>GRADIENTE DE PRESIÓN SONORA (GPS)</td>
                <td>PRIORIDAD DE ÁREAS DE EVALUACIÓN (PAE)</td>
                <td>PUESTO FIJO DE TRABAJO (PFT)</td>
                <td>EVALUACIÓN PERSONAL DE RUIDO (DOSIMETRÍA)</td>
            </tr>
            <tr>
                <td>RUIDO ESTABLE</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
            <tr>
                <td>RUIDO INESTABLE</td>
                <td>NO</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
            <tr>
                <td>RUIDO IMPULSIVO</td>
                <td>NO</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
        </table>
        <p class="ft7">Ruido estable: Es aquel que se registra con variaciones en su nivel sonora "A" dentro de un intervalo de 5 dB(A).<br>
        Ruido inestable: Es aquel que se registra con variaciones en su nivel sonora "A" con un intervalo mayor a 5 dB(A).<br>
        Ruido impulsivo: Es aquel ruido inestable que se registra durante un periodo menor a un segundo.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

/* ********************PASO 2 de reconocimiento ************************* */
$pdf->AddPage('P', 'A4');
$htmlg='';

$datosdet=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$htmlg='<style>
        .txt_td { background-color:rgb(217,217,217); }
        .algc{text-align:center; }
        .ft9{font-size:9px;}
    </style>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2">
                <img src="'.base_url().'public/img/logo.jpg" >
            </td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
            <td>REG-TEC/02-01</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <p></p>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <thead>
            <tr class="txt_td">
                <th width="10%"><p></p>No.</th>
                <th width="20%"><p></p>NOMBRE DEL ÁREA</th>
                <th width="15%"><p></p>PUESTO DE TRABAJO</th>
                <th width="15%"><p></p>No. DE TRABAJADORES</th>
                <th width="30%"><p></p>DESCRIPCIÓN DE ACTIVIDADES</th>
                <th width="10%">TIEMPO DE EXPOSICIÓN REAL AL RUIDO (h)</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($datosdet->result() as $i) {
            $htmlg.='<tr>
                <td width="10%">'.$i->num.'</td>
                <td width="20%">'.$i->area.'</td>
                <td width="15%">'.$i->puesto.'</td>
                <td width="15%">'.$i->num_trabaja.'</td>
                <td width="30%">'.$i->descrip.'</td>
                <td width="10%">'.$i->tiempo_expo.'</td>
            </tr>';
        }
    $htmlg.='</tbody>        
    </table>
    <p></p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
/* ************************************************/

/* *******************PASO 3 DEL RECONOCIMIENTO ************* */
$htmlg='';
$pdf->AddPage('P', 'A4');

$datosdet2=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso3_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$htmlg='<style>
        .txt_td { background-color:rgb(217,217,217); }
        .ft7{ font-size:7px; text-align:center; }
        .algc{text-align:center; }
        .ft9{font-size:8.7px;}
    </style>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2">
                <img src="'.base_url().'public/img/logo.jpg" >
            </td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
            <td>REG-TEC/02-01</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <p></p>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <thead>
            <tr class="txt_td">
                <th width="5%" rowspan="2"><p></p>No.</th>
                <th width="19%" rowspan="2"><p></p>FUENTES GENERADORAS DE RUIDO</th>
                <th width="12%" colspan="2">NIVEL SONORO ENCONTRADO</th>
                <th width="7%" rowspan="2"><p></p>TIPO DE RUIDO <br>(E, I, IM)</th>
                <th width="10.5%" rowspan="2"><p></p>No. DE <br>TRABAJADORES <br>FIJOS</th>
                <th width="10.5%" rowspan="2"><p></p>No. DE <br>TRABAJADORES <br>MÓVILES</th>
                <th width="36%" colspan="4">NÚMERO DE PUNTOS A MUESTRA POR MÉTODO DE EVALUACIÓN</th>
            </tr>
            <tr class="txt_td">
                <th>Min. dB(A)</th>
                <th>Max. dB(A)</th>
                <th width="9%">GRADIENTE DE PRESIÓN SONORA</th>
                <th width="9%">PUESTO FIJO DE TRABAJO</th>
                <th width="10%">PRIORIDAD DE ÁREAS DE EVALUACIÓN</th>
                <th width="8%">PERSONAL</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($datosdet2->result() as $i) {
            $htmlg.='<tr>
                <td width="5%">'.$i->num.'</td>
                <td width="19%">'.$i->fuentes_genera.'</td>
                <td width="6%">'.$i->min_db.'</td>
                <td width="6%">'.$i->max_db.'</td>
                <td width="7%">'.$i->tipo_ruido.'</td>
                <td width="10.5%">'.$i->num_trabaja_fijo.'</td>
                <td width="10.5%">'.$i->num_trabaja_mov.'</td>
                <td width="9%">'.$i->gradiente.'</td>
                <td width="9%">'.$i->puesto_fijo.'</td>
                <td width="10%">'.$i->prioridad.'</td>
                <td width="8%">'.$i->personal.'</td>
            </tr>';
        }
    $htmlg.='</tbody>        
    </table>
    <p class="ft7">E:ESTABLE, I:INESTABLE, IM:IMPULSIVO</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
/* ********************************************************/

/**************************PASO 4 DE RECONOCIMIENTO *******************************/
$pdf->AddPage('P', 'A4');
if(isset($firmaemple)){
    $fh = fopen(base_url()."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
    $linea = fgets($fh);
    fclose($fh);     
}else{
    $linea='#';
}
$det4=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso4_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$det4=$det4->row();
$htmlg='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:rgb(217,217,217);}
                .algc{text-align:center; }
                .ft9{font-size:9px;}
        </style>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>

        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="35%" class="backg"><p></p>JUSTIFICACIÓN TÉCNICA PARA LA DETERMINACIÓN DE PUNTOS</td>
                <td width="65%" height="80px">'.$det4->justificacion.'</td>
            </tr>
        </table>

        <table><tr><td></td></tr></table>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="35%" class="backg">EQUIPO DE PROTECCIÓN PERSONAL UTILIZADO (MARCA, MODELO)</td>
                <td width="65%" height="30px">'.$det4->equipo_protec.'</td>
            </tr>
        </table>
        <table><tr><td></td></tr></table>

        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr><td class="backg">PLANO DE DISTRIBUCIÓN DE ÁREAS, MAQUINARIA, EQUIPO QUE GENEREN RUIDO Y UBICACIÓN DE LOS PUNTOS DE EVALUACIÓN</td></tr>
            <tr><td height="400px" style="font-size:12px"><br><br><br><br><br><br><br><br><br><br><br><br>
                '.$det4->plano_distri.'</td>
            </tr>
            <tr><td class="backg">OBSERVACIONES</td></tr>
            <tr><td height="50px">'.$det4->observaciones.'</td></tr>
            <tr>
                <td class="backg" width="50%">Nombre y firma del ingeniero de campo</td>
                <td width="50%">'.$tecnico.' </td>
            </tr>
            <tr>
                <td class="backg" width="50%"><p></p>Nombre y firma del responsable de la revisión del registro de campo</td>
                <td width="50%">Ing. Christian Uriel Fabian Romero <img src="'.$linea.'" width="75" style="border:dotted 1px black;"> </td>
            </tr>
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

/* *********************************/

$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->writeHTML($htmlg, true, false, true, false, ''); 


$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27,15);
$pdf->Bookmark('II. Planos', 1, 0, '', '', array(0,0,0)); /////desde acá
$htmlg='
        <table>
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO II</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PLANOS</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$cont_plan=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==1){
        $pdf->AddPage('P', 'A4');
        $cont_plan++;
        
        $htmlg='<table><tr><td align="center"><i>Plano de distribución de las áreas en que exista el ruido</i></td></tr><tr><td height="800px"><img src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'" ></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        
        //$htmlg='<p style="text-align:cente;"><i>Plano de distribución de las áreas en que exista el ruido</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 150, '', '', 'M', true, 150, 'C', false, false, 1, false, false, false);
    }
}
if($cont_plan==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de distribución de las áreas en que exista el ruido</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan2=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==2){
        $pdf->AddPage('P', 'A4');
        $cont_plan2++;
        
        $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></td></tr><tr><td height="800px"><img src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        
        //$htmlg='<p style="text-align:cente;"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);
    }
}
if($cont_plan2==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan3=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==3){
        $pdf->AddPage('P', 'A4');
        $cont_plan3++;
        
        $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr><tr><td height="800px"><img src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        
        //$htmlg='<p style="text-align:cente;"><i>Plano de identificación de los puntos de medición</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

    }
}
if($cont_plan3==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$htmlg.='';
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO III</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">SECUENCIA DE CÁLCULO </td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$htmlg=''; $equipo=0;
$htmlg.='<style type="text/css">
    .titulos{background-color: rgb(217,217,217);}
    .table-bordered th, .table-bordered td {border: 1px solid #c6c9cb;}
    .table thead th {border-bottom: 2px solid #c6c9cb;}
    .table td{padding: 0.5rem;font-size: 10px;vertical-align: middle;text-align: center;}
    .table th{padding: 0.5rem;font-size: 11px;vertical-align: middle;text-align: center;}

    .title_tb{ color: #548235; font-weight: bold;}  
</style>';
foreach ($datosnom->result() as $item) { 
    $htmlg.='
        <table class="table table-bordered" border="1">
        <tr>
            <td colspan="2"></td>
            <td class="titulos">No. de Informe</td><td class="title_tb">'.$folio.'</td>
        </tr>
        <tr>
            <td class="titulos">Razón Social</td><td colspan="3" class="title_tb">'.$item->razon_social.'</td>
        </tr>
        <tr>
            <td class="titulos">Fecha</td><td class="title_tb">'.$item->reg.'</td>
            <td class="titulos">Id. del equipo</td><td class="title_tb">'.$equipo.'</td>
        </tr>
        <tr><td colspan="4"></td></tr>
    </table>';
}
//====================================================================================================== todo esto tiene que ser lo mismo al archivo cnom011view.php si se modifica uno de los dos el otro tendra lo mismo
    $rowpuntostr=1;
    $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
    foreach ($rowpuntos->result() as $item) { 
        $idpunto=$item->punto;
        
        
        $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto));
        $itemnomd=$rownomd->row();
        if($itemnomd->te>0){ $htmlgti= $itemnomd->te; }else{ $htmlgti='480';}
        $tiporuido=$itemnomd->tiempo_ruido*60;
        $htmlg.='<table class="table table-bordered ">
            <tr>
                <th>No. Punto</th>
                <th>'.$rowpuntostr.'</th>
                <th colspan="3">Tiempo  efectivo de exposición al ruido (h)</th>
                <th colspan="2">'.$itemnomd->tiempo_ruido.'</th>
            </tr>
            <tr>
                <th colspan="2"></th>
                <th>Ti (minutos)</th>
                <th>'.$tiporuido.'</th>
                <th colspan="2">Te para 8 h en minutos </th>
                <th>'.$htmlgti.'</th>
            </tr>
            <tr>
                <th>Area </th>
                <th colspan="2">'.$itemnomd->area.'</th>
                <th>Identificacion</th>
                <th colspan="3">'.$itemnomd->identificacion.'</th>
            </tr>
        </table>
        <table>
            <tr><td></td></tr>
            <tr>
                <td width="21%">
                    <table class="table table-bordered ">
                        <tr>
                            <th class="titulos" colspan="2">Ruido</th>
                        </tr>
                        <tr>
                            <th class="titulos">Estable</th>
                            <th>';
                            if($itemnomd->tipo_ruido==0){$htmlg.='X';}
                        $htmlg.='</th>
                        </tr>
                        <tr>
                            <th class="titulos">Inestable</th>
                            <th>';
                            if($itemnomd->tipo_ruido==1){$htmlg.='X';}
                        $htmlg.='</th>
                        </tr>
                        <tr>
                            <th class="titulos">Impulsivo</th>
                            <th>';
                            if($itemnomd->tipo_ruido==2){$htmlg.='X';}$htmlg.='</th>
                        </tr>
                    </table>
                </td>
                <td width="79%">
                    <table class="table table-bordered tablecaptura" align="center">
                                        <thead>
                                            <tr>
                                                <th class="titulos" rowspan="2">Lectura
                                                </th>
                                                <th class="titulos" colspan="2">Periodo 1</th>
                                                <th class="titulos" colspan="2">Periodo 2</th>
                                                <th class="titulos" colspan="2">Periodo 3</th>
                                            </tr>
                                            <tr>
                                                <th class="titulos">NSCE<br>dB(A)</th>
                                                <th class="titulos"><img src="'.base_url().'public/img/formula3.png"></th>
                                                <th class="titulos">NSCE<br>dB(A)</th>
                                                <th class="titulos"><img src="'.base_url().'public/img/formula3.png"></th>
                                                <th class="titulos">NSCE<br>dB(A)</th>
                                                <th class="titulos"><img src="'.base_url().'public/img/formula3.png"></th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                            $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); 
                                                    $rowlecturas_tr=1;
                                                    $sumaperiodos_123=0;
                                                    foreach ($rowlecturas->result() as $iteml) {
                                                    
                                                        $htmlg.='<tr  >
                                                                <th class="titulos">'.$rowlecturas_tr.'</th>
                                                                <th>';
                                                                if($iteml->periodo1>0){$htmlg.=$iteml->periodo1;}else{$htmlg.='--';}
                                                                $htmlg.='</th>
                                                                <th>';
                                                                if($iteml->periodo1>0){
                                                                            $periodo1pow=pow(10, ($iteml->periodo1/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                                    $htmlg.=$periodo1pow;
                                                                }else{
                                                                            $periodo1pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                                    $htmlg.='--';
                                                                        };
                                                                $htmlg.='</th>
                                                                <th>';
                                                                    if($iteml->periodo2>0){$htmlg.=$iteml->periodo2;}else{$htmlg.='--';}$htmlg.='
                                                                </th>
                                                                <th>';
                                                                    if($iteml->periodo2>0){
                                                                            $periodo2pow=pow(10, ($iteml->periodo2/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                                        $htmlg.=$periodo2pow;
                                                                    }else{
                                                                            $periodo2pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                                        $htmlg.='--';
                                                                    }
                                                                $htmlg.='</th>
                                                                <th>';
                                                                    if($iteml->periodo3>0){$htmlg.=$iteml->periodo3;}else{$htmlg.='--';}
                                                                $htmlg.='</th>
                                                                <th>';
                                                                    if($iteml->periodo3>0){
                                                                            $periodo3pow=pow(10, ($iteml->periodo3/10));
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                                        $htmlg.=$periodo3pow;
                                                                    }else{
                                                                            $periodo3pow=0;
                                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                                        $htmlg.='--';
                                                                    }
                                                                $htmlg.='</th>
                                                              </tr>';
                                                        $rowlecturas_tr++;
                                                    }
                                                $htmlg.='<tr>
                                                    <th class="titulos">Suma</th>
                                                    <th colspan="6">'.$sumaperiodos_123.'</th>
                                                </tr>
                                        </tbody> 
                                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="3">NIVEL SONORO CONTINUO EQUIVALENTE</th>
            </tr>
            <tr>
                <td></td>
                <td><img src="'.base_url().'public/img/formula4.png"></td>
                <td>';
                        if($itemnomd->tipo_ruido==0){
                            $num=(1/20)*$sumaperiodos_123;
                            $nivel_sonoro_continuo=10*log10((1/20)*$sumaperiodos_123);
                        }elseif($itemnomd->tipo_ruido==1){
                            $nivel_sonoro_continuo=10*log10((1/30)*$sumaperiodos_123);
                        }else{
                            $nivel_sonoro_continuo=10*log10((1/45)*$sumaperiodos_123);
                        }
                    $htmlg.='NSCEA,Ti dB(A)= <b>'.round($nivel_sonoro_continuo, 2).'</b></td>
            </tr>
            <tr>
                <th colspan="3">NIVEL DE EXPOSICIÓN AL RUIDO</th>
            </tr>
            <tr>
                <td></td>
                <td><img src="'.base_url().'public/img/formula5.png"></td>
                <td>';
                    $nivel_expocion_ruido=10*log10(($itemnomd->tiempo_ruido*60)*pow(10, ($nivel_sonoro_continuo/10)) )-10*log10($itemnomd->te);
                    $htmlg.='NER dB(A)=  <b>'.round($nivel_expocion_ruido,2).'</b></td>
            </tr>
            <tr>
                <td>TIEMPO MÁXIMO PERMISIBLE DE EXPOSICIÓN</td>
                <td><img src="'.base_url().'public/img/formula6.png"></td>
                <td>';
                        if($nivel_expocion_ruido<90){
                            $t_max_permisible_exp=0;
                            $t_max_permisible_exp_l='No Aplica';
                        }else{
                            $t_max_permisible_exp=8/(pow(2,(($nivel_expocion_ruido-90)/3)));
                            $t_max_permisible_exp_l=round($t_max_permisible_exp,2);
                        }
                    $htmlg.='TMPE dB(A)= <b>'.$t_max_permisible_exp_l.'</b></td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="20%"></td>
                <td width="60%"><img src="'.$item->grafo1.'" ></td>
                <td width="20%"></td></tr>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;"><b>REGISTRO DEL ESPECTRO ACÚSTICO</b></div>
        </div>
        <table class="table table-bordered tablecaptura rea tablev">
            <thead>
                <tr>
                    <th class="titulos" rowspan="2">Periodo</th>
                    <th class="titulos" colspan="2">Ponderación</th>
                    <th class="titulos" colspan="9">Frecuencias Centrales (Hz)</th>
                </tr>
                <tr>
                    <th class="titulos">db(A)</th>
                    <th class="titulos">Lineal</th>
                    <th class="titulos">31.5</th>
                    <th class="titulos">63</th>
                    <th class="titulos">125</th>
                    <th class="titulos">250</th>
                    <th class="titulos">500</th>
                    <th class="titulos">1000</th>
                    <th class="titulos">2000</th>
                    <th class="titulos">4000</th>
                    <th class="titulos">8000</th>
                </tr>
            </thead>
            <tbody>';
                 
                    $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                    $rownomdreaperiodo=1;
                    $rownomdrea_num=$rownomdrea->num_rows();

                    $rea_db_t=0;$rea_lineal_t=0;$rea_31_5_t=0;$rea_63_t=0;$rea_125_t=0;$rea_250_t=0;$rea_500_t=0;$rea_1000_t=0;$rea_2000_t=0;$rea_4000_t=0;$rea_8000_t=0;

                    foreach ($rownomdrea->result() as $itrea) { 
                        if($rownomdrea_num==1){
                            $rea_db_t=$itrea->rea_db;
                            $rea_lineal_t=$itrea->rea_lineal;
                            $rea_31_5_t=$itrea->rea_31_5;
                            $rea_63_t=$itrea->rea_63;
                            $rea_125_t=$itrea->rea_125;
                            $rea_250_t=$itrea->rea_250;
                            $rea_500_t=$itrea->rea_500;
                            $rea_1000_t=$itrea->rea_1000;
                            $rea_2000_t=$itrea->rea_2000;
                            $rea_4000_t=$itrea->rea_4000;
                            $rea_8000_t=$itrea->rea_8000;
                        }else{
                            $rea_db_t=$rea_db_t+(pow(10,($itrea->rea_db/10)));
                            $rea_lineal_t=$rea_lineal_t+(pow(10,($itrea->rea_lineal/10)));
                            $rea_31_5_t=$rea_31_5_t+(pow(10,($itrea->rea_31_5/10)));
                            $rea_63_t=$rea_63_t+(pow(10,($itrea->rea_63/10)));
                            $rea_125_t=$rea_125_t+(pow(10,($itrea->rea_125/10)));
                            $rea_250_t=$rea_250_t+(pow(10,($itrea->rea_250/10)));
                            $rea_500_t=$rea_500_t+(pow(10,($itrea->rea_500/10)));
                            $rea_1000_t=$rea_1000_t+(pow(10,($itrea->rea_1000/10)));
                            $rea_2000_t=$rea_2000_t+(pow(10,($itrea->rea_2000/10)));
                            $rea_4000_t=$rea_4000_t+(pow(10,($itrea->rea_4000/10)));
                            $rea_8000_t=$rea_8000_t+(pow(10,($itrea->rea_8000/10)));
                        }
                        
                        $htmlg.='<tr >
                            <th class="titulos">'.$rownomdreaperiodo.'</th>
                            <td>'.$itrea->rea_db.'</td>
                            <td>'.$itrea->rea_lineal.'</td>
                            <td>'.$itrea->rea_31_5.'</td>
                            <td>'.$itrea->rea_63.'</td>
                            <td>'.$itrea->rea_125.'</td>
                            <td>'.$itrea->rea_250.'</td>
                            <td>'.$itrea->rea_500.'</td>
                            <td>'.$itrea->rea_1000.'</td>
                            <td>'.$itrea->rea_2000.'</td>
                            <td>'.$itrea->rea_4000.'</td>
                            <td>'.$itrea->rea_8000.'</td>
                        </tr>';
                        $rownomdreaperiodo++;
                    }
                $htmlg.='<tr>
                    <th class="titulos">NPA(dB)</th>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_db_t0=$rea_db_t;    
                            }else{
                                $rea_db_t0=10*log10((1/2)*$rea_db_t);
                            }
                            $htmlg.=$rea_db_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_lineal_t0=$rea_lineal_t;    
                            }else{
                                $rea_lineal_t0=10*log10((1/2)*$rea_lineal_t);
                            }
                            $htmlg.=$rea_lineal_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_31_5_t0=$rea_31_5_t;    
                            }else{
                                $rea_31_5_t0=10*log10((1/2)*$rea_31_5_t);
                            }
                            $htmlg.=$rea_31_5_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_63_t0=$rea_63_t;    
                            }else{
                                $rea_63_t0=10*log10((1/2)*$rea_63_t);
                            }
                            $htmlg.=$rea_63_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_125_t0=$rea_125_t;    
                            }else{
                                $rea_125_t0=10*log10((1/2)*$rea_125_t);
                            }
                            $htmlg.=$rea_125_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_250_t0=$rea_250_t;    
                            }else{
                                $rea_250_t0=10*log10((1/2)*$rea_250_t);
                            }
                            $htmlg.=$rea_250_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_500_t0=$rea_500_t;    
                            }else{
                                $rea_500_t0=10*log10((1/2)*$rea_500_t);
                            }
                            $htmlg.=$rea_500_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_1000_t0=$rea_1000_t;    
                            }else{
                                $rea_1000_t0=10*log10((1/2)*$rea_1000_t);
                            }
                            $htmlg.=$rea_1000_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_2000_t0=$rea_2000_t;    
                            }else{
                                $rea_2000_t0=10*log10((1/2)*$rea_2000_t);
                            }
                            $htmlg.=$rea_2000_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_4000_t0=$rea_4000_t;    
                            }else{
                                $rea_4000_t0=10*log10((1/2)*$rea_4000_t);
                            }
                            $htmlg.=$rea_4000_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_8000_t0=$rea_8000_t;    
                            }else{
                                $rea_8000_t0=10*log10((1/2)*$rea_8000_t);
                            }
                                $htmlg.=$rea_8000_t0;
                    $htmlg.='</td>
                </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;"><b>FACTOR DE ATENUACIÓN DEL EQUIPO DE PROTECCIÓN AUDITIVA</b></div>
        </div>
        <table class="table table-bordered tablecaptura faepa tablev" align="center" width="100%">
                <tr>
                    <th class="titulos">Frecuencia (Hz)</th>
                    <th class="titulos" width="9%">125</th>
                    <th class="titulos" width="9%">250</th>
                    <th class="titulos" width="9%">500</th>
                    <th class="titulos" width="9%">1000</th>
                    <th class="titulos" width="9%">2000</th>
                    <th class="titulos" width="9%">3000</th>
                    <th class="titulos" width="9%">4000</th>
                    <th class="titulos" width="9%">6000</th>
                    <th class="titulos" width="9%">8000</th>
                </tr>';
                 
                    $rownomdfaep=$this->ModeloCatalogos->getselectwheren('nom11d_faepa',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                    $itemfp=$rownomdfaep->row();
                    //foreach ($rownomdfaep->result() as $itemfp) {
                
                    $htmlg.='<tr>
                            <td class="titulos">Atenuación(dB)</td>
                            <td>'.$itemfp->faepa_125.'</td>
                            <td>'.$itemfp->faepa_250.'</td>
                            <td>'.$itemfp->faepa_500.'</td>
                            <td>'.$itemfp->faepa_1000.'</td>
                            <td>'.$itemfp->faepa_2000.'</td>
                            <td>'.$itemfp->faepa_3000.'</td>
                            <td>'.$itemfp->faepa_4000.'</td>
                            <td>'.$itemfp->faepa_6000.'</td>
                            <td>'.$itemfp->faepa_8000.'</td>
                        </tr>';
                
                    //}
            
            $htmlg.='</table>';
        
        //----------------------------------------------
            $rrpa_125=16.2;
            $rrpa_250=8.7;
            $rrpa_500=3.3;
            $rrpa_1000=0;
            $rrpa_2000=-1.2;
            $rrpa_4000=-1;
            $rrpa_8000=1.1;
            $itrea2=$rownomdrea->row(0);
        //--------------------------------------------
    
        $htmlg.='
            <div class="row">
                <div class="col-md-12" style="text-align:center;"><b>REDUCCIÓN DE RUIDO PARA LA PROTECCIÓN AUDITIVA</b></div>
                <div class="col-md-12">Correcciones para Q</div>
            </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="titulos">125 Hz</th>
                    <th class="titulos">250 Hz</th>
                    <th class="titulos">500 Hz</th>
                    <th class="titulos">1000 Hz</th>
                    <th class="titulos">2000 Hz</th>
                    <th class="titulos">4000 Hz</th>
                    <th class="titulos">8000 Hz</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>'.$rrpa_125.'</td>
                    <td>'.$rrpa_250.'</td>
                    <td>'.$rrpa_500.'</td>
                    <td>'.$rrpa_1000.'</td>
                    <td>'.$rrpa_2000.'</td>
                    <td>'.$rrpa_4000.'</td>
                    <td>'.$rrpa_8000.'</td>
                </tr>
            </tbody>
        </table>';
    
            if($itemfp->faepa_125==0){
                $aqf_q1=0;  
            }else{
                $aqf_q1=$itemfp->faepa_125+$rrpa_125;
            }
            if($itemfp->faepa_250==0){
                $aqf_q2=0;  
            }else{
                $aqf_q2=$itemfp->faepa_250+$rrpa_250;
            }
            if($itemfp->faepa_500==0){
                $aqf_q3=0;  
            }else{
                $aqf_q3=$itemfp->faepa_500+$rrpa_500;
            }
            if($itemfp->faepa_1000==0){
                $aqf_q4=0;  
            }else{
                $aqf_q4=$itemfp->faepa_1000+$rrpa_1000;
            }
            if($itemfp->faepa_2000==0){
                $aqf_q5=0;  
            }else{
                $aqf_q5=$itemfp->faepa_2000+$rrpa_2000;
            }
            if($itemfp->faepa_3000==0){
                $aqf_q6=0;  
            }else{
                $aqf_q6=($itemfp->faepa_3000+$itemfp->faepa_4000)/2+$rrpa_4000;
            }
            if($itemfp->faepa_6000==0){
                $aqf_q7=0;  
            }else{
                $aqf_q7=($itemfp->faepa_6000+$itemfp->faepa_8000)/2+$rrpa_8000;
            }
            
        $htmlg.='<table class="table table-bordered tablev">
            <tr>
                <th class="titulos" colspan="2">Atenuación Q Final</th>
                <th class="titulos">Lj-Qj/10</th>
                <th class="titulos"><img src="'.base_url().'public/img/formula7.png" width="50px"></th>
            </tr>
            <tr>
                <th class="titulos">Q1</th>
                <td>';
                    if($aqf_q1>0){ $htmlg.=$aqf_q1;}else{ $htmlg.='--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q1>0){ 
                            $aqf_q1_r1=($itrea2->rea_125-$aqf_q1)/10;
                            $htmlg.=$aqf_q1_r1;
                        }else{ 
                            $aqf_q1_r1=0;
                            $htmlg.='--';
                        }
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q1_r1>0){ 
                            $aqf_q1_r2=pow(10,$aqf_q1_r1);
                            $htmlg.= round($aqf_q1_r2,2);
                        }else{ 
                            $aqf_q1_r2=0;
                            $htmlg.= '--';
                        }
                $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q2</th>
                <td>';
                    if($aqf_q2>0){ $htmlg.=$aqf_q2;}else{ $htmlg.='--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q2>0){ 
                            $aqf_q2_r1=($itrea2->rea_250-$aqf_q2)/10;
                            $htmlg.= round($aqf_q2_r1,2);
                        }else{ 
                            $aqf_q2_r1=0;
                            $htmlg.= '--';
                        }
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q2_r1>0){ 
                            $aqf_q2_r2=pow(10,$aqf_q2_r1);
                            $htmlg.= round($aqf_q2_r2,2);
                        }else{ 
                            $aqf_q2_r2=0;
                            $htmlg.='--';
                        }
            $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q3</th>
                <td>';
                    if($aqf_q3>0){ $htmlg.=$aqf_q3;}else{ $htmlg.= '--';}
                $htmlg.='</td>
                <td>';
                        if($aqf_q3>0){ 
                            $aqf_q3_r1=($itrea2->rea_500-$aqf_q3)/10;
                            $htmlg.=round($aqf_q3_r1,2);
                        }else{ 
                            $aqf_q3_r1=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q3_r1>0){ 
                            $aqf_q3_r2=pow(10,$aqf_q3_r1);
                            $htmlg.= round($aqf_q3_r2,2);
                        }else{ 
                            $aqf_q3_r2=0;
                            $htmlg.= '--';
                        };
            $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q4</th>
                <td>';
                if($aqf_q4>0){ $htmlg.= $aqf_q4;}else{ $htmlg.= '--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q4>0){ 
                            $aqf_q4_r1=($itrea2->rea_1000-$aqf_q4)/10;
                            $htmlg.= round($aqf_q4_r1,2);
                        }else{ 
                            $aqf_q4_r1=0;
                            $htmlg.= '--';
                        }
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q4_r1>0){ 
                            $aqf_q4_r2=pow(10,$aqf_q4_r1);
                            $htmlg.= round($aqf_q4_r2,2);
                        }else{ 
                            $aqf_q4_r2=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q5</th>
                <td>';
                if($aqf_q5>0){ $htmlg.= $aqf_q5;}else{ $htmlg.= '--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q5>0){ 
                            $aqf_q5_r1=($itrea2->rea_2000-$aqf_q5)/10;
                            $htmlg.= round($aqf_q5_r1,2);
                        }else{ 
                            $aqf_q5_r1=0;
                            $htmlg.= '--';
                        }
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q5_r1>0){ 
                            $aqf_q5_r2=pow(10,$aqf_q5_r1);
                            $htmlg.= round($aqf_q5_r2,2);
                        }else{ 
                            $aqf_q5_r2=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q6</th>
                <td>'; 
                if($aqf_q6>0){ $htmlg.= $aqf_q6;}else{ $htmlg.= '--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q6>0){ 
                            $aqf_q6_r1=($itrea2->rea_4000-$aqf_q6)/10;
                            $htmlg.= round($aqf_q6_r1,2);
                        }else{ 
                            $aqf_q6_r1=0;
                            $htmlg.= '--';
                        }
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q6_r1>0){ 
                            $aqf_q6_r2=pow(10,$aqf_q6_r1);
                            $htmlg.= round($aqf_q6_r2,2);
                        }else{ 
                            $aqf_q6_r2=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
            </tr>
            <tr>
                <th class="titulos">Q7</th>
                <td>';
                if($aqf_q7>0){ $htmlg.= $aqf_q7;}else{ $htmlg.= '--';}
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q7>0){ 
                            $aqf_q7_r1=($itrea2->rea_8000-$aqf_q7)/10;
                            $htmlg.= round($aqf_q7_r1,2);
                        }else{ 
                            $aqf_q7_r1=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
                <td>'; 
                        if($aqf_q7_r1>0){ 
                            $aqf_q7_r2=pow(10,$aqf_q7_r1);
                            $htmlg.= round($aqf_q7_r2,2);
                        }else{ 
                            $aqf_q7_r2=0;
                            $htmlg.= '--';
                        };
                $htmlg.='</td>
            </tr>
            <tr>
                <td colspan="3">Suma</td>
                <td>';
                        if($aqf_q7>0){
                            $aqf_q1_q7_suma=$aqf_q1_r2+$aqf_q2_r2+$aqf_q3_r2+$aqf_q4_r2+$aqf_q5_r2+$aqf_q6_r2+$aqf_q7_r2;
                            $htmlg.= round($aqf_q1_q7_suma,2);
                        }else{
                            $aqf_q1_q7_suma=0;
                            $htmlg.= '--';
                        }
                    
                $htmlg.='</td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="2">FACTOR DE REDUCCIÓN, MODELO POR BANDAS DE OCTAVAS</th>
            </tr>
            <tr>
                <td><img src="'.base_url().'public/img/formula8.png" height="45px"></td>
                <td>Ri dB(A)=<b>';

                    $Ri_dBa=$nivel_expocion_ruido-10*log10($aqf_q1_q7_suma)-10;
                    $htmlg.= round($Ri_dBa,1);
                $htmlg.='</b></td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="3">FACTOR DE REDUCCIÓN, MODELO CON MEDICIONES DE RUIDO</th>
            </tr>
            <tr>
                <td>NRR=<b>'; 
                                $nrr=26;
                                $htmlg.= $nrr; 
                            $htmlg.='</b></td>
                <td><img src="'.base_url().'public/img/formula9.png" height="45px"></td>
                <td>R dB(A)=<b>'; 
                    $R_dBa=($nrr-7)/2;
                    $htmlg.= $R_dBa;
                $htmlg.='</b></td>
            </tr>
        </table>
        <table>
            <tr>
                <th colspan="2">NIVEL DE RUIDO EFECTIVO</th>
            </tr>
            <tr>
                <td>NRE = dB(A) - R</td>
                <td>NRE dB(A)= <b>';
                    $NRW_dBa=$nivel_expocion_ruido-$R_dBa;
                    $htmlg.= round($NRW_dBa,1);
                $htmlg.='</b></td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="20%"></td>
                <td width="60%"><img src="'.$item->grafo2.'" ></td>
                <td width="20%"></td></tr>
        </table>
        ';
    }
$pdf->writeHTML($htmlg, true, false, true, false, '');
//======================================================================================================

$htmlg.='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Ficha técnica de la protección personal auditiva', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IV</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">FICHA TÉCNICA DE LA PROTECCIÓN PERSONAL AUDITIVA</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
$cont_f=0;

$getfi=$this->ModeloCatalogos->getselectwheren('imagenes_fichas',array('id_nom'=>$idnom,'estatus'=>1));
foreach ($getfi->result() as $item) {
    $pdf->AddPage('P', 'A4');
    $cont_f++;

    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Ficha técnica de la protección personal auditiva</i></td></tr><tr><td height="800px"><img src="'.base_url().'uploads/fichas/'.$item->url_img.'"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    //$htmlg='<p style="text-align:cente;"><i>Ficha técnica de la protección personal auditiva</i></p>';
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
    //$pdf->Image(base_url().'uploads/fichas/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

}
if($cont_f==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Ficha técnica de la protección personal auditiva</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">EN EL CENTRO DE TRABAJO NO UTILIZAN EQUIPO DE PROTECCIÓN PERSONAL AUDITIVO</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Descripción del proceso de fabricación del centro de trabajo evaluado', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO V</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$pdf->AddPage('P', 'A4');
$htmlg='<table border="0" >
        <tr><td align="center" height="200px"></td></tr>
        <tr><td style="font-size:16px;" align="center">'.$txt_proceso_fabrica.'</td></tr>
    </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VI. Programa de mantenimiento a luminarias', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VI</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PROGRAMA DE<br>MANTENIMIENTO A <br>MAQUINARIA Y EQUIPO<br> GENERADOR DE RUIDO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
    if($datosnomv->num_rows()>0){
        foreach ($datosnomv->result() as $item) {
            $pdf->AddPage('P', 'A4');
            
            $htmlg='<table><tr><td align="center"><img src="'.base_url().'uploads/imgPrograma/'.$item->url_img.'"></td></tr></table>';
            $pdf->writeHTML($htmlg, true, false, true, false, '');
            //$pdf->Image(base_url().'uploads/imgPrograma/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, false);
        }
    }else{
        $pdf->AddPage('P', 'A4');
        $htmlg='<table border="0" >
                <tr><td align="center" height="200px"></td></tr>
                <tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr>
            </table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
    }
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VII. Informe de calibración del equipo de medición', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VII</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">INFORME DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VIII. Cédula profesional del evaluador', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VIII</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">CÉDULA PROFESIONAL DEL EVALUADOR</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$pdf->AddPage('P', 'A4');
$cont_f++;

$htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i></i></td></tr><tr><td height="800px"><img src="'.base_url().'uploads/cedulas/'.$cedula.'"></td></tr></table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

    //$pdf->Image(base_url().'uploads/cedulas/'.$cedula, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IX. Documento de acreditación', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IX</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE ACREDITACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

if($datosacre->num_rows()>0){
    $htmlg='';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4'); 
        
        $htmlg='<table><tr><td align="center"><img src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        
        //$pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

        $pdf->SetXY(50, 150);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 160);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
    }   
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('X. Documento de aprobación', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO X</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE APROBACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$txt_fin=""; $cont_acre=0;
if($datosacre2->num_rows()>0){
    $htmlg='';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2=0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima
        $cont2++;
        $pdf->AddPage('P', 'A4');
        
        $htmlg='<table><tr><td align="center"><img src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        
        //$pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);
        $pdf->SetXY(50, 150);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 160);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        if($cont_acre==$cont2){
            /*$txt_fin.='<table>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p>
                    </td>
                </tr>
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true);
            //$pdf->writeHTML($txt_fin, true, false, true, false, '');

            $pdf->SetXY(60, 222);
            $pdf->SetTextColor(0,57,88);
            $pdf->SetFont('dejavusans', '', 10);
            $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas';
            $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
            $pdf->Cell(100, 50, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

            $pdf->SetXY(55, 225);
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('dejavusans', '', 10);
            $pdf->Cell(100, 50, $txt_fin2, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
        }
    }
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    /*$pdf->SetXY(50, 150);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 23);
    $txt='Documento Informativo '.$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/
}


// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();



$pdf->Output('EntregableNom11.pdf', 'I');

?>