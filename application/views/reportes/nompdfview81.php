<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $norma_name='NOM-081-SEMARNAT-1994';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);

    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');

    mb_internal_encoding("UTF-8");
    
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());
    $fechah = ucfirst($fechah);

    $firmaemple=""; $cedula=""; $firmaresp=""; $tecnico="";
    foreach ($datosoc->result() as $item) {
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $cedula=$item->cedula;
        $firmaresp=$item->firmaresp;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $estado_name=utf8_decode($estado_name);
        $direccion = mb_strtoupper($item->calle_num.', <br> '.($item->colonia).', <br>'.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $direccion2 = mb_strtoupper($item->calle_num.', '.($item->colonia).', '.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
    } 
    // Page footer
    public function Footer() {
        $html = '
        <style type="text/css">
            .cmac{font-size:9px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
            .cma{font-size:15px; color:rgb(231,99,0);}
            .cmafolio{font-size:9px; background-color: rgb(231,99,0); font-weight: bold; margin-top:10px;}
            .footerpage{font-size:9px;font-style: italic;}
            table{text-align:center}
        </style> 
          <table width="100%" cellpadding="2">
            <tr><td colspan="2" class="footerpage">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr>
            <tr>
                <td class="cmac" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx
                </td>
                <td class="cmafolio" width="16%"><span class="cma">•</span>'.$GLOBALS['folio'].'</td>
            </tr>
            <tr><td colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$GLOBALS['folio'].' </td>
            </tr>
          </table>';
          //$this->writeHTML($html, true, false, true, false, '');
          $this->writeHTMLCell(188, '', 12, 275, $html, 0, 0, 0, true, 'C', true);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-081');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-081');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,25,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 26);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page
$pdf->AddPage('P', 'A4'); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style>
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:15px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
    .sangria{text-indent: 45px;}
    .fontbold{font-weight: bold;}
    .algc{text-align:center; }
    .ft09{font-size:9px;}
    .ft20{font-size:20px;}
    .ft18{font-size:18px;}
    .ft16{font-size:16px;}
    .ft14{font-size:14px;}
    .ft13{font-size:13px;}
    .ft12{font-size:12px;}
    .ft11{font-size:11px;}
</style>';
$htmlg.='
        <table border="0">
            <tr >
                <td colspan="2" align="right" class="ft20 fontbold">'.$empresa.'</td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td width="50%" ></td>
                <td width="50%" align="right" class="ft14 fontbold">'.$direccion.'</td>
            </tr>
        </table>
        <table border="0">
            <tr><td></td></tr>
            
            <tr>
                <td class="fontbold ft16"><b>'.$nom_cargo.'</b></td>
            </tr>
            <tr>
                <td class="fontbold ft13"><b>'.$cargo_cargo.'</b></td>
            </tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes a los niveles de ruido que su fuente fija emite; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fecha.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="180px" colspan="2"></td></tr>
            <tr>
                <td>Ing. Christian Uriel Fabian Romero</td>
                <td>Ing. Tanya Alejandra Valle Tello</td>
            </tr>
            <tr>
                <td>Aprobó - Gerente General</td>
                <td>Revisó – Procesamiento de Información</td>
            </tr>
            <tr><td colspan="2" height="170px"></td></tr>
            <tr>
                <td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1));
foreach ($rowpersonal->result() as $itemp) { 
    $dosis[$itemp->num]=$itemp->dosis;                                                   
}

$htmlg='<style >
            td{font-family:calibri;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
            .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;}
            .ft09{font-size:9px;}
            .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:11px;font-size:9px;valign: middle;vertical-align: middle;}
            .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:10px;}
            .pspaces{font-size:0.2px;}
            .txt_td{background-color:rgb(217,217,217);}
            .sangria{text-indent: 45px;}
            b{font-family:Arialb;}
            .title_tb{ color: #548235; font-weight: bold;} 
            .tgr{color:green;}
            .tred{color:red;}
        </style>
            <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
            <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>'.$empresa.'</b>, el día '.$fecha.'.</p>
            <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p>';
            $htmlg.='<table class="tableb" cellpadding="5">                     
                        ';
                        $zonacriticainfo=0; $cont_zonas=0;
                        foreach ($datosnom->result() as $dc){
                            $cont_zonas++;
                            if($zonacriticainfo==$dc->zona_critica){
                                $zonacriticainfo=$dc->zona_critica;
                            }else{
                                $zonacriticainfo=$dc->zona_critica;
                                $htmlg.='<tr><th colspan="4">ZONA CRITICA '.$dc->zona_critica.'</th></tr>
                                        <tr>
                                            <th width="20%"><p class="pspaces"></p>Horario de medición</th>
                                            <th width="20%"><p class="pspaces"></p>Emisión de ruido dB(A)</th>
                                            <!--<th width="10%"><p class="pspaces"></p>U exp (dB)</th>
                                            <th width="25%"><p class="pspaces"></p>Emisión de ruido + U exp dB (A)</th>-->
                                            <th width="20%"><p class="pspaces"></p>LMP dB(A)</th>
                                            <th width="40%"><p class="pspaces"></p>Conclusión</th>
                                        </tr>';       
                            }
                            if($dc->horario==1){
                                $col1='Diurno (06:00-22:00 h)';
                                $u_exp=0.70;
                                $u_exp_txt=$u_exp;
                                $lmp=68;
                            }else{
                                $col1='Nocturno (22:00-06:00 h)';
                                $u_exp=0;
                                $u_exp_txt=$u_exp;
                                $lmp=65;
                            }
                            if($dc->emision_ruido>0){ //según el doc de ahisa los resultados no son similares
                                $emision_ruido=$dc->emision_ruido;
                                $emision_ruido_txt=round($emision_ruido,1);
                            }else{
                                $emision_ruido=0;
                                $emision_ruido_txt='La fuente fija no emite nivel sonoro';
                                $u_exp_txt='No aplica';
                            }
                            //$emi_uexp=$emision_ruido+$u_exp;
                            $emi_uexp=$emision_ruido;

                            if($emi_uexp>0){
                                $emi_uexp_txt=round($emi_uexp,1);
                            }else{
                                $emi_uexp_txt='No aplica';
                            }
                            if($emi_uexp>$lmp){
                                $l_conclusion='<span class="tred">La fuente fija supera el nivel sonoro</span>';
                            }elseif($emi_uexp>0){

                                $l_conclusion='<span class="tgr">La fuente fija no supera el nivel sonoro</span>';
                            }else{
                                $l_conclusion='<span class="tgr">La fuente fija no emite nivel sonoro</span>';
                            }
                        $htmlg.='<tr>
                                    <th>'.$col1.'</th>
                                    <td>'.$emision_ruido_txt.'</td>
                                    <!--<td>'.$u_exp_txt.'</td>
                                    <td>'.$emi_uexp_txt.'</td>-->
                                    <td>'.$lmp.'</td>
                                    <td>'.$l_conclusion.'</td>
                                </tr>';
                        }

            $htmlg.='</table><p class="ft09 algc">U = Incertidumbre expandida con factor de cobertura k=2, nivel de confianza de 95.45% <br>
            LMP = Límite Máximo Permisible para zona industrial y comercial.</p>';
    $htmlg.='
            <p class="ft09 algc"><u><i>Regla de Decisión</i></u></p>
            <p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';

    $htmlg.='<p class="algc">El límite máximo permisible se determina con base a la zona donde la fuente fija se encuentra establecida según la siguiente tabla descrita en el acuerdo por el que se modifica el numeral 5.4 de la Norma Oficial Mexicana NOM-081-SEMARNAT-1994.</p>
        <table class="tableb" cellpadding="5">
            <tr>
                <th width="40%">Zona</th>
                <th width="20%">Horario</th>
                <th width="40%">Límite Máximo Permisible dB(A)</th>
            </tr>
            <tr>
                <td>Residencial1 (exteriores)</td>
                <td>06:00 a 22:00 <br>22:00 a 06:00</td>
                <td>55<br>50</td>
            </tr>
            <tr>
                <td>Industriales y comerciales</td>
                <td>06:00 a 22:00 <br>22:00 a 06:00</td>
                <td>68<br>65</td>
            </tr>
            <tr>
                <td>Escuelas (áreas exteriores de juego)</td>
                <td>Durante el juego</td>
                <td>55</td>
            </tr>
            <tr>
                <td>Ceremonias, festivales y eventos de entretenimiento</td>
                <td>4 horas</td>
                <td>100</td>
            </tr>';
            /*$cont_res=0;
            foreach ($result2 as $i) {
                $cont_res++;
                if($cont_res<16 || $cont_res>16){
                    $htmlg.='<tr>
                            <td class="tdtext1">'.$i->punto.'</td>
                            <td class="tdtext1">'.$i->fuente.'</td>
                            <td class="tdtext1">1</td>
                            <td class="tdtext1">'.$i->valor.'</td>
                        </tr>';
                }
                if($cont_res==16){
                    $htmlg.='<tr>
                            <td class="tdtext1"><br><br><br><br></td>
                            <td class="tdtext1"></td>
                            <td class="tdtext1"></td>
                            <td class="tdtext1"></td>
                        </tr>';
                }
            }*/
    $htmlg.='</table>
            <p class="ft09 algc">1 Entendida por: vivienda habitacional unifamiliar y plurifamiliar; vivienda habitacional con comercio en planta baja; vivienda habitacional mixta; vivienda habitacional con oficinas; centros de barrio y zonas de servicios educativos.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlgtab="";
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .sangria{text-indent: 45px;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p>
        <p class="tdtext">Derivado de los resultados obtenidos se presentan las siguientes conclusiones: </p>';
        foreach ($datosnom->result() as $dc){
            $chart="";
            if($dc->chart!=""){
                $chart='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $dc->chart) . '">';
            }
            if($dc->horario==1){
                $infochar='';//aqui ira la descripcion de las graficas diurna
            }else{
                $infochar='';//aqui ira la descripcion de las graficas nocturna
            }
            $infochar='Se evalúa un total de <strong>'.(70).'</strong> puntos, de los cuales se conlcuye lo siguiente:';
            $htmlg.='<table><tr><td>'.$infochar.'</td></tr><tr><td>'.$chart.'</td></tr></table>';
        }
        /*$cont_concamb=0; $cont_concamb2=1;
        foreach ($get_conclu->result() as $c) {
            if($c->total_electrodo_pt>0){
                $cont_concamb++;
                $htmlg.='<p>1. Se evalúa un total de <strong>'.$c->total_electrodo_pt.'</strong> electrodos en <strong>sistema de puesta a tierra</strong>, de los cuales: </p>
                    <p class="tdtext sangria">a) <strong>'.$c->no_superan_pt.'</strong> electrodos no superan el límite máximo permisible (LMP) de 25 Ω.
                    </p>
                    <p class="tdtext sangria">b) <strong>'.$c->supera_pt.'</strong> electrodos superan el límite máximo permisible (LMP) de 25 Ω.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><img src="'.$c->chartSPT.'"></td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }            
            if($c->total_electrodo_pr>0){
                $cont_concamb2++;
                if($cont_concamb>0){
                    $cont_concamb++;
                    $ina="c)";
                    $inb="d)";
                }else{
                    $ina="a)";
                    $inb="b)";
                }
                $htmlg.='<p>'.$cont_concamb.'. Se evalúa un total de <strong>'.$c->total_electrodo_pr.'</strong> electrodos en <strong>sistema de pararrayos</strong>, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina.' <strong>'.$c->no_supera_pr.'</strong> electrodos no superan el límite máximo permisible (LMP) de 10 Ω.
                    </p>
                    <p class="tdtext sangria">'.$inb.' <strong>'.$c->supera_pr.'</strong> electrodos superan el límite máximo permisible (LMP) de 10 Ω.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><img src="'.$c->chartSPR.'"></td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }if($c->total_ptos_continuidad>0){
                $cont_concamb2++;
                if($cont_concamb2>0){
                    $ina2="e)";
                    $inb2="f)";
                }else{
                    $ina2="c)";
                    $inb2="d)";
                }
                $htmlg.='<p>'.$cont_concamb2.'. Se evalúa un total de <strong>'.$c->total_ptos_continuidad.'</strong> puntos de continuidad eléctrica, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina2.' En <strong>'.$c->existe_conti.'</strong> puntos existe continuidad eléctrica.
                    </p>
                    <p class="tdtext sangria">'.$inb2.' En <strong>'.$c->no_existe_conti.'</strong> puntos no existe continuidad eléctrica.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><img src="'.$c->chartCE.'"></td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }
        }//foreach
        */
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{font-family:Arial;text-align: justify;font-size:15px;}
            
        </style>
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
        <p class="tdtext">El ruido se define como todo sonido indeseable que moleste o perjudique a las personas; es por ello que los niveles de ruido emitidos por las fuentes fijas deben ser monitoreados, y en su caso, controlados, con la finalidad de evitar alteraciones en las personas que perciben el ruido. La contaminación por el ruido puede causar o contribuir a las siguientes consecuencias adversas en las personas: sordera, ansiedad, tensión nerviosa, inquietud, nerviosismo, náuseas, cambios de humor, agresividad e histeria; en cuanto a la biodiversidad, la contaminación acústica puede alterar el equilibrio de los ecosistemas, esto se fundamente en que flora y fauna no se puedan establecer en un lugar, originando un cambio en los ecosistemas de la zona afectada por el ruido.</p>
        <p class="tdtext">La contaminación por ruido en México es un tema creciente, ya que debido a su tráfico terrestre y aéreo, comercios, industria y población incrementa día a día el nivel de ruido, sin embargo, el principal contribuyente de los niveles de ruido son las fuentes fijas (por fuente fija se entiende: toda instalación establecida en un sólo lugar que tenga como finalidad desarrollar actividades industriales, comerciales, de servicios o actividades que generen o puedan generar emisiones contaminantes a la atmósfera). Dichas fuentes fijas ignoran y/o desconocen la afectación que origina al ambiente y a su población, no regulan las emisiones de ruido de sus instalaciones y aumentan la problemática de ruido en el país.</p>

        <p class="tdtext">En el país existe legislación y normatividad que restringe las altas emisiones de ruido de las fuentes fijas; la Ley General del Equilibrio Ecológico y la Protección al Ambiente establece en su artículo 155 que “Quedan prohibidas las emisiones de ruido, …, en cuanto rebasen los límites máximos establecidos en las normas oficiales mexicanas que para ese efecto expida la Secretaría, considerando los valores de concentración máxima permisibles para el ser humano de contaminantes en el ambiente que determine la Secretaría de Salud. Las autoridades federales o locales, según su esfera de competencia, adoptarán las medidas para impedir que se transgredan dichos límites y en su caso, aplicarán las sanciones correspondientes”. Por otro lado, existe la Norma Oficial Mexicana NOM-081-SEMARNAT-1994 que establece los límites máximos permisibles de emisión de ruido de las fuentes fijas.</p>
        <p class="tdtext">Es importante que las fuentes fijas conozcan los niveles de ruido que emite y que estos sean comparados contra los límites máximos permisibles, para de esta forma conocer el grado de cumplimiento y, en el caso de ser necesario, establecer medidas de control que contribuyan a un mejor ambiente.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. identificación de la fuente fija ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
            .tableb td,.tableb th{border:1px solid #808080;}
            .sp_bck{ font-weight:bold;}
            .pspaces{ font-size:0.2px; }
            .tableb tr th{background-color:rgb(217,217,217);}
        </style>
        <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>
        <p class="tdtext">Determinar el nivel de emisión de las fuentes fijas generadoras de ruido al medio ambiente en la empresa <span class="sp_bck"><b>'.$empresa.',</b></span> aplicando las especificaciones de la NOM-081-SEMARNAT-1994 para mediciones semicontinuas y comparar los resultados con los Límites Máximos Permisibles establecidos en el numeral 5.4 de la norma mencionada con anterioridad.
        </p>
        <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>
        <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Que establece los límites máximos permisibles de emisión de ruido de las fuentes fijas y su método de medición".</p>
        <p class="titulosa"><b>F. IDENTIFICACIÓN DE LA FUENTE FIJA* </b></p>
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <th width="41%"><i>a)  Nombre, denominación o razón social</i></th>
                <td width="59%">'.$empresa.'</td>
            </tr>
            <tr>
                <th><p class="pspaces"></p><i>b)  Domicilio</i></th>
                <td>'.$direccion2.'</td>
            </tr>
            <tr>
                <th><i>c)  R.F.C.</i></th>
                <td>'.$rfccli.'</td>
            </tr>
            <tr>
                <th><i>d)  Giro y/o actividad principal</i></th>
                <td>'.$girocli.'</td>
            </tr>
            <tr>
                <th><i>e)  Responsable de la empresa</i></th>
                <td>'.$representacli.'</td>
            </tr>
            <tr>
                <th><i>f)  Teléfono</i></th>
                <td> '.$telefonocli.'</td>
            </tr>
        </table>
        <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr></table>
        <p class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></p>
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <th width="41%"><i>a)  Nombre, denominación o razón social</i></th>
                <td width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td>
            </tr>
            <tr>
                <th><i>b)  Domicilio</i></th>
                <td>19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td>
            </tr>
            <tr>
                <th><i>c)  R.F.C.</i></th>
                <td>ALP160621FD6</td>
            </tr>
            <tr>
                <th><i>d) Teléfono</i></th>
                <td>(01) 222 2265395</td>
            </tr>
            <tr>
                <th><i>e) e-mail </i></th>
                <td>gerencia@ahisa.mx</td>
            </tr>
            <tr>
                <th><i>f) Número de acreditación </i></th>
                <td>AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td>
            </tr>
            <tr>
                <th><i>g) Número de aprobación</i></th>
                <td>LPSTPS-153/2022</td>
            </tr>
            <tr>
                <th><i>h) Lugar de expedición del informe </i></th>
                <td>Puebla, Puebla - México </td>
            </tr>
            <tr>
                <th><i>i) Fecha de expedición del informe de resultados.</i></th>
                <td>'.$fechah.'</td>
            </tr>
            <tr>
                <th><i>j) Signatario responsable de la evaluación</i></th>
                <td>'.$tecnico.'</td>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$datosdet=$this->ModeloNom->getPaso2_3($GLOBALS['id_rec']);
$pdf->Bookmark('H. Metodología de para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
            .checked{font-size:17px;font-family:dejavusans;}
            .pspaces{font-size:0.2px;}
        </style>
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
        <p class="tdtext"><u><i>Reconocimiento</i></u></p>
        <p class="tdtext tejus">El reconocimiento inicial debe realizarse en forma previa a la aplicación de la medición del nivel sonoro emitido por una fuente fija, con el propósito de recabar la información técnica y administrativa y para localizar las Zonas Críticas.</p>
        <p class="tdtext tejus">La información por recabar es la siguiente:</p>

        <p class="tdtext tejus sangria">a) Croquis que muestre la ubicación del predio donde se encuentre la fuente fija y la descripción de los predios con quien colinde. Ver figura No. 1 del Anexo 1 de la presente norma oficial mexicana.</p>
        <p class="tdtext tejus sangria">b) Descripción de las actividades potencialmente ruidosas.</p>
        <p class="tdtext tejus sangria">c) Relacionar y representar en un croquis interno de la fuente fija el equipo, la maquinaria y/o los procesos potencialmente emisores de ruido.</p>

        <p class="tdtext tejus">Con el sonómetro funcionando, realizar un recorrido por la parte externa de las colindancias de la fuente fija con el objeto de localizar la Zona Crítica o zonas críticas de medición. </p>

        <p class="tdtext tejus">Dentro de cada Zona Crítica (ZC) se ubicarán 5 puntos distribuidos vertical y/u horizontalmente en forma aleatoria a 0.30 m de distancia del límite de la fuente y a no menos de 1.2 m del nivel del piso. </p>

        <p class="tdtext tejus">Ubicados los puntos de medición conforme a lo señalado se deberá realizar la medición de campo de forma semicontinua, teniendo en cuenta las condiciones normales de operación de la fuente fija.</p>
        
        <p class="tdtext tejus"><u><i>Medición del Ruido de Fuente (ruido generado por la fuente fija).</i></u></p>
        <p class="tdtext tejus">De acuerdo con el procedimiento descrito en él reconocimiento se elige la zona y el horario crítico donde la fuente fija produzca los niveles máximos de emisión.<br><br>Durante el lapso de emisión máxima se elige un período no inferior a 15 minutos para la medición.<br<br>>En la zona de emisión máxima se ubicarán aleatoriamente no menos de 5 puntos. Se aconseja describir los puntos con las letras (A, B, C, D y E) para su identificación. La zona de emisión máxima se identificará con las siglas ZC y se agregará un número progresivo en el caso de encontrar más zonas de emisión máxima (ZC1, ZC2, etc.).<br><br>Se ajusta el sonómetro con el selector de la escala A y con el selector de integración lenta.<br><br>En caso de que el efecto del viento sobre la membrana del micrófono sea notorio se debe cubrir ésta con una pantalla contra el viento.</p>
        <p></p><p><br></p>

        <p class="tdtext tejus">Debe colocarse el sonómetro o el micrófono del sonómetro en cada punto de medición apuntando hacia la fuente y efectuar en cada punto no menos de 35 lecturas, procurando obtener cada 5 segundos el valor máximo observado. Antes y después de las mediciones en cada Zona Crítica debe registrarse la señal de calibración.</p>

        <p class="tdtext tejus"><u><i>Ubicación de Puntos de Medición.</i></u></p>
        <p class="tdtext tejus">Si la fuente fija se halla limitada por confinamientos constructivos (bardas, muros, etc.), los puntos de medición deben situarse lo más cerca posible a estos elementos (a una distancia de 0.30 m), al exterior del predio, a una altura del piso no inferior a 1.20 m. Deben observarse las condiciones del elemento que produzcan los niveles máximos de emisión (ventanas, ventilas, respiraderos, puertas abiertas) si es que éstas son las condiciones normales en que opera la fuente fija.<br><br>Si el elemento constructivo no divide totalmente la fuente de su alrededor, el elemento es considerado como parcial, por lo que debe buscarse la zona de menor sombra o dispersión acústica. Si el elemento divide totalmente la fuente de su alrededor deberá realizarse la determinación de la reducción acústica.<br><br>Si la fuente fija no se halla limitada por confinamientos, pero se encuentran claramente establecidos los límites del predio (cercas, mojoneras, registros, etc.), los puntos de medición deben situarse lo más cerca posible a los límites exteriores del predio, a una altura del piso no inferior a 1.20 m.<br><br>Si la fuente fija no se halla limitada por confinamientos y no existe forma de determinar los límites del predio (maquinaria en la vía pública, por ejemplo), los puntos de medición deben situarse a un 1 m de distancia de ésta, a una altura del piso no inferior a 1.20 m.
        </p>

        <p class="tdtext tejus"><u><i>Medición del Ruido de Fondo (ruido ajeno a la fuente fija).</i></u></p>
        <p class="tdtext tejus">Deben elegirse por lo menos 5 puntos aleatorios alrededor de la fuente y a una distancia no menor de 3.5 m, apuntando en dirección contraria a dicha fuente. Se aconseja describir los puntos con los números romanos (I, II, III, IV y V) para su identificación.<br><br>Debe medirse el nivel sonoro de fondo en cada uno de los puntos determinados conforme a las mediciones semicontinuas.
        </p>

        <p class="tdtext tejus"><u><i>Determinación de la reducción acústica de un elemento constructivo en una Zona Critica.</i></u></p>
        <p class="tdtext tejus">Para determinar el aislamiento producido por un elemento constructivo común a la fuente fija y a un recinto aledaño debe procederse como sigue:<br><br>Elegir 5 puntos en el interior de la fuente a 2 m de distancia del elemento constructivo común coincidente con alguna de las zonas críticas medidas y realizar la medición de conformidad a las mediciones semicontinuas, dirigiendo el micrófono o el sonómetro hacia los generadores.
        </p>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->AddPage('P', 'A4');
        $pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0)); //trar los intrumentos utilizados en levantamiento
        $htmlg='<style type="text/css">
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td,.tableb th{border:1px solid #808080;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
                .ft13{font-size:13px;}
                .ft11{font-size:11.5px;} 
                .div_diso{ float:left !important; }
                .pspaces{ font-size:0.5px; }
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
                .tableb tr th{background-color:rgb(217,217,217);}
            </style>
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table cellpadding="5" align="center" class="tableb ft13" width="100%">';

        //trae los equipos utilizados
        $getson = $this->ModeloNom->getEquiposNom81($idnom);
        foreach($getson as $s){
            $id_sonometro=$s->id_sonometro;
            $id_calibrador=$s->id_calibrador;
            $htmlg.='
                <tr><th></th><th><i>Sonómetro Integrador </i></th><th><i>Calibrador Acústico</i></th></tr>
                <tr><th><i>Marca</i></th><td>'.$s->marca.'</td><td>'.$s->marcacal.'</td></tr>
                <tr><th><i>Modelo</i></th><td>'.$s->modelo.'</td><td>'.$s->modelocal.'</td></tr>
                <tr><th><i>Número de Serie</i></th><td>'.$s->equipo.'</td><td>'.$s->equipocal.'</td></tr>
                <tr><th><i>No. de Informe de calibración</i></th><td class="ft11">'.$s->no_informe_calibracion.'</td><td class="ft11" >'.$s->no_informe_calibracioncal.'</td></tr>
                <tr><th><i>Fecha de calibración</i></th><td>'.$s->fecha_calibracion.'</td><td>'.$s->fecha_calibracioncal.'</td></tr>';
        }
        $htmlg.='</table><p></p>';

        $pdf->Bookmark('J. Informe descriptivo de las condiciones de operación -----------------------------------------------', 0, 0, '', 'B', array(0,0,0));
        $htmlg.='<p class="titulosa"><b>J. INFORME DESCRIPTIVO DE LAS CONDICIONES DE OPERACIÓN</b></p>
                
                <p class="tdtext">'.$txt_desc_opera.'</p>';
        
        $pdf->Bookmark('K. Eventualidades descriptivas -------------------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='
                <p class="titulosa"><b>K. EVENTUALIDADES DESCRIPTIVAS</b></p>
        <p class="tdtext">'.$txt_eventualidades.'</p>';

        $pdf->Bookmark('L. Adiciones, desviaciones, o exclusiones del método --------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='
                <p class="titulosa"><b>L. ADICIONES, DESVIACIONES O EXCLUSIONES DEL MÉTODO</b></p>
        <p class="tdtext">'.$txt_adiciones_desv.'</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0,0,0)); 
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';

$pdf->AddPage('P', 'A4');
$dc=$datosrec->row();
$priero="-----";
$segdo="-----";
$tercero="-----";
$otro="-----";
$pero_dias="-----";
$sdo_dias="-----";
$tero_dias="-----";
$otro_dias="-----";
$horario_critico="-----";
$zona_ubica="-----";

if($dc->priero!="")
    $priero=$dc->priero;
if($dc->segdo!="")
    $segdo=$dc->segdo;
if($dc->tercero!="")
    $tercero=$dc->tercero;
if($dc->otro!="")
    $otro=$dc->otro;
if($dc->pero_dias!="")
    $pero_dias=$dc->pero_dias;
if($dc->sdo_dias!="")
    $sdo_dias=$dc->sdo_dias;
if($dc->tero_dias!="")
    $tero_dias=$dc->tero_dias;
if($dc->otro_dias!="")
    $otro_dias=$dc->otro_dias;
if($dc->horario_critico!="")
    $horario_critico=$dc->horario_critico;
if($dc->zona_ubica!="")
    $zona_ubica=$dc->zona_ubica;

$desc_norte="-----";
$nivel_sonoro_norte="-----";
$desc_actividades_norte="-----";
$desc_sur="-----";
$nivel_sonoro_sur="-----";
$desc_actividades_sur="-----";
$desc_este="-----";
$nivel_sonoro_este="-----";
$desc_actividades_este="-----";
$desc_oeste="-----";
$nivel_sonoro_oeste="-----";
$desc_actividades_oeste="-----";
$no_zc1="-----";
$colinda_zc1="-----";
$justifica_zc="-----";
$no_zc2="-----";
$colinda_zc2="-----";
$no_zc3="-----";
$colinda_zc3="-----";
$no_zc4="-----";
$colinda_zc4="-----";
$croquis="-----";
$justifica_ubica="-----";
$observaciones="-----";

if($dc->desc_norte!="")
    $desc_norte=$dc->desc_norte;
if($dc->nivel_sonoro_norte!="")
    $nivel_sonoro_norte=$dc->nivel_sonoro_norte;
if($dc->nivel_sonoro_norte=="0")
    $nivel_sonoro_norte="Sin acceso";
if($dc->desc_actividades_norte!="")
    $desc_actividades_norte=$dc->desc_actividades_norte;
if($dc->desc_sur!="")
    $desc_sur=$dc->desc_sur;
if($dc->nivel_sonoro_sur!="")
    $nivel_sonoro_sur=$dc->nivel_sonoro_sur;
if($dc->nivel_sonoro_sur=="0")
    $nivel_sonoro_sur="Sin acceso";

if($dc->desc_actividades_sur!="")
    $desc_actividades_sur=$dc->desc_actividades_sur;
if($dc->desc_este!="")
    $desc_este=$dc->desc_este;

if($dc->nivel_sonoro_este!="")
    $nivel_sonoro_este=$dc->nivel_sonoro_este;
if($dc->nivel_sonoro_este=="0")
    $nivel_sonoro_este="Sin acceso";

if($dc->desc_actividades_este!="")
    $desc_actividades_este=$dc->desc_actividades_este;
if($dc->desc_oeste!="")
    $desc_oeste=$dc->desc_oeste;
if($dc->nivel_sonoro_oeste!="")
    $nivel_sonoro_oeste=$dc->nivel_sonoro_oeste;
if($dc->nivel_sonoro_oeste=="0")
    $nivel_sonoro_oeste="Sin acceso";
if($dc->desc_actividades_oeste!="")
    $desc_actividades_oeste=$dc->desc_actividades_oeste;
if($dc->no_zc1!="")
    $no_zc1=$dc->no_zc1;
if($dc->colinda_zc1!="")
    $colinda_zc1=$dc->colinda_zc1;
if($dc->justifica_zc!="")
    $justifica_zc=$dc->justifica_zc;
if($dc->no_zc2!="")
    $no_zc2=$dc->no_zc2;
if($dc->colinda_zc2!="")
    $colinda_zc2=$dc->colinda_zc2;
if($dc->no_zc3!="")
    $no_zc3=$dc->no_zc3;
if($dc->colinda_zc3!="")
    $colinda_zc3=$dc->colinda_zc3;
if($dc->no_zc4!="")
    $no_zc4=$dc->no_zc4;
if($dc->colinda_zc4!="")
    $colinda_zc4=$dc->colinda_zc4;
if($dc->croquis!="")
    $croquis=$dc->croquis;
if($dc->justifica_ubica!="")
    $justifica_ubica=$dc->justifica_ubica;
if($dc->observaciones!="")
    $observaciones=$dc->observaciones;

if(isset($firmaemple) && $firmaemple!="data:"){
    $firm_txt = FCPATH."uploads/firmas/".$firmaemple;
    $handle = @fopen($firm_txt, 'r');
    if($handle){
        $fh = fopen(FCPATH."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh); 
    }else{
        $linea="#";
    }  
    /*$fh = fopen(base_url()."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
    $linea = fgets($fh);
    fclose($fh);     */
}else{
    $linea='#';
}
if(isset($firmaresp) && $firmaresp!="" && $firmaresp!="data:"){
    $firm_txt2 = FCPATH."uploads/firmas/".$firmaresp;
    $handle2 = @fopen($firm_txt2, 'r');
    if($handle2){
        $fh2 = fopen(FCPATH."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
        $firmaresp_line = fgets($fh2);
        fclose($fh2); 
    }else{
        $firmaresp_line="";
    }
    /*$fh2 = fopen(base_url()."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
    $firmaresp_line = fgets($fh2);
    fclose($fh2);     */
}else{
    $firmaresp_line='';
}

$htmlg='<style type="text/css"> 
    .borderbottom{border-bottom:1px solid black;}
    .backg{background-color:rgb(217,217,217);}
    .alg{ text-align:center; }
    .ft9{ font-size:9px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
    .pspaces{font-size:0.2px;}
    .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:9px;}
    .tableb td{font-size:9px;valign: middle;vertical-align: middle;}
    .tableb tr th{background-color:rgb(217,217,217);}
 </style>';
$htmlg.='
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL '.$norma_name.'</td>
                <td>REG-TEC/01-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>
        <table border="0" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="20%"><b>No. DE INFORME</b></td>
                <td width="50%" class="borderbottom">'.$dc->num_informe_rec.'</td>
                <td width="10%"><b>FECHA:</b></td>
                <td width="20%" class="borderbottom">'.$dc->fecha.'</td>
            </tr>
            <tr>
                <td width="20%"><b>RAZÓN SOCIAL</b></td>
                <td width="80%" class="borderbottom">'.$dc->cliente.'</td>
            </tr>
            <tr >
                <td width="60%" class="borderbottom">'.$dc->calle_num.'</td>
                <td width="40%" class="borderbottom">'.$dc->colonia.'</td>
            </tr>
            <tr>
                <td width="60%" ><b>CALLE Y NÚMERO</b></td>
                <td width="40%" ><b>COLONIA</b></td>
            </tr>
            <tr >
                <td width="34%" class="borderbottom">'.$dc->poblacion.'</td>
                <td width="33%" class="borderbottom">'.mb_strtoupper($estado_name,"UTF-8").'</td>
                <td width="33%" class="borderbottom">'.$dc->cp.'</td>
            </tr>
            <tr>
                <td width="34%" ><b>MUNICIPIO</b></td>
                <td width="33%" ><b>ESTADO</b></td>
                <td width="33%" ><b>CÓDIGO POSTAL</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->rfc.'</td>
                <td width="50%" class="borderbottom">'.$dc->giro.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>RFC</b></td>
                <td width="50%" ><b>GIRO DE LA EMPRESA</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->telefono.'</td>
                <td width="50%" class="borderbottom">'.$dc->representa.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>NÚMERO TELEFÓNICO</b></td>
                <td width="50%" ><b>REPRESENTANTE LEGAL</b></td>
            </tr>
            <tr>
                <td width="100%" class="borderbottom">'.$dc->nom_cargo.'</td>
            </tr>
            <tr>
                <td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td>
            </tr>
        </table>    
        <p></p>            
        <table cellpadding="5" border="1" class="tableb" align="center"> 
            <tr>
                <th colspan="8">TURNOS, HORARIOS Y DÍAS DE OPERACIÓN DE LA FUENTE FIJA</th>
            </tr>
            <tr>
                <th width="10%">1 ro</th>
                <td width="15%">'.$priero.'</td>
                <th width="10%">2 do</th>
                <td width="15%">'.$segdo.'</td>
                <th width="10%">3 ro</th>
                <td width="15%">'.$tercero.'</td>
                <th width="10%">Otro</th>
                <td width="15%">'.$otro.'</td>
            </tr>
            <tr>
                <th>Días</th>
                <td>'.$pero_dias.'</td>
                <th>Días</th>
                <td>'.$sdo_dias.'</td>
                <th>Días</th>
                <td>'.$tero_dias.'</td>
                <th>Días</th>
                <td>'.$otro_dias.'</td>
            </tr>
        </table>
        <table cellpadding="5" border="1" class="tableb">
            <tr align="center">
                <th><b>HORARIO CRÍTICO DONDE LA FUENTE FIJA PRODUZCA LOS NIVELES MÁXIMOS DE EMISIÓNA </b></th>
            </tr>
            <tr>
                <td>'.$horario_critico.'</td>
            </tr>
            <tr align="center">
                <th><b>ZONA DONDE SE UBICA LA FUENTE FIJA </b></th>
            </tr>
            <tr>
                <td>'.$zona_ubica.'</td>
            </tr>
        </table>
        <table border="1" cellpadding="5" align="center" class="tableb">
            <thead>
                <tr>
                    <th colspan="4">DESCRIPCIÓN DE COLINDANCIAS Y FUENTES EMISORAS DE RUIDO EN LA PERIFERIA DE LA FUENTE FIJA</th>
                </tr>
                <tr>
                    <th width="10%"><p class="pspaces"></p>DIRECCIÓN</th>
                    <th width="25%"><p class="pspaces"></p>DESCRIPCIÓN DE LA COLINDANCIA</th>
                    <th width="25%">NIVEL SONORO MÁXIMO ENCONTRADO EN LA COLINDANCIA (dB "A")</th>
                    <th width="40%">DESCRIPCIÓN DE LAS ACTIVIDADES POTENCIALMENTE RUIDOSAS (EQUIPO, MÁQUINARIA Y/O PROCESOS)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th width="10%">NORTE</th>
                    <td width="25%">'.$desc_norte.'</td>
                    <td width="25%">'.$nivel_sonoro_norte.'</td>
                    <td width="40%">'.$desc_actividades_norte.'</td>
                </tr>
                <tr>
                    <th>SUR</th>
                    <td>'.$desc_sur.'</td>
                    <td>'.$nivel_sonoro_sur.'</td>
                    <td>'.$desc_actividades_sur.'</td>
                </tr>
                <tr>
                    <th>ESTE</th>
                    <td>'.$desc_este.'</td>
                    <td>'.$nivel_sonoro_este.'</td>
                    <td>'.$desc_actividades_este.'</td>
                </tr>
                <tr>
                    <th>OESTE</th>
                    <td>'.$desc_oeste.'</td>
                    <td>'.$nivel_sonoro_oeste.'</td>
                    <td>'.$desc_actividades_oeste.'</td>
                </tr>
                <tr>
                    <th colspan="4">SECCIÓN DE LA(S) ZONA(S) CRÍTICA(S) </th>
                </tr>
                <tr>
                    <th>No. ZC</th>
                    <th>COLINDANCIA</th>
                    <th colspan="2">JUSTIFICACIÓN DE LA SELECCIÓN DE LA(S) ZONA(S) CRÍTICA(S)</th>
                </tr>
                <tr>
                    <td>'.$no_zc1.'</td>
                    <td>'.$colinda_zc1.'</td>
                    <td colspan="2" rowspan="4">'.$justifica_zc.'</td>
                </tr>
                <tr>
                    <td>'.$no_zc2.'</td>
                    <td>'.$colinda_zc2.'</td>
                </tr>
                <tr>
                    <td>'.$no_zc3.'</td>
                    <td>'.$colinda_zc3.'</td>
                </tr>
                <tr>
                    <td>'.$no_zc4.'</td>
                    <td>'.$colinda_zc4.'</td>
                </tr>
            </tbody>
        </table>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->AddPage('P', 'A4');

        $htmlg='<style type="text/css"> 
                    .borderbottom{border-bottom:1px solid black;}
                    .backg{background-color:rgb(217,217,217);}
                    .alg{ text-align:center; }
                    .ft9{ font-size:9px; text-align:center; }
                    .ft7{ font-size:7px; text-align:center; }
                    .fontbold{font-weight: bold;}
                    .pspaces{font-size:0.2px;}
                    .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:9px;}
                    .tableb td{font-size:9px;valign: middle;vertical-align: middle;}
                    .tableb tr th{background-color:rgb(217,217,217);}
                </style>
            <table border="1" cellpadding="5" align="center" class="ft9">
                <tr>
                    <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
                    <td width="19%">NOMBRE DEL DOCUMENTO</td>
                    <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                    <td width="19%">VERSIÓN</td>
                    <td width="19%">No COPIA CONTROLADA</td>
                </tr>
                <tr>
                    <td>RECONOCIMIENTO INICIAL '.$norma_name.'</td>
                    <td>REG-TEC/01-01</td>
                    <td>01</td>
                    <td>ORIGINAL</td>
                </tr>
            </table>
            <p></p>
            <table border="1" cellpadding="5" align="center" class="tableb">
                <tr>
                    <th width="100%">CROQUIS DE LA UBICACIÓN DE LA FUENTE FIJA Y SUS COLINDANCIAS, MÁQUINARIA Y/O PROCESOS EMISORES DE RUIDO, VALORES OBTENIDOS EN EL RECONOCIMIENTO INICIAL, UBICACIÓN DE LA(S) ZONA(S) CRÍTICAS Y PUNTOS DE MEDICIÓN</th>
                </tr>
                <tr>
                    <td height="430px">'.$croquis.'</td>
                </tr>
                <tr>
                    <th>JUSTIFICACIÓN DE LA UBICACIÓN Y SEPARACIÓN DE LOS PUNTOS DE MEDICIÓN DE RUIDO DE FUENTE Y FONDO EN CADA ZONA CRÍTICA</th>
                </tr>
                <tr>
                    <td height="90px">'.$justifica_ubica.'</td>
                </tr>
                <tr>
                    <th>OBSERVACIONES / EVENTUALIDADES DESCRIPTTIVAS</th>
                </tr>
                <tr>
                    <td height="90px">'.$observaciones.'</td>
                </tr>

                <tr>
                    <th width="50%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></th>
                    <td width="50%"><p class="pspaces"></p><!-- '.$tecnico.' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55"> --></td>
                </tr>
                <tr>
                    <th width="50%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></th>
                    <td width="50%"><p class="pspaces"></p><!-- Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55">--> </td>
                </tr>
            </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

/********************************* */
$htmlg='';
$pdf->AddPage('P', 'A4');
$htmlg.='<style type="text/css"> 
    .ft9{ font-size:9px; text-align:center; }
    .ft8{ font-size:8px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
    .tableb td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:8.5px;
        valign: middle;
        text-align:center;
    }
    .tableb th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:9px;
    }
    .tableb6 td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:6px;
        valign: middle;
        text-align:center;
    }
    .tableb6 th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:6px;
    }
    .tableb7 td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:7px;
        valign: middle;
        text-align:center;
    }
    .tableb7 th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:7px;
    }
    .pspaces{
        font-size:0.2px;    
    }
 </style>';
/*$htmlg.='
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>HOJA DE CAMPO PARA LA MEDICIÓN DE RUIDO PERIMETRAL</td>
            <td>REG-TEC/01-02</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <table><tr><td></td></tr></table>';*/
foreach ($datosnom->result() as $dc) { 
    /*$pdf->setPrintHeader(false);
    $pdf->SetMargins(15, '10', 15);
    $pdf->AddPage('P', 'A4');*/
    if($dc->horario=="1") $horario="Diurno";
    if($dc->horario=="2") $horario="Nocturno";
    $htmlg.='
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>HOJA DE CAMPO PARA LA MEDICIÓN DE RUIDO PERIMETRAL</td>
            <td>REG-TEC/01-02</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <table><tr><td></td></tr></table>';
    $htmlg.='
    <table cellpadding="5" class="tableb">
        <tr>
            <th>NO. DE INFORME</th>
            <td colspan="3">'.$dc->num_informe.'</td>
            <th colspan="2">FECHA DE MEDICIÓN</th>
            <td colspan="2">'.$dc->fecha.'</td>
        </tr>   
        <tr>
            <th>RAZÓN SOCIAL</th>
            <td colspan="7">'.$dc->razon_social.'</td>
        </tr>
        <tr>
            <th width="15%">LUGAR</th>
            <td width="30%" colspan="3">'.$dc->lugar.'</td>
            <th width="20%">HORARIO DE MEDICIÓN</th>
            <td width="15%">'.$horario.'</td>
            <th width="10%">ZONA CRÍTICA</th>
            <td width="10%">'.$dc->zona_critica.'</td>
        </tr>';
        $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$dc->id_sonometro));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg.='<tr>
                <th width="5%">ID</th>
                <td width="20%">'.$iteme->luxometro.'</td>
                <th width="10%">MARCA</th>
                <td width="15%">'.$iteme->marca.'</td>
                <th width="10%">MODELO</th>
                <td width="15%">'.$iteme->modelo.'</td>
                <th width="10%">SERIE</th>
                <td width="15%">'.$iteme->equipo.'</td>
            </tr>';
        }
        $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$dc->id_calibrador));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg.='<tr>
                <th width="5%">ID</th>
                <td width="20%">'.$iteme->luxometro.'</td>
                <th width="10%">MARCA</th>
                <td width="15%">'.$iteme->marca.'</td>
                <th width="10%">MODELO</th>
                <td width="15%">'.$iteme->modelo.'</td>
                <th width="10%">SERIE</th>
                <td width="15%">'.$iteme->equipo.'</td>
            </tr>';
        }

    $htmlg.='</table>
    <table  cellpadding="5" class="tableb" width="100%">
        <tr>
            <th width="15%">VERIFICACIÓN INICIAL</th>
            <th width="15%">VERIFICACIÓN FINAL</th>
            <th width="15%">CRITERIO DE ACEPTACIÓN</th>
            <th width="20%">¿CUMPLE CRITERIO DE ACEPTACIÓN?</th>
            <th width="35%" colspan="4">POTENCIA DE BATERIA</th>
        </tr>
        <tr>
            <td width="15%">'.$dc->veri_ini.'</td>
            <td width="15%">'.$dc->veri_fin.'</td>
            <td width="15%">'.$dc->criterio.'</td>
            <td width="20%">'.$dc->cumple_criterio.'</td>

            <th width="8.75%">Inicio</th>
            <td width="8.75%">'.$dc->pot_bat_ini.'V</td>
            <th width="8.75%">Final</th>
            <td width="8.75%">'.$dc->pot_bat_fin.'V</td>
        </tr>
    </table>
    <table  cellpadding="5" class="tableb" width="100%">
        <tr>
            <th width="20%">ELEMENTO CONSTRUCTIVO</th>
            <th width="15%">LARGO (m)</th>
            <td width="10%">'.$dc->ec_largo.'</td>
            <th width="15%">ANCHO (m)</th>
            <td width="10%">'.$dc->ec_ancho.'</td>
            <th width="15%">ÁREA (m2)</th>
            <td width="15%">'.$dc->ec_largo*$dc->ec_ancho.'</td>
        </tr>
        <tr>
            <th width="20%">OBSERVACIONES</th>
            <td colspan="6" width="80%">'.$dc->observaciones.'</td>
        </tr>
    </table>';

    $get_dist=$this->ModeloCatalogos->getselectwheren('distancias_nom81',array('id_nom'=>$dc->idnom));  
    if($get_dist->num_rows()>0){
        $gd = $get_dist->row();
        $id_dist=$gd->id;
        $altura_fte=$gd->altura_fte; $distancia_fte=$gd->distancia_fte;
        $altura_fondo=$gd->altura_fondo; $distancia_fondo=$gd->distancia_fondo;
        $altura_reduc=$gd->altura_reduc; $distancia_reduc=$gd->distancia_reduc;
    }else{
        $id_dist="0";
        $altura_fte=""; $distancia_fte="";
        $altura_fondo=""; $distancia_fondo="";
        $altura_reduc=""; $distancia_reduc="";
    }
    $htmlg.='
        <table class="tableb ft9" width="100%">
            <thead>
                <tr>
                    <th colspan="4"><b>DISTANCIA DEL SONOMETRO</b></th>
                </tr>
                <tr>
                   <th rowspan="2"><p class="pspaces"></p>Fuente</th>
                    <td>Altura:</td>
                    <td>'.$altura_fte.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_fte.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                   <th rowspan="2"><p class="pspaces"></p>Fondo</th>
                    <td>Altura:</td>
                    <td>'.$altura_fondo.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_fondo.'</td>
                    <td>m</td>
                </tr>  
                <tr>
                    <th rowspan="2"><p class="pspaces"></p>Reducción acústica</th>
                    <td>Altura:</td>
                    <td>'.$altura_reduc.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_reduc.'</td>
                    <td>m</td>
                </tr> 
            </thead>
        </table>';

    $get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE FUENTE (dB "A")</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_fte->num_rows()>0){
        foreach ($get_fte->result() as $f) {
            $txt_tda.='<td>'.$f->a.'</td>';
            $txt_tdb.='<td>'.$f->b.'</td>';
            $txt_tdc.='<td>'.$f->c.'</td>';
            $txt_tdd.='<td>'.$f->d.'</td>';
            $txt_tde.='<td>'.$f->e.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>A</th>'.$txt_tda.'</tr>
        <tr><th>B</th>'.$txt_tdb.'</tr>
        <tr><th>C</th>'.$txt_tdc.'</tr>
        <tr><th>D</th>'.$txt_tdd.'</tr>
        <tr><th>E</th>'.$txt_tde.'</tr>
    </table>';

    $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE FONDO (dB "A")</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_fondo->num_rows()>0){
        foreach ($get_fondo->result() as $f) {
            $txt_tda.='<td>'.$f->uno.'</td>';
            $txt_tdb.='<td>'.$f->dos.'</td>';
            $txt_tdc.='<td>'.$f->dos.'</td>';
            $txt_tdd.='<td>'.$f->cuatro.'</td>';
            $txt_tde.='<td>'.$f->cinco.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>I</th>'.$txt_tda.'</tr>
        <tr><th>II</th>'.$txt_tdb.'</tr>
        <tr><th>III</th>'.$txt_tdc.'</tr>
        <tr><th>IV</th>'.$txt_tdd.'</tr>
        <tr><th>V</th>'.$txt_tde.'</tr>
    </table>';

    $hi=""; $hf=""; $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_aisla->num_rows()>0){ $gtas=$get_aisla->row(); $hi=$gtas->hora_inicio; $hf=$gtas->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE AISLAMIENTO</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_aisla->num_rows()>0){
        foreach ($get_aisla->result() as $f) {
            $txt_tda.='<td>'.$f->a.'</td>';
            $txt_tdb.='<td>'.$f->b.'</td>';
            $txt_tdc.='<td>'.$f->c.'</td>';
            $txt_tdd.='<td>'.$f->d.'</td>';
            $txt_tde.='<td>'.$f->e.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>a</th>'.$txt_tda.'</tr>
        <tr><th>b</th>'.$txt_tdb.'</tr>
        <tr><th>c</th>'.$txt_tdc.'</tr>
        <tr><th>d</th>'.$txt_tdd.'</tr>
        <tr><th>e</th>'.$txt_tde.'</tr>
    </table>
    <table><tr><td height="200px"></td></tr></table>';
}
$pdf->writeHTML($htmlg, true, false, true, false, '');  
/************************** *******************************/

$pdf->AddPage('P', 'A4');
$htmlg="";
$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27,15);
$pdf->Bookmark('II. Planos ----------------------------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table>
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO II</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PLANOS</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg=''; $htmlg2=""; $htmlg3="";
$cont_plan=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==1){
        $pdf->AddPage('P', 'A4');
        $cont_plan++;
        /*$htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de ubicación de la fuente fija y sus colindancias</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'" ></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de ubicación de la fuente fija y sus colindancias</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if($cont_plan==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la fuente fija y sus colindancias</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan2=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==2){
        $pdf->AddPage('P', 'A4');
        $cont_plan2++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y/o procesos emisores de ruido y niveles sonoros detectados en el reconocimiento inicial</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y/o procesos emisores de ruido y niveles sonoros detectados en el reconocimiento inicial</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if($cont_plan2==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y/o procesos emisores de ruido y niveles sonoros detectados en el reconocimiento inicial</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan3=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==3){
        $pdf->AddPage('P', 'A4');
        $cont_plan3++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la(s) zona(s) crítica(s) de los puntos de medición</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la(s) zona(s) crítica(s) de los puntos de medición</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if($cont_plan3==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la(s) zona(s) crítica(s) de los puntos de medición</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$htmlg.='';
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo --------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO III</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">SECUENCIA DE CÁLCULO </td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

/*$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);
$pdf->AddPage('P', 'A4');

$htmlg=''; $htmlg2='';
$htmlg.='<style type="text/css"> 
    .ft9{ font-size:9px; text-align:center; }
    .ft8{ font-size:8px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
    .tableb td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:8.5px;
        valign: middle;
        text-align:center;
    }
    .tableb th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:9px;
    }
    .tableb6 td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:6px;
        valign: middle;
        text-align:center;
    }
    .tableb6 th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:6px;
    }
    .tableb7 td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:7px;
        valign: middle;
        text-align:center;
    }
    .tableb7 th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; 
        font-size:7px;
    }
    .pspaces{
        font-size:0.2px;    
    }
 </style>';
$htmlg.='
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>HOJA DE CAMPO PARA LA MEDICIÓN DE RUIDO PERIMETRAL</td>
            <td>REG-TEC/01-02</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <table><tr><td></td></tr></table>';
foreach ($datosnom->result() as $dc) { 
    if($dc->horario=="1") $horario="Diurno";
    if($dc->horario=="2") $horario="Nocturno";
    $htmlg.='
    <table cellpadding="5" class="tableb">
        <tr>
            <th>NO. DE INFORME</th>
            <td colspan="3">'.$dc->num_informe.'</td>
            <th colspan="2">FECHA DE MEDICIÓN</th>
            <td colspan="2">'.$dc->fecha.'</td>
        </tr>   
        <tr>
            <th>RAZÓN SOCIAL</th>
            <td colspan="7">'.$dc->razon_social.'</td>
        </tr>
        <tr>
            <th width="15%">LUGAR</th>
            <td width="30%" colspan="3">'.$dc->lugar.'</td>
            <th width="20%">HORARIO DE MEDICIÓN</th>
            <td width="15%">'.$horario.'</td>
            <th width="10%">ZONA CRÍTICA</th>
            <td width="10%">'.$dc->zona_critica.'</td>
        </tr>';
        $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$dc->id_sonometro));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg.='<tr>
                <th width="5%">ID</th>
                <td width="20%">'.$iteme->luxometro.'</td>
                <th width="10%">MARCA</th>
                <td width="15%">'.$iteme->marca.'</td>
                <th width="10%">MODELO</th>
                <td width="15%">'.$iteme->modelo.'</td>
                <th width="10%">SERIE</th>
                <td width="15%">'.$iteme->equipo.'</td>
            </tr>';
        }
        $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$dc->id_calibrador));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg.='<tr>
                <th width="5%">ID</th>
                <td width="20%">'.$iteme->luxometro.'</td>
                <th width="10%">MARCA</th>
                <td width="15%">'.$iteme->marca.'</td>
                <th width="10%">MODELO</th>
                <td width="15%">'.$iteme->modelo.'</td>
                <th width="10%">SERIE</th>
                <td width="15%">'.$iteme->equipo.'</td>
            </tr>';
        }

    $htmlg.='</table>
    <table  cellpadding="5" class="tableb" width="100%">
        <tr>
            <th width="15%">VERIFICACIÓN INICIAL</th>
            <th width="15%">VERIFICACIÓN FINAL</th>
            <th width="15%">CRITERIO DE ACEPTACIÓN</th>
            <th width="20%">¿CUMPLE CRITERIO DE ACEPTACIÓN?</th>
            <th width="35%" colspan="4">POTENCIA DE BATERIA</th>
        </tr>
        <tr>
            <td width="15%">'.$dc->veri_ini.'</td>
            <td width="15%">'.$dc->veri_fin.'</td>
            <td width="15%">'.$dc->criterio.'</td>
            <td width="20%">'.$dc->cumple_criterio.'</td>

            <th width="8.75%">Inicio</th>
            <td width="8.75%">'.$dc->pot_bat_ini.'V</td>
            <th width="8.75%">Final</th>
            <td width="8.75%">'.$dc->pot_bat_fin.'V</td>
        </tr>
    </table>
    <table  cellpadding="5" class="tableb" width="100%">
        <tr>
            <th width="20%">ELEMENTO CONSTRUCTIVO</th>
            <th width="15%">LARGO (m)</th>
            <td width="10%">'.$dc->ec_largo.'</td>
            <th width="15%">ANCHO (m)</th>
            <td width="10%">'.$dc->ec_ancho.'</td>
            <th width="15%">ÁREA (m2)</th>
            <td width="15%">'.$dc->ec_largo*$dc->ec_ancho.'</td>
        </tr>
        <tr>
            <th width="20%">OBSERVACIONES</th>
            <td colspan="6" width="80%">'.$dc->observaciones.'</td>
        </tr>
    </table>';

    $get_dist=$this->ModeloCatalogos->getselectwheren('distancias_nom81',array('id_nom'=>$dc->idnom));  
    if($get_dist->num_rows()>0){
        $gd = $get_dist->row();
        $id_dist=$gd->id;
        $altura_fte=$gd->altura_fte; $distancia_fte=$gd->distancia_fte;
        $altura_fondo=$gd->altura_fondo; $distancia_fondo=$gd->distancia_fondo;
        $altura_reduc=$gd->altura_reduc; $distancia_reduc=$gd->distancia_reduc;
    }else{
        $id_dist="0";
        $altura_fte=""; $distancia_fte="";
        $altura_fondo=""; $distancia_fondo="";
        $altura_reduc=""; $distancia_reduc="";
    }
    $htmlg.='
        <table class="tableb ft9" width="100%">
            <thead>
                <tr>
                    <th colspan="4"><b>DISTANCIA DEL SONOMETRO</b></th>
                </tr>
                <tr>
                   <th rowspan="2"><p class="pspaces"></p>Fuente</th>
                    <td>Altura:</td>
                    <td>'.$altura_fte.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_fte.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                   <th rowspan="2"><p class="pspaces"></p>Fondo</th>
                    <td>Altura:</td>
                    <td>'.$altura_fondo.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_fondo.'</td>
                    <td>m</td>
                </tr>  
                <tr>
                    <th rowspan="2"><p class="pspaces"></p>Reducción acústica</th>
                    <td>Altura:</td>
                    <td>'.$altura_reduc.'</td>
                    <td>m</td>
                </tr> 
                <tr>
                    <td>Distancia:</td>
                    <td>'.$distancia_reduc.'</td>
                    <td>m</td>
                </tr> 
            </thead>
        </table>';

    $get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE FUENTE (db "A")</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_fte->num_rows()>0){
        foreach ($get_fte->result() as $f) {
            $txt_tda.='<td>'.$f->a.'</td>';
            $txt_tdb.='<td>'.$f->b.'</td>';
            $txt_tdc.='<td>'.$f->c.'</td>';
            $txt_tdd.='<td>'.$f->d.'</td>';
            $txt_tde.='<td>'.$f->e.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>A</th>'.$txt_tda.'</tr>
        <tr><th>B</th>'.$txt_tdb.'</tr>
        <tr><th>C</th>'.$txt_tdc.'</tr>
        <tr><th>D</th>'.$txt_tdd.'</tr>
        <tr><th>E</th>'.$txt_tde.'</tr>
    </table>';

    $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE FONDO (db "A")</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_fondo->num_rows()>0){
        foreach ($get_fondo->result() as $f) {
            $txt_tda.='<td>'.$f->uno.'</td>';
            $txt_tdb.='<td>'.$f->dos.'</td>';
            $txt_tdc.='<td>'.$f->dos.'</td>';
            $txt_tdd.='<td>'.$f->cuatro.'</td>';
            $txt_tde.='<td>'.$f->cinco.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>I</th>'.$txt_tda.'</tr>
        <tr><th>II</th>'.$txt_tdb.'</tr>
        <tr><th>III</th>'.$txt_tdc.'</tr>
        <tr><th>IV</th>'.$txt_tdd.'</tr>
        <tr><th>V</th>'.$txt_tde.'</tr>
    </table>';

    $hi=""; $hf=""; $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_aisla->num_rows()>0){ $gtas=$get_aisla->row(); $hi=$gtas->hora_inicio; $hf=$gtas->hora_fin; }
    $txt_tda="";$txt_tdb="";$txt_tdc="";$txt_tdd="";$txt_tde="";

    $htmlg.='<table><tr><td></td></tr></table>
    <table cellpadding="5" class="tableb" width="100%">
        <thead>
            <tr>
                <th colspan="36"><b>RUIDO DE AISLAMIENTO</b></th>
            </tr>
        </thead>
        <tr>
            <th width="15%" colspan="8">Hora inicio</th>
            <td width="35%" colspan="10">'.$hi.'</td>
            <th width="15%" colspan="8">Hora final</th>
            <td width="35%" colspan="10">'.$hf.'</td>
        </tr>
    </table>
    <table class="tableb7" width="100%">
        <tr>
            <th width="5%">No. lectura</th>';
        for ($j=1; $j<=35 ; $j++) { 
            $htmlg.='<th width="2.714%">'.$j.'</th>';
        }

    if($get_aisla->num_rows()>0){
        foreach ($get_aisla->result() as $f) {
            $txt_tda.='<td>'.$f->a.'</td>';
            $txt_tdb.='<td>'.$f->b.'</td>';
            $txt_tdc.='<td>'.$f->c.'</td>';
            $txt_tdd.='<td>'.$f->d.'</td>';
            $txt_tde.='<td>'.$f->e.'</td>';
        }
    }
    $htmlg.='</tr>
        <tr><th>a</th>'.$txt_tda.'</tr>
        <tr><th>b</th>'.$txt_tdb.'</tr>
        <tr><th>c</th>'.$txt_tdc.'</tr>
        <tr><th>d</th>'.$txt_tdd.'</tr>
        <tr><th>e</th>'.$txt_tde.'</tr>
    </table>
    <table><tr><td height="215px"></td></tr></table>';

    /*$get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }
    $htmlg.='<table><tr><td></td></tr></table>
    <table width="100%">
        <thead>
            <tr class="tableb ft8">
                <th>RUIDO DE FUENTE (db "A")</th>
                <th>RUIDO DE FONDO (db "A")</th>
                <th>RUIDO DE AISLAMIENTO</th>
            </tr>
        </thead>
        <tr>
            <td width="33.33%">
                <table cellpadding="2" class="tableb ft8" width="100%">
                    <tr>
                        <th width="15%">Hora inicio</th>
                        <td width="35%" colspan="2">'.$hi.'</td>
                        <th width="15%">Hora final</th>
                        <td width="35%" colspan="2">'.$hf.'</td>
                    </tr>
                    <tr>
                        <th>No. lectura</th>
                        <th>A</th>
                        <th>B</th>
                        <th>C</th>
                        <th>D</th>
                        <th>E</th>
                    </tr>';
                
                    $cont=0;
                    if($get_fte->num_rows()>0){
                        foreach ($get_fte->result() as $f) { $cont++;
                            $htmlg.='
                            <tr>
                                <td>'.$cont.'</td>
                                <td>'.$f->a.'</td>
                                <td>'.$f->b.'</td>
                                <td>'.$f->c.'</td>
                                <td>'.$f->d.'</td>
                                <td>'.$f->e.'</td>
                            </tr>';
                        }
                    }
                $htmlg.='
                </table>
            </td>';
            $hi=""; $hf=""; $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
            if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
        $htmlg.='
            <td width="33.33%">
                <table cellpadding="2" class="tableb ft8" width="100%">
                    <tr>
                        <th width="15%">Hora inicio</th>
                        <td width="35%" colspan="2">'.$hi.'</td>
                        <th width="15%">Hora final</th>
                        <td width="35%" colspan="2">'.$hf.'</td>
                    </tr>
                    <tr>
                        <th>No. lectura</th>
                        <th>I</th>
                        <th>II</th>
                        <th>III</th>
                        <th>IV</th>
                        <th>V</th>
                    </tr>';
                    $cont=0;
                    if($get_fondo->num_rows()>0){
                        foreach ($get_fondo->result() as $f) { $cont++;
                            $htmlg.='<tr>
                                <td>'.$cont.'</td>
                                <td>'.$f->uno.'</td>
                                <td>'.$f->dos.'</td>
                                <td>'.$f->tres.'</td>
                                <td>'.$f->cuatro.'</td>
                                <td>'.$f->cinco.'</td>
                            </tr>';
                            //$htmlg2.=$htmlg;
                        }
                    }
                $htmlg.='
                </table>
            </td>';

            $hi=""; $hf=""; $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
            if($get_aisla->num_rows()>0){ $gtas=$get_aisla->row(); $hi=$gtas->hora_inicio; $hf=$gtas->hora_fin; }

        $htmlg.='
            <td width="33.33%">
                <table cellpadding="2" class="tableb ft8" width="100%">
                    <tr>
                        <th width="15%">Hora inicio</th>
                        <td width="35%" colspan="2">'.$hi.'</td>
                        <th width="15%">Hora final</th>
                        <td width="35%" colspan="2">'.$hf.'</td>
                    </tr>
                    <tr>
                        <th>No. lectura</th>
                        <th>a</th>
                        <th>b</th>
                        <th>c</th>
                        <th>d</th>
                        <th>e</th>
                    </tr>';
                    $cont=0;
                    if($get_aisla->num_rows()>0){
                        foreach ($get_aisla->result() as $f) { $cont++;
                            $htmlg.='<tr>
                                <td>'.$cont.'</td>
                                <td>'.$f->a.'</td>
                                <td>'.$f->b.'</td>
                                <td>'.$f->c.'</td>
                                <td>'.$f->d.'</td>
                                <td>'.$f->e.'</td>
                            </tr>';
                        }
                    }
                $htmlg.='
                </table>
            </td>
        </tr>
    </table>';*/
/*}
$pdf->writeHTML($htmlg, true, false, true, false, '');
*/

//======================================================================================================

$pdf->setPrintHeader(true);
$pdf->SetMargins(15, '27', 15);
$pdf->AddPage('P', 'A4');

$htmlg='';
$htmlg.='<style type="text/css"> 
    .ft11{ font-size:11px; text-align:center; }
    .ft9{ font-size:9px; text-align:center; }
    .ft8{ font-size:8px; text-align:center; }
    .ft8_left{ font-size:8px; text-align:left; }
    .ft7{ font-size:7px; text-align:center; }
    .tableb td{
        border:1px solid #808080;
        font-family:Arial;text-align: center;
        font-size:9px;
        valign: middle;
        text-align:center;
    }
    .tableb th{
        background-color:rgb(217,217,217);
        border:1px solid #808080;  
        text-align:center; font-weight:bold;
        font-size:9px;
        color:black;
    }
    .tble_green{color:green;}
    .tgr{color:green;}
    .txt_red{color:red;}
    .pspaces{
        font-size:0.2px;    
    }
 </style>';

$cont_nom=0;
foreach ($datosnom->result() as $dc){
    $cont_nom++;
    $horario=$dc->horario;
    $idnom=$dc->idnom;
    if($dc->horario=="1") $horario="Diurno";
    if($dc->horario=="2") $horario="Nocturno";
    $htmlg.='
    <table cellpadding="5" class="tableb" width="100%">
        <tr>
            <th width="20%">Razón Social</th><td class="tgr" width="40%">'.$dc->razon_social.'</td>
            <th width="20%">Horario</th><td class="tble_green" width="20%">'.$horario.'</td>
        </tr>
        <tr>
            <th width="20%">Fecha de Evaluación</th><td class="tgr" width="40%">'.$dc->fecha.'</td>
            <th width="20%">ZC</th><td class="tble_green" width="20%">'.$dc->zona_critica.'</td>
        </tr>
    </table>
    <p class="pspaces"></p>';

    $hi=""; $hf=""; $get_fte=$this->ModeloCatalogos->getselectwheren('ruido_fte_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
    if($get_fte->num_rows()>0){ $gft=$get_fte->row(); $hi=$gft->hora_inicio; $hf=$gft->hora_fin; }

    $htmlg.='<table width="100%">
        <thead>
            <tr class="ft11">
                <th>MEDICIÓN DE FUENTE</th>
                <th>MEDICIÓN DE FONDO</th>
            </tr>
        </thead>
        <tr>
            <td width="50%">
                <table cellpadding="3" class="tableb " width="100%">
                    <tr>
                        <th rowspan="3"><p class="pspaces"></p>Lectura</th>
                        <th colspan="5">Punto de evaluación</th>
                    </tr>
                    <tr>
                        <th>A</th><th>B</th><th>C</th><th>D</th><th>E</th>
                    </tr>
                    <tr>
                        <th>dB(A)</th><th>dB(A)</th><th>dB(A)</th><th>dB(A)</th><th>dB(A)</th>
                    </tr>';

                    $cont=0;        
                    if($get_fte->num_rows()>0){
                        $array_lectura_a=[];
                        $array_lectura_b=[];
                        $array_lectura_c=[];
                        $array_lectura_d=[];
                        $array_lectura_e=[];

                        foreach ($get_fte->result() as $f) { $cont++;
                            $array_lectura_a[]=$f->a;
                            $array_lectura_b[]=$f->b;
                            $array_lectura_c[]=$f->c;
                            $array_lectura_d[]=$f->d;
                            $array_lectura_e[]=$f->e;

                            $htmlg.='<tr class="tgr"><th>'.$cont.'</th><td>'.$f->a.'</td><td>'.$f->b.'</td><td>'.$f->c.'</td><td>'.$f->d.'</td><td>'.$f->e.'</td></tr>';
                        }
                    }

        $htmlg.='</table>
            </td>';

        $hi=""; $hf=""; $get_fondo=$this->ModeloCatalogos->getselectwheren('ruido_fondo_nom81',array('id_nom'=>$dc->idnom,'estatus'=>1)); 
        if($get_fondo->num_rows()>0){ $gtfd=$get_fondo->row(); $hi=$gtfd->hora_inicio; $hf=$gtfd->hora_fin; }
        $htmlg.='
            <td width="50%">
                <table cellpadding="3" class="tableb " width="100%">
                    <tr>
                        <th rowspan="3"><p class="pspaces"></p>Lectura</th>
                        <th colspan="5">Punto de evaluación</th>
                    </tr>
                    <tr>
                        <th>I</th>
                        <th>II</th>
                        <th>III</th>
                        <th>IV</th>
                        <th>V</th>
                    </tr>
                    <tr>
                        <th>dB(A)</th>
                        <th>dB(A)</th>
                        <th>dB(A)</th>
                        <th>dB(A)</th>
                        <th>dB(A)</th>
                    </tr>';
                        $cont=0;
                        if($get_fondo->num_rows()>0){
                            $array_lectura_1=[];
                            $array_lectura_2=[];
                            $array_lectura_3=[];
                            $array_lectura_4=[];
                            $array_lectura_5=[];

                            foreach ($get_fondo->result() as $f) { $cont++;
                                $array_lectura_1[]=$f->uno;
                                $array_lectura_2[]=$f->dos;
                                $array_lectura_3[]=$f->tres;
                                $array_lectura_4[]=$f->cuatro;
                                $array_lectura_5[]=$f->cinco;
                                $htmlg.='<tr class="tgr"><th>'.$cont.'</th><td>'.$f->uno.'</td><td>'.$f->dos.'</td><td>'.$f->tres.'</td><td>'.$f->cuatro.'</td><td>'.$f->cinco.'</td></tr>';
                            }
                        }
                    $htmlg.='
                </table>
            </td>';

$htmlg.='</tr>
    </table>
    <!--<table><tr><td height="105px"></td></tr></table>-->';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>';

    $htmlg.='<p class="ft11"><b><u>Determinación del nivel Percentil 50 (N50) de cada punto de medición de fuente y fondo. (PROMEDIO)</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form1.jpg" height="60px"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                    <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                    <p>n =  Número de observaciones en el punto i = 35</p>
                </td>
            </tr>
        </table><p class="pspaces"></p>';

    $htmlg.='<table class="tableb ">
        <tr>
            <th></th>
            <th colspan="6">Mediciones de fuente</th>
            <th colspan="6"> Mediciones de fondo</th>
        </tr>
        <tr>
            <th>Punto:</th>           
            <th>A</th><th>B</th><th>C</th><th>D</th><th>E</th><th>Prom.</th>
            <th>I</th><th>II</th><th>III</th><th>IV</th><th>V</th><th>Prom.</th>
        </tr>';
            $medicion_fu_a = array_sum($array_lectura_a)/count($array_lectura_a); 
            $medicion_fu_b = array_sum($array_lectura_b)/count($array_lectura_b); 
            $medicion_fu_c = array_sum($array_lectura_c)/count($array_lectura_c); 
            $medicion_fu_d = array_sum($array_lectura_d)/count($array_lectura_d); 
            $medicion_fu_e = array_sum($array_lectura_e)/count($array_lectura_e);
            $prom_medic_fuente=($medicion_fu_a+$medicion_fu_b+$medicion_fu_c+$medicion_fu_d+$medicion_fu_e)/5;
            $medicion_fu_1 = array_sum($array_lectura_1)/count($array_lectura_1); 
            $medicion_fu_2 = array_sum($array_lectura_2)/count($array_lectura_2);
            $medicion_fu_3 = array_sum($array_lectura_3)/count($array_lectura_3);
            $medicion_fu_4 = array_sum($array_lectura_4)/count($array_lectura_4);
            $medicion_fu_5 = array_sum($array_lectura_5)/count($array_lectura_5);
            $prom_medic_fonto=($medicion_fu_1+$medicion_fu_2+$medicion_fu_3+$medicion_fu_4+$medicion_fu_5)/5;

        $htmlg.='<tr>
            <th>N<sub>50</sub></th>
            <td>'.round($medicion_fu_a,1).'</td><td>'.round($medicion_fu_b,1).'</td><td>'.round($medicion_fu_c,1).'</td><td>'.round($medicion_fu_d,1).'</td><td>'.round($medicion_fu_e,1).'</td><td>'.round($prom_medic_fuente,1).'</td>
            <td>'.round($medicion_fu_1,1).'</td><td>'.round($medicion_fu_2,1).'</td><td>'.round($medicion_fu_3,1).'</td><td>'.round($medicion_fu_4,1).'</td><td>'.round($medicion_fu_5,1).'</td><td>'.round($prom_medic_fonto,1).'</td>
        </tr>
    </table>';
    if($get_fte->num_rows()>0){
        $array_lectura_a_pow=[];
        $array_lectura_b_pow=[];
        $array_lectura_c_pow=[];
        $array_lectura_d_pow=[];
        $array_lectura_e_pow=[];

        $array_lectura_a_pow_10=[];
        $array_lectura_b_pow_10=[];
        $array_lectura_c_pow_10=[];
        $array_lectura_d_pow_10=[];
        $array_lectura_e_pow_10=[];
        foreach ($get_fte->result() as $f) { 
            $array_lectura_a_pow[]=pow(($f->a-$medicion_fu_a),2);
            $array_lectura_b_pow[]=pow(($f->b-$medicion_fu_b),2);
            $array_lectura_c_pow[]=pow(($f->c-$medicion_fu_c),2);
            $array_lectura_d_pow[]=pow(($f->d-$medicion_fu_d),2);
            $array_lectura_e_pow[]=pow(($f->e-$medicion_fu_e),2);

            $array_lectura_a_pow_10[]=pow(10,($f->a/10));
            $array_lectura_b_pow_10[]=pow(10,($f->b/10));
            $array_lectura_c_pow_10[]=pow(10,($f->c/10));
            $array_lectura_d_pow_10[]=pow(10,($f->d/10));
            $array_lectura_e_pow_10[]=pow(10,($f->e/10));
        }
    }
    if($get_fondo->num_rows()>0){
        $array_lectura_1_pow=[];
        $array_lectura_2_pow=[];
        $array_lectura_3_pow=[];
        $array_lectura_4_pow=[];
        $array_lectura_5_pow=[];

        $array_lectura_1_pow_10=[];
        $array_lectura_2_pow_10=[];
        $array_lectura_3_pow_10=[];
        $array_lectura_4_pow_10=[];
        $array_lectura_5_pow_10=[];

        foreach ($get_fondo->result() as $f) { 
            $array_lectura_1_pow[]=pow(($f->uno-$medicion_fu_1),2);
            $array_lectura_2_pow[]=pow(($f->dos-$medicion_fu_2),2);
            $array_lectura_3_pow[]=pow(($f->tres-$medicion_fu_3),2);
            $array_lectura_4_pow[]=pow(($f->cuatro-$medicion_fu_4),2);
            $array_lectura_5_pow[]=pow(($f->cinco-$medicion_fu_5),2);

            $array_lectura_1_pow_10[]=pow(10,($f->uno/10));
            $array_lectura_2_pow_10[]=pow(10,($f->dos/10));
            $array_lectura_3_pow_10[]=pow(10,($f->tres/10));
            $array_lectura_4_pow_10[]=pow(10,($f->cuatro/10));
            $array_lectura_5_pow_10[]=pow(10,($f->cinco/10));
        }
    }
    $htmlg.='<br><p class="ft11"><b><u>Determinación de la desviación estándar (s) de cada punto de medición de fuente y fondo</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form2.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                    <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                    <p>n =  Número de observaciones en el punto i = 35</p>
                    <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                </td>
            </tr>
        </table>
        <p class="pspaces"></p>';

    $medicion_fu2_a_pow = pow(array_sum($array_lectura_a_pow)/(count($array_lectura_a_pow)-1),0.5);
    $medicion_fu2_b_pow = pow(array_sum($array_lectura_b_pow)/(count($array_lectura_b_pow)-1),0.5);
    $medicion_fu2_c_pow = pow(array_sum($array_lectura_c_pow)/(count($array_lectura_c_pow)-1),0.5);  
    $medicion_fu2_d_pow = pow(array_sum($array_lectura_d_pow)/(count($array_lectura_d_pow)-1),0.5);
    $medicion_fu2_e_pow = pow(array_sum($array_lectura_e_pow)/(count($array_lectura_e_pow)-1),0.5);
    $prom_medic2_fuente=($medicion_fu2_a_pow+$medicion_fu2_b_pow+$medicion_fu2_c_pow+$medicion_fu2_d_pow+$medicion_fu2_e_pow)/5;
    $medicion_fu2_1_pow = pow(array_sum($array_lectura_1_pow)/(count($array_lectura_1_pow)-1),0.5); 
    $medicion_fu2_2_pow = pow(array_sum($array_lectura_2_pow)/(count($array_lectura_2_pow)-1),0.5);     
    $medicion_fu2_3_pow = pow(array_sum($array_lectura_3_pow)/(count($array_lectura_3_pow)-1),0.5);  
    $medicion_fu2_4_pow = pow(array_sum($array_lectura_4_pow)/(count($array_lectura_4_pow)-1),0.5);  
    $medicion_fu2_5_pow = pow(array_sum($array_lectura_5_pow)/(count($array_lectura_5_pow)-1),0.5);
    $prom_medic2_fonto=($medicion_fu2_1_pow+$medicion_fu2_2_pow+$medicion_fu2_3_pow+$medicion_fu2_4_pow+$medicion_fu2_5_pow)/5;
    
    $htmlg.='<table class="tableb ">
        <tr>
            <th></th>
            <th colspan="6">Mediciones de fuente</th>
            <th colspan="6"> Mediciones de fondo</th>
        </tr>
        <tr>
            <th>Punto:</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
            <th>E</th>
            <th>Prom.</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>IV</th>
            <th>V</th>
            <th>Prom.</th>
        </tr>
        <tr>
            <th>O</th>
            <td>'.round($medicion_fu2_a_pow, 1).'</td>
            <td>'.round($medicion_fu2_b_pow, 1).'</td>
            <td>'.round($medicion_fu2_c_pow, 1).'</td>
            <td>'.round($medicion_fu2_d_pow, 1).'</td>
            <td>'.round($medicion_fu2_e_pow, 1).'</td>
            <td>'.round($prom_medic2_fuente, 1).'</td>
            <td>'.round($medicion_fu2_1_pow, 1).'</td>
            <td>'.round($medicion_fu2_2_pow, 1).'</td>
            <td>'.round($medicion_fu2_3_pow, 1).'</td>
            <td>'.round($medicion_fu2_4_pow, 1).'</td>
            <td>'.round($medicion_fu2_5_pow, 1).'</td>
            <td>'.round($prom_medic2_fonto, 1).'</td>
        </tr>
    </table>';

    $htmlg.='<p class="ft11"><b><u>Determinación del N<sub>10</sub> de cada punto y de la Zona Crítica de medición de fuente y fondo</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form3.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>N<sub>10</sub> = Percentil 10 del punto i = dB(A)</p>
                    <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                    <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                </td>
            </tr>
        </table>
        <p class="pspaces"></p>';

    $medicion_fu3_a_pow = $medicion_fu_a+(1.2817*$medicion_fu2_a_pow);
    $medicion_fu3_b_pow = $medicion_fu_b+(1.2817*$medicion_fu2_b_pow);
    $medicion_fu3_c_pow = $medicion_fu_c+(1.2817*$medicion_fu2_c_pow);
    $medicion_fu3_d_pow = $medicion_fu_d+(1.2817*$medicion_fu2_d_pow);
    $medicion_fu3_e_pow = $medicion_fu_e+(1.2817*$medicion_fu2_e_pow);
    $prom_medic3_fuente=($medicion_fu3_a_pow+$medicion_fu3_b_pow+$medicion_fu3_c_pow+$medicion_fu3_d_pow+$medicion_fu3_e_pow)/5;
    $medicion_fu3_1_pow = $medicion_fu_1+(1.2817*$medicion_fu2_1_pow);
    $medicion_fu3_2_pow = $medicion_fu_2+(1.2817*$medicion_fu2_2_pow);
    $medicion_fu3_3_pow = $medicion_fu_3+(1.2817*$medicion_fu2_3_pow);
    $medicion_fu3_4_pow = $medicion_fu_4+(1.2817*$medicion_fu2_4_pow);
    $medicion_fu3_5_pow = $medicion_fu_5+(1.2817*$medicion_fu2_5_pow);
    $prom_medic3_fonto=($medicion_fu3_1_pow+$medicion_fu3_2_pow+$medicion_fu3_3_pow+$medicion_fu3_4_pow+$medicion_fu3_5_pow)/5;
    
    $htmlg.='<table class="tableb ">
            <tr>
                <th></th>
                <th colspan="6">Mediciones de fuente</th>
                <th colspan="6"> Mediciones de fondo</th>
            </tr>
            <tr>
                <th>Punto:</th>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>D</th>
                <th>E</th>
                <th>Prom.</th>
                <th>I</th>
                <th>II</th>
                <th>III</th>
                <th>IV</th>
                <th>V</th>
                <th>Prom.</th>
            </tr>
            <tr>
                <th>N<sub>10</sub></th>
                <td>'.round($medicion_fu3_a_pow,1).'</td>
                <td>'.round($medicion_fu3_b_pow,1).'</td>
                <td>'.round($medicion_fu3_c_pow,1).'</td>
                <td>'.round($medicion_fu3_d_pow,1).'</td>
                <td>'.round($medicion_fu3_e_pow,1).'</td>
                <td>'.round($prom_medic3_fuente,1).'</td>
                <td>'.round($medicion_fu3_1_pow,1).'</td>
                <td>'.round($medicion_fu3_2_pow,1).'</td>
                <td>'.round($medicion_fu3_3_pow,1).'</td>
                <td>'.round($medicion_fu3_4_pow,1).'</td>
                <td>'.round($medicion_fu3_5_pow,1).'</td>
                <td>'.round($prom_medic3_fonto,1).'</td>
            </tr>
        </table>';

    $htmlg.='<p class="ft11"><b><u>Determinación del N<sub>10</sub> de cada punto y de la Zona Crítica de medición de fuente y fondo</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form4.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>Neq<sub>i</sub> = Nivel equivalente del punto i = dB(A)</p>
                    <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                    <p>m = Número de observaciones en el punto i = 35</p>
                </td>
            </tr>
        </table>';
 
        $medicion_fu4_a_pow =10*(log10(array_sum($array_lectura_a_pow_10)/count($array_lectura_a_pow_10)));
        $medicion_fu4_b_pow = 10*(log10(array_sum($array_lectura_b_pow_10)/count($array_lectura_b_pow_10)));
        $medicion_fu4_c_pow = 10*(log10(array_sum($array_lectura_c_pow_10)/count($array_lectura_c_pow_10)));
        $medicion_fu4_d_pow = 10*(log10(array_sum($array_lectura_d_pow_10)/count($array_lectura_d_pow_10)));
        $medicion_fu4_e_pow = 10*(log10(array_sum($array_lectura_e_pow_10)/count($array_lectura_e_pow_10)));
        $medicion_fu4_1_pow = 10*(log10(array_sum($array_lectura_1_pow_10)/count($array_lectura_1_pow_10)));
        $medicion_fu4_2_pow = 10*(log10(array_sum($array_lectura_2_pow_10)/count($array_lectura_2_pow_10)));
        $medicion_fu4_3_pow = 10*(log10(array_sum($array_lectura_3_pow_10)/count($array_lectura_3_pow_10)));
        $medicion_fu4_4_pow = 10*(log10(array_sum($array_lectura_4_pow_10)/count($array_lectura_4_pow_10)));
        $medicion_fu4_5_pow = 10*(log10(array_sum($array_lectura_5_pow_10)/count($array_lectura_5_pow_10)));

    $htmlg.='<table class="tableb ">
        <tr>
            <th></th>
            <th colspan="5">Mediciones de fuente</th>
            <th colspan="5"> Mediciones de fondo</th>
        </tr>
        <tr>
            <th>Punto:</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
            <th>E</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>IV</th>
            <th>V</th>
        </tr>
        <tr>
            <th>N<sub>eqi</sub></th>
            <td>'.round($medicion_fu4_a_pow,1).'</td>
            <td>'.round($medicion_fu4_b_pow,1).'</td>
            <td>'.round($medicion_fu4_c_pow,1).'</td>
            <td>'.round($medicion_fu4_d_pow,1).'</td>
            <td>'.round($medicion_fu4_e_pow,1).'</td>
            <td>'.round($medicion_fu4_1_pow,1).'</td>
            <td>'.round($medicion_fu4_2_pow,1).'</td>
            <td>'.round($medicion_fu4_3_pow,1).'</td>
            <td>'.round($medicion_fu4_4_pow,1).'</td>
            <td>'.round($medicion_fu4_5_pow,1).'</td>
        </tr>
    </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>';

    $htmlg.='<p class="ft11"><b><u>Determinación del Nivel Equivalente de los niveles equivalentes Neq<sub>(eq)</sub> o Nivel equivalente de la Zona Critica para la medición de fuente y fondo.</u></b></p>';
     
    $nivel_equivalente_a=pow(10,($medicion_fu4_a_pow/10));
    $nivel_equivalente_b=pow(10,($medicion_fu4_b_pow/10));    
    $nivel_equivalente_c=pow(10,($medicion_fu4_c_pow/10));
    $nivel_equivalente_d=pow(10,($medicion_fu4_d_pow/10));
    $nivel_equivalente_e=pow(10,($medicion_fu4_e_pow/10));
    $nivel_equivalente_fuente=($nivel_equivalente_a+$nivel_equivalente_b+$nivel_equivalente_c+$nivel_equivalente_d+$nivel_equivalente_e)/5;

    $nivel_equivalente_1=pow(10,($medicion_fu4_1_pow/10));
    $nivel_equivalente_2=pow(10,($medicion_fu4_2_pow/10));
    $nivel_equivalente_3=pow(10,($medicion_fu4_3_pow/10));
    $nivel_equivalente_4=pow(10,($medicion_fu4_4_pow/10));
    $nivel_equivalente_5=pow(10,($medicion_fu4_5_pow/10));
    $nivel_equivalente_fondo=($nivel_equivalente_1+$nivel_equivalente_2+$nivel_equivalente_3+$nivel_equivalente_4+$nivel_equivalente_5)/5;

    $htmlg.='<table width="100%">
        <tr>
            <td>
                <table class="tableb ">
                    <tr>
                        <th colspan="2">Fuente</th>
                    </tr>
                    <tr>
                        <th colspan="2">10^(Neq/10)</th>
                    </tr>
                    <tr><th>A</th><td>'.round($nivel_equivalente_a,4).'</td></tr>
                    <tr><th>B</th><td>'.round($nivel_equivalente_b,4).'</td></tr>
                    <tr><th>C</th><td>'.round($nivel_equivalente_c,4).'</td></tr>
                    <tr><th>D</th><td>'.round($nivel_equivalente_d,4).'</td></tr>
                    <tr><th>E</th><td>'.round($nivel_equivalente_e,4).'</td></tr>
                    <tr><td></td><td>'.round($nivel_equivalente_fuente,4).'</td></tr>
                </table>
            </td>
            <td></td>
            <td >
                <table class="tableb ">
                    <tr>
                        <th colspan="2">Fondo</th>
                    </tr>
                    <tr>
                        <th colspan="2">10^(Neq/10)</th>
                    </tr>
                    <tr><th>I</th><td>'.round($nivel_equivalente_1,4).'</td></tr>
                    <tr><th>II</th><td>'.round($nivel_equivalente_2,4).'</td></tr>
                    <tr><th>III</th><td>'.round($nivel_equivalente_3,4).'</td></tr>
                    <tr><th>IV</th><td>'.round($nivel_equivalente_4,4).'</td></tr>
                    <tr><th>V</th><td>'.round($nivel_equivalente_5,4).'</td></tr>
                    <tr><td></td><td>'.round($nivel_equivalente_fondo,4).'</td></tr>
                </table>
            </td>
            <td></td>
        </tr>
    </table>';

    $htmlg.='
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form5.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>Neq<sub>(eq)</sub> = Nivel equivalente de los niveles equivalentes</p>
                    <p>o Nivel equivalente de la Zona Critica = dB(A)</p>
                    <p>Neqi = Nivel equivalente del punto i = dB(A)</p>
                    <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                    <p>j = Número de puntos en la Zona Critica = 5</p>
                </td>
            </tr>
        </table>
        <p class="pspaces"></p>';

    $ruido_fuente_neq=10*log10($nivel_equivalente_fuente);
    $ruido_fondo_neq=10*log10($nivel_equivalente_fondo);

    $correc_prese_v_extremos=0.9023*$prom_medic2_fuente;
    $htmlg.='<table><tr><td width="25%"></td>
                <td width="50%"><table class="tableb ">
        <tr>
            <th>* Ruido de Fuente:</th><th>Neq<sub>(eq)</sub></th><td>'.round($ruido_fuente_neq,1).'</td><td>dB(A)</td>
        </tr>
        <tr>
            <th>* Ruido de Fondo:</th><th>Neq<sub>(eq)</sub></th><td>'.round($ruido_fondo_neq,1).'</td><td>dB(A)</td>
        </tr>
    </table></td>
                <td width="25%"></td></tr></table>';

    $htmlg.='<p class="ft11"><b><u>Correción por presencia de valores extremos.</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form6.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>Ce = Correción por valores extremos = dB(A)</p>
                    <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
                </td>
            </tr>
        </table>
        <table><tr>
        <td></td>
        <td><table class="tableb ">
            <tr>
                <th>Ce =</th>
                <td>'.round($correc_prese_v_extremos,2).'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td>
        </tr></table>
        ';

    $determinacion_valor_delta=$prom_medic_fuente-$prom_medic_fonto;
    if($determinacion_valor_delta>0.75){
        $msj_det='La Fuente Fija Emite Nivel Sonoro';
    }else{
        $msj_det='La Fuente Fija No Emite Nivel Sonoro';
    }
    $htmlg.='<p class="ft11"><b><u>Determinación del valor &Delta;<sub>50</sub>.</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form7.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>N<sub>50fuente</sub> = N<sub>50</sub> Promedio de los 5 puntos de medición de fuente = dB(A)</p>
                    <p>N<sub>50fondo</sub> = N<sub>50</sub> Promedio de los 5 puntos de medición de fondo = dB(A)</p>
                </td>
            </tr>
        </table>
        <table><tr><td></td>
        <td><table class="tableb ">
            <tr>
                <th>&Delta;<sub>50</sub></th>
                <td>'.round($determinacion_valor_delta,2).'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td></tr></table>
        
        <p class="ft11"><b>Conclusión de &Delta;<sub>50</sub>:</p>
        <p class="ft11 red">'.$msj_det.'</p>';

    if($determinacion_valor_delta>0.75){
        $corre_ruido_fondo=(-($determinacion_valor_delta+9))+(3*(sqrt((4*$determinacion_valor_delta)-3)));
        $txt_corre=round($corre_ruido_fondo,1); 
    }else{
        $corre_ruido_fondo=0;
        $txt_corre='No Aplica';
    }
    $htmlg.='<p class="ft11"><b><u>Corrección de Ruido por fondo (Cf):</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form8.jpg"></td>
            </tr>
        </table>
        <p class="pspaces"></p>
        <table><tr><td></td>
        <td><table class="tableb ">
            <tr>
                <th>Cf=</th>
                <td>'.$txt_corre.'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td></tr></table>
        ';

    if($determinacion_valor_delta>0.75){
        $correccion_n50_medio_externo=$prom_medic_fuente+$correc_prese_v_extremos;
        $txt_cor=round($correccion_n50_medio_externo,1);
    }else{
        $correccion_n50_medio_externo=0;
        $txt_cor='No aplica';
    }
    $htmlg.='<p class="ft11"><b><u>Corrección del N50 medio por extremos o Emisión de la fuente fija hacia la zona Crítica (N´<sub>50</sub>)</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form9.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>Ce = Correción por valores extremos = [dB(A)]</p>
                </td>
            </tr>
        </table>
        <p class="pspaces"></p>
        <table><tr><td></td>
        <td><table class="tableb ">
            <tr>
                <th>N´<sub>50</sub> = </th>
                <td>'.$txt_cor.'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td></tr></table>';

    $pdf->writeHTML($htmlg, true, false, true, false, '');

    if($determinacion_valor_delta>0.75){
        $deter_nff=max($correccion_n50_medio_externo,$ruido_fuente_neq);
        $txt_det=round($deter_nff,1);
    }else{
        $deter_nff=0;
        $txt_det='No aplica';
    }
     
    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>';

    $htmlg.='<p class="ft11"><b><u>Determinación del Nivel de Fuente Fija (N<sub>ff</sub>)</u></b></p>
        <p class="ft11">El Valor de Nivel de fuente fija es el mayor de los determinados N´<sub>50</sub> y Neq<sub>(eq)</sub></p>
        <table><tr>
        <td width="20%"></td>
        <td width="60%">
            <table cellpadding="3" class="tableb ft9">
                <tr>
                    <th>N´<sub>50</sub> = </th>
                    <td>'.round($correccion_n50_medio_externo,1).'</td>
                    <td>dB(A)</td>
                    <td> </td>
                    <th>Neq <sub>(eq)</sub></th>
                    <td>'.round($ruido_fuente_neq,1).'</td>
                    <td>dB(A)</td>
                </tr>
            </table></td>
        <td width="20%"></td>
        </tr></table>
        
        <p class="pspaces"></p>
        <table><tr><td></td>
        <td><table class="tableb ">
            <tr>
                <th>N<sub>ff</sub> =</th>
                <td>'.$txt_det.'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td></tr></table>
        ';

    if($determinacion_valor_delta>0.75){
        $correccion_nivel_fuente_fija=$deter_nff+$corre_ruido_fondo;
        $txt_cor_fte=round($correccion_nivel_fuente_fija,1);
    }else{
        $correccion_nivel_fuente_fija=0;
        $txt_cor_fte='No aplica';
    }
    $htmlg.='<p class="ft11"><b><u>Corrección del Nivel de Fuente Fija por Ruido de Fondo (N´)</u></b></p>
        <table cellpadding="5" class="ft9">
            <tr>
                <td><img src="'.base_url().'public/img/81_form10.jpg"></td>
                <td class="ft8_left"><p>Donde:</p>
                    <p>N´<sub>ff</sub> = Nivel de Fuente Fija Corregido por Ruido de Fondo = [dB(A)]</p>
                    <p>N<sub>ff</sub> = Nivel de Fuente Fija = [dB(A)]</p>
                    <p>C<sub>f</sub> = Correción por ruido de fondo = [dB(A)]</p>
                </td>
            </tr>
        </table>
        <table><tr><td></td>
        <td><table class="tableb ">
            <tr>
                <th>(N`<sub>ff</sub>) = </th>
                <td>'.$txt_cor_fte.'</td>
                <td>dB(A)</td>
            </tr>
        </table></td>
        <td></td></tr></table><p class="pspaces"></p>';
        /*$pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->AddPage('P', 'A4');
        $htmlg="";
        $htmlg.='<style type="text/css"> 
            .ft11{ font-size:11px; text-align:center; }
            .ft9{ font-size:9px; text-align:center; }
            .ft8{ font-size:8px; text-align:center; }
            .ft8_left{ font-size:8px; text-align:left; }
            .ft7{ font-size:7px; text-align:center; }
            .tableb td{
                border:1px solid #808080;
                font-family:Arial;text-align: center;
                font-size:9px;
                valign: middle;
                text-align:center;
            }
            .tableb th{
                background-color:rgb(217,217,217);
                border:1px solid #808080;  
                text-align:center; font-weight:bold;
                font-size:9px;
                color:black;
            }
            .tble_green{color:green;}
            .tgr{color:green;}
            .txt_red{color:red;}
            .pspaces{
                font-size:0.2px;    
            }

            .fontbold{font-weight: bold;}
            .tableb6 td{
                border:1px solid #808080;
                font-family:Arial;text-align: center;
                font-size:6px;
                valign: middle;
                text-align:center;
            }
            .tableb6 th{
                background-color:rgb(217,217,217);
                border:1px solid #808080;  
                text-align:center; 
                font-size:6px;
            }
            .tableb7 td{
                border:1px solid #808080;
                font-family:Arial;text-align: center;
                font-size:7px;
                valign: middle;
                text-align:center;
            }
            .tableb7 th{
                background-color:rgb(217,217,217);
                border:1px solid #808080;  
                text-align:center; 
                font-size:7px;
            }
         </style>';
        
        */
        $get_aisla=$this->ModeloCatalogos->getselectwheren('ruido_aisla_nom81',array('id_nom'=>$idnom,'estatus'=>1)); 
        $htmlg.='<table cellpadding="2" class="tableb">
                <thead>
                 <tr>
                    <th>No. lectura</th><th>a</th><th>b</th><th>c</th><th>d</th><th>e</th>
                </tr>
                </thead>';
        if($get_aisla->num_rows()>0){
            $b_array_lectura_a=[];
            $b_array_lectura_b=[];
            $b_array_lectura_c=[];
            $b_array_lectura_d=[];
            $b_array_lectura_e=[];
            $cont=0;
            foreach ($get_aisla->result() as $f) { 
                $cont++; 
                $b_array_lectura_a[]=$f->a;
                $b_array_lectura_b[]=$f->b;
                $b_array_lectura_c[]=$f->c;
                $b_array_lectura_d[]=$f->d;
                $b_array_lectura_e[]=$f->e;
                
                $htmlg.='<tr>
                        <td>'.$cont.'</td>
                        <td>'.$f->a.'</td>
                        <td>'.$f->b.'</td>
                        <td>'.$f->c.'</td>
                        <td>'.$f->d.'</td>
                        <td>'.$f->e.'</td>
                    </tr>';
            }
        }
        $htmlg.='</table>';

    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>

    <p class="ft11"><b><u>Corrección del Nivel de Fuente Fija por Ruido de Fondo (N´)</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form1.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                <p>n =  Número de observaciones en el punto i = 35</p>
            </td>
        </tr>
    </table>';
    $b_medicion_fu_a = array_sum($b_array_lectura_a)/count($b_array_lectura_a);
    $b_medicion_fu_b = array_sum($b_array_lectura_b)/count($b_array_lectura_b); 
    $b_medicion_fu_c = array_sum($b_array_lectura_c)/count($b_array_lectura_c);
    $b_medicion_fu_d = array_sum($b_array_lectura_d)/count($b_array_lectura_d);
    $b_medicion_fu_e = array_sum($b_array_lectura_e)/count($b_array_lectura_e); 
    $b_prom_medic_fuente=($b_medicion_fu_a+$b_medicion_fu_b+$b_medicion_fu_c+$b_medicion_fu_d+$b_medicion_fu_e)/5;

$htmlg.='<table><tr><td width="25%"></td>
    <td width="50%"><table cellpadding="3" class="tableb " >
        <tr>
            <th></th>
            <th colspan="6">Mediciones en el interior de la fuente</th>
        </tr>
        <tr>
            <th>Punto:</th>
            <th>A</th><th>B</th><th>C</th><th>D</th><th>E</th><th>Prom.</th>
        </tr>
        <tr>
            <th>N<sub>50</sub></th>
            <td>'.round($b_medicion_fu_a, 1).'</td><td>'.round($b_medicion_fu_b, 1).'</td><td>'.round($b_medicion_fu_c, 1).'</td><td>'.round($b_medicion_fu_d, 1).'</td><td>'.round($b_medicion_fu_e, 1).'</td><td>'.round($b_prom_medic_fuente,1).'</td>
        </tr>
    </table></td>
    <td width="25%"></td></tr></table><p class="pspaces"></p>';
    if($get_aisla->num_rows()>0){
        $b_array_lectura_a_pow=[];
        $b_array_lectura_b_pow=[];
        $b_array_lectura_c_pow=[];
        $b_array_lectura_d_pow=[];
        $b_array_lectura_e_pow=[];

        $b_array_lectura_a_pow_10=[];
        $b_array_lectura_b_pow_10=[];
        $b_array_lectura_c_pow_10=[];
        $b_array_lectura_d_pow_10=[];
        $b_array_lectura_e_pow_10=[];
        foreach ($get_aisla->result() as $f) { 
            $b_array_lectura_a_pow[]=pow(($f->a-$b_medicion_fu_a),2);
            $b_array_lectura_b_pow[]=pow(($f->b-$b_medicion_fu_b),2);
            $b_array_lectura_c_pow[]=pow(($f->c-$b_medicion_fu_c),2);
            $b_array_lectura_d_pow[]=pow(($f->d-$b_medicion_fu_d),2);
            $b_array_lectura_e_pow[]=pow(($f->e-$b_medicion_fu_e),2);

            $b_array_lectura_a_pow_10[]=pow(10,($f->a/10));
            $b_array_lectura_b_pow_10[]=pow(10,($f->b/10));
            $b_array_lectura_c_pow_10[]=pow(10,($f->c/10));
            $b_array_lectura_d_pow_10[]=pow(10,($f->d/10));
            $b_array_lectura_e_pow_10[]=pow(10,($f->e/10));
        }
    }
    $htmlg.='
    <p class="ft11"><b><u>Determinación de la desviación estándar (&sigma;) de cada punto de medición de fuente y fondo.</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form2.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                <p>n =  Número de observaciones en el punto i = 35</p>
                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
            </td>
        </tr>
    </table>';
    if($b_medicion_fu_a>0){
        $b_medicion_fu2_a_pow = pow(array_sum($b_array_lectura_a_pow)/(count($b_array_lectura_a_pow)-1),0.5);  
    }else{
        $b_medicion_fu2_a_pow=0;
    }
    
    $b_medicion_fu2_b_pow = pow(array_sum($b_array_lectura_b_pow)/(count($b_array_lectura_b_pow)-1),0.5);
    $b_medicion_fu2_c_pow = pow(array_sum($b_array_lectura_c_pow)/(count($b_array_lectura_c_pow)-1),0.5);
    $b_medicion_fu2_d_pow = pow(array_sum($b_array_lectura_d_pow)/(count($b_array_lectura_d_pow)-1),0.5);
    $b_medicion_fu2_e_pow = pow(array_sum($b_array_lectura_e_pow)/(count($b_array_lectura_e_pow)-1),0.5);
    $b_prom_medic2_fuente=($b_medicion_fu2_a_pow+$b_medicion_fu2_b_pow+$b_medicion_fu2_c_pow+$b_medicion_fu2_d_pow+$b_medicion_fu2_e_pow)/5;

    $htmlg.='<table><tr><td width="25%"></td>
                <td width="50%"><table cellpadding="3" class="tableb " >
                <tr><th></th><th colspan="6"> Mediciones en el interior de la fuente</th></tr>
                <tr>
                    <th>Punto:</th>
                    <th>a</th><th>b</th><th>c</th><th>d</th><th>e</th><th>Prom.</th>
                    
                </tr>
                <tr>
                    <th>&sigma;</th>
                    <td>'.round($b_medicion_fu2_a_pow, 1).'</td><td>'.round($b_medicion_fu2_b_pow, 1).'</td><td>'.round($b_medicion_fu2_c_pow, 1).'</td><td>'.round($b_medicion_fu2_d_pow, 1).'</td><td>'.round($b_medicion_fu2_e_pow, 1).'</td><td>'.round($b_prom_medic2_fuente,1).'</td>
                </tr>
            </table></td>
                <td width="25%"></td></tr></table>';
    $htmlg.='
    <p class="ft11"><b><u>Determinación del N<sub>10</sub> de cada punto y de la Zona Crítica de medición de fuente y fondo.</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form3.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>N<sub>10</sub> = Percentil 10 del punto i = dB(A)</p>
                <p>N<sub>50</sub> = Percentil 50 del punto i = dB(A)</p>
                <p>&sigma; = Desviación estándar de las observaciones en el punto i = dB(A)</p>
            </td>
        </tr>
    </table>';
    $b_medicion_fu3_a_pow = $b_medicion_fu_a+(1.2817*$b_medicion_fu2_a_pow);
    $b_medicion_fu3_b_pow = $b_medicion_fu_b+(1.2817*$b_medicion_fu2_b_pow);
    $b_medicion_fu3_c_pow = $b_medicion_fu_c+(1.2817*$b_medicion_fu2_c_pow);
    $b_medicion_fu3_d_pow = $b_medicion_fu_d+(1.2817*$b_medicion_fu2_d_pow);
    $b_medicion_fu3_e_pow = $b_medicion_fu_e+(1.2817*$b_medicion_fu2_e_pow);
    $b_prom_medic3_fuente=($b_medicion_fu3_a_pow+$b_medicion_fu3_b_pow+$b_medicion_fu3_c_pow+$b_medicion_fu3_d_pow+$b_medicion_fu3_e_pow)/5;
    $htmlg.='<table><tr><td width="25%"></td>
                <td width="50%"><table cellpadding="3" class="tableb " >
                <tr>
                    <th></th>
                    <th colspan="6"> Mediciones en el interior de la fuente</th>
                </tr>
                <tr>
                    <th>Punto:</th>
                    <th>a</th><th>b</th><th>c</th><th>d</th><th>e</th><th>Prom.</th>
                </tr>
                <tr>
                    <th>N<sub>10</sub></th>
                    <td>'.round($b_medicion_fu3_a_pow,1).'</td><td>'.round($b_medicion_fu3_b_pow,1).'</td><td>'.round($b_medicion_fu3_c_pow,1).'</td><td>'.round($b_medicion_fu3_d_pow,1).'</td><td>'.round($b_medicion_fu3_e_pow,1).'</td><td>'.round($b_prom_medic3_fuente,1).'</td>                                
                </tr>
            </table></td>
                <td width="25%"></td></tr></table>
                ';
    $htmlg.='
    <p class="ft11"><b><u>Determinación del Nivel Equivalente (N<sub>eqi</sub>) de cada punto de medición de fuente y fondo.</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form4.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>Neq<sub>i</sub> = Nivel equivalente del punto i = dB(A)</p>
                <p>Ni = Nivel Sonoro A en la observación i = dB(A)</p>
                <p>m = Número de observaciones en el punto i = 35</p>
            </td>
        </tr>
    </table>';
    $b_medicion_fu4_a_pow =10*(log10(array_sum($b_array_lectura_a_pow_10)/count($b_array_lectura_a_pow_10)));
    $b_medicion_fu4_b_pow = 10*(log10(array_sum($b_array_lectura_b_pow_10)/count($b_array_lectura_b_pow_10)));
    $b_medicion_fu4_c_pow = 10*(log10(array_sum($b_array_lectura_c_pow_10)/count($b_array_lectura_c_pow_10)));
    $b_medicion_fu4_d_pow = 10*(log10(array_sum($b_array_lectura_d_pow_10)/count($b_array_lectura_d_pow_10)));
    $b_medicion_fu4_e_pow = 10*(log10(array_sum($b_array_lectura_e_pow_10)/count($b_array_lectura_e_pow_10)));
    $htmlg.='<table><tr><td width="25%"></td>
                <td width="50%"><table cellpadding="3" class="tableb " >
                <tr>
                    <th></th>
                    <th colspan="5"> Mediciones en el interior de la fuente</th>
                </tr>
                <tr>
                    <th>Punto:</th>
                    <th>A</th><th>B</th><th>C</th><th>D</th><th>E</th>
                </tr>
                <tr>
                    <th>N<sub>eqi</sub></th>
                    <td>'.round($b_medicion_fu4_a_pow,1).'</td><td>'.round($b_medicion_fu4_b_pow,1).'</td><td>'.round($b_medicion_fu4_c_pow,1).'</td><td>'.round($b_medicion_fu4_d_pow,1).'</td><td>'.round($b_medicion_fu4_e_pow,1).'</td>
                </tr>
            </table></td>
        <td width="50%"></td></tr></table><p class="pspaces"></p>';

    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>';

    if($b_medicion_fu4_a_pow>0){$b_nivel_equivalente_a=pow(10,($b_medicion_fu4_a_pow/10));}else{$b_nivel_equivalente_a=0;}
    if($b_medicion_fu4_b_pow>0){$b_nivel_equivalente_b=pow(10,($b_medicion_fu4_b_pow/10));}else{$b_nivel_equivalente_b=0;}
    if($b_medicion_fu4_c_pow>0){$b_nivel_equivalente_c=pow(10,($b_medicion_fu4_c_pow/10));}else{$b_nivel_equivalente_c=0;}
    if($b_medicion_fu4_d_pow>0){$b_nivel_equivalente_d=pow(10,($b_medicion_fu4_d_pow/10));}else{$b_nivel_equivalente_d=0;}
    if($b_medicion_fu4_e_pow>0){$b_nivel_equivalente_e=pow(10,($b_medicion_fu4_e_pow/10));}else{$b_nivel_equivalente_e=0;}
    $b_nivel_equivalente_fuente=($b_nivel_equivalente_a+$b_nivel_equivalente_b+$b_nivel_equivalente_c+$b_nivel_equivalente_d+$b_nivel_equivalente_e)/5;

    $htmlg.='<table><tr><td></td>
                <td><table cellpadding="3" class="tableb">
                <tr><th colspan="2">Aislamiento</th></tr>
                <tr><th colspan="2">10^(Neq/10)</th></tr>
                <tr><th>a</th><td>'.round($b_nivel_equivalente_a,4).'</td></tr>
                <tr><th>b</th><td>'.round($b_nivel_equivalente_b,4).'</td></tr>
                <tr><th>c</th><td>'.round($b_nivel_equivalente_c,4).'</td></tr>
                <tr><th>d</th><td>'.round($b_nivel_equivalente_d,4).'</td></tr>
                <tr><th>e</th><td>'.round($b_nivel_equivalente_e,4).'</td></tr>
                <tr><td></td><td>'.round($b_nivel_equivalente_fuente,4).'</td></tr>
            </table></td>
                <td></td></tr></table>';
    $htmlg.='
    <p class="ft11"><b><u>Determinación del Nivel Equivalente (N<sub>eqi</sub>) de cada punto de medición de fuente y fondo.</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form12.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>Nd = Nivel equivalente medido en el interior de la fuente fija  = dB(A)</p>
                <p>Nk = Nivel Sonoro A en la observación i = dB(A)</p>
                <p>k = puntos de medición en el interior de la fuente fija = 5</p>
            </td>
        </tr>
    </table>';
    $b_ruido_fuente_neq=10*log10($b_nivel_equivalente_fuente); 
    $htmlg.='<table><tr><td></td><td><table cellpadding="3" class="tableb"><tr><th>Nd = </th><td>'.round($b_ruido_fuente_neq,1).'</td><td>dB(A)</td></tr></table></td><td></td></tr></table>';
    $htmlg.='
    <p class="ft11"><b><u>Determinación de la Reducción Acústica (R)</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form11.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>R = Reducción Acústica del elemento común = dB</p>
                <p>Nd = Nivel equivalente medido en el interior de la fuente fija  = dB(A)</p>
                <p>Neq(<sub>eq</sub>) = Nivel equivalente de la Zona Critica = dB(A)</p>
                <p>S = Área del elemento común = [m<sup>2</sup>]</p>
                <p>10 = Absorción acústica normalizada del recinto receptor</p>
            </td>
        </tr>
    </table>';
    $s_deter_de_la_reduccion=1;
    $htmlg.='<table cellpadding="3" class="tableb"><tr><th>s = </th><td>--</td><td>dB(A)</td></tr></table>';
    if($b_ruido_fuente_neq>0){
        $deter_reduc_acustica=$b_ruido_fuente_neq-$ruido_fuente_neq+(10*log10($s_deter_de_la_reduccion/10));
        if($deter_reduc_acustica>0){
            $b_ruido_fuente_neq_txt= round($deter_reduc_acustica,1);    
        }else{
            $b_ruido_fuente_neq_txt= '--';
        }
    }else{
        $deter_reduc_acustica=0;
        $b_ruido_fuente_neq_txt= 'No aplica';
    }
    $htmlg.='<table cellpadding="3" class="tableb"><tr><th>s = </th><td>'.$b_ruido_fuente_neq_txt.'</td><td>dB(A)</td></tr></table>';
    $htmlg.='
    <p class="ft11"><b><u>Corrección del Nivel de fuente fija por la existencia de un elemento constructivo Total (N´´ff)</u></b></p>
    <table cellpadding="5" class="ft9">
        <tr>
            <td><img src="'.base_url().'public/img/81_form13.jpg"></td>
            <td class="ft8_left">
                <p>Donde:</p>
                <p>N´´<sub>ff</sub> = Valor de fuente fija corregido por aislamiento de un elemento constructivo = dB</p>
                <p>N´<sub>ff</sub> = Nivel de Fuente Fija Corregido por Ruido de Fondo = dB(A)</p>
                <p>R = Reducción Acústica del elemento común = dB</p>
            </td>
        </tr>
    </table>';
    if($b_ruido_fuente_neq>0){
        $correccion_nivel_fuente_fija_nff=$correccion_nivel_fuente_fija+(0.5*$deter_reduc_acustica);
        if($correccion_nivel_fuente_fija_nff>0){
           $correccion_nivel_fuente_fija_nff_txt= round($correccion_nivel_fuente_fija_nff,1);    
        }else{
            $correccion_nivel_fuente_fija_nff_txt='--';
        }
        
    }else{
        $correccion_nivel_fuente_fija_nff=0;
       $correccion_nivel_fuente_fija_nff_txt= 'No aplica';
    }
    $htmlg.='<table><tr><td></td><td><table cellpadding="3" class="tableb"><tr><th>s = </th><td>'.$correccion_nivel_fuente_fija_nff_txt.'</td><td>dB(A)</td></tr></table></td><td></td></tr></table>';

    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->setPrintHeader(true);
    $pdf->SetMargins(15, '27', 15);
    $pdf->AddPage('P', 'A4');

    $htmlg='';
    $htmlg.='<style type="text/css"> 
        .ft11{ font-size:11px; text-align:center; }
        .ft9{ font-size:9px; text-align:center; }
        .ft8{ font-size:8px; text-align:center; }
        .ft8_left{ font-size:8px; text-align:left; }
        .ft7{ font-size:7px; text-align:center; }
        .tableb td{
            border:1px solid #808080;
            font-family:Arial;text-align: center;
            font-size:9px;
            valign: middle;
            text-align:center;
        }
        .tableb th{
            background-color:rgb(217,217,217);
            border:1px solid #808080;  
            text-align:center; font-weight:bold;
            font-size:9px;
            color:black;
        }
        .tble_green{color:green;}
        .tgr{color:green;}
        .txt_red{color:red;}
        .pspaces{
            font-size:0.2px;    
        }
     </style>';

    $prom_medic4_fuente=($medicion_fu4_a_pow+$medicion_fu4_b_pow+$medicion_fu4_c_pow+$medicion_fu4_d_pow+$medicion_fu4_e_pow)/5;
    $htmlg.='<table><tr><td></td></tr><tr><td height="10px"></td></tr></table>
    <p class="ft11"><b>RESULTADOS</b></p>
    <p class="ft9"><b>Mediciones de Ruido de fuente</b></p>
    <table class="tableb ">
        <tr>
            <th>Nivel:</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
            <th>E</th>
            <th>Promedio / Neq<sub>(eq)</sub></th>
            <th>Unidad</th>
        </tr>
        <tr>
            <th>N<sub>50</sub></th>
            <td>'.round($medicion_fu_a,2).'</td>
            <td>'.round($medicion_fu_b,2).'</td>
            <td>'.round($medicion_fu_c,2).'</td>
            <td>'.round($medicion_fu_d,2).'</td>
            <td>'.round($medicion_fu_e,2).'</td>
            <td>'.round($prom_medic_fuente,2).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>10</sub></th>
            <td>'.round($medicion_fu3_a_pow,1).'</td>
            <td>'.round($medicion_fu3_b_pow,1).'</td>
            <td>'.round($medicion_fu3_c_pow,1).'</td>
            <td>'.round($medicion_fu3_d_pow,1).'</td>
            <td>'.round($medicion_fu3_e_pow,1).'</td>
            <td>'.round($prom_medic3_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>O fuente</th>
            <td>'.round($medicion_fu2_a_pow, 1).'</td>
            <td>'.round($medicion_fu2_b_pow, 1).'</td>
            <td>'.round($medicion_fu2_c_pow, 1).'</td>
            <td>'.round($medicion_fu2_d_pow, 1).'</td>
            <td>'.round($medicion_fu2_e_pow, 1).'</td>
            <td>'.round($prom_medic2_fuente, 1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>eq</sub></th>
            <td>'.round($medicion_fu4_a_pow,1).'</td>
            <td>'.round($medicion_fu4_b_pow,1).'</td>
            <td>'.round($medicion_fu4_c_pow,1).'</td>
            <td>'.round($medicion_fu4_d_pow,1).'</td>
            <td>'.round($medicion_fu4_e_pow,1).'</td>
            <td>'.round($prom_medic4_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
    </table>';

    $prom_medic4_1_fuente=($medicion_fu4_1_pow+$medicion_fu4_2_pow+$medicion_fu4_3_pow+$medicion_fu4_4_pow+$medicion_fu4_5_pow)/5;
    $htmlg.='
    <p class="ft9"><b>Mediciones de Ruido de fondo</b></p>
    <table class="tableb ">
        <tr>
            <th>Nivel:</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>IV</th>
            <th>V</th>
            <th>Promedio / Neq<sub>(eq)</sub></th>
            <th>Unidad</th>
        </tr>
        <tr>
            <th>N<sub>50</sub></th>
            <td>'.$medicion_fu_1.'</td>
            <td>'.$medicion_fu_2.'</td>
            <td>'.$medicion_fu_3.'</td>
            <td>'.$medicion_fu_4.'</td>
            <td>'.$medicion_fu_5.'</td>
            <td>'.$prom_medic_fonto.'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>10</sub></th>
            <td>'.round($medicion_fu3_1_pow,1).'</td>
            <td>'.round($medicion_fu3_2_pow,1).'</td>
            <td>'.round($medicion_fu3_3_pow,1).'</td>
            <td>'.round($medicion_fu3_4_pow,1).'</td>
            <td>'.round($medicion_fu3_5_pow,1).'</td>
            <td>'.round($prom_medic3_fonto,1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>O fondo</th>
            <td>'.round($medicion_fu2_1_pow, 1).'</td>
            <td>'.round($medicion_fu2_2_pow, 1).'</td>
            <td>'.round($medicion_fu2_3_pow, 1).'</td>
            <td>'.round($medicion_fu2_4_pow, 1).'</td>
            <td>'.round($medicion_fu2_5_pow, 1).'</td>
            <td>'.round($prom_medic2_fonto, 1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>eq</sub></th>
            <td>'.round($medicion_fu4_1_pow,1).'</td>
            <td>'.round($medicion_fu4_2_pow,1).'</td>
            <td>'.round($medicion_fu4_3_pow,1).'</td>
            <td>'.round($medicion_fu4_4_pow,1).'</td>
            <td>'.round($medicion_fu4_5_pow,1).'</td>
            <td>'.round($prom_medic4_1_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
    </table>';

    $htmlg.='
    <p class="ft9"><b>Mediciones para cálculo de reducción acústica</b></p>
    <table class="tableb ">
        <tr>
            <th>Nivel:</th>
            <th>a</th>
            <th>b</th>
            <th>c</th>
            <th>d</th>
            <th>e</th>
            <th>Promedio / Ned</th>
            <th>Unidad</th>
        </tr>
        <tr>
            <th>N<sub>50</sub></th>
            <td>'.round($b_medicion_fu_a,1).'</td>
            <td>'.round($b_medicion_fu_b,1).'</td>
            <td>'.round($b_medicion_fu_c,1).'</td>
            <td>'.round($b_medicion_fu_d,1).'</td>
            <td>'.round($b_medicion_fu_e,1).'</td>
            <td>'.round($b_prom_medic_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>10</sub></th>
            <td>'.round($b_medicion_fu3_a_pow,1).'</td>
            <td>'.round($b_medicion_fu3_b_pow,1).'</td>
            <td>'.round($b_medicion_fu3_c_pow,1).'</td>
            <td>'.round($b_medicion_fu3_d_pow,1).'</td>
            <td>'.round($b_medicion_fu3_e_pow,1).'</td>
            <td>'.round($b_prom_medic3_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>&sigma;<sub>Ais.</sub></th>
            <td>'.round($b_medicion_fu2_a_pow,1).'</td>
            <td>'.round($b_medicion_fu2_b_pow,1).'</td>
            <td>'.round($b_medicion_fu2_c_pow,1).'</td>
            <td>'.round($b_medicion_fu2_d_pow,1).'</td>
            <td>'.round($b_medicion_fu2_e_pow,1).'</td>
            <td>'.round($b_prom_medic2_fuente,1).'</td>
            <td>dB(A)</td>
        </tr>
        <tr>
            <th>N<sub>d</sub></th>
            <td>'.round($b_medicion_fu4_a_pow,1).'</td>
            <td>'.round($b_medicion_fu4_b_pow,1).'</td>
            <td>'.round($b_medicion_fu4_c_pow,1).'</td>
            <td>'.round($b_medicion_fu4_d_pow,1).'</td>
            <td>'.round($b_medicion_fu4_e_pow,1).'</td>
            <td>'.round($b_ruido_fuente_neq,1).'</td>
            <td>dB(A)</td>
        </tr>
    </table>';



    $htmlg.='<p class="ft11"><b><u>Corrección por Ruido de Fondo</u></b></p>
        <table class="tableb ">
            <tr>
                <th>&Delta;<sub>50</sub> = </th>
                <td>'.round($determinacion_valor_delta,2).'</td>
                <td>[dB(A)]</td>

                <th>Cf = </th>
                <td>'.$corre_ruido_fondo.'</td>
                <td>dB(A)</td>
            </tr>
        </table>';

    $htmlg.='<p class="ft11"><b><u>Corrección por presencia de extremos</u></b></p>
            <table><tr><td></td><td><table class="tableb "><tr><th>Ce = </th><td>'.round($correc_prese_v_extremos,2).'</td><td>dB(A)</td></tr>        </table></td><td></td></tr></table>
        ';
    $htmlg.='<p class="ft11"><b><u>Corrección por aislamiento</u></b></p>
            <table><tr><td></td><td><table class="tableb "><tr><th>R = </th><td>'.round($deter_reduc_acustica,1).'</td><td>dB(A)</td></tr></table></td><td></td></tr></table>
        ';

    $htmlg.='<p class="ft11"><b><u>Emisión de la fuente fija hacia la Zona Crítica</u></b></p>
        <table><tr><td></td><td><table class="tableb "><tr><th>N´<sub>50</sub> = </th><td>'.round($correccion_n50_medio_externo,1).'</td><td>dB(A)</td></tr>        </table></td><td></td></tr></table>
        ';

    $htmlg.='<p class="ft11"><b><u>Determinación del mayor de N´<sub>50</sub> y (Neq)<sub>eq</sub> de la fuente y obtener N<sub>ff</sub>: </u></b></p>
        <table class="tableb ">
            <tr>
                <th>N´<sub>50</sub> = </th>
                <td>'.round($correccion_n50_medio_externo,1).'</td>
                <td>dB(A)</td>
            </tr>
        </table>
        <table class="tableb ">
            <tr>
                <th>Neq<sub>(eq)</sub> = </th>
                <td>'.round($ruido_fuente_neq,1).'</td>
                <td>dB(A)</td>

                <th>N<sub>ff</sub> = </th>
                <td>'.round($deter_nff,1).'</td>
                <td>dB(A)</td>
            </tr>
        </table>';

    $htmlg.='<p class="ft11"><b><u>Corrección por Ruido de Fondo</u></b></p>
            <table><tr><td></td><td><table class="tableb "><tr><th>(N`<sub>ff</sub>) = </th><td>'.round($correccion_nivel_fuente_fija,1).'</td><td>dB(A)</td></tr></table></td><td></td></tr></table>
        ';

    $htmlg.='<p class="ft11"><b><u>Corrección por Aislamiento</u></b></p>
    <table><tr><td></td><td><table class="tableb "><tr><th>N´´<sub>ff</sub> = </th><td>'.round($correccion_nivel_fuente_fija_nff,1).'</td><td>dB(A)</td></tr></table></td><td></td></tr></table>
        ';
    if($correccion_nivel_fuente_fija>0.75){
        $corre_aslamiento=max($correccion_nivel_fuente_fija_nff,$correccion_nivel_fuente_fija);
        $txt_cor_fte1= round($corre_aslamiento,1);
    }else{
        $corre_aslamiento=0;
        $txt_cor_fte1= '0.00<br><span class="txt_red">LA FUENTE FIJA NO EMITE NIVEL SONORO</span>';
    }
    $htmlg.='<p class="ft11"><b>En la Zona Crítica <u>'.$dc->zona_critica.'</u></b></p>
        <table class="tableb " width="100%">
            <tr>
                <th width="40%">Presenta un valor de Nivel de Emisión de </th>
                <td width="40%">'.$txt_cor_fte1.'</td>
                <td width="20%">dB(A)</td>
            </tr>
        </table>';
        if($cont_nom==$datosnom->num_rows()){
            $htmlg.='<table><tr><td height="30px"></td></tr></table>';
        }else{
            $htmlg.='<table><tr><td height="58px"></td></tr></table>';
        }
        
    /*$htmlg.='<p><u>ESTIMACIÓN DE LA INCERTIDUMBRE PARA EL RUIDO DE FUENTE</u></p><p>Promedio de las lecturas del ruido de fuente y fondo</p>
            <table class="tableb" >
                <tr><th colspan="5">Fuente</th></tr>
                <tr>
                    <th>A</th><th>B</th><th>C</th><th>D</th><th>E</th>
                </tr>
                <tr>
                    <th>[dB(A)]</th><th>[dB(A)]</th><th>[dB(A)]</th><th>[dB(A)]</th><th>[dB(A)]</th>
                </tr>
                <tr>
                    <td>'.round($medicion_fu_a,1).'</td>
                    <td>'.round($medicion_fu_b,1).'</td>
                    <td>'.round($medicion_fu_c,1).'</td>
                    <td>'.round($medicion_fu_d,1).'</td>
                    <td>'.round($medicion_fu_e,1).'</td>
                </tr>
            </table>';
    $desviacion_estandar=$this->ModeloCatalogos->desvest(array($medicion_fu_a,$medicion_fu_b,$medicion_fu_c,$medicion_fu_d,$medicion_fu_e));
    $inser_tipo_a=(1/sqrt(10))*$desviacion_estandar;
    $resolucion_sonometro=0.1;
    $certif_calibracion_sonometro=0.11;
    $certif_calibracion_calibrador_acustico=0.20;

    $v_relativo_inser_tipo_a=pow(10,$inser_tipo_a/20)-1;
    $v_relativo_resolucion_sonometro=pow(10,$resolucion_sonometro/20)-1;
    $v_relativo_certif_calibracion_sonometro=pow(10,$certif_calibracion_sonometro/20)-1;
    $v_relativo_certif_calibracion_calibrador_acustico=pow(10,$certif_calibracion_calibrador_acustico/20)-1;

    $htmlg.='<p>'.round($desviacion_estandar,1).'</p>
            <table class="tableb">
                <tr>
                    <td colspan="4">Componentes de Incertidumbres</td>
                </tr>
                <tr>
                    <th>Incertidumbre tipo A, por las lecturas, Repetibilidad en dB</th>
                    <th>Resolución del sonometro (dB)</th>
                    <th>Certificado de calibración del sonometro (dB)</th>
                    <th>Certificado de calibración del calibrador acústico (dB)</th>
                </tr>
                <tr>
                    <td>'.$inser_tipo_a.'</td>
                    <td>'.$resolucion_sonometro.'</td>
                    <td>'.$certif_calibracion_sonometro.'</td>
                    <td>'.$certif_calibracion_calibrador_acustico.'</td>
                </tr>
                <tr><th colspan="4">dB convertidos a valores relativos</th></tr>
                <tr>
                    <td>'.$v_relativo_inser_tipo_a.'</td>
                    <td>'.$v_relativo_resolucion_sonometro.'</td>
                    <td>'.$v_relativo_certif_calibracion_sonometro.'</td>
                    <td>'.$v_relativo_certif_calibracion_calibrador_acustico.'</td>
                </tr>
            </table>';
            $fi_lecturas=1;
            $fi_resolucion=1;
            $fi_sonometro=1;
            $fi_calibrador=1;

            $ie_v_relativo_inser_tipo_a=$v_relativo_inser_tipo_a/1;
            
            $contribucion_l=$ie_v_relativo_inser_tipo_a*$fi_lecturas;
            $contribucion_l_pow=pow($contribucion_l,2);
            $ie_v_relativo_resolucion_sonometro=$v_relativo_resolucion_sonometro/sqrt(12);
            
            $contribucion_r=$ie_v_relativo_resolucion_sonometro*$fi_resolucion;
            $contribucion_r_pow=pow($contribucion_r,2);
            $ie_v_relativo_certif_calibracion_sonometro=$v_relativo_certif_calibracion_sonometro/2;
            $contribucion_s=$ie_v_relativo_certif_calibracion_sonometro*$fi_sonometro;
            $contribucion_s_pow=pow($contribucion_s,2);
            
            $ie_v_relativo_certif_calibracion_calibrador_acustico=$v_relativo_certif_calibracion_calibrador_acustico/2;
            
            $contribucion_c=$ie_v_relativo_certif_calibracion_calibrador_acustico*$fi_calibrador;
            $contribucion_c_pow=pow($contribucion_c,2);
            $fuentes_insertidumbre_sum=$contribucion_l_pow+$contribucion_r_pow+$contribucion_s_pow+$contribucion_c_pow; 
    $htmlg.='<p></p><table class="tableb">
                <tr>
                    <th>Fuentes de Incertidumbre</th>
                    <th>Incertidumbre estándar (rel)</th>
                    <th>Tipo de Distribución</th>
                    <th>Incertidumbre estándar u(xi)  [rel]</th>
                    <th>Coeficiente de sensibilidad Ci</th>
                    <th>Contribución ui(y)  [rel]</th>
                    <th>(ui(y))2    [rel2]</th>
                </tr>
                <tr>
                    <th>Lecturas</th>
                    <td>'.$v_relativo_inser_tipo_a.'</td>
                    <td>A, normal k=1</td>
                    <td>'.$ie_v_relativo_inser_tipo_a.'</td>
                    <td>'.$fi_lecturas.'</td>
                    <td>'.$contribucion_l.'</td>
                    <td>'.round($contribucion_l_pow,5).'</td>
                </tr>
                <tr>
                    <th>Resolución</th>
                    <td>'.$v_relativo_resolucion_sonometro.'</td>
                    <td>B, k=1</td>
                    <td>'.$ie_v_relativo_resolucion_sonometro.'</td>
                    <td>'.$fi_resolucion.'</td>
                    <td>'.$contribucion_r.'</td>
                    <td>'.round($contribucion_r_pow,5).'</td>
                </tr>
                <tr>
                    <th>Sonómetro</th>
                    <td>'.$v_relativo_certif_calibracion_sonometro.'</td>
                    <td>B, k=2</td>
                    <td>'.$ie_v_relativo_certif_calibracion_sonometro.'</td>
                    <td>'.$fi_sonometro.'</td>
                    <td>'.$contribucion_s.'</td>
                    <td>'.round($contribucion_s_pow,5).'</td>
                </tr>
                <tr>
                    <th>Calibrador</th>
                    <td>'.$v_relativo_certif_calibracion_calibrador_acustico.'</td>
                    <td>B, k=2</td>
                    <td>'.$ie_v_relativo_certif_calibracion_calibrador_acustico.'</td>
                    <td>'.$fi_calibrador.'</td>
                    <td>'.$contribucion_c.'</td>
                    <td>'.round($contribucion_c_pow,5).'</td>
                </tr>
                <tr>
                    <td colspan="6"></td>
                    <td>'.round($fuentes_insertidumbre_sum,4).'</td>
                </tr>
            </table><p></p>';
            $fu_in_sum_pow=sqrt($fuentes_insertidumbre_sum);
            $fu_in_sum_pow_u_rel=$fu_in_sum_pow*2;
            $fu_in_log=20*log10(1+$fu_in_sum_pow_u_rel);
        $htmlg.='<table><tr>
                <td><table class="tableb"><tr><th>uc(rel)</th><td>'.round($fu_in_sum_pow,5).'</td></tr><tr><th>U(rel)</th><td>'.round($fu_in_sum_pow_u_rel,5).'</td></tr><tr><th>U(rel) convertida a dB</th><td>'.round($fu_in_log,2).'</td></tr></table></td>
                <td></td>
                <td><table class="tableb"><tr><td>U (dB)</td><td>'.round($fu_in_log,2).'</td></tr></table></td>
                <td></td>
                <td><p>Factor de cobertura k=2</p><p>Nivel de Confianza 95.45%</p></td>
                </tr></table>
                <table><tr><td height="160px"></td></tr></table>';*/
}
$pdf->writeHTML($htmlg, true, false, true, false, '');

// ---======================================================================================---


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Informe de calibración del equipo de medición ----------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IV</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">INFORME DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$datosequipoc=$this->ModeloNom->getCertEquipos($id_sonometro,$id_calibrador);
foreach ($datosequipoc as $item) {
    $htmlg='';
    $pdf->AddPage('P', 'A4');
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgEquipos/'.$item->certificado.'"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt='Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt=$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
}
 
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Documento de acreditación ------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO V</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE ACREDITACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

if($datosacre->num_rows()>0){
    $htmlg='';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4'); 
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);

        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
    }   
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('VI. Documento de aprobación -------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VI</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE APROBACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$txt_fin=""; $cont_acre=0;
if($datosacre2->num_rows()>0){
    $htmlg='';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2=0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima
        $cont2++;
        $pdf->SetMargins(20,25,20); 
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->AddPage('P', 'A4');
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);
        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        if($cont_acre==$cont2){
            /*$txt_fin.='<table>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p>
                    </td>
                </tr>
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true);
            //$pdf->writeHTML($txt_fin, true, false, true, false, '');

            /*
            $pdf->SetXY(60, 221);
            $pdf->SetTextColor(0,57,88);
            $pdf->SetFont('dejavusans', '', 10);
            $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas';
            $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
            $pdf->Cell(100, 50, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

            $pdf->SetXY(55, 225);
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('dejavusans', '', 10);
            $pdf->Cell(100, 50, $txt_fin2, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
            */
        }
    }
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    /*$pdf->SetXY(50, 150);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 23);
    $txt='Documento Informativo '.$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/

    $pdf->SetXY(60, 251);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas.';
    $pdf->Cell(100, 10, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
    
    $pdf->SetXY(55, 255);
    $pdf->SetTextColor(255,0,0);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
    $pdf->Cell(100, 10, $txt_fin2,  0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

}


// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));

$bookmark_templates[0]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();



$pdf->Output('EntregableNom81.pdf', 'I');

?>