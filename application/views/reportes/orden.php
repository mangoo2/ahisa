<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $margin_left=18;
    $margin_top=27;
    $margin_right=18;
//=======================================================================================
class MYPDF extends TCPDF {
    public function Header() {
        $headimg = FCPATH.'public/img/headpdf.png';
        $html = '<table border="1" cellpadding="5" align="center" style="font-size:9px;">
                <tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg"></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr>
                <tr><td><i>ORDEN DE SERVICIOS</i></td><td><i>REG-RP/01-03</i></td><td><i>04</i></td><td><i>ORIGINAL</i></td></tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }

    public function Footer() {
        $footimg = FCPATH.'public/img/foot_ord.jpg';
        $this->Image($footimg, -1, 285, 212, 5, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $html = '
            <style type="text/css">
                .footerpage{font-size:8px;font-style: italic;}
            </style> 
          <table width="100%" cellpadding="2" border="0"><tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' </td></tr></table>';
        $this->writeHTMLCell(376, '', 12, 290, $html, 0, 0, 0, true, 'C', true);
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Orden');
$pdf->SetTitle('Orden');
$pdf->SetSubject('Orden');
$pdf->SetKeywords('Orden');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins($margin_left, $margin_top, $margin_right);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(18);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 20);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 12);
// add a page
$pdf->AddPage('P', 'A4');
$htmlg='';
$htmlg.='<style type="text/css">

<style type="text/css">
    .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:10px;}
    .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:10px;valign: middle;vertical-align: middle;}
    .table td{border-bottom:1px solid #808080;font-family:Arial;text-align: center;font-size:10px;valign: middle;vertical-align: middle;}
    .pspaces{ font-size: 0.2px;}
    .gral{ font-family:Arial Nova;text-align: center;font-size:10px;valign: middle;vertical-align: middle; }
    .checked{font-size:10px;font-family:dejavusans;}
</style>

<div class="gral">
    <table class="table" width="100%" cellpadding="4">
        <tr>
            <th>Fecha de solicitud</th>
            <td>'.$dets->fecha_solicita.'</td>
            <th>Fecha de servicio</th>
            <td>'.$dets->fecha_servicio.'</td>
        </tr>
        <tr>
            <th>Ref. No. de cotización</th>
            <td>'.$orden->folio.'</td>
            <th>No. de Orden</th>
            <td>'.$id.'</td>
        </tr>
        <tr>
            <th colspan="2">Ingeniero(s) de campo asignado(s)</th>
            <td colspan="2">'.$dets->ing_asignado.'</td>
        </tr>
    </table>
    <table>
        <tr><td height="20px"></td></tr>
    </table>
    <table class="tableb" width="100%" cellpadding="4">
        <thead>
            <tr>
                <th colspan="2">DATOS DEL CLIENTE DONDE SE EJECUTA EL SERVICIO</th>
            </tr>
        </thead>
        <tr>
            <td style="text-align:left" width="20%">Nombre o razón social:</td>
            <td width="80%">'.$dets->nom_razon.'</td>
        </tr>
        <tr>
            <td style="text-align:left" width="20%">R.F.C.:</td>
            <td width="80%">'.$dets->rfc.'</td>
        </tr>
        <tr>
            <td style="text-align:left" width="20%">Dirección:</td>
            <td width="80%">'.$dets->direc.'</td>
        </tr>
        <tr>
            <td style="text-align:left" width="20%">Giro de la empresa:</td>
            <td width="80%">'.$dets->giro.'</td>
        </tr>
    </table>
    <br></br>
    <br></br>
    <table class="tableb" width="100%" cellpadding="4">
        <thead>
            <tr><th colspan="3">ALCANCE</th></tr>
            <tr>
                <th width="50%">Servicio</th>
                <th width="10%">Cantidad</th>
                <th width="40%">Observaciones</th>
            </tr>
        </thead>
        <tbody>';

        foreach ($serv as $s) {
            //log_message('error', 'comentarios: '.$s->comentarios);
            $htmlg.='
                <tr>
                    <td width="50%"> 
                        '.$s->nombre2.'
                        '.$s->descripcion2.'
                    </td>
                    <td width="10%">'.$s->cantidad.'</td>
                    <td width="40%">'.$s->comentarios.'</td>
                </tr>';
        }
    $info_rl=$dets->info_rl;
    if($info_rl=="") $info_rl="--";
    $info_te=$dets->info_te;
    if($info_te=="") $info_te="--";
    $info_ta=$dets->info_ta;
    if($info_ta=="") $info_ta="--";
    $info_tf=$dets->info_tf;
    if($info_tf=="") $info_tf="--";
    $info_il=$dets->info_il;
    if($info_il=="") $info_il="--";
    $info_rp=$dets->info_rp;
    if($info_rp=="") $info_rp="--";
    $htmlg.='</tbody>
    </table>
    
    <table>
        <tr><td height="20px"></td></tr>
    </table>
    <table class="tableb" width="100%" cellpadding="4">
        <thead>
            <tr><th colspan="6">No. DE INFORMES DE SERVICIOS</th></tr>
        </thead>
        <tbody>
            <tr>
                <th>RL</th>
                <td>'.$info_rl.'</td>
                <th>TE</th>
                <td>'.$info_te.'</td>
                <th>TA</th>
                <td>'.$info_ta.'</td>
            </tr>
            <tr>
                <th>TF</th>
                <td>'.$info_tf.'</td>
                <th>IL</th>
                <td>'.$info_il.'</td>
                <th>RP</th>
                <td>'.$info_rp.'</td>
            </tr>
        </tbody>

    </table>
    <p style="font-size:8px">RL= Ruido Laboral; TE= Temperaturas Elevadas; TA= Temperaturas Abatidas; TF= Tierras Físicas; IL= Iluminación; RP= Ruido Perimetral</p>
    <table>
        <tr><td><br></td></tr>
    </table>';
    $doc_imss="--"; $doc_sua="--"; $doc_certif="--"; $doc_analisis="--"; $doc_listado="--"; $doc_permiso="--";
    if($dets->doc_imss==1)
        $doc_imss='<p style="color:green" class="checked">✔</p>';
    if($dets->doc_sua==1)
        $doc_sua='<p style="color:green" class="checked">✔</p>';
    if($dets->doc_certif==1)
        $doc_certif='<p style="color:green" class="checked">✔</p>';
    if($dets->doc_analisis==1)
        $doc_analisis='<p style="color:green" class="checked">✔</p>';
    if($dets->doc_listado==1)
        $doc_listado='<p style="color:green" class="checked">✔</p>';
    if($dets->doc_permiso==1)
        $doc_permiso='<p style="color:green" class="checked">✔</p>';

    $htmlg.='<table class="tableb" width="100%" cellpadding="4">
        <thead>
            <tr><th colspan="6">DOCUMENTACIÓN DE ACCESO</th></tr>
        </thead>
        <tbody>
            <tr>
                <th width="22%">Alta en el IMSS</th>
                <td width="11%">'.$doc_imss.'</td>
                <th width="23%">Copia de pago de SUA</th>
                <td width="11%">'.$doc_sua.'</td>
                <th width="22%">Certificado médico</th>
                <td width="11%">'.$doc_certif.'</td>
            </tr>
            <tr>
                <th width="22%">Análisis clínicos</th>
                <td width="11%">'.$doc_analisis.'</td>
                <th width="23%">Listado de equipo</th>
                <td width="11%">'.$doc_listado.'</td>
                <th width="22%">Permiso vehicular</th>
                <td width="11%">'.$doc_permiso.'</td>
            </tr>
            <tr>
                <th>Otro</th>
                <td colspan="5">'.$dets->doc_otro.'</td>
            </tr>
        </tbody>
    </table>

    <table>
        <tr><td><br><br></td></tr>
    </table>';

    $botas="--"; $casco="--"; $chaleco="--"; $lentes="--"; $tapones="--"; $guantes="--";
    if($dets->botas==1)
        $botas='<p style="color:green" class="checked">✔</p>';
    if($dets->casco==1)
        $casco='<p style="color:green" class="checked">✔</p>';
    if($dets->chaleco==1)
        $chaleco='<p style="color:green" class="checked">✔</p>';
    if($dets->lentes==1)
        $lentes='<p style="color:green" class="checked">✔</p>';
    if($dets->tapones==1)
        $tapones='<p style="color:green" class="checked">✔</p>';
    if($dets->guantes==1)
        $guantes='<p style="color:green" class="checked">✔</p>';

    $htmlg.='<table class="tableb" width="100%" cellpadding="3">
        <thead>
            <tr><th colspan="6">EQUIPO DE PROTECCIÓN PERSONAL</th></tr>
        </thead>
        <tbody>
            <tr>
                <th width="22%">Botas de seguridad</th>
                <td width="11%">'.$botas.'</td>
                <th width="23%">Casco de seguridad</th>
                <td width="11%">'.$casco.'</td>
                <th width="22%">Chaleco de seguridad</th>
                <td width="11%">'.$chaleco.'</td>
            </tr>
            <tr>
                <th width="22%">Lentes de seguridad</th>
                <td width="11%">'.$lentes.'</td>
                <th width="23%">Tapones auditivos</th>
                <td width="11%">'.$tapones.'</td>
                <th width="22%">Guantes de protección</th>
                <td width="11%">'.$guantes.'</td>
            </tr>
            <tr>
                <th>Otro</th>
                <td colspan="5">'.$dets->otro_equipo.'</td>
            </tr>
        </tbody>
    </table>
    
    <table>
        <tr><td><br></td></tr>
    </table>
    <table class="tableb" width="100%" cellpadding="3">
        <thead>
            <tr><th colspan="2">CONFORMIDAD DEL SERVICIO</th></tr>
        </thead>
        <tbody>
            <tr>
                <td height="75px"></td>
                <td height="75px"></td>
            </tr>
            <tr>
                <th>Nombre y firma del ingeniero de campo</th>
                <th>Nombre y firma del cliente o representante</th>
            </tr>
        </tbody>
    </table>

</div>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->Output('orden_'.$id.'.pdf', 'I');