<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    /*$margin_left=15;
    $margin_top=27;
    $margin_right=15;*/
    //$folio='AHISA/2022EP-00AL';
    
    //==== datos taltantes del equipo en su catalogo
        $equipo_marca='EVERFINE';
        $equipo_modelo='Z-10';
        $equipo_num_serie='M154879CM1371120';
        $equipo_no_informe_calibracion='SIMH-OPTICA/0308-2021';
        $equipo_fecha_calibracion='20/09/2021';
    //=======================

    $norma_name='NOM-025-STPS-2008';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);
    $GLOBALS['tablageneral1']='';
    $GLOBALS['tablageneral2']='';
    $GLOBALS['tablageneral1char']='';
    $GLOBALS['tablageneral2char']='';

    foreach ($datosnom->result() as $item) {
        $GLOBALS['tablageneral1']=$item->tablageneral1;
        $GLOBALS['tablageneral2']=$item->tablageneral2;

        $GLOBALS['tablageneral1char']=$item->tablageneral1char;
        $GLOBALS['tablageneral2char']=$item->tablageneral2char;
        $fecha = $item->fecha;

        $datosequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$item->equipo));
        //$folio='AHISA/'.date("Y", strtotime($item->fecha)).'EP-0'.$item->idnom.'AL';
        $folio=$item->num_informe;
        $idequipo=0;
        $equipo=0;
        foreach ($datosequipo->result() as $itemeq) {
            $idequipo=$itemeq->id;
            $equipo_num_serie=$itemeq->equipo;
            $equipo_marca=$itemeq->marca;
            
            $equipo_modelo=$itemeq->modelo;
            $equipo_no_informe_calibracion=$itemeq->no_informe_calibracion;
            $equipo_fecha_calibracion=$itemeq->fecha_calibracion;
        }
    }
    $datosequipoc=$this->ModeloCatalogos->getselectwheren('equipo_certificado',array('id_equipo'=>$idequipo,'estatus'=>1));
    
    setlocale(LC_ALL, 'es_ES');
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());

    foreach ($datosoc->result() as $item) {
        //$empresa =$item->empresa;
        //$direccion = $item->calle.''.$item->no_ext.', '.$item->no_int.', '.$item->colonia.', '.$item->estado.', CP.'.$item->cp_fiscal.'.';
        $tecnico = $item->tecnico;
        //$rfccli=$item->rfc;
        //$girocli=$item->giro;
        //$representacli=$item->representa;
        //$telefonocli=$item->telefono; 
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $direccion = $item->calle_num.' COL. '.$item->colonia.', <br>'.$item->poblacion.', '.$item->estado.',<br> CP.'.$item->cp.'';
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $logos = base_url().'public/img/logo.jpg';
      $html = '<table width="110%"><tr><td style="font-size:15px"><img src="'.$logos.'" width="180px"></td><td></td><td style="" align="right"></td></tr></table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $folio=$GLOBALS['folio'];
      $html = '
        <style type="text/css">
            .cmac{font-size:9px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
            .cma{font-size:15px; color:rgb(231,99,0);}
            .cmafolio{font-size:9px; background-color: rgb(231,99,0); font-weight: bold;}
            .footerpage{font-size:9px;font-style: italic;}
        </style> 
      <table width="100%" cellpadding="3">
        <tr><td align="center" colspan="2" style="font-size:9px;font-style: italic;">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr>
        <tr><td align="center" class="cmac" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx
            </td><td align="center" class="cmafolio" width="16%">'.$folio.'</td></tr>
        <tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$folio.' </td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
      $this->writeHTMLCell(189, '', 10, 280, $html, 0, 0, 0, true, 'C', true);
  }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('Entregable');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(15,27,15);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(20);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 20);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 12);
// add a page
//$pdf->AddPage('P', 'A4'); 
$pdf->AddPage('P', 'A4');
$htmlg='';
$htmlg.='<style type="text/css">
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:12px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
    .sangria{text-indent: 45px;}
</style>';
$htmlg.='
        <table border="0">
            <tr >
                <td colspan="2" align="right" style=" font-size:18px; font-weight: bold;">'.$empresa.'</td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td width="62%" ></td>
                <td width="38%" align="right" style=" font-size:12px; font-weight: bold;" >'.$direccion.'</td>
            </tr>
        </table>
        <table border="0">
            <tr><td></td></tr>
            <tr>
                <td style="font-size:14px; font-weight: bold; font-style: italic;">'.$GLOBALS['nom_cargo'].'</td>
            </tr>
            <tr>
                <td style="font-size:11px; font-weight: bold;font-style: italic;">TÉCNICO EN SEGURIDAD INDUSTRIAL.</td>
            </tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes a los niveles de iluminación y factor de reflexión; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día '.$fecha.'.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="200px" colspan="2"></td></tr>
            <tr>
                <td>Ing. Christian Uriel Fabian Romero</td>
                <td>Ing. Tanya Alejandra Valle Tello</td>
            </tr>
            <tr>
                <td>Aprobó - Gerente General</td>
                <td>Revisó – Procesamiento de Información</td>
            </tr>
            <tr><td colspan="2" height="100px"></td></tr>
            <tr>
                <td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');




/* *********************************/

$pdf->AddPage('P', 'A4');
$razon_social=""; $fecha=""; $num_informe=""; $veri_ini=""; $veri_fin=""; $criterio=""; $cumple_criterio="";
foreach ($datosnom->result() as $i) { 
    $razon_social=$i->razon_social; $fecha=$i->fecha; $num_informe=$i->num_informe; $veri_ini=$i->veri_ini; $veri_fin=$i->veri_fin; $criterio=$i->criterio; $cumple_criterio=$i->cumple_criterio;
}

$luxometro=""; $equipo=""; $marca=""; $modelo="";
foreach ($equi->result() as $i) { 
    $luxometro=$i->luxometro; $equipo=$i->equipo; $marca=$i->marca; $modelo=$i->modelo;
}
$htmlg='';
$htmlg1='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:#c6c9cb;}
                .fon9{font-size:9px;}
                .checked{font-size:17px}
        </style>
        <table border="1" cellpadding="5" align="center" class="fon9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>MEDICIÓN DE CAMPO PARA LA MEDICIÓN DE LA ILUMINACIÓN</td>
                <td>REG-TEC/05-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>';
    $cont_fecha=0;$cont_fecha2=0; $fecha_ant=""; $rowpuntorow=1;
    foreach ($datosnomdetalle as $itemd) {
        if($fecha_ant!=$itemd->fecha){
            $cont_fecha++;
        }
    }
    foreach ($datosnomdetalle as $itemd) {
        if($fecha_ant!=$itemd->fecha){
            $cont_fecha2++;
            $htmlg1.='
            <table border="1" cellpadding="5" align="center" class="fon9">
                <tr>
                    <td class="backg">RAZÓN SOCIAL</td>
                    <td colspan="8" >'.$razon_social.'</td>
                </tr>
                <tr>
                    <td class="backg">FECHA</td>
                    <td colspan="3" >'.$fecha.'</td>
                    <td colspan="2" class="backg">No DE INFORME</td>
                    <td colspan="3" >'.$num_informe.'</td>
                </tr>
                <tr class="backg">
                    <td >ID EQUIPO</td><td >'.$luxometro.'</td>
                    <td >MARCA</td><td >'.$marca.'</td>
                    <td >MODELO</td><td >'.$modelo.'</td>
                    <td >SERIE</td><td >'.$equipo.'</td>
                    <td >X</td>
                    
                </tr>
                <tr class="backg">
                    <td colspan="2" >VERIFICACIÓN INICIAL</td>
                    <td colspan="2" >VERIFICACIÓN FINAL</td>
                    <td colspan="2" >CRITERIO DE ACEPTACIÓN</td>
                    <td colspan="3" >CUMPLE CRITERIO</td>
                </tr>
                <tr>
                    <td colspan="2" >'.$veri_ini.'</td>
                    <td colspan="2" >'.$veri_fin.'</td>
                    <td colspan="2" >'.$criterio.'</td>
                    <td colspan="3" >'.$cumple_criterio.'</td>
                </tr>
                <tr class="backg">
                    <td colspan="5">NOMBRE Y FIRMA DEL INGENIERO DE CAMPO</td>
                    <td colspan="4">NOMBRE Y FIRMA DE RESPONSABLE DE LA REVISIÓN DEL REGISTRO DE CAMPO</td>
                </tr>
                 <tr><td colspan="5"></td><td colspan="4"></td></tr>
                <tr>
                    <td colspan="9" class="backg">CONDICIONES DE OPERACIÓN DAL MOMENTO DE REALIZAR LA MEDICIÓN</td>
                </tr>
                 <tr><td colspan="9"></td></tr>       
            </table>';
            
            $htmlg1.='<table border="1" cellpadding="5" align="center" class="fon9" width="100%">';
            $htmlg1.='<tr class="backg">
                        <td width="5%" rowspan="3">No</td>
                        <td width="13%" rowspan="3">ÁREA</td>
                        <td width="15%" rowspan="3">IDENTIFICACIÓN DEL PUNTO</td>
                        <td width="7%" rowspan="3" colspan="2">PLANO DE TRABAJO</td>
                        <td width="10%" rowspan="3">HORA</td>
                        <td width="20%" colspan="3" rowspan="3">NIVEL DE ILUMINACIÓN</td>
                        <td width="20%" colspan="4">REFLEXIÓN(lx)</td>
                        <td width="10%" rowspan="3">NMI</td>
                    </tr>
                    <tr class="backg">
                        <td colspan="2">PLANO DE TRABAJO</td>
                        <td colspan="2">PARED</td>
                    </tr>
                    <tr class="backg"><td>E1</td><td >E2</td><td >E1</td><td >E2</td></tr>';
            
                //}   
                   
                if($itemd->plano_t1=="v"){
                    $ptra='<p class="checked">✔</p>';
                }else{
                    $ptra='';
                }
                if($itemd->plano_t1=="h"){
                    $ptra2='<p class="checked">✔</p>';
                }else{
                    $ptra2='';
                }
                if($itemd->plano_t1=="o"){
                    $ptra3='<p class="checked">✔</p>';
                }else{
                    $ptra3='';
                } 

                $htmlg1.='<tr>
                    <td width="5%" rowspan="3">'.$rowpuntorow.'</td><td width="13%" rowspan="3">'.$itemd->area.'</td><td width="15%" rowspan="3">'.$itemd->identifica.' <br>'.$itemd->identifica2.'</td><td>H</td><td width="7%">'.$ptra.'</td><td width="10%">'.date("H:i", strtotime($itemd->h1)).'</td><td width="20%">'.$itemd->h1_medicion_a.'</td><td>'.$itemd->h1_medicion_b.'</td><td>'.$itemd->h1_mdicion_c.'</td><td width="20%">'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td><td width="10%" rowspan="3">'.$itemd->nmi.'</td>
                </tr>
                <tr>
                    <td>V</td><td>'.$ptra2.'</td><td>'.date("H:i", strtotime($itemd->h2)).'</td><td>'.$itemd->h2_medicion_a.'</td><td>'.$itemd->h2_medicion_b.'</td><td>'.$itemd->h2_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
                </tr>
                <tr>
                    <td>O</td><td>'.$ptra3.'</td><td>'.date("H:i", strtotime($itemd->h3)).'</td><td>'.$itemd->h3_medicion_a.'</td><td>'.$itemd->h3_medicion_b.'</td><td>'.$itemd->h3_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
                </tr>';
                //if($cont_fecha2==$cont_fecha){
                    $htmlg1.='</table>';
                //}
            $fecha_ant=$itemd->fecha; 
        } //if
        else{
            $htmlg.='<table border="1" cellpadding="5" align="center" class="fon9">';
            //}   
            $fecha_ant=$itemd->fecha;    
            if($itemd->plano_t1=="v"){
                $ptra='<p class="checked">✔</p>';
            }else{
                $ptra='';
            }
            if($itemd->plano_t1=="h"){
                $ptra2='<p class="checked">✔</p>';
            }else{
                $ptra2='';
            }
            if($itemd->plano_t1=="o"){
                $ptra3='<p class="checked">✔</p>';
            }else{
                $ptra3='';
            } 

            $htmlg.='<tr>
                <td rowspan="3">'.$rowpuntorow.'</td><td rowspan="3">'.$itemd->area.'</td><td rowspan="3">'.$itemd->identificacion.'</td><td>H</td><td>'.$ptra.'</td><td>'.date("H:i", strtotime($itemd->h1)).'</td><td>'.$itemd->h1_medicion_a.'</td><td>'.$itemd->h1_medicion_b.'</td><td>'.$itemd->h1_mdicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td><td rowspan="3">'.$itemd->nmi.'</td>
            </tr>
            <tr>
                <td>V</td><td>'.$ptra2.'</td><td>'.date("H:i", strtotime($itemd->h2)).'</td><td>'.$itemd->h2_medicion_a.'</td><td>'.$itemd->h2_medicion_b.'</td><td>'.$itemd->h2_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
            </tr>
            <tr>
                <td>O</td><td>'.$ptra3.'</td><td>'.date("H:i", strtotime($itemd->h3)).'</td><td>'.$itemd->h3_medicion_a.'</td><td>'.$itemd->h3_medicion_b.'</td><td>'.$itemd->h3_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
            </tr>
            </table>
            <p></p>';
        }
        $rowpuntorow++;    
    } //foreach
    
    //$htmlg.='</tbody>
    //</table>';
    
$pdf->writeHTML($htmlg1, true, false, true, false, ''); 


$pdf->Output('Entregable.pdf', 'I');

?>