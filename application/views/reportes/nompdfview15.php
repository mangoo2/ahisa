<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    log_message('error','Nom15');
    $norma_name='NOM-015-STPS-2001';

    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');
    mb_internal_encoding("UTF-8");
    
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());
    $fechah = ucfirst($fechah);

    $firmaemple=""; $cedula=""; $firmaresp=""; $tecnico="";
    foreach ($datosoc->result() as $item) {
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $cedula=$item->cedula;
        $firmaresp=$item->firmaresp;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $estado_name=utf8_decode($estado_name);
        $direccion = mb_strtoupper($item->calle_num.', <br> '.($item->colonia).', <br>'.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $direccion2 = mb_strtoupper($item->calle_num.', '.($item->colonia).', '.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
        $logos = base_url().'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
  }
    // Page footer
  public function Footer() {
      $html = '<style> .fts9{font-size:9px;} .cmac{background-color: #003958; font-weight: bold; color:white;} .cma{font-size:15px; color:#e76300;} .cmafolio{background-color: #e76300; font-weight: bold; margin-top:10px;} .footerpage{font-style: italic;} table{text-align:center} </style> <table width="100%" cellpadding="2"> <tr><td colspan="2" class="footerpage fts9">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr> <tr><td class="cmac fts9" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx</td><td class="cmafolio fts9" width="16%"><span class="cma">•</span>'.$GLOBALS['folio'].'</td></tr><tr><td colspan="2" class="footerpage fts9" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$GLOBALS['folio'].' </td></tr></table>';
      //$this->writeHTML($html, true, false, true, false, '');
      $this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
  }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-015 Abatidas');
$pdf->SetSubject('Entregable Abatidas');
$pdf->SetKeywords('NOM-015 Abatidas');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,25,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 27);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page
$pdf->AddPage('P', 'A4'); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style>
        td{font-family:calibri;}
        .tdtext{text-align: justify;font-size:15px;font-family: Calibri;}
        .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
        .sangria{text-indent: 45px;}
        .fontbold{font-weight: bold;}
        .algc{text-align:center; }
        .ft09{font-size:9px;}
        .ft20{font-size:20px;}
        .ft18{font-size:18px;}
        .ft16{font-size:16px;}
        .ft14{font-size:14px;}
        .ft13{font-size:13px;}
        .ft12{font-size:12px;}
        .ft11{font-size:11px;}
    </style>';
$htmlg.='
        <table border="0"><tr><td colspan="2" align="right" class="ft20 fontbold">'.$empresa.'</td></tr><tr><td colspan="2"></td></tr><tr><td width="50%" ></td><td width="50%" align="right" class="ft14 fontbold">'.$direccion.'</td>
            </tr></table>
        <table border="0"><tr><td></td></tr><tr><td class="fontbold ft16"><b>'.$nom_cargo.'</b></td></tr><tr><td class="fontbold ft13"><b>'.$cargo_cargo.'</b></td></tr></table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes a los niveles de condiciones térmicas abatidas determinadas en su centro de trabajo; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana  '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fecha.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="180px" colspan="2"></td></tr>
            <tr>
                <td>Ing. Christian Uriel Fabian Romero</td>
                <td>Ing. Tanya Alejandra Valle Tello</td>
            </tr>
            <tr>
                <td>Aprobó - Gerente General</td>
                <td>Revisó – Procesamiento de Información</td>
            </tr>
            <tr><td colspan="2" height="170px"></td></tr>
            <tr>
                <td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

/*$pdf->writeHTMLCell(80, '', 22, 170, '<p style="color: #003958;font-family: Calibri;font-size:12px;"><i>Ing. Christian Uriel Fabian Romero<br>Aprobó - Gerente General</i></p>', 0, 0, 0, true, 'C', true);
$pdf->writeHTMLCell(80, '', 103, 170, '<p style="color: #003958;font-family: Calibri;font-size:12px;"><i>Ing. Tanya Alejandra Valle Tello<br>Revisó – Procesamiento de Información</i></p>', 0, 0, 0, true, 'C', true);
$pdf->writeHTMLCell('', '', 15, 240, '<img src="'.base_url().'public/img/logo_ema.jpg" width="180px">', 0, 0, 0, true, 'C', true);
*/

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1));
foreach ($rowpersonal->result() as $itemp) { 
    $dosis[$itemp->num]=$itemp->dosis;                                                   
}

$htmlg='<style>
            td{font-family:calibri;} .tdtext{font-family:Arial;text-align: justify;font-size:14px;} .titulosa{ font-size:20px;text-align:center,font-weight:bold;} .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;} .ft09{font-size:9px;} .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:9px;valign: middle;vertical-align: middle;} .tableb th{background-color:#d9d9d9;border:1px solid #808080;text-align:center;font-size:10px;} .sangria{text-indent: 45px;}
            .pspaces{font-size:0.2px;}
            </style>
            <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
            <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <strong>'.$empresa.'</strong>, el día '.$fecha.'.</p>
            <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p>

            <p class="tdtext1"><i>• Condiciones Térmicas Abatidas</i></p>';
            $htmlg.='<table class="tableb"><thead><tr>
                <th width="5%">No. de punto</th>
                <th width="30%">Identificación del punto de medición (área/identificación)</th>
                <th width="5%"><p class="pspaces"></p>Ciclo</th>
                <th width="10%"><p class="pspaces"></p>IVF<sub>Prom</sub> °C</th>
                <th width="5%"><p class="pspaces"></p>T.E (h)</th>
                <th width="5%"><p class="pspaces"></p>LMPE</th>
                <th width="10%">T.A <br>Inicial (°C)</th>
                <th width="10%">T.A <br>Final (°C)</th>
                <th width="20%"><p class="pspaces"></p>Conclusión</th></tr><thead>';
            $pton=$this->ModeloCatalogos->infonomelevadas($idnom);
            $punto=1;
            $txt_ptp_sup=""; $ptos_sup="";
            $txt_ptp_no_sup=""; $ptos_no_sup="";
            foreach ($pton->result() as $p) { 
                $tiempo_expo=round($p->tiempo_expo/60,2);
                $lmpe=8.0;
                if($tiempo_expo>$lmpe){
                    $conclusion="El TE supera el LMP";
                    $ptos_sup.=$p->num_punto. ",";
                }else{
                    $conclusion="El TE no supera el LMP";
                    $ptos_no_sup.=$p->num_punto. ",";
                }
                $htmlg.='<tr><td>'.$punto.'</td><td>'.$p->area.'</td><td>'.$p->num_ciclos.'</td><td>'.$p->lvf_prom.'</td><td>'.$tiempo_expo.'</td><td>'.$lmpe.'</td><td>'.$p->temp_fin.'</td><td>'.$p->temp_fin.'</td><td>'.$conclusion.'</td></tr>';
                $punto++;
            }
            $htmlg.='</table>';
            if($ptos_sup!=""){
                $txt_ptp_sup='<p class="tdtext tejus sangria"> 1. Los puntos evaluados e identificado con número '.$ptos_sup.' superan el LMPE (límite máximo permisible de Exposición) correspondiente al indice de viento frío promedio de cada punto evaluado.</p>';
            }
            if($ptos_no_sup!=""){
                if($ptos_sup!=""){
                    $num=2;
                }else{
                    $num=1;
                }
                $txt_ptp_no_sup='<p class="tdtext tejus sangria"> '.$num.'. Los puntos evaluados e identificado con número '.$ptos_no_sup.' no superan el LMPE (límite máximo permisible de Exposición) correspondiente al indice de viento frío promedio de cada punto evaluado.</p>';
            }


            $htmlg.='<p class="ft09 algc">IVFprom = Índice de Viento Frío promedio<br>T.E.= Tiempo de exposición en horas<br>LMPE = Límite Máximo Permisible de Exposición en horas<br>T.A. = Temperatura Axilar</p>';
    $htmlg.='<p class="ft09 algc"><u><i>Regla de Decisión</i></u></p><p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlgtab="";
$htmlg='<style> .titulosa{font-size:20px;text-align:center;} .tdtext{font-family:Arial;text-align: justify;font-size:14px;} </style>
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p><p class="tdtext">Derivado de los resultados obtenidos se presentan las siguientes conclusiones: </p>
        '.$txt_ptp_sup.'
        '.$txt_ptp_no_sup.'
        <p class="tdtext">A continuación, se presenta un gráfico como resumen de los resultados obtenidos en las envaluaciones realizadas. </p>';

        if($grafica_abatida == ''){
            $htmlg.='<table><tr><td></td></tr></table>';    
        }else{
            $htmlg.='<table><tr><td><img src="'.base_url().'public/charts/'.$grafica_abatida.'"></td></tr></table>';
        }

$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style> .titulosa{font-size:20px;text-align:center;} .tdtext{font-family:Arial;text-align: justify;font-size:15px;} </style>
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
        <p class="tdtext">La higiene Industrial es un sistema de principios y reglas dedicados al reconocimiento, evaluación y control de factores del ambiente o tensionales de riesgo, que provienen del trabajo y que pueden causar enfermedades o deteriorar la salud.</p> <p class="tdtext">Dentro de los principales factores de riesgo ambiental, se encuentra el efecto fisiológico del calor sobre los trabajadores, la tensión y el riesgo originado por el calor debido a las condiciones térmicas, dependen del efecto combinado de la temperatura ambiente, la humedad, la velocidad del aire y la radiación, así como también del esfuerzo físico, del vestuario y de las características propias del trabajador.</p> <p class="tdtext">Un ambiente frío se define por unas condiciones que causan pérdidas de calor corporal mayores de lo normal. En este contexto, "normal" se refiere a lo que una persona experimenta en la vida diaria en condiciones térmicas neutras, normalmente en interiores, aunque es un concepto que puede variar en función de factores sociales, económicos o climáticos.</p> <p class="tdtext">Una condición térmica extrema es la situación ambiental, originadas por una temperatura elevada o abatida, capaz de permitir una ganancia (superior a 38o C) o pérdida (inferior a 36o C) de calor en el cuerpo humano, de tal manera que modifique el equilibrio térmico del trabajador y que ocasione un incremento o decremento en su temperatura corporal central, capaz de alterar su salud.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado -----------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='';
$htmlg0='';
$htmlg.='<style> .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} .tdtext1{font-family:Arial;text-align: center;font-size:11px;} .titulosa{font-size:20px;text-align:center;margin:0px;} .tableb td, .tableb th{border:1px solid #808080; font-style: italic;} .sp_bck{ font-weight:bold;} .pspaces{ font-size:0.2px; } .tableb tr th{background-color:#d9d9d9;} .font10{font-size:10px;} .fontcenter{text-align:center;} </style>';
        $htmlg.='<p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>';
        $htmlg.='<p>Determinar la temperatura (IVF) de los puestos de trabajo del centro de trabajo <span class="sp_bck"><b>'.$empresa.',</b></span> y compararlos contra los límites máximos permisibles.</p>';
        $htmlg.='<p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>';
        $htmlg.='<p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Condiciones térmicas elevadas o abatidas – Condiciones de seguridad e higiene".</p>';

        $htmlg.='<p class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO* </b></p>';
$htmlg.='<table cellpadding="4" class="tdtext1 tableb"><tr><th width="41%">a)  Nombre, denominación o razón social</th>
                <td width="59%">'.$empresa.'</td></tr><tr><th><p class="pspaces"></p>b)  Domicilio</th><td>'.$direccion2.'</td></tr><tr><th>c)  R.F.C.</th><td>'.$rfccli.'</td></tr><tr><th>d)  Giro y/o actividad principal</th><td>'.$girocli.'</td></tr><tr><th>e)  Responsable de la empresa</th><td>'.$representacli.'</td></tr><tr><th>f)  Teléfono</th><td> '.$telefonocli.'</td></tr></table>';
$htmlg.='<table><tr><td class="font10 fontcenter">* Información suministrada por el cliente</td></tr></table>';
        $htmlg.='<p class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></p>';
        $htmlg.='<table cellpadding="4" class="tdtext1 tableb"><tr><th width="41%">a) Nombre, denominación o razón social</th><td width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr><tr><th>b) Domicilio</th><td>19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td></tr><tr><th>c) R.F.C.</th><td>ALP160621FD6</td></tr><tr><th>d) Teléfono</th><td>(01) 222 2265395</td></tr><tr><th>e) e-mail </th><td>gerencia@ahisa.mx</td></tr><tr><th>f) Número de acreditación </th><td>AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td></tr><tr><th>g) Número de aprobación</th><td>LPSTPS-153/2022</td></tr><tr><th>h) Lugar de expedición del informe </th><td>Puebla, Puebla - México </td></tr><tr><th>i) Fecha de expedición del informe de resultados.</th><td>'.$fechah.'</td></tr><tr><th>j) Signatario responsable de la evaluación</th><td>'.$tecnico.'</td></tr></table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');

$pdf->Bookmark('H. Metodología de para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style> .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} .tejus{text-align: justify;} .titulosa{font-size:20px;text-align:center;margin:0px} .sangria{text-indent: 45px;} .txt_td{background-color:#d9d9d9;} .tableb td{border:1px solid #808080;} .cet{text-align:center;} .checked{font-size:17px;font-family:dejavusans;} .pspaces{font-size:0.2px;} </style>
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
        <p class="tdtext"><u><i>Reconocimiento</i></u></p>
        <p class="tdtext sangria tejus">El recorrido de reconocimiento inicial deberá llevarse a cabo previo a la evaluación, con el fin de recabar la información técnico-administrativa que permita desarrollar una estrategia de medición que describa las condiciones térmicas extremas que se generen en el ambiente laboral, se deberá registrar la siguiente información.</p>

        <p class="tdtext tejus sangria">a) Identificar y registrar en un plano de vista de planta del centro de trabajo, todas las fuentes que generen condiciones térmicas extremas.</p>
        <p class="tdtext tejus sangria">b) Determinar si en el área donde se ubican las fuentes, el POE se localiza en un lugar cerrado o abierto y si existe ventilación natural o artificial.</p>
        <p class="tdtext tejus sangria">c) Elaborar una relación del POE, incluyendo áreas, puestos de trabajo, tiempos y frecuencia de la exposición.</p>
        <p class="tdtext tejus sangria">d) Describir las actividades y ciclos de trabajo que realiza el POE en cada puesto de trabajo.</p>

        <p class="titulosa"><b><u>TEMPERATURAS ABATIDAS</u></b></p>

        <p class="tdtext tejus">La evaluación consiste en medir y correlacionar la temperatura de bulbo seco y la velocidad del aire para calcular el índice de viento frío.</p>

        <p class="tdtext tejus sangria">Los instrumentos de medición se deben colocar a una altura de 1.40 + 0.10 metros y se deben tomar tres lecturas: al inicio, a la mitad y al final de cada ciclo de exposición;<br>
            Cuando se realicen evaluaciones a diferentes alturas, se deben registrar y fundamentar las causas que las originaron.</p>
        <p class="tdtext tejus sangria">Para cada evaluación se debe medir la temperatura axilar del POE en su puesto de trabajo, antes y después de cada ciclo de exposición.</p>
        <p class="tdtext tejus sangria">La evaluación del índice de viento frío se debe realizar lo más cerca posible del trabajador, sin que la presencia del evaluador interrumpa la actividad del POE.</p>';
        
        $pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->AddPage('P', 'A4');
        $pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
        $htmlg='<style> .txt_td{background-color:#d9d9d9;} .tableb td,.tableb th{border:1px solid #808080; text-align: center;} th{font-style: italic;} .titulosa{font-size:20px;text-align:center;margin:0px;} .ft13{font-size:13px;} .ft11{font-size:11.5px;} .div_diso{ float:left !important; } .pspaces{ font-size:0.5px; } .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} .tableb tr th{background-color:#d9d9d9;} </style>
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table cellpadding="5" align="center" class="tableb ft13" width="100%">';

        //trae los equipos utilizados
        $cont_equi=0;
        $getson = $this->ModeloNom->getEquiposNom15($idnom);
        foreach($getson as $s){
            $cont_equi++;
            $id_termo=$s->id_termo;
            $id_anemo=$s->id_anemo;
            if($cont_equi==1){
                $htmlg.='<tr><th></th><th>Termómetro líquido en vidrio</th><th>Anemómetro</th></tr><tr><th>Marca</th><td>'.$s->marca.'</td><td>'.$s->marca_anemo.'</td></tr><tr><th>Modelo</th><td>'.$s->modelo.'</td><td>'.$s->modeloanemo.'</td></tr><tr><th>Número de Serie</th><td>'.$s->equipo.'</td><td>'.$s->equipoanemo.'</td></tr><tr><th>No. de Informe de calibración</th><td class="ft11">'.$s->no_informe_calibracion.'</td><td class="ft11" >'.$s->no_informe_calibracionanemo.'</td></tr><tr><th>Fecha de calibración</th><td>'.$s->fecha_calibracion.'</td><td>'.$s->fecha_calibracionanemo.'</td></tr>';
            }
        }
        $htmlg.='</table><p></p>';

        $pdf->Bookmark('J. Descripción de las condiciones de operación ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
        $htmlg.='<p class="titulosa"><b>J. DESCRIPCIÓN DE LAS CONDICIONES DE OPERACIÓN</b></p>
                <p class="tdtext">De acuerdo a la información proporcionada por el personal responsable de la fuente fija, las condiciones de operación al momento de la evaluación son normales, no se presentan alteraciones que pudieran influir en los resultados obtenidos. Todo el personal y maquinaria se encuentra realizando sus actividades comunes.</p>';
        
        $pdf->Bookmark('K. Descripción del proceso de fabricación del centro de trabajo evaluado -----------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='<p class="titulosa"><b>K. DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</b></p>
        <p class="tdtext">Para visualizar la descripción del proceso, ver anexo IV.</p>';

        $pdf->Bookmark('L. Descripción de los puestos de trabajo ------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='
                <p class="titulosa"><b>L. DESCRIPCIÓN DE LOS PUESTOS DE TRABAJO </b></p>
                <table cellpadding="4" class="tableb">
                    <tr><th>Área</th><th>Puesto de Trabajo</th><th>Actividad(es)</th></tr>';

        foreach($ptos->result() as $p){
            $htmlg.='<tr><td>'.$p->area.'</td><td>'.$p->puesto.'</td><td>'.$p->desc_activ.'</td></tr>';  
        }
        $htmlg.='</table>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->AddPage('P', 'A4');

        $htmlg="";
        $htmlg='<style> .tableb td,.tableb th{border:1px solid #808080; text-align:center;} .titulosa{font-size:20px;text-align:center;margin:0px;} .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} .tableb tr th{background-color:#d9d9d9;} </style>';
        $pdf->Bookmark('M. Número de trabajadores expuestos por área y puesto de trabajo ------------------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES EXPUESTOS POR ÁREA Y PUESTO DE TRABAJO </b></p>
                <table cellpadding="4" class="tableb">
                    <tr><th><i>Área</i></th><th><i>Puesto de Trabajo</i></th><th><i>No. de Trabajadores</i></th></tr>';

        foreach($ptos->result() as $p){
            $htmlg.='<tr><td>'.$p->area.'</td><td>'.$p->puesto.'</td><td>1</td></tr>';  
        }
        $htmlg.='</table>';

        $pdf->Bookmark('O. Vigencia del informe ----------------------------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
        $htmlg.='<p class="titulosa"><b>M. VIGENCIA DEL INFORME </b></p><p class="tdtext">La vigencia de este informe de resultados será de dos años, mientras se mantengan las condiciones de trabajo que sirvieron de referencia para su emisión.</p>';

    $pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0,0,0)); 
$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO I</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</p>', 0, 0, 0, true, 'R', true);

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';

$pdf->AddPage('P', 'A4');
$dc=$datosrec->row();
$id_rec=0;
$desc_proceso="-----";
$plano_ubica="-----";
$zona_ubica="-----";
$observaciones="-----";

if($dc->desc_proceso!="")
    $desc_proceso=$dc->desc_proceso;
if($dc->plano_ubica!="")
    $plano_ubica=$dc->plano_ubica;
if($dc->observaciones!="")
    $observaciones=$dc->observaciones;
if($dc->id!="")
    $id_rec=$dc->id;

if(isset($firmaemple) && $firmaemple!="data:"){
    $firm_txt = FCPATH."uploads/firmas/".$firmaemple;
    $handle = @fopen($firm_txt, 'r');
    if($handle){
        $fh = fopen(FCPATH."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh); 
    }else{
        $linea="#";
    }  
    /*$fh = fopen(base_url()."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
    $linea = fgets($fh);
    fclose($fh);     */
}else{
    $linea='#';
}
if(isset($firmaresp) && $firmaresp!="" && $firmaresp!="data:"){
    $firm_txt2 = FCPATH."uploads/firmas/".$firmaresp;
    $handle2 = @fopen($firm_txt2, 'r');
    if($handle2){
        $fh2 = fopen(FCPATH."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
        $firmaresp_line = fgets($fh2);
        fclose($fh2); 
    }else{
        $firmaresp_line="";
    }
    /*$fh2 = fopen(base_url()."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
    $firmaresp_line = fgets($fh2);
    fclose($fh2);     */
}else{
    $firmaresp_line='';
}

$htmlg='<style> .borderbottom{border-bottom:1px solid black;} .backg{background-color:#d9d9d9;} .alg{ text-align:center; } .ft9{ font-size:9px; text-align:center; } .ft7{ font-size:7px; text-align:center; }    .fontbold{font-weight: bold;} .pspaces{font-size:0.2px;} .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:9px;} .tableb td{font-size:9px;valign: middle;vertical-align: middle;} .tableb tr th{background-color:#d9d9d9;} .checked{font-size:11px;font-family:dejavusans;}  </style>';
$htmlg.='
        <table border="1" cellpadding="5" align="center" class="ft9"><tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr><tr><td>RECONOCIMIENTO INICIAL TEMPERATURAS EXTREMAS</td><td>REG-TEC/03-01</td><td>01</td><td>ORIGINAL</td></tr></table>
        <p></p>
        <table border="0" cellpadding="5" align="center" class="ft9"><tr><td width="20%"><b>No. DE INFORME</b></td><td width="50%" class="borderbottom">'.$dc->num_informe_rec.'</td><td width="10%"><b>FECHA:</b></td><td width="20%" class="borderbottom">'.$dc->fecha.'</td></tr><tr><td width="20%"><b>RAZÓN SOCIAL</b></td><td width="80%" class="borderbottom">'.$dc->cliente.'</td></tr><tr><td width="60%" class="borderbottom">'.$dc->calle_num.'</td><td width="40%" class="borderbottom">'.$dc->colonia.'</td></tr><tr><td width="60%" ><b>CALLE Y NÚMERO</b></td><td width="40%"><b>COLONIA</b></td></tr><tr><td width="34%" class="borderbottom">'.$dc->poblacion.'</td><td width="33%" class="borderbottom">'.mb_strtoupper($estado_name,"UTF-8").'</td><td width="33%" class="borderbottom">'.$dc->cp.'</td></tr><tr><td width="34%" ><b>MUNICIPIO</b></td><td width="33%" ><b>ESTADO</b></td><td width="33%" ><b>CÓDIGO POSTAL</b></td></tr><tr ><td width="50%" class="borderbottom">'.$dc->rfc.'</td><td width="50%" class="borderbottom">'.$dc->giro.'</td></tr><tr><td width="50%"><b>RFC</b></td><td width="50%"><b>GIRO DE LA EMPRESA</b></td></tr><tr ><td width="50%" class="borderbottom">'.$dc->telefono.'</td><td width="50%" class="borderbottom">'.$dc->representa.'</td></tr><tr><td width="50%" ><b>NÚMERO TELEFÓNICO</b></td><td width="50%" ><b>REPRESENTANTE LEGAL</b></td></tr><tr><td width="100%" class="borderbottom">'.$dc->nom_cargo.'</td></tr><tr><td width="100%"><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td></tr> </table>    
        <p></p>';

        $htmlg.='<table border="1" class="tableb" width="100%">
                    <thead><tr><th rowspan="2" width="14%"><p class="pspaces"></p><p class="pspaces"></p>ÁREA</th><th colspan="2" width="12%">CONDICIÓN TÉRMICA</th><th colspan="2" width="12%">TIPO DE ÁREA</th><th colspan="2" width="14%">TIPO DE VENTILACIÓN</th><th rowspan="2" width="15%"><p class="pspaces"></p>GENERADOR(ES) DE LA CONDICIÓN TÉRMICA EXTREMA</th><th rowspan="2" width="15%"><p class="pspaces"></p>PUESTO DE TRABAJO EXPUESTO</th><th rowspan="2" width="8%">TIEMPO DE EXPOSICIÓN POR CICLO EN UNA HORA (min)</th><th rowspan="2" width="10%"><p class="pspaces"></p>FRECUENCIA (No. de cilcos en una hora)</th></tr><tr><th><p class="pspaces"></p>ABATIDA</th><th><p class="pspaces"></p>ELEVADA</th><th><p class="pspaces"></p>ABIERTA</th><th><p class="pspaces"></p>CERRADA</th><th><p class="pspaces"></p>ARTIFICIAL</th><th><p class="pspaces"></p>NATURAL</th></tr></thead>
                    <tbody>';

        $rec_ptos=$this->ModeloCatalogos->getselectwheren('reconocimiento_areas_nom15',array('id_reconocimiento'=>$id_rec,"estatus"=>1));
        foreach($rec_ptos->result() as $p){
            if($p->condicion_termica==1){
                $condicion_termica='<p class="checked">✔</p>';
            }else{
                $condicion_termica='';
            }
            if($p->condicion_termica==2){
                $condicion_termica2='<p class="checked">✔</p>';
            }else{
                $condicion_termica2='';
            }
            if($p->tipo_area==1){
                $tipo_area='<p class="checked">✔</p>';
            }else{
                $tipo_area='';
            }
            if($p->tipo_area==2){
                $tipo_area2='<p class="checked">✔</p>';
            }else{
                $tipo_area2='';
            }
            if($p->tipo_ventilacion==1){
                $tipo_ventilacion='<p class="checked">✔</p>';
            }else{
                $tipo_ventilacion='';
            }
            if($p->tipo_ventilacion==2){
                $tipo_ventilacion2='<p class="checked">✔</p>';
            }else{
                $tipo_ventilacion2='';
            }
            $htmlg.='<tr><td width="14%">'.$p->area.'</td><td width="6%">'.$condicion_termica.'</td><td width="6%">'.$condicion_termica2.'</td><td width="6%">'.$tipo_area.'</td><td width="6%">'.$tipo_area2.'</td><td width="7%">'.$tipo_ventilacion.'</td><td width="7%">'.$tipo_ventilacion2.'</td><td width="15%">'.$p->generadores_condicion.'</td><td width="15%">'.$p->puesto.'</td><td width="8%">'.$p->tiempo_expo.'</td><td width="10%">'.$p->frecuencia.'</td></tr>';        
        }
        $rowpuntorowr=$rec_ptos->num_rows();
        $limitefilas=35;
        if($rowpuntorowr<36){
            $limitefilas=35;
        }else if($rowpuntorowr>35 and $rowpuntorowr<56){
            $limitefilas=35;
        }
        for ($i = $rowpuntorowr; $i <= $limitefilas; $i++) {
            $htmlg.='<tr>
                <td width="14%">--</td><td width="6%">--</td><td width="6%">--</td><td width="6%">--</td><td width="6%">--</td><td width="7%">--</td><td width="7%">--</td><td width="15%">--</td><td width="15%">--</td><td width="8%">--</td><td width="10%">--</td>
            </tr>';
        }
        $htmlg.='</tbody>
            </table>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->AddPage('P', 'A4');

        $htmlg='<style> .borderbottom{border-bottom:1px solid black;} .backg{background-color:#d9d9d9;} .alg{ text-align:center; } .ft9{ font-size:9px; text-align:center; } .ft7{ font-size:7px; text-align:center; } .fontbold{font-weight: bold;} .pspaces{font-size:0.2px;} .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:9px;} .tableb td{font-size:9px;valign: middle;vertical-align: middle;} .tableb tr th{background-color:#d9d9d9;} </style>
            <table border="1" cellpadding="5" align="center" class="ft9"><tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr><tr><td>RECONOCIMIENTO INICIAL TEMPERATURAS EXTREMAS</td><td>REG-TEC/03-01</td><td>01</td><td>ORIGINAL</td></tr> </table>
            <p></p>
            <table border="1" cellpadding="5" class="tableb"><tr><th width="100%">DESCRIPCIÓN DEL PROCESO</th></tr><tr><td height="90px">'.$desc_proceso.'</td></tr><tr><th>PLANO DE UBICACIÓN DE ÁREAS, FUENTES GENERADORAS DE LA CONDICIÓN TÉRMICA EXTREMA Y UBICACIÓN DE LOS PUNTOS DE MEDICIÓN</th></tr><tr><td height="430px">'.$plano_ubica.'</td></tr><tr><th>OBSERVACIONES</th></tr><tr><td height="80px">'.$observaciones.'</td></tr><tr><th width="50%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></th><td width="50%"><p class="pspaces"></p><!--'.$tecnico.' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55">--> </td></tr><tr><th width="50%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></th><td width="50%"><p class="pspaces"></p><!-- Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55"> --></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

/* ***********************LEVANTAMIENTO******************************* */
$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';
$pdf->AddPage('P', 'A4');
$htmlg='<style> .borderbottom{border-bottom:1px solid black;} .backg{background-color:#d9d9d9;} .algc{text-align:center; } .ft9{font-size:9px;} .checked{font-size:9px;font-family:dejavusans;} .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:11px;font-size:9px;valign: middle;vertical-align: middle;} .tableb th{background-color:#d9d9d9;border:1px solid #808080;text-align:center;font-size:9px;} .pspaces{font-size:0.5px;} </style>
        <table border="1" cellpadding="5" align="center" class="ft9"><tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg"></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr><tr><td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td><td>REG-TEC/03-03</td><td>01</td><td>ORIGINAL</td></tr></table>
        <p></p>';

        //$htmlg.='<table class="tableb" cellpadding="5">';
            $id_termo=0; $id_termo_digi=0; $id_anemo=0; $cont_ptos=0;
            foreach ($datosnom->result() as $item) {

                $ptos_nom=$this->ModeloCatalogos->getselectwheren('nom15_punto',array('id_nom'=>$item->idnom,"activo"=>1));
                foreach ($ptos_nom->result() as $p) {
                    $cont_ptos++;
                    $id_termo=$p->id_termo;
                    $id_termo_digi=$p->id_termo_digi;
                    $id_anemo=$p->id_anemo;
                    $htmlg.='<table class="tableb" cellpadding="5"><tr><th>NO. DE INFORME</th><td colspan="3">'.$item->num_informe.'</td><th colspan="2">FECHA DE EMISIÓN</th><td colspan="2">'.$p->fecha.'</td></tr><tr><th>RAZÓN SOCIAL</th><td colspan="7">'.$item->razon_social.'</td></tr>';

                    $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_termo));
                    foreach ($resultequipo->result() as $iteme) {
                        $htmlg.='<tr><th width="5%">ID</th><td width="20%">'.$iteme->luxometro.'</td><th width="10%">MARCA</th><td width="15%">'.$iteme->marca.'</td><th width="10%">MODELO</th><td width="15%">'.$iteme->modelo.'</td><th width="10%">SERIE</th><td width="15%">'.$iteme->equipo.'</td></tr>';
                    }
                    $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_termo_digi));
                    foreach ($resultequipo->result() as $iteme) {
                        $htmlg.='<tr><th width="5%">ID</th><td width="20%">'.$iteme->luxometro.'</td><th width="10%">MARCA</th><td width="15%">'.$iteme->marca.'</td><th width="10%">MODELO</th><td width="15%">'.$iteme->modelo.'</td><th width="10%">SERIE</th><td width="15%">'.$iteme->equipo.'</td></tr>';
                    }
                    $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_anemo));
                    foreach ($resultequipo->result() as $iteme) {
                        $htmlg.='<tr><th width="5%">ID</th><td width="20%">'.$iteme->luxometro.'</td><th width="10%">MARCA</th><td width="15%">'.$iteme->marca.'</td><th width="10%">MODELO</th><td width="15%">'.$iteme->modelo.'</td><th width="10%">SERIE</th><td width="15%">'.$iteme->equipo.'</td></tr>';
                    }

                    if($p->tipo_evalua==1){
                        $tipo_evalua='<p class="checked">(✔) POE</p>';
                    }else{
                        $tipo_evalua='( ) POE';
                    }
                    if($p->tipo_evalua==2){
                        $tipo_evalua2='<p class="checked">(✔) GEH </p>';
                    }else{
                        $tipo_evalua2='( ) GEH';
                    }

                    if($p->casco==1){
                        $casco='<p class="checked">Casco: (✔)</p>';
                    }else{
                        $casco='Casco: ( )';
                    }
                    if($p->tapones==1){
                        $tapones='<p class="checked">Tapones acústicos: (✔)</p>';
                    }else{
                        $tapones='Tapones acústicos: ( )';
                    }
                    if($p->botas==1){
                        $botas='<p class="checked">Botas seguridad: (✔)</p>';
                    }else{
                        $botas='Botas seguridad: ( )';
                    }
                    if($p->lentes==1){
                        $lentes='<p class="checked">Lentes: (✔)</p>';
                    }else{
                        $lentes='Lentes: ( )';
                    }
                    if($p->guantes==1){
                        $guantes='<p class="checked">Guantes: (✔)</p>';
                    }else{
                        $guantes='Guantes: ( )';
                    }
                    if($p->peto==1){
                        $peto='<p class="checked">Peto: (✔)</p>';
                    }else{
                        $peto='Peto: ( )';
                    }
                    if($p->respirador==1){
                        $respirador='<p class="checked">Respirador: (✔)</p>';
                    }else{
                        $respirador='Respirador: ( )';
                    }
                    $htmlg.='</table>
                    <table width="100%" class="tableb" cellpadding="4"><tr><th width="10%">ÁREA</th><td width="60%">'.$p->area.'</td><th width="10%"> No. PUNTO</th><td width="20%">'.$p->num_punto.'</td></tr><tr><th width="20%">UBICACIÓN</th><td colspan="3" width="80%">'.$p->ubicacion.'</td></tr><tr><th width="20%">FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</th><td colspan="3" width="80%">'.$p->fte_generadora.'</td></tr><tr><th width="20%">CONTROLES TÉCNICOS</th><td colspan="3" width="80%">'.$p->controles_tec.'</td></tr><tr><th width="20%">CONTROLES ADMINISTRATIVOS</th><td colspan="3" width="80%">'.$p->controles_adm.'</td></tr><tr><th colspan="2" width="20%">TIPO DE EVALUACIÓN</th><th colspan="3" width="80%">NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</th></tr><tr><td width="15%">'.$tipo_evalua.'</td><td width="15%">'.$tipo_evalua2.'</td><td colspan="3" width="70%">'.$p->nom_trabaja.'</td></tr>
                    </table>
                    <table cellpadding="4" class="tableb" width="100%"><tr><td width="20%">EPP Utilizado:</td><td width="20%">'.$casco.'</td><td width="20%">'.$tapones.'</td><td width="20%">'.$botas.'</td><td width="20%">'.$lentes.'</td></tr><tr><td width="20%">'.$guantes.'</td><td width="20%">'.$peto.'</td><td width="20%">'.$respirador.'</td><td colspan="2" width="40%">OTRO: '.$p->otro.'</td></tr></table>
                    <table cellpadding="4" class="tableb" width="100%"><tr><th width="20%">Puesto de trabajo</th><td width="35%">'.$p->puesto.'</td><th width="30%">Tiempo de exposición por ciclo en una h (min)</th><td width="15%">'.$p->tiempo_expo.'</td></tr><tr><th width="25%">Número de ciclos por h</th><td width="25%">'.$p->num_ciclos.'</td><th width="25%">Ciclo evaluado</th><td width="25%">'.$p->cliclo.'</td></tr></table> 
                    <table cellpadding="4" class="tableb" width="100%"><tr><th width="40%">Descripción de actividades del personal</th><td width="60%">'.$p->desc_activ.'</td></tr><tr><th width="40%">Observaciones</th><td width="60%">'.$p->observaciones.'</td></tr></table> 
                    <table class="tableb">
                        <tr><td></td></tr>
                    </table>';
                    $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15',array('id'=>$p->id,"activo"=>1));
                    foreach($med->result() as $m){
                        $htmlg.='<table cellpadding="4" class="tableb" width="100%"><tr><th width="10%">Ciclo No.</th><td width="10%">'.$m->ciclo_med.'</td><th width="30%">Temperatura auxiliar inicial (°C)</th><td width="10%">'.$m->temp_ini.'</td><th width="30%"><label>Tempertura auxiliar final (°C)</label></th><td width="10%">'.$m->temp_fin.'</td></tr><tr><th rowspan="2">Evaluación</th><th rowspan="2">Altura (m)</th><th rowspan="2">Hora</th><th colspan="3">Medición</th></tr><tr><th>Tbs °C</th><th colspan="2">Velocidad del viento (km/h)</th></tr><tr><th>Inicial</th><td>'.$m->altura_ini.'</td><td>'.$m->hora_ini.'</td><td>'.$m->tbs_ini.'</td><td colspan="2">'.$m->velocidad_ini.'</td></tr><tr><th>Mitad</th><td>'.$m->altura_media.'</td><td>'.$m->hora_media.'</td><td>'.$m->tbs_media.'</td><td colspan="2">'.$m->velocidad_media.'</td></tr><tr><th>Final</th><td>'.$m->altura_fin.'</td><td>'.$m->hora_fin.'</td><td>'.$m->tbs_fin.'</td><td colspan="2">'.$m->velocidad_fin.'</td></tr>
                        </table> 
                       <table cellpadding="4" class="tableb"><tr><td><span style="font-size: 9px;">Tbs: Temperatura bulbo seco</span></td></tr></table>
                        <table class="tableb"><tr><td></td></tr></table>
                        <table class="tableb"><tr><th width="20%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></th><td width="30%"><p class="pspaces"></p><!-- '.$tecnico.' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55"> --></td><th width="20%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></th><td width="30%"><p class="pspaces"></p><!-- Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55">--> </td></tr></table>';
                    } //foreach de mediciones

                    $pdf->writeHTML($htmlg, true, false, true, false, '');
                    
                    if($cont_ptos<$ptos_nom->num_rows()){
                        $pdf->AddPage('P', 'A4');
                        $htmlg='<style> .borderbottom{border-bottom:1px solid black;} .backg{background-color:#d9d9d9;} .algc{text-align:center; } .ft9{font-size:9px;} .checked{font-size:10px;font-family:dejavusans;} .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:11px; font-size:9px; valign: middle; vertical-align: middle; } .tableb th{ background-color:#d9d9d9; border:1px solid #808080; text-align:center; font-size:9px; } .pspaces{ font-size:0.5px;} </style>
                        <table border="1" cellpadding="5" align="center" class="ft9"><tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr><tr><td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td><td>REG-TEC/03-03</td><td>01</td><td>ORIGINAL</td></tr></table>
                        <p></p>';
                    }
                }//foreach de nom15_punto

            }
            
    //$htmlg.='</table>';

//$pdf->writeHTML($htmlg, true, false, true, false, '');

/************************** *******************************/

$pdf->AddPage('P', 'A4');
$htmlg="";
$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27,15);
$pdf->Bookmark('II. Planos ----------------------------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO II</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">PLANOS</p>', 0, 0, 0, true, 'R', true);

$htmlg=''; $htmlg2=""; $htmlg3="";
$cont_plan=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==1){
        $pdf->AddPage('P', 'A4');
        $cont_plan++;
        /*$htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'" ></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if($cont_plan==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan2=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==2){
        $pdf->AddPage('P', 'A4');
        $cont_plan2++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if($cont_plan2==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

/* ********************************************/
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo --------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO III</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">SECUENCIA DE CÁLCULO</p>', 0, 0, 0, true, 'R', true);
/************************************************************** */
$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);
    $htmlg0='';
        foreach ($ptos->result() as $p) { 
            $pdf->AddPage('P', 'A4');  
            $htmlg='';
            
            $htmlg.='<style> .ft9{ font-size:9px; text-align:center; } .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:11px;font-size:9px;valign: middle;vertical-align: middle;} .tableb th{background-color:#d9d9d9;border:1px solid #808080;text-align:center;font-size:10px;} .tablewa{width:auto;} .pspaces{font-size:0.2px;} </style>';
            $htmlg.='<table border="1" cellpadding="5" align="center" class="ft9"><tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr><tr><td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td><td>REG-TEC/03-03</td><td>01</td><td>ORIGINAL</td></tr></table><table><tr><td></td></tr></table>';
           
            $tipo_evaluai='';
            if($p->tipo_evalua==1){$tipo_evaluai="POE";}
            if($p->tipo_evalua==2){$tipo_evaluai="GEH";}

            $htmlg.='<table cellpadding="4" class="tableb"><tr><th colspan="4">No. de Informe</th><td colspan="4">'.$folio.'</td><th>Fecha</th><td>'.$p->fecha.'</td></tr><tr><th colspan="3">Razón Social</th><td colspan="7">'.$razon_social.'</td></tr>
                <tr><th width="15%">Número de Punto</th><td width="5%">'.$p->num_punto.'</td><th width="10%" colspan="2">Área</th><td colspan="4" width="30%">'.$p->area.'</td><th width="10%">Puesto</th><td width="30%">'.$p->puesto.'</td>
                </tr><tr><th colspan="4">Identificación del Punto</th><td colspan="6">'.$p->ubicacion.'</td></tr><tr><th colspan="4">Fuentes Generadoras de la Condición Términca</th><td colspan="6">'.$p->fte_generadora.'</td></tr><tr><th colspan="4">Nombre del POE O GEH</th><td colspan="6">'.$p->nom_trabaja.'</td></tr><tr><th colspan="5">Tipo de Evaluación</th><td>'.$tipo_evaluai.'</td><th colspan="3">Tiempo de exposición por ciclo (min)</th><td>'.$p->tiempo_expo.'</td></tr><tr><th colspan="5">Número de Ciclos Por Hora</th><td>'.$p->num_ciclos.'</td><th colspan="3">Ciclo Evaluado</th><td>'.$p->cliclo.'</td></tr>';
                    $med=$this->ModeloCatalogos->getselectwheren('medicion_temp_nom15',array('id'=>$p->id,"activo"=>1)); 
                    foreach($med->result() as $m) {
                    $htmlg.='<tr><th colspan="5">Temperatura axilar</th><th colspan="2">Inicial (ºC)</th><td>'.$m->temp_ini.'</td><th>Final (ºC)</th><td>'.$m->temp_fin.'</td></tr>';
                    }
                    $htmlg.='</table>';
                    foreach($med->result() as $m) {
                                $i_v=$m->velocidad_ini;
                                $i_t=$m->tbs_ini;
                                $i_c_v=round($m->velocidad_ini_c,1);
                                $i_c_t=round($m->tbs_ini_c,1);

                                $m_v=$m->velocidad_media;
                                $m_t=$m->tbs_media;
                                $m_c_v=round($m->velocidad_media_c,1);
                                $m_c_t=round($m->tbs_media_c,1);
                                $f_v =$m->velocidad_fin; 
                                $f_t =$m->tbs_fin; 
                                $f_c_v=round($m->velocidad_fin_c,1);
                                $f_c_t=round($m->tbs_fin_c,1); 


                                $i_vt=$m->lvf_ini_c;
                                $m_vt=$m->lvf_media_c;
                                $f_vt=$m->lvf_fin_c;
                                $vt_promedio=($i_vt+$m_vt+$f_vt)/3;
                        $htmlg.='<table boder="1"><tr><td colspan="3"></td></tr>
                                    <tr>
                                    <td width="15%"></td>
                                    <td width="70%">
                                        <table class="tableb">
                                            <tr>
                                                <th rowspan="4"><p class="pspaces"></p><p class="pspaces"></p>Inicial</th>
                                                <th>Velocidad del viento (km/s)</th>
                                                <th><p class="pspaces"></p>Temperatura bs (°C)</th>
                                                <th><p class="pspaces"></p>l<sub>vf</sub></th>
                                            </tr>
                                            <tr>
                                                <td>'.$i_v.'</td>
                                                <td>'.$i_t.'</td>
                                                <td rowspan="3"><p class="pspaces"></p>'.$i_vt.'</td>
                                            </tr>
                                            <tr>
                                                <th>Corregida</th>
                                                <th>Corregida</th>
                                            </tr>
                                            <tr>
                                                <td>'.$i_c_v.'</td>
                                                <td>'.$i_c_t.'</td>
                                            </tr>
                                            <tr>
                                                <th rowspan="4"><p class="pspaces"></p><p class="pspaces"></p>Mitad</th>
                                                <th>Velocidad del viento (km/s)</th>
                                                <th><p class="pspaces"></p>Temperatura bs (°C)</th>
                                                <th><p class="pspaces"></p>l<sub>vf</sub></th>
                                            </tr>
                                            <tr>
                                                <td>'.$m_v.'</td>
                                                <td>'.$m_t.'</td>
                                                <td rowspan="3"><p class="pspaces"></p>'.$m_vt.'</td>
                                            </tr>
                                            <tr>
                                                <th>Corregida</th>
                                                <th>Corregida</th>
                                            </tr>
                                            <tr>
                                                <td>'.$m_c_v.'</td>
                                                <td>'.$m_c_t.'</td>
                                            </tr>
                                            <tr>
                                                <th rowspan="4"><p class="pspaces"></p><p class="pspaces"></p>Final</th>
                                                <th>Velocidad del viento (km/s)</th>
                                                <th><p class="pspaces"></p>Temperatura bs (°C)</th>
                                                <th><p class="pspaces"></p>l<sub>vf</sub></th>
                                            </tr>
                                            <tr>
                                                <td>'.$f_v.'</td>
                                                <td>'.$f_t.'</td>
                                                <td rowspan="3"><p class="pspaces"></p>'.$f_vt.'</td>
                                            </tr>
                                            <tr>
                                                <th>Corregida</th>
                                                <th>Corregida</th>
                                            </tr>
                                            <tr>
                                                <td>'.$f_c_v.'</td>
                                                <td>'.$f_c_t.'</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="15%"></td>
                                    </tr>
                                    <tr><td colspan="3"></td></tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <table class="tableb">
                                                <tr>
                                                    <th>l<sub>vf</sub> Promedio</th>
                                                </tr>
                                                <tr>
                                                    <td>'.$vt_promedio.'</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr><td colspan="3" align="center"><img src="'.base_url().'public/img/vf_promedio.png" style="width: 300px;"></td></tr>
                                    <tr><td colspan="3"></td></tr>
                                    <tr><td colspan="3" align="center"><img src="'.base_url().'public/img/tabla_tempreraturas.JPG"></td></tr>
                                </table>';
                                 
                    }
                     
            $pdf->writeHTML($htmlg, true, false, true, false, '');
        }
       
// ---======================================================================================---

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Descripción del proceso de fabricación del centro de trabajo evaluado ---------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO IV</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</p>', 0, 0, 0, true, 'R', true);

$htmlg='';
$cont_desc_pro=0;
foreach ($datosnomiv->result() as $item) {
    $cont_desc_pro++;
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgIntro/'.$item->url_img.'"></td></tr></table>';
}

if($cont_desc_pro==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table border="0" >
            <tr><td align="center" height="200px"></td></tr>
            <tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr>
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}else{
    $pdf->AddPage('P', 'A4');
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Informes de calibración del equipo de medición ----------------------------------------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO V</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">INFORMES DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</p>', 0, 0, 0, true, 'R', true);

$datosequipoc=$this->ModeloNom->getCertEquipos015($id_termo,$id_termo_digi,$id_anemo);
foreach ($datosequipoc as $item) {
    $htmlg='';
    $pdf->AddPage('P', 'A4');
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgEquipos/'.$item->certificado.'"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt='Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt=$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
}
 
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VI. Documento de acreditación ------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO VI</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">DOCUMENTO DE ACREDITACIÓN</p>', 0, 0, 0, true, 'R', true);

if($datosacre->num_rows()>0){
    $htmlg='';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4'); 
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);

        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
    }   
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('VII. Documento de aprobación -------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));

$pdf->writeHTMLCell('', '', 0, 105, '<p style="font-family:Arial; font-size:25px; color: #eb7a26;text-align:right">ANEXO VII</p>', 0, 0, 0, true, 'R', true);
$pdf->writeHTMLCell('', '', 0, 130, '<p style="font-family:Arial; font-size:35px; color: #003958;text-align:right">DOCUMENTO DE APROBACIÓN</p>', 0, 0, 0, true, 'R', true);

$txt_fin=""; $cont_acre=0;
if($datosacre2->num_rows()>0){
    $htmlg='';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2=0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima
        $cont2++;
        $pdf->SetMargins(20,25,20); 
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->AddPage('P', 'A4');
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);
        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        if($cont_acre==$cont2){
            /*$txt_fin.='<table>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p>
                    </td>
                </tr>
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true);
            //$pdf->writeHTML($txt_fin, true, false, true, false, '');
            /*
            $pdf->SetXY(60, 211);
            $pdf->SetTextColor(0,57,88);
            $pdf->SetFont('dejavusans', '', 10);
            $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas';
            $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
            $pdf->Cell(100, 50, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

            $pdf->SetXY(55, 215);
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('dejavusans', '', 10);
            $pdf->Cell(100, 50, $txt_fin2, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
            */
        }
    }
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    /*$pdf->SetXY(50, 150);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 23);
    $txt='Documento Informativo '.$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/

    $pdf->SetXY(60, 251);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas.';
    $pdf->Cell(100, 10, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
    
    $pdf->SetXY(55, 255);
    $pdf->SetTextColor(255,0,0);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
    $pdf->Cell(100, 10, $txt_fin2,  0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
}


// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));

$bookmark_templates[0]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();



$pdf->Output('EntregableNom15_abatidas.pdf', 'I');

?>