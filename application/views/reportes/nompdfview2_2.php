<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');
    /*$margin_left=15;
    $margin_top=27;
    $margin_right=15;*/
    //$folio='AHISA/2022EP-00AL';
    //<tr nobr="true">
    
    //==== datos taltantes del equipo en su catalogo
        $equipo_marca='EVERFINE';
        $equipo_modelo='Z-10';
        $equipo_num_serie='M154879CM1371120';
        $equipo_no_informe_calibracion='SIMH-OPTICA/0308-2021';
        $equipo_fecha_calibracion='20/09/2021';
    //=======================

    $norma_name='NOM-025-STPS-2008';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);
    $GLOBALS['id_nom_ctr']=$id_nom_ctr;
    $GLOBALS['id_reconocimientoinicial']=$id_reconocimientoinicial;

    foreach ($datosnom->result() as $item) {
        $fecha = $item->fecha;
        //cambiar esta fecha por la(s) fechas de los puntos levantados - nom25_detalle

        $datosequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$item->equipo));
        //$folio='AHISA/'.date("Y", strtotime($item->fecha)).'EP-0'.$item->idnom.'AL';
        $folio=$item->num_informe;
        $idequipo=0;
        $equipo=0;
        foreach ($datosequipo->result() as $itemeq) {
            $idequipo=$itemeq->id;
            $equipo_num_serie=$itemeq->equipo;
            $equipo_marca=$itemeq->marca;
            
            $equipo_modelo=$itemeq->modelo;
            $equipo_no_informe_calibracion=$itemeq->no_informe_calibracion;
            $equipo_fecha_calibracion=$itemeq->fecha_calibracion;
        }
    }

    $intercept=$this->ModeloCatalogos->interseccion_pendiente($idequipo,0);//E29
    $pendiente=$this->ModeloCatalogos->interseccion_pendiente($idequipo,1);//E30

    $datosequipoc=$this->ModeloCatalogos->getselectwheren('equipo_certificado',array('id_equipo'=>$idequipo,'estatus'=>1));
    $fechasarray=array();
    $fecha_mes=0;
    foreach ($datosnomdetalle as $itemd) {
        $fechasarray[]=$itemd->fecha;
           
    }
    $fechasgrupo='';
    $fechasarray = array_unique($fechasarray);
    $fechasnew=array();
    foreach ($fechasarray as $item) {
        //echo $item;
        //$date = new Datetime($item);
        //$fechag = strftime("%d de %B del año %Y", $date->getTimestamp());

        //$fechasgrupo.=$fechag.', ';
        $year=date('Y',strtotime($item));
        $mes=date('n',strtotime($item));
        $day=date('d',strtotime($item));
        
        if($fecha_mes!=$mes){
            $fecha_mes=intval($mes);
            $fechasnew[$mes]=array('year'=>$year,'mes'=>$mes,'dia'=>$day);
        }else{
            $fechasnew[$mes]=array('year'=>$year,'mes'=>$mes,'dia'=>$fechasnew[$mes]['dia'].', '.$day);
        }
    }
    foreach($fechasnew as $item){
        $fechasgrupo.=$item['dia'].' de '.$this->ModeloCatalogos->mes($item['mes']).' del año '.$item['year'].',';
    }
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());
    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());

    foreach ($datosoc->result() as $item) {
        //$empresa =$item->empresa;
        //$direccion = $item->calle.''.$item->no_ext.', '.$item->no_int.', '.$item->colonia.', '.$item->estado.', CP.'.$item->cp_fiscal.'.';
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $firmaresp=$item->firmaresp;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $direccion = $item->calle_num.', <br> '.$item->colonia.', <br>'.$item->poblacion.', '.ucfirst(strtolower($item->estado)).',<br> C.P. '.$item->cp.'';
        $direccion2 = $item->calle_num.', '.$item->colonia.', '.$item->poblacion.', '.ucfirst(strtolower($item->estado)).',<br> C.P. '.$item->cp.'';
        //$direccion = $item->calle_num.''.$item->no_ext.', '.$item->no_int.', <br>'.$item->colonia.' , <br>'.$item->poblacion.','.$item->estado.',<br> CP.'.$item->cp_fiscal.'.';
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }

    $GLOBALS["tot_pgs"] = 0;

//=======================================================================================
class MYPDF extends TCPDF {
    protected $last_page = false;
    protected $pagef2=1;

    // Will be called at the end
    public function Close()
    {
        $this->last_page = true;
        $this->pagef=$this->PageNo();
        log_message('error',"pagef ".$this->pagef);
        $this->pagef2 = $this->pagef-1;
        log_message('error',"pagef2 ".$this->pagef2);
        parent::Close();
    }
    /*public function Start()
    {
        //if($this->last_page){
            $this->pagef=$this->PageNo();
            log_message('error',"pagef ".$this->pagef);
            $this->pagef2 = $this->pagef-1;
            log_message('error',"pagef2 ".$this->pagef2);
        //}
    }*/

    //Page header
    public function Header() {
        //if($this->PageNo()>1){
            $logos = base_url().'public/img/logo.jpg';
            $html = '<table width="100%"><tr><td style="font-size:15px"><img src="'.$logos.'" width="150px"></td><td></td><td style="" align="right"></td></tr></table>';
            $this->writeHTML($html, true, false, true, false, '');
        //}
    }
    // Page footer
    public function Footer() {
        $folio=$GLOBALS['folio'];
        $pf = $this->getAliasNbPages();
        $cc = $this->getAliasNumPage();

        if(!$this->last_page){
        //if ($this->PageNo()>1){
            $html = '
            <style type="text/css">
                .cmac{font-size:8px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
                .cma{font-size:14px; color:rgb(231,99,0);}
                .cmafolio{font-size:8px; background-color: rgb(231,99,0); font-weight: bold;}
                .footerpage{font-size:8px;font-style: italic;}
            </style> 
          <table width="100%" cellpadding="2" border="0"><tr><td align="center" colspan="2" style="font-size:9px;font-style: italic;">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr><tr><td align="center" class="cmac" width="82%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx</td><td align="center" style="margin-top:150px" class="cmafolio" width="18%"><span class="cma">•</span>'.$folio.'</td></tr><tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$folio.' </td></tr></table>';
            //log_message('error',$html);
            //$this->writeHTML($html, true, false, true, false, '');
            $this->writeHTMLCell(188, '', 12, 270, $html, 0, 0, 0, true, 'C', true);
            $GLOBALS["tot_pgs"] = $this->getAliasNbPages();
        }
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('Entregable');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

//$pdf->Start();
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,24,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setPrintFooter(true);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 28);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page


$pdf->AddPage('P', 'A4');
// Get the current page number
//$pageNo = $pdf->PageNo(); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style type="text/css">
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:15px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:14px;font-style:italic;}
    .sangria{text-indent: 45px;}
</style>';
$htmlg.='
        <table border="0">
            <tr><td colspan="2" align="right" style=" font-size:20px; font-weight: bold;">'.$empresa.'</td></tr>
            <tr><td colspan="2"></td></tr>
            <tr><td width="50%" ></td><td width="50%" align="right" style=" font-size:14px; font-weight: bold;" >'.$direccion.'</td></tr>
        </table>
        <table border="0">
            <tr><td></td></tr>            
            <tr><td style=" font-size:16px; font-weight: bold;"><b>'.$nom_cargo.'</b></td></tr>
            <!--<tr>
                <td style=" font-size:13px; font-weight: bold;"><b>TÉCNICO EN SEGURIDAD INDUSTRIAL.</b></td>
            </tr>-->
            <tr><td style=" font-size:13px; font-weight: bold;"><b>'.$cargo_cargo.'</b></td></tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes a los niveles de iluminación y factor de reflexión; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fechasgrupo.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="160px" colspan="2"></td></tr>
            <tr><td>Ing. Christian Uriel Fabian Romero</td><td>Ing. Tanya Alejandra Valle Tello</td></tr>
            <tr><td>Aprobó - Gerente General</td><td>Revisó – Procesamiento de Información</td></tr>
            <tr><td colspan="2" height="160px"></td></tr>
            <tr><td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
// First page of the document
//$toPage = 1;
//$pdf->movePage($pageNo, $toPage);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

    $img_1="";
    /*if($GLOBALS['tablageneral1']!=""){
        $img_1 = '<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $GLOBALS['tablageneral1']).'">';
    }*/
    $htmlg='<style type="text/css">
                td{font-family:calibri;}
                .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
                .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
                .fosi9{font-size:9px;}
                .textcenter{text-align:center;}
                .textcenter2{text-align:center; font-size:11px;}
                .sangria{text-indent: 45px;}
                b{font-family:Arialb;}
                .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:8.4px;valign: middle;vertical-align: middle;}
                .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:9px;}
                .pspaces{ font-size: 0.1px;}
            </style>
                <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
                
                <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>'.$empresa.'</b>, el día <b>'.$fechasgrupo.'.</b></p>
                <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y validos exclusivamente para las siguientes áreas e identificación del presente informe:</p>
                <p class="textcenter2"><i>• Nivel de Iluminación</i></p>';

            $htmlg.='<table class="tableb" width="100%" align="center" cellpadding="3">
                <thead><tr><th width="8%">No.<br>de<br>Punto</th><th width="37%"><p class="pspaces"></p>Identificación del punto de medición (área / identificación)</th><th width="10%">Horario <br>de <br>medición</th><th width="10%">Nivel<br>de<br>iluminación</th><th width="10%"><p class="pspaces"></p>U exp (lx)</th><!--<th width="10%">Nivel de iluminación + U exp (lx)</th>--><th width="5%"><p class="pspaces"></p>NMI (lx)</th><th width="20%"><p class="pspaces"></p>Conclusión</th></tr></thead>
                <tbody>';

    $tablas_sec="";
    $arrayniveliluminacion=array();
    $rowpunto=1;
    $rowpuntoset=1000;
    //$ndd=$this->ModeloCatalogos->getselectwheren('nom25_detalle',array('idnom'=>$id_nom_ctr,'activo'=>1));

    foreach ($datosnom->result() as $item) { 
            $datosequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$item->equipo));
            $idequipo=0;
            $equipo=0;
            foreach ($datosequipo->result() as $itemeq) {
                $idequipo=$itemeq->id;
                $equipo=$itemeq->luxometro;
            }
            $intercept=$this->ModeloCatalogos->interseccion_pendiente($idequipo,0);//E29
            $pendiente=$this->ModeloCatalogos->interseccion_pendiente($idequipo,1);//E30
    }

    $fecha_comp="";
    $ndd=$this->ModeloNom->getPuntosDetalle25(array('idnom'=>$id_nom_ctr,'activo'=>1));
    foreach ($ndd->result() as $item) { 
        $h1promedio=($item->h1_medicion_a+$item->h1_medicion_b+$item->h1_mdicion_c)/3;
        $h2promedio=($item->h2_medicion_a+$item->h2_medicion_b+$item->h2_medicion_c)/3;
        $h3promedio=($item->h3_medicion_a+$item->h3_medicion_b+$item->h3_medicion_c)/3;

        $h1_pcorr=($pendiente*$h1promedio)+$intercept;
        if($h1promedio==0){
            $h1_pcorr=0;
        }
        $h2_pcorr=($pendiente*$h2promedio)+$intercept;
        if($h2promedio==0){
            $h2_pcorr=0;
        }
        $h3_pcorr=($pendiente*$h3promedio)+$intercept;
        if($h3promedio==0){
            $h3_pcorr=0;
        }

        $h1_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h1_medicion_a,$item->h1_medicion_b,$item->h1_mdicion_c));
        $h2_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h2_medicion_a,$item->h2_medicion_b,$item->h2_medicion_c));
        $h3_desv_estandar=1/sqrt(3)*$this->ModeloCatalogos->desvest(array($item->h3_medicion_a,$item->h3_medicion_b,$item->h3_medicion_c));

        $h1_tipo_distribucion='A';
        $h2_tipo_distribucion='A';
        $h3_tipo_distribucion='A';

        $h1_coef_sensibilidad=1;
        $h2_coef_sensibilidad=1;
        $h3_coef_sensibilidad=1;

        $h1_contribuccion=$h1_desv_estandar*$h1_coef_sensibilidad;
        $h2_contribuccion=$h2_desv_estandar*$h2_coef_sensibilidad;
        $h3_contribuccion=$h3_desv_estandar*$h3_coef_sensibilidad;

        $h1_uy2=pow($h1_contribuccion, 2);
        $h2_uy2=pow($h2_contribuccion, 2);
        $h3_uy2=pow($h3_contribuccion, 2);

            $escala_1_a='';
            $escala_1_b='';
            $escala_1_c='';
            $escala_2_a='';
            $escala_2_b='';
            $escala_2_c='';
            $escala_3_a='';
            $escala_3_b='';
            $escala_3_c='';
            $distribucion1='B, rectangular';
            $distribucion2='B, rectangular';
            $distribucion3='B, rectangular';

        $resultre=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$id_nom_ctr));
        if($resultre->num_rows()>0){
            foreach ($resultre->result() as $itemre) {
                if($itemre->todos_ptos=="1"){
                    if($itemre->escala1==1){
                        $escala_1_a='X';
                    }elseif($itemre->escala1==2){
                        $escala_2_a='X';
                    }else{
                        $escala_3_a='X';
                    }
                    
                    if($itemre->escala2==1){
                        $escala_1_b='X';
                    }elseif($itemre->escala2==2){
                        $escala_2_b='X';
                    }else{
                        $escala_3_b='X';
                    }

                    if($itemre->escala3==1){
                        $escala_1_c='X';
                    }elseif($itemre->escala3==2){
                        $escala_2_c='X';
                    }else{
                        $escala_3_c='X';
                    }
                    $distribucion1=$itemre->distribucion1;
                    $distribucion2=$itemre->distribucion2;
                    $distribucion3=$itemre->distribucion3;
                    $h1_coef_sensibilidad=$itemre->sencibilidad1;
                    $h2_coef_sensibilidad=$itemre->sencibilidad2;
                    $h3_coef_sensibilidad=$itemre->sencibilidad3;
                }else{
                    $resultre=$this->ModeloCatalogos->getselectwheren('nom25_resolucion_equipo',array('idnom'=>$idnom,"id_punto"=>$item->id));
                    if($resultre->num_rows()>0){
                        foreach ($resultre->result() as $itemre) {
                                $escala_1_a='';
                                $escala_1_b='';
                                $escala_1_c='';
                                $escala_2_a='';
                                $escala_2_b='';
                                $escala_2_c='';
                                $escala_3_a='';
                                $escala_3_b='';
                                $escala_3_c='';

                            if($itemre->escala1==1){
                                $escala_1_a='X';
                            }elseif($itemre->escala1==2){
                                $escala_2_a='X';
                            }else{
                                $escala_3_a='X';
                            }
                            
                            if($itemre->escala2==1){
                                $escala_1_b='X';
                            }elseif($itemre->escala2==2){
                                $escala_2_b='X';
                            }else{
                                $escala_3_b='X';
                            }

                            if($itemre->escala3==1){
                                $escala_1_c='X';
                            }elseif($itemre->escala3==2){
                                $escala_2_c='X';
                            }else{
                                $escala_3_c='X';
                            }
                            $distribucion1=$itemre->distribucion1;
                            $distribucion2=$itemre->distribucion2;
                            $distribucion3=$itemre->distribucion3;
                            $h1_coef_sensibilidad=$itemre->sencibilidad1;
                            $h2_coef_sensibilidad=$itemre->sencibilidad2;
                            $h3_coef_sensibilidad=$itemre->sencibilidad3;  
                        }
                    }
                }
            }
        }else{
            $escala_1_a='';
            $escala_1_b='';
            $escala_1_c='';
            $escala_2_a='X';
            $escala_2_b='X';
            $escala_2_c='X';
            $escala_3_a='';
            $escala_3_b='';
            $escala_3_c='';
        }
        if($escala_1_a=='X'){
            $h1_resolicion=0.01;
        }elseif ($escala_2_a=='X') {
            $h1_resolicion=0.1;
        }else{
            $h1_resolicion=1;
        }
        if($escala_1_b=='X'){
            $h2_resolicion=0.01;
        }elseif ($escala_2_c=='X') {
            $h2_resolicion=0.1;
        }else{
            $h2_resolicion=1;
        }
        if($escala_1_c=='X'){
            $h3_resolicion=0.01;
        }elseif ($escala_2_c=='X') {
            $h3_resolicion=0.1;
        }else{
            $h3_resolicion=1;
        }

        $h1_u_estandar=$h1_resolicion/sqrt(12);
        $h2_u_estandar=$h2_resolicion/sqrt(12);
        $h3_u_estandar=$h3_resolicion/sqrt(12);

        $h1_contribuccion_rq=$h1_u_estandar*$h1_coef_sensibilidad;
        $h2_contribuccion_rq=$h2_u_estandar*$h2_coef_sensibilidad;
        $h3_contribuccion_rq=$h3_u_estandar*$h3_coef_sensibilidad;

        $h1_uy2_rq=pow($h1_contribuccion_rq, 2);
        $h2_uy2_rq=pow($h2_contribuccion_rq, 2);
        $h3_uy2_rq=pow($h3_contribuccion_rq, 2);

        $h1_u_declarada_en_el_IR=0;
        $h2_u_declarada_en_el_IR=0;
        $h3_u_declarada_en_el_IR=0;        
        
        $h1_u_declarada_en_el_IR_lx=$h1promedio*($h1_u_declarada_en_el_IR/100);
        $h2_u_declarada_en_el_IR_lx=$h2promedio*($h2_u_declarada_en_el_IR/100);
        $h3_u_declarada_en_el_IR_lx=$h3promedio*($h3_u_declarada_en_el_IR/100);

        $h1_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;
        $h2_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;
        $h3_contribucion_c=($h1_u_declarada_en_el_IR_lx/2)*$h1_coef_sensibilidad;

        $h1_uy2_c=pow($h1_contribucion_c, 2);
        $h2_uy2_c=pow($h2_contribucion_c, 2);
        $h3_uy2_c=pow($h3_contribucion_c, 2);

        $h1_uy2_suma=$h1_uy2+$h1_uy2_rq+$h1_uy2_c;
        $h2_uy2_suma=$h2_uy2+$h2_uy2_rq+$h2_uy2_c;
        $h3_uy2_suma=$h3_uy2+$h3_uy2_rq+$h3_uy2_c;

        $h1_uc_lx=sqrt($h1_uy2_suma);
        $h2_uc_lx=sqrt($h2_uy2_suma);
        $h3_uc_lx=sqrt($h3_uy2_suma);

        $h1_u_lx=$h1_uc_lx*2;
        $h2_u_lx=$h2_uc_lx*2;
        $h3_u_lx=$h3_uc_lx*2;

        if($h1_pcorr<$item->nmi){
            $h1_conclusion='No se supera el NMI';
            $h1_conclusion_style='style="color: red;font-weight: bolder;"';
        }elseif($h1promedio>$item->nmi){
            $h1_conclusion='Se supera el NMI';
            $h1_conclusion_style='style="color: green;font-weight: bolder;"';
        }else{
            $h1_conclusion='';
            $h1_conclusion_style='';
        }
        if($h2_pcorr<$item->nmi){
            $h2_conclusion='No se supera el NMI';
            $h2_conclusion_style='style="color: red;font-weight: bolder;"';
        }elseif($h2promedio>$item->nmi){
            $h2_conclusion='Se supera el NMI';
            $h2_conclusion_style='style="color: green;font-weight: bolder;"';
        }else{
            $h2_conclusion='';
            $h2_conclusion_style='';
        }
        if($h3_pcorr<$item->nmi){
            $h3_conclusion='No se supera el NMI';
            $h3_conclusion_style='style="color: red;font-weight: bolder;"';
        }elseif($h3promedio>$item->nmi){
            $h3_conclusion='Se supera el NMI';
            $h3_conclusion_style='style="color: green;font-weight: bolder;"';
        }else{
            $h3_conclusion='';
            $h3_conclusion_style='';
        }
        $h1_e1a_c=($item->h1_e1a*$pendiente)+$intercept;
        $h2_e1a_c=($item->h2_e1a*$pendiente)+$intercept;
        $h3_e1a_c=($item->h3_e1a*$pendiente)+$intercept;

        $h1_e2a_c=($item->h1_e2a*$pendiente)+$intercept;
        $h2_e2a_c=($item->h2_e2a*$pendiente)+$intercept;
        $h3_e2a_c=($item->h3_e2a*$pendiente)+$intercept;

        if($h1_e1a_c>0 && $h1_e2a_c>0){
            $h1_kf_c=($h1_e1a_c/$h1_e2a_c)*100;
        }else{
            $h1_kf_c=0;
        }
        
        if($h2_e1a_c>0 && $h2_e2a_c>0){
            $h2_kf_c=($h2_e1a_c/$h2_e2a_c)*100;
        }else{
            $h2_kf_c=0;
        }
        
        if($h3_e1a_c>0 && $h3_e2a_c>0){
            $h3_kf_c=($h3_e1a_c/$h3_e2a_c)*100;
        }else{
            $h3_kf_c=0;
        }
        $lmp=50;

        if($h1_kf_c<$lmp){
            $h1_t2_c='No se supera el NMP';
            $h1_t2_c_style='style="color: green;font-weight: bolder;"';
        }elseif ($h1_kf_c>$lmp) {
            $h1_t2_c='Se supera el NMP';
            $h1_t2_c_style='style="color: red;font-weight: bolder;"';

        }else{
            $h1_t2_c='';
            $h1_t2_c_style='';

        }
        if($h2_kf_c<$lmp){
            $h2_t2_c='No se supera el NMP';
            $h2_t2_c_style='style="color: green;font-weight: bolder;"';
        }elseif ($h2_kf_c>$lmp) {
            $h2_t2_c='Se supera el NMP';
            $h2_t2_c_style='style="color: red;font-weight: bolder;"';
        }else{
            $h2_t2_c='';
            $h2_t2_c_style='';
        }
        if($h3_kf_c<$lmp){
            $h3_t2_c='No se supera el NMP';
            $h3_t2_c_style='style="color: green;font-weight: bolder;"';
        }elseif ($h3_kf_c>$lmp) {
            $h3_t2_c='Se supera el NMP';
            $h3_t2_c_style='style="color: red;font-weight: bolder;"';
        }else{
            $h3_t2_c='';
            $h3_t2_c_style='';
        }

        if($item->h1_e1b>0){
            $h1_e1b_c=($item->h1_e1b*$pendiente)+$intercept;
        }else{
            $h1_e1b_c=0;
        }
        if($item->h2_e1b>0){
            $h2_e1b_c=($item->h2_e1b*$pendiente)+$intercept;
        }else{
            $h2_e1b_c=0;
        }
        if($item->h3_e1b>0){
            $h3_e1b_c=($item->h3_e1b*$pendiente)+$intercept;
        }else{
            $h3_e1b_c=0;
        }

        if($item->h1_e2b>0){
            $h1_e2b_c=($item->h1_e2b*$pendiente)+$intercept;
        }else{
            $h1_e2b_c=0;
        }
        if($item->h2_e2b>0){
            $h2_e2b_c=($item->h2_e2b*$pendiente)+$intercept;
        }else{
            $h2_e2b_c=0;
        }
        if($item->h3_e2b>0){
            $h3_e2b_c=($item->h3_e2b*$pendiente)+$intercept;
        }else{
            $h3_e2b_c=0;
        }

        if($h1_e1b_c>0 && $h1_e2b_c>0){
            $h1_kf2_c=($h1_e1b_c/$h1_e2b_c)*100;
        }else{
            $h1_kf2_c=0;
        }
        if($h2_e1b_c>0 && $h2_e2b_c>0){
            $h2_kf2_c=($h2_e1b_c/$h2_e2b_c)*100;  
        }else{
            $h2_kf2_c=0;
        }
        if($h3_e1b_c>0 && $h3_e2b_c>0){
            $h3_kf2_c=($h3_e1b_c/$h3_e2b_c)*100;  
        }else{
            $h3_kf2_c=0;
        }
        
        $lmp2=60;

        if($h1_kf2_c<$lmp2){
            $h1_t2_c2='No se supera el NMP';
            $h1_t2_c2_style='style="color: green;font-weight: bolder;"';
        }elseif ($h1_kf2_c>$lmp2) {
            $h1_t2_c2='Se supera el NMP';
            $h1_t2_c2_style='style="color: red;font-weight: bolder;"';
        }else{
            $h1_t2_c2='';
            $h1_t2_c2_style='';
        }
        if($h2_kf2_c<$lmp2){
            $h2_t2_c2='No se supera el NMP';
            $h2_t2_c2_style='style="color: green;font-weight: bolder;"';
        }elseif ($h2_kf2_c>$lmp2) {
            $h2_t2_c2='Se supera el NMP';
            $h2_t2_c2_style='style="color: red;font-weight: bolder;"';
        }else{
            $h2_t2_c2='';
            $h2_t2_c2_style='';
        }
        if($h3_kf2_c<$lmp2){
            $h3_t2_c2='No se supera el NMP';
            $h3_t2_c2_style='style="color: green;font-weight: bolder;"';
        }elseif ($h3_kf2_c>$lmp2) {
            $h3_t2_c2='Se supera el NMP';
            $h3_t2_c2_style='style="color: red;font-weight: bolder;"';
        }else{
            $h3_t2_c2='';
            $h3_t2_c2_style='';
        }
        //================================================
        $arrayniveliluminacion[] = array(
                                        'punto'=>$item->num_punto,
                                        'identificacion'=>$item->identificacion,
                                        'nmi'=>$item->nmi,
                                        'h1'=>$item->h1,
                                        'h1_n'=>round($h1_pcorr,2),
                                        'h1_u'=>round($h1_u_lx,2),
                                        //'h1_nu'=>round($h1_pcorr,2)+round($h1_u_lx,2),
                                        'h1_nu'=>round($h1_pcorr,2),//SE QUITA LA SUMA DEL CORREGIDO
                                        'h1_c'=>$h1_conclusion,
                                        'h1_c_style'=>$h1_conclusion_style,
                                        'h2'=>$item->h2,
                                        'h2_n'=>round($h2_pcorr,2),
                                        'h2_u'=>round($h2_u_lx,2),
                                        //'h2_nu'=>round($h2_pcorr,2)+round($h2_u_lx,2),
                                        'h2_nu'=>round($h2_pcorr,2),//SE QUITA LA SUMA DEL CORREGIDO
                                        'h2_c'=>$h2_conclusion,
                                        'h2_c_style'=>$h2_conclusion_style,
                                        'h3'=>$item->h3,
                                        'h3_n'=>round($h3_pcorr,2),
                                        'h3_u'=>round($h3_u_lx,2),
                                        //'h3_nu'=>round($h3_pcorr,2)+round($h3_u_lx,2),
                                        'h3_nu'=>round($h3_pcorr,2),//SE QUITA LA SUMA DEL CORREGIDO
                                        'h3_c'=>$h3_conclusion,
                                        'h3_c_style'=>$h3_conclusion_style,
                                        'h1_kf1'=>round($h1_kf_c,2),
                                        'h2_kf1'=>round($h2_kf_c,2),
                                        'h3_kf1'=>round($h3_kf_c,2),
                                        'h1_t2_c'=>$h1_t2_c,
                                        'h2_t2_c'=>$h2_t2_c,
                                        'h3_t2_c'=>$h3_t2_c,
                                        'h1_t2_c_style'=>$h1_t2_c_style,
                                        'h2_t2_c_style'=>$h2_t2_c_style,
                                        'h3_t2_c_style'=>$h3_t2_c_style,
                                        'lmp1'=>$lmp,
                                        'h1_kf2'=>round($h1_kf2_c,2),
                                        'h2_kf2'=>round($h2_kf2_c,2),
                                        'h3_kf2'=>round($h3_kf2_c,2),
                                        'lmp2'=>$lmp2,
                                        'h1_t2_c2'=>$h1_t2_c2,
                                        'h2_t2_c2'=>$h2_t2_c2,
                                        'h3_t2_c2'=>$h3_t2_c2,
                                        'h1_t2_c2_style'=>$h1_t2_c2_style,
                                        'h2_t2_c2_style'=>$h2_t2_c2_style,
                                        'h3_t2_c2_style'=>$h3_t2_c2_style );
        
        //para la secuencia de calculo
        if($fecha_comp!=$item->fecha){
            $tablas_sec.='<table><tr><td>
                <table class="table table-bordered" border="1">
                    <tr><td colspan="2"></td><td class="titulos">No. de Informe</td><td class="title_tb">'.$folio.'</td></tr>
                    <tr><td class="titulos">Razón Social</td><td colspan="3" class="title_tb">'.$cliente.'</td></tr>
                    <tr><td class="titulos">Fecha</td><td class="title_tb">'.$item->fecha.'</td><td class="titulos">Id. del equipo</td><td class="title_tb">'.$equipo.'</td></tr>
                    <tr><td colspan="4"></td></tr>
                </table>
            </td></tr><tr><td></td></tr></table>';
        }

        $tablas_sec.='<table border="0"><tr nobr="true"><td style="height:210px">
            
        <table width="100%" class="body_ta" cellspacing="0" cellpadding="0">
            <tr colspan="2">
                <td>
                    <table class="table table-bordered" border="1"><tr><td width="15%" class="titulos titulos-12">No. Punto</td><td class="camposi title_tb" width="20%" colspan="2">'.$item->num_punto.'</td><td width="20%" class="titulos titulos-12"><strong>Identificación</strong></td><td class="camposi title_tb" width="45%">'.$item->identificacion.'</td></tr></table>
                </td>
            </tr>
            <tr>
                <th width="70%" style="height:40px">
                </th>
                <th width="30%" rowspan="2">';
                    $table_generado1="";
                    if($item->table_generado1!=""){
                        $table_generado1='<img style="width:750" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $item->table_generado1) . '">';
                    }
                    $tablas_sec.=$table_generado1;
        $tablas_sec.='</th>
            </tr>
            <tr><td>
                <table class="table table-bordered body_ta">
                        <thead><tr class="titulos"><th width="10%">Horario</th><th width="30%" colspan="3">Medición plano de trabajo (lx)</th><th width="10%">Prom. (lx)</th><th width="10%">P Corr (lx)</th><th width="10%">U (lx)</th><th width="10%">NMI</th><th width="20%">Conclusión</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>';if(date("H:i", strtotime($item->h1))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h1)); else $tablas_sec.="--";$tablas_sec.='</td>';
                                if($item->h1_medicion_a>0 || $item->h1_medicion_a!="0.00"){
                                    $tablas_sec.='<td>'.$item->h1_medicion_a.'</td><td>'.$item->h1_medicion_b.'</td><td>'.$item->h1_mdicion_c.'</td><th>'.round($h1promedio,2).'</th><th>'.round($h1_pcorr,2).'</th><th>'.round($h1_u_lx,2).'</th>';
                                }else{
                                    $tablas_sec.='<td>--</td><td>--</td><td>--</td><th>--</th><th>--</th><th>--</th>';
                                }
                                    $tablas_sec.='<td rowspan="3"><p class="pspaces"></p><strong>'.$item->nmi.'</strong></td>';
                                if($item->h1_medicion_a>0 || $item->h1_medicion_a!="0.00"){
                                    $tablas_sec.='<th>'.$h1_conclusion.'</th>';
                                }else{
                                    $tablas_sec.='<td>--</td>';
                                }
                                $tablas_sec.='
                            </tr>
                            <tr>
                                <td>'; if(date("H:i", strtotime($item->h2))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h2)); else $tablas_sec.="--"; $tablas_sec.='</td>';
                                if($item->h2_medicion_a>0 || $item->h2_medicion_a!="0.00"){
                                    $tablas_sec.='<td>'.$item->h2_medicion_a.'</td><td>'.$item->h2_medicion_b.'</td><td>'.$item->h2_medicion_c.'</td><th>'.round($h2promedio,2).'</th><th>'.round($h2_pcorr,2).'</th><th>'.round($h2_u_lx,2).'</th><th>'.$h2_conclusion.'</th>';
                                }else{
                                    $tablas_sec.='<td>--</td><td>--</td><td>--</td><th>--</th><th>--</th><th>--</th><th>--</th>';
                                }
                                $tablas_sec.='
                            </tr>
                            <tr>
                                <td>'; if(date("H:i", strtotime($item->h3))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h3)); else $tablas_sec.="--"; $tablas_sec.='</td>';
                                if($item->h3_medicion_a>0 || $item->h3_medicion_a!="0.00"){
                                    $tablas_sec.='<td>'.$item->h3_medicion_a.'</td><td>'.$item->h3_medicion_b.'</td><td>'.$item->h3_medicion_c.'</td><th>'.round($h3promedio,2).'</th><th>'.round($h3_pcorr,2).'</th><th>'.round($h3_u_lx,2).'</th><th>'.$h3_conclusion.'</th>';
                                }else{
                                    $tablas_sec.='<td>--</td><td>--</td><td>--</td><th>--</th><th>--</th><th>--</th><th>--</th>';
                                }
                                $tablas_sec.='
                            </tr>
                        </tbody>
                    </table>
            </td></tr>
        </table>';

        if($table_generado1==""){
            $tablas_sec.='<br>';
        }
        //log_message('error', 'tablas_sec: '.$tablas_sec);
        $tablas_sec.='<table class="table table-bordered body_ta">
            <thead>
                <tr class="titulos">
                    <th width="7%" rowspan="2"><p class="pspaces"></p>Horario</th>
                    <th width="14%" colspan="2"><p class="pspaces"></p>R - PT (lx)</th>
                    <th width="14%" colspan="2"><p class="pspaces"></p>R -PT Corregido (lx)</th>
                    <th width="5%" rowspan="2"><p class="pspaces"></p>Kf (%)</th>
                    
                    <th width="4%" rowspan="2"><p class="pspaces"></p>NMP (%)</th>
                    <th width="13.5%" rowspan="2"><p class="pspaces"></p>Conclusión</th>
                    <th width="10%" colspan="2"><p class="pspaces"></p>R - P (lx)</th>
                    <th width="10%" colspan="2">R - P Corregido (lx)</th>
                    <th width="5%" rowspan="2"><p class="pspaces"></p>Fc (%)</th>
                    <th width="4%" rowspan="2">NMP (%)</th>
                    <th width="13.5%" rowspan="2"><p class="pspaces"></p>Conclusión</th>
                </tr>
                <tr class="titulos"><th>E1</th><th>E2</th><th>E1</th><th>E2</th><th>E1</th><th>E2</th><th>E1</th><th>E2</th></tr>
            </thead>
            <tbody>
                <tr>
                    <th width="7%">'; if(date("H:i", strtotime($item->h1))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h1)); else $tablas_sec.="--";$tablas_sec.='</th>';
                    if($item->h1_e1a>0){
                        $tablas_sec.='<td width="7%">'.$item->h1_e1a.'</td><td width="7%">'.$item->h1_e2a.'</td><th width="7%">'.round($h1_e1a_c,2).'</th><th width="7%">'.round($h1_e2a_c,2).'</th>';
                    }else{
                        $tablas_sec.='<td width="7%">--</td><td width="7%">--</td><th width="7%">--</th><th width="7%">--</th>';
                    }
                    $tablas_sec.='<th width="5%">'.round($h1_kf_c,2).'</th>
                    <th width="4%" rowspan="3" class="titulos"><p class="pspaces"></p><strong>'.$lmp.'</strong></th>
                    <th width="13.5%">'; if($item->h1_e1a>0) $tablas_sec.=$h1_t2_c; else $tablas_sec.="--";$tablas_sec.='</th>';
                    if($item->h1_e1b>0){
                        $tablas_sec.='<th width="5%">'.$item->h1_e1b.'</th><th width="5%">'.$item->h1_e2b.'</th><th width="5%">'.round($h1_e1b_c,2).'</th><th width="5%">'.round($h1_e2b_c,2).'</th><th width="5%">'.round($h1_kf2_c,2).'</th>';
                    }else{
                        $tablas_sec.='<th width="5%">--</th><th width="5%">-</th><th width="5%">-</th><th width="5%">-</th><th width="5%">-</th>';
                    }
                    $tablas_sec.='
                    <th width="4%" class="titulos" rowspan="3"><p class="pspaces"></p><strong>'.$lmp2.'</strong></th>
                    <th width="13.5%">'; if($item->h1_e1b>0) $tablas_sec.=$h1_t2_c2; else $tablas_sec.="--";$tablas_sec.='</th>
                </tr>
                <tr>
                    <th>'; if(date("H:i", strtotime($item->h2))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h2)); else $tablas_sec.="--";$tablas_sec.='</th>';
                    if($item->h2_e1a>0){
                        $tablas_sec.='<td>'.$item->h2_e1a.'</td><td>'.$item->h2_e2a.'</td><th>'.round($h2_e1a_c,2).'</th><th>'.round($h2_e2a_c,2).'</th><th>'.round($h2_kf_c,2).'</th><th>'.$h2_t2_c.'</th>';
                    }else{
                        $tablas_sec.='<td>--</td><td>--</td><th>--</th><th>--</th><th>--</th><th>--</th>';
                    }
                    if($item->h2_e1b>0){
                        $tablas_sec.='<th>'.$item->h2_e1b.'</th><th>'.$item->h2_e2b.'</th><th>'.round($h2_e1b_c,2).'</th><th>'.round($h2_e2b_c,2).'</th><th>'.round($h2_kf2_c,2).'</th><th>'.$h2_t2_c2.'</th>';
                    }else{
                        $tablas_sec.='<th>--</th><th>--</th><th>--</th><th>--</th><th>--</th><th>--</th>';
                    }
                   $tablas_sec.=' 
                </tr>
                <tr>
                    <th>'; if(date("H:i", strtotime($item->h3))!="00:00") $tablas_sec.=date("H:i", strtotime($item->h3)); else $tablas_sec.="--";$tablas_sec.='</th>';
                    if($item->h3_e1a>0){
                        $tablas_sec.='<td>'.$item->h3_e1a.'</td><td>'.$item->h3_e2a.'</td><th>'.round($h3_e1a_c,2).'</th><th>'.round($h3_e2a_c,2).'</th><th>'.round($h3_kf_c,2).'</th><th>'.$h3_t2_c.'</th>';
                    }else{
                        $tablas_sec.='<td>--</td><td>--</td><th>--</th><th>--</th><th>--</th><th>--</th>';
                    }
                    if($item->h3_e1b>0){
                        $tablas_sec.='<th>'.$item->h3_e1b.'</th><th>'.$item->h3_e2b.'</th><th>'.round($h3_e1b_c,2).'</th><th>'.round($h3_e2b_c,2).'</th><th>'.round($h3_kf2_c,2).'</th><th>'.$h3_t2_c2.'</th>';
                    }else{
                        $tablas_sec.='<th>--</th><th>--</th><th>--</th><th>--</th><th>--</th><th>--</th>';
                    }
                    $tablas_sec.='
                </tr>
            </tbody>
        </table></td></tr></table>';
        //log_message('error', 'tablas_sec parte2: '.$tablas_sec);
        //termina las tablas para la secuencia de calculo
        $rowpunto++;
        $fecha_comp=$item->fecha;
   }
   $tablas_sec.='<table border="0" cellpadding="1" align="center" class="fon9"><tr><td>Prom. = Promedio; P Corr= Promedio Corregido en luxes; U(lx) = Incertidumbre en luxes; NMI = Nivel  Mínimo de Iluminación; R-PT = Reflexión en Plano de Trabajo; R-P = Reflexión en Pared; Kf = Factor de Reflexión; NMP = Nivel Máximo Permisible</td></tr></table>';
   /* ***********************************************************/

    foreach ($arrayniveliluminacion as $item) {
        $htmlg.='<tr>
            <td width="8%" rowspan="3"><p class="pspaces"></p>'.$item["punto"].'</td>
            <td width="37%" rowspan="3"><p class="pspaces"></p>'.$item["identificacion"].'</td>';
            if(date("H:i", strtotime($item["h1"]))!="00:00"){ $htmlg.='<td width="10%">'.date("H:i", strtotime($item["h1"])).'</td>';} else{ $htmlg.='<td width="10%">---</td>'; } 
            if($item['h1_n']==0){ 
                $htmlg.='<td width="10%">---</td><td width="10%">---</td><td width="5%" rowspan="3">'.$item['nmi'].'</td><td width="20%">---</td>'; 
            }else{
                $htmlg.='<td width="10%">'.number_format($item["h1_n"], 2, ".", "").'</td><td width="10%">'.number_format($item["h1_u"], 2, ".", "").'</td><!--<td width="10%">'.number_format($item["h1_nu"], 2, ".", "").'</td>--><td width="5%" rowspan="3"><p class="pspaces"></p>'.$item["nmi"].'</td><td width="20%" '. $item["h1_c_style"].'>'.$item["h1_c"].'</td>';
            }
        $htmlg.='</tr>
        <tr>';
            if(date("H:i", strtotime($item["h2"]))!="00:00"){ $htmlg.='<td width="10%">'.date("H:i", strtotime($item["h2"])).'</td>';} else{ $htmlg.='<td width="10%">---</td>'; } 
            if($item['h2_n']==0){
                $htmlg.='<td width="10%">---</td><td width="10%">---</td><td width="20%">---</td>'; 
            }else{
                $htmlg.="<td width='10%'>".number_format($item['h2_n'], 2, '.', '')."</td><td width='10%'>".number_format($item['h2_u'], 2, '.', '')."</td><!--<td width='10%'>".number_format($item['h2_nu'], 2, '.', '')."</td>--><td width='20%' ".$item['h2_c_style'].">".$item['h2_c']."</td>";
            }
        $htmlg.='</tr>
        <tr>';
            if(date("H:i", strtotime($item["h3"]))!="00:00"){ $htmlg.='<td width="10%">'.date("H:i", strtotime($item["h3"])).'</td>';} else{ $htmlg.='<td width="10%">---</td>'; } 
            if($item['h3_n']==0){ 
                $htmlg.='<td width="10%">---</td><td width="10%">---</td><td width="20%">---</td>';
            }else{
                $htmlg.="<td width='10%'>".number_format($item['h3_n'], 2, '.', '')."</td><td width='10%'>".number_format($item['h3_u'], 2, '.', '')."</td><!--<td width='10%'>".number_format($item['h3_nu'], 2, '.', '')."</td>--><td width='20%' ".$item['h3_c_style'].">".$item['h3_c']."</td>";
            }
        $htmlg.="</tr>";
    }

    $htmlg.='</tbody>
        </table> 

        <table border="0" align="center">            
            <tr><td class="fosi9">NMI = Nivel Mínimo de Iluminación</td></tr>
            <tr><td class="fosi9">U exp = Incertidumbre expandida (K=2, confianza del 95.45%)</td></tr>
            <tr><td class="fosi9"><u><i>Regla de Decisión</i></u></td></tr>
            <tr><td class="fosi9"><u><i>Aceptación Simple. La zona de seguridad tiene una dimensión igual a cero (w = 0), lo que implica que la aceptación se da cuando el resultado de una medición está por arriba del límite de tolerancia y se rechaza cuando el resultado este por debajo del límite de tolerancia (Declaración Binaria).</i></u></td></tr>
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->AddPage('P', 'A4');
    $img_2="";
    /*if($GLOBALS['tablageneral1']!=""){
        $img_2 = '<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $GLOBALS['tablageneral2']).'">';
    }*/
    $htmlg="";
    $htmlg='<style type="text/css">
                .fosi9{font-size:9px;}
                .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:9px;valign: middle;vertical-align: middle;}
                .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:10px;}
                .pspaces{ font-size: 0.1px; }
                .textcenter2{text-align:center; font-size:11px;}
            </style>
            <table align="center"><tr class="textcenter2"><td><i>•  Factor de Reflexión</i></td></tr><tr><td style="height:31px"></td></tr></table>
            <table class="tableb" align="center" cellpadding="3" width="100%">
                <thead><tr><th width="6%" rowspan="2">No. de Punto</th><th width="18%" rowspan="2">Identificación del punto de medición (área/identificación)</th><th width="8%" rowspan="2">Horario de medición</th><th width="34%" colspan="3">Reflexión plano de trabajo</th><th width="34%" colspan="3">Reflexión pared</th></tr><tr><th width="10%">Kf (%)</th><th width="8%">NMP (%)</th><th width="16%">Conclusión</th><th width="10%">Kf (%)</th><th width="8%">NMP (%)</th><th width="16%">Conclusión</th></tr></thead>
                <tbody>';
                    foreach ($arrayniveliluminacion as $item) { 
                        $htmlg.='<tr><td width="6%" rowspan="3">'.$item['punto'].'</td><td width="18%" rowspan="3"><p class="pspaces"></p>'.$item['identificacion'].'</td>';
                            if(date("H:i", strtotime($item["h1"]))!="00:00"){
                                $htmlg.='<td width="8%">'.date("H:i", strtotime($item["h1"])).'</td>';
                            }else{
                                $htmlg.='<td width="8%">---</td>';
                            }
                            $htmlg.='<td width="10%">';
                                if($item['h1_n']==0){ $htmlg.='---'; }else{ $htmlg.=number_format($item['h1_kf1'], 2, '.', ''); } 
                            $htmlg.="</td>";
                            $htmlg.='<td width="8%" rowspan="3"><p class="pspaces"></p>'.$item["lmp1"].'</td>';
                            if($item['h1_n']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item["h1_t2_c_style"].'>'.$item["h1_t2_c"].'</td>'; }

                            $htmlg.='<td width="10%">';if($item['h1_kf2']==0){ $htmlg.='---';}else{ $htmlg.=number_format($item['h1_kf2'], 2, '.', '');}$htmlg.='</td>
                            <td width="8%" rowspan="3"><p class="pspaces"></p>'.$item["lmp2"].'</td>';
                            if($item['h1_kf2']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item["h1_t2_c2_style"].'>'.$item["h1_t2_c2"].'</td>'; }
                        $htmlg.='</tr><tr>';
                            if(date("H:i", strtotime($item["h2"]))!="00:00"){
                                $htmlg.='<td width="8%">'.date("H:i", strtotime($item["h2"])).'</td>';
                            }else{
                                $htmlg.='<td width="8%">---</td>';
                            }
                            $htmlg.='<td width="10%">'; if($item['h2_n']==0){ $htmlg.='---';}else{ $htmlg.=number_format($item['h2_kf1'], 2, '.', ''); } $htmlg.="</td>";
                            if($item['h2_n']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item["h2_t2_c_style"].'>'.$item["h2_t2_c"].'</td>'; }

                            $htmlg.='<td width="10%">'; if($item['h2_kf2']==0){ $htmlg.='---';}else{ $htmlg.=number_format($item['h2_kf2'], 2, '.', '');}$htmlg.="</td>";
                            if($item['h2_kf2']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item["h2_t2_c2_style"].'>'.$item["h2_t2_c2"].'</td>'; } 
                        $htmlg.='</tr><tr>';
                            if(date("H:i", strtotime($item["h3"]))!="00:00"){
                                $htmlg.='<td width="8%">'.date("H:i", strtotime($item["h3"])).'</td>';
                            }else{
                                $htmlg.='<td width="8%">---</td>';
                            }
                            $htmlg.='<td width="10%">'; if($item['h3_n']==0){ $htmlg.='---';}else{ $htmlg.=number_format($item['h3_kf1'], 2, '.', '');} $htmlg.="</td>";
                            if($item['h3_n']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item[
                                "h3_t2_c_style"].'>'.$item["h3_t2_c"].'</td>'; }

                            $htmlg.='<td width="10%">'; if($item['h3_kf2']==0){ $htmlg.='---';}else{ $htmlg.=number_format($item['h3_kf2'], 2, '.', '');}$htmlg.="</td>";
                            if($item['h3_kf2']==0){ $htmlg.='<td width="16%">---</td>';}else{ $htmlg.='<td width="16%" '.$item["h3_t2_c2_style"].'>'.$item["h3_t2_c2"].'</td>'; }
                        $htmlg.="</tr>";
                    }
                    
                $htmlg.='</tbody>
            </table> 
            <table align="center">
                <!--<tr><td>•   Factor de Reflexión</td></tr>
                <tr><td>'.$img_2.'</td></tr>-->
                <tr><td class="fosi9"><i>NMP = Nivel Máximo Permisible</i></td></tr><tr><td class="fosi9"><i>Kf = Factor de reflexión de la superficie </i></td></tr></table>';
    //log_message('error', 'htmlg: '.$htmlg);
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $tot_ptos=0; $con_incidencia=0;
    foreach ($get_conclu as $c) {
        $tot_ptos=$c->total_ptos;
        $con_incidencia=$c->con_incidencia;
    }
    $pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
    $htmlgtab="";
    $htmlg='<style type="text/css">
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                .sangria{text-indent: 45px;}
                .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
                .pspaces{ font-size: 0.1px; }
                .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:12px;valign: middle;vertical-align: middle;}
                .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:14px;}
            </style>
            <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p>';

            /*$htmlg.='
                <p class="tdtext sangria">a) <b>'.$con_incidencia.'</b> puntos cuentan con incidencia solar en donde se realizan 3 mediciones (aproximadamente en la primera hora, a la mitad y al final del turno) en el turno critico determinado en cada área. A continuación, se presentan la cantidad de puntos que superan y que no superan los niveles mínimos de iluminación. 
                </p>
                <table border="1" align="center"><tr><td><br></td></tr></table>';
            $htmlg.='<table border="1" align="center">
                <thead>
                    <tr>
                        <td>No. de puntos evaluados</td>
                        <td>No. de puntos que superan el NMI</td>
                        <td>No. de puntos que no superan el NMI</td>
                    </tr>
                </thead>';*/

            $cont_conc=0; $cont_sin=0; $cont_conc2=0; $cont_chart=""; $htmlg2=""; $htmlg3=""; $num=1;
            foreach ($get_conclu as $c) {
                $img_chart="";
                if($c->tipo==1 && $c->tipo_incidencia==1 && $c->con_incidencia>0){
                    $cont_conc++;
                    if($c->img_chart!=""){
                        //$img_chart=$c->img_chart;
                        //$img_chart='<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $c->img_chart).'">';
                        $ruta=FCPATH.'public/conclusiones/'.$c->img_chart;
                        $img_chart='<img src="'.$ruta.'">';
                    }
                    if($cont_conc==1){
                        $htmlg.='<p>'.$num.'. Se evalúa un total de <strong>'.$tot_ptos.'</strong> puntos de <strong>iluminación</strong>, de los cuales: </p>
                            <p class="tdtext sangria">a) <strong>'.$c->con_incidencia.'</strong> puntos cuentan con incidencia solar en donde se realizan 3 mediciones (aproximadamente en la primera hora, a la mitad y al final del turno) en el turno crítico determinado en cada área. A continuación, se presentan la cantidad de puntos que superan y que no superan los niveles mínimos de iluminación. 
                            </p>';
                        $htmlg.='<table  class="tableb" align="center" cellpadding="2" >
                            <thead><tr><td style="color:#7B7B7B">No. de puntos evaluados</td><td style="color:#7B7B7B">No. de puntos que superan el NMI</td><td style="color:#7B7B7B">No. de puntos que no superan el NMI</td></tr></thead>';   
                        $num="2"; 
                    }
                    if($cont_conc==1){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">1ra medición</td></tr>';
                    }
                    if($cont_conc==2){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">2da medición</td></tr>';
                    }
                    if($cont_conc==3){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">3ra medición</td></tr>';
                    }
                    $htmlg.='<tr><td>'.$c->num_ptos_evalua.'</td><td style=" color:#548235">'.$c->num_supera.'</td><td style=" color:red">'.$c->num_no_supera.'</td></tr>';
                    
                    if($cont_conc==3){
                        $htmlg.='</table>';    
                    }

                    if($cont_conc==1){
                        $htmlg2.='<table border="0" align="center">
                                    <tr>';
                    }
                    $htmlg2.='
                        <td>'.$img_chart.'</td>';

                    if($cont_conc==3){
                        $htmlg.=$htmlg2.'</tr></table>';    
                    }
                }            
                if($c->tipo==1 && $c->tipo_incidencia==2 && $c->sin_incidencia>0){
                    if($c->img_chart!=""){
                        //$img_chart='<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $c->img_chart).'">';
                        $ruta=FCPATH.'public/conclusiones/'.$c->img_chart;
                        $img_chart='<img src="'.$ruta.'">';
                    }
                    $cont_sin++;
                    $htmlg.='<p class="tdtext sangria">b) <strong>'.$c->sin_incidencia.'</strong> puntos son sin incidencia solar y, por lo tanto, se realiza solo una medición en un horario indistinto. A continuación, se presentan la cantidad de puntos que superan y que no superan los niveles mínimos de iluminación.
                    </p>
                    <table class="tableb" align="center" cellpadding="2">
                        <thead><tr><td style="color:#7B7B7B">No. de puntos evaluados</td><td style="color:#7B7B7B">No. de puntos que superan el NMI</td><td style="color:#7B7B7B">No. de puntos que no superan el NMI</td></tr>
                        </thead>
                        <tr><td>'.$c->num_ptos_evalua.'</td><td style="color:#548235">'.$c->num_supera.'</td><td style="color:red">'.$c->num_no_supera.'</td></tr>
                    </table>
                    <table border="0" align="center">
                        <tr><td><br></td></tr><tr><td width="33.33%"></td><td width="33.33%">'.$img_chart.'</td><td width="33.33%"></td></tr>
                    </table>'; 
                }

                ////////////////////////////////REFLEXION////////////////////////////////////
                if($c->tipo==2 && $c->tipo_incidencia==1 && $c->con_incidencia>0){
                    $cont_conc2++;
                    if($cont_conc2==1 && $cont_sin>0){
                        $htmlg.='<table border="0" align="center"><tr><td height="150px"></td></tr></table>'; 
                    }
                    if($cont_conc>0){
                        $inci="c)";
                        $inci2="d)";
                    }else{
                        $inci="a)"; 
                        $inci2="b)";
                    }
                    if($c->img_chart!=""){
                        //$img_chart=$c->img_chart;
                        //$img_chart='<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $c->img_chart).'">';
                        $ruta=FCPATH.'public/conclusiones/'.$c->img_chart;
                        $img_chart='<img src="'.$ruta.'">';
                    }
                    if($cont_conc2==1){
                        $htmlg.='<p> '.$num.'. Se evalúa un total de <b>'.$tot_ptos.'</b> puntos de <b> reflexión</b>, de los cuales: </p>
                            <p class="tdtext sangria">'.$inci.' <b>'.$c->con_incidencia.'</b> puntos cuentan con incidencia solar en donde se realizan 3 mediciones (aproximadamente en la primera hora, a la mitad y al final del turno) en el turno crítico determinado en cada área. A continuación, se presentan la cantidad de puntos que superan y que no superan los niveles máximos permisibles del factor de reflexión. 
                            </p>';
                        $htmlg.='<table class="tableb" align="center" cellpadding="2">
                            <thead><tr><td style="color:#7B7B7B">No. de puntos evaluados</td><td style="color:#7B7B7B">No. de puntos que superan el NMP</td><td style="color:#7B7B7B">No. de puntos que no superan el NMP</td></tr>
                            </thead>';    
                    }
                    if($cont_conc2==1){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">1ra medición</td></tr>';
                    }
                    if($cont_conc2==2){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">2da medición</td></tr>';
                    }
                    if($cont_conc2==3){
                        $htmlg.='<tr><td colspan="3" style="color:#7B7B7B">3ra medición</td></tr>';
                    }
                    $htmlg.='<tr><td>'.$c->num_ptos_evalua.'</td><td style="color:red">'.$c->num_supera.'</td><td style="color:#548235">'.$c->num_no_supera.'</td></tr>';

                    if($cont_conc2==3){
                        $htmlg.='</table>';    
                    }

                    if($cont_conc2==1){
                        $htmlg3.='<table border="0" align="center"><tr>';
                    }
                    $htmlg3.='
                            <td>'.$img_chart.'</td>';

                    if($cont_conc2==3){
                        $htmlg.=$htmlg3.'</tr></table>';    
                    }
                }
                
                if($c->tipo==2 && $c->tipo_incidencia==2 && $c->sin_incidencia>0){
                    if($c->img_chart!=""){
                        //$img_chart='<img src="@'.preg_replace('#^data:image/[^;]+;base64,#', '', $c->img_chart).'">';
                        $ruta=FCPATH.'public/conclusiones/'.$c->img_chart;
                        $img_chart='<img src="'.$ruta.'">';
                    }
                    $htmlg.='<p class="tdtext sangria">'.$inci2.' <b>'.$c->sin_incidencia.'</b> puntos son sin incidencia solar y, por lo tanto, se realiza solo una medición (en horario indistinto). A continuación, se presentan la cantidad de puntos que superan y que no superan los niveles máximos permisibles del factor de reflexión.
                    </p>
                    <table class="tableb" align="center" cellpadding="2">
                        <thead><tr><td style="color:#7B7B7B">No. de puntos evaluados</td><td style="color:#7B7B7B">No. de puntos que superan el NMI</td><td style="color:#7B7B7B">No. de puntos que no superan el NMI</td></tr>
                        </thead>
                        <tr><td>'.$c->num_ptos_evalua.'</td><td style="color:red">'.$c->num_supera.'</td><td style="color:#548235">'.$c->num_no_supera.'</td></tr>
                    </table>
                    <table border="0" align="center"><tr><td><br></td></tr><tr><td width="33.33%"></td><td width="33.33%">'.$img_chart.'</td><td width="33.33%"></td></tr></table>'; 
                }
            }//foreach

    //log_message('error', 'htmlg: '.$htmlg);
    $pdf->writeHTML($htmlg, true, false, true, false, '');


    $htmlg="";
    $pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
    $htmlg='<style type="text/css">
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                .tdtext{font-family:Arial;text-align: justify;font-size:15px;}
            </style>
            <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
            <p class="tdtext">La luz es un elemento esencial en nuestra capacidad de ver y necesaria para apreciar la forma, el color y la perspectiva de los objetos que nos rodean en nuestra vida diaria. La mayor parte de la información que obtenemos a través de nuestros sentidos la obtenemos por la vista. Por lo tanto, la iluminación es un factor importante en la ejecución de las actividades y prevención de accidentes, un ambiente de trabajo con luz deficiente daña la vista y puede ser causa de posturas inadecuadas que repercuten en la salud y el desempeño del trabajador.</p>
            <p class="tdtext">Cada actividad que se realiza en el centro de trabajo requiere de un nivel específico de iluminación. En general, cuanto mayor sea la dificultad de percepción visual, mayor deberá ser el nivel de iluminación.</p>
            <p class="tdtext">Los factores esenciales en las condiciones que afectan la visión son la distribución de la luz y el contraste de luminarias. El constante ir y venir por zonas sin una iluminación adecuada causa fatiga ocular y, con el tiempo, esto puede dar lugar a una reducción de la capacidad visual. Cuando existe una fuente de luz brillante en el campo visual se producen brillos deslumbrantes; el resultado es una disminución de la capacidad de distinguir objetos. Los trabajadores que sufren los efectos del deslumbramiento constante pueden sufrir fatiga ocular, así como, trastornos funcionales, aunque en muchos casos ni siquiera sean conscientes de ello.</p>
            <p class="tdtext">Por todo lo anterior, es importante implementar en el centro de trabajo un sistema de iluminación que cumpla con los niveles necesarios para desarrollar cada actividad en las diferentes áreas que constituyen al centro de trabajo y así evitar riesgos a la salud del trabajador e inclusive al centro de trabajo.</p>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado -----------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

    $htmlg='<style type="text/css">
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
                .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
                .txt_td{background-color:rgb(217,217,217);}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
                .tableb td{border:1px solid #808080;}
                .cet{text-align:center;}
                .padre {display: flex;align-items: center;}
                .pspaces{ font-size:0.4px; }
            </style>
            <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>
            <p class="">Determinar el nivel de iluminación y factor de reflexión en cada tarea visual o área del centro de trabajo, con el fin de proveer un ambiente seguro y saludable en la realización de las tareas que desarrollen los trabajadores de la empresa <span style="font-weight:bold">'.$empresa.'</span>
            </p>
            <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>
            <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Condiciones de iluminación en los centros de trabajo".</p>
            <table>
                    <tr><td class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO*</b></td></tr>
                    <tr><td></td></tr>
            </table>
            <table cellpadding="4" style="font-size:11px;" class="tableb">
                <tr><td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td><td class="tdtext1 cet" width="59%">'.$empresa.'</td></tr>
                <tr><td class="txt_td"><p class="pspaces"></p><i>b)  Domicilio</i></td><td class="tdtext1 cet">'.$direccion2.'</td></tr>
                <tr><td class="txt_td"><i>c)  R.F.C.</i></td><td class="tdtext1 cet">'.$rfccli.'</td></tr>
                <tr><td class="txt_td"><i>d)  Giro y/o actividad principal</i></td><td class="tdtext1 cet">'.$girocli.'</td></tr>
                <tr><td class="txt_td"><i>e)  Responsable de la empresa</i></td><td class="tdtext1 cet">'.$representacli.'</td></tr>
                <tr><td class="txt_td"><i>f)  Teléfono</i></td><td class="tdtext1 cet"> '.$telefonocli.'</td></tr>
            </table>
            <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr>
                    <tr><td></td></tr>
                    <tr><td class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></td></tr>
                    <tr><td></td></tr>
            </table>
            
            <table cellpadding="4" style="font-size:11px;" class="tableb"><tr><td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td><td class="tdtext1 cet" width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr><tr><td class="txt_td"><p class="pspaces"></p><i>b)  Domicilio</i></td><td class="tdtext1 cet">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td></tr><tr><td class="txt_td"><i>c)  R.F.C.</i></td><td class="tdtext1 cet">ALP160621FD6</td></tr><tr><td class="txt_td"><i>d) Teléfono</i></td><td class="tdtext1 cet">(01) 222 2265395</td></tr><tr><td class="txt_td"><i>e) e-mail </i></td><td class="tdtext1 cet">gerencia@ahisa.mx</td></tr><tr><td class="txt_td"><i>f) Número de acreditación </i></td><td class="tdtext1 cet">AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td></tr><tr><td class="txt_td"><i>g) Número de aprobación</i></td><td class="tdtext1 cet">LPSTPS-153/2022</td></tr><tr><td class="txt_td"><i>h) Lugar de expedición del informe </i></td><td class="tdtext1 cet">Puebla, Puebla - México </td></tr><tr><td class="txt_td"><i>i) Fecha de expedición del informe de resultados.</i></td><td class="tdtext1 cet">'.$fechah.'</td></tr><tr><td class="txt_td"><i>j) Signatario responsable de la evaluación</i></td><td class="tdtext1 cet">'.$tecnico.'</td></tr>
            </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $htmlg="";
    $pdf->AddPage('P', 'A4');
    $pdf->Bookmark('H. Metodología para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
    $htmlg='<style type="text/css">
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
                .tdtext2{font-family:Arial;text-align: justify;font-size:12px;font-style:italic;}
                .tdtext11{font-family:Arial;text-align: justify;font-size:11px;font-style:italic;}
                .tdtext2_tab{font-family:Arial;text-align: justify;font-size:8.8px;}
                .cab1{font-family:Arial;text-align: justify;font-size:10px;font-style:italic;}
                .bodycab1{font-family:Calibri;text-align: center;font-size:10px;}
                .tejus{text-align: justify;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
                .sangria{text-indent: 45px;}
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td{border:1px solid #808080;}
                .cet{text-align:center;}
                .checked{font-size:14px;font-family:dejavusans;}
                .pspaces{font-size:0.1px;}
            </style>
            <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
            <p><u>Reconocimiento.</u></p>
            <p class="tdtext sangria tejus">El propósito del reconocimiento es identificar aquellas áreas del centro de trabajo y las tareas visuales asociadas a los puestos de trabajo, asimismo, identificar aquéllas donde exista una iluminación deficiente o exceso de iluminación que provoque deslumbramiento.</p>
            <p class="tdtext sangria tejus">Para lo anterior, se debe realizar un recorrido por todas las áreas del centro de trabajo donde los trabajadores realizan sus tareas visuales, y considerar, en su caso, los reportes de los trabajadores (opinión de las condiciones de iluminación en su puesto trabajo), así como recabar la información técnica.</p>
            <p class="tdtext sangria tejus">Para determinar las áreas y tareas visuales de los puestos de trabajo debe recabarse y registrarse la información del reconocimiento de las condiciones de iluminación de las áreas de trabajo, así como de las áreas donde exista una iluminación deficiente o se presente deslumbramiento y, posteriormente, conforme se modifiquen las características de las luminarias o las condiciones de iluminación del área de trabajo, con los datos siguientes:</p>
            <ol type="a" class="tdtext">
                <li>Distribución de las áreas de trabajo, del sistema de iluminación (número y distribución de luminarias), de la maquinaria y del equipo de trabajo; </li>
                <li>Potencia de las lámparas; </li>
                <li>Descripción del área iluminada: colores y tipo de superficies del local o edificio; </li>
                <li>Descripción de las tareas visuales y de las áreas de trabajo, de acuerdo con la Tabla 2 de este apartado;</li>
                <li>Descripción de los puestos de trabajo que requieren iluminación localizada, y </li>
                <li>La información sobre la percepción de las condiciones de iluminación por parte del trabajador al patrón. </li>
            </ol>
            <p><u>Evaluación de los niveles de iluminación.</u></p>
            <p class="tdtext sangria tejus sangria">A partir de los registros del reconocimiento, se debe realizar la evaluación de los niveles de iluminación en las áreas o puestos de trabajo.</p>
            <p class="tdtext sangria tejus">Se deberá determinar el factor de reflexión en el plano de trabajo y paredes que por su cercanía al trabajador afecten las condiciones de iluminación, y compararlo contra los niveles máximos permisibles del factor de reflexión de la Tabla 1 de este apartado. </p>
            <table>
                <tr>
                    <td>
                        <table  cellpadding="5" align="center" class="tableb">
                            <tr><td class="txt_td tdtext2 cet" width="30%">Concepto</td><td class="txt_td tdtext2 cet" width="70%">Niveles Máximos Permisibles de Reflexión, kf</td></tr>
                            <tr><td class="txt_td tdtext2 cet">Paredes</td><td>60%</td></tr>
                            <tr><td class="txt_td tdtext2 cet">Plano de Trabajo</td><td>50%</td></tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="color: rgb(0,32,96); font-size:9px;" align="center">TABLA 1.</td>
                </tr>
            </table>
            <p class="tdtext sangria">Nota: Se considera que existe deslumbramiento en el área y puesto de trabajo, cuando el valor de la reflexión (Kf) supere los valores establecidos en la tabla anterior.</p>
            <p class="tdtext sangria">La evaluación de los niveles de iluminación debe realizarse en una jornada laboral bajo condiciones normales de operación, se puede hacer por áreas de trabajo, puestos de trabajo o una combinación de estos.</p>
            <p class="tdtext sangria"><u>Evaluación de los niveles de iluminación.</u></p>
            <p class="tdtext sangria">De acuerdo con la información obtenida durante el reconocimiento, se establecerá la ubicación de los puntos de medición de las áreas de trabajo seleccionadas, donde se evaluarán los niveles de iluminación.</p>
            <p class="tdtext sangria">Cuando se utilice iluminación artificial, antes de realizar las mediciones, se debe de cumplir con lo siguiente:</p>

            <ol type="a" class="tdtext">
                <li class="tejus">Encender las lámparas con antelación, permitiendo que el flujo de luz se estabilice; si se utilizan lámparas de descarga, incluyendo lámparas fluorescentes, se debe esperar un periodo de 20 minutos antes de iniciar las lecturas. Cuando las lámparas fluorescentes se encuentren montadas en luminarias cerradas, el periodo de estabilización puede ser mayor; </li>
                <li class="tejus">En instalaciones nuevas con lámparas de descarga o fluorescentes, se debe esperar un periodo de 100 horas de operación antes de realizar la medición, y </li>
                <li class="tejus">Los sistemas de ventilación deben operar normalmente, debido a que la iluminación de las lámparas de descarga y fluorescentes presentan fluctuaciones por los cambios de temperatura. </li>
            </ol>
            <p class="tdtext sangria">Cuando se utilice exclusivamente iluminación natural, se debe realizar al menos las mediciones en cada área o puesto de trabajo de acuerdo con lo siguiente:</p>
            <ol type="a" class="tdtext">            
                <li class="tejus">Cuando no influye la luz natural en la instalación ni el régimen de trabajo de la instalación, se deberá efectuar una medición en horario indistinto en cada puesto o zona determinada, independientemente de los horarios de trabajo en el sitio;</li>
                <li class="tejus">Cuando sí influye la luz natural en la instalación, el turno en horario diurno (sin periodo de oscuridad en el turno o turnos) y turnos en horario diurno y nocturnos (con periodo de oscuridad en el turno o turnos), deberán efectuarse 3 mediciones en cada punto o zona determinada distribuidas en un turno de trabajo que pueda presentar las condiciones críticas de iluminación de acuerdo con lo siguiente:
                    <ul>
                        <li>Una lectura tomada aproximadamente en la primera hora del turno;</li><li>Una lectura tomada aproximadamente a la mitad del turno, y</li><li>Una lectura tomada aproximadamente en la última hora del turno.</li>
                    </ul>
                </li>
                <li class="tejus">Cuando sí influye la luz natural en la instalación y se presentan condiciones críticas, efectuar una medición en cada punto o zona determinada en el horario que presente tales condiciones críticas de iluminación.</li>
            </ol>';
            $pdf->writeHTML($htmlg, true, false, true, false, '');

            $htmlg="";
            $pdf->AddPage('P', 'A4');
            $htmlg='<style type="text/css">
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
                .tdtext2{font-family:Arial;text-align: justify;font-size:12px;font-style:italic;}
                .tdtext11{font-family:Arial;text-align: justify;font-size:11px;font-style:italic;}
                .tdtext2_tab{font-family:Arial;text-align: justify;font-size:8.8px;}
                .cab1{font-family:Arial;text-align: justify;font-size:10px;font-style:italic;}
                .bodycab1{font-family:Calibri;text-align: center;font-size:10px;}
                .tejus{text-align: justify;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
                .sangria{text-indent: 45px;}
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td{border:1px solid #808080; text-align: center;font-size:10px;}
                .cet{text-align:center;}
                .checked{font-size:14px;font-family:dejavusans;}
                .pspaces{ font-size:0.1px;}
            </style>
            <p class="tdtext sangria"><u>Ubicación de los puntos de medición.</u></p>
            <p class="tdtext sangria">Los puntos de medición deben seleccionarse en función de las necesidades y características de cada centro de trabajo, de tal manera que describan el entorno ambiental de la iluminación de una forma confiable, considerando: el proceso de producción, la clasificación de las áreas y puestos de trabajo, el nivel de iluminación requerido con base a la Tabla 2 de este apartado, la ubicación de las luminarias respecto a los planos de trabajo, el cálculo del índice de áreas correspondiente a cada una de las áreas, la posición de la maquinaria y equipo, así como los riesgos informados a los trabajadores.</p>
            <table border="1" cellpadding="4" align="center" class="tdtext2_tab">
                <tr class="txt_td">
                    <td width="44%"><p class="pspaces"></p>Tarea Visual del Puesto de Trabajo</td>
                    <td width="40%"><p class="pspaces"></p>Área de Trabajo</td>
                    <td width="16%">Niveles Mínimos de Iluminación (luxes)</td>
                </tr>
                <tr><td>En exteriores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.</td><td>Exteriores generales: patios y estacionamientos.</td>
                    <td>20</td></tr>
                <tr><td>En interiores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.</td><td>Interiores generales: almacenes de poco movimiento, pasillos, escaleras, estacionamientos cubiertos, labores en minas subterráneas, iluminación de emergencia.</td><td>50</td></tr>
                <tr><td>En interiores.</td><td>Áreas de circulación y pasillos; salas de espera; salas de descanso; cuartos de almacén; plataformas; cuartos de calderas</td><td>100</td></tr>
                <tr><td>Requerimiento visual simple: inspección visual, recuento de piezas, trabajo en banco y máquina.</td>
                    <td>Servicios al personal: almacenaje rudo, recepción y despacho, casetas de vigilancia, cuartos de compresores y pailería.</td><td>200</td></tr>
                <tr><td>Distinción moderada de detalles: ensamble simple, trabajo medio en banco y máquina, inspección simple, empaque y trabajos de oficina.</td><td>Talleres: áreas de empaque y ensamble, aulas y oficinas.</td>
                    <td>300</td></tr>
                <tr><td>Distinción clara de detalles: maquinado y acabados delicados, ensamble de inspección moderadamente difícil, captura y procesamiento de información, manejo de instrumentos y equipo de laboratorio.</td><td>Talleres de precisión: salas de cómputo, áreas de dibujo, laboratorios.</td><td>500</td></tr>
                <tr><td>Distinción fina de detalles: maquinado de precisión, ensamble e inspección de trabajos delicados, manejo de instrumentos y equipo de precisión, manejo de piezas pequeñas.</td><td>Talleres de alta precisión: de pintura y acabado de superficies y laboratorios de control de calidad.</td><td>750</td></tr>
                <tr><td>Alta exactitud en la distinción de detalles: ensamble, proceso e inspección de piezas pequeñas y complejas, acabado con pulidos finos.</td><td>Proceso: ensamble e inspección de piezas complejas y acabados con pulidos finos.</td><td>1000</td></tr>
                <tr><td>Alto grado de especialización en la distinción de detalles,</td>
                    <td>Proceso de gran exactitud. Ejecución de tareas visuales:
                        • De bajo contraste y tamaño muy pequeño por periodos prolongados;
                        • Exactas y muy prolongadas, y
                        • Muy especiales de extremadamente bajo contraste y pequeño tamaño.
                        </td>
                    <td>2000</td>
                </tr>
                
            </table>
            <p style="color: rgb(0,32,96); font-size:9px; text-align:center">TABLA 2.</p>
            <p class="tdtext sangria">Las áreas de trabajo se deben dividir en zonas del mismo tamaño, de acuerdo a lo establecido en la columna A (número mínimo de zonas a evaluar) de la Tabla 3 de este apartado, y realizar la medición en el lugar donde haya mayor concentración de trabajadores o en el centro geométrico de cada una de estas zonas; en caso de que los puntos de medición coincidan con los puntos focales de las luminarias, se debe considerar el número de zonas de evaluación de acuerdo a lo establecido en la columna B (número mínimo de zonas a considerar por la limitación) de la Tabla 3 de este apartado. En caso de coincidir nuevamente el centro geométrico de cada zona de evaluación con la ubicación del punto focal de la luminaria, se debe mantener el número de zonas previamente definido.</p>
            <table><tr><td><br></td></tr></table>
            <table cellpadding="3" align="center" class="tableb">
                <thead>
                    <tr class="txt_td">
                        <td><p class="pspaces"></p>Índice de Área</td>
                        <td><p class="pspaces"></p>A) Número de zonas a evaluar</td>
                        <td>B) Número de zonas a considerar por la limitación</td>
                    </tr>
                </thead>
                <tr class="bodycab1"><td>IC < 1</td><td>4</td><td>6</td></tr>
                <tr class="bodycab1"><td>1 < IC < 2</td><td>9</td><td>12</td></tr>
                <tr class="bodycab1"><td>2 < IC < 3</td><td>16</td><td>20</td></tr>
                <tr class="bodycab1"><td>3 < IC</td><td>25</td><td>30</td></tr>
            </table>
            <p class="tdtext sangria">El valor del índice de área, para establecer el número de zonas a evaluar, está dado por la ecuación siguiente:</p>
            <p style="text-align:center"><img src="'.base_url().'public/img/formula1.png" width="90px" ></p>
            <p class="tdtext sangria">Donde: <br>
                    IC = Índice de Área <br>
                    x, y = dimensiones del área (largo y ancho), en metros<br>
                    h = altura de la luminaria respecto al plano de trabajo, en metros </p>
            <p class="tdtext sangria"><u><i>Evaluación de pasillos y escaleras.</i></u></p>

            <p class="tdtext sangria">En pasillos o escaleras, el plano de trabajo por evaluar debe ser un plano horizontal a 75 cm ± 10 cm, sobre el nivel del piso, realizando mediciones en los puntos medios entre luminarias contiguas.</p>
            <p class="tdtext sangria">En el puesto de trabajo se debe realizar al menos una medición en cada plano de trabajo, colocando el luxómetro tan cerca como sea posible del plano de trabajo y tomando precauciones para no proyectar sombras ni reflejar luz adicional sobre el luxómetro.</p>
            <p class="tdtext sangria"><u>Evaluación de reflexión</u></p>
            <p class="tdtext sangria">Los puntos de medición deben ser los mismos ubicados para la medición de iluminación, considerando adicionalmente cualquier superficie que esté dentro del campo visual del trabajador cuyo brillo pueda afectar la capacidad visual en el plano de trabajo evaluado.</p>
            <p class="tdtext sangria">Cálculo del factor de reflexión de las superficies:</p>
            <ol type="a" class="tdtext">
                <li class="txtjust">Se efectúa una primera medición (E1), con la fotocelda del luxómetro colocada de cara a la superficie, a una distancia de 10 cm ± 2 cm, hasta que la lectura permanezca constante; </li>
                <li class="txtjust">La segunda medición (E2), se realiza con la fotocelda orientada en sentido contrario y apoyada en la superficie, con el fin de medir la luz incidente, y </li>
                <li class="txtjust">El factor de reflexión de la superficie (Kf) se determina con la ecuación siguiente:</li>
            </ol>
            <p style="text-align:center"><img src="'.base_url().'public/img/formula2.png" width="90px"  ></p>
            <p></p>
            <p class="tdtext">La evaluación de los puntos presentados en este informe se realizó bajo el(los) siguiente(s) método(s): </p>
            <table cellpadding="5" align="center" class="tableb tdtext11">
                <thead><tr class="txt_td"><td width="25%"><i>Área</i></td><td width="35%"><i>Método de evaluación</i></td>
                        <td width="20%"><i>Con incidencia de la luz natural</i></td><td width="20%"><i>Sin incidencia de la luz natural</i></td></tr></thead>';

                foreach($datosrec->result() as $d){
                    $id_reconocimiento=$d->id;
                }
                $datosdetalle=$this->ModeloCatalogos->getDataReco($id_reconocimiento,1);
                $cont_det=0;
                foreach($datosdetalle as $d){
                    $cont_det++;
                    /*log_message('error', 'area: '.$d->area);
                    log_message('error', 'existe_incidencia: '.$d->existe_incidencia);
                    log_message('error', 'indice_area: '.$d->indice_area);
                    log_message('error', 'puesto_trabajo: '.$d->puesto_trabajo);
                    log_message('error', 'pasillo_escalera: '.$d->pasillo_escalera);*/

                    $chk="---"; $chk_no="---"; $chk2="---"; $chk_no2="---"; $chk3="---"; $chk_no3="---";
                    if(trim(strtoupper($d->existe_incidencia))=="SI" && $d->indice_area=="1"){ $chk='<p style="color:red" class="checked">✔</p>'; }
                    if(trim(strtoupper($d->existe_incidencia))=="NO" && $d->indice_area=="1"){ $chk_no='<p style="color:red" class="checked">✔</p>'; }
                    if(trim(strtoupper($d->existe_incidencia))=="SI" && $d->puesto_trabajo=="1"){ $chk2='<p style="color:red" class="checked">✔</p>'; }
                    if(trim(strtoupper($d->existe_incidencia))=="NO" && $d->puesto_trabajo=="1"){ $chk_no2='<p style="color:red" class="checked">✔</p>'; }
                    if(trim(strtoupper($d->existe_incidencia))=="SI" && $d->pasillo_escalera=="1"){ $chk3='<p style="color:red" class="checked">✔</p>'; }
                    if(trim(strtoupper($d->existe_incidencia))=="NO" && $d->pasillo_escalera=="1"){ $chk_no3='<p style="color:red" class="checked">✔</p>'; }

                    $htmlg.='<tr><td width="25%" style="vertical-align:center" rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>'.$d->area.'</td><td width="35%">Indice de áreas</td><td width="20%">'.$chk.'</td><td width="20%">'.$chk_no.'</td></tr>
                    <tr><td>Puesto de trabajo</td><td>'.$chk2.'</td><td>'.$chk_no2.'</td></tr>
                    <tr><td>Pasillo y/o escalera</td><td>'.$chk3.'</td><td>'.$chk_no3.'</td></tr>';
                    
                }          
            $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
            $htmlg='<style type="text/css">
                .txt_td{background-color:rgb(217,217,217);}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
                .tableb td{border:1px solid #808080;}
            </style>

            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table  cellpadding="5" align="center" class="tableb">
                <tr class="txt_td"><td></td><td><i>Medidor de Iluminancia</i></td></tr>
                <tr><td class="txt_td"><i>Marca</i></td><td>'.$equipo_marca.'</td></tr>
                <tr><td class="txt_td"><i>Modelo</i></td><td>'.$equipo_modelo.'</td></tr>
                <tr><td class="txt_td"><i>Número de Serie</i></td><td>'.$equipo_num_serie.'</td></tr>
                <tr><td class="txt_td"><i>No. de Informe de calibración</i></td><td>'.$equipo_no_informe_calibracion.'</td></tr>
                <tr><td class="txt_td"><i>Fecha de calibración</i></td><td>'.$equipo_fecha_calibracion.'</td></tr>
            </table>';

$pdf->Bookmark('J. Descripción de las condiciones de operación ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
            $htmlg.='<p class="titulosa"><b>J. DESCRIPCIÓN DE LAS CONDICIONES DE OPERACIÓN</b></p>
            <table>';
                $nom_25d=$this->ModeloCatalogos->getPtosFechaGroup(array('idnom'=>$id_nom_ctr,'activo'=>1));
                foreach ($nom_25d as $nd) {
                    $desc_proc=$this->ModeloCatalogos->getselectwheren('condiciones_nom25det',array('id_nom25'=>$id_nom_ctr,"fecha"=>$nd->fecha,'estatus'=>1));
                    foreach ($desc_proc->result() as $d) { //lo trae de la tabla condiciones_nom25det
                        $htmlg.='<tr><td class="tdtext">'.$d->condiciones.' ('.$d->fecha.'). </td></tr>';
                    }
                }
            $htmlg.='</table>';'

                    ';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $htmlg2="";
    //$pdf->AddPage('P', 'A4');
$pdf->Bookmark('K. Descripción del proceso de fabricación del centro de trabajo evaluado -----------------------', 0, 0, '', '', array(0,0,0));
$pdf->Bookmark('L. Descripción de los puestos de trabajo ------------------------------------------------------------------', 0, 0, '', '', array(0,0,0));
    $tipoktext=0;
    $text_info='';
    foreach ($datosnomiv->result() as $item) {
        if($item->text_info!=null or $item->text_info!=''){
            $text_info=$item->text_info;
        }else{
            $tipoktext=1;
            $text_info='Ver anexo IV';
        }
    }
    foreach($datosrec->result() as $d){
        $id_reconocimiento=$d->id;
    }
    $datosdetalle=$this->ModeloCatalogos->getDataReco2($id_reconocimiento,1);
    $htmlg='<style type="text/css">
            .tdtext{text-align: justify;font-size:13px;}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tableb td{border:1px solid #808080; font-size:11px;}
            .cet{text-align:center;}
            .pspaces5{font-size:5px;}
        </style>
        <p class="titulosa"><b>K.  DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</b></p>
        <p>'.$text_info.'</p>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->AddPage('P', 'A4');

        $htmlg='<style type="text/css">
            .tdtext{text-align: justify;font-size:13px;}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tableb td{border:1px solid #808080; font-size:11px;}
            .cet{text-align:center;}
            .pspaces5{font-size:5px;}
        </style>
        <p class="titulosa"><b>L.  DESCRIPCIÓN DE LOS PUESTOS DE TRABAJO</b></p>
        <table cellpadding="5" align="center" class="tableb">
            <thead><tr class="txt_td"><td width="30%"><div class="pspaces5">&nbsp;</div><i>Área</i></td><td width="30%"><div class="pspaces5">&nbsp;</div><i>Puesto de Trabajo</i></td><td width="40%"><i>Actividad(es) conforme a la tabla 1 de la '.$norma_name.'</i></td></tr>
            </thead><tbody>';
            
            $cont_td=0;
            
            foreach ($datosdetalle as $p2) {
                $cont_td++;
                //if($p2->actividades!=""){
                    $htmlg.='<tr><td width="30%">'.$p2->area.'</td><td width="30%">'.$p2->puesto.'</td><td width="40%"><!--'.$p2->identificacion.'--> '.$p2->puesto_trabajo.'<!--'.$p2->actividades.'--></td>
                    </tr>';
                //} 
                $htmlg2.='<tr><td>'.$p2->area.'</td><td>'.$p2->puesto.'</td><td>'.$p2->num_trabaja.'</td></tr>';
            }
            $htmlg.='<tbody></table>';
            $pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->Bookmark('M. Número de trabajadores por área y puesto de trabajo -------------------------------------------', 0, 0, '', '', array(0,0,0));
            //log_message('error', 'cont_td: '.$cont_td);
            if($cont_td>=14){
                $pdf->AddPage('P', 'A4');
                $htmlg='<style type="text/css">
                    .txt_td{background-color:rgb(217,217,217);}
                    .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                    .tableb td{border:1px solid #808080; font-size:11px;}
                    .cet{text-align:center;}
                </style>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <thead><tr nobr="true" class="txt_td"><td width="40%"><i>Área</i></td><td width="40%"><i>Puesto de Trabajo</i></td><td width="20%"><i>No. de Trabajadores</i></td></tr></thead>';
                    foreach ($datosdetalle as $p2) {
                        
                        $htmlg.='<tr nobr="true"><td width="40%">'.$p2->area.'</td><td width="40%">'.$p2->puesto.'</td><td width="20%">'.$p2->num_trabaja.'</td></tr>';
                    }
                $htmlg.='</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');  
            }
            else{
                $htmlg='<style type="text/css">
                    .txt_td{background-color:rgb(217,217,217);}
                    .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                    .tableb td{border:1px solid #808080; font-size:11px;}
                    .cet{text-align:center;}
                </style>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <thead><tr nobr="true" class="txt_td"><td width="40%"><i>Área</i></td><td width="40%"><i>Puesto de Trabajo</i></td><td width="20%"><i>No. de Trabajadores</i></td></tr></thead>';
                    foreach ($datosdetalle as $p2) {
                        
                        $htmlg.='<tr nobr="true"><td width="40%">'.$p2->area.'</td><td width="40%">'.$p2->puesto.'</td><td width="20%">'.$p2->num_trabaja.'</td></tr>';
                    }
                $htmlg.='</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');   
            }
    //$pdf->AddPage('P', 'A4');
$pdf->Bookmark('N. Criterios utilizados para seleccionar el método de evaluación -----------------------------------', 0, 0, '', '', array(0,0,0)); 
$pdf->Bookmark('Ñ. Vigencia del informe ----------------------------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
    $htmlg='<style type="text/css">
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                .tdtext{text-align: justify;font-size:14px;}
            </style>
            <p class="titulosa"><b>N. CRITERIOS UTILIZADOS PARA SELECCIONAR EL MÉTODO DE EVALUACIÓN</b></p>
            <table><tr><td class="tdtext">'.$txt_criterios_metodo.'</td></tr></table>
            <p class="titulosa"><b>Ñ. VIGENCIA DEL INFORME</b></p>
            <p class="tdtext">La vigencia de este informe de resultados se conservará siempre y cuando, se mantengan las condiciones que dieron origen al resultado de la evaluación.</p>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->AddPage('P', 'A4');
    //'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0,0,0)); 
    $htmlg='
            <table border="0" >
                <tr><td height="280px"></td></tr>
                <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr>
                <tr><td height="50px"></td></tr>
                <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr>
            </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->setPrintHeader(false);
    $pdf->SetMargins(15, '10', 15);

    $htmlg='';
    if($datosrec->num_rows()>0){
        $datosrecr=$datosrec->result();
        $datosrecr=$datosrecr[0];
        $pdf->AddPage('P', 'A4');
        if($datosrecr->no==='1'){
            //$cuales_areas='--------------------------------------------------------------------------------------------';
            $cuales_areas='---';
        }else{
            $cuales_areas=$datosrecr->cuales_areas;
        }

        if($datosrecr->pero_turno==1){
            //$pero_turno='<img src="'.base_url().'public/img/check.png" width="15px" >✔';
            $pero_turno='<p class="checked">✔</p>';
            $class_act1="";
        }else{
            $pero_turno='---';
            $class_act1="backg";
        }
        if($datosrecr->sdo_turno==1){
            $sdo_turno='<p class="checked">✔</p>';
            $class_act2="";
        }else{
            $sdo_turno='---';
            $class_act2="backg";
        }
        if($datosrecr->tero_turno==1){
            $tero_turno='<p class="checked">✔</p>';
            $class_act3="";
        }else{
            $tero_turno='---';
            $class_act3="backg";
        }
        if($datosrecr->otro_turno==1){
            $otro_turno='<p class="checked">✔</p>';
            $class_act4="";
        }else{
            $otro_turno='---';
            $class_act4="backg";
        }
        if($datosrecr->adm_turno==1){
            $adm_turno='<p class="checked">✔</p>';
            $class_act5="";
        }else{
            $adm_turno='---';
            $class_act5="backg";
        }
        if($datosrecr->si==1){
            $datosrecrsi='<p class="checked">✔</p>';
        }else{
            $datosrecrsi='---';
        }
        if($datosrecr->no==1){
            $datosrecrno='<p class="checked">✔</p>';
        }else{
            $datosrecrno='---';
        }

        if($datosrecr->justifica_horario!=""){
            $justifica_horario=$datosrecr->justifica_horario;
        }else{
            $justifica_horario='---';
        }
        if($datosrecr->desc_proceso!=""){
            $desc_proceso=$datosrecr->desc_proceso;
        }else{
            $desc_proceso='---';
        }
        if($datosrecr->plan_mante!=""){
            $plan_mante=$datosrecr->plan_mante;
        }else{
            $plan_mante=='---';
        }

        if($datosrecr->priero!="" && $datosrecr->priero>0){
            $priero=$datosrecr->priero;
        }else{
            $priero='---';
        }if($datosrecr->segdo!="" && $datosrecr->segdo>0){
            $segdo=$datosrecr->segdo;
        }else{
            $segdo='---';
        }if($datosrecr->tercero!="" && $datosrecr->tercero>0){
            $tercero=$datosrecr->tercero;
        }else{
            $tercero='---';
        }if($datosrecr->hora_admin!="" && $datosrecr->hora_admin>0){
            $hora_admin=$datosrecr->hora_admin;
        }else{
            $hora_admin='---';
        }if($datosrecr->hora_otro!="" && $datosrecr->hora_otro>0){
            $hora_otro=$datosrecr->hora_otro;
        }else{
            $hora_otro='---';
        }
        
        $htmlg='<style type="text/css">
                    .borderbottom{border-bottom:1px solid black;}
                    .backg{background-color:rgb(217,217,217);}
                    .checked{font-size:14px;font-family:dejavusans;}
                    .pspaces{ font-size:0.2px;}
                </style>
                <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                    <tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg"></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr>
                    <tr><td>RECONOCIMIENTO INICIAL PARA ILUMINACIÓN</td><td>REG-TEC/05-01</td><td>02</td><td>ORIGINAL</td></tr>
                </table>
                <table>
                    <tr><td></td></tr>
                </table>
                <table border="0" cellpadding="5" align="center" style="font-size:9px;">
                    <tr><td width="20%"><b>No. DE INFORME</b></td><td width="50%" class="borderbottom">'.$datosrecr->num_informe_rec.'</td><td width="10%"><b>Fecha:</b></td><td width="20%" class="borderbottom">'.$datosrecr->fecha.'</td></tr>
                    <tr><td width="20%"><b>RAZÓN SOCIAL</b></td><td width="80%" class="borderbottom">'.$datosrecr->cliente.'</td></tr>
                    <tr><td width="60%" class="borderbottom">'.$datosrecr->calle_num.'</td>                        <td width="40%" class="borderbottom">'.$datosrecr->colonia.'</td></tr>
                    <tr><td width="60%" ><b>CALLE Y NÚMERO</b></td><td width="40%" ><b>COLONIA</b></td></tr>
                    <tr><td width="34%" class="borderbottom">'.$datosrecr->poblacion.'</td><td width="33%" class="borderbottom">'.ucfirst(strtolower($datosrecr->estado)).'</td><td width="33%" class="borderbottom">'.$datosrecr->cp.'</td></tr>
                    <tr><td width="34%" ><b>MUNICIPIO</b></td><td width="33%" ><b>ESTADO</b></td>                        <td width="33%" ><b>CÓDIGO POSTAL</b></td></tr>
                    <tr><td width="50%" class="borderbottom">'.$datosrecr->rfc.'</td><td width="50%" class="borderbottom">'.$datosrecr->giro.'</td></tr>
                    <tr><td width="50%" ><b>RFC</b></td><td width="50%" ><b>GIRO DE LA EMPRESA</b></td></tr>
                    <tr><td width="50%" class="borderbottom">'.$datosrecr->telefono.'</td><td width="50%" class="borderbottom">'.$datosrecr->representa.'</td></tr>
                    <tr><td width="50%" ><b>NÚMERO TELEFÓNICO</b></td><td width="50%" ><b>REPRESENTANTE LEGAL</b></td></tr>
                    <tr><td width="100%" class="borderbottom">'.$datosrecr->nom_cargo.'</td></tr>
                    <tr><td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td></tr>
                </table>
                <table><tr><td><br></td></tr></table>
                <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                    <tr><td  colspan="6" class="backg"><b>HORARIO DE TRABAJO</b></td></tr>
                    <tr><td width="10%" class="backg"><b>1ero</b></td><td width="24%">'.$priero.'</td><td width="10%" class="backg"><b>2do</b></td><td width="23%">'.$segdo.'</td><td width="10%" class="backg"><b>3ero</b></td><td width="23%">'.$tercero.'</td></tr>
                    <tr><td width="16%" class="backg"><b>ADMINISTRATIVO</b></td><td width="37%">'.$hora_admin.'</td><td width="10%" class="backg"><b>OTRO</b></td><td width="37%">'.$hora_otro.'</td>                        
                    </tr>
                </table>
                <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                    <tr><td width="50%" class="backg"><b>SELECCIONAR EL TURNO DONDE SE REALIZA LA MEDICIÓN</b></td><td width="5%" class="backg"><b>1ero</b></td><td width="5%" >'.$pero_turno.'</td><td width="5%" class="backg"><b>2do</b></td><td width="5%" >'.$sdo_turno.'</td><td width="5%" class="backg"><b>3ero</b></td><td width="5%" >'.$tero_turno.'</td><td width="5%" class="backg"><b>OTRO</b></td><td width="5%" >'.$otro_turno.'</td><td width="5%" class="backg"><b>Adm</b></td><td width="5%" >'.$adm_turno.'</td></tr>
                    <tr><td colspan="13" class="backg"><b>JUSTIFICACIÓN DEL HORARIO EN EL QUE SE REALIZÁN LAS MEDICIONES</b></td></tr>
                    <tr><td colspan="11" height="90px">'.$justifica_horario.'</td></tr>
                    <tr><td colspan="11" class="backg"><b>DESCRIPCIÓN DEL PROCESO PRODUCTIVO</b></td></tr>
                    <tr><td colspan="11" height="90px">'.$desc_proceso.'</td></tr>
                    <!--<tr><td colspan="11" class="backg"><b>PERCEPCIÓN DE LAS CONDICIONES DE ILUMINACIÓN POR PARTE DEL TRABAJADOR</b></td></tr>
                    <tr><td colspan="11" height="50px">'.$datosrecr->percep_condi.'</td></tr>-->
                    <tr><td colspan="11" class="backg"><b>PLAN DE MANTENIMIENTO DE LUMINARIAS</b></td></tr>
                    <tr><td colspan="11" height="50px">'.$plan_mante.'</td></tr>
                    
                </table>
                <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                    <tr class="backg"><td width="80%" rowspan="2"><b>¿EXISTE ÁREAS CUYAS LUMINARIAS SEAN NUEVAS Y TENGAN UN PERIODO DE OPERACIÓN MENOR A 100 HORAS?</b></td><td width="10%"><b>SI</b></td><td width="10%"><b>NO</b></td></tr>
                    <tr><td>'.$datosrecrsi.'</td><td>'.$datosrecrno.'</td></tr>
                </table>
                <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                    <tr><td width="30%" class="backg"><b>EN CASO AFIRMATIVO, MENCIONAR CUALES SON ESAS ÁREAS</b></td>
                        <td width="70%">'.$cuales_areas.'</td></tr>                    
                </table>
                ';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
    }
    $pdf->AddPage('P', 'A4');

    $htmlg='<style type="text/css"> .backg{ background-color:rgb(217,217,217); } .pspaces{ font-size:0.2px; } </style>';
    $htmlg.='<table border="1" cellpadding="5" align="center" style="font-size:9px;">
                <tr><td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td><td width="19%">NOMBRE DEL DOCUMENTO</td><td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td><td width="19%">VERSIÓN</td><td width="19%">No COPIA CONTROLADA</td></tr>
                <tr><td>RECONOCIMIENTO INICIAL PARA ILUMINACIÓN</td><td>REG-TEC/05-01</td><td>02</td><td>ORIGINAL</td>                </tr>
            </table>
            <table><tr><td style="height:30px;"></td></tr></table>
            <table border="1" cellpadding="5" align="center" style="font-size:9px;">
                <tr class="backg"><td width="14%" >Identificación interna para la identificación de la tarea visual y/o área del trabajo</td><td width="38%" ><p class="pspaces"></p>Tarea Visual del Puesto de Trabajo</td><td width="38%" ><p class="pspaces"></p>Área de Trabajo</td><td width="10%" >Niveles Mínimos de Iluminación (luxes)</td>
                </tr>
                <tr><td>N1</td><td>En exteriores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.</td><td>Exteriores generales: patios y estacionamientos.</td>
                    <td>20</td></tr>
                <tr><td>N2</td><td>En interiores: distinguir el área de tránsito, desplazarse caminando, vigilancia, movimiento de vehículos.</td><td>Interiores generales: almacenes de poco movimiento, pasillos, escaleras, estacionamientos cubiertos, labores en minas subterráneas, iluminación de emergencia.</td>
                    <td>50</td></tr>
                <tr><td>N3</td><td>En interiores.</td><td>Áreas de circulación y pasillos; salas de espera; salas de descanso; cuartos de almacén; plataformas; cuartos de calderas</td><td>100</td></tr>
                <tr><td>N4</td><td>Requerimiento visual simple: inspección visual, recuento de piezas, trabajo en banco y máquina.</td><td>Servicios al personal: almacenaje rudo, recepción y despacho, casetas de vigilancia, cuartos de compresores y pailería.</td><td>200</td></tr>
                <tr><td>N5</td><td>Distinción moderada de detalles: ensamble simple, trabajo medio en banco y máquina, inspección simple, empaque y trabajos de oficina.</td><td>Talleres: áreas de empaque y ensamble, aulas y oficinas.</td><td>300</td></tr>
                <tr><td>N6</td><td>Distinción clara de detalles: maquinado y acabados delicados, ensamble de inspección moderadamente difícil, captura y procesamiento de información, manejo de instrumentos y equipo de laboratorio.</td><td>Talleres de precisión: salas de cómputo, áreas de dibujo, laboratorios.</td><td>500</td></tr>
                <tr><td>N7</td><td>Distinción fina de detalles: maquinado de precisión, ensamble e inspección de trabajos delicados, manejo de instrumentos y equipo de precisión, manejo de piezas pequeñas.</td><td>Talleres de alta precisión: de pintura y acabado de superficies y laboratorios de control de calidad.</td><td>750</td></tr>
                <tr><td>N8</td><td>Alta exactitud en la distinción de detalles: ensamble, proceso e inspección de piezas pequeñas y complejas, acabado con pulidos finos.</td><td>Proceso: ensamble e inspección de piezas complejas y acabados con pulidos finos.</td><td>1000</td></tr>
                <tr><td>N9</td><td>Alto grado de especialización en la distinción de detalles,</td><td>Proceso de gran exactitud. Ejecución de tareas visuales:
                        • De bajo contraste y tamaño muy pequeño por periodos prolongados;
                        • Exactas y muy prolongadas, y
                        • Muy especiales de extremadamente bajo contraste y pequeño tamaño.
                    </td><td>2000</td></tr>           
            </table>
            <table border="0" cellpadding="5" align="center" style="font-size:9px;">
                <tr><td colspan="2">NIVELES DE ILUMINACIÓN PARA TAREAS VISUALES Y ÁREAS DE TRABAJO</td></tr>
                <tr><td colspan="2"></td></tr>
                <tr><td>
                        <table border="0" cellpadding="5" align="center"><tr><td><img src="'.base_url().'public/img/formula1.png" ></td></tr><tr><td>IC = Índice de Área[adimensional]</td></tr><tr><td>x = Largo del área de trabajo[m]</td></tr><tr><td>y = Ancho del área de trabajo[m]</td></tr><tr><td>h = altura de la luminaria respecto al plano de trabajo[m]</td></tr></table>
                    </td>
                    <td>
                        <table border="1" cellpadding="5" align="center">
                            <tr class="backg">
                                <td><p class="pspaces"></p>Índice de Área</td>
                                <td><p class="pspaces"></p>A) Número de zonas a evaluar</td>
                                <td>B) Número de zonas a considerar por la limitación</td>
                            </tr>
                            <tr><td>IC < 1</td><td>4</td><td>6</td></tr><tr><td>1 < IC < 2</td><td>9</td><td>12</td></tr><tr><td>2 < IC < 3</td><td>16</td><td>20</td></tr><tr><td>3 < IC</td><td>25</td><td>30</td></tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2">DETERMINACIÓN DEL ÍNDICE DE ÁREA</td></tr>
            </table>
            ';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->AddPage('P','A4','','','off');

$html0='';
$html0.='<style type="text/css">
    .artext{font-size:35px; color: rgb(0,57,88);font-family: Calibri;font-size:14px;font-style:italic;}
</style>';
$html0.='
        <table border="0" align="center" class="artext">
            <tr><td height="160px">ESTA ES LA HOJA 0 DE LA PORTADA DE LA CARPETA</td></tr>
        </table>';
$pdf->writeHTML($html0, true, false, true, false, '');
$pdf->Close();

$pdf->Output('Entregable.pdf', 'I');

?>