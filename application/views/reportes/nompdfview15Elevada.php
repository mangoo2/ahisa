<?php
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');
//log_message('error','Nom15 elevada');
$norma_name = 'NOM-015-STPS-2001';

setlocale(LC_ALL, 'spanish-mexican');
setlocale(LC_ALL, 'es_MX');
mb_internal_encoding("UTF-8");
$date = new Datetime($fecha);
$fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

$dateh = new Datetime(date('Y-m-d'));
$fechah = strftime("%B %d, %Y", $dateh->getTimestamp());
$fechah = ucfirst($fechah);

$firmaemple = "";
$cedula = "";
$firmaresp = "";
$tecnico = "";
foreach ($datosoc->result() as $item) {
    $tecnico = $item->tecnico;
    $firmaemple = $item->firmaemple;
    $cedula = $item->cedula;
    $firmaresp = $item->firmaresp;
}
$GLOBALS['nom_cargo'] = "";
$GLOBALS['id_rec'] = "";
foreach ($datosrec->result() as $item) {
    $GLOBALS['nom_cargo'] = $item->nom_cargo;
    $GLOBALS['id_rec'] = $item->id;
    $empresa = $item->cliente;
    $estado_name = utf8_decode($estado_name);
    $direccion = mb_strtoupper($item->calle_num . ', <br> ' . ($item->colonia) . ', <br>' . ($item->poblacion) . ', ' . $estado_name . ',<br> C.P. ' . $item->cp . '', "UTF-8");
    $direccion2 = mb_strtoupper($item->calle_num . ', ' . ($item->colonia) . ', ' . ($item->poblacion) . ', ' . $estado_name . ',<br> C.P. ' . $item->cp . '', "UTF-8");
    $rfccli = $item->rfc;
    $girocli = $item->giro;
    $representacli = $item->representa;
    $telefonocli = $item->telefono;
    if ($folio == "" || $folio == null) {
        $folio = $item->num_informe_rec;
    }
}

$GLOBALS['folio'] = $folio;
$nom_cargo = "";
$cargo_cargo = "";
//$GLOBALS['nom_cargo']; 

$n_cargo = explode("/", $GLOBALS['nom_cargo']);
if (isset($n_cargo[0])) {
    $nom_cargo = $n_cargo[0];
}
if (isset($n_cargo[1])) {
    $cargo_cargo = $n_cargo[1];
}
//======================================================================================= 
class MYPDF extends TCPDF
{
    //Page header 
    public function Header()
    {
        $logos = base_url() . 'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
    }
    // Page footer 
    public function Footer()
    {
        $html = ' 
        <style type="text/css"> 
            .cmac{font-size:9px; background-color: rgb(0,57,88); font-weight: bold; color:white;} 
            .cma{font-size:15px; color:rgb(231,99,0);} 
            .cmafolio{font-size:9px; background-color: rgb(231,99,0); font-weight: bold; margin-top:10px;} 
            .footerpage{font-size:9px;font-style: italic;} 
            table{text-align:center} 
        </style>  
      <table width="100%" cellpadding="2"> 
        <tr><td colspan="2" class="footerpage">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr> 
        <tr> 
            <td class="cmac" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx 
            </td> 
            <td class="cmafolio" width="16%"><span class="cma">•</span>' . $GLOBALS['folio'] . '</td> 
        </tr> 
        <tr><td colspan="2" class="footerpage" >Página ' . $this->getAliasNumPage() . ' de ' . $this->getAliasNbPages() . ' del informe de resultados ' . $GLOBALS['folio'] . ' </td> 
        </tr> 
      </table>';
        //$this->writeHTML($html, true, false, true, false, ''); 
        $this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311, 396), true, 'UTF-8', false);
// set document information 
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-015');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-015');
$pdf->setPrintFooter(true);
// set default header data 
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts 
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font 
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins 
$pdf->SetMargins(20, 25, 20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30); 
$pdf->SetFooterMargin(26);

// set auto page breaks 
$pdf->SetAutoPageBreak(true, 27);
//$pdf->SetAutoPageBreak(true, 25); 

// set image scale factor 
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page 
$pdf->AddPage('P', 'A4');
//$pdf->AddPage('P', 'LETTER'); 
$htmlg = '';
$htmlg .= '<style> 
    td{font-family:calibri;} 
    .tdtext{text-align: justify;font-size:15px;font-family: Calibri;} 
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;} 
    .sangria{text-indent: 45px;} 
    .fontbold{font-weight: bold;} 
    .algc{text-align:center; } 
    .ft09{font-size:9px;} 
    .ft20{font-size:20px;} 
    .ft18{font-size:18px;} 
    .ft16{font-size:16px;} 
    .ft14{font-size:14px;} 
    .ft13{font-size:13px;} 
    .ft12{font-size:12px;} 
    .ft11{font-size:11px;} 
</style>';
$htmlg .= ' 
        <table border="0"> 
            <tr > 
                <td colspan="2" align="right" class="ft20 fontbold">' . $empresa . '</td> 
            </tr> 
            <tr><td colspan="2"></td></tr> 
            <tr> 
                <td width="50%" ></td> 
                <td width="50%" align="right" class="ft14 fontbold">' . $direccion . '</td> 
            </tr> 
        </table> 
        <table border="0"> 
            <tr><td></td></tr> 
             
            <tr> 
                <td class="fontbold ft16"><b>' . $nom_cargo . '</b></td> 
            </tr> 
            <tr> 
                <td class="fontbold ft13"><b>' . $cargo_cargo . '</b></td> 
            </tr> 
        </table> 
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes a los niveles de condiciones térmicas elevadas determinados en su centro de trabajo; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana ' . $norma_name . '.</p> 
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>' . $fecha . '</u>.</p> 
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p> 
        <table border="0" align="center" class="artext"> 
            <tr><td height="180px" colspan="2"></td></tr> 
            <tr> 
                <td>Ing. Christian Uriel Fabian Romero</td> 
                <td>Ing. Tanya Alejandra Valle Tello</td> 
            </tr> 
            <tr> 
                <td>Aprobó - Gerente General</td> 
                <td>Revisó – Procesamiento de Información</td> 
            </tr> 
            <tr><td colspan="2" height="170px"></td></tr> 
            <tr> 
                <td colspan="2"><img src="' . base_url() . 'public/img/logo_ema.jpg" width="180px"></td> 
            </tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg: '.$htmlg);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));

$tr_ptps = "";
$tr_ptps_det = "";
$txt_conclus = "";
$get_ptos = $this->ModeloCatalogos->getselectwheren('nom15_punto_elevada', array('id_nom' => $idnom, 'activo' => 1));
$ct = 0;
foreach ($get_ptos->result() as $p) {
    //$dosis[$p->num]=$p->dosis;                                                    

    //inciso A. 
    $tr_ptps .= '<tr> 
                <td>' . $p->num_punto . '</td> 
                <td>' . $p->area . '</td> 
                <td>' . $p->num_ciclos . '</td> 
            </tr>';

    $txt_tm = "";
    if ($p->calc_prom_ini > 31.1 || $p->calc_prom_fin > 31.1) {
        $txt_tm = "El I<sub>tgbh</sub> promedio supera la TM";
        $sn = "";
        $class = "color:red";
    } else {
        $txt_tm = "El I<sub>tgbh</sub> promedio no supera la TM";
        $sn = "no";
        $class = "color:green";
    }
    $tr_ptps_det .= ' 
            <tr> 
                <td rowspan="2"><p class="pspaces"></p><p class="pspaces"></p>' . $p->num_punto . '</td> 
                <td rowspan="2"><p class="pspaces"></p><p class="pspaces"></p>' . $p->regimen . '</td> 
                <td>Inicial</td> 
                <td>' . $p->temp_aux_ini . '</td> 
                <td>' . $p->calc_prom_ini . '</td> 
                <td rowspan="2"><p class="pspaces"></p>' . $p->porc_expo . '% de exposición <br>' . $p->porc_no_expo . '% de recuperación en cada hora</td> 
                <td rowspan="2"><p class="pspaces"></p><p class="pspaces"></p>31.1</td> 
                <td style="' . $class . '">' . $txt_tm . '</td> 
            </tr>   
            <tr> 
                <td>Final</td> 
                <td>' . $p->temp_aux_fin . '</td> 
                <td>' . $p->calc_prom_fin . '</td> 
                <td style="' . $class . '">' . $txt_tm . '</td> 
            </tr>';

    //inciso B. 
    $ct++;
    $txt_conclus .= '<p class="tdtext sangria">' . $ct . '. El punto evaluado e identificado como número ' . $p->num_punto . ', <b>' . $sn . ' supera</b> la temperatura máxima del ITGBH establecido para un régimen de trabajo moderado y con porcentaje de tiempo de exposición y de no exposición de ' . $p->porc_expo . '% y ' . $p->porc_no_expo . '% respectivamente para la medición inicial del ciclo. </p>';

    if($p->chart_inicial == ''){
        $txt_conclus .='<table><tr><td></td></tr></table>';    
    }else{
        $txt_conclus .= '<table><tr><td><img width="520px" src="' . base_url() . 'public/charts/' . $p->chart_inicial . '"></td></tr></table>';
    }


    if ($p->chart_mitad != "") {
        $ct++;
        $txt_conclus .= '<p class="tdtext sangria">' . $ct . '. El punto evaluado e identificado como número ' . $p->num_punto . ', <b>' . $sn . ' supera</b> la temperatura máxima del ITGBH establecido para un régimen de trabajo moderado y con porcentaje de tiempo de exposición y de no exposición de ' . $p->porc_expo . '% y ' . $p->porc_no_expo . '% respectivamente para la medición inicial del ciclo. </p>';
        $txt_conclus .= '<table><tr><td><img width="520px" src="' . base_url() . 'public/charts/' . $p->chart_mitad . '"></td></tr></table>';
    }else{
        $ct++;
        $txt_conclus .= '<p class="tdtext sangria">' . $ct . '. El punto evaluado e identificado como número ' . $p->num_punto . ', <b>' . $sn . ' supera</b> la temperatura máxima del ITGBH establecido para un régimen de trabajo moderado y con porcentaje de tiempo de exposición y de no exposición de ' . $p->porc_expo . '% y ' . $p->porc_no_expo . '% respectivamente para la medición inicial del ciclo. </p>';
        $txt_conclus .= '<table><tr><td></td></tr></table>';
    }

    $ct++;
    $txt_conclus .= '<p class="tdtext sangria">' . $ct . '. El punto evaluado e identificado como número ' . $p->num_punto . ', <b>' . $sn . ' supera</b> la temperatura máxima del ITGBH establecido para un régimen de trabajo moderado y con porcentaje de tiempo de exposición y de no exposición de ' . $p->porc_expo . '% y ' . $p->porc_no_expo . '% respectivamente para la medición inicial del ciclo. </p>';

    if($p->chart_final == ''){
        $txt_conclus .='<table><tr><td></td></tr></table>';    
    }else{
        $txt_conclus .= '<table><tr><td><img width="520px" src="' . base_url() . 'public/charts/' . $p->chart_final . '"></td></tr></table>';
    }
    
}

$htmlg = ' 
    <style > 
        td{font-family:calibri;} 
        .tdtext{font-family:Arial;text-align: justify;font-size:14px;} 
        .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;} 
        .tdtext1{font-family:Arial;text-align: center;font-size:11px;} 
        .algc{text-align:center;} 
        .ft09{font-size:9px;} 
        .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:11px;font-size:9px;valign: middle;vertical-align: middle;} 
        .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:10px;} 
        .pspaces{font-size:0.2px;} 
        .txt_td{background-color:rgb(217,217,217);} 
        .sangria{text-indent: 45px;} 
        b{font-family:Arialb;} 
        .title_tb{ color: #548235; font-weight: bold;}  
        .tgr{color:green;} 
        .tred{color:red;} 
    </style> 
    <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p> 
    <p class="tdtext">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>' . $empresa . '</b>, el día ' . $fecha . '.</p> 
    <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p> 
 
    <p class="tdtext1"><i>• Condiciones Térmicas Elevadas</i></p>';

$htmlg .= '<table class="tableb" cellpadding="5">                      
                <tr> 
                    <th width="20%">No. de Punto</th> 
                    <th width="60%">Identificación del punto de medición (área / identificación)</th> 
                    <th width="20%">Ciclos en una hora</th> 
                </tr> 
                ' . $tr_ptps . ' 
            </table> 
            <table>                      
                <tr> 
                    <th></th> 
                </tr> 
            </table>';

$htmlg .= '<table class="tableb" cellpadding="5">                      
                <tr> 
                    <th width="7%"><p class="pspaces"></p>No. de <br>Punto</th> 
                    <th width="10%"><p class="pspaces"></p>Régimen de trabajo</th> 
                    <th width="10%"><p class="pspaces"></p>Medición</th> 
                    <th width="8%"><p class="pspaces"></p>T.A. <br>del ciclo <br>(°C)</th> 
                    <th width="8%"><p class="pspaces"></p>ITGBH <br>promedio (°C)</th> 
                    <th width="27%"><p class="pspaces"></p>% del tiempo de exposición y de no exposición</th> 
                    <th width="10%"><p class="pspaces"></p>TM <br>del ITGBH<br> (°C)</th> 
                    <th width="20%"><p class="pspaces"></p><p class="pspaces"></p>Conclusión</th> 
                </tr> 
                ' . $tr_ptps_det . ' 
            ';

$htmlg .= '</table><p class="ft09 algc">ITGBH = Índice de temperatura de globo bulbo húmedo<br> 
                        U exp = Incertidumbre expandida (K=2, confianza del 95.45%)<br> 
                        TM = Temperatura Máxima en °C del ITGBH<br> 
                         T.A.= Temperatura Axilar 
                        </p>';
$htmlg .= ' 
            <p class="ft09 algc"><u><i>Regla de Decisión</i></u></p> 
            <p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg2: '.$htmlg);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$htmlgtab = "";
$htmlg = "";
$htmlg = '<style type="text/css"> 
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;} 
            .sangria{text-indent: 45px;} 
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;} 
        </style> 
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p> 
        <p class="tdtext">Derivado de los resultados obtenidos se presentan las siguientes conclusiones: </p> 
 
        ' . $txt_conclus . ' ';
$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg3: '.$htmlg);

$htmlg = "";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$htmlg = '<style type="text/css"> 
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;} 
            .tdtext{font-family:Arial;text-align: justify;font-size:15px;} 
             
        </style> 
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p> 
        <p class="tdtext">La higiene Industrial es un sistema de principios y reglas dedicados al reconocimiento, evaluación y control de factores del ambiente o tensionales de riesgo, que provienen del trabajo y que pueden causar enfermedades o deteriorar la salud.</p> 
        <p class="tdtext">Dentro de los principales factores de riesgo ambiental, se encuentra el efecto fisiológico del calor sobre los trabajadores, la tensión y el riesgo originado por el calor debido a las condiciones térmicas, dependen del efecto combinado de la temperatura ambiente, la humedad, la velocidad del aire y la radiación, así como también del esfuerzo físico, del vestuario y de las características propias del trabajador.</p> 
 
        <p class="tdtext">Un ambiente frío se define por unas condiciones que causan pérdidas de calor corporal mayores de lo normal. En este contexto, "normal" se refiere a lo que una persona experimenta en la vida diaria en condiciones térmicas neutras, normalmente en interiores, aunque es un concepto que puede variar en función de factores sociales, económicos o climáticos.</p> 
        <p class="tdtext">Una condición térmica extrema es la situación ambiental, originadas por una temperatura elevada o abatida, capaz de permitir una ganancia (superior a 38° C) o pérdida (inferior a 36° C) de calor en el cuerpo humano, de tal manera que modifique el equilibrio térmico del trabajador y que ocasione un incremento o decremento en su temperatura corporal central, capaz de alterar su salud.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg4: '.$htmlg);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado -----------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));

$htmlg = '<style type="text/css"> 
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} 
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;} 
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;} 
            .tableb td,.tableb th{border:1px solid #808080;} 
            .sp_bck{ font-weight:bold;} 
            .pspaces{ font-size:0.2px; } 
            .tableb tr th{background-color:rgb(217,217,217);} 
        </style> 
        <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p> 
        <p class="">Determinar la temperatura (ITGBH) de los puestos de trabajo del centro de trabajo <span class="sp_bck"><b>' . $empresa . ',</b></span> y compararlos contra los límites máximos permisibles. 
        </p> 
        <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p> 
        <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la ' . $norma_name . ' "Condiciones térmicas elevadas o abatidas – Condiciones de seguridad e higiene".</p> 
 
        <p class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO* </b></p> 
        <table cellpadding="4" class="tdtext1 tableb"> 
            <tr> 
                <th width="41%"><i>a)  Nombre, denominación o razón social</i></th> 
                <td width="59%">' . $empresa . '</td> 
            </tr> 
            <tr> 
                <th><p class="pspaces"></p><i>b)  Domicilio</i></th> 
                <td>' . $direccion2 . '</td> 
            </tr> 
            <tr> 
                <th><i>c)  R.F.C.</i></th> 
                <td>' . $rfccli . '</td> 
            </tr> 
            <tr> 
                <th><i>d)  Giro y/o actividad principal</i></th> 
                <td>' . $girocli . '</td> 
            </tr> 
            <tr> 
                <th><i>e)  Responsable de la empresa</i></th> 
                <td>' . $representacli . '</td> 
            </tr> 
            <tr> 
                <th><i>f)  Teléfono</i></th> 
                <td> ' . $telefonocli . '</td> 
            </tr> 
        </table> 
        <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr></table> 
        <p class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></p> 
        <table cellpadding="4" class="tdtext1 tableb"> 
            <tr> 
                <th width="41%"><i>a) Nombre, denominación o razón social</i></th> 
                <td width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td> 
            </tr> 
            <tr> 
                <th><i>b)  Domicilio</i></th> 
                <td>19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td> 
            </tr> 
            <tr> 
                <th><i>c)  R.F.C.</i></th> 
                <td>ALP160621FD6</td> 
            </tr> 
            <tr> 
                <th><i>d) Teléfono</i></th> 
                <td>(01) 222 2265395</td> 
            </tr> 
            <tr> 
                <th><i>e) e-mail </i></th> 
                <td>gerencia@ahisa.mx</td> 
            </tr> 
            <tr> 
                <th><i>f) Número de acreditación </i></th> 
                <td>AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td> 
            </tr> 
            <tr> 
                <th><i>g) Número de aprobación</i></th> 
                <td>LPSTPS-153/2022</td> 
            </tr> 
            <tr> 
                <th><i>h) Lugar de expedición del informe </i></th> 
                <td>Puebla, Puebla - México </td> 
            </tr> 
            <tr> 
                <th><i>i) Fecha de expedición del informe de resultados.</i></th> 
                <td>' . $fechah . '</td> 
            </tr> 
            <tr> 
                <th><i>j) Signatario responsable de la evaluación</i></th> 
                <td>' . $tecnico . '</td> 
            </tr> 
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg5: '.$htmlg);

$pdf->AddPage('P', 'A4');

$pdf->Bookmark('H. Metodología de para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$htmlg = '<style type="text/css"> 
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} 
            .tejus{text-align: justify;} 
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px} 
            .sangria{text-indent: 45px;} 
            .txt_td{background-color:rgb(217,217,217);} 
            .tableb td{border:1px solid #808080;} 
            .cet{text-align:center;} 
            .checked{font-size:17px;font-family:dejavusans;} 
            .pspaces{font-size:0.2px;} 
        </style> 
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p> 
        <p class="tdtext"><i>Reconocimiento</i></p> 
        <p class="tdtext sangria tejus">El recorrido de reconocimiento inicial deberá llevarse a cabo previo a la evaluación, con el fin de recabar la información técnico-administrativa que permita desarrollar una estrategia de medición que describa las condiciones térmicas extremas que se generen en el ambiente laboral, se deberá registrar la siguiente información.</p> 
 
        <p class="tdtext tejus sangria">a) Identificar y registrar en un plano de vista de planta del centro de trabajo, todas las fuentes que generen condiciones térmicas extremas.</p> 
        <p class="tdtext tejus sangria">b) Determinar si en el área donde se ubican las fuentes, el POE se localiza en un lugar cerrado o abierto y si existe ventilación natural o artificial.</p> 
        <p class="tdtext tejus sangria">c) Elaborar una relación del POE, incluyendo áreas, puestos de trabajo, tiempos y frecuencia de la exposición.</p> 
        <p class="tdtext tejus sangria">d) Describir las actividades y ciclos de trabajo que realiza el POE en cada puesto de trabajo.</p> 
 
        <p class="titulosa"><b><u><i>TEMPERATURAS ELEVADAS</i></u></b></p> 
        <p class="tdtext sangria"><i>Principio del método</i></p> 
 
        <p class="tdtext tejus sangria">Consiste en aplicar el índice de temperatura de globo bulbo húmedo (Itgbh), medir la temperatura axilar del trabajador expuesto, la humedad relativa, la velocidad del aire y determinar el régimen de trabajo.</p> 
 
        <p class="tdtext sangria"><i>Medición para cada trabajador o grupo de exposición homogénea en puestos fijos</i></p> 
        <p class="tdtext tejus">Medir la temperatura axilar del POE en su puesto de trabajo, antes y después de cada ciclo de exposición. La evaluación del índice de temperatura de globo bulbo húmedo se debe realizar lo más cerca posible del POE, sin que la presencia del evaluador interrumpa sus actividades. La evaluación consiste en medir y promediar a tres diferentes alturas la temperatura de globo bulbo húmedo, colocando los instrumentos de medición en:</p> 
 
        <p class="tdtext tejus sangria">a) La primera medición, a una altura de 0.10 m + 0.05 m (región de los tobillos), en relación con el plano de sustentación del trabajador;</p> 
        <p class="tdtext tejus sangria">b) La segunda medición a la altura de la región abdominal a 0.60 m + 0.05 m, en relación con el plano de sustentación del trabajador sentado, y de 1.10 m + 0.05 m si la actividad es desarrollada de pie;</p> 
        <p class="tdtext tejus sangria">c) La tercera medición, a la altura de la región superior de la cabeza a 1.10 m + 0.05 m en relación al plano de sustentación del trabajador sentado, y de 1.70 m + 0.05 m si desarrolla sus actividades de pie;</p> 
 
        <p class="tdtext tejus">La medición se debe realizar al inicio y al final de todos los ciclos de exposición que se generen durante una hora continua de actividades; <br>En el caso de tener un grupo de exposición homogénea, se debe ubicar el equipo de medición en el centro geométrico del grupo, y realizar la evaluación como se describió.</p> 
 
        <p class="tdtext sangria"><i>Medición para trabajador o grupo de exposición homogénea en movimiento</i></p> 
        <p class="tdtext tejus">Se tendrá que realizar lo establecido repitiéndose en tres ocasiones:</p> 
        <p class="tdtext tejus sangria">a) La primera medición se realizará en el lugar donde se inicia la actividad sujeta a exposición;</p> 
        <p class="tdtext tejus sangria">b) La segunda medición a la mitad de su trayectoria;</p> 
        <p class="tdtext tejus sangria">c) Una tercera medición se realiza al concluir su actividad.</p>';

//$pdf->writeHTML($htmlg, true, false, true, false, ''); 
//$pdf->AddPage('P', 'A4'); 

$pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$htmlg .= '<style type="text/css"> 
                .txt_td{background-color:rgb(217,217,217);} 
                .tableb td,.tableb th{border:1px solid #808080; text-align: center;} 
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;} 
                .ft13{font-size:13px;} 
                .ft11{font-size:11.5px;}  
                .div_diso{ float:left !important; } 
                .pspaces{ font-size:0.5px; } 
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} 
                .tableb tr th{background-color:rgb(217,217,217);} 
            </style> 
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p> 
            <table cellpadding="5" align="center" class="tableb ft13" width="100%">';

//trae los equipos utilizados 
$cont_equi = 0;
$getson = $this->ModeloNom->getEquiposNom15Elevada($idnom);
foreach ($getson as $s) {
    $cont_equi++;
    $id_bulbo = $s->id_bulbo;
    $id_bulbo_humedo = $s->id_bulbo_humedo;
    $id_globo = $s->id_globo;
    if ($cont_equi == 1) {
        $htmlg .= ' 
                <tr><th></th><th><i>Monitor de estrés térmico </i></th><th><i>Monitor de estrés térmico</i></th><th><i>Monitor de estrés térmico</i></th></tr> 
                <tr><th><i>Marca</i></th><td>' . $s->marca . '</td><td>' . $s->marca2 . '</td><td>' . $s->marca3 . '</td></tr> 
                <tr><th><i>Modelo</i></th><td>' . $s->modelo . '</td><td>' . $s->modelo2 . '</td><td>' . $s->modelo3 . '</td></tr> 
                <tr><th><i>Número de Serie</i></th><td>' . $s->equipo . '</td><td>' . $s->equipo2 . '</td><td>' . $s->equipo3 . '</td></tr> 
                <tr><th><i>No. de Informe de calibración</i></th><td class="ft11">' . $s->no_informe_calibracion . '</td><td class="ft11" >' . $s->no_informe_calibracion2 . '</td><td class="ft11" >' . $s->no_informe_calibracion3 . '</td></tr> 
                <tr><th><i>Fecha de calibración</i></th><td>' . $s->fecha_calibracion . '</td><td>' . $s->fecha_calibracion2 . '</td><td>' . $s->fecha_calibracion3 . '</td></tr>';
    }
}
$htmlg .= '</table><p></p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg6: '.$htmlg);

$pdf->AddPage('P', 'A4');
$htmlg = '<style type="text/css"> 
            .txt_td{background-color:rgb(217,217,217);} 
            .tableb td,.tableb th{border:1px solid #808080; text-align:center;} 
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;} 
            .ft13{font-size:13px;} 
            .ft11{font-size:11.5px;}  
            .div_diso{ float:left !important; } 
            .pspaces{ font-size:0.5px; } 
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} 
            .tableb tr th{background-color:rgb(217,217,217);} 
        </style>';

$pdf->Bookmark('J. Descripción de las condiciones de operación ----------------------------------------------------------', 0, 0, '', 'B', array(0, 0, 0));
$htmlg .= '<p class="titulosa"><b>J. DESCRIPCIÓN DE LAS CONDICIONES DE OPERACIÓN</b></p> 
                 
                <p class="tdtext">De acuerdo con la información proporcionada por el personal responsable del centro de trabajo, las condiciones de operación al momento de la evaluación son normales, no se presentan alteraciones que pudieran influir en los resultados obtenidos. Todo el personal y maquinaria se encuentra realizando sus actividades comunes.</p>';

$pdf->Bookmark('K. Descripción del proceso de fabricación del centro de trabajo evaluado -----------------------', 0, 0, '', '', array(0, 0, 0));
$htmlg .= ' 
                <p class="titulosa"><b>K. DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</b></p> 
        <p class="tdtext">Para visualizar la descripción del proceso, ver anexo IV.</p>';

$pdf->Bookmark('L. Descripción de los puestos de trabajo ------------------------------------------------------------------', 0, 0, '', '', array(0, 0, 0));
$htmlg .= ' 
                <p class="titulosa"><b>L. DESCRIPCIÓN DE LOS PUESTOS DE TRABAJO </b></p> 
                <table cellpadding="4" class="tableb"> 
                    <tr> 
                        <th width="30%"><i>Área</i></th> 
                        <th width="30%"><i>Puesto de Trabajo</i></th> 
                        <th width="40%"><i>Actividad(es)</i></th> 
                    </tr>';

foreach ($ptos->result() as $p) {
    $htmlg .= ' 
                <tr> 
                    <td>' . $p->area . '</td> 
                    <td>' . $p->puesto . '</td> 
                    <td>' . $p->desc_activ . '</td> 
                </tr>';
}
$htmlg .= '</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg7: '.$htmlg);

$pdf->AddPage('P', 'A4');

$htmlg = "";
$htmlg = '<style type="text/css"> 
                .txt_td{background-color:rgb(217,217,217);} 
                .tableb td,.tableb th{border:1px solid #808080; text-align:center;} 
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;} 
                .ft13{font-size:13px;} 
                .ft11{font-size:11.5px;}  
                .div_diso{ float:left !important; } 
                .pspaces{ font-size:0.5px; } 
                .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;} 
                .tableb tr th{background-color:rgb(217,217,217);} 
            </style>';
$pdf->Bookmark('M. Número de trabajadores expuestos por área y puesto de trabajo ------------------------------', 0, 0, '', '', array(0, 0, 0));
$htmlg .= ' 
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES EXPUESTOS POR ÁREA Y PUESTO DE TRABAJO </b></p> 
                <table cellpadding="4" class="tableb"> 
                    <tr> 
                        <th><i>Área</i></th> 
                        <th><i>Puesto de Trabajo</i></th> 
                        <th><i>No. de Trabajadores</i></th> 
                    </tr>';

foreach ($ptos->result() as $p) {
    $htmlg .= ' 
                <tr> 
                    <td>' . $p->area . '</td> 
                    <td>' . $p->puesto . '</td> 
                    <td>1</td> 
                </tr>';
}
$htmlg .= '</table>';

$pdf->Bookmark('O. Vigencia del informe ----------------------------------------------------------------------------------------', 0, 0, '', '', array(0, 0, 0));
$htmlg .= ' 
                <p class="titulosa"><b>M. VIGENCIA DEL INFORME </b></p> 
                <p class="tdtext">La vigencia de este informe de resultados será de dos años, mientras se mantengan las condiciones de trabajo que sirvieron de referencia para su emisión.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg8: '.$htmlg);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0, 0, 0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg = '';

$pdf->AddPage('P', 'A4');
$dc = $datosrec->row();
$id_rec = 0;
$desc_proceso = "-----";
$plano_ubica = "-----";
$zona_ubica = "-----";
$observaciones = "-----";

if ($dc->desc_proceso != "")
    $desc_proceso = $dc->desc_proceso;
if ($dc->plano_ubica != "")
    $plano_ubica = $dc->plano_ubica;
if ($dc->observaciones != "")
    $observaciones = $dc->observaciones;
if ($dc->id != "")
    $id_rec = $dc->id;

if (isset($firmaemple) && $firmaemple != "data:") {
    $firm_txt = FCPATH . "uploads/firmas/" . $firmaemple;
    $handle = @fopen($firm_txt, 'r');
    if ($handle) {
        $fh = fopen(FCPATH . "uploads/firmas/" . $firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh);
    } else {
        $linea = "#";
    }
    /*$fh = fopen(base_url()."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo"); 
    $linea = fgets($fh); 
    fclose($fh);     */
} else {
    $linea = '#';
}
if (isset($firmaresp) && $firmaresp != "" && $firmaresp != "data:") {
    $firm_txt2 = FCPATH . "uploads/firmas/" . $firmaresp;
    $handle2 = @fopen($firm_txt2, 'r');
    if ($handle2) {
        $fh2 = fopen(FCPATH . "uploads/firmas/" . $firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
        $firmaresp_line = fgets($fh2);
        fclose($fh2);
    } else {
        $firmaresp_line = "";
    }
    /*$fh2 = fopen(base_url()."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo"); 
    $firmaresp_line = fgets($fh2); 
    fclose($fh2);     */
} else {
    $firmaresp_line = '';
}

$htmlg = '<style type="text/css">  
    .borderbottom{border-bottom:1px solid black;} 
    .backg{background-color:rgb(217,217,217);} 
    .alg{ text-align:center; } 
    .ft9{ font-size:9px; text-align:center; } 
    .ft7{ font-size:7px; text-align:center; } 
    .fontbold{font-weight: bold;} 
    .pspaces{font-size:0.2px;} 
    .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:8.5px;} 
    .tableb td{valign: middle;vertical-align: middle;} 
    .tableb tr th{background-color:rgb(217,217,217);} 
    .tableb85 {font-size:8.5px;} 
    .checked{font-size:11px;font-family:dejavusans;} 
 </style>';
$htmlg .= ' 
        <table border="1" cellpadding="5" align="center" class="ft9"> 
            <tr> 
                <td width="24%" rowspan="2"><img src="' . base_url() . 'public/img/logo.jpg" ></td> 
                <td width="19%">NOMBRE DEL DOCUMENTO</td> 
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
                <td width="19%">VERSIÓN</td> 
                <td width="19%">No COPIA CONTROLADA</td> 
            </tr> 
            <tr> 
                <td>RECONOCIMIENTO INICIAL TEMPERATURAS EXTREMAS</td> 
                <td>REG-TEC/03-01</td> 
                <td>01</td> 
                <td>ORIGINAL</td> 
            </tr> 
        </table> 
        <p></p> 
        <table border="0" cellpadding="5" align="center" class="ft9"> 
            <tr> 
                <td width="20%"><b>No. DE INFORME</b></td> 
                <td width="50%" class="borderbottom">' . $dc->num_informe_rec . '</td> 
                <td width="10%"><b>FECHA:</b></td> 
                <td width="20%" class="borderbottom">' . $dc->fecha . '</td> 
            </tr> 
            <tr> 
                <td width="20%"><b>RAZÓN SOCIAL</b></td> 
                <td width="80%" class="borderbottom">' . $dc->cliente . '</td> 
            </tr> 
            <tr > 
                <td width="60%" class="borderbottom">' . $dc->calle_num . '</td> 
                <td width="40%" class="borderbottom">' . $dc->colonia . '</td> 
            </tr> 
            <tr> 
                <td width="60%" ><b>CALLE Y NUMERO</b></td> 
                <td width="40%" ><b>COLONIA</b></td> 
            </tr> 
            <tr > 
                <td width="34%" class="borderbottom">' . $dc->poblacion . '</td> 
                <td width="33%" class="borderbottom">' . mb_strtoupper($estado_name, "UTF-8") . '</td> 
                <td width="33%" class="borderbottom">' . $dc->cp . '</td> 
            </tr> 
            <tr> 
                <td width="34%" ><b>MUNICIPIO</b></td> 
                <td width="33%" ><b>ESTADO</b></td> 
                <td width="33%" ><b>CÓDIGO POSTAL</b></td> 
            </tr> 
            <tr > 
                <td width="50%" class="borderbottom">' . $dc->rfc . '</td> 
                <td width="50%" class="borderbottom">' . $dc->giro . '</td> 
            </tr> 
            <tr> 
                <td width="50%" ><b>RFC</b></td> 
                <td width="50%" ><b>GIRO DE LA EMPRESA</b></td> 
            </tr> 
            <tr > 
                <td width="50%" class="borderbottom">' . $dc->telefono . '</td> 
                <td width="50%" class="borderbottom">' . $dc->representa . '</td> 
            </tr> 
            <tr> 
                <td width="50%" ><b>NúMERO TELEFÓNICO</b></td> 
                <td width="50%" ><b>REPRESENTANTE LEGAL</b></td> 
            </tr> 
            <tr> 
                <td width="100%" class="borderbottom">' . $dc->nom_cargo . '</td> 
            </tr> 
            <tr> 
                <td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td> 
            </tr> 
        </table>     
        <p></p>';

$htmlg .= '<table border="1" class="tableb" width="100%"> 
                    <thead> 
                        <tr> 
                            <th rowspan="2" width="14%"><p class="pspaces"></p><p class="pspaces"></p>ÁREA</th> 
                            <th colspan="2" width="12%"><p class="pspaces"></p>CONDICIÓN TÉRMICA</th> 
                            <th colspan="2" width="12%"><p class="pspaces"></p>TIPO DE ÁREA</th> 
                            <th colspan="2" width="12%">TIPO DE VENTILACIÓN</th> 
                            <th rowspan="2" width="15%"><p class="pspaces"></p>GENERADOR(ES) DE LA CONDICIÓN TÉRMICA EXTREMA</th> 
                            <th rowspan="2" width="15%"><p class="pspaces"></p>PUESTO DE TRABAJO EXPUESTO</th> 
                            <th rowspan="2" width="10%">TIEMPO DE EXPOSICIÓN POR CICLO EN UNA HORA (min)</th> 
                            <th rowspan="2" width="10%"><p class="pspaces"></p>FRECUENCIA (No. de cilcos en una hora)</th> 
                        </tr> 
                        <tr> 
                            <th><p class="pspaces"></p>ABATIDA</th> 
                            <th><p class="pspaces"></p>ELEVADA</th> 
                            <th><p class="pspaces"></p>ABIERTA</th> 
                            <th><p class="pspaces"></p>CERRADA</th> 
                            <th><p class="pspaces"></p>ARTIFICIAL</th> 
                            <th><p class="pspaces"></p>NATURAL</th> 
                        </tr> 
                    </thead> 
                    <tbody>';

$rec_ptos = $this->ModeloCatalogos->getselectwheren('reconocimiento_areas_nom15', array('id_reconocimiento' => $id_rec, "estatus" => 1));
foreach ($rec_ptos->result() as $p) {
    if ($p->condicion_termica == 1) {
        $condicion_termica = '<p class="checked">✔</p>';
    } else {
        $condicion_termica = '';
    }
    if ($p->condicion_termica == 2) {
        $condicion_termica2 = '<p class="checked">✔</p>';
    } else {
        $condicion_termica2 = '';
    }
    if ($p->tipo_area == 1) {
        $tipo_area = '<p class="checked">✔</p>';
    } else {
        $tipo_area = '';
    }
    if ($p->tipo_area == 2) {
        $tipo_area2 = '<p class="checked">✔</p>';
    } else {
        $tipo_area2 = '';
    }
    if ($p->tipo_ventilacion == 1) {
        $tipo_ventilacion = '<p class="checked">✔</p>';
    } else {
        $tipo_ventilacion = '';
    }
    if ($p->tipo_ventilacion == 2) {
        $tipo_ventilacion2 = '<p class="checked">✔</p>';
    } else {
        $tipo_ventilacion2 = '';
    }
    $htmlg .= '<tr> 
                    <td width="14%">' . $p->area . '</td> 
                    <td width="6%">' . $condicion_termica . '</td> 
                    <td width="6%">' . $condicion_termica2 . '</td> 
                    <td width="6%">' . $tipo_area . '</td> 
                    <td width="6%">' . $tipo_area2 . '</td> 
                    <td width="6%">' . $tipo_ventilacion . '</td> 
                    <td width="6%">' . $tipo_ventilacion2 . '</td> 
                    <td width="15%">' . $p->generadores_condicion . '</td> 
                    <td width="15%">' . $p->puesto . '</td> 
                    <td width="10%">' . $p->tiempo_expo . '</td> 
                    <td width="10%">' . $p->frecuencia . '</td> 
                </tr>';
}
$rowpuntorowr = $rec_ptos->num_rows();
$limitefilas = 35;
if ($rowpuntorowr < 36) {
    $limitefilas = 35;
} else if ($rowpuntorowr > 35 and $rowpuntorowr < 56) {
    $limitefilas = 35;
}
for ($i = $rowpuntorowr; $i <= $limitefilas; $i++) {
    $htmlg .= '<tr> 
                    <td width="14%">--</td> 
                    <td width="6%">--</td> 
                    <td width="6%">--</td> 
                    <td width="6%">--</td> 
                    <td width="6%">--</td> 
                    <td width="6%">--</td> 
                    <td width="6%">--</td> 
                    <td width="15%">--</td> 
                    <td width="15%">--</td> 
                    <td width="10%">--</td> 
                    <td width="10%">--</td> 
                </tr>';
}

$htmlg .= '</tbody> 
            </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg9: '.$htmlg);

$pdf->AddPage('P', 'A4');

$htmlg = '<style type="text/css">  
                    .borderbottom{border-bottom:1px solid black;} 
                    .backg{background-color:rgb(217,217,217);} 
                    .alg{ text-align:center; } 
                    .ft9{ font-size:9px; text-align:center; } 
                    .ft7{ font-size:7px; text-align:center; } 
                    .fontbold{font-weight: bold;} 
                    .pspaces{font-size:0.2px;} 
                    .tableb td,.tableb th{border:1px solid #808080;text-align: center;font-size:9px;} 
                    .tableb td{font-size:9px;valign: middle;vertical-align: middle;} 
                    .tableb tr th{background-color:rgb(217,217,217);} 
                </style> 
            <table border="1" cellpadding="5" align="center" class="ft9"> 
                <tr> 
                    <td width="24%" rowspan="2"><img src="' . base_url() . 'public/img/logo.jpg" ></td> 
                    <td width="19%">NOMBRE DEL DOCUMENTO</td> 
                    <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
                    <td width="19%">VERSIÓN</td> 
                    <td width="19%">No COPIA CONTROLADA</td> 
                </tr> 
                <tr> 
                    <td>RECONOCIMIENTO INICIAL TEMPERATURAS EXTREMAS</td> 
                    <td>REG-TEC/03-01</td> 
                    <td>01</td> 
                    <td>ORIGINAL</td> 
                </tr> 
            </table> 
            <p></p> 
            <table border="1" cellpadding="5" class="tableb"> 
                <tr> 
                    <th width="100%">DESCRIPCIÓN DEL PROCESO</th> 
                </tr> 
                <tr> 
                    <td height="90px">' . $desc_proceso . '</td> 
                </tr> 
                <tr> 
                    <th>PLANO DE UBICACIÓN DE ÁREAS, FUENTES GENERADORAS DE LA CONDICIÓN TÉRMICA EXTREMA Y UBICACIÓN DE LOS PUNTOS DE MEDICIÓN</th> 
                </tr> 
                <tr> 
                    <td height="430px">' . $plano_ubica . '</td> 
                </tr> 
                <tr> 
                    <th>OBSERVACIONES</th> 
                </tr> 
                <tr> 
                    <td height="80px">' . $observaciones . '</td> 
                </tr> 
 
                <tr> 
                    <th width="50%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></th> 
                    <td width="50%"><p class="pspaces"></p><!-- ' . $tecnico . ' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55">--> </td> 
                </tr> 
                <tr> 
                    <th width="50%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></th> 
                    <td width="50%"><p class="pspaces"></p><!-- Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55"> --></td> 
                </tr> 
            </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg10: '.$htmlg);

/* ***********************LEVANTAMIENTO******************************* */
$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg = '';
$pdf->AddPage('P', 'A4');
$htmlg = '<style type="text/css"> 
            .borderbottom{border-bottom:1px solid black;} 
            .backg{background-color:rgb(217,217,217);} 
            .algc{text-align:center; } 
            .ft9{font-size:9px;} 
            .checked{font-size:9px;font-family:dejavusans;} 
            .tableb td{ 
                border:1px solid #808080; 
                font-family:Arial;text-align: center;; 
                font-size:9px; 
                valign: middle; 
                vertical-align: middle; 
            } 
            .tableb th{ 
                background-color:rgb(217,217,217); 
                border:1px solid #808080;   
                text-align:center;  
                font-size:9px; 
            } 
            .pspaces{ 
                font-size:0.5px;     
            } 
        </style> 
        <table border="1" cellpadding="5" align="center" class="ft9"> 
            <tr> 
                <td width="24%" rowspan="2"> 
                    <img src="' . base_url() . 'public/img/logo.jpg" > 
                </td> 
                <td width="19%">NOMBRE DEL DOCUMENTO</td> 
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
                <td width="19%">VERSIÓN</td> 
                <td width="19%">No COPIA CONTROLADA</td> 
            </tr> 
            <tr> 
                <td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td> 
                <td>REG-TEC/03-03</td> 
                <td>01</td> 
                <td>ORIGINAL</td> 
            </tr> 
        </table> 
        <p></p>';

//$htmlg.='<table class="tableb" cellpadding="5">'; 
$id_bulbo = 0;
$id_bulbo_humedo = 0;
$id_globo = 0;
$cont_ptos = 0;
$num_informe = "";
foreach ($datosnom->result() as $item) {

    $ptos_nom = $this->ModeloCatalogos->getselectwheren('nom15_punto_elevada', array('id_nom' => $item->idnom, "activo" => 1));
    foreach ($ptos_nom->result() as $p) {
        $cont_temps = 0;
        $cont_ptos++;
        $id_bulbo = $p->id_bulbo;
        $id_bulbo_humedo = $p->id_bulbo_humedo;
        $id_globo = $p->id_globo;
        $num_informe = $item->num_informe;
        $htmlg .= '<table class="tableb" cellpadding="5"> 
                            <tr> 
                                <th>NO. DE INFORME</th> 
                                <td colspan="3">' . $item->num_informe . '</td> 
                                <th colspan="2">FECHA DE EMISIÓN</th> 
                                <td colspan="2">' . $p->fecha . '</td> 
                            </tr> 
                            <tr> 
                                <th>RAZÓN SOCIAL</th> 
                                <td colspan="7">' . $item->razon_social . '</td> 
                            </tr>';

        $resultequipo = $this->ModeloCatalogos->getselectwheren('equipo', array('id' => $id_bulbo));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg .= '<tr> 
                            <th width="5%">ID</th> 
                            <td width="20%">' . $iteme->luxometro . '</td> 
                            <th width="10%">MARCA</th> 
                            <td width="15%">' . $iteme->marca . '</td> 
                            <th width="10%">MODELO</th> 
                            <td width="15%">' . $iteme->modelo . '</td> 
                            <th width="10%">SERIE</th> 
                            <td width="15%">' . $iteme->equipo . '</td> 
                        </tr>';
        }
        $resultequipo = $this->ModeloCatalogos->getselectwheren('equipo', array('id' => $id_bulbo_humedo));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg .= '<tr> 
                            <th width="5%">ID</th> 
                            <td width="20%">' . $iteme->luxometro . '</td> 
                            <th width="10%">MARCA</th> 
                            <td width="15%">' . $iteme->marca . '</td> 
                            <th width="10%">MODELO</th> 
                            <td width="15%">' . $iteme->modelo . '</td> 
                            <th width="10%">SERIE</th> 
                            <td width="15%">' . $iteme->equipo . '</td> 
                        </tr>';
        }
        $resultequipo = $this->ModeloCatalogos->getselectwheren('equipo', array('id' => $id_globo));
        foreach ($resultequipo->result() as $iteme) {
            $htmlg .= '<tr> 
                            <th width="5%">ID</th> 
                            <td width="20%">' . $iteme->luxometro . '</td> 
                            <th width="10%">MARCA</th> 
                            <td width="15%">' . $iteme->marca . '</td> 
                            <th width="10%">MODELO</th> 
                            <td width="15%">' . $iteme->modelo . '</td> 
                            <th width="10%">SERIE</th> 
                            <td width="15%">' . $iteme->equipo . '</td> 
                        </tr>';
        }

        if ($p->carga_solar == 1) {
            $carga_solar = '<p class="checked">(✔) Si ( ) No</p>';
        } else {
            $carga_solar = '<p class="checked">( ) Si (✔) No </p>';
        }
        /*if($p->carga_solar==2){ 
                        $carga_solar2='<p class="checked">(✔) No </p>'; 
                    }else{ 
                        $carga_solar2='( ) No'; 
                    }*/

        if ($p->tipo_evalua == 1) {
            $tipo_evalua = '<p class="checked">(✔) POE</p>';
        } else {
            $tipo_evalua = '( ) POE';
        }
        if ($p->tipo_evalua == 2) {
            $tipo_evalua2 = '<p class="checked">(✔) GEH </p>';
        } else {
            $tipo_evalua2 = '( ) GEH';
        }
        if ($p->tipo_mov == 1) {
            $tipo_mov = '<p class="checked">(✔) Movimiento</p>';
        } else {
            $tipo_mov = '( ) Movimiento';
        }
        if ($p->tipo_mov == 2) {
            $tipo_mov2 = '<p class="checked">(✔) Fijo </p>';
        } else {
            $tipo_mov2 = '( ) Fijo';
        }
        if ($p->casco == 1) {
            $casco = '<p class="checked">Casco: (✔)</p>';
        } else {
            $casco = 'Casco: ( )';
        }
        if ($p->tapones == 1) {
            $tapones = '<p class="checked">Tapones acústicos: (✔)</p>';
        } else {
            $tapones = 'Tapones acústicos: ( )';
        }
        if ($p->botas == 1) {
            $botas = '<p class="checked">Botas seguridad: (✔)</p>';
        } else {
            $botas = 'Botas seguridad: ( )';
        }
        if ($p->lentes == 1) {
            $lentes = '<p class="checked">Lentes: (✔)</p>';
        } else {
            $lentes = 'Lentes: ( )';
        }
        if ($p->guantes == 1) {
            $guantes = '<p class="checked">Guantes: (✔)</p>';
        } else {
            $guantes = 'Guantes: ( )';
        }
        if ($p->peto == 1) {
            $peto = '<p class="checked">Peto: (✔)</p>';
        } else {
            $peto = 'Peto: ( )';
        }
        if ($p->respirador == 1) {
            $respirador = '<p class="checked">Respirador: (✔)</p>';
        } else {
            $respirador = 'Respirador: ( )';
        }
        $htmlg .= '</table> 
                    <table width="100%" class="tableb" cellpadding="4"> 
                        <tr> 
                            <th width="10%">ÁREA</th>    
                            <td width="40%">' . $p->area . '</td>    
                            <th width="10%"> No. PUNTO</th>   
                            <td width="5%">' . $p->num_punto . '</td> 
                            <th width="20%">¿EXISTE CARGAR SOLAR?</th> 
                            <td width="15%">' . $carga_solar . '</td> 
                        </tr>  
                        <tr> 
                            <th width="20%">FUENTE(S) GENERADORA(S) DE LA CONDICIÓN TÉRMICA</th> 
                            <td colspan="5" width="80%">' . $p->fte_generadora . '</td> 
                        </tr> 
                        <tr> 
                            <th width="20%">CONTROLES TÉCNICOS</th>    
                            <td colspan="8" width="80%">' . $p->controles_tec . '</td> 
                        </tr> 
                        <tr> 
                            <th width="20%">CONTROLES ADMINISTRATIVOS</th>    
                            <td colspan="5" width="80%">' . $p->controles_adm . '</td> 
                        </tr> 
                        <tr> 
                            <th width="20%" colspan="2">TIPO DE EVALUACIÓN</th> 
                            <th width="50%">NOMBRE DEL TRABAJADOR O GRUPO DE EXPOSICIÓN HOMOGÉNEA EVALUADO</th> 
                            <th width="15%">Temperatura auxiliar inicial de la jornada (°C)</th> 
                            <th width="15%">Temperatura auxiliar final de la jornada (°C)</th> 
                        </tr> 
                        <tr> 
                            <td>' . $tipo_evalua . '</td> 
                            <td>' . $tipo_evalua2 . '</td> 
                            <td>' . $p->nom_trabaja . '</td> 
                            <td>' . $p->temp_aux_ini . '</td> 
                            <td>' . $p->temp_aux_fin . '</td> 
                        </tr> 
                        <tr> 
                            <td>' . $tipo_mov . '</td> 
                            <td>' . $tipo_mov2 . '</td> 
                            <td colspan="3"></td> 
                        </tr> 
                    </table> 
                    <table cellpadding="4" class="tableb" width="100%"> 
                        <tr> 
                            <td width="20%"><p class="pspaces"></p>EPP Utilizado:</td>  
                            <td width="20%"> ' . $casco . '</td> 
                            <td width="20%"> ' . $tapones . '</td><td width="20%"> ' . $botas . '</td><td width="20%"> ' . $lentes . '</td>     
                        </tr> 
                        <tr> 
                            <td width="20%">' . $guantes . '</td> 
                            <td width="20%">' . $peto . '</td>  
                            <td width="20%">' . $respirador . '</td> 
                            <td colspan="2" width="40%">OTRO: ' . $p->otro . '</td> 
                        </tr> 
                    </table> 
                    <table cellpadding="4" class="tableb" width="100%"> 
                        <tr> 
                            <th width="15%">Puesto de trabajo</th> 
                            <td width="20%">' . $p->puesto . '</td> 
                            <th width="30%">Tiempo de exposición por ciclo en cada h (min)</th> 
                            <td width="5%">' . $p->tiempo_expo . '</td> 
                            <th width="20%">Número de ciclos por h</th> 
                            <td width="10%">' . $p->num_ciclos . '</td> 
                        </tr> 
                        <tr> 
                            <th width="15%">% exposición</th> 
                            <td width="20%">' . $p->porc_expo . '</td> 
                            <th width="30%">% no exposición</th> 
                            <td width="5%">' . $p->porc_no_expo . '</td> 
                            <th width="20%">Régimen de trabajo</th> 
                            <td width="10%">' . $p->regimen . '</td> 
                        </tr> 
                    </table>  
                    <table cellpadding="4" class="tableb" width="100%"> 
                        <tr> 
                            <th width="35%">Descripción de actividades del personal</th> 
                            <td width="65%">' . $p->desc_activ . '</td> 
                        </tr> 
                        <tr> 
                            <th width="35%">Observaciones</th> 
                            <td width="65%">' . $p->observaciones . '</td> 
                        </tr> 
                    </table>  
                    <table class="tableb"> 
                        <tr><td></td></tr> 
                    </table>';

        $cont_temps_det = 0;
        $med = $this->ModeloCatalogos->getselectwheren('medicion_temp_nom15_elevadas2', array('id_nom_punto' => $p->id, "activo" => 1));
        foreach ($med->result() as $m) {
            $cont_temps++;
            $cont_temps_det++;
            //log_message('error', 'cont_temps_det: '.$cont_temps_det); 
            if ($cont_temps == 1) { //ver como pintar o armar la tabla antes de pintar 
                $htmlg .= '<table class="tableb" width="100%" cellpadding="2"> 
                            <tr> 
                                <th width="10%">Ciclo No.</th> 
                                <td width="10%">' . $m->ciclo_med . '</td> 
                                <th width="30%">Temperatura axiliar inicial (°C)</th> 
                                <td width="10%">' . $m->temp_ini . '</td> 
                                <th width="30%">Tempertura axiliar final (°C)</th> 
                                <td colspan="2" width="10%">' . $m->temp_fin . '</td> 
                            </tr> 
                            <tr> 
                                <th rowspan="2"><p class="pspaces"></p>Evaluación</th> 
                                <th rowspan="2"><p class="pspaces"></p>Regíón</th> 
                                <th rowspan="2"><p class="pspaces"></p>Altura (m)</th> 
                                <th rowspan="2"><p class="pspaces"></p>Hora</th> 
                                <th colspan="3">Medición</th> 
                            </tr> 
                            <tr> 
                                <th>Tbh °C </th> 
                                <th>Tbs °C </th> 
                                <th>Tg °C </th> 
                            </tr>';
            }
            if ($m->tipo == 1 && $m->tipo_med == 1) {
                $tipo = "Inicial";
                $tmpo = "Cabeza";
            } else if ($m->tipo == 2 && $m->tipo_med == 1) {
                $tipo = "Inicial";
                $tmpo = "Abdomen";
            } else if ($m->tipo == 3 && $m->tipo_med == 1) {
                $tipo = "Inicial";
                $tmpo = "Tobillo";
            } else if ($m->tipo == 1 && $m->tipo_med == 2) {
                $tipo = "Mitad";
                $tmpo = "Cabeza";
            } else if ($m->tipo == 2 && $m->tipo_med == 2) {
                $tipo = "Mitad";
                $tmpo = "Abdomen";
            } else if ($m->tipo == 3 && $m->tipo_med == 2) {
                $tipo = "Mitad";
                $tmpo = "Tobillo";
            } else if ($m->tipo == 1 && $m->tipo_med == 3) {
                $tipo = "Final";
                $tmpo = "Cabeza";
            } else if ($m->tipo == 2 && $m->tipo_med == 3) {
                $tipo = "Final";
                $tmpo = "Abdomen";
            } else if ($m->tipo == 3 && $m->tipo_med == 3) {
                $tipo = "Final";
                $tmpo = "Tobillo";
            }
            if ($cont_temps_det == 1 || $cont_temps_det == 4 || $cont_temps_det == 7) {
                $rows_tipo = '<th rowspan="3"><p class="pspaces"></p>' . $tipo . '</th>';
            } else {
                $rows_tipo = "";
            }
            $htmlg .= '<tr> 
                                ' . $rows_tipo . ' 
                                <td>' . $tmpo . '</td> 
                                <td>' . $m->altura . '</td> 
                                <td>' . $m->hora . '</td> 
                                <td>' . $m->tbh . '</td> 
                                <td>' . $m->tbh . '</td> 
                                <td>' . $m->tg . '</td> 
                            </tr>';
        } //foreach de mediciones 
        if($cont_temps>0){
            $htmlg .= '</table>';   
        }
        

        $htmlg .= '<table class="tableb"> 
                        <tr><td><span style="font-size: 9px;">Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</span></td></tr> 
                    </table> 
                    <table class="tableb"> 
                        <tr><td></td></tr> 
                    </table> 
 
                    <table class="tableb"> 
                        <tr> 
                            <th width="50%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></th> 
                            <td width="50%"><p class="pspaces"></p><!--' . $tecnico . ' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55"> --></td> 
                        </tr> 
                        <tr> 
                            <th width="50%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></th> 
                            <td width="50%"><p class="pspaces"></p><!--Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55">--> </td> 
                        </tr> 
                    </table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        //log_message('error','htmlg11: '.$htmlg);

        if ($cont_ptos < $ptos_nom->num_rows()) {
            $pdf->AddPage('P', 'A4');
            $htmlg = '<style type="text/css"> 
                            .borderbottom{border-bottom:1px solid black;} 
                            .backg{background-color:rgb(217,217,217);} 
                            .algc{text-align:center; } 
                            .ft9{font-size:9px;} 
                            .checked{font-size:9px;font-family:dejavusans;} 
                            .tableb td{ 
                                border:1px solid #808080; 
                                font-family:Arial;text-align: center;; 
                                font-size:9px; 
                                valign: middle; 
                                vertical-align: middle; 
                            } 
                            .tableb th{ 
                                background-color:rgb(217,217,217); 
                                border:1px solid #808080;   
                                text-align:center;  
                                font-size:9px; 
                            } 
                            .pspaces{ 
                                font-size:0.5px;     
                            } 
                        </style> 
                        <table border="1" cellpadding="5" align="center" class="ft9"> 
                            <tr> 
                                <td width="24%" rowspan="2"> 
                                    <img src="' . base_url() . 'public/img/logo.jpg" > 
                                </td> 
                                <td width="19%">NOMBRE DEL DOCUMENTO</td> 
                                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
                                <td width="19%">VERSIÓN</td> 
                                <td width="19%">No COPIA CONTROLADA</td> 
                            </tr> 
                            <tr> 
                                <td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td> 
                                <td>REG-TEC/03-03</td> 
                                <td>01</td> 
                                <td>ORIGINAL</td> 
                            </tr> 
                        </table> 
                        <p></p>';
        }
        //log_message('error', 'htmlg: '.$htmlg); 
    } //foreach de nom15_punto_elevada 

}

//$htmlg.='</table>'; 

//$pdf->writeHTML($htmlg, true, false, true, false, ''); 

/************************** *******************************/

$pdf->AddPage('P', 'A4');
$htmlg = "";
$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27, 15);
$pdf->Bookmark('II. Planos ----------------------------------------------------------------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table> 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO II</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PLANOS</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg = '';
$htmlg2 = "";
$htmlg3 = "";
$cont_plan = 0;
foreach ($datosnomplanos->result() as $item) {
    if ($item->tipo == 1) {
        $pdf->AddPage('P', 'A4');
        $cont_plan++;
        /*$htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'" ></td></tr></table>';*/
        $htmlg = '<table width="100%" align="center"><tr><td align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url() . 'uploads/imgPlanos/' . $item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if ($cont_plan == 0) {
    $pdf->AddPage('P', 'A4');
    $htmlg = '<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de las fuentes generadoras de la condición térmica extrema</i></td></tr></table> 
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg = '';
$cont_plan2 = 0;
foreach ($datosnomplanos->result() as $item) {
    if ($item->tipo == 2) {
        $pdf->AddPage('P', 'A4');
        $cont_plan2++;

        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg = '<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url() . 'uploads/imgPlanos/' . $item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}
if ($cont_plan2 == 0) {
    $pdf->AddPage('P', 'A4');
    $htmlg = '<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table> 
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

/* ********************************************/
$htmlg .= '';
//$pdf->writeHTML($htmlg, true, false, true, false, ''); 
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo --------------------------------------------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO III</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">SECUENCIA DE CÁLCULO </td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

/************************************************************** */
$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);
$pdf->AddPage('P', 'A4');

$htmlg = '';
$htmlg .= '<style type="text/css">  
    .ft11{ font-size:11px; text-align:center; } 
    .ft9{ font-size:9px; text-align:center; } 
    .ft8{ font-size:8px; text-align:center; } 
    .ft7{ font-size:7px; text-align:center; } 
    .fontbold{font-weight: bold;} 
    .tableb td{ 
        border:1px solid #808080; 
        font-family:Arial;text-align: center; 
        font-size:9px; 
        valign: middle; 
        text-align:center; 
    } 
    .tableb th{ 
        background-color:rgb(217,217,217); 
        border:1px solid #808080;   
        text-align:center;  
        font-size:9px; 
    } 
    .tableb6 td{ 
        border:1px solid #808080; 
        font-family:Arial;text-align: center; 
        font-size:6px; 
        valign: middle; 
        text-align:center; 
    } 
    .tableb6 th{ 
        background-color:rgb(217,217,217); 
        border:1px solid #808080;   
        text-align:center;  
        font-size:6px; 
    } 
    .tableb7 td{ 
        border:1px solid #808080; 
        font-family:Arial;text-align: center; 
        font-size:7px; 
        valign: middle; 
        text-align:center; 
    } 
    .tableb7 th{ 
        background-color:rgb(217,217,217); 
        border:1px solid #808080;   
        text-align:center;  
        font-size:7px; 
    } 
    .pspaces{ 
        font-size:0.2px;     
    } 
 </style>';
$htmlg .= ' 
    <table border="1" cellpadding="5" align="center" class="ft9"> 
        <tr> 
            <td width="24%" rowspan="2"><img src="' . base_url() . 'public/img/logo.jpg" ></td> 
            <td width="19%">NOMBRE DEL DOCUMENTO</td> 
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
            <td width="19%">VERSIÓN</td> 
            <td width="19%">No COPIA CONTROLADA</td> 
        </tr> 
        <tr> 
            <td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td> 
            <td>REG-TEC/03-03</td> 
            <td>01</td> 
            <td>ORIGINAL</td> 
        </tr> 
    </table> 
    <table><tr><td></td></tr></table>';


$interseccion_16_2 = $this->ModeloCatalogos->interseccion_pendiente3(16, 0, 2); //bulbo humedo(G11) 
$pendiente_16_2 = $this->ModeloCatalogos->interseccion_pendiente3(16, 1, 2); //bulbo humedo(G12) 

$interseccion_17_2 = $this->ModeloCatalogos->interseccion_pendiente3(17, 0, 2); //bulbo humedo(O11) 
$pendiente_17_2 = $this->ModeloCatalogos->interseccion_pendiente3(17, 1, 2); //bulbo humedo(O12) 

$interseccion_18_2 = $this->ModeloCatalogos->interseccion_pendiente3(18, 0, 2); //bulbo humedo(W11) 
$pendiente_18_2 = $this->ModeloCatalogos->interseccion_pendiente3(18, 1, 2); //bulbo humedo(W12) 

$interseccion_16_3 = $this->ModeloCatalogos->interseccion_pendiente3(16, 0, 3); //globo(G19) 
$pendiente_16_3 = $this->ModeloCatalogos->interseccion_pendiente3(16, 1, 3); //globo(G20) 

$interseccion_17_3 = $this->ModeloCatalogos->interseccion_pendiente3(17, 0, 3); //globo(O11) 
$pendiente_17_3 = $this->ModeloCatalogos->interseccion_pendiente3(17, 1, 3); //globo(O12) 

$interseccion_18_3 = $this->ModeloCatalogos->interseccion_pendiente3(18, 0, 3); //globo(W11) 
$pendiente_18_3 = $this->ModeloCatalogos->interseccion_pendiente3(18, 1, 3); //globo(W12) 

$interseccion_16_1 = $this->ModeloCatalogos->interseccion_pendiente3(16, 0, 1); //Bulbo seco(G3) 
$pendiente_16_1 = $this->ModeloCatalogos->interseccion_pendiente3(16, 1, 1); //Bulbo seco(G4) 

$interseccion_17_1 = $this->ModeloCatalogos->interseccion_pendiente3(17, 0, 1); //Bulbo seco(O3) 
$pendiente_17_1 = $this->ModeloCatalogos->interseccion_pendiente3(17, 1, 1); //Bulbo seco(O4) 

$interseccion_18_1 = $this->ModeloCatalogos->interseccion_pendiente3(18, 0, 1); //Bulbo seco(W3) 
$pendiente_18_1 = $this->ModeloCatalogos->interseccion_pendiente3(18, 1, 1); //Bulbo seco(W4) 

$cont_ptos_ini = 0;
$ipto = 0;
foreach ($get_ptos->result() as $p) {
    $cont_ptos_ini++;
    $ipto++;
    if ($p->tipo_evalua == 1) {
        $tipo_evalua = "POE";
    }
    if ($p->tipo_evalua == 2) {
        $tipo_evalua = "GEH";
    }

    if ($p->tipo_mov == 1) {
        $tipo_mov = "Movimiento";
    }
    if ($p->tipo_mov == 2) {
        $tipo_mov = "Fijo";
    }

    if ($p->carga_solar == 1) {
        $carga_solar = "X";
        $carga_solar2 = "--";
    }
    if ($p->carga_solar == 2) {
        $carga_solar = "--";
        $carga_solar2 = "X";
    }

    if ($cont_ptos_ini > 1) {
        $htmlg .= '<table border="1" cellpadding="5" align="center" class="ft9"> 
                <tr> 
                    <td width="24%" rowspan="2"><img src="' . base_url() . 'public/img/logo.jpg" ></td> 
                    <td width="19%">NOMBRE DEL DOCUMENTO</td> 
                    <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td> 
                    <td width="19%">VERSIÓN</td> 
                    <td width="19%">No COPIA CONTROLADA</td> 
                </tr> 
                <tr> 
                    <td>HOJA DE CAMPO PARA TEMPERATURAS ABATIDAS</td> 
                    <td>REG-TEC/03-03</td> 
                    <td>01</td> 
                    <td>ORIGINAL</td> 
                </tr> 
            </table> 
            <table><tr><td></td></tr></table>';
    }

    $htmlg .= '<table class="tableb" cellpadding="5"> 
            <tr> 
                <th>No. de Informe</th> 
                <td>' . $num_informe . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Razón Social</th> 
                <td>' . $empresa . '</td> 
            </tr> 
            <tr> 
                <th>Fecha de Medición</th> 
                <td>' . $p->fecha . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Número de Punto </th> 
                <td>' . $p->num_punto . '</td> 
                <th>Área</th> 
                <td>' . $p->area . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th width="30%">Identificación del Punto</th> 
                <td width="70%">' . $p->area . '</td> 
            </tr> 
            <tr> 
                <th>Fuentes Generadoras de la Condición Térmica</th> 
                <td>' . $p->fte_generadora . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Tipo de Evaluación</th> 
                <td>' . $tipo_evalua . '</td> 
                <th>Tipo de puesto de Trabajo</th> 
                <td>' . $tipo_mov . '</td> 
            </tr> 
        </table> 
        <table><tr><td></td></tr></table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Nombre del POE</th> 
                <td>' . $p->nom_trabaja . '</td> 
            </tr> 
             
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Puesto  </th> 
                <td>' . $p->puesto . '</td> 
                <th>Tiempo de exposición por ciclo (min)</th> 
                <td>' . $p->tiempo_expo . '</td> 
            </tr> 
            <tr> 
                <th>Número de Ciclos Por Hora</th> 
                <td>' . $p->num_ciclos . '</td> 
                <th>Ciclo Evaluado</th> 
                <td>' . $ipto . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Régimen de trabajo</th> 
                <td>' . $p->regimen . '</td> 
                <th>% de Exposición</th> 
                <td>' . $p->porc_expo . '</td> 
                <th>% de Recuperación</th> 
                <td>' . $p->porc_no_expo . '</td> 
            </tr> 
        </table> 
        <table class="tableb" cellpadding="5"> 
            <tr> 
                <th>Sin carga solar</th> 
                <td>' . $carga_solar . '</td> 
                <th>Con carga solar</th> 
                <td>' . $carga_solar2 . '</td> 
            </tr> 
        </table> 
        <table><tr><td></td></tr></table>';

    $cont_temps = 0;
    $med = $this->ModeloCatalogos->getselectwheren('medicion_temp_nom15_elevadas2', array('id_nom_punto' => $p->id, "activo" => 1));
    $inical_promedio_sin = array();
    $inical_promedio = 0;
    $mitad_promedio_sin = array();
    $mitad_promedio = 0;
    $final_promedio_sin = array();
    $final_promedio = 0;

    $inical_promedio_con = array();
    $mitad_promedio_con = array();
    $final_promedio_con = array();

    foreach ($med->result() as $m) {
        if ($m->tipo == 1 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $inical_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $inical_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 2 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $inical_promedio_sin[] = $ltgbh_sin * 2;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $inical_promedio_con[] = $ltgbh_con * 2;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 3 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $inical_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $inical_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 1 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $mitad_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $mitad_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 2 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $mitad_promedio_sin[] = $ltgbh_sin * 2;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $mitad_promedio_con[] = $ltgbh_con * 2;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 3 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $mitad_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $mitad_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 1 && $m->tipo_med == 3) { //ultimos registros 
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $final_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $final_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 2 && $m->tipo_med == 3) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $final_promedio_sin[] = $ltgbh_sin * 2;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $final_promedio_con[] = $ltgbh_con * 2;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
        if ($m->tipo == 3 && $m->tipo_med == 3) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    $final_promedio_sin[] = $ltgbh_sin;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    $final_promedio_con[] = $ltgbh_con;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
        }
    }

    foreach ($med->result() as $m) {
        $cont_temps++;
        if ($cont_temps == 1) {
            $htmlg .= '<table class="tableb" cellpadding="5"> 
                        <tr> 
                            <th width="60%">Temperatura axilar (jornada laboral)</th> 
                            <th width="10%">inicial (°C)</th> 
                            <td width="10%">' . $p->temp_aux_ini . '</td> 
                            <th width="10%">final (°C)</th> 
                            <td width="10%">' . $p->temp_aux_fin . '</td> 
                        </tr>  
                        <tr> 
                            <th width="60%">Temperatura axilar (ciclo de exposición)</th> 
                            <th width="10%">inicial (°C)</th> 
                            <td width="10%">' . $m->temp_ini . '</td> 
                            <th width="10%">final (°C)</th> 
                            <td width="10%">' . $m->temp_fin . '</td> 
                        </tr>  
                    </table> 
                    <table><tr><td></td></tr></table> 
                    <table class="tableb" cellpadding="5"> 
                        <tr> 
                            <th rowspan="2"><p class="pspaces"></p>Medición</th> 
                            <th rowspan="2"><p class="pspaces"></p>Regíón</th> 
                            <th rowspan="2"><p class="pspaces"></p>Altura (m)</th> 
                            <th rowspan="2"><p class="pspaces"></p>Hora</th> 
                            <th colspan="3">Medición</th> 
                            <th colspan="3">Mediciones Corregidas</th> 
                            <th rowspan="2">L <sub>tgbh</sub> Sin carga solar °C</th> 
                            <th rowspan="2">L <sub>tgbh</sub> con carga solar °C</th> 
                            <th rowspan="2">L <sub>tgbh</sub> Promedio °C</th> 
                        </tr> 
                        <tr> 
                            <th>Tbh °C</th> 
                            <th>Tbs °C</th> 
                            <th>Tg °C</th> 
                            <th>Tbh °C</th> 
                            <th>Tbs °C</th> 
                            <th>Tg °C</th> 
                        </tr>';
        }

        if ($m->tipo == 1 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    if (array_sum($inical_promedio_sin) > 0) {
                        $inical_promedio = array_sum($inical_promedio_sin) / 4;
                    }
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    if (array_sum($inical_promedio_con) > 0) {
                        $inical_promedio = array_sum($inical_promedio_con) / 4;
                    }
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= '<tr> 
                    <th rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>Inicial</th> 
                    <th>Cabeza</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                    <td rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>' . round($inical_promedio, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 2 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= '<tr> 
                    <th>Abdomen</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 3 && $m->tipo_med == 1) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th>Tobillo</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 1 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    if (array_sum($mitad_promedio_sin) > 0) {
                        $mitad_promedio = array_sum($mitad_promedio_sin) / 4;
                    }
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    if (array_sum($mitad_promedio_con) > 0) {
                        $mitad_promedio = array_sum($mitad_promedio_con) / 4;
                    }
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>Mitad</th> 
                    <th>Cabeza</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                    <td rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>' . round($mitad_promedio, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 2 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th>Abdomen</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 3 && $m->tipo_med == 2) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th>Tobillo</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 1 && $m->tipo_med == 3) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_16_2 * $m->tbh) + $interseccion_16_2;
                $tbs_c = ($pendiente_16_3 * $m->tbs) + $interseccion_16_3;
                $tg_c = ($pendiente_16_1 * $m->tg) + $interseccion_16_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                    if (array_sum($final_promedio_sin) > 0) {
                        $final_promedio = array_sum($final_promedio_sin) / 4;
                    }
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                    if (array_sum($final_promedio_con) > 0) {
                        $final_promedio = array_sum($final_promedio_con) / 4;
                    }
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>Final</th> 
                    <th>Cabeza</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                    <td rowspan="3"><p class="pspaces"></p><p class="pspaces"></p>' . round($final_promedio, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 2 && $m->tipo_med == 3) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_17_2 * $m->tbh) + $interseccion_17_2;
                $tbs_c = ($pendiente_17_3 * $m->tbs) + $interseccion_17_3;
                $tg_c = ($pendiente_17_1 * $m->tg) + $interseccion_17_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th>Abdomen</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
        if ($m->tipo == 3 && $m->tipo_med == 3) {
            if ($m->hora != "00:00:00") {
                $hora = $m->hora;
                $tbh_c = ($pendiente_18_2 * $m->tbh) + $interseccion_18_2;
                $tbs_c = ($pendiente_18_3 * $m->tbs) + $interseccion_18_3;
                $tg_c = ($pendiente_18_1 * $m->tg) + $interseccion_18_1;
                if ($p->carga_solar == 1) {
                    $ltgbh_sin = 0.7 * $tbh_c + 0.3 * $tbs_c;
                } else {
                    $ltgbh_sin = 0;
                }
                if ($p->carga_solar == 2) {
                    $ltgbh_con = 0.7 * $tbh_c + 0.2 * $tbs_c + 0.1 * $tg_c;
                } else {
                    $ltgbh_con = 0;
                }
            } else {
                $hora = "--";
                $tbh_c = 0;
                $tbs_c = 0;
                $tg_c = 0;
                $ltgbh_sin = 0;
                $ltgbh_con = 0;
            }
            $htmlg .= ' 
                <tr> 
                    <th>Tobillo</th> 
                    <td>' . $m->altura . '</td> 
                    <td>' . $hora . '</td> 
                    <td>' . $m->tbh . '</td> 
                    <td>' . $m->tbs . '</td> 
                    <td>' . $m->tg . '</td> 
                    <td>' . round($tbh_c, 1) . '</td> 
                    <td>' . round($tbs_c, 1) . '</td> 
                    <td>' . round($tg_c, 1) . '</td> 
                    <td>' . round($ltgbh_sin, 1) . '</td> 
                    <td>' . round($ltgbh_con, 1) . '</td> 
                </tr>';
        }
    } //foreach 
    $htmlg .= '</table>  
        <table cellpadding="2" align="center" class="ft9"> 
            <tr> 
                <td>Tbh: Temperatura bulbo húmedo, Tbs: Temperatura bulbo seco, Tg: Temperatura de globo</td> 
            </tr> 
            <tr> 
                <td>I<sub>tgbh</sub> Índice de la temperatura de globo bulbo húmedo promedio</td> 
            </tr> 
        </table> 
        <table cellpadding="2" align="center" class="ft11"> 
            <tr> 
                <td> 
                    INDÍCE DE TEMPERATURA DE GLOBO BÚLBO HÚMEDO (sin carga solar)<br><img width="200px" src="' . base_url() . 'public/img/form_elevada1.png"> 
                </td> 
            </tr> 
            <tr> 
                <td> 
                    INDÍCE DE TEMPERATURA DE GLOBO BÚLBO HÚMEDO (con carga solar)<br><img width="200px" src="' . base_url() . 'public/img/form_elevada2.png"> 
                </td> 
            </tr> 
            <tr> 
                <td> 
                    TEMPERATURA DE GLOBO BÚLBO HÚMEDO PROMEDIO<br><img width="300px" src="' . base_url() . 'public/img/form_elevada3.png"> 
                </td> 
            </tr> 
            <tr> 
                <td height="30px"></td> 
            </tr> 
        </table>';
}

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error','htmlg12 -: '.$htmlg);

// ---======================================================================================--- 


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Descripción del proceso de fabricación del centro de trabajo evaluado ---------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IV</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg = '';
$cont_desc_pro = 0;
foreach ($datosnomiv->result() as $item) {
    $cont_desc_pro++;
    $htmlg .= '<table><tr><td align="center"><img width="800px" height="850px" src="' . base_url() . 'uploads/imgIntro/' . $item->url_img . '"></td></tr></table>';
}

if ($cont_desc_pro == 0) {
    $pdf->AddPage('P', 'A4');
    $htmlg = '<table border="0" > 
            <tr><td align="center" height="200px"></td></tr> 
            <tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr> 
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
} else {
    $pdf->AddPage('P', 'A4');
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Informes de calibración del equipo de medición ----------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO V</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">INFORMES DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$datosequipoc = $this->ModeloNom->getCertEquipos015($id_bulbo, $id_bulbo_humedo, $id_globo);
foreach ($datosequipoc as $item) {
    $htmlg = '';
    $pdf->AddPage('P', 'A4');
    $htmlg .= '<table><tr><td align="center"><img width="800px" height="850px" src="' . base_url() . 'uploads/imgEquipos/' . $item->certificado . '"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0, 57, 88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt = 'Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0, 57, 88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt = $folio;
    $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');
}

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VI. Documento de acreditación ------------------------------------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VI</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE ACREDITACIÓN</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

if ($datosacre->num_rows() > 0) {
    $htmlg = '';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4');

        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>'; 
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/

        $pdf->Image(base_url() . 'uploads/imgAcre/' . $item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);

        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0, 57, 88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt = 'Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0, 57, 88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt = $folio;
        $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');
    }
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color' 
$pdf->Bookmark('VII. Documento de aprobación -------------------------------------------------------------------------', 1, 0, '', '', array(0, 0, 0));
$htmlg = ' 
        <table border="0" > 
            <tr><td height="280px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VII</td></tr> 
            <tr><td height="50px"></td></tr> 
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE APROBACIÓN</td></tr> 
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$txt_fin = "";
$cont_acre = 0;
if ($datosacre2->num_rows() > 0) {
    $htmlg = '';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2 = 0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima 
        $cont2++;
        $pdf->SetMargins(20,25,20); 
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->AddPage('P', 'A4');

        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>'; 
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/

        $pdf->Image(base_url() . 'uploads/imgAcre/' . $item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);
        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0, 57, 88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt = 'Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0, 57, 88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt = $folio;
        $pdf->Cell(100, 50, $txt, 0, $ln = 0, 'C', 0, '', 0, false, 'B', 'B');

        if ($cont_acre == $cont2) {
            /*$txt_fin.='<table> 
                <tr> 
                    <td width="100%" align="center"> 
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p> 
                    </td> 
                </tr> 
                <tr> 
                    <td width="100%" align="center"> 
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p> 
                    </td> 
                </tr> 
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true); 
            //$pdf->writeHTML($txt_fin, true, false, true, false, ''); 

            /*
            $pdf->SetXY(60, 211);
            $pdf->SetTextColor(0, 57, 88);
            $pdf->SetFont('dejavusans', '', 10);
            $txt_fin = 'El informe de resultados ' . $folio . ' consta de un total de ' . $pdf->getAliasNbPages() . ' páginas';
            $txt_fin2 = '---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
            $pdf->Cell(100, 50, $txt_fin, 0, $ln = 0, 'C', 0, '', 0, false, 'N', 'N');

            $pdf->SetXY(55, 215);
            $pdf->SetTextColor(255, 0, 0);
            $pdf->SetFont('dejavusans', '', 10);
            $pdf->Cell(100, 50, $txt_fin2, 0, $ln = 0, 'C', 0, '', 0, false, 'N', 'N');
            */
        }
    }
    //$pdf->writeHTML($htmlg, true, false, true, false, ''); 
    /*$pdf->SetXY(50, 150); 
    $pdf->SetTextColor(0,57,88); 
    $pdf->SetFont('dejavusans', '', 23); 
    $txt='Documento Informativo '.$folio; 
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/

    
    $pdf->SetXY(60, 251);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas.';
    $pdf->Cell(100, 10, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
    
    $pdf->SetXY(55, 255);
    $pdf->SetTextColor(255,0,0);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
    $pdf->Cell(100, 10, $txt_fin2,  0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
}


// add a new page for TOC 
$pdf->addTOCPage();
$pdf->SetTextColor(0, 0, 0);
// write the TOC title 
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page 
// (check the example n. 59 for the HTML version) 
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0)); 
$bookmark_templates[0] = '<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1] = '<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2] = '<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128, 0, 0));

// end of TOC page 
$pdf->endTOCPage();

$pdf->Output('EntregableNom15_elevadas.pdf', 'I');
