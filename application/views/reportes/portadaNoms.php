<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    foreach ($datosnom->result() as $item) {
        $folio=$item->num_informe;
    }

    foreach ($datosrec->result() as $item) {
        $empresa =$item->cliente;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    $GLOBALS['folio']=$folio;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        //if($this->PageNo()>1){
            $logos = base_url().'public/img/logo.jpg';
            $html = '<table width="100%"><tr><td style="font-size:15px"><img src="'.$logos.'" width="150px"></td><td></td><td style="" align="right"></td></tr></table>';
            $this->writeHTML($html, true, false, true, false, '');
        //}
    }
    // Page footer
    public function Footer() {
        
        
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Portada Entregable');
$pdf->SetTitle('Portada Entregable');
$pdf->SetSubject('Portada Entregable');
$pdf->SetKeywords('Portada');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,24,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setPrintFooter(true);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(15);

// set auto page breaks
$pdf->SetAutoPageBreak(false, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('calibri', '', 12);

$pdf->setPrintHeader(false);

$pdf->AddPage('P','A4'); 

/*$logos = base_url().'public/img/logo.jpg';
$html= '<table width="100%"><tr><td><img src="'.$logos.'" width="450px"></td><td></td><td style="" align="right"></td></tr></table>';
$pdf->writeHTMLCell(340, '', 50, 80, $html, 0, 0, 0, true, 'C', true);*/

$porta = base_url().'public/img/portada2_2.jpg';
$pdf->Image($porta, 0, 0, 210, 299, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

$html0='';
$html0.='<style type="text/css">
    .artext{font-size:20px; color: rgb(0,57,88); font-family: Calibri; font-weight:bold;}
    .emptext{font-size:16px; color:rgb(0,57,88); font-family: Calibri; font-weight:bold;}
    .emptext2{font-size:28px; color:rgb(0,57,88); font-family: Calibri; font-weight:bold;}
</style>';

$html='<style type="text/css">
    .inf2{font-size:38px; color:white; background-color: rgb(231,99,0); font-weight: bold;}
    .inf{font-size:38px; color:rgb(231,99,0); font-weight: bold;}
</style>';
$html.='
        <table width="100%" cellpadding="3" border="0">
            <tr>
                <td align="center" class="inf">INFORME DE RESULTADO DE ENSAYOS</td>
             </tr>
        </table>';

$pdf->writeHTMLCell(220, '',-1, 100, $html, 0, 0, 0, true, 'C', true);

$html0.='
        <table border="0" align="center" class="artext">
            <tr><td>EVALUACIÓN CONFORME A LA NORMA OFICIAL MEXICANA:</td></tr>
            <tr><td><u>'.$nom.'</u></td></tr> 
            <tr><td>'.$sub.'</td></tr> 
        </table>
        <table border="0" align="center" class="artext">
            <tr><td></td></tr>
        </table>
        <table border="0">
            <tr><td height="50px"></td></tr> 
        </table>
        <table border="0" align="center" class="emptext">
            <tr><td>REALIZADO PARA LA EMPRESA:</td></tr>
        </table>
        <table border="0" align="center" class="emptext2">
            <tr><td>'.$empresa.'</td></tr> 
        </table>';
//$pdf->writeHTML($html0, true, false, true, false, '');
$pdf->writeHTMLCell(188, '', 12, 120, $html0, 0, 0, 0, true, 'C', true);

$html0='<style type="text/css">
    .cmafolio{font-size:16px; color:rgb(231,99,0); font-weight: bold;}
</style>';
$html0.='
        <table width="100%" cellpadding="3" border="0">
            <tr>
                <td align="center" class="cmafolio">INFORME NO. '.$folio.' 
                </td>
             </tr>
        </table>';

$pdf->writeHTMLCell(200, '', 59, 287, $html0, 0, 0, 0, true, 'C', true);

$pdf->Output('Portada_Nom.pdf', 'I');

?>