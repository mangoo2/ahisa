<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $norma_name='NOM-022-STPS-2001';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);
    header('Content-Type: text/html; charset=UTF-8');
    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');
    mb_internal_encoding("UTF-8");
    
    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());
    $fechah = ucfirst($fechah);

    $firmaemple=""; $cedula=""; $firmaresp=""; $tecnico="";
    foreach ($datosoc->result() as $item) {
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $cedula=$item->cedula;
        $firmaresp=$item->firmaresp;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $estado_name=utf8_decode($estado_name);
        $direccion = mb_strtoupper($item->calle_num.', <br> '.($item->colonia).', <br>'.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $direccion2 = mb_strtoupper($item->calle_num.', '.($item->colonia).', '.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=($item->representa);
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    //$direccion=preg_replace('/[^(\x20-\x7F)]*/','', $direccion);
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $logos = base_url().'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
    }
    // Page footer
    public function Footer() {
        $folio=$GLOBALS['folio'];
        $html = '
        <style type="text/css">
            .cmac{font-size:8px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
            .cma{font-size:14px; color:rgb(231,99,0);}
            .cmafolio{font-size:8px; background-color: rgb(231,99,0); font-weight: bold;}
            .footerpage{font-size:8px;font-style: italic;}
        </style> 
      <table width="100%" cellpadding="2" border="0"><tr><td align="center" colspan="2" style="font-size:9px;font-style: italic;">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr><tr><td align="center" class="cmac" width="82%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx</td><td align="center" style="margin-top:150px" class="cmafolio" width="18%"><span class="cma">•</span>'.$folio.'</td></tr><tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$folio.' </td></tr></table>';
      //log_message('error',$html);
      //$this->writeHTML($html, true, false, true, false, '');
      $this->writeHTMLCell(188, '', 12, 274, $html, 0, 0, 0, true, 'C', true);
      $GLOBALS["tot_pgs"] = $this->getAliasNbPages();
    }

} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-22');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-22');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,25,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 28);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page
$pdf->AddPage('P', 'A4'); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style type="text/css">
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:15px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
    .sangria{text-indent: 45px;}
    .fontbold{font-weight: bold;}
    .algc{text-align:center; }
    .ft09{font-size:9px;}
    .ft20{font-size:20px;}
    .ft18{font-size:18px;}
    .ft16{font-size:16px;}
    .ft14{font-size:14px;}
    .ft13{font-size:13px;}
    .ft12{font-size:12px;}
    .ft11{font-size:11px;}
</style>';
$htmlg.='<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <table border="0">
            <tr>
                <td colspan="2" align="right" class="ft20 fontbold">'.($empresa).'</td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td width="50%" ></td>
                <td width="50%" align="right" class="ft14 fontbold">'.($direccion).'</td>
            </tr>
        </table>
        <table border="0">
            <tr><td></td></tr>
            <tr>
                <td class="fontbold ft16"><b>'.($nom_cargo).'</b></td>
            </tr>
            <tr>
                <td class="fontbold ft13"><b>'.($cargo_cargo).'</b></td>
            </tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes al valor de la resistencia de la red de puesta a tierra, la comprobación de la continuidad eléctrica y, en su caso, de la humedad relativa en su centro de trabajo; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fecha.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="160px" colspan="2"></td></tr>
            <tr><td>Ing. Christian Uriel Fabian Romero</td><td>Ing. Tanya Alejandra Valle Tello</td></tr>
            <tr><td>Aprobó - Gerente General</td><td>Revisó – Procesamiento de Información</td></tr>
            <tr><td colspan="2" height="160px"></td></tr>
            <tr><td colspan="2"><img src="'.FCPATH.'public/img/logo_ema.jpg" width="180px"></td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error', 'htmlg: '.$htmlg);

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1));
foreach ($rowpersonal->result() as $itemp) { 
    $dosis[$itemp->num]=$itemp->dosis;                                                   
}

$htmlg='<style type="text/css">
            td{font-family:calibri;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
            .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;}
            .ft09{font-size:9px;}
            .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:11px;
                    font-size:8px;
                    valign: middle;
                    vertical-align: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:8px;
                }
                .pspaces{
                    font-size:0.2px;    
                }
            .txt_td{background-color:rgb(217,217,217);}
            .sangria{text-indent: 45px;}
            b{font-family:Arialb;}
            .title_tb{ color: #548235; font-weight: bold;} 
        </style>
            <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
            
            <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>'.utf8_encode($empresa).'</b>, el día '.$fecha.'.</p>
            <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p>
            <p class="algc"><i>•  Valor de resistencia a tierra.</i></p>';
            $htmlg.='<table class="tableb" cellpadding="5">
                    <thead>
                        <tr>
                            <th width="10%"><p class="pspaces"></p>No. de punto</th>
                            <th width="40%"><p class="pspaces"></p>Identificación del punto de medición (área/ Identificación)</th>
                            <th width="10%">Valor de la<br>resistencia a tierra (Ω)</th>
                            <th width="10%"><p class="pspaces"></p>LMP (Ω)</th>
                            <th width="30%"><p class="pspaces"></p>Conclusión</th>
                        </tr>
                    </thead>';

                $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom22d',array('idnom'=>$idnom,'area!='=>"",'activo'=>1));
                            foreach ($rowpuntos->result() as $i) {
                                $val_lmp=0;
                                if($i->resistencia_corregida && $i->pararrayo==1 && $i->resistencia_corregida>10){
                                    $conclusion_ner='Supera el LMP';
                                }else{
                                    $conclusion_ner='No supera el LMP';
                                }
                                if($i->resistencia_corregida && $i->pararrayo==2 && $i->resistencia_corregida>25){
                                    $conclusion_ner='Supera el LMP';
                                }else{
                                    $conclusion_ner='No supera el LMP';
                                }
                                if($i->pararrayo==1){
                                    $val_lmp=10;
                                }else{
                                    $val_lmp=25;
                                }
                                $htmlg.='<tr>
                                        <td width="10%">'.$i->punto.'</td>
                                        <td width="40%">'.$i->area.' / '.$i->ubicacion.'</td>
                                        <td width="10%">'.$i->resistencia_corregida.'</td>
                                        <td width="10%">'.$val_lmp.'</td>
                                        <td width="30%" class="title_tb">'.$conclusion_ner.'</td>
                                     </tr>';                                
                            }
            $htmlg.='</table><p class="ft09 algc">LMP = Limite Máximo Permisible</p>';
    $htmlg.='
            <p class="ft09 algc"><u><i>Regla de Decisión</i></u></p>
            <p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';
        $result2=$this->ModeloNom->getAllFuentes($idnom);

    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->AddPage('P', 'A4');
    $htmlg='<style type="text/css">
            td{font-family:calibri;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
            .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;}
            .ft09{font-size:9px;}
            .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:11px;
                    font-size:8px;
                    valign: middle;
                    vertical-align: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:8px;
                }
                .pspaces{
                    font-size:0.2px;    
                }
            .txt_td{background-color:rgb(217,217,217);}
            .sangria{text-indent: 45px;}
            b{font-family:Arialb;}
            .title_tb{ color: #548235; font-weight: bold;} 
        </style>';
        $htmlg.='<p class="algc"><i>•  Comprobación de continuidad.</i></p><table class="tableb" cellpadding="5">
                <table class="tableb" cellpadding="5">
                    <thead>    
                        <tr>
                            <th width="15%">No. de <br>Punto de<br>continuidad</th>
                            <th width="50%"><p class="pspaces"></p>Fuente de generación y/o acumulación de cargas eléctricas estáticas</th>
                            <th width="20%"><p class="pspaces"></p>Número de electrodo al que se encuentra conectada la fuente</th>
                            <th width="15%"><p class="pspaces"></p>Valor de continuidad (Ω)</th>
                        </tr>
                    </thead>';
                    $cont_res=0;
                    foreach ($result2 as $i) {
                        $cont_res++;
                        if($cont_res<14 /*|| $cont_res>14*/){
                            $htmlg.='<tr>
                                    <td width="15%" class="tdtext1">'.$cont_res.'</td>
                                    <td width="50%" class="tdtext1">'.$i->fuente.'</td>
                                    <td width="20%" class="tdtext1">'.$i->punto.'</td>
                                    <td width="15%" class="tdtext1">'.$i->valor.'</td>
                                </tr>';
                        }
                        if($cont_res==14 || $cont_res==48){
                            //$pdf->AddPage('P', 'A4');
                            $htmlg.='<tr>
                                    <td style="border:none;" colspan="4"><br><br><br><br></td>
                                </tr>';
                        }if($cont_res>=14){
                            $htmlg.='<tr>
                                    <td width="15%" class="tdtext1">'.$cont_res.'</td>
                                    <td width="50%" class="tdtext1">'.$i->fuente.'</td>
                                    <td width="20%" class="tdtext1">'.$i->punto.'</td>
                                    <td width="15%" class="tdtext1">'.$i->valor.'</td>
                                </tr>';
                        }
                        /*$htmlg.='<tr>
                                    <td class="tdtext1">'.$i->punto.'</td>
                                    <td class="tdtext1">'.$i->fuente.'</td>
                                    <td class="tdtext1">1</td>
                                    <td class="tdtext1">'.$i->valor.'</td>
                                </tr>';*/
                    }
       $htmlg.='</table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlgtab="";
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .sangria{text-indent: 45px;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p>';

        $cont_concamb=0; $cont_concamb2=1;
        foreach ($get_conclu->result() as $c) {
            $chartSPT=""; $chartSPR=""; $chartCE="";
            if($c->total_electrodo_pt>0){
                if($c->chartSPT!="" && $c->chartSPT!="data:,"){
                    //log_message('error','CHARTSPT: '.$c->chartSPT);
                    //$chartSPT=$c->chartSPT; //280px
                    $chartSPT='<img width="265px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $c->chartSPT) . '">';
                }
                $cont_concamb++;
                $htmlg.='<p>1. Se evalúa un total de <strong>'.$c->total_electrodo_pt.'</strong> electrodo(s) en <strong>sistema de puesta a tierra</strong>, de los cuales: </p>
                    <p class="tdtext sangria">a) <strong>'.$c->no_superan_pt.'</strong> electrodo(s) no superan el límite máximo permisible (LMP) de 25 Ω.
                    </p>
                    <p class="tdtext sangria">b) <strong>'.$c->supera_pt.'</strong> electrodo(s) superan el límite máximo permisible (LMP) de 25 Ω.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">'.$chartSPT.'</td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }            
            if($c->total_electrodo_pr>0){
                $cont_concamb2++;
                if($c->chartSPR!="" && $c->chartSPR!="data:,"){
                    //$chartSPR=$c->chartSPR;
                    $chartSPR='<img width="265px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $c->chartSPR) . '">';
                }
                if($cont_concamb>0){
                    $cont_concamb++;
                    $ina="c)";
                    $inb="d)";
                }else{
                    $ina="a)";
                    $inb="b)";
                }
                $htmlg.='<p>'.$cont_concamb.'. Se evalúa un total de <strong>'.$c->total_electrodo_pr.'</strong> electrodo(s) en <strong>sistema de pararrayos</strong>, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina.' <strong>'.$c->no_supera_pr.'</strong> electrodo(s) no superan el límite máximo permisible (LMP) de 10 Ω.
                    </p>
                    <p class="tdtext sangria">'.$inb.' <strong>'.$c->supera_pr.'</strong> electrodo(s) superan el límite máximo permisible (LMP) de 10 Ω.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">'.$chartSPR.'</td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }if($c->total_ptos_continuidad>0){
                $cont_concamb2++;
                if($c->chartCE!="" && $c->chartCE!="data:,"){
                    //$chartCE=$c->chartCE;
                    $chartCE='<img width="265px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $c->chartCE) . '">';
                }
                if($cont_concamb2>0){
                    $ina2="e)";
                    $inb2="f)";
                }else{
                    $ina2="c)";
                    $inb2="d)";
                }
                $htmlg.='<p>'.$cont_concamb2.'. Se evalúa un total de <strong>'.$c->total_ptos_continuidad.'</strong> punto(s) de continuidad eléctrica, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina2.' En <strong>'.$c->existe_conti.'</strong> punto(s) existe continuidad eléctrica.
                    </p>
                    <p class="tdtext sangria">'.$inb2.' En <strong>'.$c->no_existe_conti.'</strong> punto(s) no existe continuidad eléctrica.
                    </p> 
                <table border="0" align="center">
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">'.$chartCE.'</td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }
        }//foreach
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb; font-size:20px; text-align:center;}
            .tdtext{font-family:Arial; text-align: justify; font-size:15px;}
        </style>
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
        <p class="tdtext">El fenómeno de la electricidad estática es un peligro presente en los procesos industriales donde se ocupe maquinaría, instrumentación y equipos que funcionen con energía eléctrica principalmente, otro factor a tener en cuenta es que, según la naturaleza de los materiales expuestos, existen diversas formas físicas de producirse las descargas electro estáticas, las cuales pueden ser generadas por la fricción del aire en superficies metálicas, procesado y transporte de sólidos o líquidos, desplazamiento de personas o equipos de trabajo sobre superficies aislantes o descargas atmosféricas que impacten directamente en el centro de trabajo.</p>
        <p class="tdtext">La acumulación y liberación de energía estática puede producir efectos de diversa índole, tanto sobre los trabajadores como en el entorno de trabajo. Estos efectos pueden generar accidentes graves, molestias o afectación al producto, por ello, es importante implementar un sistema de control de energía estática, el cual tendrá el objetivo de proporcionar seguridad a las personas, proteger las instalaciones, equipos y maquinaría utilizados para el desarrollo de las actividades laborales.</p>

        <p class="tdtext">El control de la electricidad estática puede realizar mediante la implementación de un sistema de red de puesta a tierra, el cual tendrá el objetivo de establecer un potencial de referencia para estabilizar la tensión eléctrica ante posibles descargas originadas por la maquinaria, equipo y/o descargas atmosféricas en el centro de trabajo.</p>
        <p class="tdtext">Para aquellas áreas cerradas donde la presencia de electricidad estática represente un riesgo de incendio, explosión o por que la naturaleza de sus actividades así lo demande, el control de electricidad estática podrá llevarse a cabo mediante el monitoreo de humedad relativa.</p>

        <p class="tdtext">Como parte del seguimiento a un sistema de red de puesta a tierra se debe realizar una evaluación periódica al sistema de red de puesta a tierra, en el cual se verifica la resistividad de la puesta a tierra y la existencia de continuidad de la maquinaría y/o los equipos aterrizados. Si la medida de controlar las descargas de electricidad estática es la humedad relativa ésta deberá ser monitoreada.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error', 'htmlg de intriduccion: '.$htmlg);

$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado -----------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

$htmlg='<style type="text/css">
            .tdtext{font-family:Arial; text-align: justify; font-size:14.5px;}
            .tdtext1{font-family:Arial; text-align: center; font-size:11px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
            .sp_bck{ font-weight:bold;}
        </style>
        <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>
        <p class="tdtext">Conocer el valor de la resistencia eléctrica de los electrodos instalados en la red puesta a tierra del centro de trabajo y corroborar la correcta conexión entre fuente generadora de electricidad estática y electrodo para determinar la existencia de continuidad; en caso de requerirse, determinar la humedad relativa de cada área donde se determine que esto es la forma de controlar la electricidad del centro de trabajo denominado <span class="sp_bck">'.utf8_encode($empresa).'</span>
        </p>
        <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>
        <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Condiciones de seguridad".</p>
        <table>
            <tr><td class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO*</b></td></tr>
            <tr><td ></td></tr>
        </table>
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class="cet" width="59%">'.($empresa).'</td>
            </tr>
            <tr>
                <td class="txt_td"><br><br><i>b)  Domicilio</i></td>
                <td class="cet">'.($direccion2).'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class="cet">'.$rfccli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d)  Giro y/o actividad principal</i></td>
                <td class="cet">'.$girocli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e)  Responsable de la empresa</i></td>
                <td class="cet">'.($representacli).'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f)  Teléfono</i></td>
                <td class="cet"> '.$telefonocli.'</td>
            </tr>
        </table>
        <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr>
                <tr><td ></td></tr>
                <tr><td class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></td></tr>
                <tr><td ></td></tr>
        </table>
        
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class=" cet" width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td>
            </tr>
            <tr>
                <td class="txt_td"><i>b)  Domicilio</i></td>
                <td class=" cet">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class=" cet">ALP160621FD6</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d) Teléfono</i></td>
                <td class=" cet">(01) 222 2265395</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e) e-mail </i></td>
                <td class="cet">gerencia@ahisa.mx</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f) Número de acreditación </i></td>
                <td class="cet">AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td>
            </tr>
            <tr>
                <td class="txt_td"><i>g) Número de aprobación</i></td>
                <td class="cet">LPSTPS-153/2022</td>
            </tr>
            <tr>
                <td class="txt_td"><i>h) Lugar de expedición del informe </i></td>
                <td class="cet">Puebla, Puebla - México </td>
            </tr>
            <tr>
                <td class="txt_td"><i>i) Fecha de expedición del informe de resultados.</i></td>
                <td class="cet">'.ucfirst(strtolower($fechah)).'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>j) Signatario responsable de la evaluación</i></td>
                <td class="cet">'.$tecnico.'</td>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');
//log_message('error', 'htmlg faltante: '.$htmlg);

$htmlg='';
$pdf->AddPage('P', 'A4');
$datosdet=$this->ModeloNom->getPaso2_3($GLOBALS['id_rec']);
$pdf->Bookmark('H. Metodología de para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
            .checked{font-size:17px;font-family:dejavusans;}
            .pspaces{font-size:0.2px;}
        </style>
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
        <p class="tdtext tejus">La medición de la resistencia a tierra de la red de puesta a tierra se deberá realizar conforme a lo siguiente:</p>

        <p class="tdtext tejus sangria">a) Verificar que el electrodo bajo prueba (que corresponde a la red de puesta a tierra) esté desconectado de la red de puesta a tierra, considerando lo siguiente:</p>
        <p class="tdtext tejus sangria">1) Realizar la desconexión de la red de puesta a tierra, con los equipos eléctricos desenergizados, y </p>
        <p class="tdtext tejus sangria">2) Efectuar la medición de la resistencia a tierra desconectando cada electrodo de forma individual, cuando ésta se realice en condiciones de operación normal, a fin de no desproteger a los trabajadores;</p>

        <p class="tdtext tejus sangria">b) Ajustar a cero la aguja del instrumento de medición analógico o verificar que la fuente de poder del equipo digital tenga suficiente energía para realizar el conjunto de mediciones;</p>
        <p class="tdtext tejus sangria">c) Aplicar el método de caída de tensión de la manera siguiente:</p>

        <p class="tdtext tejus sangria">1) Hacer circular una corriente entre dos electrodos: uno llamado C1 (que corresponde a la red de puesta a tierra) y un electrodo auxiliar denominado C2, mismo que se introduce al terreno a una distancia mínima de 20 metros de C1. Para realizar la primera medición se introduce en el terreno otro electrodo auxiliar llamado P1, a un metro de distancia de C1, entre el electrodo bajo prueba C1 y el electrodo auxiliar C2;</p>
        <p class="tdtext tejus sangria">2) Desplazar el electrodo auxiliar P1 de manera lineal a 3 metros de la primera medición y en dirección al electrodo auxiliar C2 para realizar la segunda medición, y </p>
        <p class="tdtext tejus sangria">3) Realizar las mediciones siguientes desplazando el electrodo auxiliar P1 cada 3 metros hasta complementar 19 metros. </p>

        <table border="0" align="center">
            <tr>
                <td width="25%"></td>
                <td width="50%"><img src="'.FCPATH.'/public/img/img1_nom22.jpg"></td>
                <td width="25%"></td>
            </tr>
        </table>
        <p></p><p></p>
        <p class="tdtext tejus sangria">d) Registrar los valores obtenidos de las mediciones;</p>
        <p class="tdtext tejus sangria">e) Elaborar una gráfica con base en los valores registrados;</p>
        <p class="tdtext tejus sangria">f) Obtener el valor de la resistencia a tierra de la red de puesta a tierra de la intersección del eje de resistencia con la parte paralela de la curva al eje de las distancias;</p>
        <p class="tdtext tejus sangria">g) Repetir las mediciones alejando el electrodo C2 del electrodo C1, cuando la curva obtenida no presente un tramo paralelo, hasta obtener valores paralelos al eje de las distancias, y </p>
        <p class="tdtext tejus sangria">h) Verificar que los valores de la resistencia a tierra, de la red de puesta a tierra que se obtengan en esta prueba, sean menores o iguales a 10 ohms para el (los) electrodo(s) del sistema de pararrayos, y/o tener un valor menor o igual a 25 ohms para la resistencia a tierra de la red de puesta a tierra. </p><p><br></p>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0)); //trar los intrumentos utilizados en levantamiento
        $htmlg='<style type="text/css">
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td{border:1px solid #808080;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
                .ft13{font-size:13px;}
                .ft11{font-size:11.5px;} 
                .div_diso{ float:left !important; }
                .pspaces{ font-size:0.5px; }
            </style>
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table cellpadding="5" align="center" class="tableb ft13" width="100%">';

        //trae los equipos utilizados
        $getson = $this->ModeloNom->getEquiposNom22($idnom);
        foreach($getson as $s){
            $id_multimetro=$s->id_multimetro;
            $id_medidor=$s->id_medidor;
            $id_set_resis=$s->id_set_resis;
            $htmlg.='
                <tr class="txt_td">
                    <td></td><td><i>Medidor de resistencia de tierras </i></td>
                    <td><i>Óhmetro (multímetro)</i></td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>
                    <td >'.$s->marcacal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>
                    <td >'.$s->modelocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>
                    <td >'.$s->equipocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>No. de Informe de calibración</i></td><td class="ft11">'.$s->no_informe_calibracion.'</td>
                    <td class="ft11" >'.$s->no_informe_calibracioncal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>
                    <td >'.$s->fecha_calibracioncal.'</td>
                </tr>';
        }
        $htmlg.='</table><p></p>';

        $pdf->Bookmark('J. Características del sistema de pararrayos utilizado --------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
        $htmlg.='<p class="titulosa"><b>J. CARACTERÍSTICAS DEL SISTEMA DE PARARRAYOS UTILIZADO</b></p>
                <table class="tableb ft11" align="center" width="100%" cellpadding="5">
                    <thead>
                        <tr class="txt_td">
                            <td width="20%"><p class="pspaces"></p>Área de localización</td>
                            <td width="20%"><p class="pspaces"></p>Tipo de sistema de pararrayos</td>
                            <td width="15%">Altura de las terminales aéreas</td>
                            <td width="30%"><p class="pspaces"></p>Ubicación (m)</td>
                            <td width="15%">Área de cobertura de protección (m<sup>2</sup>)</td>
                        </tr>
                    </thead>';

        $pr=$this->ModeloNom->getSisParaRayoNom22($idnom);
        $cont_pr=0;
        foreach ($pr as $i) {
            $htmlg.='<tr>
                    <td width="20%" >'.$i->area.'</td>
                    <td width="20%" >'.$i->tipo_sist.'</td>
                    <td width="15%" >'.$i->altura.'</td>
                    <td width="30%" >'.$i->ubicacion.'</td>
                    <td width="15%" >'.$i->area_cober.'</td>
                 </tr>';
            $cont_pr++;
            if($cont_pr==3){
                $pdf->AddPage('P', 'A4');
            }                                
        }
        if($cont_pr==0){
            $htmlg.='<tr>
                <td width="20%"></td>
                <td width="20%"></td>
                <td width="15%"></td>
                <td width="30%"></td>
                <td width="15%"></td>
            </tr>';
        }
        $htmlg.='</table>';
        
//log_message('error', 'htmlg: '.$htmlg);
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('K. Vigencia del informe ----------------------------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>K. VIGENCIA DEL INFORME</b></p>
        <p class="tdtext">La vigencia del informe de resultados cuando éstos sean favorables será de un año, siempre y cuando no sean modificadas las condiciones que sirvieron para su emisión.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0,0,0)); 
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';

$pdf->AddPage('P', 'A4');
$dc=$datosrec->row();
$num_electro_tierra="---";
$num_electro_pararayo="---";
$num_area_humedad="---";
$diagrama="---";
$observaciones="---";

if($dc->num_electro_tierra!="")
    $num_electro_tierra=$dc->num_electro_tierra;
if($dc->num_electro_pararayo!="")
    $num_electro_pararayo=$dc->num_electro_pararayo;
if($dc->num_area_humedad!="")
    $num_area_humedad=$dc->num_area_humedad;
if($dc->diagrama!="")
    $diagrama=$dc->diagrama;
if($dc->observaciones!="")
    $observaciones=$dc->observaciones;

//log_message('error', 'firmaemple: '.$firmaemple);
/*if(isset($firmaemple) && $firmaemple!="data:"){
    $firm_txt = FCPATH."uploads/firmas/".$firmaemple;
    $handle = @fopen($firm_txt, 'r');
    if($handle){
        $fh = fopen(FCPATH."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh); 
    }else{
        $linea="#";
    }  
}else{
    $linea='#';
}
//log_message('error', 'linea: '.$linea);
if(isset($firmaresp) && $firmaresp!="" && $firmaresp!="data:"){
    $firm_txt2 = FCPATH."uploads/firmas/".$firmaresp;
    $handle2 = @fopen($firm_txt2, 'r');
    if($handle2){
        $fh2 = fopen(FCPATH."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
        $firmaresp_line = fgets($fh2);
        fclose($fh2); 
    }else{
        $firmaresp_line="";
    }
}else{
    $firmaresp_line='';
}*/

$htmlg='<style type="text/css"> 
    .borderbottom{border-bottom:1px solid black;}
    .backg{background-color:rgb(217,217,217);}
    .alg{ text-align:center; }
    .ft9{ font-size:9px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
 </style>';
$htmlg.='<table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2"><img src="'.FCPATH.'public/img/logo.jpg" ></td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL ELECTRICIDAD ESTÁTICA</td>
                <td>REG-TEC/04-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>
        <table border="0" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="20%"><b>No. DE INFORME</b></td>
                <td width="50%" class="borderbottom">'.$dc->num_informe_rec.'</td>
                <td width="10%"><b>Fecha:</b></td>
                <td width="20%" class="borderbottom">'.$dc->fecha.'</td>
            </tr>
            <tr>
                <td width="20%"><b>RAZÓN SOCIAL</b></td>
                <td width="80%" class="borderbottom">'.$dc->cliente.'</td>
            </tr>
            <tr >
                <td width="60%" class="borderbottom">'.$dc->calle_num.'</td>
                <td width="40%" class="borderbottom">'.$dc->colonia.'</td>
            </tr>
            <tr>
                <td width="60%" ><b>CALLE Y NUMERO</b></td>
                <td width="40%" ><b>COLONIA</b></td>
            </tr>
            <tr >
                <td width="34%" class="borderbottom">'.$dc->poblacion.'</td>
                <td width="33%" class="borderbottom">'.mb_strtoupper($estado_name,"UTF-8").'</td>
                <td width="33%" class="borderbottom">'.$dc->cp.'</td>
            </tr>
            <tr>
                <td width="34%" ><b>MUNICIPIO</b></td>
                <td width="33%" ><b>ESTADO</b></td>
                <td width="33%" ><b>CÓDIGO POSTAL</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->rfc.'</td>
                <td width="50%" class="borderbottom">'.$dc->giro.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>RFC</b></td>
                <td width="50%" ><b>GIRO DE LA EMPRESA</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->telefono.'</td>
                <td width="50%" class="borderbottom">'.$dc->representa.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>NÚMERO TELEFÓNICO</b></td>
                <td width="50%" ><b>REPRESENTANTE LEGAL</b></td>
            </tr>
            <tr>
                <td width="100%" class="borderbottom">'.$dc->nom_cargo.'</td>
            </tr>
            <tr>
                <td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td>
            </tr>
        </table>                
        <p></p>
        <table class="table ft9" border="1" align="center"> 
            <tr style="color: black;" class="ft9 fontbold alg">
                <td width="33.33%" class="backg titulosC">No. DE ELECTRODOS DE PUESTA TIERRA A EVALUAR</td>
                <td width="33.33%" class="backg titulosC">No. DE ELECTRODOS DE PARARRAYOS</td>
                <td width="33.33%" class="backg titulosC">No. DE ÁREAS A EVALUAR HUMEDAD RELATIVA</td>
            </tr>
            <tr>
                <td>'.$num_electro_tierra.'</td>
                <td>'.$num_electro_pararayo.'</td>
                <td>'.$num_area_humedad.'</td>
            </tr>
 
        </table>
        <p></p>
        <table class="table ft9" border="1">
            <tr align="center">
                <td class="backg"><b>DIAGRAMA UNIFILIAR O LAY OUT CON LA UBICACIÓN DE LOS PUNTOS DE MEDICIÓN DEL SISTEMA DE PUESTA A TIERRA, SISTEMA DE PARARRAYOS O ÁREAS A EVALUAR HUMEDAD RELATIVA </b></td>
            </tr>
            <tr>
                <td height="300px"><div height="150px"><br><br><br><br><br><br><br></div>'.$diagrama.'</td>
            </tr>
        </table>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr><td class="backg" width="50%"><b>OBSERVACIONES</b></td>
                <td width="50%">'.$observaciones.'</td>
            </tr>
            <tr>
                <td class="backg" width="50%"><p class="pspaces"></p><b>Nombre y firma del ingeniero de campo</b></td>
                <td width="50%"></td>
            </tr>
            <tr>
                <td class="backg" width="50%"><p class="pspaces"></p><b>Nombre y firma del responsable de la revisión del registro de campo</b></td>
                <td width="50%"></td>
            </tr>
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    //log_message('error', 'htmlg Reconocimiento: '.$htmlg);
/************************** *******************************/

$pdf->AddPage('P', 'A4');
$htmlg='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:rgb(217,217,217);}
                .algc{text-align:center; }
                .ft9{font-size:9px;}
                .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:9px;
                    font-size:9px;
                    valign: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:9px;
                }
                .pspaces{
                    font-size:0.2px;    
                }
                .checked{font-size:13px;font-family:dejavusans;}
        </style>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.FCPATH.'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>HOJA DE CAMPO NOM-022-STPS-2015</td>
                <td>REG-TEC/04-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <table><tr><td></td></tr></table>
        <table class="tableb" cellpadding="5">';
            $id_medidor=0;
            foreach ($datosnom->result() as $item) {
                $id_medidor=$item->id_medidor;
                $id_multimetro=$item->id_multimetro;
                $htmlg.='<tr>
                            <th>NO. DE INFORME</th>
                            <td colspan="3">'.$item->num_informe.'</td>
                            <th colspan="2">FECHA DE EMISIÓN</th>
                            <td colspan="2">'.$item->fecha.'</td>
                        </tr>
                        <tr>
                            <th>RAZÓN SOCIAL</th>
                            <td colspan="7">'.$item->razon_social.'</td>
                        </tr>';
            }
            $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_medidor));
            foreach ($resultequipo->result() as $iteme) {
                $htmlg.='<tr>
                    <th width="5%">ID</th>
                    <td width="20%">'.$iteme->luxometro.'</td>
                    <th width="10%">MARCA</th>
                    <td width="15%">'.$iteme->marca.'</td>
                    <th width="10%">MODELO</th>
                    <td width="15%">'.$iteme->modelo.'</td>
                    <th width="10%">SERIE</th>
                    <td width="15%">'.$iteme->equipo.'</td>
                </tr>';
            }
            $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_multimetro));
            foreach ($resultequipo->result() as $iteme) {
                $htmlg.='<tr>
                    <th width="5%">ID</th>
                    <td width="20%">'.$iteme->luxometro.'</td>
                    <th width="10%">MARCA</th>
                    <td width="15%">'.$iteme->marca.'</td>
                    <th width="10%">MODELO</th>
                    <td width="15%">'.$iteme->modelo.'</td>
                    <th width="10%">SERIE</th>
                    <td width="15%">'.$iteme->equipo.'</td>
                </tr>';
            }
            $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_set_resis));
            foreach ($resultequipo->result() as $iteme) {
                $htmlg.='<tr>
                    <th width="5%">ID</th>
                    <td width="20%">'.$iteme->luxometro.'</td>
                    <th width="10%">MARCA</th>
                    <td width="15%">'.$iteme->marca.'</td>
                    <th width="10%">MODELO</th>
                    <td width="15%">'.$iteme->modelo.'</td>
                    <th width="10%">SERIE</th>
                    <td width="15%">'.$iteme->equipo.'</td>
                </tr>';
            }
        $htmlg.='</table>';
        
        foreach ($datosnom->result() as $item) {
            $htmlg.='<table class="tableb" cellpadding="5">
      
                    <tr>
                        <th width="10%">VALOR DE REFERENCIA</th>
                        <th width="15%">VERIFICACIÓN<br> INICIAL</th>
                        <th width="10%"><p class="pspaces"></p>CUMPLE</th>
                        <th width="15%">VERIFICACIÓN<br> FINAL</th>
                        <th width="10%"><p class="pspaces"></p>CUMPLE</th>
                        <th width="25%"><p class="pspaces"></p>CRITERIO</th>
                        <th width="15%"><p class="pspaces"></p>VALOR RK</th>
                    </tr>
                    <tr>
                        <td>'.$item->valor_ref.'</td>
                        <td>'.$item->veri_ini.'</td>
                        <td>'.$item->cumple_criterio.'</td>
                        <td>'.$item->veri_fin.'</td>
                        <td>'.$item->cumple_criterio_fin.'</td>
                        <td>'.$item->criterio.'</td>
                        <td>'.$item->valor_rk.'</td>
                    </tr>
                </table>
                <table class="tableb">
                    <tr>
                        <th><p class="pspaces"></p>Nombre y Firma del Ingeniero de campo</th>
                        <td><p class="pspaces"></p></td>
                        <th><p class="pspaces"></p>Nombre y Firma del responsable de la revisión de campo</th>
                        <td><p class="pspaces"></p></td>
                    </tr>
                </table>';
        }
        $descn=""; $descs=""; $cont_pts=0;
        //$rowpuntos=$this->ModeloCatalogos->getselectwheren('nom22d',array('idnom'=>$idnom,'activo'=>1));
        $rowpuntos=$this->ModeloCatalogos->getselectwherenOrder('nom22d',array('idnom'=>$idnom,"r1>"=>0,'activo'=>1),"punto","asc");
        //log_message('error','ROW: '.json_encode($rowpuntos->result()));
        foreach ($rowpuntos->result() as $item) { 
            $cont_pts++;
            $punto=$item->punto; $idnomd=$item->id;
            if($item->desconectado=="1"){
                $descs='<p class="checked">✔</p>';
                $descn="---";
            }else if($item->desconectado=="2"){
                $descn='<p class="checked">✔</p>';
                $descs="---";
            }
            $area="---";
            $ubicacion="---";
            if($item->area!="")
                $area=$item->area;
            if($item->ubicacion!="")
                $ubicacion=$item->ubicacion;

            if($cont_pts==1){
                $htmlg.='<table><tr><td></td></tr></table>';
            }
            if($cont_pts==3 || $cont_pts>2 && $cont_pts%2==true){
                //$htmlg.='</tbody></table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');
                $pdf->AddPage('P', 'A4');
                $htmlg="";
                $htmlg.='
                    <style type="text/css">
                            .borderbottom{border-bottom:1px solid black;}
                            .backg{background-color:rgb(217,217,217);}
                            .algc{text-align:center; }
                            .ft9{font-size:9px;}
                            .tableb td{
                                border:1px solid #808080;
                                font-family:Arial;text-align: center;font-size:9px;
                                font-size:9px;
                                valign: middle;
                            }
                            .tableb th{
                                background-color:rgb(217,217,217);
                                border:1px solid #808080;  
                                text-align:center; 
                                font-size:9px;
                            }
                            .pspaces{font-size:0.2px;    }
                            .checked{font-size:13px;font-family:dejavusans;}
                    </style>
                    <table border="1" cellpadding="5" align="center" class="ft9">
                        <tr>
                            <td width="24%" rowspan="2">
                                <img src="'.FCPATH.'public/img/logo.jpg" >
                            </td>
                            <td width="19%">NOMBRE DEL DOCUMENTO</td>
                            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                            <td width="19%">VERSIÓN</td>
                            <td width="19%">No COPIA CONTROLADA</td>
                        </tr>
                        <tr>
                            <td>HOJA DE CAMPO NOM-022-STPS-2015</td>
                            <td>REG-TEC/04-01</td>
                            <td>01</td>
                            <td>ORIGINAL</td>
                        </tr>
                    </table>
                <table><tr><td></td></tr></table>';
            }
            /*if($cont_pts==3){
                $htmlg.='<table><tr><td></td></tr></table>';
            }*/
            if($item->d1>0) $d1=$item->d1; else $d1="---";
            if($item->d2>0) $d2=$item->d2; else $d2="---";
            if($item->d3>0) $d3=$item->d3; else $d3="---";
            if($item->d4>0) $d4=$item->d4; else $d4="---";
            if($item->d5>0) $d5=$item->d5; else $d5="---";
            if($item->d6>0) $d6=$item->d6; else $d6="---";
            if($item->d7>0) $d7=$item->d7; else $d7="---";
            if($item->d8>0) $d8=$item->d8; else $d8="---";
            if($item->d9>0) $d9=$item->d9; else $d9="---";

            if($item->r1>0) $r1=$item->r1; else $r1="---";
            if($item->r2>0) $r2=$item->r2; else $r2="---";
            if($item->r3>0) $r3=$item->r3; else $r3="---";
            if($item->r4>0) $r4=$item->r4; else $r4="---";
            if($item->r5>0) $r5=$item->r5; else $r5="---";
            if($item->r6>0) $r6=$item->r6; else $r6="---";
            if($item->r7>0) $r7=$item->r7; else $r7="---";
            if($item->r8>0) $r8=$item->r8; else $r8="---";
            if($item->r9>0) $r9=$item->r9; else $r9="---";
            $htmlg.='<table class="tableb" cellpadding="5">
                        <tr>
                            <th width="15%">No. de Punto</th>
                            <td width="15%">'.$item->punto.'</td>
                            <th width="10%">Área</th>
                            <td colspan="4" width="60%">'.$area.'</td>
                        </tr>
                        <tr>
                            <th width="20%">Ubicación del electrodo</th>
                            <td width="40%">'.$ubicacion.'</td>
                            <th width="20%">¿Electrodo desconectado?</th>
                            <th width="5%">Si</th>
                            <td width="5%">'.$descs.'</td>
                            <th width="5%">No</th>
                            <td width="5%">'.$descn.'</td>
                        </tr>
                    </table>
                    <table class="tableb" cellpadding="5">
                        <thead><tr><th colspan="10"><b>VALORES DE RESISTENCIA</b></th></tr></thead>
                        <tbody>
                            <tr>
                                <th>Distancia(m)</th>
                                <td>'.$d1.'</td>
                                <td>'.$d2.'</td>
                                <td>'.$d3.'</td>
                                <td>'.$d4.'</td>
                                <td>'.$d5.'</td>
                                <td>'.$d6.'</td>
                                <td>'.$d7.'</td>
                                <td>'.$d8.'</td>
                                <td>'.$d9.'</td>
                            </tr>
                            <tr>
                                <th class="tdinput">Resistencia(Ω)</th>
                                <td>'.$r1.'</td>
                                <td>'.$r2.'</td>
                                <td>'.$r3.'</td>
                                <td>'.$r4.'</td>
                                <td>'.$r5.'</td>
                                <td>'.$r6.'</td>
                                <td>'.$r7.'</td>
                                <td>'.$r8.'</td>
                                <td>'.$r9.'</td>
                            </tr>
                        </tbody>
                    </table>';
            //$get_fts=$this->ModeloCatalogos->getselectwheren('fuentes_genera_nom22',array('idnom'=>$idnom,'punto'=>$punto,'activo'=>1));
            $get_fts=$this->ModeloNom->getFuentesPDFNom22($idnom,$idnomd,$punto);
            foreach ($get_fts as $f) { 
                if($f->id_fuente!=0 || $f->id_fuente2!=0 || $f->id_fuente3!=0 || $f->id_fuente4!=0){
                    $fuente="---";
                    $valor="---";
                    $caracteristica="---";
                    $fuente2="---";
                    $valor2="---";
                    $caracteristica2="---";
                    $fuente3="---";
                    $valor3="---";
                    $caracteristica3="---";
                    $fuente4="---";
                    $valor4="---";
                    $caracteristica4="---";

                    $medicion_humedad="---";
                    $observaciones="---";
                    
                    if($f->fuente!="")
                        $fuente=$f->fuente;
                    if($f->valor!="")
                       $valor= $f->valor;
                    if($f->caracteristica!="")
                        $caracteristica=$f->caracteristica;
                    if($f->fuente2!="")
                        $fuente2=$f->fuente2;
                    if($f->valor2!="")
                       $valor2= $f->valor2;
                    if($f->caracteristica2!="")
                        $caracteristica2=$f->caracteristica2;
                    if($f->fuente3!="")
                        $fuente3=$f->fuente3;
                    if($f->valor3!="")
                       $valor3= $f->valor3;
                    if($f->caracteristica3!="")
                        $caracteristica3=$f->caracteristica3;
                    if($f->fuente4!="")
                        $fuente4=$f->fuente4;
                    if($f->valor4!="")
                       $valor4= $f->valor4;
                    if($f->caracteristica4!="")
                        $caracteristica4=$f->caracteristica4;
                    if($f->medicion_humedad!="")
                       $medicion_humedad= $f->medicion_humedad;
                    if($f->observaciones!="")
                        $observaciones=$f->observaciones;

                    $htmlg.='<table class="tableb" cellpadding="5">
                        <thead>
                            <tr><th colspan="4"><b>FUENTES GENERADORAS CONECTADAS AL ELECTRODO</b></th></tr>
                            <tr>
                                <th width="30%"><p class="pspaces"></p>FUENTE</th>
                                <th width="15%">VALOR DE<br> CONTINUIDAD</th>
                                <th width="55%" colspan="2"><p class="pspaces"></p>CARACTERÍSTICAS DEL SISTEMA DE PARARRAYOS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="30%">'.$fuente.'</td>
                                <td width="15%">'.$valor.'</td>
                                <td width="25%"><b>Tipo de sistema de pararrayos</b></td>
                                <td width="30%">'.$caracteristica.'</td>
                            </tr>
                            <tr>
                                <td width="30%">'.$fuente2.'</td>
                                <td width="15%">'.$valor2.'</td>
                                <td width="25%"><b>Ubicación del pararrayos</b></td>
                                <td width="30%">'.$caracteristica2.'</td>
                            </tr>
                            <tr>
                                <td width="30%">'.$fuente3.'</td>
                                <td width="15%">'.$valor3.'</td>
                                <td width="25%"><b>Altura de las terminales aéreas (m)</b></td>
                                <td width="30%">'.$caracteristica3.'</td>
                            </tr>
                            <tr>
                                <td width="30%">'.$fuente4.'</td>
                                <td width="15%">'.$valor4.'</td>
                                <td width="25%"><b>Área de cobertura de protección (m<sup>2</sup>)</b></td>
                                <td width="30%">'.$caracteristica4.'</td>
                            </tr>
                        </tbody>
                    </table>'; 
                            
                    /*if($cont_pts>4){
                        $htmlg.='<tr><td><br><br></td></tr>';
                    }*/
                    $htmlg.='<table class="tableb" cellpadding="5">
                        <tr>
                            <th width="35%">MEDICIÓN DE HUMEDAD RELATIVA (%)</th>
                            <th colspan="3" width="65%">OBSERVACIONES</th>
                        </tr>
                        <tr>
                            <td width="35%">'.$medicion_humedad.'</td>
                            <td colspan="3" width="65%">'.$observaciones.'</td>
                        </tr>
                    </table>
                    <table><tr><td></td></tr></table>';
                }
            }
        }

    $pdf->writeHTML($htmlg, true, false, true, false, '');
    //log_message('error', 'htmlg: '.$htmlg);
//============================================

$pdf->AddPage('P', 'A4');
$htmlg="";
$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27,15);
$pdf->Bookmark('II. Planos ----------------------------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table>
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO II</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PLANOS</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$cont_plan=0;
foreach ($datosnomplanos->result() as $item) {
    $pdf->AddPage('P', 'A4');
    $cont_plan++;
    
    /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
    $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
}
if($cont_plan==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$htmlg.='';
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo ---------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO III</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">SECUENCIA DE CÁLCULO </td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$htmlg=''; $equipo=0;
$htmlg.='<style type="text/css">
    .titulos{background-color: rgb(217,217,217);}
    .table-bordered th, .table-bordered td {border: 1px solid #c6c9cb;}
    .table thead th {border-bottom: 2px solid #c6c9cb;}
    .table td{padding: 0.5rem;font-size: 9.5px;vertical-align: middle;text-align: center;}
    .table th{padding: 0.5rem;font-size: 10.5px;vertical-align: middle;text-align: center;}

    .title_tb{ color: #548235; font-weight: bold;}  
    .pmed{font-family:Arial; font-size:7.5px; text-align:center;}
    .pmed2{font-family:Arial; font-size:6.5px; text-align:center;}
</style>';
foreach ($datosnom->result() as $item) { 
    $htmlg.='
        <table class="table table-bordered" border="1">
        <tr>
            <td class="titulos">Razón Social</td><td colspan="3" class="title_tb">'.$item->razon_social.'</td>
        </tr>
    </table>';
}
//====================================================================================================== 
/*$unidad2=2; $inter2=0.0203; $pend2=1.0014;
$unidad20=20; $inter20=0.0070; $pend20=0.9977;
$unidad200=200; $inter200=-0.0256; $pend200=0.9967;
$unidad2000=2000; $inter2000=-2.2912; $pend2000=1.0059;
$unidad20000=20000; $inter20000=-25.9980; $pend20000=1.0069;*/
$medicion=0; $cont_row=0; $img_grafica="";
    $tot=array(); $factor_corprom=0;
    if($id_higro>0){
        $get_hidro=$this->ModeloCatalogos->getselectwheren('equipo_higro_detalle',array('id_equipo'=>$id_higro));
        foreach($get_hidro->result() as $h){ 
            array_push($tot,round($h->correccion,5));
        }
        $factor_corprom=array_sum($tot) / count($tot);
        //log_message('error', 'factor_corprom: '.$factor_corprom);
    }
    //$rowpuntos=$this->ModeloCatalogos->getselectwheren('nom22d',array('idnom'=>$idnom,"r1>"=>0,'activo'=>1));
    $rowpuntos=$this->ModeloCatalogos->getselectwherenOrder('nom22d',array('idnom'=>$idnom,"r1>"=>0,"area!="=>"",'activo'=>1),"punto","asc");
    //log_message('error','rowPuntos: '.json_encode($rowpuntos->result()));
    foreach ($rowpuntos->result() as $itemfp) { 
        $idpunto=$itemfp->punto;                     
        $idnom=$itemfp->idnom;
        $grafica=$itemfp->grafica;
        //log_message('error','ID: '.json_encode($itemfp->id));
        //log_message('error','GRAFICA: '.json_encode($grafica));
        if($grafica!="" && $grafica!="data:,"){
            //$img_grafica='';
            $img_grafica = '<img height="185px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica) . '" >';
        }

        $array_rrs=array(); $array_rrs_graf=array();
        $getFts=$this->ModeloCatalogos->getselectwheren('fuentes_genera_nom22',array('id_nom'=>$idnom,'punto'=>$idpunto,"medicion_humedad!="=>'','activo'=>1));
        $ift=$getFts->row(); if($getFts->num_rows()>0 && $ift->medicion_humedad=="" || $getFts->num_rows()==0) $medicion="---"; else if($getFts->num_rows()>0 && $ift->medicion_humedad!="") $medicion=$ift->medicion_humedad;

        if($medicion!=0) $med_cor=number_format($medicion*$factor_corprom,3); else $med_cor="---";

        if($itemfp->rcf1>0) $rcf1=$itemfp->rcf1; else $rcf1="---";
        if($itemfp->rcf2>0) $rcf2=$itemfp->rcf2; else $rcf2="---";
        if($itemfp->rcf3>0) $rcf3=$itemfp->rcf3; else $rcf3="---";
        if($itemfp->rcf4>0) $rcf4=$itemfp->rcf4; else $rcf4="---";
        if($itemfp->rcf5>0) $rcf5=$itemfp->rcf5; else $rcf5="---";
        if($itemfp->rcf6>0) $rcf6=$itemfp->rcf6; else $rcf6="---";
        if($itemfp->rcf7>0) $rcf7=$itemfp->rcf7; else $rcf7="---";
        if($itemfp->rcf8>0) $rcf8=$itemfp->rcf8; else $rcf8="---";
        if($itemfp->rcf9>0) $rcf9=$itemfp->rcf9; else $rcf9="---";

        if($itemfp->r1>0) $r1=$itemfp->r1; else $r1="---";
        if($itemfp->r2>0) $r2=$itemfp->r2; else $r2="---";
        if($itemfp->r3>0) $r3=$itemfp->r3; else $r3="---";
        if($itemfp->r4>0) $r4=$itemfp->r4; else $r4="---";
        if($itemfp->r5>0) $r5=$itemfp->r5; else $r5="---";
        if($itemfp->r6>0) $r6=$itemfp->r6; else $r6="---";
        if($itemfp->r7>0) $r7=$itemfp->r7; else $r7="---";
        if($itemfp->r8>0) $r8=$itemfp->r8; else $r8="---";
        if($itemfp->r9>0) $r9=$itemfp->r9; else $r9="---";

        $cont_row++;
        if($cont_row>3 && $cont_row%3){
            $htmlg.='<table>
                    <tr>
                        <td></td>
                    </tr>
            </table>';
        }
        if($cont_row==1 || $cont_row>3 && $cont_row%3){
            $htmlg.='<table class="table table-bordered" border="1">
                    <tr>
                        <td class="titulos">Fecha de medición</td><td colspan="3" class="title_tb">'.$itemfp->fecha_pto.'</td>
                    </tr>
            </table>';
        }

        $htmlg.='<table><tr><td></td></tr></table>
            <table class="table table-bordered">
                <tr>
                    <th class="titulos">No. de Punto:</th>
                    <td class="title_tb">'.$idpunto.'</td>
                    <td colspan="2"> </td>
                    <th class="titulos">Area: </th>
                    <td class="title_tb" colspan="4">'.$itemfp->area.' / '.$itemfp->ubicacion.'</td>
                </tr>
            </table>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="24%" class="titulos">Distancia (m)</th>
                    <td width="5%" class="titulos">0</td>
                    <th width="8%" class="titulos">1</th>
                    <th width="8%" class="titulos">4</th>
                    <th width="8%" class="titulos">7</th>
                    <th width="8%" class="titulos">10</th>
                    <th width="8%" class="titulos">13</th>
                    <th width="8%" class="titulos">16</th>
                    <th width="8%" class="titulos">19</th>
                    <th width="5%" class="titulos">22</th>
                    <th width="5%" class="titulos">25</th>
                    <th width="5%" class="titulos">28</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th width="24%" class="titulos">Resistencia (Ω)</th>
                    <td width="5%"> 0 </td>
                    <td width="8%">'.$r1.'</td>
                    <td width="8%">'.$r2.'</td>
                    <td width="8%">'.$r3.'</td>
                    <td width="8%">'.$r4.'</td>
                    <td width="8%">'.$r5.'</td>
                    <td width="8%">'.$r6.'</td>
                    <td width="8%">'.$r7.'</td>
                    <td width="5%">'.$r8.'</td>
                    <td width="5%">'.$r9.'</td>
                    <td width="5%"> --- </td>
                </tr>
                <tr>
                    <th width="24%" class="titulos">Resistencia Corregida (Ω)</th>
                    <td width="5%"> 0</td>
                    <td width="8%">'.$rcf1.'</td>
                    <td width="8%">'.$rcf2.'</td>
                    <td width="8%">'.$rcf3.'</td>
                    <td width="8%">'.$rcf4.'</td>
                    <td width="8%">'.$rcf5.'</td>
                    <td width="8%">'.$rcf6.'</td>
                    <td width="8%">'.$rcf7.'</td>
                    <td width="5%">'.$rcf8.'</td>
                    <td width="5%">'.$rcf9.'</td>
                    <td width="5%"> --- </td>
                </tr>
            </tbody>
        </table>
        <table><tr><td></td></tr></table>
        <table>
            <tr>
                <th width="15%">
                    <table class="table table-bordered">
                        <tr>
                            <th class="titulos">Valor de la resistencia(Ω)</th>
                        </tr>
                        <tr>
                            <th>'.$itemfp->resistencia_corregida.'</th>
                        </tr>
                    </table>
                </th>
                <th width="5%" rowspan="3">
                    <table>
                        <tr>
                            <th><!--<p class="pmed2">R<br>E<br>S<br>I<br>S<br>T<br>E<br>N<br>C<br>I<br>A<br>(Ω)</p>--></th>
                        </tr>
                    </table>
                </th>
                <th rowspan="3" width="85%">
                    '.$img_grafica.'
                    <!--<p class="pmed">DISTANCIA (M)</p>-->
                </th>
            </tr>
            <tr>
                <th width="15%">
                    <table>
                        <tr><th></th></tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th class="titulos">Humedad Relativa</th>
                        </tr>
                        <tr>
                            <th>'.$medicion.'</th>
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                <th width="15%">
                    <table>
                        <tr><th></th></tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th class="titulos">Humedad Relativa <br>Corregida (%)</th>
                        </tr>
                        <tr>
                            <th>'.$med_cor.'</th>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>';   
    }
$pdf->writeHTML($htmlg, true, false, true, false, '');
//======================================================================================================

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Informe de calibración del equipo de medición ----------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IV</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">INFORME DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$datosequipoc=$this->ModeloNom->getCertEquipos($id_medidor,$id_multimetro);
foreach ($datosequipoc as $item) {
    $htmlg='';
    $pdf->AddPage('P', 'A4');
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgEquipos/'.$item->certificado.'"></td></tr></table>';
    log_message('error','IMGCertificado: '.$item->certificado);
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt='Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt=$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
}
 
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Documento de acreditación ------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO V</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE ACREDITACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

//log_message('error',"datosacre num_rows: ".$datosacre->num_rows());
if($datosacre->num_rows()>0){
    $htmlg='';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4'); 
        //log_message('error',"datosacre url_img: ".$item->url_img);
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);

        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
    }   
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('X. Documento de aprobación --------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO X</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE APROBACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$txt_fin=""; $cont_acre=0;
if($datosacre2->num_rows()>0){
    $htmlg='';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2=0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima
        $cont2++;
        $pdf->SetMargins(20,25,20); 
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->AddPage('P', 'A4');
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 0, false, false, true);
        log_message('error','acre: '.$item->url_img);
        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,210);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,222);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        if($cont_acre==$cont2){
            /*$txt_fin.='<table>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p>
                    </td>
                </tr>
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true);
            //$pdf->writeHTML($txt_fin, true, false, true, false, '');            
        }
    }

    //$pdf->AddPage('P', 'A4');
    //$pdf->SetXY(60, 221);
    //$pdf->Cell(100, 50, '',1, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

    $pdf->SetXY(60, 251);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas';
    $pdf->Cell(100, 10, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
    
    $pdf->SetXY(55, 255);
    $pdf->SetTextColor(255,0,0);
    $pdf->SetFont('dejavusans', '', 10);
    $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
    $pdf->Cell(100, 10, $txt_fin2,  0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');


    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    /*$pdf->SetXY(50, 150);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 23);
    $txt='Documento Informativo '.$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/
}


// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));

$bookmark_templates[0]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();



$pdf->Output('EntregableNom22.pdf', 'I');

?>