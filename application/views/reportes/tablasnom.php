<style type="text/css">
                .fosi9{font-size:9px;}
                .tableb td{border:1px solid #808080;font-family:Arial;text-align: center;font-size:9px;valign: middle;vertical-align: middle;}
                .tableb th{background-color:rgb(217,217,217);border:1px solid #808080;text-align:center;font-size:10px;}
                .pspaces{ font-size: 0.1px; }
            </style>
            <table align="center">
                <tr><td>•  Factor de Reflexión</td></tr>
            </table>
            <table class="tableb" align="center" cellpadding="3" width="100%">
                <thead>
                    <tr>
                        <th width="8%" rowspan="2">No. de Punto</th>
                        <th width="16%" rowspan="2">Identificación del punto de medición (área/identificación)</th>
                        <th width="8%" rowspan="2">Horario de medición</th>
                        <th width="34%" colspan="3">Reflexión plano de trabajo</th>
                        <th width="34%" colspan="3">Reflexión pared</th>
                    </tr>
                    <tr>
                        <th>Kf (%)</th>
                        <th>NMP (%)</th>
                        <th>Conclusión</th>
                        <th>Kf (%)</th>
                        <th>NMP (%)</th>
                        <th>Conclusión</th>
                    </tr>
                </thead>
                <tbody><tr>
                            <td width="8%" rowspan="3">1</td>
                            <td width="16%" rowspan="3">IDENTIFICA PUNTO DE ORD 4 RECONO 5</td>
                            <td width="8%">09:00</td>
                            <td width="12%">1685037.53</td>
                            <td width="8%" rowspan="3">50</td>
                            <td width="14%" style="color: red;font-weight: bolder;">Se supera al NMP</td>
                            <td width="12%">6009.70</td>
                            <td width="8%" rowspan="3">60</td>
                            <td width='14%' style="color: red;font-weight: bolder;">Se supera al NMP</td>
                        </tr>
                        <tr>
                            <td width="8%">15:00</td>
                            <td width="12%">111.18</td>
                            <td width="14%" style="color: red;font-weight: bolder;">Se supera al NMP</td>
                            <td width="8%">875.08</td>
                            <td width="14%" style="color: red;font-weight: bolder;">Se supera al NMP</td>
                        </tr>
                        <tr>
                            <td width="8%">16:00</td>
                            <td width="12%">38.85</td>
                            <td width="14%" style="color: green;font-weight: bolder;">No se supera al NMP</td>
                            <td width="8%">100.00</td><td width="14%" style="color: red;font-weight: bolder;">Se supera al NMP</td>
                        </tr>
                    </tbody>
            </table> 
            <table align="center">
                <!--<tr><td>•   Factor de Reflexión</td></tr>
                <tr><td></td></tr>-->
                <tr><td class="fosi9"><i>NMP = Nivel Máximo Permisible</i></td></tr>
                <tr><td class="fosi9"><i>Kf = Factor de reflexión de la superficie </i></td></tr>
            </table>
