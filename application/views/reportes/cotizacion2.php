<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $margin_left=18;
    $margin_top=28;
    $margin_right=18;

    $fecha=$dets->fecha;
    $id_cotizacion=$id;
    $empresa=$dets->empresa;
    $direccion=$dets->direccion;
    $edo_cp=$dets->edo_cp;
    $tel=$dets->tel;
    $mail=$dets->mail;
    $atencion=$dets->atencion;

//=======================================================================================
class MYPDF extends TCPDF {
    public function Header() {
        $headimg = base_url().'public/img/headpdf.jpg';
        /*$html = '<table border="0">
            <tr>
                <td>
                    <img src="'.$logos.'" >
                </td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');*/
        $this->Image($headimg, -1, 0, 212, 22, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    public function Footer() {
        $html2="";
        //log_message('error','this->PageNo(): '.$this->PageNo());
        if($this->PageNo()==1){
            $html2 = '<table style="font-weight: bold;">
                <tr><td>ATENTAMENTE</td></tr>
                <tr><td height="75px"><br><img style="margin-top: 8px;" src="'.base_url().'public/img/firmacot.png" width="75px"></td></tr>
                <tr>
                    <td>Gerencia de Laboratorio</td>
                </tr>
            </table>';
            //$this->writeHTML($html2, true, false, true, false, '');
            $this->writeHTMLCell(50, '', 12, 233, $html2, 0, 0, 0, true, 'C', true);
        }

        $html="";
        $footimg = base_url().'public/img/footerpdf2.png';
        $this->Image($footimg, -1, 274, 212, 22, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $html .= '
            <style type="text/css">
                .footerpage{font-size:8px;font-style: italic;}
            </style> 
          <table width="100%" cellpadding="2" border="0"><tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' </td></tr></table>';
        $this->writeHTMLCell(376, '', 12, 290, $html, 0, 0, 0, true, 'C', true);
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('cotizacion');
$pdf->SetTitle('cotizacion');
$pdf->SetSubject('cotizacion');
$pdf->SetKeywords('cotizacion');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins($margin_left, $margin_top, $margin_right);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(23);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 26);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 12);
// add a page
$pdf->AddPage('P', 'A4');
$htmlg='';
$htmlg.='<style type="text/css">
    .borderbottom{
        border-bottom:1px solid black;
    }
    .backg{
        background-color:#c6c9cb;
    }
    .txt_just{
        text-align: justify;
    }
    .txt_justSubs{
        text-align:justify;
        font-size:7px;
    }
    .txt_justNota{
        text-align:justify;
        font-size:8px;
    }
    .pspaces{ font-size: 0.2px;}
    .checked{font-size:12px;font-family:dejavusans;}
    .back_tot{ background-color:rgb(244,244,244); }
</style>

<div style="font-size: 10.5px; font-family:Arial Nova;">
    <table width="100%">
        <tr>
            <td><br></td>
        </tr>
        <tr>
            <td align="right" colspan="2"><span>'.$fecha.'
                <br>Cotización No: 
                '.$id.'
                </span>
            </td>
        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td width="50%"></td>
            <td align="center" width="50%"><span><b>'.$empresa.'</b>
                <br>'.$direccion.'
                <br>'.$edo_cp.'
                <br>'.$tel.'
                <br>'.$mail.'
                <br><i>'.$atencion.'</i>
                </span>
                
                <p style="font-size:1px; background-color: #002060">____________________________________________</p>
            </td>
        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td colspan="2"><p class="txt_just">En atención a su amable solicitud, se presenta la cotización de servicios requeridos por su distinguida organización. Es importante mencionarle que dentro del contenido encontrará información técnica y administrativa importante a considerar para la correcta ejecución de su servicio.</p>
            
                <p class="txt_just">Como agradecimiento a la confianza depositada en nosotros nos comprometemos a ofrecer un servicio de calidad que en todo momento buscará cumplir con sus expectativas.</p>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table border="1" align="center" style="font-weight: bold; font-size:10.5px; font-family: Calibri; border: 1px solid black">
        <thead>
            <tr >
                <td rowspan="2" width="8%"><p class="pspaces"></p>Partida</td>
                <td rowspan="2" width="56%"><p class="pspaces"></p>Descripción</td>
                <td rowspan="2" width="8%"><p class="pspaces"></p>Cant.</td>
                <td colspan="2" width="28%">Precio</td>
            </tr>
            <tr>
                <td>Unitario</td>
                <td>Total</td>
            </tr>
        </thead>
    </table>';
       
    $i=1; $total=0;
    foreach ($serv as $s) {
        $htmlg.='<table border="1" align="center" style="font-family: Arial Nova; font-size: 10.5px !important;" cellpadding="5">
            <tr>
                <td width="8%">'.$i.'</td>
                <td width="56%">'.$s->nombre2.' '.$s->descripcion2.'
                </td>
                <td width="8%">'.$s->cantidad.'</td>
                <td width="14%">$ '.number_format($s->precio,2).'</td>
                <td class="back_tot" width="14%">$ '.number_format($s->precio*$s->cantidad,2) .'</td>
            </tr>
        </table>';
        $total+=$s->precio*$s->cantidad;
        $i++;
    }
    
    $htmlg.='<table align="center" style="font-size:10.5px" cellpadding="5">
        <tr>
            <td width="1%"></td>
            <td width="70%" rowspan="3"><p class="txt_justSubs">La presente cotización considera el alcance señalado y acordado entre el solicitante y Ahisa Laboratorio de Pruebas, S. de R.L. de C.V., al momento que es aceptada y autorizada por ambas partes se considera como un contrato legal en los términos que aplique según la ley mercantil y sus reglamentos.</p></td>
            <td width="1%"></td>
            <td border="1" width="14%"><b>Subtotal</b></td>
            <td class="back_tot" border="1" width="14%">$ '.number_format($cotizacion->subtotal,2).'</td>
        </tr>
        <tr>
            <td width="72%"></td>
            <td border="1" width="14%"><b>IVA (16%)</b></td>
            <td class="back_tot" border="1" width="14%">$ '.number_format($cotizacion->subtotal*.16,2).'</td>
        </tr>
        <tr>
            <td width="72%"></td>
            <td border="1" width="14%"><b>Total (MXN)</b></td>
            <td class="back_tot" border="1" width="14%">$ '.number_format($cotizacion->subtotal*1.16,2).'</td>
        </tr>
    </table>';
    if($cotizacion->descuento!=0){ 
        $desc_cantp = $cotizacion->subtotal*$cotizacion->descuento/100;
        $desc_cant = $cotizacion->subtotal - $desc_cantp;
        $htmlg.='<table border="1" nobr="true" align="center">
            <tr>
                <td width="10%"></td>
                <td width="52%">Descuento sobre el total<br>*puede no aplicar sobre todos los servicios</td>
                <td width="10%"></td>
                <td width="14%">% '.$cotizacion->descuento.'</td>
                <td width="14%">$ '.number_format($desc_cantp,2).'</td>
            </tr>
             <tr>
                <td colspan="3"></td><td class="tr-sv">CON DESCUENTO</td><td class="tr-sv">$ '.number_format($desc_cant,2).'</td>
            </tr>
        </table>';    
    }
    $htmlg.='<br><br>
    <!--<table>
        <tr><td height="140px"></td></tr>
    </table>
    <table style="font-weight: bold;">
        <tr><td>ATENTAMENTE</td></tr>
        <tr><td height="75px"><br><img style="margin-top: 8px;" src="'.base_url().'public/img/firmacot.png" width="75px"></td></tr>
        <tr>
            <td>Gerencia de Laboratorio</td>
        </tr>
    </table>-->
    <table>
        <tr><td><br></td></tr>
    </table>
</div>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->AddPage('P', 'A4');
    $htmlg='';
    $htmlg.='<style type="text/css">
        .borderbottom{
            border-bottom:1px solid black;
        }
        .backg{
            background-color:#c6c9cb;
        }
        .txt_just{
            text-align: justify;
        }
        .txt_center{
            text-align: certer;
        }
        .txt_justSubs{
            text-align:justify;
            font-size:7px;
        }
        .txt_justNota{
            text-align:justify;
            font-size:8px;
        }
        .pspaces{ font-size: 0.2px;}
        .checked{font-size:12px;font-family:dejavusans;}
    </style>
<div style="font-size: 10.5px; font-family:Arial Nova;">
    <table>
        <tr>
            <td><i><b>ALCANCE ACRÉDITADO Y APROBADO</b></i>
                <p style="font-size:1px; background-color: #e4e4e4">_</p>
            </td>
        </tr>
        <tr>
            <td><p class="txt_just">Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. cuenta con las siguientes Normas Oficiales Mexicana (NOM´s) acreditadas y aprobadas por sus respectivas autoridades competentes.</p></td>
        </tr>
        <tr>
            <td>
                <p class="txt_just">NOM-011-STPS-2001 / Condiciones de seguridad e higiene en los centros de trabajo donde se genere ruido</p>
                <p class="txt_just">NOM-015-STPS-2001 / Condiciones térmicas elevadas o abatidas - Condiciones de seguridad e higiene</p>
                <p class="txt_just">NOM-022-STPS-2015 / Electricidad estática en los centros de trabajo - Condiciones de seguridad</p>
                <p class="txt_just">NOM-025-STPS-2008 / Condiciones de iluminación en los centros de trabajo</p>
                <p class="txt_just">NOM-081-SEMARNAT-1994 / Que establece los límites máximos permisibles de emisión de ruido de las fuentes fijas y su método de medición</p>
                <p class="txt_justNota">NOTA: Las NOM´s que sea mencionadas en la presente cotización y que no se encuentren en el listado anterior no se consideran como normas acreditadas y aprobadas.</p>
            </td>
        </tr>
    </table>
    <table>
        <tr><td><br><br></td></tr>
    </table>
    <table>
        <tr><td><b><i>EVALUACIÓN DE LA CAPACIDAD TÉCNICA E IMPARCIALIDAD</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p></td></tr>
        <tr>';
        if($dets->preg1==1)//si
            $preg1='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg1='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg2==1)//si
            $preg2='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg2='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg3==1)//si
            $preg3='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg3='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg4==1)//si
            $preg4='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg4='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg5==1)//si
            $preg5='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg5='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg6==1)//si
            $preg6='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg6='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';
        if($dets->preg7==1)//si
            $preg7='<span class="txt_center">Si <span style="color:green" class="checked">✔</span> No</span>';
        else
            $preg7='<span class="txt_center">Si &nbsp;  No <span style="color:green" class="checked">✔</span></span>';

            $htmlg.='<td class="txt_just">
                <ol>
                    <li>¿Se cuenta con la acreditación y aprobación vigente de la(s) norma(s) solicitada(s)?
                    <br>'.$preg1.'</li><br>
                    <li>¿Los equipos y patrones involucrados en el(los) servicio(s) cuentan con calibración vigente?  
                    <br>'.$preg2.'</li><br>
                    <li>¿La(s) norma(s) solicitada(s) por el cliente es(son) la(s) adecuada(s) para cubrir sus necesidades?
                    <br>'.$preg3.'</li><br>
                    <li>¿Se cuenta con personal calificado para desarrollar el(los) servicio(s)?
                    <br>'.$preg4.'</li><br>
                    <li>¿La capacidad instalada del laboratorio no se ve comprometida con la realización del(los) servicio(s)?
                    <br>'.$preg5.'</li><br>
                    <li>¿Se identifican riesgos a la imparcialidad en donde se ve comprometido el(los) servicio(s)?
                    <br>'.$preg6.'</li><br>
                    <li>¿Se identifican algún conflicto de intereses para la realización del(los) servicio(s)?
                    <br>'.$preg7.'</li><br>
                </ol>
            </td>
        </tr>
    </table>

    <table>
        <tr><td><br><br></td></tr>
    </table>
    <table>
        <tr>
            <td><b><i>REGLA DE DECISIÓN</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                <p class="txt_just">Aceptación Simple. La zona de seguridad tiene una dimensión igual a cero (w = 0), lo que implica que la aceptación se da cuando el resultado de una medición está por debajo del límite de tolerancia y se rechaza cuando el resultado este por arriba del límite de tolerancia (Declaración Binaria).</p>
                <p class="txt_just">Nota: Para la NOM-025-STPS-2008 la zona de seguridad tiene una dimensión igual a cero (w = 0), lo que implica que la aceptación se da cuando el resultado de una medición está por arriba del límite de tolerancia y se rechaza cuando el resultado este por debajo del límite de tolerancia (Declaración Binaria).</p>
            </td>
        </tr>
    </table>

    <table>
        <tr><td><br><br></td></tr>
    </table>
    <table>
        <tr>
            <td><b><i>DECLARACIÓN DE CONFORMIDAD</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                <p class="txt_just">Los resultados que se derivan de los servicios que Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. son comparados contra niveles permisibles establecidos en Normas Oficiales Mexicanas (NOM´s), por lo que para los servicios solicitados en la presente cotización se realizará una declaración de conformidad con las siguientes normas:</p>
            </td>
        </tr>
        <tr><td></td></tr>';
        if($dets->declara1==1)//si
            $declara1='<span style="color:green" class="checked">✔</span>';
        else
            $declara1='';
        if($dets->declara2==1)//si
            $declara2='<span style="color:green" class="checked">✔</span>';
        else
            $declara2='';
        if($dets->declara3==1)//si
            $declara3='<span style="color:green" class="checked">✔</span>';
        else
            $declara3='';
        if($dets->declara4==1)//si
            $declara4='<span style="color:green" class="checked">✔</span>';
        else
            $declara4="";
        if($dets->declara5==1)//si
            $declara5='<span style="color:green" class="checked">✔</span>';
        else
            $declara5="";
        $htmlg.='<tr class="txt_just">
            <td width="95%">NOM-011-STPS-2001 / Condiciones de seguridad e higiene en los centros de trabajo donde se genere ruido</td>
            <td width="5%" style="text-align-right">'.$declara1.'</td>
        </tr>
        <tr class="txt_just">
            <td width="95%">NOM-015-STPS-2001 / Condiciones térmicas elevadas o abatidas - Condiciones de seguridad e higiene</td>
            <td width="5%" style="text-align-right">'.$declara2.'</td>
        </tr>
        <tr class="txt_just">
            <td width="95%">NOM-022-STPS-2015 / Electricidad estática en los centros de trabajo - Condiciones de seguridad</td>
            <td width="5%" style="text-align-right">'.$declara3.'</td>
        </tr>
        <tr class="txt_just">
            <td width="95%">NOM-025-STPS-2008 / Condiciones de iluminación en los centros de trabajo</td>
            <td width="5%" style="text-align-right">'.$declara4.'</td>
        </tr>
        <tr class="txt_just">
            <td width="95%">NOM-081-SEMARNAT-1994 / Que establece los límites máximos permisibles de emisión de ruido de las fuentes fijas y su método de medición</td>
            <td width="5%" style="text-align-right">'.$declara5.'</td>
        </tr>
    </table>

    <!--<table>
        <tr><td height="70px"></td></tr>
    </table>-->
</div>';
    /*$pdf->writeHTML($htmlg, true, false, true, false, '');

    $pdf->AddPage('P', 'A4');
    $htmlg='';
    $htmlg.='<style type="text/css">
        .borderbottom{
            border-bottom:1px solid black;
        }
        .backg{
            background-color:#c6c9cb;
        }
        .txt_just{
            text-align: justify;
        }
        .txt_justSubs{
            text-align:justify;
            font-size:7px;
        }
        .txt_justNota{
            text-align:justify;
            font-size:8px;
        }
        .pspaces{ font-size: 0.2px;}
        .checked{font-size:12px;font-family:dejavusans;}
    </style>';*/
$htmlg.='<div style="font-size: 10.5px; font-family:Arial Nova;">
    <table>
        <tr>
            <td><b><i>TRATAMIENTO DE LOS DATOS PERSONALES Y/O EMPRESARIALES</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                <p class="txt_just">De acuerdo a nuestro aviso de privacidad se le notifica que Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. utilizará sus datos personales y/o empresariales para las siguientes finalidades.</p>
            </td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <td>
                <ol>
                    <li>Cotizar los servicios que le interesen.</li>
                    <li>Realizar informes de resultados.</li>
                    <li>Notificar a las autoridades competentes (STPS, SEMARNAT, ema, entre otros) sobre los servicios que le fueron ofrecidos.</li>
                    <li>Realizar e interpretar estudios de mercado.</li>
                    <li>Analizar comportamientos históricos de los servicios solicitados por su organización.</li>
                </ol>
            </td>
        </tr>
    </table>
    <p class="txt_just">Los datos personales a los que nos referimos son: nombres y firmas autógrafas del personal gerencial de su organización (representante legal, responsables de áreas, etc.), nombres de sus colaboradores (trabajadores), medios de contacto tales como razón social, números telefónicos, correos electrónicos, registro federal de contribuyentes (RFC), domicilio físico y fiscal, entre otros.</p>
    <p class="txt_just">Para mayor información sobre nuestro aviso de privacidad puede consultarlo en nuestra página de internet www.ahisa.mx, o bien, solicitarlo vía correo electrónico. </p>

    <table>
        <tr><td><br></td></tr>
    </table>';
    $metodo1=""; $metodo2=""; $metodo3=""; $metodo4="";
    if($dets->metodo1==1)
        $metodo1='<span class="checked" style="color:green; font-size:10px;">✔</span>';
    if($dets->metodo2==1)
        $metodo2='<span class="checked" style="color:green; font-size:10px;">✔</span>';
    if($dets->metodo3==1)
        $metodo3='<span class="checked" style="color:green; font-size:10px;">✔</span>';
    if($dets->metodo4==1)
        $metodo4='<span class="checked" style="color:green; font-size:10px;">✔</span>';

    $htmlg.='<table>
        <tr>
            <td><b><i>CONDICIONES DE PAGO</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                <ol>
                    <li><p class="txt_just"><b>Formas de pago.</b> El cliente podrá realizar el pago de los servicios asentados en esta cotización mediante efectivo, cheque nominativo y/o transferencia electrónica de fondos. Los pagos serán destinados a la razón social Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. con R.F.C. ALP160621FD6.</p>

                        <p class="txt_just">En caso de optar por la forma de transferencia electrónica de fondos, esta se deberá realizar a la institución bancaria SCOTIABANK INVERLAT, S.A., número de cuenta 03605443768 y con CLABE 044650036054437685</p>
                    </li>
                    <li><p class="txt_just"><b>Plazo de pago.</b> Se acordará con el cliente el plazo de pago a otorgar, pudiendo ser:</p>
                        <table>
                            <tr class="txt_justSubs">
                                <th width="32%">50% de anticipo, 50% contra entrega de servicio(s) </th>
                                <th width="3%" border="1">  '.$metodo1.'</th>
                                <th width="1%"></th>
                                <th width="22%"> Pago contra entrega de servicio(s) </th>
                                <th width="3%" border="1">  '.$metodo2.'</th>
                                <th width="1%"></th>
                                <th width="26%"> Pago anticipado del 100% por servicio(s) </th>
                                <th width="3%" border="1">  '.$metodo3.'</th>
                                <th width="1%"></th>
                                <th width="5%"> Otro </th>
                                <th width="3%" border="1">  '.$metodo4.'</th>
                            </tr>
                            <tr><th></th></tr>
                        </table>
                    </li>
                    <li><p class="txt_just"><b>Facturación.</b> Realizado el pago por parte del cliente se deberá notificar mediante un correo electrónico a la dirección administracion@ahisa.mx y, en su caso, enviar comprobante que avale el pago, Constancia de Situación Fiscal actualizada, así como señalar uso de CFDI y régimen fiscal.</p></li>
                </ol>
            </td>
        </tr>
    </table>
    
    <table>
        <tr><td><br></td></tr>
    </table>
    <table width="100%">
        <tr>
            <td width="49%">
                <table>
                    <tr><td><b><i>ACUERDOS COMERCIALES</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                        <p class="txt_just">1. La presente cotización tiene una vigencia de 60 días naturales contados a partir de su fecha de emisión.</p>
                        <p class="txt_just">2. El tiempo de entrega de los resultados será de 13 días hábiles, dichos días son considerados a partir de que se proporcione la información completa por parte del cliente para cada servicio solicitado.</p>
                        <p class="txt_just">3. Comprometidos con el medio ambiente y con la disminución de uso de papel, la entrega de informes de resultados será de forma digital, en caso de requerir informe impreso se deberá cubrir el costo correspondiente por impresión. </p>
                    </td></tr>
                </table>
            </td>
            <td width="2%"></td>
            <td width="49%">
                <table>
                    <tr><td>   
                        <p class="txt_just">4. Cuando se realice una visita al centro de trabajo, programada y notificada, y esta no pueda ser atendida por causas atribuibles al cliente se considerará realizar un cobro por "visita en falso", el monto a cobrar será de $5,000.00 más los costos de viáticos, en caso que aplique.</p>
                        <p class="txt_just">5. En el caso de cancelaciones o reprogramaciones, tanto por parte del cliente como de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. estas deberán notificarse con al menos 5 días hábiles de anticipación. </p>
                        <p class="txt_just">6. El cliente dispondrá de 5 días hábiles para el envío de información que no pudo se entregada en sitio, de lo contrario se podrá establecer dentro del informe de resultados que es información no proporcionada. </p>
                    </td></tr>
                </table>
            </td>
        </tr>
    </table>

    <table>
        <tr><td><br></td></tr>
    </table>
    <table width="100%">
        <tr>
            <td width="49%">
                <table>
                    <tr><td><b><i>RESPONSABILIDADES DE AHISA</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                        <p class="txt_just">1. Toda la información a la que se tenga acceso y que sea propiedad del cliente deberá ser tratada con confidencialidad.</p>
                        <p class="txt_just">2. Asegurarse de que todo el personal que colabora en este laboratorio actué de forma imparcial y objetiva en las actividades que desempeña.</p>
                        <p class="txt_just">3. Poner a disponibilidad de clientes y personas interesadas, nuestro proceso de tratamiento de quejas cada que se requiera.</p>
                        <p class="txt_just">4. Salvaguardar entrega de resultados libres de cualquier presión y/o influencia económica, comercial o de otro tipo.</p>
                    </td></tr>
                </table>
            </td>
            <td width="2%"></td>
            <td width="49%">
                <table>
                    <tr><td><i>Derechos de Ahisa Laboratorio de Prueba, S. de R.L. de C.V.</i>
                        <p class="txt_just">1. No realizar y/o suspender servicios que puedan presentar algún conflicto de interés o riesgo para el laboratorio y/o sus colaboradores. </p>
                        <p class="txt_just">2. Aplicar los costos correspondientes cuando un servicio sea cancelado fuera de los tiempos establecidos. </p>
                        <p class="txt_just">3. Abstenerse de dar resultados previos a la emisión del informe de resultados.</p>
                    </td></tr>
                </table>
            </td>
        </tr>
    </table>

    <table>
        <tr><td><br></td></tr>
    </table>
    <table width="100%">
        <tr>
            <td width="49%">
                <table>
                    <tr><td><b><i>DERECHOS Y OBLIGACIONES</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                        <p><i>Derechos del Cliente</i></p>
                        <p class="txt_just">1. Ser notificados de cualquier cambio en el proceso de su servicio de forma clara y exacta.</p>
                        <p class="txt_just">2. Dar solución a cualquier duda que surja durante el proceso de su servicio.</p>
                        <p class="txt_just">3. Permitirle el acceso a las instalaciones de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. para corroborar y/o supervisar el proceso de su servicio.</p>
                        <p class="txt_just">4. En caso de ser necesario, poder cancelar su servicio con una justificación lo suficientemente valida.</p>
                        <p class="txt_just">5. En caso de considerar una afectación en alguna etapa de su servicio y tener una reclamación o queja proceder conforme a lo establecido en los artículos 163, 164 y 165 de la Ley de Infraestructura de la Calidad.</p>
                    </td></tr>
                </table>
            </td>
            <td width="2%"></td>
            <td width="49%">
                <table>
                    <tr><td><i>Obligaciones de Ahisa Laboratorio de Prueba, S. de R.L. de C.V.</i>
                        <p class="txt_just">1. Realizar los servicios (reconocimiento, evaluación y emisión de informes de resultados) conforme a lo indicado en las Normas Oficiales Mexicanas involucradas y al sistema de gestión.</p>
                        <p class="txt_just">2. Realizar el registro de los informes de resultados en el sistema informático de la STPS de los servicios realizados.</p>
                        <p class="txt_just">3. Atender cualquier reclamación y/o queja por parte del cliente.</p>

                    </td></tr>
                </table>
            </td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <td width="49%">
                <table>
                    <tr><td><i>Obligaciones del Cliente</i>
                        <p class="txt_just">1. Permitir el acceso a Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. a sus instalaciones, áreas, puestos de trabajo, etc. para efectos del reconocimiento y evaluación del servicio solicitado.</p>
                        <p class="txt_just">2. Facilitar a Ahisa Laboratorio de Pruebas, S. de R.L. de C.V. toda la información que se requiera para los trabajos de campo y la emisión del informe de resultados.</p>
                    </td></tr>
                </table>
            </td>
            <td width="2%"></td>
            <td width="49%">
                <table>
                    <tr><td><b><i>ACEPTACIÓN DE LA COTIZACIÓN</i></b><p style="font-size:1px; background-color: #e4e4e4">_</p>
                        <p class="txt_just">Firmada la presente cotización por parte del cliente se presume que se acepta todo lo establecido en ella.</p>
                        <p class="txt_just" style="color:#bcbcbc">FECHA, NOMBRE Y FIRMA DEL CLIENTE.</p>
                        <p class="txt_just">______________________________________________________</p>

                    </td></tr>
                </table>
            </td>
        </tr>
    </table>


</div>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->Output(FCPATH . 'mail/cotizacion_'.$id.'.pdf', 'FI');