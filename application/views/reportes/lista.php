<script src="https://cdn.jsdelivr.net/npm/chart.js@latest/dist/Chart.min.js"></script>
<div class="main-content">
    <div class="content-wrapper">
        <section class="color-palette">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-header mb-3">Reportes</div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <h5><i class="ft-mail"></i> Listado de Reportes</h5><hr>
                        <div class="container">
                        <form class="form">
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="Fecha1">Desde</label>
                                    <input id="Fecha1" type="date" name="Fecha1" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-5 form-group">
                                <label for="Fecha2">Hasta</label>
                                    <input id="Fecha2" type="date" name="Fecha2" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                                <div class="col-md-5 form-group">
                                    <label for="sucursal">Sucursal</label>
                                    <select id="sucursal" class="form-control">
                                        <option value="">Todas</option>
                                        <?php foreach ($sucs as $k) { ?>
                                        <option value="<?php echo $k->id; ?>"><?php echo $k->nombre; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-5 form-group">
                                    <label for="zonas">Zonas</label>
                                    <select id="zona" class="form-control">
                                        <option value="">Todas</option>
                                        <?php foreach ($zonas as $z) { ?>
                                        <option value="<?php echo $z->zona; ?>"><?php echo $z->zona; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                              
                            </div>
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label for="Vendedor">Vendedor</label>
                                    <select id="Vendedor" name="Vendedor" class="form-control">
                                        <option value=""> </option>
                                        <?php /*foreach ($vendedores->result() as $key) { ?>
                                        <option value="<?php echo $key->id; ?>"><?php echo $key->nombre; ?></option>
                                        <?php }*/ ?>
                                    </select>
                                </div>
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-5 form-group">
                                    <label for="Cliente">Cliente</label>
                                    <select id="Cliente" name="Cliente" class="form-control">
                                        <option value=""> </option>
                                        <?php foreach ($clientes->result() as $key) { ?>
                                        <option value="<?php echo $key->id; ?>"><?php echo $key->alias; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-5 form-group">
                                    <label for="estatus">Estatus</label>
                                    <select id="tiposta" name="tiposta" class="form-control" multiple="multiple" size="4" style="height: 100%;">
                                        <option id="0" selected value="0">Todas</option>
                                        <option id="1" value="1">Pendiente</option>
                                        <option id="2" value="2">Aceptada</option>
                                        <option id="3" value="3">Rechazada</option>
                                        <option id="4" value="4">Modificada</option>
                                    </select>
                                </div>
                                <div class="col-md-1 form-group"></div>
                                <div class="col-md-2 form-group">
                                    <label for="btnCambio" style="color:transparent;">busqueda</label>
                                    <button type="button" id="btnCambio" class="btn btn-info">Aplicar</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="col-md-12"><br><br></div>
                        <div style="">
                            <h5> Reporte General</h5><hr>
                            <div class="col-md-2 form-group">
                                <button class="btn btn-success" type="button" id="export" >Exportar Excel</button>
                            </div>
                            <table class="table table-striped table-responsive data-tables" id="tabla">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cotizacion</th>
                                        <th>Fecha</th>
                                        <th>Cliente</th>
                                        <th>Vendedor</th>
                                        <th>Importe</th>
                                        <th>Forma de Pago</th>
                                        <th>Zona</th>
                                        <th>Estatus</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <br><br>
                        <div >
                            <h5>Ventas por sucursal</h5><hr>
                            <div id="cont_table"></div>
                            <!--<table class="table table-striped table-responsive data-tables" id="tabla_por_suc">
                                <thead>
                                    <tr>
                                        <th>Sucursal</th>
                                        <th>Servicios</th>
                                        <th>Viaticos</th>
                                        <th>Importe total</th>
                                    </tr>
                                </thead>
                                <tbody id="bdy_suc">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="text-align:right">Total:</th>
                                        <th id="tot_suc"></th>
                                    </tr>
                                </tfoot>
                            </table>-->
                        </div>
                        <br><br>
                        <div >
                            <h5>Ventas por empresa</h5><hr>
                            <div id="cont_table_pe"></div>
                            <!--<table class="table table-striped table-responsive data-tables" id="tabla_por_emp">
                                <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Servicios</th>
                                        <th>Viaticos</th>
                                        <th>Importe total</th>
                                    </tr>
                                </thead>
                                <tbody id="bdy_suc">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="text-align:right">Total:</th>
                                        <th id="tot_emp"></th>
                                    </tr>
                                </tfoot>
                            </table>-->
                        </div>

                        <br><br>
                        <div >
                            <h5>Ventas por vendedor</h5><hr>
                            <div id="cont_table_pv"></div>
                            <!--<table class="table table-striped table-responsive data-tables" id="tabla_por_vende">
                                <thead>
                                    <tr>
                                        <th>Vendedor</th>
                                        <th>Servicios</th>
                                        <th>Viaticos</th>
                                        <th>Importe vendido</th>
                                    </tr>
                                </thead>
                                <tbody id="bdy_suc">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="text-align:right">Total:</th>
                                        <th id="tot_vend"></th>
                                    </tr>
                                </tfoot>
                            </table>-->
                        </div>
                        <br><br>
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-md-12">
                                    <canvas id="grafica"></canvas>
                                    <div id="bar-container">
                                        <div id="bar-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
var table;
    $(document).ready(function () {
        $('#Vendedor').select2({
            width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un vendedor',
            ajax: {
                url: '<?php echo base_url(); ?>index.php/Reportes/getVendedor',
                dataType: "json",
                data: function(params) {
                    var query = {
                        search: params.term,
                        sucursal: $("#sucursal").val(), 
                        zona: $("#zona").val() ,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data) {
                    var itemscli = [];
                    data.forEach(function(element) {
                        itemscli.push({
                            id: element.id,
                            text: element.nombre,
                        });
                    });
                    return {
                        results: itemscli
                    };
                },
            }
        });

        $('#Cliente').select2({
            width: 'resolve',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar un cliente',
        });


        $("#tiposta").on("change",function(){
            var values = $('#tiposta').val();
            //console.log("values: "+values);
            var cont_aux=0;
            values.forEach(function(i) {
                cont_aux++;
                //console.log("cont_aux:" +cont_aux);
            });
            values.forEach(function(i) {
                //console.log("valores:" +i);
                if (i == 0  && cont_aux>1 || i == 2 && cont_aux>1 ) {
                    //$("#tiposta").find('option.0').removeAttr("selected");
                    $("#"+i+"").prop("selected", false);
                }
            });
        });
        
        $("#export").on("click",function(){
            vendedor = $('#Vendedor option:selected').val();
            fecha1= $('#Fecha1').val();
            fecha2= $('#Fecha2').val();
            sucursal= $('#sucursal option:selected').val();
            zona= $('#zona option:selected').val();
            cliente= $('#Cliente option:selected').val();
            tiposta= $('#tiposta').val();
            if(vendedor=="")
                vendedor=0;
            if(fecha1=="")
                fecha1=0;
            if(fecha2=="")
                fecha2=0;
            if(sucursal=="")
                sucursal=0;
            if(zona=="")
                zona=0;
            if(cliente=="")
                cliente=0;
            if(tiposta=="")
                tiposta=0;
            /*console.log("vendedor: "+vendedor);
            console.log("fecha1: "+fecha1);
            console.log("fecha2: "+fecha2);
            console.log("sucursal: "+sucursal);
            console.log("zona: "+zona);
            console.log("cliente: "+cliente);*/
            
            /*s_sta = JSON.stringify(tiposta);
            console.log("s_sta: "+s_sta);*/
            window.open("<?php echo base_url(); ?>index.php/Reportes/ExportReporte/"+fecha1+"/"+fecha2+"/"+sucursal+"/"+zona+"/"+cliente+"/"+tiposta+"/"+vendedor, '_blank');
        });

        table = $('#tabla').DataTable({
            "bProcessing": true,
            "serverSide": true,
            destroy:true,
            "pageLength":10,
            "order": [[ 0, "desc" ]],
             "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Reportes/GetReportes",
                 type: "post",
                "data": function(d){
                    d.vendedor = $('#Vendedor option:selected').val(),
                    d.fecha1 = $('#Fecha1').val(),
                    d.fecha2 = $('#Fecha2').val(),
                    d.sucursal = $('#sucursal option:selected').val(),
                    d.zona = $('#zona option:selected').val(),
                    d.cliente = $('#Cliente option:selected').val(),
                    d.tiposta = $('#tiposta').val()
                },
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            responsive: true,
            "columns": [
                {"data": "id"},
                {"data": "clave",
                    "render":function(data,type,row,meta){
                        return Clave(data);
                    }
                },
                 {"data": null,
                    "render":function(url, type, full){
                        var msj='';
                        if(full["tipo"]=="2" && full["id_ord"]==0){
                            msj=full["fecha_creacion_cot"];
                            return msj;
                        }else if(full["tipo"]=="2" && full["id_ord"]>0){
                            msj=full["fecha_creacion_ord"];
                            return msj;
                        }else if(full["tipo"]=="1") {
                           msj=full["fecha_creacion_ord"];
                            return msj; 
                        }
                    }
                },
                //{"data": "fecha_creacion"},
                {"data": "alias"},
                {"data": "nombre"},
                //{"data": "monto"},
                {"data": null,
                    "render" : function ( url, type, full) {
                        var msj='';
                        msj+="$" +full["monto"];
                        return msj;
                    }
                },
                {"data": "forma_pago",
                    "render": function (data, type, row, meta) {
                        return forma_pago(data);
                    }
                },
                {"data": "zona"},
                {"data": "status",
                    "render": function (data, type, row, meta) {
                        return estatus(data);
                    }
                },
            ],
            dom: 'Bfrtip',
            buttons: [
                /*{extend: 'excel'},
                {extend: 'pdf'},
                {extend:'print'},*/
                'pageLength'
            /*{
                
                extend:'print',
                //extend: 'pdf',
                exportOptions: {
                    columns: [0,1,2,3,4,5]     
                },    
            }*/]
        });

        $('#btnCambio').click(function(){
            table.ajax.reload();
            
        });

        $("#Fecha1").on("change",function(){
            if($("#Fecha1").val()!="" && $("#Fecha2").val()!=""){
                loadPorSucursal();
                setTimeout(function() { loadPorEmpresa(); }, 1500); 
                loadPorVende();
                barCharVentas();
            }
        });
        $("#Fecha2").on("change",function(){
            if($("#Fecha1").val()!="" && $("#Fecha2").val()!=""){
                loadPorSucursal();
                setTimeout(function() { loadPorEmpresa(); }, 1500); 
                loadPorVende();
                barCharVentas();
            }
        });

        loadPorSucursal();
        setTimeout(function() { loadPorEmpresa(); }, 1500); 
        loadPorVende();
        barCharVentas();
    });

    function loadPorSucursal(){
        /*table_xsuc = $('#tabla_por_suc').DataTable({
            "pageLength":10,
            destroy:true,
            "order": [[ 0, "desc" ]],
             "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Reportes/GetVentaSuc",
                 type: "post",
                "data": function(d){
                    d.fecha1 = $('#Fecha1').val(),
                    d.fecha2 = $('#Fecha2').val()
                },
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            responsive: true,
            "columns": [
                {"data": "sucursal"},
                {"data": null,
                    "render":function(data, type, row, meta){
                        var montos='';
                        total_sucursal = parseFloat(row.total_sucursal);
                        descuentos = parseFloat(row.descuentos);
                        montos=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal-descuentos);
                        return montos;
                    }
                },
                {"data": null,
                    "render": function (data, type, row, meta) {
                        var tot_viat='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        tot_viat=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal_viat);
                        return tot_viat;
                    }
                },
                {"data": "tot_fin",
                    "render":function(data, type, row, meta){
                        var total='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        sub = parseFloat(row.total_sucursal)-parseFloat(row.descuentos);
                        total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(sub+total_sucursal_viat);
                        return total;
                    }
                },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Total over this page
                pageTotal = api
                    .column(3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Update footer
                $( api.column(3).footer() ).html(
                    new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(pageTotal)
                );
            },
            dom: 'Bfrtip',
            buttons: [
                'pageLength',
                {extend: 'excel'},
                {extend: 'pdf'},
                {extend:'print'},
            ],
            
        });*/
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Reportes/GetVentaSuc",
            data: {fecha1: $("#Fecha1").val(), fecha2: $("#Fecha2").val()},
            success: function (data) {
                //console.log("data: "+data);
                $("#cont_table").html(data);
                $('#tabla_por_suc').DataTable({
                    "pageLength":10,
                    destroy:true,
                    "order": [[ 0, "asc" ]],
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {extend: 'excel'},
                        {extend: 'pdf'},
                        {extend:'print'},
                    ],
                });
            }
        });
    }

    function loadPorEmpresa(){
        /*table_xsuc = $('#tabla_por_emp').DataTable({
            "pageLength":10,
            destroy:true,
            "order": [[ 0, "desc" ]],
             "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Reportes/GetVentaEmp",
                 type: "post",
                "data": function(d){
                    d.fecha1 = $('#Fecha1').val(),
                    d.fecha2 = $('#Fecha2').val()
                },
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            responsive: true,
            "columns": [
                {"data": "empresa"},
                {"data": null,
                    "render":function(data, type, row, meta){
                        var montos='';
                        total_sucursal = parseFloat(row.total_sucursal);
                        descuentos = parseFloat(row.descuentos);
                        montos=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal-descuentos);
                        return montos;
                    }
                },
                {"data": null,
                    "render": function (data, type, row, meta) {
                        var tot_viat='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        tot_viat=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal_viat);
                        return tot_viat;
                    }
                },
                {"data": "tot_fin",
                    "render":function(data, type, row, meta){
                        var total='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        sub = parseFloat(row.total_sucursal)-parseFloat(row.descuentos);
                        total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(sub+total_sucursal_viat);
                        return total;
                    }
                },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Total over this page
                pageTotal = api
                    .column(3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Update footer
                $( api.column(3).footer() ).html(
                    new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(pageTotal)
                );
            },
            dom: 'Bfrtip',
            buttons: [
                'pageLength',
                {extend: 'excel'},
                {extend: 'pdf'},
                {extend:'print'},
            ],
            
        });*/
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Reportes/GetVentaEmp",
            data: {fecha1: $("#Fecha1").val(), fecha2: $("#Fecha2").val()},
            async:false,
            success: function (data2) {
                //console.log("data2: "+data2);
                $("#cont_table_pe").html(data2);
                $('#tabla_por_emp').DataTable({
                    "pageLength":10,
                    destroy:true,
                    "order": [[ 0, "asc" ]],
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {extend: 'excel'},
                        {extend: 'pdf'},
                        {extend:'print'},
                    ],
                });
                loadPorVende();
            }
        });
    }

    function loadPorVende(){
        /*table_xsuc = $('#tabla_por_vende').DataTable({
            "pageLength":10,
            destroy:true,
            "order": [[ 0, "desc" ]],
             "ajax": {
                "url": "<?php echo base_url(); ?>index.php/Reportes/GetVentaVende",
                 type: "post",
                "data": function(d){
                    d.fecha1 = $('#Fecha1').val(),
                    d.fecha2 = $('#Fecha2').val()
                },
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            responsive: true,
            "columns": [
                {"data": "empresa"},
                {"data": null,
                    "render":function(data, type, row, meta){
                        var montos='';
                        total_sucursal = parseFloat(row.total_sucursal);
                        descuentos = parseFloat(row.descuentos);
                        montos=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal-descuentos);
                        return montos;
                    }
                },
                {"data": null,
                    "render": function (data, type, row, meta) {
                        var tot_viat='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        tot_viat=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_sucursal_viat);
                        return tot_viat;
                    }
                },
                {"data": "tot_fin",
                    "render":function(data, type, row, meta){
                        var total='';
                        total_sucursal_viat = parseFloat(row.total_sucursal_viat);
                        sub = parseFloat(row.total_sucursal)-parseFloat(row.descuentos);
                        total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(sub+total_sucursal_viat);
                        return total;
                    }
                },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Total over this page
                pageTotal = api
                    .column(3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Update footer
                $( api.column(3).footer() ).html(
                    new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(pageTotal)
                );
            },
            dom: 'Bfrtip',
            buttons: [
                'pageLength',
                {extend: 'excel'},
                {extend: 'pdf'},
                {extend:'print'},
            ],
            
        });*/
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Reportes/GetVentaVende",
            data: {fecha1: $("#Fecha1").val(), fecha2: $("#Fecha2").val()},
            async:false,
            success: function (data2) {
                //console.log("data2: "+data2);
                $("#cont_table_pv").html(data2);
                $('#tabla_por_vende').DataTable({
                    "pageLength":10,
                    destroy:true,
                    "order": [[ 0, "asc" ]],
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {extend: 'excel'},
                        {extend: 'pdf'},
                        {extend:'print'},
                    ],
                });
            }
        });
    }
    var label = [];
    var datos = [];
    function barCharVentas(){
        if(window.chart) {
            window.chart.clear();
            window.chart.destroy();
        }
        $.ajax({  
            type: "POST",
            //contentType: "application/json; charset=utf-8",
            url:"<?php echo base_url(); ?>index.php/Reportes/GraficaVentas",
            data: {fecha1: $("#Fecha1").val(), fecha2: $("#Fecha2").val()},
            async: false,
            success:function(result){
                //console.log(result);
                var array = $.parseJSON(result);
                var ctx = document.getElementById('grafica').getContext('2d');
                window.chart = new Chart(ctx, {
                      type: 'bar',
                      title: {
                        text: "Chart with export feature enabled",
                      },
                      exportEnabled: true,
                      data: {
                          labels: array.fechasarray,
                          datasets: [{
                              label: 'Ventas por mes',
                              backgroundColor: 'rgba(54, 162, 235, 0.2)',
                              borderColor: 'rgba(54, 162, 235, 1)',
                              data: array.valoresarray //ok
                          }
                    ]},
                    options: {
                        responsive: true,
                        scales: {
                          yAxes: [{
                            display: true,
                            scaleLabel: {
                              display: true,
                              labelString: 'Monto de Ventas'
                            }
                          }]
                        }
                    }
                });
            }


        });
    }

    function forma_pago(data, type, row, meta) {
        var forma = "50% de anticipo y 50% contraentrega";
        switch (data) {
            case "2":
                forma = "Crédito 30 días";
                break;
            case "3":
                forma = "Crédito 60 días";
                break;
            case "4":
                forma = "100% contraentrega";
                break;
            case "5":
                forma = "En común acuerdo con el cliente";
                break;
        }

        return forma;
    }

    function estatus(data, type, row, meta) {
        var stat = "<span class='badge badge-secondary width-100'>Pendiente</span>";
        switch (data) {
            case "2":
                stat = "<span class='badge badge-success width-100'>Aceptada</span>";
                break;
            case "3":
                stat = "<span class='badge badge-danger width-100'>Rechazada</span>";
                break;
            case "4":
                stat = "<span class='badge badge-warning width-100'>Modificada</span>";
                break;
        }

        return stat;
    }

    function Clave(data, type, row, meta){
        //console.log(data);
        if(data!=null){
            var nueva=data.split("-");
            var codigo=nueva[0];
        }else{
            var codigo='';
        }
        
        return codigo;
    }

    /*function buscarEstatus(status){
        //console.log("busco x estatus: "+status);
        var url;
        if(status==0){
            url = "<?php echo base_url(); ?>index.php/cotizaciones/getData_cotizaciones";
        }
        else{
            url = "<?php echo base_url(); ?>index.php/cotizaciones/getData_cotizacionesEstatus/"+status;
        }
        //console.log("url: "+url);
        table.destroy();
        table = $('#tabla').DataTable({
            "bProcessing": true,
            "serverSide": true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                url: url,
                type: "post",
                error: function () {
                    $("#tabla").css("display", "none");
                }
            },
            "columns": [
                {"data": "id"},
                {"data": "clave",
                    "render":function(data,type,row,meta){
                        return Clave(data);
                    }
                },
                {"data": "alias"},
                {"data": "nombre"},
                //{"data": "monto"},
                {"data": null,
                    "render" : function ( url, type, full) {
                        var msj='';
                        msj+="$" +full["monto"];
                        return msj;
                    }
                },
                {"data": "forma_pago",
                    "render": function (data, type, row, meta) {
                        return forma_pago(data);
                    }
                },
                {"data": "status",
                    "render": function (data, type, row, meta) {
                        return estatus(data);
                    }
                },
            ]
        });
    }*/

</script>