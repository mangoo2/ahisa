<?php 
$razon_social=""; $fecha=""; $num_informe=""; $veri_ini=""; $veri_fin=""; $criterio=""; $cumple_criterio="";
foreach ($datosnom->result() as $i) { 
    $razon_social=$i->razon_social; $fecha=$i->fecha; $num_informe=$i->num_informe; $veri_ini=$i->veri_ini; $veri_fin=$i->veri_fin; $criterio=$i->criterio; $cumple_criterio=$i->cumple_criterio;
}

$luxometro=""; $equipo=""; $marca=""; $modelo="";
foreach ($equi->result() as $i) { 
    $luxometro=$i->luxometro; $equipo=$i->equipo; $marca=$i->marca; $modelo=$i->modelo;
}
$htmlg='';
$htmlg1='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:#c6c9cb;}
                .fon9{font-size:9px;}
                .checked{font-size:17px}
        </style>
        <table border="1" cellpadding="5" align="center" class="fon9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>MEDICIÓN DE CAMPO PARA LA MEDICIÓN DE LA ILUMINACIÓN</td>
                <td>REG-TEC/05-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>';
    $cont_fecha=0;$cont_fecha2=0; $fecha_ant=""; $rowpuntorow=1;
    foreach ($datosnomdetalle as $itemd) {
        if($fecha_ant!=$itemd->fecha){
            $cont_fecha++;
        }
    }
    foreach ($datosnomdetalle as $itemd) {
        if($fecha_ant!=$itemd->fecha){
            $cont_fecha2++;
            $htmlg1.='
            <table border="1" cellpadding="5" align="center" class="fon9">
                <tr>
                    <td class="backg">RAZÓN SOCIAL</td>
                    <td colspan="8" >'.$razon_social.'</td>
                </tr>
                <tr>
                    <td class="backg">FECHA</td>
                    <td colspan="3" >'.$fecha.'</td>
                    <td colspan="2" class="backg">No DE INFORME</td>
                    <td colspan="3" >'.$num_informe.'</td>
                </tr>
                <tr class="backg">
                    <td >ID EQUIPO</td><td >'.$luxometro.'</td>
                    <td >MARCA</td><td >'.$marca.'</td>
                    <td >MODELO</td><td >'.$modelo.'</td>
                    <td >SERIE</td><td >'.$equipo.'</td>
                    <td >X</td>
                    
                </tr>
                <tr class="backg">
                    <td colspan="2" >VERIFICACIÓN INICIAL</td>
                    <td colspan="2" >VERIFICACIÓN FINAL</td>
                    <td colspan="2" >CRITERIO DE ACEPTACIÓN</td>
                    <td colspan="3" >CUMPLE CRITERIO</td>
                </tr>
                <tr>
                    <td colspan="2" >'.$veri_ini.'</td>
                    <td colspan="2" >'.$veri_fin.'</td>
                    <td colspan="2" >'.$criterio.'</td>
                    <td colspan="3" >'.$cumple_criterio.'</td>
                </tr>
                <tr class="backg">
                    <td colspan="5">NOMBRE Y FIRMA DEL INGENIERO DE CAMPO</td>
                    <td colspan="4">NOMBRE Y FIRMA DE RESPONSABLE DE LA REVISIÓN DEL REGISTRO DE CAMPO</td>
                </tr>
                 <tr><td colspan="5"></td><td colspan="4"></td></tr>
                <tr>
                    <td colspan="9" class="backg">CONDICIONES DE OPERACIÓN DAL MOMENTO DE REALIZAR LA MEDICIÓN</td>
                </tr>
                 <tr><td colspan="9"></td></tr>       
            </table>';
            
            $htmlg1.='<table border="1" cellpadding="5" align="center" class="fon9" width="100%">';
            $htmlg1.='<tr class="backg">
                        <td width="5%" >No</td>
                        <td width="13%" >ÁREA</td>
                        <td width="15%" >IDENTIFICACIÓN DEL PUNTO</td>
                        <td width="7%"  colspan="2">PLANO DE TRABAJO</td>
                        <td width="10%" >HORA</td>
                        <td width="20%" colspan="3" >NIVEL DE ILUMINACIÓN</td>
                        <td width="20%" colspan="4">
                            <table border="1" align="center" class="fon9" width="100%">
                                <tr><td colspan="4">REFLEXIÓN(lx)</td></tr>
                                <tr><td colspan="2">PLANO DE TRABAJO</td><td colspan="2">PARED</td></tr>
                                <tr><td width="25%">E1</td><td width="25%">E2</td><td width="25%">E1</td><td width="25%">E2</td></tr>
                            </table>
                        </td>
                        <td width="10%" >NMI</td>
                    </tr>';
            
                //}   
                   
                if($itemd->plano_t1=="v"){
                    $ptra='<p class="checked">✔</p>';
                }else{
                    $ptra='';
                }
                if($itemd->plano_t1=="h"){
                    $ptra2='<p class="checked">✔</p>';
                }else{
                    $ptra2='';
                }
                if($itemd->plano_t1=="o"){
                    $ptra3='<p class="checked">✔</p>';
                }else{
                    $ptra3='';
                } 

                $htmlg1.='<tr>
                    <td width="5%" rowspan="3">'.$rowpuntorow.'</td><td width="13%" rowspan="3">'.$itemd->area.'</td><td width="15%" rowspan="3">'.$itemd->identifica.' <br>'.$itemd->identifica2.'</td><td>H</td><td width="7%">'.$ptra.'</td><td width="10%">'.date("H:i", strtotime($itemd->h1)).'</td><td width="20%">'.$itemd->h1_medicion_a.'</td><td>'.$itemd->h1_medicion_b.'</td><td>'.$itemd->h1_mdicion_c.'</td>
                    <td >'.$itemd->h1_e1a.'</td>
                    <td>'.$itemd->h1_e2a.'</td>
                    <td>'.$itemd->h1_e1b.'</td>
                    <td>'.$itemd->h1_e2b.'</td>
                    <td width="10%" rowspan="3">'.$itemd->nmi.'</td>
                </tr>
                <tr>
                    <td>V</td><td>'.$ptra2.'</td><td>'.date("H:i", strtotime($itemd->h2)).'</td><td>'.$itemd->h2_medicion_a.'</td><td>'.$itemd->h2_medicion_b.'</td><td>'.$itemd->h2_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
                </tr>
                <tr>
                    <td>O</td><td>'.$ptra3.'</td><td>'.date("H:i", strtotime($itemd->h3)).'</td><td>'.$itemd->h3_medicion_a.'</td><td>'.$itemd->h3_medicion_b.'</td><td>'.$itemd->h3_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
                </tr>';
                //if($cont_fecha2==$cont_fecha){
                    $htmlg1.='</table>';
                //}
            $fecha_ant=$itemd->fecha; 
        } //if
        else{
            $htmlg.='<table border="1" cellpadding="5" align="center" class="fon9">';
            //}   
            $fecha_ant=$itemd->fecha;    
            if($itemd->plano_t1=="v"){
                $ptra='<p class="checked">✔</p>';
            }else{
                $ptra='';
            }
            if($itemd->plano_t1=="h"){
                $ptra2='<p class="checked">✔</p>';
            }else{
                $ptra2='';
            }
            if($itemd->plano_t1=="o"){
                $ptra3='<p class="checked">✔</p>';
            }else{
                $ptra3='';
            } 

            $htmlg.='<tr>
                <td rowspan="3">'.$rowpuntorow.'</td><td rowspan="3">'.$itemd->area.'</td><td rowspan="3">'.$itemd->identificacion.'</td><td>H</td><td>'.$ptra.'</td><td>'.date("H:i", strtotime($itemd->h1)).'</td><td>'.$itemd->h1_medicion_a.'</td><td>'.$itemd->h1_medicion_b.'</td><td>'.$itemd->h1_mdicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td><td rowspan="3">'.$itemd->nmi.'</td>
            </tr>
            <tr>
                <td>V</td><td>'.$ptra2.'</td><td>'.date("H:i", strtotime($itemd->h2)).'</td><td>'.$itemd->h2_medicion_a.'</td><td>'.$itemd->h2_medicion_b.'</td><td>'.$itemd->h2_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
            </tr>
            <tr>
                <td>O</td><td>'.$ptra3.'</td><td>'.date("H:i", strtotime($itemd->h3)).'</td><td>'.$itemd->h3_medicion_a.'</td><td>'.$itemd->h3_medicion_b.'</td><td>'.$itemd->h3_medicion_c.'</td><td>'.$itemd->h1_e1a.'</td><td>'.$itemd->h1_e2a.'</td><td>'.$itemd->h1_e1b.'</td><td>'.$itemd->h1_e2b.'</td>
            </tr>
            </table>
            <p></p>';
        }
        $rowpuntorow++;    
    } //foreach
    
    //$htmlg.='</tbody>
    //</table>';
    echo $htmlg1;
?>