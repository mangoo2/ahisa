<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $norma_name='NOM-011-STPS-2001';
    //$GLOBALS['razonsocial']=strtoupper($razonsocial);

    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');
    mb_internal_encoding("UTF-8");

    $date = new Datetime($fecha);
    $fecha = strftime("%d de %B del año %Y", $date->getTimestamp());

    $dateh = new Datetime(date('Y-m-d'));
    $fechah = strftime("%B %d, %Y", $dateh->getTimestamp());
    $fechah = ucfirst($fechah);

    $firmaemple=""; $cedula=""; $firmaresp=""; $tecnico="";
    foreach ($datosoc->result() as $item) {
        $tecnico = $item->tecnico;
        $firmaemple=$item->firmaemple;
        $cedula=$item->cedula;
        $firmaresp=$item->firmaresp;
    }
    $GLOBALS['nom_cargo']="";
    $GLOBALS['id_rec']="";
    foreach ($datosrec->result() as $item) {
        $GLOBALS['nom_cargo']=$item->nom_cargo;
        $GLOBALS['id_rec']=$item->id;
        $empresa =$item->cliente;
        $estado_name=utf8_decode($estado_name);
        $direccion = mb_strtoupper($item->calle_num.', <br> '.($item->colonia).', <br>'.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $direccion2 = mb_strtoupper($item->calle_num.', '.($item->colonia).', '.($item->poblacion).', '.$estado_name.',<br> C.P. '.$item->cp.'',"UTF-8");
        $rfccli=$item->rfc;
        $girocli=$item->giro;
        $representacli=$item->representa;
        $telefonocli=$item->telefono;
        if($folio=="" || $folio==null){
            $folio=$item->num_informe_rec;
        }
    }
    
    $GLOBALS['folio']=$folio;
    $nom_cargo=""; $cargo_cargo="";
    //$GLOBALS['nom_cargo'];

    $n_cargo = explode("/", $GLOBALS['nom_cargo']);
    if(isset($n_cargo[0])){
        $nom_cargo=$n_cargo[0];
    }
    if(isset($n_cargo[1])){
        $cargo_cargo=$n_cargo[1];
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $logos = base_url().'public/img/logo.jpg';
        $this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);
  }
    // Page footer
  public function Footer() {
      $html = '
        <style type="text/css">
            .cmac{font-size:9px; background-color: rgb(0,57,88); font-weight: bold; color:white;}
            .cma{font-size:15px; color:rgb(231,99,0);}
            .cmafolio{font-size:9px; background-color: rgb(231,99,0); font-weight: bold; margin-top:10px;}
            .footerpage{font-size:9px;font-style: italic;}
        </style> 
      <table width="100%" cellpadding="2">
        <tr><td align="center" colspan="2" class="footerpage">Prohibida la reproducción total o parcial de este informe de resultados sin la autorización previa de Ahisa Laboratorio de Pruebas, S. de R.L. de C.V.</td></tr>
        <tr>
            <td align="center" class="cmac" width="84%">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410 <span class="cma">•</span> (222) 226 5395 <span class="cma">•</span> www.ahisa.mx <span class="cma">•</span> ventas@ahisa.mx
            </td>
            <td align="center" class="cmafolio" width="16%"><span class="cma">•</span>'.$GLOBALS['folio'].'</td>
        </tr>
        <tr><td align="center" colspan="2" class="footerpage" >Página '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().' del informe de resultados '.$GLOBALS['folio'].' </td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
      $this->writeHTMLCell(188, '', 12, 274, $html, 0, 0, 0, true, 'C', true);
  }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable NOM-11');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-11');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20,25,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 30);
//$pdf->SetAutoPageBreak(true, 21);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);
// add a page
$pdf->AddPage('P', 'A4'); 
//$pdf->AddPage('P', 'LETTER');
$htmlg='';
$htmlg.='<style>
    td{font-family:calibri;}
    .tdtext{text-align: justify;font-size:15px;font-family: Calibri;}
    .artext{color: rgb(0,57,88);font-family: Calibri;font-size:12px;font-style:italic;}
    .sangria{text-indent: 45px;}
    .fontbold{font-weight: bold;}
    .algc{text-align:center; }
    .ft09{font-size:9px;}
    .ft20{font-size:20px;}
    .ft18{font-size:18px;}
    .ft16{font-size:16px;}
    .ft14{font-size:14px;}
    .ft13{font-size:13px;}
    .ft12{font-size:12px;}
    .ft11{font-size:11px;}
</style>';
$htmlg.='
        <table border="0">
            <tr >
                <td colspan="2" align="right" class="ft20 fontbold">'.$empresa.'</td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td width="50%" ></td>
                <td width="50%" align="right" class="ft14 fontbold">'.$direccion.'</td>
            </tr>
        </table>
        <table border="0">
            <tr><td></td></tr>
            
            <tr>
                <td class="fontbold ft16"><b>'.$nom_cargo.'</b></td>
            </tr>
            <tr>
                <td class="fontbold ft13"><b>'.$cargo_cargo.'</b></td>
            </tr>
        </table>
        <p class="tdtext sangria">Con base en las mediciones realizadas en su centro de trabajo, le hacemos de su conocimiento los resultados correspondientes al Nivel de Exposición a Ruido (NER) y el Nivel de Presión Acústica (NPA) determinados en su centro de trabajo; la metodología seguida para realizar la evaluación es la descrita en la Norma Oficial Mexicana '.$norma_name.'.</p>
        <p class="tdtext sangria">Los resultados expresados en este informe son válidos exclusivamente bajo las condiciones de medición presentadas en su centro de trabajo el día <u>'.$fecha.'</u>.</p>
        <p class="tdtext sangria">Se le comunica que el presente informe de resultados es aprobado y revisado técnicamente por:</p>
        <table border="0" align="center" class="artext">
            <tr><td height="180px" colspan="2"></td></tr>
            <tr>
                <td>Ing. Christian Uriel Fabian Romero</td>
                <td>Ing. Tanya Alejandra Valle Tello</td>
            </tr>
            <tr>
                <td>Aprobó - Gerente General</td>
                <td>Revisó – Procesamiento de Información</td>
            </tr>
            <tr><td colspan="2" height="170px"></td></tr>
            <tr>
                <td colspan="2"><img src="'.base_url().'public/img/logo_ema.jpg" width="180px"></td>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('A. Resultados de la evaluación -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$rowpersonal=$this->ModeloCatalogos->getselectwheren('nom11_personal',array('idnom'=>$idnom,'activo'=>1));
foreach ($rowpersonal->result() as $itemp) { 
    $dosis[$itemp->num]=$itemp->dosis;                                                   
}

$htmlg='<style >
            td{font-family:calibri;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
            .titulosa{font-family:Arialb; font-size:20px;text-align:center,font-weight:bold;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .algc{text-align:center;}
            .ft09{font-size:9px;}
            .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:11px;
                    font-size:8px;
                    valign: middle;
                    vertical-align: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:8px;
                }
                .pspaces{
                    font-size:0.2px;    
                }
            .txt_td{background-color:rgb(217,217,217);}
            .sangria{text-indent: 45px;}
            b{font-family:Arialb;}
        </style>
            <p class="titulosa"><b>A. RESULTADOS DE LA EVALUACIÓN</b></p>
            
            <p class="tdtext sangria">A continuación, se presentan los resultados obtenidos de las mediciones realizadas en las instalaciones de <b>'.$empresa.'</b>, el día '.$fecha.'.</p>
            <p class="tdtext sangria">Los resultados obtenidos de las mediciones son relacionados y válidados exclusivamente para las siguientes áreas e identificación del presente informe:</p>';

        $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,"activo"=>1));
        if($rownomd->num_rows()>0){
            $htmlg.='<p class="algc"><i>•  Nivel de Exposición al Ruido (NER) y Tiempo Máximo Permisible de Exposición (TMPE) de cada punto de medición ambiental.</i></p>
                <table class="tableb" cellpadding="5">
                    <tr>
                        <th width="10%"><p class="pspaces"></p>No. de punto</th>
                        <th width="30%">Identificación del punto de medición (área/ Identificación)</th>
                        <th width="10%"><p class="pspaces"></p>NER (dB(A))</th>
                        <th width="10%"><p class="pspaces"></p>Te</th>
                        <th width="10%"><p class="pspaces"></p>TMPE</th>
                        <th width="30%"><p class="pspaces"></p>Conclusión</th>
                    </tr>';
                $rowpuntostr=1;
                $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
                foreach ($rowpuntos->result() as $item) {
                    $idpunto=$item->punto;
                    $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto,"activo"=>1));
                    foreach ($rownomd->result() as $itemnomd) {
                        $numeronosabemos=8.3;
                        $ner_db=90+9.97*(log10($numeronosabemos/(12.5*$itemnomd->tiempo_ruido)) );
                        if($ner_db<90){
                            $tmpe_h=0;
                            $ner_db_result='No Aplica';
                        }else{
                            $tmpe_h=8/pow(2,(($ner_db-90)/3 ));
                            $ner_db_result= $tmpe_h;
                        }
                        if($ner_db>80){
                            $conclusion_ner='Te supera el TMPE';
                            $class='style="color:red"';
                        }else{
                            $conclusion_ner='Te no supera el TMPE';
                            $class='style="color:green"';
                        }
                        $htmlg.='<tr>
                            <td>'.$rowpuntostr.'</td>
                            <td>'.$itemnomd->area.' / '.$itemnomd->identificacion.'</td>
                            <td>'.round($ner_db,1).'</td>
                            <td>'.$itemnomd->tiempo_ruido.'</td>
                            <td>'.$ner_db_result.'</td>
                            <td '.$class.'>'.$conclusion_ner.'</td>
                         </tr>';
                        $rowpuntostr++;
                    }
                }
            $htmlg.='</table>';
            $htmlg.='<p class="ft09 algc">De acuerdo al punto 7.2 de la NOM-011-STPS-2001 el cálculo del TMPE debe realizarse cuando el NER se encuentre entre dos de las magnitudes consignadas en la tabla 1 de la misma (90 y 105 dBA).<br>
                NER = Nivel de Exposición a Ruido<br>
                U exp = Incertidumbre expandida (K=2, confianza del 95.45%).<br>
                Te = Tiempo de Exposición Real a Ruido del Trabajador<br>
                TMPE = Tiempo Máximo Permisible de Exposición</p>
            <p class="ft09 algc"><u><i>Regla de Decisión</i></u></p>
            <p class="ft09 algc"><u><i>A los resultados obtenidos de la medición original le será sumada la incertidumbre de las mediciones, es decir, será considerada la incertidumbre positiva obtenida de cada medición para poder determinar el cumplimiento de la conformidad.</i></u></p>';
        }

        $result2=$this->ModeloCatalogos->table2a($idnom);
        if($result2->num_rows()>0){ //sí existen mediciones de personal se muestran 
            $htmlg.='<p class="algc"><i>•  Nivel de Exposición al Ruido (NER) y Tiempo Máximo Permisible de Exposición (TMPE) de cada punto de medición personal(dosimetrias).</i></p><table class="tableb" cellpadding="5">
                        <thead>
                            <tr>
                                <th width="10%"><p class="pspaces"></p>No. de punto</th>
                                <th width="30%">Identificación del punto de medición (nombre del trabajador/ puesto)</th>
                                <th width="10%"><p class="pspaces"></p>NER (dB(A))</th>
                                <th width="10%"><p class="pspaces"></p>Te</th>
                                <th width="10%"><p class="pspaces"></p>TMPE</th>
                                <th width="30%"><p class="pspaces"></p>Conclusión</th>
                            </tr>
                        </thead>';
                        $cont_ner=0;
                        foreach ($result2->result() as $item) {
                            $cont_ner++;
                            $difference = date_diff(date_create($item->ti), date_create($item->tf));
                            if($difference->h>0){
                                $ner_db=90+9.97*(log10($dosis[$item->num]/(12.5*$difference->h)) );
                            }else{
                                $ner_db=90+9.97*(log10($dosis[$item->num]/(12.5)) );
                            }
                            if($ner_db<90){
                                $tmpe_h=0;
                                $ner_db_result='No Aplica';
                            }else{
                                $tmpe_h=8/pow(2,(($ner_db-90)/3 ));
                                $ner_db_result= $tmpe_h;
                            }
                            if($ner_db>80){
                                $conclusion_ner='Te supera el TMPE';
                                $class2='style="color:red"';
                            }else{
                                $conclusion_ner='Te no supera el TMPE';
                                $class2='style="color:green"';
                            }
                            $htmlg.='<tr>
                                        <td width="10%" class="tdtext1">'.$item->num.'</td>
                                        <td width="30%" class="tdtext1">'.$item->trabajador.' / '.$item->puesto.'</td>
                                        <td width="10%" class="tdtext1">'.round($ner_db,1).'</td>
                                        <td width="10%" class="tdtext1">'.$difference->h.'</td>
                                        <td width="10%" class="tdtext1">'.$ner_db_result.'</td>
                                        <td width="30%" class="tdtext1" '.$class2.'>'.$conclusion_ner.'</td>
                                    </tr>';
                        }
            $htmlg.='</table>';
            if($cont_ner==12){
                $htmlg.='<table><tr><td height="70px"></td></tr></table>'; 
            }
        }

        $htmlg.='<p class="algc"><i>• Nivel de Presión Acústica Promedio (NPAi), de cada punto de medición.</i></p>';
    $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'activo'=>1));
    $htmlg.='<table class="tableb" cellpadding="3">
                <thead>
                    <tr>
                        <th>No. de Punto</th>
                        <th>dB(A)</th>
                        <th>Lineal</th>
                        <th>31.5</th>
                        <th>63</th>
                        <th>125</th>
                        <th>250</th>
                        <th>500</th>
                        <th>1000</th>
                        <th>2000</th>
                        <th>4000</th>
                        <th>8000</th>
                    </tr>
                </thead>
                <tbody>';
                        
                foreach ($rownomdrea->result() as $itrea) { 
                    $htmlg.='<tr>
                        <td>'.$itrea->punto.'</td>
                        <td>'.$itrea->rea_db.'</td>
                        <td>'.$itrea->rea_lineal.'</td>
                        <td>'.$itrea->rea_31_5.'</td>
                        <td>'.$itrea->rea_63.'</td>
                        <td>'.$itrea->rea_125.'</td>
                        <td>'.$itrea->rea_250.'</td>
                        <td>'.$itrea->rea_500.'</td>
                        <td>'.$itrea->rea_1000.'</td>
                        <td>'.$itrea->rea_2000.'</td>
                        <td>'.$itrea->rea_4000.'</td>
                        <td>'.$itrea->rea_8000.'</td>
                    </tr>';
           
                }
                
                $htmlg.='</tbody>
            </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('B. Conclusiones de la evaluación ----------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlgtab="";
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .sangria{text-indent: 45px;}
            .tdtext{font-family:Arial;text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>B. CONCLUSIONES DE LA EVALUACIÓN</b></p>';

        $cont_concamb=0;
        foreach ($get_conclu->result() as $c) {
            $chartAmb=""; $chartPers="";
            if($c->total_ptos_amb>0){
                if($c->chartAmb!="" && $c->chartAmb!="data:,"){
                    //$chartAmb=$c->chartAmb;
                    $chartAmb='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $c->chartAmb) . '">';
                }
                $cont_concamb++;
                $htmlg.='<p>1. Se evalúa un total de <strong>'.$c->total_ptos_amb.'</strong> puntos <strong>ambientales</strong>, de los cuales: </p>
                    <p class="tdtext sangria">a) <strong>'.$c->no_superan_amb.'</strong> puntos no superan el tiempo máximo permisible de exposición (TMPE).
                    </p>
                    <p class="tdtext sangria">b) <strong>'.$c->superan_amb.'</strong> puntos superan el tiempo máximo permisible de exposición (TMPE).
                    </p> 
                <table border="0" align="center">
                    <tr><td><br></td></tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">'.$chartAmb.'</td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }            
            if($c->total_ptos_pers>0){
                if($c->chartPers!="" && $c->chartPers!="data:,"){
                    //$chartPers=$c->chartPers;
                    $chartPers='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $c->chartPers) . '">';
                }
                if($cont_concamb>0){
                    $ina="c)";
                    $inb="d)";
                }else{
                    $ina="a)";
                    $inb="b)";
                }
                $htmlg.='<p>'.$cont_concamb.'. Se evalúa un total de <strong>'.$c->total_ptos_pers.'</strong> puntos <strong>personales</strong>, de los cuales: </p>
                    <p class="tdtext sangria">'.$ina.' <strong>'.$c->no_superan_pers.'</strong> puntos no superan el tiempo máximo permisible de exposición (TMPE).
                    </p>
                    <p class="tdtext sangria">'.$inb.' <strong>'.$c->superan_pers.'</strong> puntos superan el tiempo máximo permisible de exposición (TMPE).
                    </p> 
                <table border="0" align="center">
                    <tr><td><br></td></tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%">'.$chartPers.'</td>
                        <td width="25%"></td>
                    </tr>
                </table>';
            }
        }//foreach
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('C. Introducción ---------------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{font-family:Arial;text-align: justify;font-size:15px;}
        </style>
        <p class="titulosa"><b>C. INTRODUCCIÓN</b></p>
        <p class="tdtext">El ruido es uno de los peligros laborales más comunes dentro de las industrias y comercios; diariamente los trabajadores se ven expuestos a niveles de ruido medios de 85 decibeles ponderados A (en adelante, dB”A”). Estos niveles de ruido son potencialmente peligrosos para su audición y pueden producir además otros efectos perjudiciales como, por ejemplo: estrés y ansiedad.</p>
        <p class="tdtext">Para prevenir los efectos perjudiciales del ruido para los trabajadores, es preciso elegir con cuidado instrumentos, métodos de medición y procedimientos que permitan evaluar el ruido al que se ven expuestos. Es importante evaluar correctamente los diferentes tipos de ruido (estable, inestable o impulsivo), distinguir los ambientes ruidosos con diferentes espectros de frecuencias, y considerar asimismo las diversas situaciones laborales. Los principales objetivos de la medición del ruido en ambientes laborales son:</p>
        <p class="tdtext sangria">a) identificar a los trabajadores sometidos a exposiciones excesivas.</p>
        <p class="tdtext sangria">b) valorar la necesidad de implantar controles técnicos del ruido y demás tipos de control indicados en la NOM-011-STPS-2001.</p>

        <p class="tdtext">La prevención de las pérdidas auditivas en el trabajo beneficia al trabajador porque preserva las capacidades auditivas que son cruciales para disfrutar de una buena calidad de vida: comunicación interpersonal, disfrute de la música, detección de sonidos y muchas más. El Programa de Conservación de la Audición (PCA) proporciona un beneficio en términos de chequeo sanitario, ya que las pérdidas auditivas de carácter no laboral y las enfermedades auditivas con posible tratamiento suelen detectarse por medio de audiometrías anuales. La reducción de la exposición al ruido también reduce el estrés y la fatiga relacionados con el ruido.</p>
        <p class="tdtext">La empresa se beneficia directamente de la implantación de un PCA eficaz que mantenga a sus trabajadores en buenas condiciones de audición, ya que éstos serán más productivos y versátiles si no se deterioran sus capacidades de comunicación.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('D. Objetivo del estudio -----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('E. Norma de referencia ----------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('F. Datos generales del centro de trabajo evaluado -----------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$pdf->Bookmark('G. Datos generales del laboratorio de pruebas ----------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tdtext1{font-family:Arial;text-align: center;font-size:11px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
        </style>
        <p class="titulosa"><b>D. OBJETIVO DEL ESTUDIO</b></p>
        <p>Determinar el Nivel de Exposición a Ruido (NER), el Tiempo Máximo Permisible de Exposición (TMPE) y Nivel de Presión Acústica (NPA) al que están expuestos los trabajadores de las áreas que presenten un NSA (ruido) igual o superior a 80 dB(A), lo anterior en el centro de trabajo denominado <b>'.$empresa.'</b>
        </p>
        <p class="titulosa"><b>E. NORMA DE REFERENCIA</b></p>
        <p class="tdtext">El presente informe de resultados se realiza conforme a lo solicitado en la '.$norma_name.' "Condiciones de seguridad e higiene en los centros de trabajo donde se genere ruido".</p>
        <table>
                <tr><td class="titulosa"><b>F. DATOS GENERALES DEL CENTRO DE TRABAJO EVALUADO*</b></td></tr>
                <tr><td ></td></tr>
        </table>
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class="cet" width="59%">'.$empresa.'</td>
            </tr>
            <tr>
                <td class="txt_td"><br><br><i>b)  Domicilio</i></td>
                <td class="cet">'.$direccion2.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class="cet">'.$rfccli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d)  Giro y/o actividad principal</i></td>
                <td class="cet">'.$girocli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e)  Responsable de la empresa</i></td>
                <td class="cet">'.$representacli.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f)  Teléfono</i></td>
                <td class="cet"> '.$telefonocli.'</td>
            </tr>
        </table>
        <table><tr><td style="font-size:10px; text-align:center">* Información suministrada por el cliente</td></tr>
                <tr><td ></td></tr>
                <tr><td class="titulosa"><b>G. DATOS GENERALES DEL LABORATORIO DE PRUEBAS</b></td></tr>
                <tr><td ></td></tr>
        </table>
        
        <table cellpadding="4" class="tdtext1 tableb">
            <tr>
                <td class="txt_td" width="41%"><i>a)  Nombre, denominación o razón social</i></td>
                <td class=" cet" width="59%">AHISA Laboratorio de Pruebas, S. de R.L. de C.V.</td>
            </tr>
            <tr>
                <td class="txt_td"><i>b)  Domicilio</i></td>
                <td class=" cet">19 poniente No. 1508 Int. 1, Barrio de Santiago, Puebla, Puebla, C.P. 72410</td>
            </tr>
            <tr>
                <td class="txt_td"><i>c)  R.F.C.</i></td>
                <td class=" cet">ALP160621FD6</td>
            </tr>
            <tr>
                <td class="txt_td"><i>d) Teléfono</i></td>
                <td class=" cet">(01) 222 2265395</td>
            </tr>
            <tr>
                <td class="txt_td"><i>e) e-mail </i></td>
                <td class="cet">gerencia@ahisa.mx</td>
            </tr>
            <tr>
                <td class="txt_td"><i>f) Número de acreditación </i></td>
                <td class="cet">AL-1246-104/20 (ver alcance acreditado en el anexo IX)</td>
            </tr>
            <tr>
                <td class="txt_td"><i>g) Número de aprobación</i></td>
                <td class="cet">LPSTPS-153/2022</td>
            </tr>
            <tr>
                <td class="txt_td"><i>h) Lugar de expedición del informe </i></td>
                <td class="cet">Puebla, Puebla - México </td>
            </tr>
            <tr>
                <td class="txt_td"><i>i) Fecha de expedición del informe de resultados.</i></td>
                <td class="cet">'.$fechah.'</td>
            </tr>
            <tr>
                <td class="txt_td"><i>j) Signatario responsable de la evaluación</i></td>
                <td class="cet">'.$tecnico.'</td>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$datosdet=$this->ModeloNom->getPaso2_3($GLOBALS['id_rec']);
$pdf->Bookmark('H. Metodología de para la evaluación ---------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .cet{text-align:center;}
            .checked{font-size:17px;font-family:dejavusans;}
            .pspaces{font-size:0.2px;}
        </style>
        <p class="titulosa"><b>H. METODOLOGÍA PARA LA EVALUACIÓN</b></p>
        <p><u><i>Reconocimiento.</i></u></p>
        <p class="tdtext tejus">Esta actividad debe realizarse previamente a la evaluación y consiste en recabar toda aquella información técnica y administrativa que permita seleccionar el método de evaluación y la prioridad de las zonas y puestos por evaluar. Esta información debe comprender:</p>
        <p class="tdtext tejus">a) planos de distribución de las áreas en que exista ruido y de la maquinaria y equipo generadores de ruido;</p>
        <p class="tdtext tejus">b) descripción del proceso de fabricación;</p>
        <p class="tdtext tejus">c) descripción de los puestos de trabajo expuestos a ruidos;</p>
        <p class="tdtext tejus">d) programas de mantenimiento de maquinaria y equipo generadores de ruidos;</p>
        <p class="tdtext tejus">e) registros de producción;</p>
        <p class="tdtext tejus">f) número de trabajadores expuestos a ruidos por área y por proceso de fabricación, incluyendo el tiempo de exposición;</p>
        <p class="tdtext tejus">g) reporte del reconocimiento sensorial de las zonas por evaluar, con el objeto de determinar las características del ruido (estable, inestable o impulsivo).</p>

        <p><u><i>Condiciones para la evaluación.</i></u></p>
        <p class="tdtext tejus">a) La evaluación de los NSA o NSCEA,T, debe realizarse bajo condiciones normales de operación.</p>
        <p class="tdtext tejus">b) La evaluación debe realizarse como mínimo durante una jornada laboral de 8 horas y en aquella jornada que, bajo condiciones normales de operación, presente la mayor emisión de ruido.</p>
        <p class="tdtext tejus">b) Si la evaluación dura más de una jornada laboral, en todas las jornadas en que se realice se deben conservar las condiciones normcles de operación.</p>
        <p class="tdtext tejus">d) Se debe usar pantalla contra viento en el micrófono de los instrumentos de medición, durante todo el tiempo que dure la evaluación.</p>

        <table><tr><td><br><br><br><br><br><br></td></tr></table>

        <p><u><i>Métodos de evaluación.</i></u></p>
        <p class="tdtext sangria"><u><i>Puntos de medición</i>.</u></p>
        <p class="tdtext tejus">Los puntos de medición deben seleccionarse de tal manera que describan el entorno ambiental de manera confiable, determinando su número, entre otros factores, por la ubicación de los puestos de trabajo o posiciones de control de la maquinaria y equipo del local de trabajo, el proceso de producción y las facilidades para su ubicación.</p>
        <p class="tdtext tejus">Todos los puntos de medición de una zona de evaluación deben identificarse con un número progresivo y registrar su posición en el plano correspondiente, según lo establecido en el inciso a) del Apartado B.4. de la norma NOM-011-STPS-2001.</p>


        <p><u><i>Calibración del equipo de medición en campo.</i></u></p>
        <p class="tdtext sangria">El equipo de medición debe ser calibrado al inicio y final de cada evaluación con un calibrador acústico. La calibración en campo deberá cumplir con el criterio de aceptación de ± 1 dB entre la calibración inicial y la calibración final.</p>

        <p><u><i>Ubicación.</i></u></p>
        <p class="tdtext tejus">La ubicación de los puntos de medición en función de las necesidades y características físicas y acústicas de cada local de trabajo debe efectuarse seleccionando el método conforme se indica en la tabla siguiente:</p>
        <table align="center" class="tableb">
            <thead>
                <tr class="txt_td">
                    <td rowspan="2"><p class="pspaces"></p><i>TIPO DE RUIDO</i></td>
                    <td colspan="3" align="center"><i>Método para la ubicación de los puntos de medición</i></td>
                </tr>
                <tr class="txt_td">
                    <td><i>Gradiente de Presión Sonora</i></td>
                    <td><i>Prioridad de Áreas de Evaluación</i></td>
                    <td><i>Puesto Fijo de Trabajo</i></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="txt_td"><i>Ruido estable</i></td>
                    <td>SI</td>
                    <td>NO</td>
                    <td>NO</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Ruido inestable</i></td>
                    <td>SI</td>
                    <td>SI</td>
                    <td>SI</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Ruido impulsivo</i></td>
                    <td>SI</td>
                    <td>SI</td>
                    <td>SI</td>
                </tr>
            </tbody>
        </table>
        <p class="tdtext tejus">La evaluación de los puntos presentados en este informe se realizó bajo el(los) siguiente(s) método(s):</p>
        <table align="center" class="tableb">
            <thead >
                <tr class="txt_td">
                    <td width="40%" rowspan="2"><p class="pspaces"></p><i>Área</i></td>
                    <td width="60%" colspan="4" align="center"><i>Método de evaluación </i></td>
                </tr>
                <tr class="txt_td">
                    <td><i>G.P.S.</i></td>
                    <td><i>P.A.E.</i></td>
                    <td><i>P.F.T.</i></td>
                    <td><i>E.P.</i></td>
                </tr>
            </thead>
            <tbody>';
           
            foreach ($datosdet as $i) {
                $chpgra=" --"; $chppri="--"; $chp="--"; $chppf="--";
                if($i->gradiente>0){
                    $chpgra='<p class="checked">✔</p>';
                }
                if($i->prioridad>0){
                    $chppri='<p class="checked">✔</p>';
                }
                if($i->puesto_fijo>0){
                    $chppf='<p class="checked">✔</p>';
                }
                if($i->personal>0){
                    $chp='<p class="checked">✔</p>';
                }
                $htmlg.='<tr>
                    <td width="40%">'.$i->area.'</td>
                    <td width="15%">'.$chpgra.'</td>
                    <td width="15%">'.$chppri.'</td>
                    <td width="15%">'.$chppf.'</td>
                    <td width="15%">'.$chp.'</td>
                </tr>';
            }
            $htmlg.='</tbody>
        </table>
        <p style="color: rgb(0,32,96); font-size:8pt;" align="center">G.P.S.: Gradiente de Presión Sonora; P.A.E.: Prioridad de Área de Evaluación; P.F.T.: Puesto Fijo de Trabajo; E.P.: Evaluación Personal (dosimetrías)</p>';

        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->AddPage('P', 'A4');   

        $htmlg='<style type="text/css">
            .tdtext{font-family:Arial;text-align: justify;font-size:14.5px;}
            .tejus{text-align: justify;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{border:1px solid #808080;}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
        </style>
        <p><u><i>El método de gradiente de presión sonora:</i></u></p>
        <p class="tdtext tejus">a) el punto inicial debe fijarse al centro de la zona de evaluación, registrándose el NSA máximo (el cual debe utilizarse como referencia para iniciar la evaluación);</p>
        <p class="tdtext tejus">b) el observador se debe desplazar con el sonómetro en una trayectoria previamente determinada, hasta encontrar un NSA que difiera ± 3 dB(A), respecto al punto de referencia, marcando en el plano de distribución este punto. El procedimiento se repite a lo largo de esa trayectoria, hasta cubrir completamente la trayectoria de evaluación. Los puntos de medición son aquellos que registren su NSA, con diferencia de ± 3 dB(A) del punto de medición contiguo;</p>
        <p class="tdtext tejus">c) una vez concluida esa trayectoria, se procede de la forma descrita anteriormente, pero en forma transversal;</p>
        <p class="tdtext tejus">d) las trayectorias de ubicación de puntos de medición deben hacerse en función de las características del local de trabajo y de la distribución espacial del campo sonoro, pero siempre debe garantizarse que se ha cubierto toda la zona de trabajo; </p>
        <p class="tdtext tejus">e) la distancia entre puntos de medición no debe ser mayor de 12 metros;</p>
        <p class="tdtext tejus">f) cuando se han identificado todos los puntos de medición debe procederse a su evaluación.</p>

        <p><u><i>Método de Prioridad de Áreas de Evaluación:</i></u></p>
        <p class="tdtext">a) del análisis de la información, realizado en el reconocimiento sensorial, deben determinarse las zonas de evaluación; </p>
        <p class="tdtext tejus">b) las zonas de trabajo identificadas con NSA superior o igual a 80 dB(A), deben dividirse en áreas, guiándose por los ejes de columnas del plano de distribución de planta y cuidando que éstas no sean superiores a 6 metros por lado. No deben incluirse las áreas o pasillos de circulación; </p>
        <p class="tdtext tejus">c) una vez efectuada la división, deben identificarse aquellas áreas en las que existan trabajadores, a las que se les denominará áreas de evaluación; </p>
        <p class="tdtext tejus">d) las áreas de evaluación pueden ser jerarquizadas, exponiendo las razones en el registro de evaluación del estudio de niveles sonoros; </p>
        <p class="tdtext tejus">e) los puntos de medición en las áreas de evaluación deben ubicarse en las zonas de mayor densidad de trabajadores. De no ser posible esta ubicación, deben localizarse en el centro geométrico de cada área.</p>

        <table><tr><td><br><br><br><br></td></tr></table>
        <p><u><i>Método Puesto Fijo de Trabajo:</i></u></p>
        <p class="tdtext tejus">Para evaluar ruido en puesto fijo de trabajo, el punto de medición debe ubicarse en el lugar que habitualmente ocupa el trabajador o, de no ser posible, lo más cercano a él, sin interferir en sus labores.</p><br>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');

        $pdf->Bookmark('I. Instrumento de medición utilizado -----------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0)); //trar los intrumentos utilizados en levantamiento
        $htmlg='<style type="text/css">
                .txt_td{background-color:rgb(217,217,217);}
                .tableb td{border:1px solid #808080;}
                .titulosa{font-family:Arialb;font-size:20px;text-align:center;margin:0px;}
                .ft13{font-size:13px;}
                .ft11{font-size:11.5px;} 
                .div_diso{ float:left !important; }
                .pspaces{ font-size:0.5px; }
            </style>
            <p class="titulosa"><b>I. INSTRUMENTO DE MEDICIÓN UTILIZADO</b></p>
            <table cellpadding="5" align="center" class="tableb ft13" width="100%">';

        //trae los equipos utilizados de tipo sonometro
        $getson = $this->ModeloNom->getEquiposNom($idnom);
        foreach($getson as $s){
            $id_sonometro=$s->id_sonometro;
            $id_calibrador=$s->id_calibrador;
            $htmlg.='
                <tr class="txt_td">
                    <td></td><td><i>Sonómetro Integrador</i></td>
                    <td><i>Filtro en bandas de octava</i></td>
                    <td><i>Calibrador Acústico</i></td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>
                    <td >'.$s->marca.'</td>
                    <td >'.$s->marcacal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>
                    <td >'.$s->modelo.'</td>
                    <td >'.$s->modelocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>
                    <td >'.$s->equipo.'</td>
                    <td >'.$s->equipocal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>No. de Informe de calibración</i></td><td class="ft11"><p class="pspaces"></p>'.$s->no_informe_calibracion.'</td>
                    <td class="ft11" ><p class="pspaces"></p>'.$s->num_calibra.'</td>
                    <td class="ft11" ><p class="pspaces"></p>'.$s->no_informe_calibracioncal.'</td>
                </tr>
                <tr>
                    <td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>
                    <td >'.$s->fecha_calibra.'</td>
                    <td >'.$s->fecha_calibracioncal.'</td>
                </tr>';
        }
        $htmlg.='</table><p></p>';

        //trae los equipos utilizados para personal
        $getper = $this->ModeloNom->getEquiposPersonalNom($idnom);
        $contp=0; $htmlgInt=""; $htmlgf="";
        //$htmlg.='<table border="1" cellpadding="5" align="center" class="tableb" width="100%"><tr><td>';
        foreach($getper as $s){
            $contp++;
            $htmlg.='<!--<div class="div_diso">
                <table align="left" width="50%">
                    <tr>
                        <td></td>
                    </tr>
                </table>-->
                <table width="100%">
                    <tr>
                        <td width="15%">
                            <table width="100%"><tr><td></td></tr></table>
                        </td>

                        <td width="70%">
                            <table align="center" class="tableb ft13" width="100%">
                                <tr class="txt_td">
                                    <td width="60%"></td><td width="40%"><i>Dosímetro de ruido</i></td>
                                </tr>
                                <tr>
                                    <td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>
                                </tr>
                                <tr>
                                    <td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>
                                </tr>
                                <tr>
                                   <td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>
                                </tr>
                                <tr>
                                    <td class="txt_td"><i>No. de Informe de calibración</i></td><td>'.$s->no_informe_calibracion.'</td>
                                </tr>
                                <tr>
                                    <td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>
                                </tr>
                            </table>
                        </td>
                        <td width="15%">
                            <table width="100%"><tr><td></td></tr></table>
                        </td>
                    </tr>
                </table>
            <!--</div>-->';
            if($contp==4){
                $htmlg.='<table align="center"><tr><td height="50px"></td></tr></table>';
            }
            if($contp>=12 && $contp%12==0){
                $htmlg.='<table align="center"><tr><td height="65px"></td></tr></table>';
            }
        }

        /*$contp=0; $band=0; $aux=0;
        $htmlg.='<table border="1" style="font-size:10px" cellpadding="5" align="center" class="tableb">';
        foreach($getper as $s){
            $contp++;
            if($aux>3){ //para la segunda fila
                $htmlg.='</tr><tr><td><table align="center" class="tableb"><tr>';
            }

            if($contp==1 && $band==0){
                $htmlg.='<tr><td><table align="center" class="tableb">';
            }else if($contp==1 && $band>0){
                $htmlg.='<td><table align="center" class="tableb">';
            }
            $aux++;
            $htmlg.='
                <tr class="txt_td">';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td></td><td><i>Dosímetro de ruido</i></td>';
                    else
                        $htmlg.='<td><i>Dosímetro <br> de ruido</i></td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td class="txt_td"><i>Marca</i></td><td >'.$s->marca.'</td>';
                    else 
                        $htmlg.='<td >'.$s->marca.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td class="txt_td"><i>Modelo</i></td><td >'.$s->modelo.'</td>';
                    else
                        $htmlg.='<td >'.$s->modelo.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td class="txt_td"><i>Número de Serie</i></td><td >'.$s->equipo.'</td>';
                    else 
                        $htmlg.='<td >'.$s->equipo.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td class="txt_td"><i>No. de Informe de calibración</i></td><td class="ft11">'.$s->no_informe_calibracion.'</td>';
                    else
                        $htmlg.='<td class="ft11">'.$s->no_informe_calibracion.'</td>';
                $htmlg.='</tr>
                <tr>';
                    if($contp==1 && $aux==1 || $aux%5==0)
                        $htmlg.='<td class="txt_td"><i>Fecha de calibración</i></td><td >'.$s->fecha_calibracion.'</td>';
                    else
                        $htmlg.='<td >'.$s->fecha_calibracion.'</td>';
                $htmlg.='</tr>';

            if($contp%1==0){ //para la segunda fila
                $htmlg.='</table></td>';
                $contp=0; $band++; 
            }
            if($aux%5==0 && end($getper) || $aux>3 && end($getper)){ //para la segunda fila
                $htmlg.='</tr>';
            }
            if($aux==3){
                //$htmlg.='</table>';
            }
        }
        if($contp==1 && $band==0 || end($getper)){
            $htmlg.='</table>';
        }
        $htmlg.='</td></tr></table>';*/

        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$htmlg="";
        
        $pdf->Bookmark('J. Informe descriptivo de las condiciones de operación -----------------------------------------------', 0, 0, '', 'B', array(0,0,0));
        
        $htmlg.='<p class="titulosa"><b>J. INFORME DESCRIPTIVO DE LAS CONDICIONES DE OPERACIÓN</b></p>
        <table>
            <tr><td class="tdtext"> '.$txt_desc_proc.' </td></tr>
        </table>';

//log_message('error', 'htmlg: '.$htmlg);
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg2=""; $htmlg3="";
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('K. Descripción del proceso de fabricación del centro de trabajo evaluado -----------------------', 0, 0, '', '', array(0,0,0));
$pdf->Bookmark('L. Descripción de los puestos de trabajo ------------------------------------------------------------------', 0, 0, '', '', array(0,0,0));
$htmlg='<style type="text/css">
            .tdtext{text-align: justify;font-size:13px;}
            .sangria{text-indent: 45px;}
            .txt_td{background-color:rgb(217,217,217);}
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tableb td{border:1px solid #808080;}
            .tableb {font-size:11px;}
            .cet{text-align:center;}
        </style>
        <p class="titulosa"><b>K.  DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</b></p>
        <p>Ver anexo V.</p>
        <p class="titulosa"><b>L.  DESCRIPCIÓN DE LOS PUESTOS DE TRABAJO</b></p>
        <table cellpadding="5" align="center" class="tableb">
            <thead>
                <tr class="txt_td">
                    <td width="30%">Área</td>
                    <td width="25%">Puesto de Trabajo</td>
                    <td width="45%">Actividad(es)</td>
                </tr>
            </thead>';
            foreach($datosrec->result() as $d){
                $id_reconocimiento=$d->id;
            }
            //log_message('error', 'id_reconocimiento: '.$id_reconocimiento);
            $cont_td=0;
            $datosdetalle=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$id_reconocimiento,"estatus"=>1));
            foreach ($datosdetalle->result() as $p2) {
                $cont_td++;
                //if($p2->actividades!=""){
                    $htmlg.='<tr>
                        <td width="30%">'.$p2->area.'</td><td width="25%">'.$p2->puesto.'</td><td width="45%">'.$p2->descrip.'</td>
                    </tr>';
                //} 
                $htmlg2.='<tr>
                        <td width="30%">'.$p2->area.'</td><td width="25%">'.$p2->puesto.'</td><td width="45%">'.$p2->num_trabaja.'</td>
                    </tr>';
                $htmlg3.='<tr>
                        <td width="30%">'.$p2->area.'</td><td width="25%">'.$p2->puesto.'</td><td width="45%">'.$p2->tiempo_expo.'</td>
                    </tr>';
            }
            $pdf->Bookmark('M. Número de trabajadores por área y puesto de trabajo -------------------------------------------', 0, 0, '', '', array(0,0,0));
            //log_message('error', 'cont_td: '.$cont_td);
            if($cont_td>=15){
                $htmlg.='</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');
                $pdf->AddPage('P', 'A4');
              
                $htmlg='<style type="text/css">
                    .txt_td{background-color:rgb(217,217,217);}
                    .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
                    .tableb td{border:1px solid #808080;}
                    .cet{text-align:center;}
                    .tableb {font-size:11px;}
                </style>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <tr class="txt_td">
                        <td width="30%">Área</td><td width="25%">Puesto de Trabajo</td><td width="45%">No. de Trabajadores</td>
                    </tr>';
                    $htmlg.= $htmlg2
                .'</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');  
            }
            else{
                $htmlg.='</table>
                <p></p>
                <p class="titulosa"><b>M. NÚMERO DE TRABAJADORES POR ÁREA Y PUESTO DE TRABAJO</b></p>
                <table cellpadding="5" align="center" class="tableb">
                    <tr class="txt_td">
                        <td width="30%">Área</td><td width="25%">Puesto de Trabajo</td><td width="45%">No. de Trabajadores</td>
                    </tr>';
                    $htmlg.= $htmlg2
                .'</table>';
                $pdf->writeHTML($htmlg, true, false, true, false, '');   
            }

        
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('N. Tiempo de exposición a ruido por áreas y puesto de trabajo ------------------------------------', 0, 0, '', '', array(0,0,0)); 
$pdf->Bookmark('Ñ. Criterios utilizados para seleccionar el método de evaluación -----------------------------------', 0, 0, '', '', array(0,0,0)); 
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{text-align: justify;font-size:14px;}
            .txt_td{background-color:rgb(217,217,217);}
            .tableb td{
                border:1px solid #808080;
            }
            .tableb {font-size:11px;}
        </style>
        <p class="titulosa"><b>N. TIEMPO DE EXPOSICIÓN A RUIDO POR ÁREAS Y PUESTO DE TRABAJO</b></p>
        <table cellpadding="5" align="center" class="tableb">
            <tr class="txt_td">
                <td width="30%">Área</td><td width="25%">Puesto de Trabajo</td><td width="45%">Tiempo de Exposición (h)</td>
            </tr>';
            $htmlg.= $htmlg3
        .'</table>
        <p class="titulosa"><b>Ñ. CRITERIOS UTILIZADOS PARA SELECCIONAR EL MÉTODO DE EVALUACIÓN</b></p>
        <p class="tdtext">Derivado del reconocimiento se identificaron las áreas susceptibles a evaluación, donde se determinó el tipo de ruido con base a los siguientes datos:</p>

        <table cellpadding="5" align="center" class="tableb">
            <tr class="txt_td">
                <td>Área</td>
                <td>NSA mínimo (dB(A))</td>
                <td>NSA máximo (dB(A))</td>
                <td>Tipo de Ruido</td>
            </tr>';
            $da3=$this->ModeloNom->getPaso2_3($id_reconocimiento);
            foreach ($da3 as $p2) {
                $tipo_ruido=$p2->tipo_ruido;
                if($p2->tipo_ruido=="E")
                    $tipo_ruido="Estable";
                else if($p2->tipo_ruido=="I")
                    $tipo_ruido="Inestable";
                else if($p2->tipo_ruido=="IM")
                    $tipo_ruido="Impulsivo";
                $htmlg.='<tr>
                            <td>'.$p2->area.'</td><td>'.$p2->min_db.'</td><td>'.$p2->max_db.'</td><td>'.$tipo_ruido.'</td>
                </tr>';
            }
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('O. Vigencia del informe ----------------------------------------------------------------------------------------', 0, 0, '', '', array(0,0,0)); 
$htmlg='<style type="text/css">
            .titulosa{font-family:Arialb;font-size:20px;text-align:center;}
            .tdtext{text-align: justify;font-size:14px;}
        </style>
        <p class="titulosa"><b>O. VIGENCIA DEL INFORME</b></p>
        <p class="tdtext">La vigencia de este informe de resultados será de dos años, a menos que se modifique la maquinaría, el equipo, su distribución o las condiciones de operación, de tal manera que puedan ocasionar variaciones en los resultados de la evaluación del ruido.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('ANEXOS ------------------------------------------------------------------------------------------------------------', 0, 0, '', 'I', array(0,0,0));
$pdf->Bookmark('I. Reconocimiento inicial y hojas de campo --------------------------------------------------------', 1, 0, '', '', array(0,0,0)); 
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO I</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">RECONOCIMIENTO INICIAL Y HOJAS DE CAMPO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->setPrintHeader(false);
$pdf->SetMargins(15, '10', 15);

$htmlg='';

$pdf->AddPage('P', 'A4');
$dc=$datosrec->row();
$priero="---";
$pero_comida="---";
$segdo="---";
$sdo_comida="---";
$tercero="---";
$tero_comida="---";
$hora_admin="---";
$adm_comida="---";
$hora_otro="---";
$otro_comida="---";
$desc_proceso="---";
$plan_mante="---";
$registros_prod="---";


if($dc->priero!="")
    $priero=$dc->priero;
if($dc->pero_comida!="")
    $pero_comida=$dc->pero_comida;
if($dc->segdo!="")
    $segdo=$dc->segdo;
if($dc->sdo_comida!="")
    $sdo_comida=$dc->sdo_comida;
if($dc->tercero!="")
    $tercero=$dc->tercero;
if($dc->tero_comida!="")
    $tero_comida=$dc->tero_comida;
if($dc->hora_admin!="")
    $hora_admin=$dc->hora_admin;
if($dc->hora_admin!="")
    $adm_comida=$dc->adm_comida;
if($dc->hora_otro!="")
    $hora_otro=$dc->hora_otro;
if($dc->otro_comida!="")
    $otro_comida=$dc->otro_comida;

if($dc->desc_proceso!="")
    $desc_proceso=$dc->desc_proceso;
if($dc->plan_mante!="")
    $plan_mante=$dc->plan_mante;
if($dc->registros_prod!="")
    $registros_prod=$dc->registros_prod;

$htmlg='<style type="text/css"> 
    .borderbottom{border-bottom:1px solid black;}
    .backg{background-color:rgb(217,217,217);}
    .alg{ text-align:center; }
    .ft9{ font-size:9px; text-align:center; }
    .ft7{ font-size:7px; text-align:center; }
    .fontbold{font-weight: bold;}
 </style>';
$htmlg.='<table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2"><img src="'.base_url().'public/img/logo.jpg" ></td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>
        <table border="0" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="20%"><b>No. DE INFORME</b></td>
                <td width="50%" class="borderbottom">'.$dc->num_informe_rec.'</td>
                <td width="10%"><b>Fecha:</b></td>
                <td width="20%" class="borderbottom">'.$dc->fecha.'</td>
            </tr>
            <tr>
                <td width="20%"><b>RAZÓN SOCIAL</b></td>
                <td width="80%" class="borderbottom">'.$dc->cliente.'</td>
            </tr>
            <tr >
                <td width="60%" class="borderbottom">'.$dc->calle_num.'</td>
                <td width="40%" class="borderbottom">'.$dc->colonia.'</td>
            </tr>
            <tr>
                <td width="60%" ><b>CALLE Y NÚMERO</b></td>
                <td width="40%" ><b>COLONIA</b></td>
            </tr>
            <tr >
                <td width="34%" class="borderbottom">'.$dc->poblacion.'</td>
                <td width="33%" class="borderbottom">'.mb_strtoupper($estado_name,"UTF-8").'</td>
                <td width="33%" class="borderbottom">'.$dc->cp.'</td>
            </tr>
            <tr>
                <td width="34%" ><b>MUNICIPIO</b></td>
                <td width="33%" ><b>ESTADO</b></td>
                <td width="33%" ><b>CÓDIGO POSTAL</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->rfc.'</td>
                <td width="50%" class="borderbottom">'.$dc->giro.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>RFC</b></td>
                <td width="50%" ><b>GIRO DE LA EMPRESA</b></td>
            </tr>
            <tr >
                <td width="50%" class="borderbottom">'.$dc->telefono.'</td>
                <td width="50%" class="borderbottom">'.$dc->representa.'</td>
            </tr>
            <tr>
                <td width="50%" ><b>NÚMERO TELEFÓNICO</b></td>
                <td width="50%" ><b>REPRESENTANTE LEGAL</b></td>
            </tr>
            <tr>
                <td width="100%" class="borderbottom">'.$dc->nom_cargo.'</td>
            </tr>
            <tr>
                <td width="100%" ><b>NOMBRE Y CARGO A QUIEN SE DIRIGIRÁ EL INFORME DE RESULTADOS</b></td>
            </tr>
        </table>                
        <p></p>
        <table class="table ft9" border="1" align="center"> 
           
                <tr style="color: black;" class="ft9 fontbold alg">
                    <td colspan="3" class="backg">TURNOS DE TRABAJO</td>
                </tr>
                <tr style="color: black;" class="ft9 fontbold alg">
                    <td width="30%" class="backg">TURNOS</td>
                    <td width="35%" class="backg">HORARIOS</td>
                    <td width="35%" class="backg">TIEMPO DE COMIDA (min)</td>
                </tr>

                <tr>
                    <td class="backg">1er</td>
                    <td>'.$priero.'</td>
                    <td>'.$pero_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">2do</td>
                    <td>'.$segdo.'</td>
                    <td>'.$sdo_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">3er</td>
                    <td>'.$tercero.'</td>
                    <td>'.$tero_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">ADMINISTRATIVO</td>
                    <td>'.$hora_admin.'</td>
                    <td>'.$adm_comida.'</td>
                </tr>
                <tr>
                    <td class="backg">OTRO</td>
                    <td>'.$hora_otro.'</td>
                    <td>'.$otro_comida.'</td>
                </tr>
            
        </table>
        <p></p>
        <table class="table ft9" border="1">
            <tr align="center">
                <td class="backg"><b>DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN</b></td>
            </tr>
            <tr>
                <td height="80px">'.$desc_proceso.'</td>
            </tr>
        </table>

        <table class="table ft9" border="1" align="center">
            <tr align="center">
                <td class="backg"><b>PLAN DE MANTENIMIENTO DE MAQUINARIA Y EQUIPO GENERADORES DE RUIDO</b></td>
                <td class="backg"><b>REGISTROS DE PRODUCCIÓN</b></td>
            </tr>
            <tr>
                <td height="40px">'.$plan_mante.'</td>
                <td height="40px">'.$registros_prod.'</td>
            </tr>
        </table>
    
        <p class="ft9"><b>MÉTODOS DE UBICACIÓN DE LOS PUNTOS DE MUESTREO</b></p>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr class="backg">
                <td>TIPO DE RUIDO</td>
                <td>GRADIENTE DE PRESIÓN SONORA (GPS)</td>
                <td>PRIORIDAD DE ÁREAS DE EVALUACIÓN (PAE)</td>
                <td>PUESTO FIJO DE TRABAJO (PFT)</td>
                <td>EVALUACIÓN PERSONAL DE RUIDO (DOSIMETRÍA)</td>
            </tr>
            <tr>
                <td>RUIDO ESTABLE</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
            <tr>
                <td>RUIDO INESTABLE</td>
                <td>NO</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
            <tr>
                <td>RUIDO IMPULSIVO</td>
                <td>NO</td>
                <td>SI</td>
                <td>SI</td>
                <td>SI</td>
            </tr>
        </table>
        <p class="ft7">Ruido estable: Es aquel que se registra con variaciones en su nivel sonora "A" dentro de un intervalo de 5 dB(A).<br>
        Ruido inestable: Es aquel que se registra con variaciones en su nivel sonora "A" con un intervalo mayor a 5 dB(A).<br>
        Ruido impulsivo: Es aquel ruido inestable que se registra durante un periodo menor a un segundo.</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

/* ********************PASO 2 de reconocimiento ************************* */
$pdf->AddPage('P', 'A4');
$htmlg='';

$datosdet=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso2_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$htmlg='<style>
        .txt_td { background-color:rgb(217,217,217); }
        .algc{text-align:center; }
        .ft9{font-size:9px;}
        .pspaces{ font-size:0.2px; }
    </style>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2">
                <img src="'.base_url().'public/img/logo.jpg" >
            </td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
            <td>REG-TEC/02-01</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <p></p>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <thead>
            <tr class="txt_td">
                <th width="10%"><p class="pspaces"></p>No.</th>
                <th width="20%"><p class="pspaces"></p>NOMBRE DEL ÁREA</th>
                <th width="15%"><p class="pspaces"></p>PUESTO DE TRABAJO</th>
                <th width="13%"><p class="pspaces"></p>No. DE TRABAJADORES</th>
                <th width="32%"><p class="pspaces"></p>DESCRIPCIÓN DE ACTIVIDADES</th>
                <th width="10%">TIEMPO DE EXPOSICIÓN REAL AL RUIDO (h)</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($datosdet->result() as $i) {
            $htmlg.='<tr>
                <td width="10%">'.$i->num.'</td>
                <td width="20%">'.$i->area.'</td>
                <td width="15%">'.$i->puesto.'</td>
                <td width="13%">'.$i->num_trabaja.'</td>
                <td width="32%">'.$i->descrip.'</td>
                <td width="10%">'.$i->tiempo_expo.'</td>
            </tr>';
        }
        $rowpuntorowr=$datosdet->num_rows();
        $limitefilas=32;
        if($rowpuntorowr<33){
            $limitefilas=32;
        }else if($rowpuntorowr>32 and $rowpuntorowr<58){
            $limitefilas=32;
        }
        for ($i = $rowpuntorowr; $i <= $limitefilas; $i++) {
            $htmlg.='<tr>
                <td width="10%">---</td>
                <td width="20%">---</td>
                <td width="15%">---</td>
                <td width="13%">---</td>
                <td width="32%">---</td>
                <td width="10%">---</td>
            </tr>';
        }

    $htmlg.='</tbody>        
    </table>
    <p></p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
/* ************************************************/

/* *******************PASO 3 DEL RECONOCIMIENTO ************* */
$htmlg='';
$pdf->AddPage('P', 'A4');

$datosdet2=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso3_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$htmlg='<style>
        .txt_td { background-color:rgb(217,217,217); }
        .ft7{ font-size:7px; text-align:center; }
        .algc{text-align:center; }
        .ft9{font-size:8.7px;}
        .pspaces{ font-size:0.2px; }
    </style>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <tr>
            <td width="24%" rowspan="2">
                <img src="'.base_url().'public/img/logo.jpg" >
            </td>
            <td width="19%">NOMBRE DEL DOCUMENTO</td>
            <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
            <td width="19%">VERSIÓN</td>
            <td width="19%">No COPIA CONTROLADA</td>
        </tr>
        <tr>
            <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
            <td>REG-TEC/02-01</td>
            <td>01</td>
            <td>ORIGINAL</td>
        </tr>
    </table>
    <p></p>
    <table border="1" cellpadding="5" align="center" class="ft9">
        <thead>
            <tr class="txt_td">
                <th width="5%" rowspan="2"><p></p>No.</th>
                <th width="19%" rowspan="2"><p></p>FUENTES GENERADORAS DE RUIDO</th>
                <th width="12%" colspan="2">NIVEL SONORO ENCONTRADO</th>
                <th width="7%" rowspan="2"><p></p>TIPO DE RUIDO <br>(E, I, IM)</th>
                <th width="10.5%" rowspan="2"><p></p>No. DE <br>TRABAJADORES <br>FIJOS</th>
                <th width="10.5%" rowspan="2"><p></p>No. DE <br>TRABAJADORES <br>MÓVILES</th>
                <th width="36%" colspan="4">NÚMERO DE PUNTOS A MUESTRA POR MÉTODO DE EVALUACIÓN</th>
            </tr>
            <tr class="txt_td">
                <th>Min. dB(A)</th>
                <th>Max. dB(A)</th>
                <th width="9%">GRADIENTE DE PRESIÓN SONORA</th>
                <th width="9%">PUESTO FIJO DE TRABAJO</th>
                <th width="10%">PRIORIDAD DE ÁREAS DE EVALUACIÓN</th>
                <th width="8%"><p class="pspaces"></p>PERSONAL</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($datosdet2->result() as $i) {
            $htmlg.='<tr>
                <td width="5%">'.$i->num.'</td>
                <td width="19%">'.$i->fuentes_genera.'</td>
                <td width="6%">'.$i->min_db.'</td>
                <td width="6%">'.$i->max_db.'</td>
                <td width="7%">'.$i->tipo_ruido.'</td>
                <td width="10.5%">'.$i->num_trabaja_fijo.'</td>
                <td width="10.5%">'.$i->num_trabaja_mov.'</td>
                <td width="9%">'.$i->gradiente.'</td>
                <td width="9%">'.$i->puesto_fijo.'</td>
                <td width="10%">'.$i->prioridad.'</td>
                <td width="8%">'.$i->personal.'</td>
            </tr>';
        }
        $rowpuntorowr=$datosdet2->num_rows();
        $limitefilas=32;
        if($rowpuntorowr<33){
            $limitefilas=32;
        }else if($rowpuntorowr>32 and $rowpuntorowr<58){
            $limitefilas=32;
        }
        for ($i = $rowpuntorowr; $i <= $limitefilas; $i++) {
            $htmlg.='<tr>
                <td width="5%">---</td>
                <td width="19%">---</td>
                <td width="6%">---</td>
                <td width="6%">---</td>
                <td width="7%">---</td>
                <td width="10.5%">---</td>
                <td width="10.5%">---</td>
                <td width="9%">---</td>
                <td width="9%">---</td>
                <td width="10%">---</td>
                <td width="8%">---</td>
            </tr>';
        }
    $htmlg.='</tbody>        
    </table>
    <p class="ft7">E:ESTABLE, I:INESTABLE, IM:IMPULSIVO</p>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
/* ********************************************************/

/**************************PASO 4 DE RECONOCIMIENTO *******************************/
$pdf->AddPage('P', 'A4');
if(isset($firmaemple) && $firmaemple!="" && $firmaemple!="data:"){
    $firm_txt = FCPATH."uploads/firmas/".$firmaemple;
    $handle = @fopen($firm_txt, 'r');
    if($handle){
        $fh = fopen(FCPATH."uploads/firmas/".$firmaemple, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh); 
    }else{
        $linea="#";
    }  
}else{
    $linea='#';
}
if(isset($firmaresp) && $firmaresp!="" && $firmaresp!="data:"){
    $firm_txt2 = FCPATH."uploads/firmas/".$firmaresp;
    $handle2 = @fopen($firm_txt2, 'r');
    if($handle2){
        $fh2 = fopen(FCPATH."uploads/firmas/".$firmaresp, 'r') or die("Se produjo un error al abrir el archivo");
        $firmaresp_line = fgets($fh2);
        fclose($fh2); 
    }else{
        $firmaresp_line="";
    }
}else{
    $firmaresp_line='';
}

$det4=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso4_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$det4=$det4->row();
log_message('error','DATA det4: '.json_encode($det4));
$htmlg='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:rgb(217,217,217);}
                .algc{text-align:center; }
                .ft9{font-size:9px;}
                .pspaces{font-size:1.2px;}
        </style>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>RECONOCIMIENTO INICIAL PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>

        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="35%" class="backg"><p></p>JUSTIFICACIÓN TÉCNICA PARA LA DETERMINACIÓN DE PUNTOS</td>
                <td width="65%" height="80px">'.$det4->justificacion.'</td>
            </tr>
        </table>

        <table><tr><td></td></tr></table>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="35%" class="backg">EQUIPO DE PROTECCIÓN PERSONAL UTILIZADO (MARCA, MODELO)</td>
                <td width="65%" height="30px">'.$det4->equipo_protec.'</td>
            </tr>
        </table>
        <table><tr><td></td></tr></table>

        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr><td class="backg">PLANO DE DISTRIBUCIÓN DE ÁREAS, MAQUINARIA, EQUIPO QUE GENEREN RUIDO Y UBICACIÓN DE LOS PUNTOS DE EVALUACIÓN</td></tr>
            <tr><td height="400px" style="font-size:12px"><br><br><br><br><br><br><br><br><br><br><br><br>
                '.$det4->plano_distri.'</td>
            </tr>
            <tr><td class="backg">OBSERVACIONES</td></tr>
            <tr><td height="50px">'.$det4->observaciones.'</td></tr>
            <tr>
                <td class="backg" width="50%"><p class="pspaces"></p>Nombre y firma del ingeniero de campo</td>
                <td width="50%"><!-- '.$tecnico.'<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55">--></td>
            </tr>
            <tr>
                <td class="backg" width="50%"><p class="pspaces"></p>Nombre y firma del responsable de la revisión del registro de campo</td>
                <td width="50%"><!-- Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55"> --></td>
            </tr>
        </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');

/* *********************************/
/************************** *******************************/
$pdf->AddPage('P', 'A4');

$det4=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso4_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
$det4=$det4->row();
$htmlg='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:rgb(217,217,217);}
                .algc{text-align:center; }
                .ft9{font-size:9px;}
                .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:11px;
                    font-size:9px;
                    valign: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:9px;
                }
                .pspaces{
                    font-size:0.5px;    
                }
        </style>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>HOJA DE CAMPO PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <p></p>
        <table class="tableb" cellpadding="5">';
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom11',array('idnom'=>$idnom,"activo"=>1));
            $id_calibrador=0;
            foreach ($resultnorma->result() as $item) {
                $id_calibrador=$item->id_calibrador;
                $htmlg.='<tr>
                            <th>NO. DE INFORME</th>
                            <td colspan="4">'.$item->num_informe.'</td>
                            <th colspan="2">FECHA DE EMISIÓN</th>
                            <td>'.$item->fecha.'</td>
                        </tr>
                        <tr>
                            <th>Razón social</th>
                            <td colspan="7">'.$item->razon_social.'</td>
                        </tr>';
            }
            $resultequipo=$this->ModeloCatalogos->getselectwheren('equipo',array('id'=>$id_calibrador));
            foreach ($resultequipo->result() as $iteme) {
                $htmlg.='<tr>
                <th>ID EQUIPO</th>
                <td>'.$iteme->luxometro.'</td>
                <th>MARCA</th>
                <td>'.$iteme->marca.'</td>
                <th>MODELO</th>
                <td>'.$iteme->modelo.'</td>
                <th>SERIE</th>
                <td>'.$iteme->equipo.'</td>
            </tr>';
            }
        $htmlg.='</table>';
        
        foreach ($resultnorma->result() as $item) {
        $htmlg.='<table class="tableb" cellpadding="5">
                    <tr>
                        <th width="15%">VERIFICACIÓN INICIAL</th>
                        <th width="15%">VERIFICACIÓN FINAL</th>
                        <th width="15%">CRITERIO DE ACEPTACIÓN</th>
                        <th width="25%">¿CUMPLE CRITERIO DE ACEPTACIÓN?</th>
                        <th width="30%" colspan="4"><p class="pspaces"></p>POTENCIA DE BATERIA</th>
                    </tr>
                    <tr>
                        <td>'.$item->veri_ini.'</td>
                        <td>'.$item->veri_fin.'</td>
                        <td>'.$item->criterio.'</td>
                        <td>'.$item->cumple_criterio.'</td>
                        <td class="backg">INICIAL</td>
                        <td>'.$item->poten_ini.' V</td>
                        <td class="backg">FINAL</td>
                        <td>'.$item->poten_fin.' V</td>
                    </tr>
                </table>
                <table class="tableb">
                    <tr>
                        <th><p class="pspaces"></p>Nombre y Firma del Ingeniero de campo</th>
                        <td><!-- '.$tecnico.' <br><img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55">--></td>
                        <th><p class="pspaces"></p>Nombre y Firma del responsable de la revisión de campo</th>
                        <td><!-- Ing. Christian Uriel Fabian Romero <br><img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55">--></td>
                    </tr>
                </table>
                <table class="tableb" cellpadding="5">
                    <tr>
                        <th>CONDICIONES DE OPERACIÓN AL MOMENTO DE LA EVALUACIÓN</th>
                        <td>'.$item->condiciones_opera.'</td>
                        
                    </tr>
                </table>';
        }
        $htmlg.='<table>
                    <tr>
                        <th></th>
                    </tr>
                </table>';
        $rowpuntostr=1; $cont_pts=0;
        $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
        foreach ($rowpuntos->result() as $item) { 
            $cont_pts++;
            $idpunto=$item->punto;
            $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto,"activo"=>1));
            foreach ($rownomd->result() as $itemnomd) {
                $horai1=$itemnomd->horai1; 
                $horaf1=$itemnomd->horaf1;
                $horai2=$itemnomd->horai2; 
                $horaf2=$itemnomd->horaf2;
                $horai3=$itemnomd->horai3; 
                $horaf3=$itemnomd->horaf3; 
                if($itemnomd->tipo_ruido==0){
                    $tipo_ruido="Estable";
                }elseif ($itemnomd->tipo_ruido==1) {
                    $tipo_ruido="Inestable";
                }elseif ($itemnomd->tipo_ruido==2) {
                    $tipo_ruido="Impulsivo";
                }else{
                    $tipo_ruido="";
                }
                $htmlg.='<table class="tableb" cellpadding="5">
                            <tr>
                                <th width="10%">No</th>
                                <td width="20%">'.$itemnomd->punto.'</td>
                                <th width="10%">ÁREA</th>
                                <td width="40%">'.$itemnomd->area.'</td>
                                <th width="20%">TIPO DE RUIDO</th>
                            </tr>
                            <tr>
                                <th colspan="2">IDENTIFICACIÓN DEL PUNTO</th>
                                <td colspan="2">'.$itemnomd->identificacion.'</td>
                                <td>'.$tipo_ruido.'</td>
                            </tr>
                            <tr>
                                <th rowspan="2" width="20%"><p></p><BR>OBSERVACIONES</th>
                                <td rowspan="2" width="60%">'.$itemnomd->observaciones.'</td>
                                <th width="20%">TIEMPO REAL DE EXPOSICIÓN AL RUIDO(h)</th>
                            </tr>
                            <tr><td>'.$itemnomd->tiempo_ruido.'</td></tr>
                            <tr><th width="100%" >NSCE(A,T)dB(A)</th></tr>
                        </table>';
            }
            $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
            $numlectura=1;
            $lecturasnumeracion='';
            $tdperiodo1='';
            $tdperiodo2='';
            $tdperiodo3='';        
            foreach ($rowlecturas->result() as $itempp) {
                $lecturasnumeracion.='<th>'.$numlectura.'</th>';
                if($itempp->periodo1>0){
                    $periodo1=$itempp->periodo1;
                }else{
                    $periodo1="---";
                }
                if($itempp->periodo2>0){
                    $periodo2=$itempp->periodo2;
                }else{
                    $periodo2="---";
                }
                if($itempp->periodo3>0){
                    $periodo3=$itempp->periodo3;
                }else{
                    $periodo3="---";
                }
                $tdperiodo1.='<td rowspan="2"><p class="pspaces"></p>'.$periodo1.'</td>';
                $tdperiodo2.='<td rowspan="2"><p class="pspaces"></p>'.$periodo2.'</td>';
                $tdperiodo3.='<td rowspan="2"><p class="pspaces"></p>'.$periodo3.'</td>';
                $numlectura++;
            }
            $htmlg.='<table class="tableb" cellpadding="5">';
            $htmlg.='<tr>';
            $htmlg.='<th colspan="2">1er Periodo</th>';
            $htmlg.=$lecturasnumeracion;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Inicial</th><td>'.date("H:i", strtotime($horai1)).'</td>';
            $htmlg.=$tdperiodo1;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Final</th><td>'.date("H:i", strtotime($horaf1)).'</td>';
            $htmlg.='</tr>';

            $htmlg.='<tr>';
            $htmlg.='<th colspan="2">2do Periodo</th>';
            $htmlg.=$lecturasnumeracion;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Inicial</th><td>'.date("H:i", strtotime($horai2)).'</td>';
            $htmlg.=$tdperiodo2;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Final</th><td>'.date("H:i", strtotime($horaf2)).'</td>';
            $htmlg.='</tr>';

            $htmlg.='<tr>';
            $htmlg.='<th colspan="2">3er Periodo</th>';
            $htmlg.=$lecturasnumeracion;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Inicial</th><td>'.date("H:i", strtotime($horai3)).'</td>';
            $htmlg.=$tdperiodo3;
            $htmlg.='</tr>';
            $htmlg.='<tr>';
            $htmlg.='<th>Hora Final</th><td>'.date("H:i", strtotime($horaf3)).'</td>';
            $htmlg.='</tr>';

            $htmlg.='</table>';

            $htmlg.='<table class="tableb" cellpadding="5">
                        <tr>
                            <td rowspan="3"></td>
                            <th colspan="13">NIVEL DE PRESIÓN ACÚSTICA (NPA)</th>
                        </tr>
                        <tr>
                            <th rowspan="2">Hora Inicial</th>
                            <th rowspan="2">Hora Final</th>
                            <th colspan="2">Ponderación</th>
                            <th colspan="9">Frecuencias Centrales (Hz)</th>
                        </tr>
                        <tr><th>dB(A)</th><th>Lineal</th><th>31.5</th><th>63</th><th>125</th><th>250</th><th>500</th><th>1000</th><th>2000</th><th>4000</th><th>8000</th></tr>';
                        $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                        $rownomdreaperiodo=1;
                        foreach ($rownomdrea->result() as $itrea) {
                            if($itrea->rea_db>0){
                            $htmlg.='<tr>
                                <th>Periodo '.$rownomdreaperiodo.'</th><td>'.date("H:i", strtotime($itrea->horai)).'</td><td>'.date("H:i", strtotime($itrea->horaf)).'</td><td>'.$itrea->rea_db.'</td><td>'.$itrea->rea_lineal.'</td><td>'.$itrea->rea_31_5.'</td><td>'.$itrea->rea_63.'</td><td>'.$itrea->rea_125.'</td><td>'.$itrea->rea_250.'</td><td>'.$itrea->rea_500.'</td><td>'.$itrea->rea_1000.'</td><td>'.$itrea->rea_2000.'</td><td>'.$itrea->rea_4000.'</td><td>'.$itrea->rea_8000.'</td></tr>';
                            $rownomdreaperiodo++;
                            }
                        }
            $htmlg.='</table><p class="pspaces"></p>';
            if($cont_pts<=$rowpuntos->num_rows()){
                $pdf->writeHTML($htmlg, true, false, true, false, '');
                $htmlg='<style type="text/css">
                        .borderbottom{border-bottom:1px solid black;}
                        .backg{background-color:rgb(217,217,217);}
                        .algc{text-align:center; }
                        .ft9{font-size:9px;}
                        .tableb td{
                            border:1px solid #808080;
                            font-family:Arial;text-align: center;font-size:11px;
                            font-size:9px;
                            valign: middle;
                        }
                        .tableb th{
                            background-color:rgb(217,217,217);
                            border:1px solid #808080;  
                            text-align:center; 
                            font-size:9px;
                        }
                        .pspaces{
                            font-size:0.5px;    
                        }
                </style>
                <table border="1" cellpadding="5" align="center" class="ft9">
                    <tr>
                        <td width="24%" rowspan="2">
                            <img src="'.base_url().'public/img/logo.jpg" >
                        </td>
                        <td width="19%">NOMBRE DEL DOCUMENTO</td>
                        <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                        <td width="19%">VERSIÓN</td>
                        <td width="19%">No COPIA CONTROLADA</td>
                    </tr>
                    <tr>
                        <td>HOJA DE CAMPO PARA RUIDO LABORAL</td>
                        <td>REG-TEC/02-01</td>
                        <td>01</td>
                        <td>ORIGINAL</td>
                    </tr>
                </table>
                <p></p>';
            }
            if($cont_pts<$rowpuntos->num_rows()){
                $pdf->AddPage('P', 'A4');
            }
        }
        //log_message('error',$htmlg);
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
//============================================

$resultequipospti=$this->ModeloCatalogos->nom11personalequiposti($idnom);
if($resultequipospti->num_rows()>0){ //solo se muestra si existen lecturas en personal
    $pdf->AddPage('P', 'A4');
    //$det4=$this->ModeloCatalogos->getselectwheren('reconocimiento_paso4_nom11',array('id_reconocimiento'=>$GLOBALS['id_rec'],'estatus'=>1));
    //$det4=$det4->row();
    $htmlg='<style type="text/css">
                .borderbottom{border-bottom:1px solid black;}
                .backg{background-color:rgb(217,217,217);}
                .algc{text-align:center; }
                .ft9{font-size:9px;}
                .checked{font-size:11px;font-family:dejavusans;}
                .tableb td{
                    border:1px solid #808080;
                    font-family:Arial;text-align: center;font-size:9px;
                    font-size:9px;
                    valign: middle;
                    vertical-align: middle;
                }
                .tableb th{
                    background-color:rgb(217,217,217);
                    border:1px solid #808080;  
                    text-align:center; 
                    font-size:9px;
                }
                .pspaces{
                    font-size:0.5px;    
                }
        </style>
        <table border="1" cellpadding="5" align="center" class="ft9">
            <tr>
                <td width="24%" rowspan="2">
                    <img src="'.base_url().'public/img/logo.jpg" >
                </td>
                <td width="19%">NOMBRE DEL DOCUMENTO</td>
                <td width="19%">IDENTIFICACIÓN DEL DOCUMENTO</td>
                <td width="19%">VERSIÓN</td>
                <td width="19%">No COPIA CONTROLADA</td>
            </tr>
            <tr>
                <td>HOJA DE CAMPO PARA RUIDO LABORAL</td>
                <td>REG-TEC/02-01</td>
                <td>01</td>
                <td>ORIGINAL</td>
            </tr>
        </table>
        <table><tr><td></td></tr></table>
        <table class="tableb" cellpadding="5">';
            $resultnorma=$this->ModeloCatalogos->getselectwheren('nom11',array('idnom'=>$idnom,"activo"=>1));
            $id_calibrador=0;
            foreach ($resultnorma->result() as $item) {
                $htmlg.='<tr>
                            <th>NO. DE INFORME</th>
                            <td colspan="5">'.$item->num_informe.'</td>
                            <th>FECHA</th>
                            <td>'.$item->fecha.'</td>
                        </tr>
                        <tr>
                            <th>Razón social</th>
                            <td colspan="7">'.$item->razon_social.'</td>
                        </tr>
                        <tr>
                            <th colspan="3"><p class="pspaces"></p>NOMBRE Y FIRMA DEL INGENIERO DE CAMPO</th>
                            <td colspan="5"><!--'.$tecnico.' <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $linea) . '" width="55"> --></td>
                        </tr>
                        <tr>
                            <th colspan="3"><p class="pspaces"></p>NOMBRE Y FIRMA DEL RESPONSABLE DE LA REVISIÓN DEL REGISTRO DE CAMPO</th>
                            <td colspan="5"><!--Ing. Christian Uriel Fabian Romero <img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $firmaresp_line) . '" width="55">--></td>
                        </tr>';
            }
            
            $htmlg.='</table><table><tr><td></td></tr></table>';
            $htmlg.='<table class="tableb" cellpadding="5">
                    <thead>
                    <tr>
                        <th colspan="5"><p class="pspaces"></p>EQUIPO UTILIZADO</th>
                        <th colspan="2">VERIFICACIÓN DE CALIBRACIÓN CRITERIO DE ACEPTACIÓN s1 dB(A)</th>
                        <th colspan="2">¿SE CUMPLE CRITERIO DE ACEPTACIÓN?</th>
                    </tr>
                    <tr>
                        <th>MARCA</th>
                        <th>MODELO</th>
                        <th>No. SERIE</th>
                        <th>ID</th>
                        <th>SELECCION</th>
                        <th>CALIBRACIÓN INICIAL</th>
                        <th>CALIBRACIÓN FINAL</th>
                        <th>SI</th>
                        <th>NO</th>
                    </tr>
                    </thead>';
            $resultequiposp=$this->ModeloCatalogos->nom11personalequipos($idnom);
                foreach ($resultequiposp->result() as $itemeqp) {
                    $htmlg.='<tr>
                            <td>'.$itemeqp->marca.'</td>
                            <td>'.$itemeqp->modelo.'</td>
                            <td>'.$itemeqp->equipo.'</td>
                            <td>'.$itemeqp->luxometro.'</td>
                            <td><p class="checked">✔</p></td>
                            <td>114.0</td>
                            <td>114.0</td>
                            <td><p class="checked">✔</p></td>
                            <td>---</td>
                        </tr>';
                }
            $htmlg.='</table><table><tr><td></td></tr></table>';
            $htmlg.='<table class="tableb" cellpadding="5">
                    <thead>
                    <tr>
                        <th width="5%"><p class="pspaces"></p>No.</th>
                        <th width="12%"><p class="pspaces"></p>ID DOSÍMETRO</th>
                        <th width="17%"><p class="pspaces"></p>ÁREA</th>
                        <th width="22%"><p class="pspaces"></p>NOMBRE DEL TRABAJADOR / PUESTO DE TRABAJO</th>
                        <th width="11%">TOTAL DE TRABAJADORES EXPUESTOS</th>
                        <th width="8%"><p class="pspaces"></p>HORA INICIAL</th>
                        <th width="8%"><p class="pspaces"></p>HORA FINAL</th>
                        <th width="8%">TIEMPO TOTAL DE MEDICIÓN (h)</th>
                        <th width="9%"><p class="pspaces"></p>% DOSIS</th>
                    </tr>
                    </thead>';
                
                foreach ($resultequipospti->result() as $itemeqt) {
                    $difference_itemeqt = date_diff(date_create($itemeqt->ti), date_create($itemeqt->tf));
                    if($itemeqt->ti=="")
                        $ti="---";
                    else 
                        $ti=$itemeqt->ti;
                    if($itemeqt->tf=="")
                        $tf="---";
                    else 
                        $tf=$itemeqt->tf;

                    if($resultequipospti->num_rows()<13){
                        $htmlg.='<tr>
                            <td width="5%">'.$itemeqt->num.'</td>
                            <td width="12%">'.$itemeqt->luxometro.'</td>
                            <td width="17%">'.$itemeqt->area.'</td>
                            <td width="22%">'.$itemeqt->trabajador.'<br>'.$itemeqt->puesto.'</td>
                            <td width="11%">1</td>
                            <td width="8%">'.$ti.'</td>
                            <td width="8%">'.$tf.'</td>
                            <td width="8%">'.$difference_itemeqt->h.'</td>
                            <td width="9%">'.$itemeqt->dosis.'</td>
                        </tr>';
                    }if($resultequipospti->num_rows()==13){
                        //$pdf->writeHTML($htmlg, true, false, true, false, '');
                        $pdf->AddPage('P', 'A4');
                    }
                    if($resultequipospti->num_rows()>=13){
                        $htmlg.='<tr>
                            <td width="5%">'.$itemeqt->num.'</td>
                            <td width="12%">'.$itemeqt->luxometro.'</td>
                            <td width="17%">'.$itemeqt->area.'</td>
                            <td width="22%">'.$itemeqt->trabajador.'<br>'.$itemeqt->puesto.'</td>
                            <td width="11%">1</td>
                            <td width="8%">'.$ti.'</td>
                            <td width="8%">'.$tf.'</td>
                            <td width="8%">'.$difference_itemeqt->h.'</td>
                            <td width="9%">'.$itemeqt->dosis.'</td>
                        </tr>';
                    }
                }
        $htmlg.='</table><table><tr><td></td></tr></table>
            <table class="tableb" cellpadding="5">
                <tr>
                    <th width="15%">OBSERVACIONES</th>
                    <td width="85%">---</td>
                </tr>
            </table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

/* *********************************/
$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->writeHTML($htmlg, true, false, true, false, ''); 


$pdf->setPrintHeader(true);
$pdf->SetMargins(15, 27,15);
$pdf->Bookmark('II. Planos ----------------------------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0)); /////desde acá
$htmlg='
        <table>
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO II</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PLANOS</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$cont_plan=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==1){
        $pdf->AddPage('P', 'A4');
        $cont_plan++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de distribución de las áreas en que exista el ruido</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'" ></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td align="center"><i>Plano de distribución de las áreas en que exista el ruido</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        //$htmlg='<p style="text-align:cente;"><i>Plano de distribución de las áreas en que exista el ruido</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 150, '', '', 'M', true, 150, 'C', false, false, 1, false, false, false);
    }
}
if($cont_plan==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table ><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de distribución de las áreas en que exista el ruido</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan2=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==2){
        $pdf->AddPage('P', 'A4');
        $cont_plan2++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);

        //$htmlg='<p style="text-align:cente;"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);
    }
}
if($cont_plan2==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de ubicación de la maquinaria y equipos generadores de ruido</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}
$htmlg=''; $cont_plan3=0;
foreach ($datosnomplanos->result() as $item) {
    if($item->tipo==3){
        $pdf->AddPage('P', 'A4');
        $cont_plan3++;
        
        /*$htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr><tr><td align="center"><img width="800px" height="830px" src="'.base_url().'uploads/imgPlanos/'.$item->url_img.'"></td></tr></table>';*/
        $htmlg='<table width="100%" align="center"><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        $pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        //$htmlg='<p style="text-align:cente;"><i>Plano de identificación de los puntos de medición</i></p>';
        //$pdf->writeHTML($htmlg, true, false, true, false, '');
        //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
        //$pdf->Image(base_url().'uploads/imgPlanos/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

    }
}
if($cont_plan3==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Plano de identificación de los puntos de medición</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$htmlg.='';
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('III. Secuencia de cálculo --------------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO III</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">SECUENCIA DE CÁLCULO </td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$htmlg=''; $equipo=0;
$htmlg.='<style type="text/css">
    .titulos{background-color: rgb(217,217,217);}
    .table-bordered th, .table-bordered td {border: 1px solid #c6c9cb;}
    .table thead th {border-bottom: 2px solid #c6c9cb;}
    .table td{padding: 0.5rem;font-size: 10px;vertical-align: middle;text-align: center;}
    .table th{padding: 0.5rem;font-size: 10px;vertical-align: middle;text-align: center;}

    .title_tb{ color: #548235; font-weight: bold;}
    .pspaces{
        font-size:0.2px;    
    }
</style>';
foreach ($datosnom->result() as $item) { 
    $htmlg.='
        <table class="table table-bordered" border="1">
        <tr>
            <td colspan="2"></td>
            <td class="titulos">No. de Informe</td><td class="title_tb">'.$folio.'</td>
        </tr>
        <tr>
            <td class="titulos">Razón Social</td><td colspan="3" class="title_tb">'.$item->razon_social.'</td>
        </tr>
        <tr>
            <td class="titulos">Fecha</td><td class="title_tb">'.$item->reg.'</td>
            <td class="titulos">Id. del equipo</td><td class="title_tb">'.$equipo.'</td>
        </tr>
        <tr><td colspan="4"></td></tr>
    </table>';
}
//====================================================================================================== todo esto tiene que ser lo mismo al archivo cnom011view.php si se modifica uno de los dos el otro tendra lo mismo
    $rowpuntostr=1;
    $rowpuntos=$this->ModeloCatalogos->getselectwheren('nom11d_puntos',array('idnom'=>$idnom,'activo'=>1));
    foreach ($rowpuntos->result() as $item) { 
        $idpunto=$item->punto;
        
        $val_nrr=0;
        $rownomd=$this->ModeloCatalogos->getselectwheren('nom11d',array('idnom'=>$idnom,'punto'=>$idpunto));
        $itemnomd=$rownomd->row();
        $val_nrr=$itemnomd->val_nrr;
        if($itemnomd->te>0){ $htmlgti= $itemnomd->te; }else{ $htmlgti='480';}
        $tiporuido=$itemnomd->tiempo_ruido*60;
        $htmlg.='<table class="table table-bordered ">
            <tr>
                <th class="titulos">No. Punto</th>
                <th>'.$rowpuntostr.'</th>
                <th class="titulos" colspan="3">Tiempo  efectivo de exposición al ruido (h)</th>
                <th colspan="2">'.$itemnomd->tiempo_ruido.'</th>
            </tr>
            <tr>
                <th colspan="2"></th>
                <th class="titulos">Ti (minutos)</th>
                <th>'.$tiporuido.'</th>
                <th class="titulos" colspan="2">Te para 8 h en minutos </th>
                <th>'.$htmlgti.'</th>
            </tr>
            <tr>
                <th class="titulos">Área </th>
                <th colspan="2">'.$itemnomd->area.'</th>
                <th class="titulos">Identificacion</th>
                <th colspan="3">'.$itemnomd->identificacion.'</th>
            </tr>
        </table>
        <table>
            <tr><td></td></tr>
            <tr>
                <td width="18%">
                    <table class="table table-bordered ">
                        <tr>
                            <th class="titulos" colspan="2">Ruido</th>
                        </tr>
                        <tr>
                            <th class="titulos">Estable</th>
                            <th>';
                            if($itemnomd->tipo_ruido==0){$htmlg.='X';}
                        $htmlg.='</th>
                        </tr>
                        <tr>
                            <th class="titulos">Inestable</th>
                            <th>';
                            if($itemnomd->tipo_ruido==1){$htmlg.='X';}
                        $htmlg.='</th>
                        </tr>
                        <tr>
                            <th class="titulos">Impulsivo</th>
                            <th>';
                            if($itemnomd->tipo_ruido==2){$htmlg.='X';}$htmlg.='</th>
                        </tr>
                    </table>
                </td>
                <td width="82%">
                    <table class="table table-bordered tablecaptura" align="center">
                        <thead>
                            <tr>
                                <th class="titulos" rowspan="2"><p class="pspaces"></p>Lectura
                                </th>
                                <th class="titulos" colspan="2">Periodo 1</th>
                                <th class="titulos" colspan="2">Periodo 2</th>
                                <th class="titulos" colspan="2">Periodo 3</th>
                            </tr>
                            <tr>
                                <th class="titulos">NSCE<br>dB(A)</th>
                                <th class="titulos"><img width="55px" src="'.base_url().'public/img/formula3_2.png"></th>
                                <th class="titulos">NSCE<br>dB(A)</th>
                                <th class="titulos"><img width="55px" src="'.base_url().'public/img/formula3_2.png"></th>
                                <th class="titulos">NSCE<br>dB(A)</th>
                                <th class="titulos"><img width="55px" src="'.base_url().'public/img/formula3_2.png"></th>
                            </tr>
                        </thead>
                        <tbody>';
                            $rowlecturas=$this->ModeloCatalogos->getselectwheren('nom11d_lecturas',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1)); 
                                    $rowlecturas_tr=1;
                                    $sumaperiodos_123=0;
                                    foreach ($rowlecturas->result() as $iteml) {
                                    
                                        $htmlg.='<tr>
                                                <th class="titulos">'.$rowlecturas_tr.'</th>
                                                <th>';
                                                if($iteml->periodo1>0){$htmlg.=$iteml->periodo1;}else{$htmlg.='---';}
                                                $htmlg.='</th>
                                                <th>';
                                                if($iteml->periodo1>0){
                                                            $periodo1pow=pow(10, ($iteml->periodo1/10));
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                    $htmlg.=$periodo1pow;
                                                }else{
                                                            $periodo1pow=0;
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo1pow;
                                                    $htmlg.='---';
                                                        };
                                                $htmlg.='</th>
                                                <th>';
                                                    if($iteml->periodo2>0){$htmlg.=$iteml->periodo2;}else{$htmlg.='---';}$htmlg.='
                                                </th>
                                                <th>';
                                                    if($iteml->periodo2>0){
                                                            $periodo2pow=pow(10, ($iteml->periodo2/10));
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                        $htmlg.=$periodo2pow;
                                                    }else{
                                                            $periodo2pow=0;
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo2pow;
                                                        $htmlg.='---';
                                                    }
                                                $htmlg.='</th>
                                                <th>';
                                                    if($iteml->periodo3>0){$htmlg.=$iteml->periodo3;}else{$htmlg.='---';}
                                                $htmlg.='</th>
                                                <th>';
                                                    if($iteml->periodo3>0){
                                                            $periodo3pow=pow(10, ($iteml->periodo3/10));
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                        $htmlg.=$periodo3pow;
                                                    }else{
                                                            $periodo3pow=0;
                                                            $sumaperiodos_123=$sumaperiodos_123+$periodo3pow;
                                                        $htmlg.='---';
                                                    }
                                                $htmlg.='</th>
                                              </tr>';
                                        $rowlecturas_tr++;
                                    }
                                $htmlg.='<tr>
                                    <th class="titulos">Suma</th>
                                    <th colspan="6">'.$sumaperiodos_123.'</th>
                                </tr>
                        </tbody> 
                    </table>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <th colspan="3">NIVEL SONORO CONTINUO EQUIVALENTE</th>
            </tr>
            <tr>
                <td></td>
                <td><img src="'.base_url().'public/img/formula4.png"></td>
                <td>';
                        if($itemnomd->tipo_ruido==0){
                            $num=(1/20)*$sumaperiodos_123;
                            $nivel_sonoro_continuo=10*log10((1/20)*$sumaperiodos_123);
                        }elseif($itemnomd->tipo_ruido==1){
                            $nivel_sonoro_continuo=10*log10((1/30)*$sumaperiodos_123);
                        }else{
                            $nivel_sonoro_continuo=10*log10((1/45)*$sumaperiodos_123);
                        }
                    $htmlg.='NSCEA,Ti dB(A)= <b>'.round($nivel_sonoro_continuo, 2).'</b></td>
            </tr>
            <tr>
                <th colspan="3">NIVEL DE EXPOSICIÓN AL RUIDO</th>
            </tr>
            <tr>
                <td></td>
                <td><img src="'.base_url().'public/img/formula5.png"></td>
                <td>';
                    if($nivel_sonoro_continuo>0 && $itemnomd->tiempo_ruido>0){
                        $nivel_expocion_ruido=10*log10(($itemnomd->tiempo_ruido*60)*pow(10, ($nivel_sonoro_continuo/10)) )-10*log10($itemnomd->te);
                    }else{
                        $nivel_expocion_ruido=0;
                    }
                    //$nivel_expocion_ruido=10*log10(($itemnomd->tiempo_ruido*60)*pow(10, ($nivel_sonoro_continuo/10)) )-10*log10($itemnomd->te);
                    $htmlg.='NER dB(A)= <b>'.round($nivel_expocion_ruido,2).'</b></td>
            </tr>
            <tr>
                <td>TIEMPO MÁXIMO PERMISIBLE DE EXPOSICIÓN</td>
                <td><img src="'.base_url().'public/img/formula6.png"></td>
                <td>';
                        if($nivel_expocion_ruido<90){
                            $t_max_permisible_exp=0;
                            $t_max_permisible_exp_l='No Aplica';
                        }else{
                            $t_max_permisible_exp=8/(pow(2,(($nivel_expocion_ruido-90)/3)));
                            $t_max_permisible_exp_l=round($t_max_permisible_exp,2);
                        }
                    $htmlg.='TMPE dB(A)= <b>'.$t_max_permisible_exp_l.'</b></td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <td width="20%"></td>
                <td width="60%"><img src="'.$item->grafo1.'" ></td>
                <td width="20%"></td></tr>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;"><b>REGISTRO DEL ESPECTRO ACÚSTICO</b></div>
        </div>
        <table class="table table-bordered tablecaptura rea tablev">
            <thead>
                <tr>
                    <th class="titulos" rowspan="2">Periodo</th>
                    <th class="titulos" colspan="2">Ponderación</th>
                    <th class="titulos" colspan="9">Frecuencias Centrales (Hz)</th>
                </tr>
                <tr>
                    <th class="titulos">dB(A)</th>
                    <th class="titulos">Lineal</th>
                    <th class="titulos">31.5</th>
                    <th class="titulos">63</th>
                    <th class="titulos">125</th>
                    <th class="titulos">250</th>
                    <th class="titulos">500</th>
                    <th class="titulos">1000</th>
                    <th class="titulos">2000</th>
                    <th class="titulos">4000</th>
                    <th class="titulos">8000</th>
                </tr>
            </thead>
            <tbody>';
                 
                $rownomdrea=$this->ModeloCatalogos->getselectwheren('nom11d_reg_espec_acustico',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
                $rownomdreaperiodo=1;
                $rownomdrea_num=$rownomdrea->num_rows();

                $rea_db_t=0;$rea_lineal_t=0;$rea_31_5_t=0;$rea_63_t=0;$rea_125_t=0;$rea_250_t=0;$rea_500_t=0;$rea_1000_t=0;$rea_2000_t=0;$rea_4000_t=0;$rea_8000_t=0;

                foreach ($rownomdrea->result() as $itrea) { 
                    if($rownomdrea_num==1){
                        $rea_db_t=$itrea->rea_db;
                        $rea_lineal_t=$itrea->rea_lineal;
                        $rea_31_5_t=$itrea->rea_31_5;
                        $rea_63_t=$itrea->rea_63;
                        $rea_125_t=$itrea->rea_125;
                        $rea_250_t=$itrea->rea_250;
                        $rea_500_t=$itrea->rea_500;
                        $rea_1000_t=$itrea->rea_1000;
                        $rea_2000_t=$itrea->rea_2000;
                        $rea_4000_t=$itrea->rea_4000;
                        $rea_8000_t=$itrea->rea_8000;
                    }else{
                        $rea_db_t=$rea_db_t+(pow(10,($itrea->rea_db/10)));
                        $rea_lineal_t=$rea_lineal_t+(pow(10,($itrea->rea_lineal/10)));
                        $rea_31_5_t=$rea_31_5_t+(pow(10,($itrea->rea_31_5/10)));
                        $rea_63_t=$rea_63_t+(pow(10,($itrea->rea_63/10)));
                        $rea_125_t=$rea_125_t+(pow(10,($itrea->rea_125/10)));
                        $rea_250_t=$rea_250_t+(pow(10,($itrea->rea_250/10)));
                        $rea_500_t=$rea_500_t+(pow(10,($itrea->rea_500/10)));
                        $rea_1000_t=$rea_1000_t+(pow(10,($itrea->rea_1000/10)));
                        $rea_2000_t=$rea_2000_t+(pow(10,($itrea->rea_2000/10)));
                        $rea_4000_t=$rea_4000_t+(pow(10,($itrea->rea_4000/10)));
                        $rea_8000_t=$rea_8000_t+(pow(10,($itrea->rea_8000/10)));
                    }
                    
                    $htmlg.='<tr >
                        <th class="titulos">'.$rownomdreaperiodo.'</th>
                        <td>'.$itrea->rea_db.'</td>
                        <td>'.$itrea->rea_lineal.'</td>
                        <td>'.$itrea->rea_31_5.'</td>
                        <td>'.$itrea->rea_63.'</td>
                        <td>'.$itrea->rea_125.'</td>
                        <td>'.$itrea->rea_250.'</td>
                        <td>'.$itrea->rea_500.'</td>
                        <td>'.$itrea->rea_1000.'</td>
                        <td>'.$itrea->rea_2000.'</td>
                        <td>'.$itrea->rea_4000.'</td>
                        <td>'.$itrea->rea_8000.'</td>
                    </tr>';
                    $rownomdreaperiodo++;
                }
                $htmlg.='<tr>
                    <th class="titulos">NPA(dB)</th>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_db_t0=$rea_db_t;    
                            }else{
                                $rea_db_t0=10*log10((1/2)*$rea_db_t);
                            }
                            $htmlg.=$rea_db_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_lineal_t0=$rea_lineal_t;    
                            }else{
                                $rea_lineal_t0=10*log10((1/2)*$rea_lineal_t);
                            }
                            $htmlg.=$rea_lineal_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_31_5_t0=$rea_31_5_t;    
                            }else{
                                $rea_31_5_t0=10*log10((1/2)*$rea_31_5_t);
                            }
                            $htmlg.=$rea_31_5_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_63_t0=$rea_63_t;    
                            }else{
                                $rea_63_t0=10*log10((1/2)*$rea_63_t);
                            }
                            $htmlg.=$rea_63_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_125_t0=$rea_125_t;    
                            }else{
                                $rea_125_t0=10*log10((1/2)*$rea_125_t);
                            }
                            $htmlg.=$rea_125_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_250_t0=$rea_250_t;    
                            }else{
                                $rea_250_t0=10*log10((1/2)*$rea_250_t);
                            }
                            $htmlg.=$rea_250_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_500_t0=$rea_500_t;    
                            }else{
                                $rea_500_t0=10*log10((1/2)*$rea_500_t);
                            }
                            $htmlg.=$rea_500_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_1000_t0=$rea_1000_t;    
                            }else{
                                $rea_1000_t0=10*log10((1/2)*$rea_1000_t);
                            }
                            $htmlg.=$rea_1000_t0;
                    $htmlg.='</td>
                    <td>';
                            if($rownomdrea_num==1){
                                $rea_2000_t0=$rea_2000_t;    
                            }else{
                                $rea_2000_t0=10*log10((1/2)*$rea_2000_t);
                            }
                            $htmlg.=$rea_2000_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_4000_t0=$rea_4000_t;    
                            }else{
                                $rea_4000_t0=10*log10((1/2)*$rea_4000_t);
                            }
                            $htmlg.=$rea_4000_t0;
                    $htmlg.='</td>
                    <td>'; 
                            if($rownomdrea_num==1){
                                $rea_8000_t0=$rea_8000_t;    
                            }else{
                                $rea_8000_t0=10*log10((1/2)*$rea_8000_t);
                            }
                                $htmlg.=$rea_8000_t0;
                    $htmlg.='</td>
                </tr>
            </tbody>
        </table>';
        $rownomdfaep=$this->ModeloCatalogos->getselectwheren('nom11d_faepa',array('idnom'=>$idnom,'punto'=>$idpunto,'activo'=>1));
        $itemfp=$rownomdfaep->row();
        $aqf_q1_q7_suma=0;
        $aqf_q1_r1=0;
        $aqf_q1_r2=0;
        $aqf_q2_r1=0;
        $aqf_q2_r2=0;
        $aqf_q3_r1=0;
        $aqf_q3_r2=0;
        $aqf_q4_r1=0;
        $aqf_q4_r2=0;
        $aqf_q5_r1=0;
        $aqf_q5_r2=0;
        $aqf_q6_r1=0;
        $aqf_q6_r2=0;
        $aqf_q7_r1=0;
        $aqf_q7_r2=0;
        if($itemfp->faepa_125!=0){
            $htmlg.='<div class="row">
                <div class="col-md-12" style="text-align:center;"><b>FACTOR DE ATENUACIÓN DEL EQUIPO DE PROTECCIÓN AUDITIVA</b></div>
            </div>
            <table class="table table-bordered tablecaptura faepa tablev" align="center" width="100%">
                <tr>
                    <th class="titulos">Frecuencia (Hz)</th>
                    <th class="titulos" width="9%">125</th>
                    <th class="titulos" width="9%">250</th>
                    <th class="titulos" width="9%">500</th>
                    <th class="titulos" width="9%">1000</th>
                    <th class="titulos" width="9%">2000</th>
                    <th class="titulos" width="9%">3000</th>
                    <th class="titulos" width="9%">4000</th>
                    <th class="titulos" width="9%">6000</th>
                    <th class="titulos" width="9%">8000</th>
                </tr>';          
                    //foreach ($rownomdfaep->result() as $itemfp) {          
                    $htmlg.='<tr>
                            <td class="titulos">Atenuación(dB)</td>
                            <td>'.$itemfp->faepa_125.'</td>
                            <td>'.$itemfp->faepa_250.'</td>
                            <td>'.$itemfp->faepa_500.'</td>
                            <td>'.$itemfp->faepa_1000.'</td>
                            <td>'.$itemfp->faepa_2000.'</td>
                            <td>'.$itemfp->faepa_3000.'</td>
                            <td>'.$itemfp->faepa_4000.'</td>
                            <td>'.$itemfp->faepa_6000.'</td>
                            <td>'.$itemfp->faepa_8000.'</td>
                        </tr>';
                
                    //}
            
                $htmlg.='</table>';
            
            //----------------------------------------------
                $rrpa_125=16.2;
                $rrpa_250=8.7;
                $rrpa_500=3.3;
                $rrpa_1000=0;
                $rrpa_2000=-1.2;
                $rrpa_4000=-1;
                $rrpa_8000=1.1;
                $itrea2=$rownomdrea->row(0);
            //--------------------------------------------
    
            $htmlg.='
                <div class="row">
                    <div class="col-md-12" style="text-align:center;"><b>REDUCCIÓN DE RUIDO PARA LA PROTECCIÓN AUDITIVA</b></div>
                    <div class="col-md-12">Correcciones para Q</div>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="titulos">125 Hz</th>
                            <th class="titulos">250 Hz</th>
                            <th class="titulos">500 Hz</th>
                            <th class="titulos">1000 Hz</th>
                            <th class="titulos">2000 Hz</th>
                            <th class="titulos">4000 Hz</th>
                            <th class="titulos">8000 Hz</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>'.$rrpa_125.'</td>
                            <td>'.$rrpa_250.'</td>
                            <td>'.$rrpa_500.'</td>
                            <td>'.$rrpa_1000.'</td>
                            <td>'.$rrpa_2000.'</td>
                            <td>'.$rrpa_4000.'</td>
                            <td>'.$rrpa_8000.'</td>
                        </tr>
                    </tbody>
                </table>';
    
            if($itemfp->faepa_125==0){
                $aqf_q1=0;  
            }else{
                $aqf_q1=$itemfp->faepa_125+$rrpa_125;
            }
            if($itemfp->faepa_250==0){
                $aqf_q2=0;  
            }else{
                $aqf_q2=$itemfp->faepa_250+$rrpa_250;
            }
            if($itemfp->faepa_500==0){
                $aqf_q3=0;  
            }else{
                $aqf_q3=$itemfp->faepa_500+$rrpa_500;
            }
            if($itemfp->faepa_1000==0){
                $aqf_q4=0;  
            }else{
                $aqf_q4=$itemfp->faepa_1000+$rrpa_1000;
            }
            if($itemfp->faepa_2000==0){
                $aqf_q5=0;  
            }else{
                $aqf_q5=$itemfp->faepa_2000+$rrpa_2000;
            }
            if($itemfp->faepa_3000==0){
                $aqf_q6=0;  
            }else{
                $aqf_q6=($itemfp->faepa_3000+$itemfp->faepa_4000)/2+$rrpa_4000;
            }
            if($itemfp->faepa_6000==0){
                $aqf_q7=0;  
            }else{
                $aqf_q7=($itemfp->faepa_6000+$itemfp->faepa_8000)/2+$rrpa_8000;
            }
            
            $htmlg.='<table class="table table-bordered tablev">
                <tr>
                    <th class="titulos" colspan="2">Atenuación Q Final</th>
                    <th class="titulos">Lj-Qj/10</th>
                    <th class="titulos"><img src="'.base_url().'public/img/formula7_2.png" width="50px"></th>
                </tr>
                <tr>
                    <th class="titulos">Q1</th>
                    <td>';
                        if($aqf_q1>0){ $htmlg.=$aqf_q1;}else{ $htmlg.='--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q1>0){ 
                                $aqf_q1_r1=($itrea2->rea_125-$aqf_q1)/10;
                                $htmlg.=$aqf_q1_r1;
                            }else{ 
                                $aqf_q1_r1=0;
                                $htmlg.='---';
                            }
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q1_r1>0){ 
                                $aqf_q1_r2=pow(10,$aqf_q1_r1);
                                $htmlg.= round($aqf_q1_r2,2);
                            }else{ 
                                $aqf_q1_r2=0;
                                $htmlg.= '---';
                            }
                    $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q2</th>
                    <td>';
                        if($aqf_q2>0){ $htmlg.=$aqf_q2;}else{ $htmlg.='--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q2>0){ 
                                $aqf_q2_r1=($itrea2->rea_250-$aqf_q2)/10;
                                $htmlg.= round($aqf_q2_r1,2);
                            }else{ 
                                $aqf_q2_r1=0;
                                $htmlg.= '---';
                            }
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q2_r1>0){ 
                                $aqf_q2_r2=pow(10,$aqf_q2_r1);
                                $htmlg.= round($aqf_q2_r2,2);
                            }else{ 
                                $aqf_q2_r2=0;
                                $htmlg.='---';
                            }
                $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q3</th>
                    <td>';
                        if($aqf_q3>0){ $htmlg.=$aqf_q3;}else{ $htmlg.= '--';}
                    $htmlg.='</td>
                    <td>';
                            if($aqf_q3>0){ 
                                $aqf_q3_r1=($itrea2->rea_500-$aqf_q3)/10;
                                $htmlg.=round($aqf_q3_r1,2);
                            }else{ 
                                $aqf_q3_r1=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q3_r1>0){ 
                                $aqf_q3_r2=pow(10,$aqf_q3_r1);
                                $htmlg.= round($aqf_q3_r2,2);
                            }else{ 
                                $aqf_q3_r2=0;
                                $htmlg.= '---';
                            };
                $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q4</th>
                    <td>';
                    if($aqf_q4>0){ $htmlg.= $aqf_q4;}else{ $htmlg.= '--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q4>0){ 
                                $aqf_q4_r1=($itrea2->rea_1000-$aqf_q4)/10;
                                $htmlg.= round($aqf_q4_r1,2);
                            }else{ 
                                $aqf_q4_r1=0;
                                $htmlg.= '---';
                            }
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q4_r1>0){ 
                                $aqf_q4_r2=pow(10,$aqf_q4_r1);
                                $htmlg.= round($aqf_q4_r2,2);
                            }else{ 
                                $aqf_q4_r2=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q5</th>
                    <td>';
                    if($aqf_q5>0){ $htmlg.= $aqf_q5;}else{ $htmlg.= '--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q5>0){ 
                                $aqf_q5_r1=($itrea2->rea_2000-$aqf_q5)/10;
                                $htmlg.= round($aqf_q5_r1,2);
                            }else{ 
                                $aqf_q5_r1=0;
                                $htmlg.= '---';
                            }
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q5_r1>0){ 
                                $aqf_q5_r2=pow(10,$aqf_q5_r1);
                                $htmlg.= round($aqf_q5_r2,2);
                            }else{ 
                                $aqf_q5_r2=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q6</th>
                    <td>'; 
                    if($aqf_q6>0){ $htmlg.= $aqf_q6;}else{ $htmlg.= '--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q6>0){ 
                                $aqf_q6_r1=($itrea2->rea_4000-$aqf_q6)/10;
                                $htmlg.= round($aqf_q6_r1,2);
                            }else{ 
                                $aqf_q6_r1=0;
                                $htmlg.= '---';
                            }
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q6_r1>0){ 
                                $aqf_q6_r2=pow(10,$aqf_q6_r1);
                                $htmlg.= round($aqf_q6_r2,2);
                            }else{ 
                                $aqf_q6_r2=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                </tr>
                <tr>
                    <th class="titulos">Q7</th>
                    <td>';
                    if($aqf_q7>0){ $htmlg.= $aqf_q7;}else{ $htmlg.= '--';}
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q7>0){ 
                                $aqf_q7_r1=($itrea2->rea_8000-$aqf_q7)/10;
                                $htmlg.= round($aqf_q7_r1,2);
                            }else{ 
                                $aqf_q7_r1=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                    <td>'; 
                            if($aqf_q7_r1>0){ 
                                $aqf_q7_r2=pow(10,$aqf_q7_r1);
                                $htmlg.= round($aqf_q7_r2,2);
                            }else{ 
                                $aqf_q7_r2=0;
                                $htmlg.= '---';
                            };
                    $htmlg.='</td>
                </tr>
                <tr>
                    <td colspan="3">Suma</td>
                    <td>';
                        if($aqf_q7>0){
                            $aqf_q1_q7_suma=$aqf_q1_r2+$aqf_q2_r2+$aqf_q3_r2+$aqf_q4_r2+$aqf_q5_r2+$aqf_q6_r2+$aqf_q7_r2;
                            $htmlg.= round($aqf_q1_q7_suma,2);
                        }else{
                            $aqf_q1_q7_suma=0;
                            $htmlg.= '---';
                        }
                        
                    $htmlg.='</td>
                </tr>
            </table>';
        }

        $htmlg.='<table class="table">
            <tr>
                <th colspan="2">FACTOR DE REDUCCIÓN, MODELO POR BANDAS DE OCTAVAS</th>
            </tr>
            <tr>
                <td><img src="'.base_url().'public/img/formula8.png" height="45px"></td>
                <td>Ri dB(A)=<b>';

                    $Ri_dBa=$nivel_expocion_ruido-10*log10($aqf_q1_q7_suma)-10;
                    $htmlg.= round($Ri_dBa,1);
                $htmlg.='</b></td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <th colspan="3">FACTOR DE REDUCCIÓN, MODELO CON MEDICIONES DE RUIDO</th>
            </tr>
            <tr>
                <td>NRR=<b>'; 
                    if($val_nrr==0){
                        $nrr=26;
                    }else{
                        $nrr=$val_nrr;
                    }            
                    $htmlg.= $nrr; 
                $htmlg.='</b></td>
                <td><img src="'.base_url().'public/img/formula99.jpg" height="45px"></td>
                <td>R dB(A)=<b>'; 
                    $R_dBa=($nrr-7)/2;
                    $htmlg.= $R_dBa;
                $htmlg.='</b></td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <th colspan="2">NIVEL DE RUIDO EFECTIVO</th>
            </tr>
            <tr>
                <td>NRE = dB(A) - R</td>
                <td>NRE dB(A)= <b>';
                    $NRW_dBa=round($nivel_expocion_ruido,2)-$R_dBa;
                    $htmlg.= round($NRW_dBa,1);
                $htmlg.='</b></td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <td width="20%"></td>
                <td width="60%"><img src="'.$item->grafo2.'" ></td>
                <td width="20%"></td></tr>
        </table>
        ';
    }
$pdf->writeHTML($htmlg, true, false, true, false, '');
//======================================================================================================

$htmlg.='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IV. Ficha técnica de la protección personal auditiva ---------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IV</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">FICHA TÉCNICA DE LA PROTECCIÓN PERSONAL AUDITIVA</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');
$cont_f=0;

$getfi=$this->ModeloCatalogos->getselectwheren('imagenes_fichas',array('id_nom'=>$idnom,'estatus'=>1));
foreach ($getfi->result() as $item) {
    $pdf->AddPage('P', 'A4');
    $cont_f++;

    /*$htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Ficha técnica de la protección personal auditiva</i></td></tr><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/fichas/'.$item->url_img.'"></td></tr></table>';*/
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Ficha técnica de la protección personal auditiva</i></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->Image(base_url().'uploads/fichas/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);

    //$htmlg='<p style="text-align:cente;"><i>Ficha técnica de la protección personal auditiva</i></p>';
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    //$pdf->Image($url, $x, $y, $w, $h, $tipeimg, $link, '', true, 150, '', false, false, 1, false, false, false);
    //$pdf->Image(base_url().'uploads/fichas/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

}
if($cont_f==0){
    $pdf->AddPage('P', 'A4');
    $htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i>Ficha técnica de la protección personal auditiva</i></td></tr></table>
            <table><tr><td align="center" height="200px"></td></tr><tr><td style="font-size:16px;" align="center">EN EL CENTRO DE TRABAJO NO UTILIZAN EQUIPO DE PROTECCIÓN PERSONAL AUDITIVO</td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
}

$pdf->AddPage('P', 'A4');
$pdf->Bookmark('V. Descripción del proceso de fabricación del centro de trabajo evaluado ----------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO V</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DESCRIPCIÓN DEL PROCESO DE FABRICACIÓN DEL CENTRO DE TRABAJO EVALUADO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$pdf->AddPage('P', 'A4');
$htmlg='<table border="0" >
        <tr><td align="center" height="200px"></td></tr>
        <tr><td style="font-size:16px;" align="center">'.$txt_proceso_fabrica.'</td></tr>
    </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VI. Programa de mantenimiento a luminarias -----------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VI</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">PROGRAMA DE<br>MANTENIMIENTO A <br>MAQUINARIA Y EQUIPO<br> GENERADOR DE RUIDO</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
    if($datosnomv->num_rows()>0){
        foreach ($datosnomv->result() as $item) {
            $pdf->AddPage('P', 'A4');
            
            /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgPrograma/'.$item->url_img.'"></td></tr></table>';
            $pdf->writeHTML($htmlg, true, false, true, false, '');*/
            $pdf->Image(base_url().'uploads/imgPrograma/'.$item->url_img, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            //$pdf->Image(base_url().'uploads/imgPrograma/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, false);
        }
    }else{
        $pdf->AddPage('P', 'A4');
        $htmlg='<table border="0" >
                <tr><td align="center" height="200px"></td></tr>
                <tr><td style="font-size:16px;" align="center">NO PROPORCIONADO POR SER UN DOCUMENTO CONFIDENCIAL</td></tr>
            </table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
    }
//$pdf->writeHTML($htmlg, true, false, true, false, '');
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VII. Informe de calibración del equipo de medición ----------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VII</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">INFORME DE CALIBRACIÓN DEL EQUIPO DE MEDICIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');


$datosequipoc=$this->ModeloNom->getCertEquipos($id_sonometro,$id_calibrador);
foreach ($datosequipoc as $item) {
    $htmlg='';
    $pdf->AddPage('P', 'A4');
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgEquipos/'.$item->certificado.'"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt='Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt=$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
}

$datosequipoc=$this->ModeloNom->getCertEquiposPersonal($idnom);
foreach ($datosequipoc as $item) {
    $htmlg='';
    $pdf->AddPage('P', 'A4');
    $htmlg.='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgEquipos/'.$item->certificado.'"></td></tr></table>';
    $pdf->writeHTML($htmlg, true, false, true, false, '');
    $pdf->SetXY(50, 130);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt='Documento Informativo';
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

    $pdf->SetXY(50, 140);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 35);
    $txt=$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
}
    
$htmlg='';
$pdf->AddPage('P', 'A4');
$pdf->Bookmark('VIII. Cédula profesional del evaluador ---------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO VIII</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">CÉDULA PROFESIONAL DEL EVALUADOR</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$htmlg='';
$pdf->AddPage('P', 'A4');
$cont_f++;

/*$htmlg='<table><tr><td style="font-family:Arial; font-size:14px" align="center"><i></i></td></tr><tr><td align="center"><img src="'.base_url().'uploads/cedulas/'.$cedula.'" width="800px" height="850px"></td></tr></table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');*/

$pdf->Image(base_url().'uploads/cedulas/'.$cedula, 25, 32, "170", "230", '', '', 'T', false, 300, '', false, false, 0, false, false, false);

    //$pdf->Image(base_url().'uploads/cedulas/'.$cedula, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);


$pdf->AddPage('P', 'A4');
$pdf->Bookmark('IX. Documento de acreditación -----------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO IX</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE ACREDITACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

if($datosacre->num_rows()>0){
    $htmlg='';
    foreach ($datosacre->result() as $item) {
        $pdf->AddPage('P', 'A4'); 
        
        /*$htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');*/
        
        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);

        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
    }   
}

$pdf->AddPage('P', 'A4');
//'titulo','nivel(int)','posicion(int)','pagina actual','B = Bold, I = Italic, BI = Bold + Italic','color'
$pdf->Bookmark('X. Documento de aprobación --------------------------------------------------------------------------', 1, 0, '', '', array(0,0,0));
$htmlg='
        <table border="0" >
            <tr><td height="280px"></td></tr>
            <tr><td style="font-family:Arial; font-size:25px; color: rgb(235,122,38)" align="right">ANEXO X</td></tr>
            <tr><td height="50px"></td></tr>
            <tr><td style="font-family:Arial; font-size:35px; color: rgb(0,57,88)" align="right">DOCUMENTO DE APROBACIÓN</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$txt_fin=""; $cont_acre=0;
if($datosacre2->num_rows()>0){
    $htmlg='';
    foreach ($datosacre2->result() as $item) {
        $cont_acre++;
    }
    $cont2=0;
    foreach ($datosacre2->result() as $item) { //falta poner el folio - marca de agua en todoas las imgs solo lo pone en la ultima
        $cont2++;
        $pdf->SetMargins(20,25,20); 
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->AddPage('P', 'A4');
        
        /*
        $htmlg='<table><tr><td align="center"><img width="800px" height="850px" src="'.base_url().'uploads/imgAcre/'.$item->url_img.'"></td></tr></table>';
        $pdf->writeHTML($htmlg, true, false, true, false, '');
        */

        $pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '160', '230', '', '', 'M', true, 150, 'C', false, false, false, true, false, true);
        //$pdf->Image(base_url().'uploads/imgAcre/'.$item->url_img, '', '', '', 230, '', '', 'M', true, 150, 'C', false, false, 1, false, false, true);
        $pdf->SetXY(50, 130);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt='Documento Informativo';
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetXY(50, 140);
        $pdf->SetTextColor(0,57,88);
        $pdf->SetFont('dejavusans', '', 35);
        $txt=$folio;
        $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        if($cont_acre==$cont2){
            /*$txt_fin.='<table>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:#003958; font-size:12px">El informe de resultados '.$folio.' consta de un total de <u>'.$GLOBALS["tot_pgs"].'</u> páginas</p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <p style="color:red; font-size:12px">---------------- FIN DEL INFORME DE RESULTADOS ----------------</p>
                    </td>
                </tr>
            </table>';*/
            //$pdf->writeHTMLCell(170, '', 12, 269, $txt_fin, 0, 0, 0, true, 'C', true);
            //$pdf->writeHTML($txt_fin, true, false, true, false, '');

            $pdf->SetXY(60, 214);
            $pdf->SetTextColor(0,57,88);
            $pdf->SetFont('dejavusans', '', 9.5);
            $txt_fin='El informe de resultados '.$folio.' consta de un total de '.$pdf->getAliasNbPages().' páginas';
            $txt_fin2='---------------------- FIN DEL INFORME DE RESULTADOS ----------------------';
            $pdf->Cell(100, 50, $txt_fin, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');

            $pdf->SetXY(55, 217);
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('dejavusans', '', 9.5);
            $pdf->Cell(100, 50, $txt_fin2, 0, $ln=0, 'C', 0, '', 0, false, 'N', 'N');
        }
    }
    //$pdf->writeHTML($htmlg, true, false, true, false, '');
    /*$pdf->SetXY(50, 150);
    $pdf->SetTextColor(0,57,88);
    $pdf->SetFont('dejavusans', '', 23);
    $txt='Documento Informativo '.$folio;
    $pdf->Cell(100, 50, $txt, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');*/
}


// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));

$bookmark_templates[0]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();



$pdf->Output('EntregableNom11.pdf', 'I');

?>