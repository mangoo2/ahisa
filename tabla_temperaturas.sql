-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-12-2022 a las 00:21:32
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ahisa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_temperaturas`
--

DROP TABLE IF EXISTS `tabla_temperaturas`;
CREATE TABLE IF NOT EXISTS `tabla_temperaturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `v_min` int(11) NOT NULL COMMENT 'solo info para calculos',
  `velocidad` int(11) NOT NULL,
  `v_max` int(11) NOT NULL COMMENT 'solo info para calculos',
  `t_10` int(11) NOT NULL,
  `t_4` int(11) NOT NULL,
  `t_m1` int(11) NOT NULL,
  `t_m7` int(11) NOT NULL,
  `t_m12` int(11) NOT NULL,
  `t_m18` int(11) NOT NULL,
  `t_m23` int(11) NOT NULL,
  `t_m29` int(11) NOT NULL,
  `t_m34` int(11) NOT NULL,
  `t_m40` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tabla_temperaturas`
--

INSERT INTO `tabla_temperaturas` (`id`, `v_min`, `velocidad`, `v_max`, `t_10`, `t_4`, `t_m1`, `t_m7`, `t_m12`, `t_m18`, `t_m23`, `t_m29`, `t_m34`, `t_m40`, `activo`) VALUES
(1, 0, 8, 12, 10, 4, -1, -7, -12, -18, -23, -29, -34, -40, 1),
(2, 12, 16, 20, 9, 3, -3, -9, -14, -21, -26, -32, -38, -44, 1),
(3, 20, 24, 28, 4, -2, -9, -15, -23, -31, -36, -43, -50, -57, 1),
(4, 28, 32, 36, 2, -6, -13, -21, -28, -36, -43, -50, -58, -65, 1),
(5, 36, 40, 44, 0, -8, -16, -23, -32, -39, -47, -55, -63, -71, 1),
(6, 44, 48, 52, -1, -9, -18, -26, -34, -42, -50, -59, -67, -76, 1),
(7, 52, 56, 60, -2, -11, -19, -28, -36, -44, -53, -62, -70, -78, 1),
(8, 60, 64, 64, -3, -12, -20, -29, -37, -46, -55, -63, -72, -80, 1),
(9, 64, 66, 300, -3, -12, -21, -29, -38, -47, -56, -65, -73, -82, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
