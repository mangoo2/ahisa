ALTER TABLE `nom22` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;


ALTER TABLE `nom22d` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;


ALTER TABLE `fuentes_genera_nom22` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `valores_resistencia_nom22` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `emision_ruido`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `ruido_fte_nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `ruido_fondo_nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `ruido_aisla_nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `distancias_nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom15` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom15_punto` ADD `insert_user` INT NULL DEFAULT NULL AFTER `lvf_prom`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `medicion_temp_nom15` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom15_punto_elevada` ADD `insert_user` INT NULL DEFAULT NULL AFTER `chart_final`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `medicion_temp_nom15_elevadas2` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom` ADD `insert_user` INT NULL DEFAULT NULL AFTER `observaciones`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom25_detalle` ADD `insert_user` INT NULL DEFAULT NULL AFTER `grafica`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `condiciones_nom25det` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_nom11` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_nom22` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_nom81` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_nom15` ADD `insert_user` INT NULL DEFAULT NULL AFTER `reg`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_paso4_nom11` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_paso2_nom11` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_paso2_nom25` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_areas_nom15` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_paso3_nom11` ADD `insert_user` INT NULL DEFAULT NULL AFTER `estatus`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `puntos_conclusion` ADD `insert_user` INT NULL DEFAULT NULL AFTER `img_chart`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_inicial` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `reconocimiento_inicial_detalle` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;


ALTER TABLE `nom11` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11d` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11d_lecturas` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`,
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11d_reg_espec_acustico` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11d_faepa` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11_personal` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11_personal_ti` ADD `insert_user` INT NULL DEFAULT NULL AFTER `activo`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;

ALTER TABLE `nom11d_puntos` ADD `insert_user` INT NULL DEFAULT NULL AFTER `grafo2`, 
ADD `insert_reg` DATETIME NULL DEFAULT NULL AFTER `insert_user`, 
ADD `update_user` INT NULL DEFAULT NULL AFTER `insert_reg`, 
ADD `update_reg` DATETIME NULL DEFAULT NULL AFTER `update_user`, 
ADD `delete_user` INT NULL DEFAULT NULL AFTER `update_reg`, 
ADD `delete_reg` DATETIME NULL DEFAULT NULL AFTER `delete_user`;